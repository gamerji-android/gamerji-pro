package com.esports.gamerjipro.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esports.gamerjipro.Model.DashboardGamesModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.Utils.AppConstants;
import com.esports.gamerjipro.activity.ActivityGameType;
import com.esports.gamerjipro.activity.QuickGameTypesActivity;

import java.util.ArrayList;

public class GamesListAdapter extends RecyclerView.Adapter<GamesListAdapter.ViewHolder> {
    private ArrayList<DashboardGamesModel> games_list_models;
    private Context context;

    public GamesListAdapter(Context context, ArrayList<DashboardGamesModel> games_list_models) {
        this.context = context;
        this.games_list_models = games_list_models;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_games_list_grid, parent, false);
        // work here if you need to control height of your items

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final DashboardGamesModel dashboardGamesModel = games_list_models.get(position);
        holder.txt_game_name.setText(dashboardGamesModel.Name);
        Glide.with(context).load(dashboardGamesModel.FeaturedImage).into(holder.img_game);

        holder.img_game.setOnClickListener(v ->
        {
            if(dashboardGamesModel.GameID.equalsIgnoreCase("")) {
                Intent i = new Intent(context, QuickGameTypesActivity.class);
                i.putExtra(AppConstants.BUNDLE_QUICK_GAME_TYPE, dashboardGamesModel.Name);
                context.startActivity(i);
            } else {
                Intent i = new Intent(context, ActivityGameType.class);
                i.putExtra("game_type", dashboardGamesModel.GameID);
                i.putExtra("game_name", dashboardGamesModel.Name);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return games_list_models.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_game;
        TextView txt_game_name;
        CardView lyt_cv_games;
        View view_shadow;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_game = itemView.findViewById(R.id.img_game);
            txt_game_name = itemView.findViewById(R.id.txt_game_name);
            lyt_cv_games = itemView.findViewById(R.id.lyt_cv_games);
            view_shadow = itemView.findViewById(R.id.view_shadow);
        }
    }
}
