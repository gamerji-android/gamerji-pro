package com.esports.gamerjipro.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Model.NotificationModel;
import com.esports.gamerjipro.R;

import java.util.ArrayList;


public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder>
{

    private Context context;
    private ArrayList<NotificationModel> notificationModelArrayList;
    private SubNotificationAdapter subNotificationAdapter;

    public NotificationAdapter(Context context, ArrayList<NotificationModel> notificationModelArrayList)
    {
        this.context=context;
        this.notificationModelArrayList=notificationModelArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notification_layout, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {

        holder.mTxtDate.setText(notificationModelArrayList.get(position).getDate());
        subNotificationAdapter= new SubNotificationAdapter(context,notificationModelArrayList.get(position).getNotificationDetailModels());
        holder.mRecyclerViewList.setAdapter(subNotificationAdapter);
    }

    @Override
    public int getItemCount()
    {
        return notificationModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        RecyclerView mRecyclerViewList;
        TextView mTxtDate;

        ViewHolder(View itemView)
        {
            super(itemView);
            mRecyclerViewList = itemView.findViewById(R.id.mRecyclerViewList);
            mRecyclerViewList.setLayoutManager(new LinearLayoutManager(context));
            mRecyclerViewList.setItemAnimator(new DefaultItemAnimator());
            mTxtDate = itemView.findViewById(R.id.mTxtDate);
        }
    }
}
