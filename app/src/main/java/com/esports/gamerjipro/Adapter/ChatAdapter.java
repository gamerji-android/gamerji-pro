package com.esports.gamerjipro.Adapter;

import android.content.Context;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esports.gamerjipro.Model.ChatModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.Pref;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder>
{
    private Context context;
    private ArrayList<ChatModel> chatModelArrayList;

    public ChatAdapter(Context context, ArrayList<ChatModel> chatModelArrayList)
    {
        this.chatModelArrayList=chatModelArrayList;
        this.context=context;

    }

    @NonNull
    @Override
    public ChatAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_chat_layout, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatAdapter.ViewHolder holder, int position)
    {
        if (chatModelArrayList.get(position).getSender_id().equalsIgnoreCase(Pref.getValue(context, Constants.UserID,"",Constants.FILENAME)))
        {
            holder.lyt_sender.setVisibility(View.GONE);

            holder.txt_my_message.setText(chatModelArrayList.get(position).getChat_message());
            Calendar cal = Calendar.getInstance(Locale.ENGLISH);
            cal.setTimeInMillis(Long.parseLong(chatModelArrayList.get(position).getChat_datetime()) * 1000);
            String lastOnline = DateFormat.format("HH:mm a", cal).toString();
            holder.txt_receiver_time.setText(lastOnline);
            holder.txt_my_team_name.setText(chatModelArrayList.get(position).getSender_full_name());

        }
        else
        {
            holder.lyt_receiver.setVisibility(View.GONE);
            holder.lyt_sender.setVisibility(View.VISIBLE);
            holder.txt_sender_message.setText(chatModelArrayList.get(position).getChat_message());
            Glide.with(context).load(chatModelArrayList.get(position).getLevel_icon()).into(holder.img_user_icon);
            Calendar cal = Calendar.getInstance(Locale.ENGLISH);
            cal.setTimeInMillis(Long.parseLong(chatModelArrayList.get(position).getChat_datetime()) * 1000);
            String lastOnline = DateFormat.format("HH:mm a", cal).toString();
            holder.txt_sender_time.setText(lastOnline);
            holder.txt_oppo_team_name.setText(chatModelArrayList.get(position).getSender_full_name());
        }
    }

    @Override
    public int getItemCount()
    {
        return chatModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {

        LinearLayout lyt_sender;
        TextView txt_my_message,txt_sender_message,txt_sender_time,txt_receiver_time,txt_oppo_team_name,txt_my_team_name;
        CardView lyt_receiver;
        ImageView img_user_icon;
        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            txt_my_message=itemView.findViewById(R.id.txt_my_message);
            lyt_sender=itemView.findViewById(R.id.lyt_sender);
            txt_sender_message=itemView.findViewById(R.id.txt_sender_message);
            lyt_receiver=itemView.findViewById(R.id.lyt_receiver);
            txt_receiver_time=itemView.findViewById(R.id.txt_receiver_time);
            txt_sender_time=itemView.findViewById(R.id.txt_sender_time);
            txt_oppo_team_name=itemView.findViewById(R.id.txt_oppo_team_name);
            txt_my_team_name=itemView.findViewById(R.id.txt_my_team_name);
            img_user_icon=itemView.findViewById(R.id.img_user_icon);
        }
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
