package com.esports.gamerjipro.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Model.GetVideosModel;
import com.esports.gamerjipro.R;

import java.util.ArrayList;

public class YoutubeVideoAdapter extends RecyclerView.Adapter<YoutubeVideoAdapter.ViewHolder>
{
    Context context;
    private ArrayList<GetVideosModel.VideoData.VideosData> videosDataArrayList;

    public YoutubeVideoAdapter(Context context, ArrayList<GetVideosModel.VideoData.VideosData> videosDataArrayList)
    {
        this.videosDataArrayList=videosDataArrayList;
        this.context=context;

    }

    @NonNull
    @Override
    public YoutubeVideoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_youtube_webview, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull YoutubeVideoAdapter.ViewHolder holder, int position)
    {
        holder.webView.loadUrl(videosDataArrayList.get(position).YoutubeLink);
        holder.txt_video_title.setText(videosDataArrayList.get(position).Name);
        holder.txt_video_views.setText(videosDataArrayList.get(position).Views+" Views");

    }

    @Override
    public int getItemCount() {
        return videosDataArrayList.size();
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public int getItemViewType(int position)
    {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {

        WebView webView ;
        TextView txt_video_title,txt_video_views;
        @SuppressLint("SetJavaScriptEnabled")
        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            webView=itemView.findViewById(R.id.wv_youtube);
            txt_video_title=itemView.findViewById(R.id.txt_video_title);
            txt_video_views=itemView.findViewById(R.id.txt_video_views);

            webView.setWebChromeClient(new WebChromeClient());
            webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setPluginState(WebSettings.PluginState.ON);
            webView.getSettings().setAllowFileAccess(true);
            webView.getSettings().setDefaultFontSize(18);
        }
    }
}
