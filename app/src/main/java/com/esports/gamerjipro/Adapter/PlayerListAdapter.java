package com.esports.gamerjipro.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esports.gamerjipro.Model.ContestDetailsModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.Pref;

import java.util.ArrayList;

public class PlayerListAdapter extends RecyclerView.Adapter<PlayerListAdapter.ViewHolder>
{
    private Context context;
    private ArrayList<ContestDetailsModel.ContestsData.UsersData> playerListModels;
    private String status;
    int user_position;

    public PlayerListAdapter(Context context, ArrayList<ContestDetailsModel.ContestsData.UsersData> playerListModels, String status)
    {
        this.context=context;
        this.playerListModels=playerListModels;
        this.status=status;
    }

    @NonNull
    @Override
    public PlayerListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_player_list, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull PlayerListAdapter.ViewHolder holder, int position)
    {
        holder.txt_name.setText(playerListModels.get(position).Name);
        Glide.with(context).load(playerListModels.get(position).UserProfileIcon).into(holder.img_player_badge);
        if (playerListModels.get(position).Kills.isEmpty())
            holder.txt_kill.setText("-");
        else
            holder.txt_kill.setText(playerListModels.get(position).Kills);

        if (playerListModels.get(position).Rank.isEmpty())
            holder.txt_rank.setText("-");
        else
            holder.txt_rank.setText(playerListModels.get(position).Rank);

        if (playerListModels.get(position).MobileNumber.isEmpty())
            holder.txt_acc_number.setVisibility(View.GONE);
        else
        holder.txt_acc_number.setText("*****"+playerListModels.get(position).MobileNumber.substring(5));

        // Profile Hide
       /*holder.img_player_badge.setOnClickListener(v ->
        {
            Intent i = new Intent(context, ActivityViewPlayerProfile.class);
            i.putExtra("player_id",playerListModels.get(position).UserID);
            context.startActivity(i);
        });*/

        /*holder.img_ss.setOnClickListener(v ->
        {
            if (playerListModels.get(position).ScreenshotFlag)
            {
                final Dialog nagDialog = new Dialog(context,android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
                nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                nagDialog.setCancelable(false);
                nagDialog.setContentView(R.layout.preview_image);
                Button btnClose = nagDialog.findViewById(R.id.btnIvClose);
                ImageView ivPreview = nagDialog.findViewById(R.id.iv_preview_image);
                Glide.with(context).load(playerListModels.get(position).ScreenshotURL).into(ivPreview);
                btnClose.setOnClickListener(arg0 -> nagDialog.dismiss());
                nagDialog.show();
            }
        });

        if (playerListModels.get(position).ScreenshotFlag)
        {
            holder.img_ss.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_check));
            holder.img_ss.setEnabled(true);
            holder.img_ss.setOnClickListener(v ->
            {
                Intent i = new Intent(context, Full_Image_Activity.class);
                i.putExtra("image",playerListModels.get(position).ScreenshotURL);
                context.startActivity(i);
            });
        }
        else
        {
            holder.img_ss.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_close));
            holder.img_ss.setEnabled(false);
        }*/

        if (status.equalsIgnoreCase(Constants.COMPLETED))
        {
            if (!playerListModels.get(position).WinningAmount.isEmpty())
            {
                holder.lyt_congratulartions.setVisibility(View.VISIBLE);
                holder.txt_winnigs_amount.setText(playerListModels.get(position).WinningAmount);
            }
        }
        if (Pref.getValue(context, Constants.UserID,"",Constants.FILENAME).equalsIgnoreCase(playerListModels.get(position).UserID))
        {
            user_position=position;
        }





       /* if (position==playerListModels.size()-1)
        {
         //new Handler().postDelayed(this::notifyDataSetChanged,100);
            ContestDetailsModel.ContestsData.UsersData usersData;
            usersData=playerListModels.get(user_position);
            playerListModels.remove(user_position);
            playerListModels.add(0,usersData);
        }*/

    }

    @Override
    public int getItemViewType(int position)
    {
        return position;
    }

    @Override
    public int getItemCount()
    {
        return playerListModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView txt_name,txt_kill,txt_rank,txt_acc_number,txt_winnigs_amount;
        ImageView img_player_badge;

        LinearLayout lyt_palyers,lyt_congratulartions;
        ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            txt_name=itemView.findViewById(R.id.txt_name);
            txt_kill=itemView.findViewById(R.id.txt_kill);
//            img_ss=itemView.findViewById(R.id.img_ss);
            txt_rank=itemView.findViewById(R.id.txt_rank);
            txt_acc_number=itemView.findViewById(R.id.txt_acc_number);
            lyt_palyers=itemView.findViewById(R.id.lyt_palyers);
            lyt_congratulartions=itemView.findViewById(R.id.lyt_congratulartions);
            txt_winnigs_amount=itemView.findViewById(R.id.txt_winnigs_amount);
            img_player_badge=itemView.findViewById(R.id.img_player_badge);
        }
    }
}

