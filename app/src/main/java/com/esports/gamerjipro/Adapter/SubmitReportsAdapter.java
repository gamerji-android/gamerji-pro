package com.esports.gamerjipro.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Interface.TournamentTimeSelectedListener;
import com.esports.gamerjipro.Model.ReportsDataModel;
import com.esports.gamerjipro.R;

import java.util.ArrayList;

public class SubmitReportsAdapter extends RecyclerView.Adapter<SubmitReportsAdapter.ViewHolder>
        {

        Context context;
private TournamentTimeSelectedListener onTournamentTimeSelectedListener;
private ArrayList<ReportsDataModel.ReportsData> reportsDataArrayList;
private int lastCheckedPosition = -1;

public SubmitReportsAdapter(Context context, ArrayList<ReportsDataModel.ReportsData> reportsDataArrayList, TournamentTimeSelectedListener onTournamentTimeSelectedListener)
        {
        this.context= context;
        this.onTournamentTimeSelectedListener= onTournamentTimeSelectedListener;
        this.reportsDataArrayList=reportsDataArrayList;
        }

@NonNull
@Override
public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {

        View itemView = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.item_time_slots, parent, false);
        return new ViewHolder(itemView);
        }

@Override
public void onBindViewHolder(@NonNull ViewHolder holder, int position)
        {
        holder.rb_time.setText(reportsDataArrayList.get(position).ReportTitle);
        holder.rb_time.setChecked(position == lastCheckedPosition);
        }

@Override
public int getItemCount()
        {
        return reportsDataArrayList.size();
        }

class ViewHolder extends RecyclerView.ViewHolder
{

    RadioButton rb_time;
    public ViewHolder(@NonNull View itemView)
    {
        super(itemView);
        rb_time=itemView.findViewById(R.id.rb_time);
        rb_time.setOnClickListener(v ->
        {
            lastCheckedPosition = getAdapterPosition();
            //because of this blinking problem occurs so
            //i have a suggestion to add notifyDataSetChanged();
            //   notifyItemRangeChanged(0, list.length);//blink list problem
            onTournamentTimeSelectedListener.onReportSelected(reportsDataArrayList.get(getAdapterPosition()));
            notifyDataSetChanged();
        });

    }
}
}
