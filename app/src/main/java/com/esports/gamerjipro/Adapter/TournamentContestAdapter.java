package com.esports.gamerjipro.Adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.TransitionManager;

import com.bumptech.glide.Glide;
import com.esports.gamerjipro.Interface.ScreenshotClickListener;
import com.esports.gamerjipro.Interface.WinnerClickListener;
import com.esports.gamerjipro.Model.TournamentDetailModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.FileUtils;
import com.esports.gamerjipro.activity.ActivityTournamentDetail;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class TournamentContestAdapter extends RecyclerView.Adapter<TournamentContestAdapter.ViewHolder> {
    Context context;
    private ArrayList<TournamentDetailModel.TournamentData.ContestsData> contestsDataArrayList;
    private String captainID;
    private WinnerClickListener winnerClickListener;
    private ScreenshotClickListener screenshotClickListener;
    private int mExpandedPosition = -1;
    private boolean isFirst = true;

    public TournamentContestAdapter(Context context, ArrayList<TournamentDetailModel.TournamentData.ContestsData> contestsDataArrayList, String captainID,
                                    WinnerClickListener winnerClickListener, ScreenshotClickListener screenshotClickListener) {
        this.contestsDataArrayList = contestsDataArrayList;
        this.context = context;
        this.captainID = captainID;
        this.winnerClickListener = winnerClickListener;
        this.screenshotClickListener = screenshotClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_tournament_contest_detail, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final boolean isExpanded = position == mExpandedPosition;

        if (position == 0) {
            holder.lyt_contest_detail.setVisibility(isExpanded ? View.GONE : View.VISIBLE);
            holder.img_show_hide.setImageDrawable(context.getDrawable(R.drawable.ic_down_arrow));
        } else {
            holder.lyt_contest_detail.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
            holder.img_show_hide.setImageDrawable(context.getDrawable(R.drawable.ic_next_arrow));
        }

        if (isExpanded) {
            holder.img_show_hide.setImageDrawable(context.getDrawable(R.drawable.ic_down_arrow));
        } else {
            holder.img_show_hide.setImageDrawable(context.getDrawable(R.drawable.ic_next_arrow));
        }

        setContestUserData(contestsDataArrayList.get(position).currentContestUserData, holder, contestsDataArrayList.get(position).Status);
        holder.itemView.setActivated(isExpanded);

        holder.itemView.setOnClickListener(v ->
        {
            mExpandedPosition = isExpanded ? -1 : position;
            TransitionManager.beginDelayedTransition(((ActivityTournamentDetail) context).rv_tournament_contests);
            notifyItemChanged(position);

        });
        holder.txt_contest_title.setText(contestsDataArrayList.get(position).Title);
        holder.txt_contest_message.setText(contestsDataArrayList.get(position).Message);
        @SuppressLint("SimpleDateFormat") DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        @SuppressLint("SimpleDateFormat") DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
        String inputDateStr = contestsDataArrayList.get(position).Date;

                    /*if(Pref.getValue(context, Constants.UserID,"",Constants.FILENAME)
                            .equalsIgnoreCase(captainID))
                        holder.cv_ss.setVisibility(View.VISIBLE);
                    else
                        holder.cv_ss.setVisibility(View.GONE);*/

        Date date = null;
        try {
            date = inputFormat.parse(inputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String outputDateStr = outputFormat.format(date);
        holder.txt_date.setText(outputDateStr);
        holder.txt_time.setText(FileUtils.getFormatedDateTime(contestsDataArrayList.get(position).Time, "HH:mm:ss", "hh:mm a"));


        holder.txt_map.setText(contestsDataArrayList.get(position).Map_Length);
        holder.txt_persp.setText(contestsDataArrayList.get(position).Perspective_LevelCap);
        if (contestsDataArrayList.get(position).WinningAmount.equalsIgnoreCase("") ||
                contestsDataArrayList.get(position).WinningAmount.equalsIgnoreCase("0")) {
            holder.winning_amount.setText("-");
            holder.rupees_symbol.setVisibility(View.GONE);
        } else
            holder.winning_amount.setText(" " + contestsDataArrayList.get(position).WinningAmount);

        holder.txt_winners.setText(contestsDataArrayList.get(position).WinnersCount);
        holder.txt_per_kill_title.setText(contestsDataArrayList.get(position).PerKill_MaxLosesTitle);

        if (contestsDataArrayList.get(position).PerKill_MaxLosesCurrency)
            holder.txrupee_symbol.setVisibility(View.VISIBLE);
        else
            holder.txrupee_symbol.setVisibility(View.GONE);

        if (contestsDataArrayList.get(position).WinnersCount.isEmpty() && contestsDataArrayList.get(position).PerKill_MaxLoses.isEmpty()) {
            holder.lyt_winners.setVisibility(View.GONE);
            holder.lyt_kills.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 3.0f);
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 1.0f);
            holder.lyt_winnings.setLayoutParams(params);
            holder.lyt_entry_fees.setLayoutParams(params1);

        } else if (contestsDataArrayList.get(position).WinnersCount.isEmpty()) {
            holder.lyt_winners.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 2.0f);
            holder.lyt_kills.setLayoutParams(params);
        } else if (contestsDataArrayList.get(position).PerKill_MaxLoses.isEmpty()) {
            holder.lyt_kills.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 2.0f);
            holder.lyt_winners.setLayoutParams(params);
        } else {
            if (Integer.parseInt(contestsDataArrayList.get(position).WinnersCount) == 1)
                holder.txt_winner_title.setText("Winner");
        }

        if (!contestsDataArrayList.get(position).RoomColumn.isEmpty()) {
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 1.0f);
            holder.lyt_columns.setVisibility(View.VISIBLE);
            holder.lyt_columns.setLayoutParams(params1);
            holder.lyt_room.setLayoutParams(params1);
            holder.lyt_password.setLayoutParams(params1);
            holder.txt_column.setText(contestsDataArrayList.get(position).RoomColumn);
            holder.lyt_password.setLayoutParams(params1);
        } else {
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 1.5f);
            holder.lyt_columns.setVisibility(View.GONE);
            holder.txt_slot_info.setVisibility(View.GONE);
            holder.lyt_password.setLayoutParams(params1);
            holder.lyt_room.setLayoutParams(params1);
        }


        holder.txt_perkill.setText(" " + contestsDataArrayList.get(position).PerKill_MaxLoses);
        if (contestsDataArrayList.get(position).EntryFee.equalsIgnoreCase("0")) {
            holder.txt_entry_fees.setText("Free");
            holder.txt_entry_rupee.setVisibility(View.GONE);
        } else
            holder.txt_entry_fees.setText(" " + contestsDataArrayList.get(position).EntryFee);

        if (contestsDataArrayList.get(position).RoomID.isEmpty())
            holder.txt_room_id.setText("-");
        else
            holder.txt_room_id.setText(contestsDataArrayList.get(position).RoomID);

        if (contestsDataArrayList.get(position).RoomPassword.isEmpty())

            holder.txt_room_pass.setText("-");
        else
            holder.txt_room_pass.setText(contestsDataArrayList.get(position).RoomPassword);


        int available_spots = Integer.parseInt(contestsDataArrayList.get(position).TotalSpots);
        int joined_spots = Integer.parseInt(contestsDataArrayList.get(position).JoinedSpots);
        int remaining_spots = available_spots - joined_spots;
        if (remaining_spots > 1)
            holder.txt_available_spots.setText(remaining_spots + " players remaining");
        else
            holder.txt_available_spots.setText(remaining_spots + " player remaining");
        if (joined_spots > 1)
            holder.txt_available_remaining.setText(joined_spots + " players joined");
        else
            holder.txt_available_remaining.setText(joined_spots + " player joined");

        holder.progress_bar.setMax(available_spots);
        holder.progress_bar.setProgress(joined_spots);

        holder.lyt_winners.setOnClickListener(v -> winnerClickListener.onTournamentContestWinnersClickListener(contestsDataArrayList.get(position)));

        Glide.with(context).load(contestsDataArrayList.get(position).currentContestUserData.ScreenshotURL).into(holder.uploaded_ss);

        if (contestsDataArrayList.get(position).Status.equals(Constants.REVIEW)) {
            holder.lyt_review.setVisibility(View.VISIBLE);
        }

        /*if (captainID.equalsIgnoreCase(contestsDataArrayList.get(position).currentContestUserData.UserID))
        {
            holder.cv_ss.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.cv_ss.setVisibility(View.GONE);
        }


        holder.cv_ss.setOnClickListener(v ->
        {
            if(Pref.getValue(context,Constants.UserID,"",Constants.FILENAME)
                    .equalsIgnoreCase(captainID))
            {
                if (position==0)
                {
                    if (contestsDataArrayList.get(position).Status.equals(Constants.WAITING))
                        Constants.SnakeMessageYellow(((ActivityTournamentDetail)context).getWindow().getDecorView(), "You can upload a screenshot once game is in progress or review mode.");
                    else if (contestsDataArrayList.get(position).Status.equals(Constants.STARTED))
                        Constants.SnakeMessageYellow(((ActivityTournamentDetail)context).getWindow().getDecorView(), "You can upload a screenshot once game is in progress or review mode.");
                    else if(contestsDataArrayList.get(position).Status.equals(Constants.IN_PROGRESS))
                        screenshotClickListener.OnTournamnetContestScreenShotClickListener(contestsDataArrayList.get(position));
                    else if (contestsDataArrayList.get(position).Status.equals(Constants.REVIEW))
                        screenshotClickListener.OnTournamnetContestScreenShotClickListener(contestsDataArrayList.get(position));
                    else if (contestsDataArrayList.get(position).Status.equals(Constants.COMPLETED))
                        Constants.SnakeMessageYellow(((ActivityTournamentDetail)context).getWindow().getDecorView(), "You can upload a screenshot once game is in progress or review mode.");
                }
            }
            else
                Constants.SnakeMessageYellow(((ActivityTournamentDetail)context).getWindow().getDecorView(),"You are not captain ask your Captain to upload screenshot");
        });*/

        holder.uploaded_ss.setOnClickListener(v ->
        {
            final Dialog nagDialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
            nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            nagDialog.setCancelable(false);
            nagDialog.setContentView(R.layout.preview_image);
            Button btnClose = nagDialog.findViewById(R.id.btnIvClose);
            ImageView ivPreview = nagDialog.findViewById(R.id.iv_preview_image);
            Glide.with(context).load(contestsDataArrayList.get(position).currentContestUserData.ScreenshotURL).placeholder(R.mipmap.ic_launcher).into(ivPreview);
            btnClose.setOnClickListener(arg0 -> nagDialog.dismiss());
            nagDialog.show();

        });

        if (contestsDataArrayList.get(position).Status.equalsIgnoreCase(Constants.WAITING)) {
            holder.txt_room_id.setText("-");
            holder.txt_room_pass.setText("-");
        }

    }

    @SuppressLint("SetTextI18n")
    private void setContestUserData(TournamentDetailModel.CurrentContestUserData contestsData, ViewHolder holder, String status) {
        holder.txt_name.setText(contestsData.Name);
        if (!contestsData.MobileNumber.isEmpty()) {
            holder.txt_acc_number.setText("*****" + contestsData.MobileNumber.substring(5));
        }
        if (contestsData.Kills.isEmpty()) {
            holder.txt_kill.setText("-");
        } else {
            holder.txt_kill.setText(contestsData.Kills);
        }

        if (contestsData.Rank.isEmpty()) {
            holder.txt_rank.setText("-");
        } else {
            holder.txt_rank.setText(contestsData.Rank);
        }

        if (!contestsData.WinningAmount.isEmpty()) {
            holder.lyt_congratulartions.setVisibility(View.VISIBLE);
            holder.txt_winning_amount.setText(" " + contestsData.WinningAmount);
        }

        Glide.with(context).load(contestsData.UserProfileIcon).into(holder.img_user);

        /*if (contestsData.ScreenshotFlag) {
            holder.img_view_ss.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_check));

            holder.img_view_ss.setOnClickListener(v ->
            {
                Intent i1 = new Intent(context, Full_Image_Activity.class);
                i1.putExtra("image", contestsData.ScreenshotURL);
                context.startActivity(i1);
            });
        } else {
            holder.img_view_ss.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_close));
        }*/
    }

    @Override
    public int getItemCount() {
        return contestsDataArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_date, txt_time, txt_map, txt_persp, winning_amount, txt_winners, txt_winner_title, txt_perkill, txt_entry_fees, txt_available_spots,
                txt_available_remaining, txt_join_contest, txt_room_id, txt_room_pass, rupees_symbol, txt_per_kill_title, txrupee_symbol, txt_column, txt_slot_info,
                txt_entry_rupee, txt_contest_title, txt_contest_message, txt_name, txt_acc_number, txt_kill, txt_rank, txt_winning_amount;
        ProgressBar progress_bar;
        LinearLayout pool_price_bottom, lyt_winners, lyt_entry_fees, lyt_winnings, lyt_kills, lyt_contest_detail;
        LinearLayout lyt_columns, lyt_password;
        LinearLayout lyt_room, lyt_congratulartions;
        //        CardView cv_ss;
        RelativeLayout lyt_review;
        ImageView uploaded_ss, img_show_hide, img_user;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_date = itemView.findViewById(R.id.txt_date);
            txt_time = itemView.findViewById(R.id.txt_time);
            txt_map = itemView.findViewById(R.id.txt_map);
            txt_persp = itemView.findViewById(R.id.txt_persp);
            winning_amount = itemView.findViewById(R.id.winning_amount);
            txt_winners = itemView.findViewById(R.id.txt_winners);
            txt_perkill = itemView.findViewById(R.id.txt_perkill);
            txt_entry_fees = itemView.findViewById(R.id.txt_entry_fees);
            txt_available_spots = itemView.findViewById(R.id.txt_available_spots);
            txt_available_remaining = itemView.findViewById(R.id.txt_available_remaining);
            txt_join_contest = itemView.findViewById(R.id.txt_join_contest);
            progress_bar = itemView.findViewById(R.id.progress_bar);
            txt_winner_title = itemView.findViewById(R.id.txt_winner_title);
            pool_price_bottom = itemView.findViewById(R.id.pool_price_bottom);
            txt_room_id = itemView.findViewById(R.id.txt_room_id);
            txt_room_pass = itemView.findViewById(R.id.txt_room_pass);
            lyt_winners = itemView.findViewById(R.id.lyt_winners);
            lyt_entry_fees = itemView.findViewById(R.id.lyt_entry_fees);
            lyt_winnings = itemView.findViewById(R.id.lyt_winnings);
            lyt_kills = itemView.findViewById(R.id.lyt_kills);
//            cv_ss=itemView.findViewById(R.id.cv_ss);
            rupees_symbol = itemView.findViewById(R.id.rupees_symbol);
            txt_per_kill_title = itemView.findViewById(R.id.txt_per_kill_title);
            txrupee_symbol = itemView.findViewById(R.id.txrupee_symbol);
            lyt_columns = itemView.findViewById(R.id.lyt_columns);
            lyt_room = itemView.findViewById(R.id.lyt_room);
            txt_column = itemView.findViewById(R.id.txt_column);
            txt_slot_info = itemView.findViewById(R.id.txt_slot_info);
            txt_entry_rupee = itemView.findViewById(R.id.txt_entry_rupee);
            lyt_review = itemView.findViewById(R.id.lyt_review);
            lyt_password = itemView.findViewById(R.id.lyt_password);
            uploaded_ss = itemView.findViewById(R.id.uploaded_ss);
            txt_contest_message = itemView.findViewById(R.id.txt_contest_message);
            txt_contest_title = itemView.findViewById(R.id.txt_contest_title);
            img_show_hide = itemView.findViewById(R.id.img_show_hide);
            lyt_contest_detail = itemView.findViewById(R.id.lyt_contest_detail);
            img_user = itemView.findViewById(R.id.img_user);
            txt_name = itemView.findViewById(R.id.txt_name);
            txt_acc_number = itemView.findViewById(R.id.txt_acc_number);
            txt_kill = itemView.findViewById(R.id.txt_kill);
            txt_rank = itemView.findViewById(R.id.txt_rank);
//            img_view_ss=itemView.findViewById(R.id.img_view_ss);
            lyt_congratulartions = itemView.findViewById(R.id.lyt_congratulartions);
            txt_winning_amount = itemView.findViewById(R.id.txt_winning_amount);
        }
    }
}
