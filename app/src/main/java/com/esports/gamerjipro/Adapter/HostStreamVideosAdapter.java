package com.esports.gamerjipro.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Model.HostStreamVideosModel;
import com.esports.gamerjipro.R;

import java.util.ArrayList;

public class HostStreamVideosAdapter extends RecyclerView.Adapter {


    private Activity mActivity;
    private ArrayList<HostStreamVideosModel.Data.VideosData> mArrHostStreamVideos;

    /**
     * Adapter contains the data to be displayed
     */
    public HostStreamVideosAdapter(Activity mActivity, ArrayList<HostStreamVideosModel.Data.VideosData> mArrHostStreamVideos) {
        this.mActivity = mActivity;
        this.mArrHostStreamVideos = mArrHostStreamVideos;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .row_host_stream_videos_item, parent, false);
        return new CustomViewHolder(itemView);
    }

    @SuppressLint({"SetJavaScriptEnabled", "SetTextI18n"})
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final HostStreamVideosModel.Data.VideosData hostStreamVideosModel = mArrHostStreamVideos.get(position);

        ((CustomViewHolder) holder).mWebView.setWebChromeClient(new WebChromeClient());
        ((CustomViewHolder) holder).mWebView.getSettings().setJavaScriptEnabled(true);
        ((CustomViewHolder) holder).mWebView.getSettings().setLoadsImagesAutomatically(true);
        ((CustomViewHolder) holder).mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        ((CustomViewHolder) holder).mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        ((CustomViewHolder) holder).mWebView.getSettings().setAllowFileAccess(true);
        ((CustomViewHolder) holder).mWebView.loadUrl(hostStreamVideosModel.getYoutubeLink());

        ((CustomViewHolder) holder).mTextTitle.setSelected(true);
        ((CustomViewHolder) holder).mTextChannelName.setSelected(true);
        ((CustomViewHolder) holder).mTextTitle.setText(hostStreamVideosModel.getTitle());
        ((CustomViewHolder) holder).mTextChannelName.setText(hostStreamVideosModel.getYTChannelName());

        if (hostStreamVideosModel.getViews() != null) {
            ((CustomViewHolder) holder).mTextViews.setVisibility(View.VISIBLE);
            ((CustomViewHolder) holder).mTextViews.setText(hostStreamVideosModel.getViews() + mActivity.getResources().getString(R.string.text_views));
        } else {
            ((CustomViewHolder) holder).mTextViews.setVisibility(View.GONE);
        }

        // ToDo: Subscribe Button Click
        ((CustomViewHolder) holder).mButtonSubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callToYoutubeChannelSubscribe(hostStreamVideosModel.getYTChannelLink());
            }
        });
    }

    /**
     * This should redirects you to the Youtube Channel Subscribe Page
     */
    private void callToYoutubeChannelSubscribe(String mChannelURL) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mChannelURL));
            intent.setPackage("com.google.android.youtube");
            mActivity.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mArrHostStreamVideos.size();
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public int getItemViewType(int position)
    {
        return position;
    }

    /**
     * Id declarations
     */
    public static class CustomViewHolder extends RecyclerView.ViewHolder {

        WebView mWebView;
        TextView mTextTitle, mTextChannelName, mTextViews;
        Button mButtonSubscribe;

        @SuppressLint("SetJavaScriptEnabled")
        public CustomViewHolder(final View itemView) {
            super(itemView);

            // Web View
            this.mWebView = itemView.findViewById(R.id.web_view_row_hot_stream_video);

            // Text Views
            this.mTextTitle = itemView.findViewById(R.id.text_row_hot_stream_video_title);
            this.mTextChannelName = itemView.findViewById(R.id.text_row_hot_stream_video_channel_name);
            this.mTextViews = itemView.findViewById(R.id.text_row_hot_stream_video_views);

            this.mButtonSubscribe = itemView.findViewById(R.id.button_row_host_stream_video_channel_subscribe);
        }
    }
}
