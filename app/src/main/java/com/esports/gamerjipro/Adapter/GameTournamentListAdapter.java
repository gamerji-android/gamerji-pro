package com.esports.gamerjipro.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esports.gamerjipro.Interface.OnTournamentShowTimeListener;
import com.esports.gamerjipro.Interface.RulesClickListener;
import com.esports.gamerjipro.Interface.WinnerClickListener;
import com.esports.gamerjipro.Model.TournamentTypeModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.Utils.CustomLinearLayoutManager;
import com.esports.gamerjipro.activity.ActivityGameTournament;

import java.util.ArrayList;

public class GameTournamentListAdapter extends  RecyclerView.Adapter<GameTournamentListAdapter.ViewHolder>
{
    private Activity context;
    private ArrayList<TournamentTypeModel.Data.TypesData> gameContestListModels;
    private WinnerClickListener winnerClickListener;
    private OnTournamentShowTimeListener onTournamentTimeSelectedListener ;
    private RulesClickListener rulesClickListener ;

    public GameTournamentListAdapter(Activity context, ArrayList<TournamentTypeModel.Data.TypesData> gameContestListModels, WinnerClickListener winnerClickListener,
                                     OnTournamentShowTimeListener onTournamentTimeSelectedListener,RulesClickListener rulesClickListener)
    {
        this.context=context;
        this.gameContestListModels=gameContestListModels;
        this.winnerClickListener=winnerClickListener;
        this.onTournamentTimeSelectedListener=onTournamentTimeSelectedListener;
        this.rulesClickListener=rulesClickListener;

    }

    @NonNull
    @Override
    public GameTournamentListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_game_contest, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull GameTournamentListAdapter.ViewHolder holder, int position)
    {

        if (Integer.parseInt(gameContestListModels.get(position).TournamentsCount)>0)
        {
            ((ActivityGameTournament)context).isEmpty=false;
            holder.txt_contest_title.setText(gameContestListModels.get(position).Name);
            Glide.with(context).load(gameContestListModels.get(position).FeaturedImage).into(holder.img_contest_image);
            CustomLinearLayoutManager customLayoutManager = new CustomLinearLayoutManager(context, LinearLayoutManager.VERTICAL,false);
            GameTournamentDetailAdapter gameContestDetailAdapter = new GameTournamentDetailAdapter(context, gameContestListModels.get(position).tournamentsDataArrayList,winnerClickListener,onTournamentTimeSelectedListener,rulesClickListener);
            holder.rv_contest.setLayoutManager(customLayoutManager);
            holder.rv_contest.setItemAnimator(new DefaultItemAnimator());
            holder.rv_contest.setAdapter(gameContestDetailAdapter);

        }
        else
        {
            holder.lyt_trophy.setVisibility(View.GONE);
            holder.rv_contest.setVisibility(View.GONE);
            // gameContestListModels.remove(position);
            if (((ActivityGameTournament)context).isEmpty)
                ((ActivityGameTournament)context).isEmpty=true;
        }

        if (gameContestListModels.get(position).Name.equalsIgnoreCase(""))
        {
            holder.lyt_trophy.setVisibility(View.GONE);
        }
        else
        {
            holder.lyt_trophy.setVisibility(View.VISIBLE);
        }

        if (position==gameContestListModels.size()-1)
        {
            if ((((ActivityGameTournament) context).isEmpty))
            {
                ((ActivityGameTournament) context).lyt_empty.setVisibility(View.VISIBLE);
                ((ActivityGameTournament) context).swipe_refresh_home.setVisibility(View.GONE);
            }
            else
            {
                ((ActivityGameTournament) context).lyt_empty.setVisibility(View.GONE);
                ((ActivityGameTournament) context).swipe_refresh_home.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public int getItemCount()
    {
        return gameContestListModels.size();
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public int getItemViewType(int position)
    {
        return position;
    }
    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView txt_contest_title;
        RecyclerView rv_contest;
        ImageView img_contest_image;
        LinearLayout contest_list_layout,lyt_trophy;
        ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            txt_contest_title=itemView.findViewById(R.id.txt_contest_title);
            rv_contest=itemView.findViewById(R.id.rv_contest);
            contest_list_layout=itemView.findViewById(R.id.contest_list_layout);
            img_contest_image=itemView.findViewById(R.id.img_contest_image);
            lyt_trophy=itemView.findViewById(R.id.lyt_trophy);

        }
    }
}
