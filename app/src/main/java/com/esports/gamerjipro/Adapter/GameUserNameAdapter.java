package com.esports.gamerjipro.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Fragment.ProfileFragment;
import com.esports.gamerjipro.Model.GameUsernameModel;
import com.esports.gamerjipro.R;

import java.util.ArrayList;

public class GameUserNameAdapter extends RecyclerView.Adapter<GameUserNameAdapter.ViewHolder> {
    Context context;
    private ArrayList<GameUsernameModel> gamesDataArrayList;

    //    public boolean enable=false;
    public GameUserNameAdapter(Context context, ArrayList<GameUsernameModel> gamesDataArrayList) {
        this.context = context;
        this.gamesDataArrayList = gamesDataArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_game_username, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
//        if (enable)
//        {
//            if(gamesDataArrayList.get(position).isEnable())
//            if(gamesDataArrayList.get(position).getUniqueName().equalsIgnoreCase(""))
//            {
//                holder.et_game_username.setEnabled(true);
//            }
//            else
//            {
//                holder.et_game_username.setEnabled(false);
//            }
//        }

        if (ProfileFragment.mTextEdit.getText().toString().equalsIgnoreCase("SAVE")) {
            if (gamesDataArrayList.get(position).getUniqueName().equalsIgnoreCase("")) {
                holder.et_game_username.setEnabled(true);
            } else {
                holder.et_game_username.setEnabled(false);
            }
        }

        holder.et_game_username.setText(gamesDataArrayList.get(position).getUniqueName());
        if (gamesDataArrayList.get(position).getUniqueName().equalsIgnoreCase("")) {
            holder.et_game_username.setHint("Enter Your " + gamesDataArrayList.get(position).getGameName() + " Username");
        }

        holder.txt_game_username.setText(gamesDataArrayList.get(position).getGameName() + " Username");
        holder.info_game_username.setOnClickListener(v ->
        {
            ProfileFragment.profileBadgeClickListener.OnOtherGameInfoClickListener(gamesDataArrayList.get(position));
        });
    }

    @Override
    public int getItemCount() {
        return gamesDataArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txt_game_username;
        public EditText et_game_username;
        ImageView info_game_username;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            et_game_username = itemView.findViewById(R.id.et_game_username);
            txt_game_username = itemView.findViewById(R.id.txt_game_username);
            info_game_username = itemView.findViewById(R.id.info_game_username);
        }
    }

    public void enalbleEdittext() {
        notifyDataSetChanged();
    }
}
