package com.esports.gamerjipro.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esports.gamerjipro.Model.GameTypesModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.activity.ActivityGameContestNew;
import com.esports.gamerjipro.activity.ActivityGameTournament;
import com.esports.gamerjipro.activity.ActivityGameType;

import java.util.ArrayList;

public class GameTypeListAdapter extends RecyclerView.Adapter<GameTypeListAdapter.ViewHolder> {
    private ArrayList<GameTypesModel.Data.TypesData> games_list_models;
    private Context context;
    String game_name;

    public GameTypeListAdapter(Context context, ArrayList<GameTypesModel.Data.TypesData> games_list_models, String game_name) {
        this.context = context;
        this.game_name = game_name;
        this.games_list_models = games_list_models;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_game_type_grid, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Glide.with(context).load(games_list_models.get(position).getFeaturedImage()).into(holder.img_game);
        holder.txt_game_name.setText(games_list_models.get(position).getName());
        holder.img_game.setOnClickListener(v ->
        {
            if (games_list_models.get(position).getIsTournament().equalsIgnoreCase("1")) {
                Intent i = new Intent(context, ActivityGameContestNew.class);
                i.putExtra("game_name", games_list_models.get(position).getName());
                i.putExtra("game_id", ((ActivityGameType) context).mLoginGameId);
                i.putExtra("game_type_id", games_list_models.get(position).getTypeID());
                Constants.Main_game_name = ((ActivityGameType) context).mTextHeaderGameName.getText().toString();
                i.putExtra("main_game_name", ((ActivityGameType) context).mTextHeaderGameName.getText().toString());
                context.startActivity(i);
            } else {
                Intent i = new Intent(context, ActivityGameTournament.class);
                i.putExtra("game_name", games_list_models.get(position).getName());
                i.putExtra("game_id", ((ActivityGameType) context).mLoginGameId);
                i.putExtra("game_type_id", games_list_models.get(position).getTypeID());
                Constants.Main_game_name = ((ActivityGameType) context).mTextHeaderGameName.getText().toString();
                i.putExtra("main_game_name", ((ActivityGameType) context).mTextHeaderGameName.getText().toString());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return games_list_models.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_game;
        TextView txt_game_name;
        View view_shadow;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_game = itemView.findViewById(R.id.img_game);
            txt_game_name = itemView.findViewById(R.id.txt_game_name);
            view_shadow = itemView.findViewById(R.id.view_shadow);

            final float scale = context.getResources().getDisplayMetrics().density;
            int pixels = (int) (130 * scale + 0.5f);
            view_shadow.getLayoutParams().height = pixels;
            img_game.getLayoutParams().height = pixels;
        }
    }
}
