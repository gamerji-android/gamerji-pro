package com.esports.gamerjipro.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cruxlab.sectionedrecyclerview.lib.BaseSectionAdapter;
import com.cruxlab.sectionedrecyclerview.lib.SectionAdapter;
import com.esports.gamerjipro.Interface.ContestJoinClickListener;
import com.esports.gamerjipro.Interface.WinnerClickListener;
import com.esports.gamerjipro.Model.ContestTypeModelNew;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.Utils.FileUtils;
import com.esports.gamerjipro.activity.ActivityGameContestNew;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class GameContestSectionAdapter extends SectionAdapter<GameContestSectionAdapter.MyItemViewHolder, GameContestSectionAdapter.MyHeaderViewHolder>
{

    private WinnerClickListener winnerClickListener ;
    private ContestJoinClickListener contestJoinClickListener;
    private ArrayList<ContestTypeModelNew.Data.ContestsData> contestsDataArrayList;
    private Context context;

    public GameContestSectionAdapter(ArrayList<ContestTypeModelNew.Data.ContestsData> contestsDataArrayList, Context context ,WinnerClickListener winnerClickListener, ContestJoinClickListener contestJoinClickListener, boolean isHeaderVisible, boolean isHeaderPinned)
    {
        super(isHeaderVisible, isHeaderPinned);
        this.contestJoinClickListener=contestJoinClickListener;
        this.winnerClickListener=winnerClickListener;
        this.contestsDataArrayList=contestsDataArrayList;
        this.context=context;

    }


    @Override
    public int getItemCount()
    {
        return contestsDataArrayList.size();
    }

    @Override
    public MyItemViewHolder onCreateItemViewHolder(ViewGroup parent, short type)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout_contest_detail, parent, false);
        return new MyItemViewHolder(view);
    }

    @Override
    public void onBindItemViewHolder(MyItemViewHolder holder, int position)
    {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM-yyyy");
        @SuppressLint("SimpleDateFormat") DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        @SuppressLint("SimpleDateFormat") DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");


        String inputDateStr=contestsDataArrayList.get(position).Date;
        Date date = null;
        try
        {
            date = inputFormat.parse(inputDateStr);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }

        String outputDateStr = outputFormat.format(date);

        holder.txt_date.setText(outputDateStr);

        ((ActivityGameContestNew)context).game_id=contestsDataArrayList.get(position).GameID;

        holder.txt_time.setText(FileUtils.getFormatedDateTime(contestsDataArrayList.get(position).Time,"HH:mm:ss", "hh:mm a"));
        holder.txt_map.setText(contestsDataArrayList.get(position).Map_Length);
        holder.txt_persp.setText(contestsDataArrayList.get(position).Perspective_LevelCap);

        if (contestsDataArrayList.get(position).WinningAmount.equalsIgnoreCase("") || contestsDataArrayList.get(position).WinningAmount.equalsIgnoreCase("0"))
        {
            holder.winning_amount.setText("-");
            holder.rupees_symbol.setVisibility(View.GONE);
        }
        else
            holder.winning_amount.setText(" "+contestsDataArrayList.get(position).WinningAmount);

        holder.txt_winners.setText(contestsDataArrayList.get(position).WinnersCount);
        holder.txt_perkill_title.setText(contestsDataArrayList.get(position).PerKill_MaxLosesTitle);
        holder.txt_perkill.setText(" "+contestsDataArrayList.get(position).PerKill_MaxLoses);

        if (contestsDataArrayList.get(position).PerKill_MaxLosesCurrency)
        {
            holder.txt_rupee_symbol.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.txt_rupee_symbol.setVisibility(View.GONE);
        }
        if (contestsDataArrayList.get(position).WinnersCount.isEmpty() && contestsDataArrayList.get(position).PerKill_MaxLoses.isEmpty())
        {
            holder.lyt_winners.setVisibility(View.GONE);
            holder.lyt_kills.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT,3.0f);
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT,1.0f);
            holder.lyt_winnings.setLayoutParams(params);
            holder.ly_entry_fees.setLayoutParams(params1);

        }
        else if (contestsDataArrayList.get(position).WinnersCount.isEmpty())
        {
            holder.lyt_winners.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT,2.0f);
            holder.lyt_kills.setLayoutParams(params);
        }
        else if (contestsDataArrayList.get(position).PerKill_MaxLoses.isEmpty())
        {
            holder.lyt_kills.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT,2.0f);
            holder.lyt_winners.setLayoutParams(params);
        }
        else
        {
            if (Integer.parseInt(contestsDataArrayList.get(position).WinnersCount)==1)
                holder.txt_winner_title.setText("Winner");

            if (Integer.parseInt(contestsDataArrayList.get(position).PerKill_MaxLoses)==1)
                holder.txt_perkill.setText(" "+contestsDataArrayList.get(position).PerKill_MaxLoses);
        }

        if (contestsDataArrayList.get(position).EntryFee.equalsIgnoreCase("0"))
        {
            holder.txt_entry_fees.setText("Free");
            holder.txt_entry_rupee.setVisibility(View.GONE);
        }
        else
            holder.txt_entry_fees.setText(" "+contestsDataArrayList.get(position).EntryFee);

        holder.txt_map_title.setText(contestsDataArrayList.get(position).Map_LengthTitle);
        holder.txt_perspective_title.setText(contestsDataArrayList.get(position).Perspective_LevelCapTitle);
        int available_spots= Integer.parseInt(contestsDataArrayList.get(position).TotalSpots);
        int joined_spots=Integer.parseInt(contestsDataArrayList.get(position).JoinedSpots);
        int remaining_spots=available_spots-joined_spots;

        if (remaining_spots>1)
            holder.txt_available_spots.setText(remaining_spots +" players remaining");
        else
            holder.txt_available_spots.setText(remaining_spots +" player remaining");

        if (joined_spots>1)
            holder.txt_available_remaining.setText(joined_spots +" players joined");
        else
            holder.txt_available_remaining.setText(joined_spots +" player joined");

        holder.progress_bar.setMax(available_spots);
        holder.progress_bar.setProgress(joined_spots);
        holder.progress_bar.setProgress(joined_spots);

        if (contestsDataArrayList.get(position).ConfirmStatus.equalsIgnoreCase("1"))
            holder.txt_confirm.setVisibility(View.VISIBLE);
        else
            holder.txt_confirm.setVisibility(View.GONE);

        if (contestsDataArrayList.get(position).Joined)
        {
            holder.txt_join_contest.setText("Joined");
            holder.txt_join_contest.setBackgroundColor(context.getResources().getColor(R.color.orange_color));
            holder.txt_join_contest.setTextColor(Color.WHITE);
        }

        holder.lyt_winners_drop.setOnClickListener(v -> winnerClickListener.onWinnersClick(contestsDataArrayList.get(position)));
        holder.txt_join_contest.setOnClickListener(v -> contestJoinClickListener.onContestJoinClickLitener(contestsDataArrayList.get(position)));
    }

    @Override
    public MyHeaderViewHolder onCreateHeaderViewHolder(ViewGroup parent)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.section_header_games_contest, parent, false);
        return new MyHeaderViewHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(MyHeaderViewHolder holder)
    {
        holder.textView.setText(contestsDataArrayList.get(0).TypeName);
    }

    static class MyItemViewHolder extends BaseSectionAdapter.ItemViewHolder
    {
        TextView txt_date,txt_time,txt_map,txt_persp,winning_amount,txt_winners,txt_perkill,txt_entry_fees,txt_available_spots,txt_entry_rupee,
                txt_available_remaining,txt_join_contest,txt_perspective_title,txt_map_title,txt_confirm,txt_winner_title,txt_perkill_title,txt_rupee_symbol,rupees_symbol;
        LinearLayout lyt_winners_drop,lyt_kills,lyt_winners,ly_entry_fees,lyt_winnings;
        ProgressBar progress_bar;
        LinearLayout lyt_winning_amount;

        MyItemViewHolder(View itemView)
        {
            super(itemView);
            txt_date=itemView.findViewById(R.id.txt_date_t);
            txt_time=itemView.findViewById(R.id.txt_time_t);
            txt_map=itemView.findViewById(R.id.txt_map_t);
            txt_persp=itemView.findViewById(R.id.txt_persp_t);
            winning_amount=itemView.findViewById(R.id.winning_amount_t);
            txt_winners=itemView.findViewById(R.id.txt_winners_t);
            txt_perkill=itemView.findViewById(R.id.txt_perkill_t);
            txt_entry_fees=itemView.findViewById(R.id.txt_entry_fees_t);
            txt_available_spots=itemView.findViewById(R.id.txt_available_spots_t);
            txt_available_remaining=itemView.findViewById(R.id.txt_available_remaining_t);
            txt_join_contest=itemView.findViewById(R.id.txt_join_contest_t);
            progress_bar=itemView.findViewById(R.id.progress_bar_t);
            txt_perspective_title=itemView.findViewById(R.id.txt_perspective_title_t);
            txt_map_title=itemView.findViewById(R.id.txt_map_title_t);
            txt_confirm=itemView.findViewById(R.id.txt_confirm_t);
            lyt_winners_drop=itemView.findViewById(R.id.lyt_winners_drop_t);
            txt_winner_title=itemView.findViewById(R.id.txt_winner_title_t);
            lyt_winners=itemView.findViewById(R.id.lyt_winners_t);
            lyt_kills=itemView.findViewById(R.id.lyt_kills_t);
            ly_entry_fees=itemView.findViewById(R.id.ly_entry_fees_t);
            lyt_winnings=itemView.findViewById(R.id.lyt_winnings_t);
            txt_perkill_title=itemView.findViewById(R.id.txt_perkill_title_t);
            txt_rupee_symbol=itemView.findViewById(R.id.txt_rupee_symbol_t);
            rupees_symbol=itemView.findViewById(R.id.rupees_symbol_t);
            lyt_winning_amount=itemView.findViewById(R.id.lyt_winning_amount_t);
            txt_entry_rupee=itemView.findViewById(R.id.txt_entry_rupee_t);
        }
    }

    static class MyHeaderViewHolder extends BaseSectionAdapter.HeaderViewHolder
    {

        TextView textView;

        MyHeaderViewHolder(View itemView)
        {
            super(itemView);
            this.textView = itemView.findViewById(R.id.txt_contest_title);
        }
    }
}
