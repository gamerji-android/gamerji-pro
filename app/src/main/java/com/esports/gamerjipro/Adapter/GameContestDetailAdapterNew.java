package com.esports.gamerjipro.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Interface.ContestJoinClickListener;
import com.esports.gamerjipro.Interface.WinnerClickListener;
import com.esports.gamerjipro.Model.ContestTypeModelNew;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.Utils.FileUtils;
import com.esports.gamerjipro.activity.ActivityGameContestNew;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class GameContestDetailAdapterNew extends RecyclerView.Adapter<GameContestDetailAdapterNew.ViewHolder>
{
private Context context;
private ArrayList<ContestTypeModelNew.Data.ContestsData> gameContestDetailsModels;
private WinnerClickListener winnerClickListener ;
private ContestJoinClickListener contestJoinClickListener;

    GameContestDetailAdapterNew(Context context, ArrayList<ContestTypeModelNew.Data.ContestsData> gameContestDetailsModels, WinnerClickListener winnerClickListener, ContestJoinClickListener contestJoinClickListener)
        {
        this.context=context;
        this.gameContestDetailsModels=gameContestDetailsModels;
        this.winnerClickListener=winnerClickListener;
        this.contestJoinClickListener=contestJoinClickListener;
        setHasStableIds(true);
        }

@NonNull
@Override
public GameContestDetailAdapterNew.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
        View itemView = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.item_layout_contest_detail, parent, false);
        if (gameContestDetailsModels.isEmpty())
        {
        itemView.setVisibility(View.GONE);
        }
        return new ViewHolder(itemView);
        }

@SuppressLint({"SetTextI18n", "ResourceAsColor"})
public void onBindViewHolder(@NonNull GameContestDetailAdapterNew.ViewHolder holder, int position)
        {

@SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM-yyyy");
@SuppressLint("SimpleDateFormat") DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
@SuppressLint("SimpleDateFormat") DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
        String inputDateStr=gameContestDetailsModels.get(position).Date;
        Date date = null;
        try
        {
        date = inputFormat.parse(inputDateStr);
        }
        catch (ParseException e)
        {
        e.printStackTrace();
        }

        String outputDateStr = outputFormat.format(date);

        holder.txt_date.setText(outputDateStr);

        ((ActivityGameContestNew)context).game_id=gameContestDetailsModels.get(position).GameID;

        holder.txt_time.setText(FileUtils.getFormatedDateTime(gameContestDetailsModels.get(position).Time,"HH:mm:ss", "hh:mm a"));
        holder.txt_map.setText(gameContestDetailsModels.get(position).Map_Length);
        holder.txt_persp.setText(gameContestDetailsModels.get(position).Perspective_LevelCap);

        if (gameContestDetailsModels.get(position).WinningAmount.equalsIgnoreCase("") || gameContestDetailsModels.get(position).WinningAmount.equalsIgnoreCase("0"))
        {
        holder.winning_amount.setText("-");
        holder.rupees_symbol.setVisibility(View.GONE);
        }
        else
        holder.winning_amount.setText(" "+gameContestDetailsModels.get(position).WinningAmount);

        holder.txt_winners.setText(gameContestDetailsModels.get(position).WinnersCount);
        holder.txt_perkill_title.setText(gameContestDetailsModels.get(position).PerKill_MaxLosesTitle);
        holder.txt_perkill.setText(" "+gameContestDetailsModels.get(position).PerKill_MaxLoses);

        if (gameContestDetailsModels.get(position).PerKill_MaxLosesCurrency)
        {
        holder.txt_rupee_symbol.setVisibility(View.VISIBLE);
        }
        else
        {
        holder.txt_rupee_symbol.setVisibility(View.GONE);
        }
        if (gameContestDetailsModels.get(position).WinnersCount.isEmpty() && gameContestDetailsModels.get(position).PerKill_MaxLoses.isEmpty())
        {
        holder.lyt_winners.setVisibility(View.GONE);
        holder.lyt_kills.setVisibility(View.GONE);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT,3.0f);
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT,1.0f);
        holder.lyt_winnings.setLayoutParams(params);
        holder.ly_entry_fees.setLayoutParams(params1);

        }
        else if (gameContestDetailsModels.get(position).WinnersCount.isEmpty())
        {
        holder.lyt_winners.setVisibility(View.GONE);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT,2.0f);
        holder.lyt_kills.setLayoutParams(params);
        }
        else if (gameContestDetailsModels.get(position).PerKill_MaxLoses.isEmpty())
        {
        holder.lyt_kills.setVisibility(View.GONE);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT,2.0f);
        holder.lyt_winners.setLayoutParams(params);
        }
        else
        {
        if (Integer.parseInt(gameContestDetailsModels.get(position).WinnersCount)==1)
        holder.txt_winner_title.setText("Winner");

        if (Integer.parseInt(gameContestDetailsModels.get(position).PerKill_MaxLoses)==1)
        holder.txt_perkill.setText(" "+gameContestDetailsModels.get(position).PerKill_MaxLoses);
        }

        if (gameContestDetailsModels.get(position).EntryFee.equalsIgnoreCase("0"))
        {
        holder.txt_entry_fees.setText("Free");
        holder.txt_entry_rupee.setVisibility(View.GONE);
        }
        else
        holder.txt_entry_fees.setText(" "+gameContestDetailsModels.get(position).EntryFee);

        holder.txt_map_title.setText(gameContestDetailsModels.get(position).Map_LengthTitle);
        holder.txt_perspective_title.setText(gameContestDetailsModels.get(position).Perspective_LevelCapTitle);
        int available_spots= Integer.parseInt(gameContestDetailsModels.get(position).TotalSpots);
        int joined_spots=Integer.parseInt(gameContestDetailsModels.get(position).JoinedSpots);
        int remaining_spots=available_spots-joined_spots;

        if (remaining_spots>1)
        holder.txt_available_spots.setText(remaining_spots +" players remaining");
        else
        holder.txt_available_spots.setText(remaining_spots +" player remaining");

        if (joined_spots>1)
        holder.txt_available_remaining.setText(joined_spots +" players joined");
        else
        holder.txt_available_remaining.setText(joined_spots +" player joined");

        holder.progress_bar.setMax(available_spots);
        holder.progress_bar.setProgress(joined_spots);
        holder.progress_bar.setProgress(joined_spots);

        if (gameContestDetailsModels.get(position).ConfirmStatus.equalsIgnoreCase("1"))
        holder.txt_confirm.setVisibility(View.VISIBLE);
        else
        holder.txt_confirm.setVisibility(View.GONE);

        if (gameContestDetailsModels.get(position).Joined)
        {
        holder.txt_join_contest.setText("Joined");
        holder.txt_join_contest.setBackgroundColor(context.getResources().getColor(R.color.orange_color));
        holder.txt_join_contest.setTextColor(Color.WHITE);
        }

        holder.lyt_winners_drop.setOnClickListener(v -> winnerClickListener.onWinnersClick(gameContestDetailsModels.get(position)));
        holder.txt_join_contest.setOnClickListener(v -> contestJoinClickListener.onContestJoinClickLitener(gameContestDetailsModels.get(position)));
        }

@Override
public int getItemCount()
        {
        return gameContestDetailsModels.size();
        }

class ViewHolder extends RecyclerView.ViewHolder
{
    TextView txt_date,txt_time,txt_map,txt_persp,winning_amount,txt_winners,txt_perkill,txt_entry_fees,txt_available_spots,txt_entry_rupee,
            txt_available_remaining,txt_join_contest,txt_perspective_title,txt_map_title,txt_confirm,txt_winner_title,txt_perkill_title,txt_rupee_symbol,rupees_symbol;
    LinearLayout lyt_winners_drop,lyt_kills,lyt_winners,ly_entry_fees,lyt_winnings;
    ProgressBar progress_bar;
    LinearLayout lyt_winning_amount;
    ViewHolder(@NonNull View itemView)
    {
        super(itemView);
        txt_date=itemView.findViewById(R.id.txt_date_t);
        txt_time=itemView.findViewById(R.id.txt_time_t);
        txt_map=itemView.findViewById(R.id.txt_map_t);
        txt_persp=itemView.findViewById(R.id.txt_persp_t);
        winning_amount=itemView.findViewById(R.id.winning_amount_t);
        txt_winners=itemView.findViewById(R.id.txt_winners_t);
        txt_perkill=itemView.findViewById(R.id.txt_perkill_t);
        txt_entry_fees=itemView.findViewById(R.id.txt_entry_fees_t);
        txt_available_spots=itemView.findViewById(R.id.txt_available_spots_t);
        txt_available_remaining=itemView.findViewById(R.id.txt_available_remaining_t);
        txt_join_contest=itemView.findViewById(R.id.txt_join_contest_t);
        progress_bar=itemView.findViewById(R.id.progress_bar_t);
        txt_perspective_title=itemView.findViewById(R.id.txt_perspective_title_t);
        txt_map_title=itemView.findViewById(R.id.txt_map_title_t);
        txt_confirm=itemView.findViewById(R.id.txt_confirm_t);
        lyt_winners_drop=itemView.findViewById(R.id.lyt_winners_drop_t);
        txt_winner_title=itemView.findViewById(R.id.txt_winner_title_t);
        lyt_winners=itemView.findViewById(R.id.lyt_winners_t);
        lyt_kills=itemView.findViewById(R.id.lyt_kills_t);
        ly_entry_fees=itemView.findViewById(R.id.ly_entry_fees_t);
        lyt_winnings=itemView.findViewById(R.id.lyt_winnings_t);
        txt_perkill_title=itemView.findViewById(R.id.txt_perkill_title_t);
        txt_rupee_symbol=itemView.findViewById(R.id.txt_rupee_symbol_t);
        rupees_symbol=itemView.findViewById(R.id.rupees_symbol_t);
        lyt_winning_amount=itemView.findViewById(R.id.lyt_winning_amount_t);
        txt_entry_rupee=itemView.findViewById(R.id.txt_entry_rupee_t);

    }
}

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }
}
