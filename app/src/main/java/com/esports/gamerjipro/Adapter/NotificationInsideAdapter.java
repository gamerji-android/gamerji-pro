package com.esports.gamerjipro.Adapter;

/*
public class NotificationInsideAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    Context context;
    ArrayList<NotificationModelLocalModelInside> arrayList;

    SetOnNotificationClickListner setOnNotificationClickListner;

    public NotificationInsideAdapter(Context context, ArrayList<NotificationModelLocalModelInside> arrayList, SetOnNotificationClickListner setOnNotificationClickListner) {
        this.context = context;
        this.arrayList = arrayList;
        this.setOnNotificationClickListner = setOnNotificationClickListner;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notification_sub_layout, viewGroup, false);
        return new ViewHolderNotificationInside(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ViewHolderNotificationInside notificationInside = (ViewHolderNotificationInside) viewHolder;
        notificationInside.mTxtMessage.setText(arrayList.get(i).getMessage());
        String mDate = GeneralUtils.GetDateForNotification((arrayList.get(i).getCreated_timestamp() * 1000));
        mDate = mDate.replace("am", "AM").replace("pm", "PM");
        notificationInside.mTxtDate.setText(mDate);
        try {
            int mType = arrayList.get(i).getNotification_type();
            Drawable drawable = LoadImageFromAssets(context, "notification_" + mType);
            notificationInside.imgNotificationIcon.setImageDrawable(drawable);
        } catch (IOException e) {
            e.printStackTrace();
            notificationInside.imgNotificationIcon.setImageResource(R.mipmap.ic_launcher_round);
        }
        if (i == arrayList.size() - 1) {
            notificationInside.mLineView.setVisibility(View.GONE);
        } else {
            notificationInside.mLineView.setVisibility(View.VISIBLE);
        }
        notificationInside.mRootViewNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setOnNotificationClickListner.OnClick(arrayList.get(i).getNotification_type(), arrayList.get(i).getLeague_id(), arrayList.get(i).getNotification_id());
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    private class ViewHolderNotificationInside extends RecyclerView.ViewHolder {

        ImageView imgNotificationIcon;
        TextView mTxtMessage, mTxtDate;

        LinearLayout mRootViewNotification;
        View mLineView;

        public ViewHolderNotificationInside(@NonNull View itemView) {
            super(itemView);
            imgNotificationIcon = itemView.findViewById(R.id.mImgNotificationIcon);
            mTxtDate = itemView.findViewById(R.id.mTxtDateTimeInside);
            mTxtMessage = itemView.findViewById(R.id.mTxtMessage);
            mLineView = itemView.findViewById(R.id.mLineView);
            mRootViewNotification = itemView.findViewById(R.id.mRootViewNotification);
        }
    }

    Drawable LoadImageFromAssets(Context context, String mImageName) throws IOException {

        // get input stream
        InputStream ims = context.getAssets().open("notification_icons/" + mImageName + ".png");
        // load image as Drawable
        Drawable d = Drawable.createFromStream(ims, null);
        // set image to ImageView
        ims.close();
        return d;
    }

}
*/
