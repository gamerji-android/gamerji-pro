package com.esports.gamerjipro.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Model.GamerProfileModel;
import com.esports.gamerjipro.R;

import java.util.ArrayList;

public class GameProfileAdapter extends RecyclerView.Adapter<GameProfileAdapter.ViewHolder>
{
    Context context;
    private ArrayList<GamerProfileModel.Data.GamesData.GameData> gamesDataArrayList;

    public GameProfileAdapter(Context context, ArrayList<GamerProfileModel.Data.GamesData.GameData> gamesDataArrayList)
    {
        this.context=context;
        this.gamesDataArrayList=gamesDataArrayList;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_game_profiel, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        holder.txt_pubg_game.setText(gamesDataArrayList.get(position).GameName);
        holder.txt_pubg_played.setText(gamesDataArrayList.get(position).GamePlayed);
        holder.txt_pubg_won.setText(gamesDataArrayList.get(position).GameWon);
        holder.txt_pubg_points.setText(gamesDataArrayList.get(position).GamePoints);
        if (position>0 && position%2==0)
        {
            holder.rv_game_profile_bg.setBackgroundColor(context.getResources().getColor(R.color.colorAccent));
        }
        else
        {
            holder.rv_game_profile_bg.setBackgroundColor(context.getResources().getColor(R.color.orange_color));
        }
    }

    @Override
    public int getItemCount()
    {
        return gamesDataArrayList.size();
    }

    public class  ViewHolder extends RecyclerView.ViewHolder
    {
        TextView txt_pubg_game,txt_pubg_played,txt_pubg_won,txt_pubg_points;
        RelativeLayout rv_game_profile_bg;
        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            txt_pubg_game=itemView.findViewById(R.id.txt_pubg_game);
            txt_pubg_played=itemView.findViewById(R.id.txt_pubg_played);
            txt_pubg_won=itemView.findViewById(R.id.txt_pubg_won);
            txt_pubg_points=itemView.findViewById(R.id.txt_pubg_points);
            rv_game_profile_bg=itemView.findViewById(R.id.rv_game_profile_bg);

        }
    }

}
