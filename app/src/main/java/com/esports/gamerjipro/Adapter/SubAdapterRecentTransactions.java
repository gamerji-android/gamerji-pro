package com.esports.gamerjipro.Adapter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Model.GeneralResponseModel;
import com.esports.gamerjipro.Model.RecentTransactionsModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.Pref;
import com.esports.gamerjipro.activity.ActivityRecentTransactions;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubAdapterRecentTransactions extends RecyclerView.Adapter<SubAdapterRecentTransactions.ViewHolder>
{
    Context context ;
    private ArrayList<RecentTransactionsModel.TransactionDetail> recentTransactionsModel;
    private APIInterface apiInterface;

    SubAdapterRecentTransactions(Context context, ArrayList<RecentTransactionsModel.TransactionDetail> recentTransactionsModel)
    {
        this.context=context;
        this.recentTransactionsModel=recentTransactionsModel;
        apiInterface = APIClient.getClient().create(APIInterface.class);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.subitem_recent_transaction, parent, false);
        return new ViewHolder(itemView);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        holder.txt_amount.setText(" "+recentTransactionsModel.get(position).getAmount());
        holder.txt_league_status.setText(recentTransactionsModel.get(position).getTitle());
        holder.txt_sign.setText(recentTransactionsModel.get(position).getAmountType()+" ");
        holder.lyt_download.setOnClickListener(v -> {});
        holder.lyt_email_invoice.setOnClickListener(v -> {});
        if (recentTransactionsModel.get(position).getGTypeName().isEmpty())
            holder.txt_game_type.setText("-");
        else
        holder.txt_game_type.setText(recentTransactionsModel.get(position).getGTypeName());

        if (recentTransactionsModel.get(position).getGameName().isEmpty())
            holder.txt_game_name.setText("-");
        else
        holder.txt_game_name.setText(recentTransactionsModel.get(position).getGameName());

        holder.txt_transaction_date.setText(recentTransactionsModel.get(position).getDateTime());
        holder.txt_transcation_id.setText(recentTransactionsModel.get(position).getUniqueID());
        if(recentTransactionsModel.get(position).getInvoiceFile().isEmpty())
        holder.lyt_invoice.setVisibility(View.GONE);
        else
            holder.lyt_invoice.setVisibility(View.VISIBLE);

        holder.lyt_download.setOnClickListener(v ->
                TedPermission.with(context)
                        .setPermissionListener(new PermissionListener()
                        {
                            @Override
                            public void onPermissionGranted()
                            {
                                if (!recentTransactionsModel.get(position).getInvoiceFile().isEmpty())
                                {
                                    Constants.DownloadReceipt(context, Uri.parse(recentTransactionsModel.get(position).getInvoiceFile()));
                                    Constants.SnakeMessageYellow(((ActivityRecentTransactions)context).layout_parent,"Receipt Downloading Started.");
                                }else
                                    Constants.SnakeMessageYellow(((ActivityRecentTransactions)context).layout_parent,"No receipt generated.");
                            }

                            @Override
                            public void onPermissionDenied(List<String> deniedPermissions)
                            {
                                Constants.SnakeMessageYellow(((ActivityRecentTransactions)context).layout_parent,"Task couldn't be performed as permission is required for it.");
                            }
                        })
                        .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                        .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .check());

        holder.lyt_email_invoice.setOnClickListener(v ->
        {
            final ProgressDialog progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Please wait...."); // Setting Message
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
            progressDialog.show(); // Display Progress Dialog
            progressDialog.setCancelable(false);
            Call<GeneralResponseModel> generalResponseModelCall = apiInterface.sendEmailInvoice(Constants.SEND_INVOICE, Pref.getValue(context,Constants.UserID,"",Constants.FILENAME),
                    recentTransactionsModel.get(position).getTransactionID());

            generalResponseModelCall.enqueue(new Callback<GeneralResponseModel>()
            {
                @Override
                public void onResponse(@NonNull Call<GeneralResponseModel> call, @NonNull Response<GeneralResponseModel> response)
                {
                    progressDialog.dismiss();
                    GeneralResponseModel generalResponseModel = response.body();
                    assert generalResponseModel != null;
                    if (generalResponseModel.status.equalsIgnoreCase("success"))
                    {
                        Constants.SnakeMessageYellow(((ActivityRecentTransactions)context).layout_parent,generalResponseModel.message);
                    }
                    else
                    {
                        Constants.SnakeMessageYellow(((ActivityRecentTransactions)context).layout_parent,generalResponseModel.message);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GeneralResponseModel> call, @NonNull Throwable t)
                {
                    progressDialog.dismiss();
                    t.printStackTrace();
                }
            });
        });
    }

    @Override
    public int getItemCount()
    {
        return recentTransactionsModel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView txt_amount,txt_transcation_id,txt_transaction_date,txt_game_name,txt_league_status,txt_game_type,txt_sign;
            LinearLayout lyt_email_invoice,lyt_download;
        ImageView img_more;
        LinearLayout child_transaction,lyt_invoice;
        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            txt_amount=itemView.findViewById(R.id.txt_amount);
            txt_transcation_id=itemView.findViewById(R.id.txt_transcation_id);
            txt_sign=itemView.findViewById(R.id.txt_sign);
            child_transaction=itemView.findViewById(R.id.child_transaction);
            txt_transaction_date=itemView.findViewById(R.id.txt_transaction_date);
            txt_game_name=itemView.findViewById(R.id.txt_game_name);
            txt_game_type=itemView.findViewById(R.id.txt_game_type);
            lyt_email_invoice=itemView.findViewById(R.id.lyt_email_invoice);
            lyt_download=itemView.findViewById(R.id.lyt_download);
            lyt_invoice=itemView.findViewById(R.id.lyt_invoice);
            img_more=itemView.findViewById(R.id.img_more);
            txt_league_status=itemView.findViewById(R.id.txt_league_status);
            img_more.setOnClickListener(v ->
            {
                if (child_transaction.getVisibility()==View.VISIBLE)
                {
                    child_transaction.setVisibility(View.GONE);
                    img_more.setImageResource(R.drawable.ic_next_arrow);
                }
                else
                {
                    child_transaction.setVisibility(View.VISIBLE);
                    img_more.setImageResource(R.drawable.ic_down_arrow);
                }
            });
        }
    }
}
