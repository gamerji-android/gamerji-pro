package com.esports.gamerjipro.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esports.gamerjipro.Interface.FeedbackListener;
import com.esports.gamerjipro.Model.ContestDetailsModel;
import com.esports.gamerjipro.R;

import java.util.ArrayList;

public class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.ViewHolder>
{

    Context context;
    private ArrayList<ContestDetailsModel.ContestsData.RatingsData> ratingsDataArrayList;
    private FeedbackListener feedbackListener ;
    String selectedPosition;

    public FeedbackAdapter(Context context, ArrayList<ContestDetailsModel.ContestsData.RatingsData> ratingsDataArrayList,FeedbackListener feedbackListener)
    {
        this.context=context;
        this.ratingsDataArrayList=ratingsDataArrayList;
        this.feedbackListener=feedbackListener;
    }

    @NonNull
    @Override
    public FeedbackAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_feedback_gif, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FeedbackAdapter.ViewHolder holder, int position)
    {
        Glide.with(context).asGif().load(ratingsDataArrayList.get(position).FeaturedImage).into(holder.img_gif);
        holder.txt_gif_text.setText(ratingsDataArrayList.get(position).RatingText);
        holder.itemView.setOnClickListener(v ->
        {
            final float scale = context.getResources().getDisplayMetrics().density;
            int pixels = (int) (80 * scale + 0.5f);
            holder.img_gif.setLayoutParams(new LinearLayout.LayoutParams(pixels,pixels));
            selectedPosition=ratingsDataArrayList.get(position).RatingID;
            feedbackListener.onFeedbackClickListener(ratingsDataArrayList.get(position));
            removeunselectedItems();
        }) ;
    }

    private void removeunselectedItems()
    {

        ArrayList<ContestDetailsModel.ContestsData.RatingsData> temMovieList = new ArrayList<>();
        for (ContestDetailsModel.ContestsData.RatingsData movie:ratingsDataArrayList)
        {
            if(movie.RatingID.equals(selectedPosition))
            {
                temMovieList.add(movie);
            }
        }
        ratingsDataArrayList.clear();
        ratingsDataArrayList.addAll(temMovieList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount()
    {
        return ratingsDataArrayList.size();
    }

    public class ViewHolder  extends RecyclerView.ViewHolder
    {
        ImageView img_gif;
        TextView txt_gif_text;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            img_gif=itemView.findViewById(R.id.img_gif);
            txt_gif_text=itemView.findViewById(R.id.txt_gif_text);
        }
    }
}
