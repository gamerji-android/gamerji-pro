package com.esports.gamerjipro.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.R;

public class ItemFeedbackAdapter extends RecyclerView.Adapter<ItemFeedbackAdapter.ViewHolder>
{
    Context context ;
    public ItemFeedbackAdapter(Context contest)
    {
            this.context=contest;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item_feedback, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemFeedbackAdapter.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount()
    {
        return 5;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
        }
    }
}
