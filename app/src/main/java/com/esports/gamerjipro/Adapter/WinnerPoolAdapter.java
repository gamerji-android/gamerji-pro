package com.esports.gamerjipro.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Model.WinnerContestModel;
import com.esports.gamerjipro.R;

import java.util.ArrayList;

public class WinnerPoolAdapter extends RecyclerView.Adapter<WinnerPoolAdapter.ViewHolder>
{
    Context context;
    private ArrayList<WinnerContestModel.Data.PrizePoolsData> prizePoolsDataArrayList;
    public WinnerPoolAdapter(Context context, ArrayList<WinnerContestModel.Data.PrizePoolsData> data)
    {
        this.context= context;
        prizePoolsDataArrayList=data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_all_player_info, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        holder.mTxtRank.setText(prizePoolsDataArrayList.get(position).Title);
        holder.txt_rupee_symbol.setText(prizePoolsDataArrayList.get(position).PrizeSign);
        holder.mTxtPointsTeam.setText(prizePoolsDataArrayList.get(position).Prize);

    }

    @Override
    public int getItemCount()
    {
        return prizePoolsDataArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView mTxtRank,mTxtPointsTeam,txt_rupee_symbol;
        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            mTxtRank=itemView.findViewById(R.id.mTxtRank);
            mTxtPointsTeam=itemView.findViewById(R.id.mTxtPointsTeam);
            txt_rupee_symbol=itemView.findViewById(R.id.txt_rupee_symbol);

        }
    }
}
