package com.esports.gamerjipro.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Interface.OnBankClikcListener;
import com.esports.gamerjipro.Model.BankDetail;
import com.esports.gamerjipro.R;

import java.util.ArrayList;

public class BankListAdapter extends RecyclerView.Adapter<BankListAdapter.ViewHolder>
{    Context context;
    private ArrayList<BankDetail> bankDetailArrayList;
    private OnBankClikcListener onBankClikcListener;

    public BankListAdapter(Context context, ArrayList<BankDetail> bankDetailArrayList, OnBankClikcListener onBankClikcListener )
    {
        this.context=context;
        this.bankDetailArrayList=bankDetailArrayList;
        this.onBankClikcListener=onBankClikcListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_bank_list, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        holder.txt_bank_name.setText(bankDetailArrayList.get(position).name);
        holder.itemView.setOnClickListener(v ->
        {
            onBankClikcListener.OnBankItemClickListener(bankDetailArrayList.get(position));

        });
    }



    @Override
    public int getItemCount()
    {
        return bankDetailArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {

        TextView txt_bank_name;
        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            txt_bank_name=itemView.findViewById(R.id.txt_bank_name);
        }
    }

}
