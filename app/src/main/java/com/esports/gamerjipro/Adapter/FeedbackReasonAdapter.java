package com.esports.gamerjipro.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Interface.FeedbackReasonListener;
import com.esports.gamerjipro.Model.ContestDetailsModel;
import com.esports.gamerjipro.R;

import java.util.ArrayList;

public class FeedbackReasonAdapter extends RecyclerView.Adapter<FeedbackReasonAdapter.ViewHoler>
{
    Context context;
    private ArrayList<ContestDetailsModel.ContestsData.RatingsData.OptionsData> optionsDataArrayList;
    private boolean checked;
    private FeedbackReasonListener feedbackReasonListener;
    public FeedbackReasonAdapter(Context context, ArrayList<ContestDetailsModel.ContestsData.RatingsData.OptionsData> optionsDataArrayList, boolean checke, FeedbackReasonListener feedbackReasonListener )
    {
        this.context=context;
        this.optionsDataArrayList=optionsDataArrayList;
        this.checked=checke;
        this.feedbackReasonListener=feedbackReasonListener;
    }

    @NonNull
    @Override
    public ViewHoler onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item_feedback, parent, false);
        return new ViewHoler(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHoler holder, int position)
    {
        holder.cb_feedback.setText(optionsDataArrayList.get(position).OptionText);
        if (checked)
        {
            holder.cb_feedback.setChecked(true);
        }
        else
        {
            holder.cb_feedback.setChecked(false);
        }

        holder.cb_feedback.setOnCheckedChangeListener((buttonView, isChecked) ->
        {

            if (checked)
                checked=false;
            else
                checked=true;

            feedbackReasonListener.onFeedbackReasonClick(optionsDataArrayList.get(position).ROptionID,checked);
        });

    }

    @Override
    public int getItemCount()
    {
        return optionsDataArrayList.size();
    }

    class ViewHoler extends RecyclerView.ViewHolder
    {
        CheckBox cb_feedback;
        ViewHoler(@NonNull View itemView)
        {
            super(itemView);
            cb_feedback=itemView.findViewById(R.id.cb_feedback);
        }
    }
}
