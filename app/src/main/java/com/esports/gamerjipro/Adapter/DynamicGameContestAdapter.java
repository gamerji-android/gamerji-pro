package com.esports.gamerjipro.Adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.esports.gamerjipro.Fragment.DynamicGamesTabFragment;
import com.esports.gamerjipro.Model.DashboardModel;

import java.util.ArrayList;

public class DynamicGameContestAdapter extends FragmentStatePagerAdapter {
    private int tabCount;
    private ArrayList<DashboardModel.GamesData.Gdata> gdataArrayList;

    public DynamicGameContestAdapter(FragmentManager fm, int tabCount, ArrayList<DashboardModel.GamesData.Gdata> gdataArrayList) {
        super(fm);
        this.tabCount = tabCount;
        this.gdataArrayList = gdataArrayList;
    }

    @Override
    public Fragment getItem(int position) {
        return DynamicGamesTabFragment.newInstance(position, gdataArrayList);
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return super.getPageTitle(position);
    }
}
