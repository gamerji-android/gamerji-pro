package com.esports.gamerjipro.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esports.gamerjipro.Interface.ContestJoinClickListener;
import com.esports.gamerjipro.Interface.WinnerClickListener;
import com.esports.gamerjipro.Model.ContestTypesModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.Utils.CustomLinearLayoutManager;
import com.esports.gamerjipro.activity.ActivityGameContest;

import java.util.ArrayList;

public class GameContestListAdapter extends RecyclerView.Adapter<GameContestListAdapter.ViewHolder>
{
    private Activity context;
    private ArrayList<ContestTypesModel.Data.TypesData> gameContestListModels;
    private WinnerClickListener winnerClickListener;
    private ContestJoinClickListener contestJoinClickListener;

    public GameContestListAdapter(Activity context, ArrayList<ContestTypesModel.Data.TypesData> gameContestListModels, WinnerClickListener winnerClickListener,
                                  ContestJoinClickListener contestJoinClickListener )
    {
        this.context=context;
        this.gameContestListModels=gameContestListModels;
        this.winnerClickListener=winnerClickListener;
        this.contestJoinClickListener=contestJoinClickListener;
    }

    @NonNull
    @Override
    public GameContestListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_game_contest, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull GameContestListAdapter.ViewHolder holder, int position)
    {

        if (Integer.parseInt(gameContestListModels.get(position).ContestsCount)>0)
        {
            ((ActivityGameContest)context).isEmpty=false;
            holder.txt_contest_title.setText(gameContestListModels.get(position).Name);
            Glide.with(context).load(gameContestListModels.get(position).FeaturedImage).into(holder.img_contest_image);
            CustomLinearLayoutManager customLayoutManager = new CustomLinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
            GameContestDetailAdapter gameContestDetailAdapter = new GameContestDetailAdapter(context, gameContestListModels.get(position).contestsDataArrayList,winnerClickListener,contestJoinClickListener);
            holder.rv_contest.setLayoutManager(customLayoutManager);
            holder.rv_contest.setItemAnimator(new DefaultItemAnimator());
            holder.rv_contest.setAdapter(gameContestDetailAdapter);

        }
        else
        {
            holder.lyt_trophy.setVisibility(View.GONE);
            holder.rv_contest.setVisibility(View.GONE);
           // gameContestListModels.remove(position);
            if (((ActivityGameContest)context).isEmpty)
            ((ActivityGameContest)context).isEmpty=true;
        }

        if (position==gameContestListModels.size()-1)
        {
            if ((((ActivityGameContest) context).isEmpty))
            {
                ((ActivityGameContest) context).lyt_empty.setVisibility(View.VISIBLE);
                ((ActivityGameContest) context).swipe_refresh_home.setVisibility(View.GONE);
            }
            else
            {
                ((ActivityGameContest) context).lyt_empty.setVisibility(View.GONE);
                ((ActivityGameContest) context).swipe_refresh_home.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public int getItemCount()
    {
        return gameContestListModels.size();
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public int getItemViewType(int position)
    {
        return position;
    }
    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView txt_contest_title;
        RecyclerView rv_contest;
        ImageView img_contest_image;
        LinearLayout contest_list_layout,lyt_trophy;
        ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            txt_contest_title=itemView.findViewById(R.id.txt_contest_title);
            rv_contest=itemView.findViewById(R.id.rv_contest);
            contest_list_layout=itemView.findViewById(R.id.contest_list_layout);
            img_contest_image=itemView.findViewById(R.id.img_contest_image);
            lyt_trophy=itemView.findViewById(R.id.lyt_trophy);

        }
    }
}
