package com.esports.gamerjipro.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Model.GamerjiPointsModel;
import com.esports.gamerjipro.R;

import java.util.ArrayList;

public class GamerjiPointsAdapter extends RecyclerView.Adapter<GamerjiPointsAdapter.ViewHolder>
{
    Context context ;
    ArrayList<GamerjiPointsModel.Data.StatesData> statesData;

    public GamerjiPointsAdapter(Context context, ArrayList<GamerjiPointsModel.Data.StatesData> statesData)
    {
        this.context=context;
        this.statesData=statesData;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_gamerji_points, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        holder.txt_amount.setText(statesData.get(position).Name);
        holder.txt_league_status.setText(statesData.get(position).Points);

    }

    @Override
    public int getItemCount()
    {
        return statesData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView txt_amount,txt_league_status;
        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            txt_league_status=itemView.findViewById(R.id.txt_league_status);
            txt_amount=itemView.findViewById(R.id.txt_amount);
        }
    }
}
