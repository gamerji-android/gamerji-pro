package com.esports.gamerjipro.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Interface.TournamentTimeSelectedListener;
import com.esports.gamerjipro.Model.TournamentTimeSlotModel;
import com.esports.gamerjipro.R;

import java.util.ArrayList;

public class TournamentTimeAdapter extends RecyclerView.Adapter<TournamentTimeAdapter.ViewHolder>
{

    Context context;
    private TournamentTimeSelectedListener onTournamentTimeSelectedListener;
    private ArrayList<TournamentTimeSlotModel.Data.TimeSlots> timeSlotsArrayList;
    private int lastCheckedPosition = -1;
    public TournamentTimeAdapter(Context context, ArrayList<TournamentTimeSlotModel.Data.TimeSlots> timeSlotsArrayList, TournamentTimeSelectedListener onTournamentTimeSelectedListener)
    {
        this.context= context;
        this.onTournamentTimeSelectedListener= onTournamentTimeSelectedListener;
        this.timeSlotsArrayList=timeSlotsArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_time_slots, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        holder.rb_time.setText(timeSlotsArrayList.get(position).ContestTime);
        holder.rb_time.setChecked(position == lastCheckedPosition);
    }

    @Override
    public int getItemCount()
    {
        return timeSlotsArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {

        RadioButton rb_time;
        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            rb_time=itemView.findViewById(R.id.rb_time);
            rb_time.setOnClickListener(v ->
            {
                lastCheckedPosition = getAdapterPosition();
                //because of this blinking problem occurs so
                //i have a suggestion to add notifyDataSetChanged();
                //   notifyItemRangeChanged(0, list.length);//blink list problem
                onTournamentTimeSelectedListener.onTournamentTimeSelectedListener(timeSlotsArrayList.get(getAdapterPosition()));
                notifyDataSetChanged();
            });

        }
    }
}
