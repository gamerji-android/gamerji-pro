package com.esports.gamerjipro.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Interface.RemovePlayerListener;
import com.esports.gamerjipro.Interface.SquadMobileListener;
import com.esports.gamerjipro.Model.SquadModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.activity.ActivitySquadRegistration;
import com.hbb20.CountryCodePicker;

import java.util.ArrayList;

public class SquadRegistrationAdapter extends RecyclerView.Adapter<SquadRegistrationAdapter.ViewHolder>
{
    Context context ;
    private ArrayList<SquadModel> squadModelArrayList;
    private SquadMobileListener squadMobileListener ;
    private RemovePlayerListener removePlayerListener ;
    private int canJoinPlayers;
    private String main_game_name;

    public SquadRegistrationAdapter(Context context, ArrayList<SquadModel> squadModelArrayList, int canJoinPlayers, SquadMobileListener squadMobileListener,
                                    RemovePlayerListener removePlayerListener, String main_game_name)
    {
        this.context=context;
        this.squadModelArrayList=squadModelArrayList;
        this.squadMobileListener=squadMobileListener;
        this.removePlayerListener=removePlayerListener;
        this.canJoinPlayers=canJoinPlayers;
        this.main_game_name=main_game_name;
    }

    @NonNull
    @Override
    public SquadRegistrationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_player_registration, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull SquadRegistrationAdapter.ViewHolder holder, int position)
    {
        int number= position+2;
        holder.txt_player_number.setText("Player-"+ number );
        holder.txt_game_name.setText(main_game_name+" "+"Name");
        if (position>=(canJoinPlayers-1))
        {
            holder.txt_player_number.setText("Player-"+ number+"(Optional)" );
        }

        holder.txt_player_game_name.setText(squadModelArrayList.get(position).getUser_name());
        holder.txt_player_mobile.setText(squadModelArrayList.get(position).getMobile_number());
        holder.txt_gamerji_name.setText(squadModelArrayList.get(position).getGamerjiName());

        if (squadModelArrayList.get(position).getGamerjiName().equalsIgnoreCase(""))
        {
            holder.txt_player_game_name.setHint("Enter Player "+number +" Username");
            holder.txt_gamerji_name.setHint("Enter Player "+ number +" Gamerji Name");
        }
        if (squadModelArrayList.get(position).getMobile_number().equalsIgnoreCase(""))
            holder.txt_player_mobile.setHint("Mobile No.");

        if (squadModelArrayList.get(position).isAddplayer())
        {
            holder.lyt_add_player.setBackgroundColor(Color.RED);
            holder.txt_player_mobile.setEnabled(false);
            holder.txt_apply.setText("REMOVE");
        }
        else
        {
            holder.txt_player_mobile.setEnabled(true);
            holder.lyt_add_player.setBackgroundColor(context.getResources().getColor(R.color.orange_color));
            holder.txt_apply.setText("ADD");
        }

            holder.cv_add_player.setOnClickListener(v ->
            {
                    if (holder.txt_player_mobile.getText().length()<10)
                    {
                        Constants.SnakeMessageYellow(((ActivitySquadRegistration)context).lyt_parent,"Enter the number properly.");
                    }
                    else
                    {
                        if (squadModelArrayList.get(position).isAddplayer())
                        {
                            removePlayerListener.onPlayerRemoveListener(position);
                        }
                        else
                        {
                            squadMobileListener.onSquadMobileEnteredListener(squadModelArrayList.get(position),position,
                                    holder.txt_player_mobile.getText().toString(),holder.player_country_code.getSelectedCountryCodeWithPlus());
                        }
                    }
            });
    }




    @Override
    public int getItemCount()
    {
        return squadModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
       public CountryCodePicker player_country_code;
       public TextView txt_player_number,txt_game_name;
       public EditText txt_player_mobile;
       public TextView txt_player_game_name,txt_gamerji_name,txt_apply;
       public CardView cv_add_player;
       RelativeLayout lyt_add_player;
        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            player_country_code=itemView.findViewById(R.id.player_country_code);
            txt_player_mobile=itemView.findViewById(R.id.txt_player_mobile);
            txt_player_game_name=itemView.findViewById(R.id.txt_player_game_name);
            txt_gamerji_name=itemView.findViewById(R.id.txt_gamerji_name);
            txt_player_number=itemView.findViewById(R.id.txt_player_number);
            cv_add_player=itemView.findViewById(R.id.cv_add_player);
            lyt_add_player=itemView.findViewById(R.id.lyt_add_player);
            txt_apply=itemView.findViewById(R.id.txt_apply);
            txt_game_name=itemView.findViewById(R.id.txt_game_name);
        }
    }
}
