package com.esports.gamerjipro.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Model.NotificationModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.activity.ActivityContestDetails;
import com.esports.gamerjipro.activity.BottomNavigationActivity;

import java.util.ArrayList;

public class SubNotificationAdapter extends RecyclerView.Adapter<SubNotificationAdapter.ViewHolder>
{

    private Context context;
    private ArrayList<NotificationModel.NotificationDetailModel> notificationDetailModels;

    public SubNotificationAdapter(Context context, ArrayList<NotificationModel.NotificationDetailModel> notificationDetailModels)
    {
        this.context=context;
        this.notificationDetailModels=notificationDetailModels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sub_notification_layout, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
       // Glide.with()
        holder.mTxtMessage.setText(notificationDetailModels.get(position).getDescription());
        holder.mTxtDateTimeInside.setText(notificationDetailModels.get(position).getCreationDate());
        holder.itemView.setOnClickListener(v -> {
            if (notificationDetailModels.get(position).getType().equalsIgnoreCase("1"))
            {
                Intent i = new Intent(context, ActivityContestDetails.class);
                i.putExtra("contest_id",notificationDetailModels.get(position).getRelationID());
                context.startActivity(i);
            }
            else
            {
//                Intent i = new Intent(context, ActivityProfilee.class);
                Intent i = new Intent(context, BottomNavigationActivity.class);
                context.startActivity(i);
                BottomNavigationActivity.currentTabPosition = 3;
            }
        });

        if (position==notificationDetailModels.size()-1)
        {
            holder.mLineView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount()
    {
        return notificationDetailModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        ImageView mImgNotificationIcon;
        TextView mTxtMessage,mTxtDateTimeInside;
        View mLineView;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            mImgNotificationIcon=itemView.findViewById(R.id.mImgNotificationIcon);
            mTxtMessage=itemView.findViewById(R.id.mTxtMessage);
            mTxtDateTimeInside=itemView.findViewById(R.id.mTxtDateTimeInside);
            mLineView=itemView.findViewById(R.id.mLineView);

        }
    }
}
