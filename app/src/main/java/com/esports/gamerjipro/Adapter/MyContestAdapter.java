package com.esports.gamerjipro.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Fragment.MyContestsFragment;
import com.esports.gamerjipro.Model.MyContestModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.Utils.AppConstants;
import com.esports.gamerjipro.Utils.FileUtils;
import com.esports.gamerjipro.activity.ActivityContestDetails;
import com.esports.gamerjipro.activity.ActivityTournamentDetail;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MyContestAdapter extends RecyclerView.Adapter<MyContestAdapter.ViewHolder> {
    private Activity context;
    private ArrayList<MyContestModel.Data.ContestsData> contestsDataArrayList;

    public MyContestAdapter(Activity context, ArrayList<MyContestModel.Data.ContestsData> contestsDataArrayList) {
        this.context = context;
        this.contestsDataArrayList = contestsDataArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_my_contest, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return contestsDataArrayList.size();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if (contestsDataArrayList.get(position).Type.equalsIgnoreCase("2")) {
            holder.lyt_tournament.setVisibility(View.VISIBLE);
            holder.txt_tournamnet_name.setText(contestsDataArrayList.get(position).Title);
        } else {
            holder.lyt_tournament.setVisibility(View.GONE);
        }
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM-yyyy");
        @SuppressLint("SimpleDateFormat") DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        @SuppressLint("SimpleDateFormat") DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
        String inputDateStr = contestsDataArrayList.get(position).Date;
        Date date = null;
        try {
            date = inputFormat.parse(inputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String outputDateStr = outputFormat.format(date);

        holder.txt_date.setText(outputDateStr);
        holder.txt_time.setText(FileUtils.getFormatedDateTime(contestsDataArrayList.get(position).Time, "HH:mm:ss", "hh:mm a"));
        holder.txt_map.setText(contestsDataArrayList.get(position).Map_Length);
        holder.txt_persp.setText(contestsDataArrayList.get(position).Perspective_LevelCap);
        if (contestsDataArrayList.get(position).WinningAmount.equalsIgnoreCase("") || contestsDataArrayList.get(position).WinningAmount.equalsIgnoreCase("0")) {
            holder.winning_amount.setText("-");
            holder.rupees_symbol.setVisibility(View.GONE);
        } else
            holder.winning_amount.setText(" " + contestsDataArrayList.get(position).WinningAmount);

        holder.txt_perkill_title.setText(contestsDataArrayList.get(position).PerKill_MaxLosesTitle);


        holder.txt_winners.setText(contestsDataArrayList.get(position).WinnersCount);
        if (contestsDataArrayList.get(position).WinnersCount.isEmpty() && contestsDataArrayList.get(position).PerKill_MaxLoses.isEmpty()) {
            holder.lyt_winners.setVisibility(View.GONE);
            holder.lyt_kills.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 3.0f);
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 1.0f);
            holder.lyt_winnings.setLayoutParams(params);
            holder.lyt_entry_fees.setLayoutParams(params1);
        } else if (contestsDataArrayList.get(position).WinnersCount.isEmpty()) {
            holder.lyt_winners.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 2.0f);
            holder.lyt_kills.setLayoutParams(params);
        } else if (contestsDataArrayList.get(position).PerKill_MaxLoses.isEmpty()) {
            holder.lyt_kills.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 2.0f);
            holder.lyt_winners.setLayoutParams(params);
        } else {
            if (Integer.parseInt(contestsDataArrayList.get(position).WinnersCount) == 1)
                holder.txt_winner_title.setText("Winner");

            if (Integer.parseInt(contestsDataArrayList.get(position).PerKill_MaxLoses) == 1)
                holder.txt_perkill.setText(contestsDataArrayList.get(position).PerKill_MaxLoses);
        }

        if (!contestsDataArrayList.get(position).RoomColumn.isEmpty()) {
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 1.0f);
            holder.lyt_column.setVisibility(View.VISIBLE);
            holder.lyt_column.setLayoutParams(params1);
            holder.lyt_room.setLayoutParams(params1);
            holder.lyt_password.setLayoutParams(params1);
            holder.txt_column.setText(contestsDataArrayList.get(position).RoomColumn);
        } else {
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 1.5f);
            holder.lyt_column.setVisibility(View.GONE);
            holder.txt_slot_info.setVisibility(View.GONE);
            holder.lyt_password.setLayoutParams(params1);
            holder.lyt_room.setLayoutParams(params1);
        }


        holder.txt_perkill.setText(contestsDataArrayList.get(position).PerKill_MaxLoses);
        if (contestsDataArrayList.get(position).EntryFee.equalsIgnoreCase("0")) {
            holder.txt_entry_fees.setText("Free");
            holder.txt_entry_rupee.setVisibility(View.GONE);
        } else
            holder.txt_entry_fees.setText(contestsDataArrayList.get(position).EntryFee);

        if (contestsDataArrayList.get(position).RoomID.isEmpty()) {
            holder.txt_room_id.setText("-");
        } else {
            holder.txt_room_id.setText(contestsDataArrayList.get(position).RoomID);
        }

        if (contestsDataArrayList.get(position).RoomPassword.isEmpty()) {
            holder.txt_password.setText("-");
        } else {
            holder.txt_password.setText(contestsDataArrayList.get(position).RoomPassword);
        }


        holder.txt_map_title.setText(contestsDataArrayList.get(position).Map_LengthTitle);
        holder.txt_perspective_title.setText(contestsDataArrayList.get(position).Perspective_LevelCapTitle);
        int available_spots = Integer.parseInt(contestsDataArrayList.get(position).TotalSpots);
        int joined_spots = Integer.parseInt(contestsDataArrayList.get(position).JoinedSpots);
        int remaining_spots = available_spots - joined_spots;

        if (remaining_spots > 1)
            holder.txt_available_spots.setText(remaining_spots + "  players remaining");
        else
            holder.txt_available_spots.setText(remaining_spots + "  player remaining");

        if (joined_spots > 1)
            holder.txt_available_remaining.setText(joined_spots + " players joined");
        else
            holder.txt_available_remaining.setText(joined_spots + " player joined");

        holder.progress_bar.setMax(available_spots);
        holder.progress_bar.setProgress(joined_spots);

        holder.txt_title.setText(contestsDataArrayList.get(position).GameTypeName);

        // ToDo: Old -> Game Status
        /*if (contestsDataArrayList.get(position).Status.equalsIgnoreCase("3")) {
            holder.txt_live.setVisibility(View.VISIBLE);
        } else if (contestsDataArrayList.get(position).Status.equalsIgnoreCase("4")) {
            holder.txt_live.setVisibility(View.VISIBLE);
            holder.txt_live.setText(context.getResources().getString(R.string.review));
        } else {
            holder.txt_live.setVisibility(View.GONE);
        }*/

        // ToDo: New -> Game Status
        if (contestsDataArrayList.get(position).Status.equalsIgnoreCase(AppConstants.GAME_STATUS_ID_WAITING)) {
            holder.txt_live.setVisibility(View.VISIBLE);
            holder.mLinearSeparator.setVisibility(View.VISIBLE);
            holder.txt_live.setText(AppConstants.GAME_STATUS_WAITING);
        } else if (contestsDataArrayList.get(position).Status.equalsIgnoreCase(AppConstants.GAME_STATUS_ID_STARTED)) {
            holder.txt_live.setVisibility(View.VISIBLE);
            holder.mLinearSeparator.setVisibility(View.VISIBLE);
            holder.txt_live.setText(AppConstants.GAME_STATUS_STARTED);
        } else if (contestsDataArrayList.get(position).Status.equalsIgnoreCase(AppConstants.GAME_STATUS_ID_IN_PROGRESS)) {
            holder.txt_live.setVisibility(View.VISIBLE);
            holder.mLinearSeparator.setVisibility(View.VISIBLE);
        } else if (contestsDataArrayList.get(position).Status.equalsIgnoreCase(AppConstants.GAME_STATUS_ID_REVIEWING)) {
            holder.txt_live.setVisibility(View.VISIBLE);
            holder.mLinearSeparator.setVisibility(View.VISIBLE);
            holder.txt_live.setText(AppConstants.GAME_STATUS_REVIEWING);
        } else if (contestsDataArrayList.get(position).Status.equalsIgnoreCase(AppConstants.GAME_STATUS_ID_COMPLETED)) {
            holder.txt_live.setVisibility(View.VISIBLE);
            holder.mLinearSeparator.setVisibility(View.VISIBLE);
            holder.txt_live.setText(AppConstants.GAME_STATUS_COMPLETED);
        } else if (contestsDataArrayList.get(position).Status.equalsIgnoreCase(AppConstants.GAME_STATUS_ID_CANCELLED)) {
            holder.txt_live.setVisibility(View.VISIBLE);
            holder.mLinearSeparator.setVisibility(View.VISIBLE);
            holder.txt_live.setText(AppConstants.GAME_STATUS_CANCELLED);
        } else {
            holder.txt_live.setVisibility(View.GONE);
            holder.mLinearSeparator.setVisibility(View.GONE);
        }

        holder.lyt_rules.setOnClickListener(v ->
        {
            MyContestsFragment.rulesClickListener.OnMyContestRulesClickListener(contestsDataArrayList.get(position));

        });

        holder.lyt_contest_item.setOnClickListener(v ->
        {
            Intent i;
            if (contestsDataArrayList.get(position).Type.equalsIgnoreCase("1")) {
                i = new Intent(context, ActivityContestDetails.class);
                i.putExtra("contest_id", contestsDataArrayList.get(position).ContestID);
            } else {
                i = new Intent(context, ActivityTournamentDetail.class);
                i.putExtra("contest_id", contestsDataArrayList.get(position).TournamentID);
            }
            context.startActivity(i);

        });
        holder.lyt_winner.setOnClickListener(v -> MyContestsFragment.winnerClickListener.onMyContestWinnerClick(contestsDataArrayList.get(position)));

        if (contestsDataArrayList.get(position).Status.equalsIgnoreCase("1")) {
            holder.txt_room_id.setText("-");
            holder.txt_password.setText("-");
        }
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        TextView txt_date, txt_time, txt_map, txt_persp, winning_amount, txt_winners, txt_perkill, txt_entry_fees, txt_available_spots, rupees_symbol, txt_entry_rupee,
                txt_available_remaining, txt_join_contest, txt_room_id, txt_password, txt_title, txt_live, txt_map_title, txt_perspective_title, txt_winner_title, txt_column, txt_perkill_title,
                txt_slot_info, txt_tournamnet_name;
        ProgressBar progress_bar;
        LinearLayout lyt_contest_item, lyt_winner, lyt_kills, lyt_winners, lyt_entry_fees, lyt_winnings, lyt_column, lyt_password, lyt_room, lyt_tournament, lyt_rules, mLinearSeparator;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_date = itemView.findViewById(R.id.txt_date);
            txt_time = itemView.findViewById(R.id.txt_time);
            txt_map = itemView.findViewById(R.id.txt_map);
            lyt_tournament = itemView.findViewById(R.id.lyt_tournament);
            txt_persp = itemView.findViewById(R.id.txt_persp);
            winning_amount = itemView.findViewById(R.id.winning_amount);
            txt_winners = itemView.findViewById(R.id.txt_winners);
            txt_perkill = itemView.findViewById(R.id.txt_perkill);
            txt_entry_fees = itemView.findViewById(R.id.txt_entry_fees);
            txt_available_spots = itemView.findViewById(R.id.txt_available_spots);
            txt_available_remaining = itemView.findViewById(R.id.txt_available_remaining);
            txt_join_contest = itemView.findViewById(R.id.txt_join_contest);
            progress_bar = itemView.findViewById(R.id.progress_bar);
            txt_room_id = itemView.findViewById(R.id.txt_room_id);
            rupees_symbol = itemView.findViewById(R.id.rupees_symbol);
            txt_password = itemView.findViewById(R.id.txt_password);
            txt_title = itemView.findViewById(R.id.txt_title);
            txt_entry_rupee = itemView.findViewById(R.id.txt_entry_rupee);
            txt_live = itemView.findViewById(R.id.txt_live);
            lyt_contest_item = itemView.findViewById(R.id.lyt_contest_item);
            lyt_winner = itemView.findViewById(R.id.lyt_winner);
            txt_slot_info = itemView.findViewById(R.id.txt_slot_info);
            txt_map_title = itemView.findViewById(R.id.txt_map_title);
            txt_perspective_title = itemView.findViewById(R.id.txt_perspective_title);
            txt_winner_title = itemView.findViewById(R.id.txt_winner_title);
            lyt_kills = itemView.findViewById(R.id.lyt_kills);
            lyt_winners = itemView.findViewById(R.id.lyt_winners);
            lyt_entry_fees = itemView.findViewById(R.id.luy_entry_fees);
            lyt_winnings = itemView.findViewById(R.id.lyt_winnings);
            lyt_room = itemView.findViewById(R.id.lyt_room);
            lyt_column = itemView.findViewById(R.id.lyt_column);
            lyt_password = itemView.findViewById(R.id.lyt_password);
            txt_column = itemView.findViewById(R.id.txt_column);
            txt_perkill_title = itemView.findViewById(R.id.txt_perkill_title);
            txt_tournamnet_name = itemView.findViewById(R.id.txt_tournamnet_name);
            lyt_rules = itemView.findViewById(R.id.lyt_rules);
            mLinearSeparator = itemView.findViewById(R.id.linear_row_separator);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }
}
