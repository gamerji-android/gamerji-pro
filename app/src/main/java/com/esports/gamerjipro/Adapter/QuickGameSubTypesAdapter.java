package com.esports.gamerjipro.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esports.gamerjipro.Model.QuickGameSubTypesModel;
import com.esports.gamerjipro.R;

import java.util.ArrayList;

public class QuickGameSubTypesAdapter extends RecyclerView.Adapter {


    private Activity mActivity;
    private ArrayList<QuickGameSubTypesModel.Data.GamesData> mArrQuickGameSubTypes;
    private static SingleClickListener sClickListener;

    /**
     * Adapter contains the data to be displayed
     */
    public QuickGameSubTypesAdapter(Activity mActivity, ArrayList<QuickGameSubTypesModel.Data.GamesData> mArrQuickGameSubTypes) {
        this.mActivity = mActivity;
        this.mArrQuickGameSubTypes = mArrQuickGameSubTypes;
    }

    /**
     * Create an interface for the recycler view click events
     */
    public void setOnCardViewClickListener(SingleClickListener clickListener) {
        sClickListener = clickListener;
    }

    public interface SingleClickListener {
        void onCardViewClickListener(QuickGameSubTypesModel.Data.GamesData quickGameTypesModel);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .row_quick_game_sub_types_item, parent, false);
        return new CustomViewHolder(itemView);
    }

    @SuppressLint({"SetJavaScriptEnabled", "SetTextI18n"})
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final QuickGameSubTypesModel.Data.GamesData quickGameTypesModel = mArrQuickGameSubTypes.get(position);

        ((CustomViewHolder) holder).mTextGameName.setSelected(true);
        ((CustomViewHolder) holder).mTextGameName.setText(quickGameTypesModel.getName());
        ((CustomViewHolder) holder).mTextTimesPlayed.setText(quickGameTypesModel.getPlayedCount());
        Glide.with(mActivity).load(quickGameTypesModel.getThumbImage()).into(((CustomViewHolder) holder).mImageGameSubType);

        // ToDo: Subscribe Button Click
        ((CustomViewHolder) holder).mLinearSubGameNameFullView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sClickListener.onCardViewClickListener(quickGameTypesModel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrQuickGameSubTypes.size();
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public int getItemViewType(int position)
    {
        return position;
    }

    /**
     * Id declarations
     */
    public static class CustomViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mLinearSubGameNameFullView;
        ImageView mImageGameSubType;
        TextView mTextGameName, mTextTimesPlayed;

        @SuppressLint("SetJavaScriptEnabled")
        public CustomViewHolder(final View itemView) {
            super(itemView);

            // Image Views
            mLinearSubGameNameFullView = itemView.findViewById(R.id.linear_row_quick_game_sub_type_game_name);

            // Image Views
            mImageGameSubType = itemView.findViewById(R.id.image_row_quick_game_sub_type_game);

            // Text Views
            this.mTextGameName = itemView.findViewById(R.id.text_row_quick_game_sub_type_game_name);
            this.mTextTimesPlayed = itemView.findViewById(R.id.text_row_quick_game_sub_type_times_played);
        }
    }
}
