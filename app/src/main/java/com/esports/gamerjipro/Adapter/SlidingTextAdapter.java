package com.esports.gamerjipro.Adapter;

import android.content.Context;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.esports.gamerjipro.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class SlidingTextAdapter extends PagerAdapter
{
//    private ArrayList<String> slider_title;
//    private ArrayList<String> slider_info;
    private ArrayList<Integer> imagesArray;
    private LayoutInflater inflater;
    private Context context;

    public SlidingTextAdapter(Context context, ArrayList<Integer> imagesArray)
    {
        this.context=context;
//        this.slider_title=slider_title;
//        this.slider_info=slider_info;
        this.imagesArray=imagesArray;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount()
    {
        return imagesArray.size();
    }

    @NotNull
    @Override
    public Object instantiateItem(ViewGroup view, int position)
    {
        View textLayout = inflater.inflate(R.layout.sliding_texts, view, false);

        assert textLayout != null;
//         TextView txt_slider_title = textLayout.findViewById(R.id.txt_slider_title);
//         TextView txt_slider_text = textLayout.findViewById(R.id.txt_slider_text);
         ImageView img_main = textLayout.findViewById(R.id.img_main);


//        txt_slider_title.setText(slider_title.get(position));
//        txt_slider_text.setText(slider_info.get(position));
        img_main.setImageResource(imagesArray.get(position));
        view.addView(textLayout, 0);

        return textLayout;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object)
    {
        return view.equals(object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }
    @Override
    public Parcelable saveState() {
        return null;
    }
}
