package com.esports.gamerjipro.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Model.RecentTransactionsModel;
import com.esports.gamerjipro.R;

import java.util.ArrayList;

public class RecentTransactionAdapter  extends RecyclerView.Adapter<RecentTransactionAdapter.ViewHolder>
{
    private Context context;
    private ArrayList<RecentTransactionsModel> recent_transaction_list;
    SubAdapterRecentTransactions subAdapterRecentTransactions ;

    public RecentTransactionAdapter(Context context, ArrayList<RecentTransactionsModel> recent_transaction_list)
    {
        this.recent_transaction_list=recent_transaction_list;
        this.context=context;
    }

    @NonNull
    @Override
    public RecentTransactionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recent_transacations, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecentTransactionAdapter.ViewHolder holder, int position)
    {
        holder.txt_date.setText(recent_transaction_list.get(position).getDate());
        subAdapterRecentTransactions= new SubAdapterRecentTransactions(context,recent_transaction_list.get(position).getTransactionDetailArrayList());
        holder.rv_transcations.setAdapter(subAdapterRecentTransactions);

    }

    @Override
    public int getItemCount()
    {
        return recent_transaction_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {

        TextView txt_date;
        RecyclerView rv_transcations;
        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            rv_transcations=itemView.findViewById(R.id.rv_transcations);
            rv_transcations.setLayoutManager(new LinearLayoutManager(context));
            rv_transcations.setItemAnimator(new DefaultItemAnimator());
            txt_date=itemView.findViewById(R.id.txt_date);
        }
    }
}
