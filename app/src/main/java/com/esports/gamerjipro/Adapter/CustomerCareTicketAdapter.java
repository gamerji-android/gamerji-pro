package com.esports.gamerjipro.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Model.CustomerTicketsModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.activity.ActivityComplainTicketDetail;

import java.util.ArrayList;

public class CustomerCareTicketAdapter extends RecyclerView.Adapter<CustomerCareTicketAdapter.ViewHolder>
{

    Context context;
    private ArrayList<CustomerTicketsModel.Data.TicketsData> customerTicketsModelArrayList;

    public CustomerCareTicketAdapter(Context context, ArrayList<CustomerTicketsModel.Data.TicketsData> customerTicketsModelArrayList)
    {
        this.context=context;
        this.customerTicketsModelArrayList=customerTicketsModelArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_customer_care_ticket, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        holder.txt_date.setText(customerTicketsModelArrayList.get(position).Date);
        holder.txt_subject.setText(customerTicketsModelArrayList.get(position).Subject);
        holder.txt_status.setText(customerTicketsModelArrayList.get(position).Status);
        if(customerTicketsModelArrayList.get(position).StatusValue.equalsIgnoreCase("1"))
        {
            holder.txt_status.setTextColor(Color.BLACK);
        }
        else if (customerTicketsModelArrayList.get(position).StatusValue.equalsIgnoreCase("2"))
        {
            holder.txt_status.setTextColor(Color.RED);
        }
        else if (customerTicketsModelArrayList.get(position).StatusValue.equalsIgnoreCase("3"))
        {
            holder.txt_status.setTextColor(Color.YELLOW);
        }
        else if (customerTicketsModelArrayList.get(position).StatusValue.equalsIgnoreCase("4"))
        {
            holder.txt_status.setTextColor(context.getResources().getColor(R.color.green_color));
        }
        else if (customerTicketsModelArrayList.get(position).StatusValue.equalsIgnoreCase("5"))
        {
            holder.txt_status.setTextColor(Color.RED);
        }

        holder.txt_status.setText(customerTicketsModelArrayList.get(position).Status);
        holder.txt_ticket_no.setText(customerTicketsModelArrayList.get(position).Code);

        holder.itemView.setOnClickListener(v ->
        {
            Intent i = new Intent(context, ActivityComplainTicketDetail.class);
            i.putExtra("ticket_id",customerTicketsModelArrayList.get(position).TicketID);
            i.putExtra("Date",customerTicketsModelArrayList.get(position).Date);
            i.putExtra("Subject",customerTicketsModelArrayList.get(position).Subject);
            i.putExtra("Status",customerTicketsModelArrayList.get(position).Status);
            i.putExtra("StatusValue",customerTicketsModelArrayList.get(position).StatusValue);
            context.startActivity(i);
        });
    }

    @Override
    public int getItemCount()
    {
        return customerTicketsModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView txt_subject,txt_status,txt_date,txt_ticket_no;
        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            txt_date=itemView.findViewById(R.id.txt_date);
            txt_subject=itemView.findViewById(R.id.txt_subject);
            txt_status=itemView.findViewById(R.id.txt_status);
            txt_ticket_no=itemView.findViewById(R.id.txt_ticket_no);
        }
    }
}
