package com.esports.gamerjipro.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esports.gamerjipro.Model.GetMessagesModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.Pref;

import java.util.ArrayList;

public class ComaplintChatAdapter extends RecyclerView.Adapter<ComaplintChatAdapter.ViewHolder>
{
    Context context;
    private ArrayList<GetMessagesModel.Data.MessagesData> messagesDataArrayList;

    public ComaplintChatAdapter(Context context, ArrayList<GetMessagesModel.Data.MessagesData> messagesDataArrayList)
    {
        this.context=context;
        this.messagesDataArrayList=messagesDataArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_complaint_chat, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        if (messagesDataArrayList.get(position).IsUser.equalsIgnoreCase("2"))
        {
            holder.lyt_admin.setVisibility(View.GONE);
            holder.lyt_user.setVisibility(View.VISIBLE);
            holder.txt_user_msg.setText(messagesDataArrayList.get(position).Message);
            holder.txt_user_time.setText(messagesDataArrayList.get(position).DateAgo);
            Glide.with(context).load(Pref.getValue(context, Constants.ProfileImage,"",Constants.FILENAME)).into(holder.img_user);
        }
        else
        {
            holder.lyt_admin.setVisibility(View.VISIBLE);
            holder.lyt_user.setVisibility(View.GONE);
            holder.txt_admin_msg.setText(messagesDataArrayList.get(position).Message);
            holder.txt_admin_time.setText(messagesDataArrayList.get(position).DateAgo);
        }

    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount()
    {
        return messagesDataArrayList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder
    {
        ImageView img_user,img_admin;
        TextView txt_user_time,txt_user_msg,txt_admin_time,txt_admin_msg;
        LinearLayout lyt_admin,lyt_user;
        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            img_user=itemView.findViewById(R.id.img_user);
            img_admin=itemView.findViewById(R.id.img_admin);
            txt_user_time=itemView.findViewById(R.id.txt_user_time);
            txt_user_msg=itemView.findViewById(R.id.txt_user_msg);
            txt_admin_time=itemView.findViewById(R.id.txt_admin_time);
            txt_admin_msg=itemView.findViewById(R.id.txt_admin_msg);
            lyt_user=itemView.findViewById(R.id.lyt_user);
            lyt_admin=itemView.findViewById(R.id.lyt_admin);

        }
    }
}
