package com.esports.gamerjipro.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Interface.ContestJoinClickListener;
import com.esports.gamerjipro.Interface.WinnerClickListener;
import com.esports.gamerjipro.Model.ContestTypesModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.Utils.FileUtils;
import com.esports.gamerjipro.activity.ActivityGameContest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class GameContestDetailAdapter extends RecyclerView.Adapter<GameContestDetailAdapter.ViewHolder>
{
    private Activity context;
    private ArrayList<ContestTypesModel.Data.TypesData.ContestsData> gameContestDetailsModels;
    private WinnerClickListener winnerClickListener ;
    private ContestJoinClickListener contestJoinClickListener;

    GameContestDetailAdapter(Activity context, ArrayList<ContestTypesModel.Data.TypesData.ContestsData> gameContestDetailsModels, WinnerClickListener winnerClickListener, ContestJoinClickListener contestJoinClickListener)
    {
        this.context=context;
        this.gameContestDetailsModels=gameContestDetailsModels;
        this.winnerClickListener=winnerClickListener;
        this.contestJoinClickListener=contestJoinClickListener;
    }

    @NonNull
    @Override
    public GameContestDetailAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_layout_contest_detail, parent, false);
        if (gameContestDetailsModels.isEmpty())
        {
            itemView.setVisibility(View.GONE);
        }
        return new ViewHolder(itemView);
    }

    @SuppressLint({"SetTextI18n", "ResourceAsColor"})
    @Override
    public void onBindViewHolder(@NonNull GameContestDetailAdapter.ViewHolder holder, int position)
    {

        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM-yyyy");
        @SuppressLint("SimpleDateFormat") DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        @SuppressLint("SimpleDateFormat") DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
        String inputDateStr=gameContestDetailsModels.get(position).Date;
        Date date = null;
        try
        {
            date = inputFormat.parse(inputDateStr);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }

        String outputDateStr = outputFormat.format(date);

        holder.txt_date.setText(outputDateStr);

        ((ActivityGameContest)context).game_id=gameContestDetailsModels.get(position).GameID;

        holder.txt_time.setText(FileUtils.getFormatedDateTime(gameContestDetailsModels.get(position).Time,"HH:mm:ss", "hh:mm a"));
        holder.txt_map.setText(gameContestDetailsModels.get(position).Map_Length);
        holder.txt_persp.setText(gameContestDetailsModels.get(position).Perspective_LevelCap);

        if (gameContestDetailsModels.get(position).WinningAmount.equalsIgnoreCase("") || gameContestDetailsModels.get(position).WinningAmount.equalsIgnoreCase("0"))
        {
            holder.winning_amount.setText("-");
            holder.rupees_symbol.setVisibility(View.GONE);
        }
        else
        holder.winning_amount.setText(" "+gameContestDetailsModels.get(position).WinningAmount);

        holder.txt_winners.setText(gameContestDetailsModels.get(position).WinnersCount);
        holder.txt_perkill_title.setText(gameContestDetailsModels.get(position).PerKill_MaxLosesTitle);
        holder.txt_perkill.setText(" "+gameContestDetailsModels.get(position).PerKill_MaxLoses);

        if (gameContestDetailsModels.get(position).PerKill_MaxLosesCurrency)
        {
            holder.txt_rupee_symbol.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.txt_rupee_symbol.setVisibility(View.GONE);
        }
        if (gameContestDetailsModels.get(position).WinnersCount.isEmpty() && gameContestDetailsModels.get(position).PerKill_MaxLoses.isEmpty())
        {
            holder.lyt_winners.setVisibility(View.GONE);
            holder.lyt_kills.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT,3.0f);
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT,1.0f);
            holder.lyt_winnings.setLayoutParams(params);
            holder.ly_entry_fees.setLayoutParams(params1);

        }
        else if (gameContestDetailsModels.get(position).WinnersCount.isEmpty())
        {
            holder.lyt_winners.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT,2.0f);
            holder.lyt_kills.setLayoutParams(params);
        }
        else if (gameContestDetailsModels.get(position).PerKill_MaxLoses.isEmpty())
        {
            holder.lyt_kills.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT,2.0f);
            holder.lyt_winners.setLayoutParams(params);
        }
        else
        {
            if (Integer.parseInt(gameContestDetailsModels.get(position).WinnersCount)==1)
                holder.txt_winner_title.setText("Winner");

            if (Integer.parseInt(gameContestDetailsModels.get(position).PerKill_MaxLoses)==1)
                holder.txt_perkill.setText(" "+gameContestDetailsModels.get(position).PerKill_MaxLoses);
        }

        if (gameContestDetailsModels.get(position).EntryFee.equalsIgnoreCase("0"))
        {
            holder.txt_entry_fees.setText("Free");
            holder.txt_entry_rupee.setVisibility(View.GONE);
        }
        else
        holder.txt_entry_fees.setText(" "+gameContestDetailsModels.get(position).EntryFee);

        holder.txt_map_title.setText(gameContestDetailsModels.get(position).Map_LengthTitle);
        holder.txt_perspective_title.setText(gameContestDetailsModels.get(position).Perspective_LevelCapTitle);
        int available_spots= Integer.parseInt(gameContestDetailsModels.get(position).TotalSpots);
        int joined_spots=Integer.parseInt(gameContestDetailsModels.get(position).JoinedSpots);
        int remaining_spots=available_spots-joined_spots;

        if (remaining_spots>1)
        holder.txt_available_spots.setText(remaining_spots +" players remaining");
        else
            holder.txt_available_spots.setText(remaining_spots +" player remaining");

        if (joined_spots>1)
        holder.txt_available_remaining.setText(joined_spots +" players joined");
        else
            holder.txt_available_remaining.setText(joined_spots +" player joined");

        holder.progress_bar.setMax(available_spots);
        holder.progress_bar.setProgress(joined_spots);
        holder.progress_bar.setProgress(joined_spots);

        if (gameContestDetailsModels.get(position).ConfirmStatus.equalsIgnoreCase("1"))
            holder.txt_confirm.setVisibility(View.VISIBLE);
        else
            holder.txt_confirm.setVisibility(View.GONE);

        /*if (context instanceof ActivityMyContestList)
        {
            holder.txt_join_contest.setVisibility(View.GONE);
        }*/

        if (gameContestDetailsModels.get(position).Joined)
        {
            holder.txt_join_contest.setText("Joined");
            holder.txt_join_contest.setBackgroundColor(context.getResources().getColor(R.color.orange_color));
            holder.txt_join_contest.setTextColor(Color.WHITE);
        }




       /* holder.txt_join_contest.setOnClickListener(v ->
        {
                if (gameContestDetailsModels.get(position).Joined)
                {
                    Intent i = new Intent(context, ActivityContestDetails.class);
                    i.putExtra("contest_id",gameContestDetailsModels.get(position).ContestID);
                    context.startActivity(i);
                    context.finish();
                }

                else
                {
                    ((ActivityGameContest)context).contest_id=gameContestDetailsModels.get(position).ContestID;
                    ((ActivityGameContest)context).txt_entry_fees.setText(gameContestDetailsModels.get(position).EntryFee);
                    ((ActivityGameContest)context).txt_usable_cash.setText("0");
                    ((ActivityGameContest)context).txt_to_pay.setText(gameContestDetailsModels.get(position).EntryFee);

                    if ( ((ActivityGameContest)context).UniqueName.equalsIgnoreCase(""))
                    {
                        Intent i = new Intent(context, ActivityDOB.class);
                        i.putExtra("game_name",((ActivityGameContest) context).game_name);
                        i.putExtra("user_game_id",((ActivityGameContest) context).userGameId);
                        i.putExtra("game_id",((ActivityGameContest) context).game_id);
                        context.startActivityForResult(i, Constants.PROFILE_UPDATE_STATUS_REQUEST_CODE);
                    }
                    else
                    {
                        if (((ActivityGameContest)context).sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED)
                        {

                            ((ActivityGameContest)context).sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        }
                        else
                        {
                            ((ActivityGameContest)context).sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        }
                    }
                }

        });*/
        holder.lyt_winners_drop.setOnClickListener(v -> winnerClickListener.onWinnersClick(gameContestDetailsModels.get(position)));
        holder.txt_join_contest.setOnClickListener(v -> contestJoinClickListener.onContestJoinClickLitener(gameContestDetailsModels.get(position)));
    }

    @Override
    public int getItemCount()
    {
        return gameContestDetailsModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView txt_date,txt_time,txt_map,txt_persp,winning_amount,txt_winners,txt_perkill,txt_entry_fees,txt_available_spots,txt_entry_rupee,
                txt_available_remaining,txt_join_contest,txt_perspective_title,txt_map_title,txt_confirm,txt_winner_title,txt_perkill_title,txt_rupee_symbol,rupees_symbol;
        LinearLayout lyt_winners_drop,lyt_kills,lyt_winners,ly_entry_fees,lyt_winnings;
        ProgressBar progress_bar;
        LinearLayout lyt_winning_amount;
        ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            txt_date=itemView.findViewById(R.id.txt_date);
            txt_time=itemView.findViewById(R.id.txt_time);
            txt_map=itemView.findViewById(R.id.txt_map);
            txt_persp=itemView.findViewById(R.id.txt_persp);
            winning_amount=itemView.findViewById(R.id.winning_amount);
            txt_winners=itemView.findViewById(R.id.txt_winners);
            txt_perkill=itemView.findViewById(R.id.txt_perkill);
            txt_entry_fees=itemView.findViewById(R.id.txt_entry_fees);
            txt_available_spots=itemView.findViewById(R.id.txt_available_spots);
            txt_available_remaining=itemView.findViewById(R.id.txt_available_remaining);
            txt_join_contest=itemView.findViewById(R.id.txt_join_contest);
            progress_bar=itemView.findViewById(R.id.progress_bar);
            txt_perspective_title=itemView.findViewById(R.id.txt_perspective_title);
            txt_map_title=itemView.findViewById(R.id.txt_map_title);
            txt_confirm=itemView.findViewById(R.id.txt_confirm);
            lyt_winners_drop=itemView.findViewById(R.id.lyt_winners_drop);
            txt_winner_title=itemView.findViewById(R.id.txt_winner_title);
            lyt_winners=itemView.findViewById(R.id.lyt_winners);
            lyt_kills=itemView.findViewById(R.id.lyt_kills);
            ly_entry_fees=itemView.findViewById(R.id.ly_entry_fees);
            lyt_winnings=itemView.findViewById(R.id.lyt_winnings);
            txt_perkill_title=itemView.findViewById(R.id.txt_perkill_title);
            txt_rupee_symbol=itemView.findViewById(R.id.txt_rupee_symbol);
            rupees_symbol=itemView.findViewById(R.id.rupees_symbol);
            lyt_winning_amount=itemView.findViewById(R.id.lyt_winning_amount);
            txt_entry_rupee=itemView.findViewById(R.id.txt_entry_rupee);

        }
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
