package com.esports.gamerjipro.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esports.gamerjipro.Interface.ContestJoinClickListener;
import com.esports.gamerjipro.Interface.WinnerClickListener;
import com.esports.gamerjipro.Model.ContestSortedListModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.Utils.CustomLinearLayoutManager;
import com.esports.gamerjipro.activity.ActivityGameContestNew;

import java.util.ArrayList;

public class GameContestListAdapterNew extends RecyclerView.Adapter<GameContestListAdapterNew.ViewHolder>
{
    private Activity context;
    private ArrayList<ContestSortedListModel> sortedListModelArrayList;
    private WinnerClickListener winnerClickListener;
    private ContestJoinClickListener contestJoinClickListener;

    public GameContestListAdapterNew(Activity context, ArrayList<ContestSortedListModel> sortedListModelArrayList, WinnerClickListener winnerClickListener,
                                     ContestJoinClickListener contestJoinClickListener )
    {
        this.context=context;
        this.sortedListModelArrayList=sortedListModelArrayList;
        this.winnerClickListener=winnerClickListener;
        this.contestJoinClickListener=contestJoinClickListener;
       // setHasStableIds(true);
    }

    @NonNull
    @Override
    public GameContestListAdapterNew.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_game_contest, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull GameContestListAdapterNew.ViewHolder holder, int position)
    {
        if (sortedListModelArrayList.size()>0)
        {
            ((ActivityGameContestNew)context).isEmpty=false;

            holder.txt_contest_title.setText(sortedListModelArrayList.get(position).getContest_Name());
            Glide.with(context).load(sortedListModelArrayList.get(position).getContest_Image()).into(holder.img_contest_image);

            CustomLinearLayoutManager customLayoutManager = new CustomLinearLayoutManager(context, LinearLayoutManager.VERTICAL,false);
            GameContestDetailAdapterNew gameContestDetailAdapter = new GameContestDetailAdapterNew(context, sortedListModelArrayList.get(position).getContestsDataArrayList(),winnerClickListener,contestJoinClickListener);
            holder.rv_contest.setLayoutManager(customLayoutManager);
            holder.rv_contest.setItemAnimator(new DefaultItemAnimator());
            holder.rv_contest.setAdapter(gameContestDetailAdapter);
            gameContestDetailAdapter.notifyDataSetChanged();
        }
        else
        {
            holder.lyt_trophy.setVisibility(View.GONE);
            holder.rv_contest.setVisibility(View.GONE);
            if (((ActivityGameContestNew)context).isEmpty)
                ((ActivityGameContestNew)context).isEmpty=true;
        }

        if (position==sortedListModelArrayList.size()-1)
        {
            if ((((ActivityGameContestNew) context).isEmpty))
            {
                ((ActivityGameContestNew) context).lyt_empty.setVisibility(View.VISIBLE);
                ((ActivityGameContestNew) context).swipe_refresh_home.setVisibility(View.GONE);
            }
            else
            {
                ((ActivityGameContestNew) context).lyt_empty.setVisibility(View.GONE);
                ((ActivityGameContestNew) context).swipe_refresh_home.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public int getItemCount()
    {
        return sortedListModelArrayList.size();
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public int getItemViewType(int position)
    {
        return position;
    }

    static class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView txt_contest_title;
        RecyclerView rv_contest;
        ImageView img_contest_image;
        LinearLayout contest_list_layout,lyt_trophy;
        ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            txt_contest_title=itemView.findViewById(R.id.txt_contest_title);
            rv_contest=itemView.findViewById(R.id.rv_contest);
            contest_list_layout=itemView.findViewById(R.id.contest_list_layout);
            img_contest_image=itemView.findViewById(R.id.img_contest_image);
            lyt_trophy=itemView.findViewById(R.id.lyt_trophy);

        }
    }
}
