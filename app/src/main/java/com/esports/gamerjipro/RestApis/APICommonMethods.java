package com.esports.gamerjipro.RestApis;

import com.esports.gamerjipro.Utils.Common;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;

public class APICommonMethods {

    /**
     * Sets up the media type for api call
     *
     * @return - Return Media Type
     */
    public static MediaType getMediaType() {
        return MediaType.parse("application/octet-stream");
    }

    /**
     * Sets the child json for Game Type - Get Games Data API
     *
     * @param mGameId - Game Id
     * @param mUserId - User Id
     * @return - Return results
     */
    public static String setGameTypeGetGamesDataJson(String mGameId, String mUserId) {
        String result = "";

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GAME_TYPE_GET_GAMES_DATA.REQUEST_ACTION, WebFields.GAME_TYPE_GET_GAMES_DATA.MODE);
            jsonBody.put(WebFields.GAME_TYPE_GET_GAMES_DATA.REQUEST_VAL_GAME_ID, mGameId);
            jsonBody.put(WebFields.GAME_TYPE_GET_GAMES_DATA.REQUEST_VAL_USER_ID, mUserId);

            Common.insertLog("Request::::> " + jsonBody.toString());

            result = jsonBody.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Add Change Username API
     *
     * @param mUserGameId  - User Game Id
     * @param mGameId      - Game Id
     * @param mUserId      - User Id
     * @param mNewUsername - New Username
     * @return - Return results
     */
    public static String setAddChangeUsernameJson(String mUserGameId, String mGameId, String mUserId, String mNewUsername) {
        String result = "";

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_CHANGE_USERNAME.REQUEST_ACTION, WebFields.ADD_CHANGE_USERNAME.MODE);
            jsonBody.put(WebFields.ADD_CHANGE_USERNAME.REQUEST_VAL_USER_GAME_ID, mUserGameId);
            jsonBody.put(WebFields.ADD_CHANGE_USERNAME.REQUEST_VAL_GAME_ID, mGameId);
            jsonBody.put(WebFields.ADD_CHANGE_USERNAME.REQUEST_VAL_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_CHANGE_USERNAME.REQUEST_VAL_USER_IGN, mNewUsername);

            Common.insertLog("Request::::> " + jsonBody.toString());

            result = jsonBody.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Get Games API
     *
     * @param mUserId - User Id
     * @return - Return results
     */
    public static String setGetGamesJson(String mUserId) {
        String result = "";

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_GAMES.REQUEST_ACTION, WebFields.GET_GAMES.MODE);
            jsonBody.put(WebFields.GET_GAMES.REQUEST_VAL_USER_ID, mUserId);

            Common.insertLog("Request::::> " + jsonBody.toString());

            result = jsonBody.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Add Streamer API
     *
     * @param mUserId             - User Id
     * @param mName               - Name
     * @param mCountryCode        - Country Code
     * @param mMobileNo           - Mobile No
     * @param mEmail              - Email
     * @param mGender             - Gender
     * @param mYoutubeChannelName - Youtube Channel Name
     * @param mYoutubeChannelLink - Youtube Channel Link
     * @param mOtherGameName      - Other Game Name
     * @param jsonArrayGames      - Selected Games Array
     * @param jsonArrayPlatforms  - Selected Platforms Array
     * @return - Return results
     */
    public static String setAddStreamerJson(String mUserId, String mName, String mCountryCode, String mMobileNo, String mEmail,
                                            String mGender, String mYoutubeChannelName, String mYoutubeChannelLink,
                                            String mOtherGameName, JSONArray jsonArrayGames, JSONArray jsonArrayPlatforms) {
        String result = "";

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_STREAMER.REQUEST_ACTION, WebFields.ADD_STREAMER.MODE);

            jsonBody.put(WebFields.ADD_STREAMER.REQUEST_VAL_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_STREAMER.REQUEST_VAL_NAME, mName);
            jsonBody.put(WebFields.ADD_STREAMER.REQUEST_VAL_COUNTRY_CODE, mCountryCode);
            jsonBody.put(WebFields.ADD_STREAMER.REQUEST_VAL_MOBILE_NUMBER, mMobileNo);
            jsonBody.put(WebFields.ADD_STREAMER.REQUEST_VAL_EMAIL_ADDRESS, mEmail);
            jsonBody.put(WebFields.ADD_STREAMER.REQUEST_VAL_GENDER, mGender);
            jsonBody.put(WebFields.ADD_STREAMER.REQUEST_VAL_YOUTUBE_CHANNEL_NAME, mYoutubeChannelName);
            jsonBody.put(WebFields.ADD_STREAMER.REQUEST_VAL_YOUTUBE_CHANNEL_LINK, mYoutubeChannelLink);
            jsonBody.put(WebFields.ADD_STREAMER.REQUEST_VAL_OTHER_GAME, mOtherGameName);
            jsonBody.put(WebFields.ADD_STREAMER.REQUEST_ARR_GAMES, jsonArrayGames);
            jsonBody.put(WebFields.ADD_STREAMER.REQUEST_ARR_PLATFORMS, jsonArrayPlatforms);

            Common.insertLog("Request::::> " + jsonBody.toString());

            result = jsonBody.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Get Streamer Status
     *
     * @param mUserId - User Id
     * @return - Return results
     */
    public static String setGetStreamerStatusJson(String mUserId) {
        String result = "";

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_STREAMER_STATUS.REQUEST_ACTION, WebFields.GET_STREAMER_STATUS.MODE);
            jsonBody.put(WebFields.GET_STREAMER_STATUS.REQUEST_VAL_USER_ID, mUserId);

            Common.insertLog("Request::::> " + jsonBody.toString());

            result = jsonBody.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Get Videos
     *
     * @param mUserId      - User Id
     * @param mVideoType   - Videos Type (1 -> Default, 2 -> Influencer, 3 -> Host)
     * @param mCurrentPage - Current Page
     * @param mPageSize    - Page Size
     * @return - Return results
     */
    public static String setGetVideosJson(String mUserId, String mVideoType, int mCurrentPage, int mPageSize) {
        String result = "";

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_VIDEOS.REQUEST_ACTION, WebFields.GET_VIDEOS.MODE);
            jsonBody.put(WebFields.GET_VIDEOS.REQUEST_VAL_USER_ID, mUserId);
            jsonBody.put(WebFields.GET_VIDEOS.REQUEST_VAL_TYPE, mVideoType);
            jsonBody.put(WebFields.GET_VIDEOS.REQUEST_VAL_PAGE, String.valueOf(mCurrentPage));
            jsonBody.put(WebFields.GET_VIDEOS.REQUEST_VAL_LIMIT, String.valueOf(mPageSize));

            Common.insertLog("Request::::> " + jsonBody.toString());

            result = jsonBody.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Add Videos API
     *
     * @param mUserId     - User Id
     * @param mVideoTitle - Video Title
     * @param mVideoLink  - Video Link
     * @return - Return results
     */
    public static String setAddVideosJson(String mUserId, String mVideoTitle, String mVideoLink) {
        String result = "";

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_VIDEOS.REQUEST_ACTION, WebFields.ADD_VIDEOS.MODE);

            jsonBody.put(WebFields.ADD_VIDEOS.REQUEST_VAL_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_VIDEOS.REQUEST_VAL_TITLE, mVideoTitle);
            jsonBody.put(WebFields.ADD_VIDEOS.REQUEST_VAL_LINK, mVideoLink);

            Common.insertLog("Request::::> " + jsonBody.toString());

            result = jsonBody.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Get Customer Care
     *
     * @param mUserId - User Id
     * @return - Return results
     */
    public static String setGetCustomerCareJson(String mUserId) {
        String result = "";

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_CUSTOMER_CARE.REQUEST_ACTION, WebFields.GET_CUSTOMER_CARE.MODE);
            jsonBody.put(WebFields.GET_CUSTOMER_CARE.REQUEST_VAL_USER_ID, mUserId);

            Common.insertLog("Request::::> " + jsonBody.toString());

            result = jsonBody.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }
}
