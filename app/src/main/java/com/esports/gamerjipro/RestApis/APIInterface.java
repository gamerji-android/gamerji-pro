package com.esports.gamerjipro.RestApis;

import com.esports.gamerjipro.Model.AccountDetailsModel;
import com.esports.gamerjipro.Model.AddChangeUsernameModel;
import com.esports.gamerjipro.Model.AddStreamerModel;
import com.esports.gamerjipro.Model.AddVideosModel;
import com.esports.gamerjipro.Model.AppMaintenanceStatusModel;
import com.esports.gamerjipro.Model.ContestDetailsModel;
import com.esports.gamerjipro.Model.ContestPlayersModel;
import com.esports.gamerjipro.Model.ContestTypeModelNew;
import com.esports.gamerjipro.Model.ContestTypesModel;
import com.esports.gamerjipro.Model.CustomerCareModel;
import com.esports.gamerjipro.Model.CustomerIssuesModel;
import com.esports.gamerjipro.Model.CustomerTicketsModel;
import com.esports.gamerjipro.Model.DashboardModel;
import com.esports.gamerjipro.Model.DashboardQuickGamesModel;
import com.esports.gamerjipro.Model.GameTypeGamesDataModel;
import com.esports.gamerjipro.Model.GameTypesModel;
import com.esports.gamerjipro.Model.GamerProfileModel;
import com.esports.gamerjipro.Model.GamerjiPointsModel;
import com.esports.gamerjipro.Model.GamerjiProPopupModel;
import com.esports.gamerjipro.Model.GeneralResponseModel;
import com.esports.gamerjipro.Model.GetAllGamesModel;
import com.esports.gamerjipro.Model.GetGamesModel;
import com.esports.gamerjipro.Model.GetMessagesModel;
import com.esports.gamerjipro.Model.GetStreamerStatusModel;
import com.esports.gamerjipro.Model.GetVideosModel;
import com.esports.gamerjipro.Model.HostStreamVideosModel;
import com.esports.gamerjipro.Model.JoinContestModel;
import com.esports.gamerjipro.Model.JoinedContestTimeSlotsModel;
import com.esports.gamerjipro.Model.LeaderboardModel;
import com.esports.gamerjipro.Model.MyContestModel;
import com.esports.gamerjipro.Model.PopularVideosModel;
import com.esports.gamerjipro.Model.QuickGameEndGameModel;
import com.esports.gamerjipro.Model.QuickGameJoinGameModel;
import com.esports.gamerjipro.Model.QuickGameJoinGamePopupModel;
import com.esports.gamerjipro.Model.QuickGameSubTypesModel;
import com.esports.gamerjipro.Model.QuickGameTypesModel;
import com.esports.gamerjipro.Model.ResendOTPModel;
import com.esports.gamerjipro.Model.RulesModel;
import com.esports.gamerjipro.Model.SearchModel;
import com.esports.gamerjipro.Model.SignInModel;
import com.esports.gamerjipro.Model.SignUpModel;
import com.esports.gamerjipro.Model.SingleContestModel;
import com.esports.gamerjipro.Model.StatesListModel;
import com.esports.gamerjipro.Model.StatisticsModel;
import com.esports.gamerjipro.Model.TCModel;
import com.esports.gamerjipro.Model.TournamentDetailModel;
import com.esports.gamerjipro.Model.TournamentPlayersModel;
import com.esports.gamerjipro.Model.TournamentTimeSlotModel;
import com.esports.gamerjipro.Model.TournamentTypeModel;
import com.esports.gamerjipro.Model.UpdateProfileModel;
import com.esports.gamerjipro.Model.VerifyContestUserModel;
import com.esports.gamerjipro.Model.ViewAllPopularVideosModel;
import com.esports.gamerjipro.Model.WalletUsageLimitModel;
import com.esports.gamerjipro.Model.WinnerContestModel;
import com.esports.gamerjipro.Utils.Constants;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface APIInterface
{
    @POST(Constants.SIGNUP_USER)
    @FormUrlEncoded
    Call<SignInModel> doSignUp(@Field("Action") String Action, @Field("Val_Ucountrycode") String Val_Ucountrycode, @Field("Val_Umobilenumber") String Val_Umobilenumber,
                               @Field("Val_Uemailaddress") String Val_Uemailaddress, @Field("Val_Upassword") String Val_Upassword, @Field("Val_Ufirstname") String Val_Ufirstname, @Field("Val_Ulastname") String Val_Ulastname,
                               @Field("Val_Signupcode") String Val_Coupon, @Field("Val_Type") String Val_Type, @Field("Val_Ufacebookid") String Val_Ufacebookid, @Field("Val_Ugoogleid") String Val_Ugoogleid);

    @POST(Constants.CHECK_MOBILE)
    @FormUrlEncoded
    Call<SignUpModel> checkMobile(@Field("Action") String Action, @Field("Val_Ucountrycode") String Val_Ucountrycode, @Field("Val_Umobilenumber") String Val_Umobilenumber,
                                  @Field("Val_Uemailaddress") String Val_Uemailaddress);

    @POST(Constants.CHECK_MOBILE)
    @FormUrlEncoded
    Call<SignUpModel> applyPromo(@Field("Action") String Action, @Field("Val_Signupcode") String Val_Signupcode);

    @POST(Constants.UPDATE_MOBILE)
    @FormUrlEncoded
    Call<SignInModel> updateMobile(@Field("Action") String Action,  @Field("Val_Umobilenumber") String Val_Umobilenumber,@Field("Val_Ucountrycode") String Val_Ucountrycode,
                                  @Field("Val_Userid") String Val_Userid,@Field("Val_Signupcode") String Val_Signupcode);

    @POST(Constants.SIGNIN_USER)
    @FormUrlEncoded
    Call<SignInModel> doEmailSignIn(@Field("Val_Type") int Val_Type, @Field("Val_Uemailaddress") String Val_Uemailaddress,@Field("Val_Upassword") String Val_Upassword);

    @POST(Constants.SIGNIN_USER)
    @FormUrlEncoded
    Call<SignInModel> doMobileSignIn(@Field("Val_Type") int Val_Type, @Field("Val_Ucountrycode") String Val_Ucountrycode,@Field("Val_Umobilenumber") String Val_Umobilenumber);

    @POST(Constants.V3_DASHBOARD)
    @FormUrlEncoded
    Call<DashboardModel> getDashboard(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid, @Field("Val_Apptype") String Val_Apptype, @Field("Val_Appversion") String Val_Appversion);

    @POST(Constants.V3_USER_FETCH)
    @FormUrlEncoded
    Call<MyContestModel> getMyContest(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid, @Field("Val_Gameid") String Val_Gameid, @Field("Val_Type") String Val_Type, @Field("Val_Page") int Val_Page);

    @POST(Constants.GAMES_TYPE)
    @FormUrlEncoded
    Call<GameTypesModel> getGameTypes(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid,
                                      @Field("Val_Gameid") String Val_Gameid);

    @POST(Constants.GENERAL_FETCH_V2)
    @FormUrlEncoded
    Call<StatisticsModel> getStats(@Field("Action") String Action,@Field("Val_Gameid") String Val_Gameid);

    @POST(Constants.GENERAL_FETCH_V3)
    @FormUrlEncoded
    Call<GetVideosModel> getVideos(@Field("Action") String Action,@Field("Val_Page") int Val_Page,@Field("Val_Limit") int Val_Limit);


    @POST(Constants.RESEND_OTP)
    @FormUrlEncoded
    Call<ResendOTPModel> resendOTP(@Field("Action") String Action, @Field("Val_Ucountrycode") String Val_Ucountrycode,
                                   @Field("Val_Umobilenumber") String Val_Umobilenumber);

    @POST(Constants.FORGOT_OTP)
    @FormUrlEncoded
    Call<SignInModel> forgotOTP(@Field("Action") String Action, @Field("Val_Ucountrycode") String Val_Ucountrycode,
                                   @Field("Val_Umobilenumber") String Val_Umobilenumber);

    @POST(Constants.V2_CONTESTS)
    @FormUrlEncoded
    Call<ContestTypesModel> getContests(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid,
                                        @Field("Val_Gameid") String Val_Gameid, @Field("Val_Typeid") String Val_Typeid);


    @POST(Constants.V3_CONTESTS)
    @FormUrlEncoded
    Call<ContestTypeModelNew> getContests(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid,
                                          @Field("Val_Gameid") String Val_Gameid, @Field("Val_Typeid") String Val_Typeid,
                                          @Field("Val_Page") int Val_Page, @Field("Val_Limit") String Val_Limit);

    @POST(Constants.V3_CONTESTS)
    @FormUrlEncoded
    Call<ContestPlayersModel> getContestsPlayers(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid,
                                                 @Field("Val_Contestid") String Val_Contestid, @Field("Val_Page") int Val_Page,@Field("Val_Limit") int Val_Limit);

    @POST(Constants.TOURNAMENT_FETCH)
    @FormUrlEncoded
    Call<TournamentPlayersModel> getTournamentPlayers(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid,
                                                      @Field("Val_Tournamentid") String Val_Tournamentid, @Field("Val_Page") int Val_Page, @Field("Val_Limit") int Val_Limit);

    @POST(Constants.TOURNAMENT_FETCH)
    @FormUrlEncoded
    Call<TournamentTypeModel> getAllTournent(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid,
                                             @Field("Val_Gameid") String Val_Gameid, @Field("Val_Typeid") String Val_Typeid);

    @POST(Constants.TOURNAMENT_FETCH)
    @FormUrlEncoded
    Call<TournamentTimeSlotModel> getTimeSlots(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid,
                                               @Field("Val_Tournamentid") String Val_Gameid);

    @POST(Constants.V2_USER_FETCH)
    @FormUrlEncoded
    Call<VerifyContestUserModel> verifyContestUser(@Field("Action") String Action, @Field("Val_Ucountrycode") String Val_Ucountrycode,
                                                   @Field("Val_Umobilenumber") String Val_Umobilenumber, @Field("Val_Gameid") String Val_Gameid);

    @POST(Constants.CONTET_FETCH_V3)
    @FormUrlEncoded
    Call<SingleContestModel> getSingleContest(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid,
                                              @Field("Val_Contestid") String Val_Contestid, @Field("Val_IsWithUsers") String Val_IsWithUsers,
                                              @Field("Val_Contestcode") String Val_Contestcode);

    @POST(Constants.GENERAL_FETCH_V3)
    @FormUrlEncoded
    Call<SingleContestModel> checkContestOrTournament(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid,@Field("Val_IsWithUsers") String Val_IsWithUsers,
                                              @Field("Val_CTcode") String Val_Contestcode);


    @POST(Constants.USER_FETCH)
    @FormUrlEncoded
    Call<ContestTypesModel> getMyContests(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid,
                                        @Field("Val_Gameid") String Val_Gameid,@Field("Val_Typeid") String Val_Typeid);

    @POST(Constants.GENERAL_FETCH)
    @FormUrlEncoded
    Call<StatesListModel> getAllStates(@Field("Action") String Action);

    @POST(Constants.GENERAL_FETCH_V3)
    @FormUrlEncoded
    Call<CustomerIssuesModel> getGetCustomerIssues(@Field("Action") String Action);

    @POST(Constants.V3_USER_FETCH)
    @FormUrlEncoded
    Call<JoinedContestTimeSlotsModel> getJoinedContestTimeSlots(@Field("Action") String Action,@Field("Val_Userid") String Val_Userid,@Field("Val_Gameid") String Val_Gameid,@Field("Val_GTypeid") String Val_GTypeid
    ,@Field("Val_Date") String Val_Date);

    @POST(Constants.GAME_FETCH_V3)
    @FormUrlEncoded
    Call<GetAllGamesModel> getAllGames(@Field("Action") String Action,@Field("Val_Userid")  String value);

    @POST(Constants.GENERAL_FETCH_V3)
    @FormUrlEncoded
    Call<CustomerTicketsModel> getCustomerTickets(@Field("Action") String Action,@Field("Val_Userid") String Val_Userid,@Field("Val_Page") int Val_Page,@Field("Val_Limit") int Val_Limit);

    @POST(Constants.GENERAL_FETCH_V3)
    @FormUrlEncoded
    Call<GetMessagesModel> getMessages(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid, @Field("Val_Ticketid") String Val_Ticketid, @Field("Val_Page") int Val_Page, @Field("Val_Limit") int Val_Limit);

    @POST(Constants.GENERAL_FETCH_V3)
    @FormUrlEncoded
    Call<GetMessagesModel> getNewCustomerCareMessage(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid, @Field("Val_Ticketid") String Val_Ticketid, @Field("Val_Messageid") String Val_Messageid);

    @POST(Constants.GENERAL_DETAILS)
    @FormUrlEncoded
    Call<GetMessagesModel.Data> sendMessages(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid, @Field("Val_Ticketid") String Val_Ticketid, @Field("Val_Message") String Val_Message, @Field("Val_Appversion") String Val_Appversion,
                                             @Field("Val_Devicetype") String Val_Devicetype);

    @POST(Constants.GENERAL_FETCH)
    @FormUrlEncoded
    Call<GamerjiPointsModel> getGamerjiPoints(@Field("Action") String Action);

    @POST(Constants.GENERAL_FETCH)
    @FormUrlEncoded
    Call<TCModel> getTerms(@Field("Action") String Action);

    @POST(Constants.UPDATE_PROFILE)
    @FormUrlEncoded
    Call<UpdateProfileModel> updateProfile(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid,
                                           @Field("Val_Ubirthdate") String Val_Ubirthdate, @Field("Val_Ustate") String Val_Ustate,
                                           @Field("Val_Uusergameid") String Val_Uusergameid, @Field("Val_Uuniquename") String Val_Uuniquename, @Field("Val_Uteamname") String Val_Uteamname);

    @POST(Constants.V3_USER_FETCH)
    @FormUrlEncoded
    Call<GamerProfileModel> getGamerProfile(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid);

    @POST(Constants.USER_FETCH)
    @FormUrlEncoded
    Call<GamerProfileModel> viewUserProfile(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid,@Field("Val_Viewerid") String Val_Viewerid);

    @POST(Constants.CONTET_FETCH_V3)
    @FormUrlEncoded
    Call<ContestDetailsModel> getContestDetails(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid,
                                                @Field("Val_Contestid") String Val_Contestid);

    @POST(Constants.TOURNAMENT_FETCH)
    @FormUrlEncoded
    Call<TournamentDetailModel> getTournamentDetails(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid,
                                                     @Field("Val_Tournamentid") String Val_Tournamentid);

    @POST(Constants.USER_PROFILE_V2)
    @FormUrlEncoded
    Call<GeneralResponseModel> addFeedback(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid,
                                                @Field("Val_Ratingid") String Val_Ratingid,@Field("Val_ROptionid") String Val_ROptionid,@Field("Val_Comments") String Val_Comments);

    @POST(Constants.V3_CONTESTS_DETAIL)
    @FormUrlEncoded
    Call<GeneralResponseModel> submitReportIssue(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid,
                                           @Field("Val_Contestid") String Val_Contestid,@Field("Val_Reportid") String Val_Reportid);

    @POST(Constants.V3_CONTESTS_DETAIL)
    @FormUrlEncoded
    Call<GeneralResponseModel> submitRating(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid,
                                                 @Field("Val_Contestid") String Val_Contestid,@Field("Val_Rating") String Val_Rating);

    @POST(Constants.V3_JOIN_CONTESTS)
    @FormUrlEncoded
    Call<JoinContestModel> joinContest(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid,
                                       @Field("Val_Contestid") String Val_Contestid,@Field("Val_Jointype") String Val_Jointype);

    @POST(Constants.V_3_TOURNAMENT_DETAILS)
    @FormUrlEncoded
    Call<JoinContestModel> joinTournament(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid,
                                       @Field("Val_Tournamentid") String Val_Tournamentid,@Field("Val_Contestid") String Val_Contestid);

    @POST(Constants.RESET_PASSWORD)
    @FormUrlEncoded
    Call<SignInModel> resetPassword(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid,
                                       @Field("Val_Upassword") String Val_Upassword);

    @POST(Constants.V3_USER_FETCH)
    @FormUrlEncoded
    Call<AccountDetailsModel> getAccount(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid);

    @POST(Constants.UPDATE_PROFILE)
    @FormUrlEncoded
    Call<GeneralResponseModel>   applyPromo(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid, @Field("Val_Promocode") String Val_Promocode);

    @POST(Constants.USER_FETCH)
    @FormUrlEncoded
    Call<GeneralResponseModel>   getUserRefferal(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid);

    @POST(Constants.USER_FETCH)
    @FormUrlEncoded
    Call<SignInModel>   getProfile(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid);

    @POST(Constants.PAYMENT_DETAILS)
    @FormUrlEncoded
    Call<GeneralResponseModel>   getPaymentToken(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid ,@Field("Val_Amount") String Val_Amount);

    @POST(Constants.PAYMENT_DETAILS)
    @FormUrlEncoded
    Call<GeneralResponseModel>   getPaymentStatus(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid ,@Field("Val_Orderid") String Val_Orderid);

    @POST(Constants.SINGLE_CONTESTS)
    @FormUrlEncoded
    Call<WinnerContestModel>   getContestWinner(@Field("Action") String Action, @Field("Val_Contestid") String Val_Contestid );

    @POST(Constants.TOURNAMENT_FETCH)
    @FormUrlEncoded
    Call<WinnerContestModel>   getTournamentWinners(@Field("Action") String Action, @Field("Val_Tournamentid") String Val_Tournamentid );

    @POST(Constants.TOURNAMENT_FETCH)
    @FormUrlEncoded
    Call<RulesModel> getRules(@Field("Action") String Action,@Field("Val_Userid") String Val_Userid, @Field("Val_Tournamentid") String Val_Tournamentid );

    @POST(Constants.LEADERBOARD_V2)
    @FormUrlEncoded
    Call<LeaderboardModel>   getLeaderBoard(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid,@Field("Val_Page") String Val_Page,@Field("Val_Limit") String Val_Limit,@Field("Val_Type") String Val_Type,
                                            @Field("Val_Rank") String Val_Rank,@Field("Val_Points") String Val_Points);

    @POST(Constants.SEARCH)
    @FormUrlEncoded
    Call<SearchModel>   searchUser(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid, @Field("Val_Keyword") String Val_Keyword );


    @POST(Constants.V3_USER_FETCH)
    @FormUrlEncoded
    Call<WalletUsageLimitModel>   getUserWalletLimit(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid, @Field("Val_Contestid") String Val_Contestid );

    @POST(Constants.V3_USER_FETCH)
    @FormUrlEncoded
    Call<WalletUsageLimitModel>   getUserWalletLimitTournament(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid, @Field("Val_Tournamentid") String Val_Tournamentid );

    @POST(Constants.UPDATE_PROFILE)
    @FormUrlEncoded
    Call<GeneralResponseModel>   sendEmailInvoice(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid, @Field("Val_Transactionid") String Val_Transactionid );

    @POST(Constants.CHANGE_PASS)
    @FormUrlEncoded
    Call<GeneralResponseModel>   changePassword(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid, @Field("Val_Ucpassword") String Val_Transactionid,
                                                @Field("Val_Upassword") String Val_Upassword );

    @POST(Constants.UPDATE_MOBILE)
    @FormUrlEncoded
    Call<GeneralResponseModel>   verifyEmail(@Field("Action") String Action,@Field("Val_Uemailaddress") String Val_Uemailaddress, @Field("Val_Userid") String Val_Userid,
                                                    @Field("Val_Type") String Val_Type );

    @POST(Constants.UPDATE_PROFILE_V3)
    @FormUrlEncoded
    Call<GeneralResponseModel>   withdrawRequest(@Field("Action") String Action, @Field("Val_Userid") String Val_Userid,
                                             @Field("Val_Amount") String Val_Amount,@Field("Val_Mode") String Val_Mode );

    /*@Multipart
    @POST(Constants.JOIN_CONTESTS)
    Call<GeneralResponseModel>   UploadGameSS(@Part("Action") RequestBody Action, @Part("Val_Userid") RequestBody Val_Userid,
                                              @Part("Val_Contestid") RequestBody Val_Contestid, @Part("Val_Contestuserid") RequestBody Val_Contestuserid,
                                              @Part("Val_CUscreenshotimage") RequestBody Val_CUscreenshotimage );*/

    @POST(Constants.OTP)
    Call<JsonObject> validatePaytm(@Query("phone") String phone, @Query("scope") String scope, @Query("responseType") String responseType);


    @GET()
    Call<JsonElement> getJsonFile(@Url String url);

    @POST()
    Call<JsonElement> checkAppVersion(@Field("Action") String Action);

    @POST(Constants.UPDATE_MOBILE)
    @FormUrlEncoded
    Call<GeneralResponseModel> sendToken(@Field("Action") String action, @Field("Val_Userid") String token,@Field("Val_Udevicetype") String Val_Udevicetype,@Field("Val_Ufirebasetoken") String Val_Ufirebasetoken);

    // Game Type Games Data API Interface
    @Headers({"Content-Type: application/json"})
    @POST(Constants.USER_FETCH_RAW)
    Call<GameTypeGamesDataModel> getGameTypeGamesData(@Body RequestBody body);

    // Add Change Username API Interface
    @Headers({"Content-Type: application/json"})
    @POST(Constants.USER_DETAILS)
    Call<AddChangeUsernameModel> addChangeUsername(@Body RequestBody body);

    // Get Games API Interface
    @Headers({"Content-Type: application/json"})
    @POST(Constants.GAME_FETCH_RAW)
    Call<GetGamesModel> getGames(@Body RequestBody body);

    // Add Streamer API Interface
    @Headers({"Content-Type: application/json"})
    @POST(Constants.STREAMER_DETAILS)
    Call<AddStreamerModel> addStreamer(@Body RequestBody body);

    // Get Streamer Status API Interface
    @Headers({"Content-Type: application/json"})
    @POST(Constants.STREAMER_FETCH)
    Call<GetStreamerStatusModel> getStreamerStatus(@Body RequestBody body);

    // Get Host Stream Videos API Interface
    @Headers({"Content-Type: application/json"})
    @POST(Constants.GENERAL_FETCH_RAW)
    Call<HostStreamVideosModel> getHostStreamVideos(@Body RequestBody body);

    // Add Videos API Interface
    @Headers({"Content-Type: application/json"})
    @POST(Constants.STREAMER_DETAILS)
    Call<AddVideosModel> addVideos(@Body RequestBody body);

    // Get Customer Care API Interface
    @Headers({"Content-Type: application/json"})
    @POST(Constants.GENERAL_FETCH_RAW)
    Call<CustomerCareModel> getCustomerCare(@Body RequestBody body);

    // App Maintenance Status API Interface
    @Headers({"Content-Type: application/json"})
    @POST(Constants.APP_MAINTENANCE_STATUS_API)
    Call<AppMaintenanceStatusModel> getAppMaintenanceStatus();

    // GamerJi Pro Popup API Interface
    @POST(Constants.V3_DASHBOARD)
    @FormUrlEncoded
    Call<GamerjiProPopupModel> getGamerJiProPopup(@Field(WebFields.GAMERJI_PRO_POPUP.REQUEST_ACTION) String Action, @Field(WebFields.GAMERJI_PRO_POPUP.REQUEST_VAL_USER_ID) String Val_Userid);

    // GamerJi Pro Popup API Interface
    @POST(Constants.V3_DASHBOARD)
    @FormUrlEncoded
    Call<DashboardQuickGamesModel> getDashboardQuickGames(@Field(WebFields.DASHBOARD_GET_QUICK_GAMES.REQUEST_ACTION) String Action, @Field(WebFields.DASHBOARD_GET_QUICK_GAMES.REQUEST_VAL_USER_ID) String Val_Userid);

    // Quick Game Types API Interface
    @POST(Constants.H5_GAMES_FETCH)
    @FormUrlEncoded
    Call<QuickGameTypesModel> getQuickGameTypes(@Field(WebFields.QUICK_GAME_TYPES.REQUEST_ACTION) String Action, @Field(WebFields.QUICK_GAME_TYPES.REQUEST_VAL_USER_ID) String Val_Userid);

    // Quick Game Sub Types API Interface
    @POST(Constants.H5_GAMES_FETCH)
    @FormUrlEncoded
    Call<QuickGameSubTypesModel> getQuickGameSubTypes(@Field(WebFields.QUICK_GAME_SUB_TYPES.REQUEST_ACTION) String Action, @Field(WebFields.QUICK_GAME_SUB_TYPES.REQUEST_VAL_USER_ID) String Val_Userid,
                                                      @Field(WebFields.QUICK_GAME_SUB_TYPES.REQUEST_CATEGORY_ID) String Val_Categoryid, @Field(WebFields.QUICK_GAME_SUB_TYPES.REQUEST_VAL_PAGE) String Val_Page,
                                                      @Field(WebFields.QUICK_GAME_SUB_TYPES.REQUEST_VAL_LIMIT) String Val_Limit);

    // Quick Game - Join Game Popup API Interface
    @POST(Constants.V3_USER_FETCH)
    @FormUrlEncoded
    Call<QuickGameJoinGamePopupModel> quickGameJoinGamePopup(@Field(WebFields.QUICK_GAME_JOIN_GAME_POPUP.REQUEST_ACTION) String Action, @Field(WebFields.QUICK_GAME_JOIN_GAME_POPUP.REQUEST_VAL_USER_ID) String Val_Userid,
                                                             @Field(WebFields.QUICK_GAME_JOIN_GAME_POPUP.REQUEST_VAL_H5_GAME_ID) String Val_H5Gameid);

    // Quick Game - Join Game API Interface
    @POST(Constants.H5_GAMES_DETAILS)
    @FormUrlEncoded
    Call<QuickGameJoinGameModel> quickGameJoinGame(@Field(WebFields.QUICK_GAME_JOIN_GAME.REQUEST_ACTION) String Action, @Field(WebFields.QUICK_GAME_JOIN_GAME.REQUEST_VAL_USER_ID) String Val_Userid,
                                                   @Field(WebFields.QUICK_GAME_JOIN_GAME.REQUEST_VAL_H5_GAME_ID) String Val_H5Gameid, @Field(WebFields.QUICK_GAME_JOIN_GAME.REQUEST_VAL_H5_CATEGORY_ID) String Val_H5Categoryid);

    // Quick Game - End Game API Interface
    @POST(Constants.H5_GAMES_DETAILS)
    @FormUrlEncoded
    Call<QuickGameEndGameModel> quickGameEndGame(@Field(WebFields.QUICK_GAME_END_GAME.REQUEST_ACTION) String Action, @Field(WebFields.QUICK_GAME_END_GAME.REQUEST_VAL_USER_ID) String Val_Userid,
                                                 @Field(WebFields.QUICK_GAME_END_GAME.REQUEST_VAL_H5_GAME_ID) String Val_H5Gameid, @Field(WebFields.QUICK_GAME_END_GAME.REQUEST_VAL_H5_CATEGORY_ID) String Val_H5Categoryid,
                                                 @Field(WebFields.QUICK_GAME_END_GAME.REQUEST_VAL_JOINED_ID) String Val_Joinedid);


    // Get Popular Videos API Interface
    @POST(Constants.GENERAL_FETCH_V3)
    @FormUrlEncoded
    Call<PopularVideosModel> getPopularVideos(@Field(WebFields.GET_POPULAR_VIDEOS.REQUEST_ACTION) String Action, @Field(WebFields.GET_POPULAR_VIDEOS.REQUEST_VAL_USER_ID) String Val_Userid,
                                              @Field(WebFields.GET_POPULAR_VIDEOS.REQUEST_VAL_TYPE) String Val_Type, @Field(WebFields.GET_POPULAR_VIDEOS.REQUEST_VAL_PAGE) String Val_Page,
                                              @Field(WebFields.GET_POPULAR_VIDEOS.REQUEST_VAL_LIMIT) String Val_Limit);


    // Get View All Popular Videos API Interface
    @POST(Constants.GENERAL_FETCH_V3)
    @FormUrlEncoded
    Call<ViewAllPopularVideosModel> getViewAllPopularVideos(@Field(WebFields.GET_VIEW_ALL_POPULAR_VIDEOS.REQUEST_ACTION) String Action, @Field(WebFields.GET_VIEW_ALL_POPULAR_VIDEOS.REQUEST_VAL_USER_ID) String Val_Userid,
                                                            @Field(WebFields.GET_VIEW_ALL_POPULAR_VIDEOS.REQUEST_VAL_CHANNEL_ID) String Val_YTChannelid, @Field(WebFields.GET_VIEW_ALL_POPULAR_VIDEOS.REQUEST_VAL_TYPE) String Val_Type,
                                                            @Field(WebFields.GET_VIEW_ALL_POPULAR_VIDEOS.REQUEST_VAL_PAGE) String Val_Page, @Field(WebFields.GET_VIEW_ALL_POPULAR_VIDEOS.REQUEST_VAL_LIMIT) String Val_Limit);
}