package com.esports.gamerjipro.RestApis;

public class WebFields {

    // Common Params
    public final static String STATUS = "status";
    public final static String FLAG = "flag";
    public final static String MESSAGE = "message";
    public final static String DATA = "data";

    // Game Type - Get Games Data API Params
    public final static class GAME_TYPE_GET_GAMES_DATA {
        public final static String MODE = "GetUserIGN";

        public final static String REQUEST_ACTION = "Action";
        public final static String REQUEST_VAL_GAME_ID = "Val_Gameid";
        public final static String REQUEST_VAL_USER_ID = "Val_Userid";

        public final static String RESPONSE_USER_GAME_ID = "UserGameID";
        public final static String RESPONSE_GAME_ID = "GameID";
        public final static String RESPONSE_USER_ID = "UserID";
        public final static String RESPONSE_GAME_NAME = "GameName";
        public final static String RESPONSE_UNIQUE_NAME = "UniqueName";
        public final static String RESPONSE_DAYS_LIMIT = "DaysLimit";
        public final static String RESPONSE_LAST_UPDATED_DTTM = "LastUpdatedDttm";
        public final static String RESPONSE_HELP_TEXT = "HelpText";
    }

    // Add Change Username API Params
    public final static class ADD_CHANGE_USERNAME {
        public final static String MODE = "UpdateUserIGN";

        public final static String REQUEST_ACTION = "Action";
        public final static String REQUEST_VAL_USER_GAME_ID = "Val_Usergameid";
        public final static String REQUEST_VAL_GAME_ID = "Val_Gameid";
        public final static String REQUEST_VAL_USER_ID = "Val_Userid";
        public final static String REQUEST_VAL_USER_IGN = "Val_Userign";
    }

    // Get Games API Params
    public final static class GET_GAMES {
        public final static String MODE = "GetGames";

        public final static String REQUEST_ACTION = "Action";
        public final static String REQUEST_VAL_USER_ID = "Val_Userid";
    }

    // Add Streamer API Params
    public final static class ADD_STREAMER {
        public final static String MODE = "AddStreamer";

        public final static String REQUEST_ARR_GAMES = "Val_Games";
        public final static String REQUEST_ARR_PLATFORMS = "Val_Platforms";

        public final static String REQUEST_ACTION = "Action";
        public final static String REQUEST_VAL_USER_ID = "Val_Userid";
        public final static String REQUEST_VAL_NAME = "Val_Name";
        public final static String REQUEST_VAL_COUNTRY_CODE = "Val_Countrycode";
        public final static String REQUEST_VAL_MOBILE_NUMBER = "Val_Mobilenumber";
        public final static String REQUEST_VAL_EMAIL_ADDRESS = "Val_Emailaddress";
        public final static String REQUEST_VAL_GENDER = "Val_Gender";
        public final static String REQUEST_VAL_YOUTUBE_CHANNEL_NAME = "Val_Ytchannelname";
        public final static String REQUEST_VAL_YOUTUBE_CHANNEL_LINK = "Val_Ytchannellink";
        public final static String REQUEST_VAL_OTHER_GAME = "Val_OtherGame";
    }

    // Get Streamer Status API Params
    public final static class GET_STREAMER_STATUS {
        public final static String MODE = "GetStreamerStatus";

        public final static String REQUEST_ACTION = "Action";
        public final static String REQUEST_VAL_USER_ID = "Val_Userid";

        public final static String RESPONSE_REASON = "Reason";
        public final static String RESPONSE_STATUS = "Status";
        public final static String RESPONSE_DISPLAY_STATUS = "DisplayStatus";
    }

    // Get Videos API Params
    public final static class GET_VIDEOS {
        public final static String MODE = "GetYTVideos";

        public final static String REQUEST_ACTION = "Action";
        public final static String REQUEST_VAL_USER_ID = "Val_Userid";
        public final static String REQUEST_VAL_TYPE = "Val_Type";
        public final static String REQUEST_VAL_PAGE = "Val_Page";
        public final static String REQUEST_VAL_LIMIT = "Val_Limit";

        public final static String RESPONSE_CHANNEL_LINK = "ChannelLink";
        public final static String RESPONSE_CHANNEL_LOGO = "ChannelLogo";
        public final static String RESPONSE_CHANNEL_NAME = "ChannelName";
        public final static String RESPONSE_CHANNEL_SUBSCRIBERS = "ChannelSubscribers";

        public final static String REQUEST_ARR_VIDEOS_DATA = "VideosData";
    }

    // Add Streamer API Params
    public final static class ADD_VIDEOS {
        public final static String MODE = "AddVideo";

        public final static String REQUEST_ACTION = "Action";
        public final static String REQUEST_VAL_USER_ID = "Val_Userid";
        public final static String REQUEST_VAL_TITLE = "Val_Title";
        public final static String REQUEST_VAL_LINK = "Val_Link";
    }

    // Get Customer Care API Params
    public final static class GET_CUSTOMER_CARE {
        public final static String MODE = "GetCustomerCare";

        public final static String REQUEST_ACTION = "Action";
        public final static String REQUEST_VAL_USER_ID = "Val_Userid";
    }

    // GamerJi Pro Popup API Params
    public final static class GAMERJI_PRO_POPUP {
        public final static String MODE = "CheckUserAppVersion";

        public final static String REQUEST_ACTION = "Action";
        public final static String REQUEST_VAL_USER_ID = "Val_Userid";
    }

    // GamerJi Pro Popup API Params
    public final static class DASHBOARD_GET_QUICK_GAMES {
        public final static String MODE = "GetH5GStatus";

        public final static String REQUEST_ACTION = "Action";
        public final static String REQUEST_VAL_USER_ID = "Val_Userid";
    }

    // Quick Game Types API Params
    public final static class QUICK_GAME_TYPES {
        public final static String MODE = "GetCategoriesList";

        public final static String REQUEST_ACTION = "Action";
        public final static String REQUEST_VAL_USER_ID = "Val_Userid";
    }

    // Quick Game Sub Types API Params
    public final static class QUICK_GAME_SUB_TYPES {
        public final static String MODE = "GetGamesList";

        public final static String REQUEST_ACTION = "Action";
        public final static String REQUEST_VAL_USER_ID = "Val_Userid";
        public final static String REQUEST_CATEGORY_ID = "Val_Categoryid";
        public final static String REQUEST_VAL_PAGE = "Val_Page";
        public final static String REQUEST_VAL_LIMIT = "Val_Limit";
    }

    // Quick Game - Join Game Popup API Params
    public final static class QUICK_GAME_JOIN_GAME_POPUP {
        public final static String MODE = "GetWalletUsageLimit";

        public final static String REQUEST_ACTION = "Action";
        public final static String REQUEST_VAL_USER_ID = "Val_Userid";
        public final static String REQUEST_VAL_H5_GAME_ID = "Val_H5Gameid";
    }

    // Quick Game - Join Game API Params
    public final static class QUICK_GAME_JOIN_GAME {
        public final static String MODE = "JoinGame";

        public final static String REQUEST_ACTION = "Action";
        public final static String REQUEST_VAL_USER_ID = "Val_Userid";
        public final static String REQUEST_VAL_H5_GAME_ID = "Val_H5Gameid";
        public final static String REQUEST_VAL_H5_CATEGORY_ID = "Val_H5Categoryid";
    }

    // Quick Game - End Game API Params
    public final static class QUICK_GAME_END_GAME {
        public final static String MODE = "EndGame";

        public final static String REQUEST_ACTION = "Action";
        public final static String REQUEST_VAL_USER_ID = "Val_Userid";
        public final static String REQUEST_VAL_H5_GAME_ID = "Val_H5Gameid";
        public final static String REQUEST_VAL_H5_CATEGORY_ID = "Val_H5Categoryid";
        public final static String REQUEST_VAL_JOINED_ID = "Val_Joinedid";
    }

    // Get Popular Videos API Params
    public final static class GET_POPULAR_VIDEOS {
        public final static String MODE = "GetYTChannelsList";

        public final static String REQUEST_ACTION = "Action";
        public final static String REQUEST_VAL_USER_ID = "Val_Userid";
        public final static String REQUEST_VAL_TYPE = "Val_Type";
        public final static String REQUEST_VAL_PAGE = "Val_Page";
        public final static String REQUEST_VAL_LIMIT = "Val_Limit";
    }

    // Get View All Popular Videos API Params
    public final static class GET_VIEW_ALL_POPULAR_VIDEOS {
        public final static String MODE = "GetYTChannelsVideosList";

        public final static String REQUEST_ACTION = "Action";
        public final static String REQUEST_VAL_USER_ID = "Val_Userid";
        public final static String REQUEST_VAL_CHANNEL_ID = "Val_YTChannelid";
        public final static String REQUEST_VAL_TYPE = "Val_Type";
        public final static String REQUEST_VAL_PAGE = "Val_Page";
        public final static String REQUEST_VAL_LIMIT = "Val_Limit";
    }
}
