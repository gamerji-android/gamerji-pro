package com.esports.gamerjipro.RestApis;

import com.esports.gamerjipro.Utils.Constants;
import com.google.gson.JsonElement;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {

    private static Retrofit retrofit = null;

    public static Retrofit getClient()
    {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                                    .addInterceptor(interceptor)
                                    .connectTimeout(60, TimeUnit.SECONDS)
                                    .readTimeout(60,TimeUnit.SECONDS)
                                    .writeTimeout(60,TimeUnit.SECONDS)
                                    .build();


        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_API)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();


        return retrofit;
    }


    public static Retrofit getPaytmClient()
    {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();


        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.PAYTM_API)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();


        return retrofit;
    }



    public static Retrofit HowToPlay()
    {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();


        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.HOW_TO_PLAY_BASE_API)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();


        return retrofit;
    }

    public static APIInterface getAPI() {
        return APIClient.getClient().create(APIInterface.class);
    }


    public static void getJsonFile(String url, OnApiResponseListener<JsonElement> listener) {
        Call<JsonElement> call = getAPI().getJsonFile(url);
        call.enqueue(new APICallBack<>(listener, 0));
    }

}
