package com.esports.gamerjipro.adGyde;

//import com.adgyde.android.AdGyde;

public interface CommonEvents {

    /**
     * Sets the Simple AdGyde Event
     *
     * @param mKey - AdGyde Key Name
     */
    static void setSimpleEvent(String mKey) {
//        AdGyde.onSimpleEvent(mKey);
    }

    /**
     * Sets the Simple AdGyde Event
     *
     * @param mToken - Firebase Token
     */
    static void setFirebaseTokenEvent(String mToken) {
//        AdGyde.onTokenRefresh(mToken);
    }

    /**
     * Sets the Simple AdGyde Event
     *
     * @param mKey - AdGyde Key Name
     * @param mValue - AdGyde Key Value
     */
    static void setCountingEvent(String mKey, String mValue) {
//        Map<String, String> params = new HashMap<String, String>();
//        params.put(mKey, mValue);
//        AdGyde.onCountingEvent(mKey, params);
    }
}