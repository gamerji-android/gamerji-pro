package com.esports.gamerjipro.adGyde;

public class AdGydeConstants {

//    android:name=".adGyde.ExampleApplication"

    // AdGyde Keys
    public static final String KEY_APP_LAUNCH = "AppLaunch";
    public static final String KEY_LOGIN = "Login";

    // AdGyde Values
    public static final String VALUE_LOGIN_VIA_MOBILE = "LoginViaMobile";
    public static final String VALUE_LOGIN_VIA_EMAIL = "LoginViaEmail";
    public static final String VALUE_LOGIN_VIA_FACEBOOK = "LoginViaFacebook";
    public static final String VALUE_LOGIN_VIA_GOOGLE = "LoginViaGoogle";
}
