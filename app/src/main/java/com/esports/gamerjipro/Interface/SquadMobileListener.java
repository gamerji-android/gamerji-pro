package com.esports.gamerjipro.Interface;

import com.esports.gamerjipro.Model.SquadModel;

public interface SquadMobileListener
{
    void onSquadMobileEnteredListener(SquadModel squadModel, int position, String mobile_number, String country_code);
}
