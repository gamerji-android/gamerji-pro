package com.esports.gamerjipro.Interface;

import com.esports.gamerjipro.Model.ReportsDataModel;
import com.esports.gamerjipro.Model.TournamentTimeSlotModel;

public interface TournamentTimeSelectedListener
{
    public void onTournamentTimeSelectedListener(TournamentTimeSlotModel.Data.TimeSlots timeSlots);
    public void onReportSelected(ReportsDataModel.ReportsData reportsData);
}
