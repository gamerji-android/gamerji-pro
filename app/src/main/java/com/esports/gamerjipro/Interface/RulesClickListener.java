package com.esports.gamerjipro.Interface;

import com.esports.gamerjipro.Model.MyContestModel;
import com.esports.gamerjipro.Model.TournamentTypeModel;

public interface RulesClickListener {
    void OnRulesClickListener(TournamentTypeModel.Data.TypesData.TournamentsData tournamentsData);

    void OnMyContestRulesClickListener(MyContestModel.Data.ContestsData contestsData);
}
