package com.esports.gamerjipro.Interface;

public interface FeedbackReasonListener
{
    void onFeedbackReasonClick(String ROptionID, boolean checked);
}
