package com.esports.gamerjipro.Interface;

import com.esports.gamerjipro.Model.ContestTypeModelNew;
import com.esports.gamerjipro.Model.ContestTypesModel;
import com.esports.gamerjipro.Model.MyContestModel;
import com.esports.gamerjipro.Model.TournamentDetailModel;
import com.esports.gamerjipro.Model.TournamentTypeModel;

public interface WinnerClickListener {
    void onWinnersClick(ContestTypesModel.Data.TypesData.ContestsData contestsData);

    void onWinnersClick(ContestTypeModelNew.Data.ContestsData contestsData);

    void onMyContestWinnerClick(MyContestModel.Data.ContestsData contestsData);

    void onTournamentWinnerClick(TournamentTypeModel.Data.TypesData.TournamentsData tournamentsData);

    void onTournamentContestWinnersClickListener(TournamentDetailModel.TournamentData.ContestsData contestsData);
}
