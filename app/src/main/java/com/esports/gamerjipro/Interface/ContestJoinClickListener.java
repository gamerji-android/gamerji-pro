package com.esports.gamerjipro.Interface;

import com.esports.gamerjipro.Model.ContestTypeModelNew;
import com.esports.gamerjipro.Model.ContestTypesModel;

public interface ContestJoinClickListener
{
    public void onContestJoinClickLitener(ContestTypesModel.Data.TypesData.ContestsData contestsData);
    public void onContestJoinClickLitener(ContestTypeModelNew.Data.ContestsData contestsData);
}
