package com.esports.gamerjipro.Interface;

import com.esports.gamerjipro.Model.GameUsernameModel;
import com.esports.gamerjipro.Model.GamerProfileModel;

public interface ProfileGameInfoClickListener {
    void OnGameInfoClickListener(String mValue);

    void OnOtherGameInfoClickListener(GameUsernameModel gameUsernameModel);

    void OnBadgeClickListener(GamerProfileModel.Data.LevelsData.LevelData badgesList);
}
