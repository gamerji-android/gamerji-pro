package com.esports.gamerjipro.Interface;

public interface SetOnDownloadProgressListner
{
    void OnDownloadStarted();

    void OnProgressUpdate(Integer mValue);

    void OnDownloadComplete(String mOutPutPath);

    void OnErrorOccure(String mMessage);
}
