package com.esports.gamerjipro.Interface;

import com.esports.gamerjipro.Model.TournamentTypeModel;

public interface OnTournamentShowTimeListener
{
    public void OnTournamentTimeSelected(TournamentTypeModel.Data.TypesData.TournamentsData tournamentsData);
}
