package com.esports.gamerjipro.Interface;

public interface RemovePlayerListener
{
    void onPlayerRemoveListener(int position);
}
