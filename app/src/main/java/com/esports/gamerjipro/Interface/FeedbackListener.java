package com.esports.gamerjipro.Interface;

import com.esports.gamerjipro.Model.ContestDetailsModel;

public interface FeedbackListener
{
    void onFeedbackClickListener(ContestDetailsModel.ContestsData.RatingsData ratingsData);
}
