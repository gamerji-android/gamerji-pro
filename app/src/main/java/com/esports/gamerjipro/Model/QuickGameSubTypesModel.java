package com.esports.gamerjipro.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QuickGameSubTypesModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("IsLast")
        @Expose
        private Boolean isLast;
        @SerializedName("GamesCount")
        @Expose
        private Integer gamesCount;
        @SerializedName("GamesData")
        @Expose
        private List<GamesData> gamesData = null;

        public Boolean getIsLast() {
            return isLast;
        }

        public void setIsLast(Boolean isLast) {
            this.isLast = isLast;
        }

        public Integer getGamesCount() {
            return gamesCount;
        }

        public void setGamesCount(Integer gamesCount) {
            this.gamesCount = gamesCount;
        }

        public List<GamesData> getGamesData() {
            return gamesData;
        }

        public void setGamesData(List<GamesData> gamesData) {
            this.gamesData = gamesData;
        }

        public class GamesData {

            @SerializedName("GameID")
            @Expose
            private String gameID;
            @SerializedName("Name")
            @Expose
            private String name;
            @SerializedName("URL")
            @Expose
            private String url;
            @SerializedName("ThumbImage")
            @Expose
            private String thumbImage;
            @SerializedName("DisplayMode")
            @Expose
            private Integer displayMode;
            @SerializedName("PlayedCount")
            @Expose
            private String playedCount;

            public String getGameID() {
                return gameID;
            }

            public void setGameID(String gameID) {
                this.gameID = gameID;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public void setPlayedCount(String playedCount) {
                this.playedCount = playedCount;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getThumbImage() {
                return thumbImage;
            }

            public void setThumbImage(String thumbImage) {
                this.thumbImage = thumbImage;
            }

            public Integer getDisplayMode() {
                return displayMode;
            }

            public void setDisplayMode(Integer displayMode) {
                this.displayMode = displayMode;
            }

            public String getPlayedCount() {
                return playedCount;
            }
        }
    }
}
