package com.esports.gamerjipro.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PopularVideosModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("IsLast")
        @Expose
        private Boolean isLast;
        @SerializedName("YTChannelsCount")
        @Expose
        private Integer yTChannelsCount;
        @SerializedName("YTChannelsData")
        @Expose
        private List<YTChannelsData> yTChannelsData = null;
        @SerializedName("ChannelName")
        @Expose
        private String channelName;
        @SerializedName("ChannelLogo")
        @Expose
        private String channelLogo;
        @SerializedName("ChannelSubscribers")
        @Expose
        private String channelSubscribers;
        @SerializedName("ChannelLink")
        @Expose
        private String channelLink;

        public Boolean getIsLast() {
            return isLast;
        }

        public void setIsLast(Boolean isLast) {
            this.isLast = isLast;
        }

        public Integer getYTChannelsCount() {
            return yTChannelsCount;
        }

        public void setYTChannelsCount(Integer yTChannelsCount) {
            this.yTChannelsCount = yTChannelsCount;
        }

        public List<YTChannelsData> getYTChannelsData() {
            return yTChannelsData;
        }

        public void setYTChannelsData(List<YTChannelsData> yTChannelsData) {
            this.yTChannelsData = yTChannelsData;
        }

        public String getChannelName() {
            return channelName;
        }

        public void setChannelName(String channelName) {
            this.channelName = channelName;
        }

        public String getChannelLogo() {
            return channelLogo;
        }

        public void setChannelLogo(String channelLogo) {
            this.channelLogo = channelLogo;
        }

        public String getChannelSubscribers() {
            return channelSubscribers;
        }

        public void setChannelSubscribers(String channelSubscribers) {
            this.channelSubscribers = channelSubscribers;
        }

        public String getChannelLink() {
            return channelLink;
        }

        public void setChannelLink(String channelLink) {
            this.channelLink = channelLink;
        }

        public class YTChannelsData {

            @SerializedName("YTChannelID")
            @Expose
            private String yTChannelID;
            @SerializedName("YTChannelName")
            @Expose
            private String yTChannelName;
            @SerializedName("YTChannelLink")
            @Expose
            private String yTChannelLink;
            @SerializedName("TotalVideosCount")
            @Expose
            private Integer totalVideosCount;
            @SerializedName("VideosCount")
            @Expose
            private Integer videosCount;
            @SerializedName("VideosData")
            @Expose
            private List<VideosData> videosData = null;

            public String getYTChannelID() {
                return yTChannelID;
            }

            public void setYTChannelID(String yTChannelID) {
                this.yTChannelID = yTChannelID;
            }

            public String getYTChannelName() {
                return yTChannelName;
            }

            public void setYTChannelName(String yTChannelName) {
                this.yTChannelName = yTChannelName;
            }

            public String getYTChannelLink() {
                return yTChannelLink;
            }

            public void setYTChannelLink(String yTChannelLink) {
                this.yTChannelLink = yTChannelLink;
            }

            public Integer getTotalVideosCount() {
                return totalVideosCount;
            }

            public void setTotalVideosCount(Integer totalVideosCount) {
                this.totalVideosCount = totalVideosCount;
            }

            public Integer getVideosCount() {
                return videosCount;
            }

            public void setVideosCount(Integer videosCount) {
                this.videosCount = videosCount;
            }

            public List<VideosData> getVideosData() {
                return videosData;
            }

            public void setVideosData(List<VideosData> videosData) {
                this.videosData = videosData;
            }

            public class VideosData {

                @SerializedName("VideoID")
                @Expose
                private String videoID;
                @SerializedName("Title")
                @Expose
                private String title;
                @SerializedName("YoutubeLink")
                @Expose
                private String youtubeLink;

                public String getVideoID() {
                    return videoID;
                }

                public void setVideoID(String videoID) {
                    this.videoID = videoID;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getYoutubeLink() {
                    return youtubeLink;
                }

                public void setYoutubeLink(String youtubeLink) {
                    this.youtubeLink = youtubeLink;
                }
            }
        }
    }
}