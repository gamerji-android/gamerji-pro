package com.esports.gamerjipro.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StatisticsModel
{
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("ContestsCreated")
        @Expose
        private String contestsCreated;
        @SerializedName("WinnigDistributions")
        @Expose
        private String winnigDistributions;
        @SerializedName("PlayersJoined")
        @Expose
        private String playersJoined;

        public String getContestsCreated() {
            return contestsCreated;
        }

        public void setContestsCreated(String contestsCreated) {
            this.contestsCreated = contestsCreated;
        }

        public String getWinnigDistributions() {
            return winnigDistributions;
        }

        public void setWinnigDistributions(String winnigDistributions) {
            this.winnigDistributions = winnigDistributions;
        }

        public String getPlayersJoined() {
            return playersJoined;
        }

        public void setPlayersJoined(String playersJoined) {
            this.playersJoined = playersJoined;
        }
    }
}
