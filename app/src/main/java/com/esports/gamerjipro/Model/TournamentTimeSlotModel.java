package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TournamentTimeSlotModel
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data DataClass;

    public  class Data
    {

        @SerializedName("TournamentID")
        public String TournamentID;

        @SerializedName("InfoText")
        public String InfoText;

        @SerializedName("TimeSlots")
        public ArrayList<TimeSlots> timeSlotsArrayList= new ArrayList<>();

        public  class TimeSlots
        {
            @SerializedName("ContestID")
            public String ContestID;

            @SerializedName("ContestTime")
            public String ContestTime;
        }
    }
}
