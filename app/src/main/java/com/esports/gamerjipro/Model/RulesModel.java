package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

public class RulesModel
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data Data;

    public class Data
    {
        @SerializedName("TournamentID")
        public String TournamentID;

        @SerializedName("Rules")
        public String Rules;

    }
}
