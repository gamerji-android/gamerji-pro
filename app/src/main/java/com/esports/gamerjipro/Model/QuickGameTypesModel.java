package com.esports.gamerjipro.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QuickGameTypesModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("CategoriesCount")
        @Expose
        private Integer categoriesCount;
        @SerializedName("CategoriesData")
        @Expose
        private List<CategoriesData> categoriesData = null;

        public Integer getCategoriesCount() {
            return categoriesCount;
        }

        public void setCategoriesCount(Integer categoriesCount) {
            this.categoriesCount = categoriesCount;
        }

        public List<CategoriesData> getCategoriesData() {
            return categoriesData;
        }

        public void setCategoriesData(List<CategoriesData> categoriesData) {
            this.categoriesData = categoriesData;
        }

        public class CategoriesData {

            @SerializedName("CategoryID")
            @Expose
            private String categoryID;
            @SerializedName("Name")
            @Expose
            private String name;
            @SerializedName("FeaturedImage")
            @Expose
            private String featuredImage;

            public String getCategoryID() {
                return categoryID;
            }

            public void setCategoryID(String categoryID) {
                this.categoryID = categoryID;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getFeaturedImage() {
                return featuredImage;
            }

            public void setFeaturedImage(String featuredImage) {
                this.featuredImage = featuredImage;
            }
        }
    }
}
