package com.esports.gamerjipro.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReadNotificationModel
{
    @SerializedName("status")
    public int status;

    @SerializedName("message")
    public String message;
}
