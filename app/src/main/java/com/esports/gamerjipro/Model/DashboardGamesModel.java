package com.esports.gamerjipro.Model;

import java.io.Serializable;

public class DashboardGamesModel implements Serializable {

    public String GameID;
    public String Name;
    public String FeaturedImage;

    public String getGameID() {
        return GameID;
    }

    public void setGameID(String gameID) {
        GameID = gameID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getFeaturedImage() {
        return FeaturedImage;
    }

    public void setFeaturedImage(String featuredImage) {
        FeaturedImage = featuredImage;
    }
}
