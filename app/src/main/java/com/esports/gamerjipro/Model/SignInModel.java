package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SignInModel
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public UserData userDataClass;

    public class UserData
    {
        @SerializedName("UserID")
        public String UserID;

        @SerializedName("FullName")
        public String FullName;

        @SerializedName("FirstName")
        public String FirstName;

        @SerializedName("LastName")
        public String LastName;

        @SerializedName("TeamName")
        public String TeamName;

        @SerializedName("CountryCode")
        public String CountryCode;

        @SerializedName("MobileNumber")
        public String MobileNumber;

        @SerializedName("EmailAddress")
        public String EmailAddress;

        @SerializedName("CanPlayGames")
        public String CanPlayGames;

        @SerializedName("BirthDate")
        public String BirthDate;

        @SerializedName("State")
        public String State;

        @SerializedName("StateID")
        public String StateID;

        @SerializedName("ReferralCode")
        public String ReferralCode;

        @SerializedName("GamesData")
        public GamesData GamesData;


        @SerializedName("OTPCode")
        public String OTPCode;

        @SerializedName("OTP")
        public String OTP;

        @SerializedName("ProfileImage")
        public String ProfileImage;

        @SerializedName("LevelIcon")
        public String LevelIcon;

        public class GamesData
        {
            @SerializedName("GamesCount")
            public String GamesCount;


            @SerializedName("GamesData")
            public ArrayList<GameData> gamesDataArrayList;

            public  class GameData
            {
                @SerializedName("UserGameID")
                public String UserGameID;

                @SerializedName("GameID")
                public String GameID;

                @SerializedName("UniqueName")
                public String UniqueName;

                @SerializedName("Status")
                public String Status;
            }
        }
    }
}
