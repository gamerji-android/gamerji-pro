package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SearchModel
{

    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data DataClass;

    public class Data
    {
        @SerializedName("IsLast")
        public String IsLast;

        @SerializedName("UsersCount")
        public String UsersCount;

        @SerializedName("UsersData")
        public ArrayList<Data.UserLevelsData> userLevelsData;

        public class UserLevelsData
        {
            @SerializedName("UserID")
            public String UserID;

            @SerializedName("Name")
            public String Name;

            @SerializedName("MobileNumber")
            public String MobileNumber;

            @SerializedName("FeaturedIcon")
            public String FeaturedIcon;

            @SerializedName("Points")
            public String Points;

            @SerializedName("LevelNumber")
            public String LevelNumber;

            @SerializedName("LevelName")
            public String LevelName;

            @SerializedName("Rank")
            public String Rank;

        }
    }
}
