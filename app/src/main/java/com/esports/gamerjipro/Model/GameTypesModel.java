package com.esports.gamerjipro.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GameTypesModel implements Serializable {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("TypesCount")
        @Expose
        private String typesCount;
        @SerializedName("TypesData")
        @Expose
        private List<TypesData> typesData = null;

        public String getTypesCount() {
            return typesCount;
        }

        public void setTypesCount(String typesCount) {
            this.typesCount = typesCount;
        }

        public List<TypesData> getTypesData() {
            return typesData;
        }

        public void setTypesData(List<TypesData> typesData) {
            this.typesData = typesData;
        }

        public class TypesData implements Serializable {

            @SerializedName("TypeID")
            @Expose
            private String typeID;
            @SerializedName("GameID")
            @Expose
            private String gameID;
            @SerializedName("Name")
            @Expose
            private String name;
            @SerializedName("FeaturedImage")
            @Expose
            private String featuredImage;
            @SerializedName("IsTournament")
            @Expose
            private String isTournament;

            public String getTypeID() {
                return typeID;
            }

            public void setTypeID(String typeID) {
                this.typeID = typeID;
            }

            public String getGameID() {
                return gameID;
            }

            public void setGameID(String gameID) {
                this.gameID = gameID;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getFeaturedImage() {
                return featuredImage;
            }

            public void setFeaturedImage(String featuredImage) {
                this.featuredImage = featuredImage;
            }

            public String getIsTournament() {
                return isTournament;
            }

            public void setIsTournament(String isTournament) {
                this.isTournament = isTournament;
            }
        }
    }
}
