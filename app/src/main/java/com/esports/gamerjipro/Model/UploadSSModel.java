package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

public class UploadSSModel
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data data;

    public class Data
    {
        @SerializedName("ContestUserID")
        public String ContestUserID;

        @SerializedName("UserID")
        public String UserID;

        @SerializedName("ScreenshotFlag")
        public String ScreenshotFlag;

        @SerializedName("ScreenshotURL")
        public String ScreenshotURL;
    }
}
