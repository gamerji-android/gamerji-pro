package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TournamentPlayersModel
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data Data;

    public class Data
    {
        @SerializedName("IsLast")
        public boolean IsLast;

        @SerializedName("PlayersCount")
        public String PlayersCount;

        @SerializedName("PlayersData")
        public ArrayList<TournamentDetailModel.TournamentData.UsersData> userDataArrayList;
    }
}
