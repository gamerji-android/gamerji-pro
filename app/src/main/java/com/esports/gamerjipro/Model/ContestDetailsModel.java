package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ContestDetailsModel implements Serializable
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public ContestsData contestsData;

    public  class ContestsData implements Serializable
    {
        @SerializedName("ContestID")
        public String ContestID;

        @SerializedName("CaptainID")
        public String CaptainID;

        @SerializedName("GameID")
        public String GameID;

        @SerializedName("GameTypeID")
        public String GameTypeID;

        @SerializedName("Date")
        public String Date;

        @SerializedName("Time")
        public String Time;

        @SerializedName("Map_Length")
        public String Map_Length;

        @SerializedName("Map_LengthTitle")
        public String Map_LengthTitle;

        @SerializedName("Perspective_LevelCap")
        public String Perspective_LevelCap;

        @SerializedName("Perspective_LevelCapTitle")
        public String Perspective_LevelCapTitle;

        @SerializedName("WinningAmount")
        public String WinningAmount;

        @SerializedName("WinnersCount")
        public String WinnersCount;

        @SerializedName("PerKill_MaxLoses")
        public String PerKill_MaxLoses;

        @SerializedName("PerKill_MaxLosesCurrency")
        public boolean PerKill_MaxLosesCurrency;

        @SerializedName("PerKill_MaxLosesTitle")
        public String PerKill_MaxLosesTitle;

        @SerializedName("EntryFee")
        public String EntryFee;

        @SerializedName("TotalSpots")
        public String TotalSpots;

        @SerializedName("JoinedSpots")
        public String JoinedSpots;

        @SerializedName("RoomID")
        public String RoomID;

        @SerializedName("RoomPassword")
        public String RoomPassword;

        @SerializedName("RoomColumn")
        public String RoomColumn;

        @SerializedName("Rules")
        public String Rules;

        @SerializedName("UsersCount")
        public String UsersCount;

        @SerializedName("UsersData")
        public ArrayList<UsersData> usersDataArrayList ;

        @SerializedName("CurrentUserData")
        public CurrentUserData CurrentUserData ;

        @SerializedName("RatingsCount")
        public String RatingsCount;

        @SerializedName("RatingsData")
        public ArrayList<RatingsData> ratingsDataArrayList ;

        @SerializedName("RatingsComment")
        public String RatingsComment;

        @SerializedName("FacebookLink")
        public String FacebookLink;

        @SerializedName("InstagramLink")
        public String InstagramLink;

        @SerializedName("TelegramLink")
        public String TelegramLink;

        @SerializedName("ChannelLink")
        public String ChannelLink;

        @SerializedName("DiscordLink")
        public String DiscordLink;

        @SerializedName("Joined")
        public String Joined;

        @SerializedName("ConfirmStatus")
        public String ConfirmStatus;

        @SerializedName("Status")
        public String Status;

        @SerializedName("AdsCount")
        public int AdsCount;

        @SerializedName("AdsData")
        public ArrayList<AdsDataModel> adsDataArrayList;

        @SerializedName("RatingEnabled")
        public boolean RatingEnabled;

        @SerializedName("ReportEnabled")
        public boolean ReportEnabled;

        @SerializedName("ReportsData")
        public ArrayList<ReportsDataModel.ReportsData> ReportsData;

        @SerializedName("RatingSubmitted")
        public boolean RatingSubmitted;

        @SerializedName("RatingSubmittedValue")
        public String RatingSubmittedValue;

        @SerializedName("ReviewSubmitted")
        public boolean ReviewSubmitted;

        @SerializedName("ReviewSubmittedValue")
        public String ReviewSubmittedValue;

        public class CurrentUserData implements Serializable {
            @SerializedName("ContestUserID")
            public String ContestUserID;

            @SerializedName("UserID")
            public String UserID;

            @SerializedName("UserProfileIcon")
            public String UserProfileIcon;

            @SerializedName("MobileNumber")
            public String MobileNumber;

            @SerializedName("Name")
            public String Name;

            @SerializedName("Kills")
            public String Kills;

            @SerializedName("Rank")
            public String Rank;

            @SerializedName("WinningAmount")
            public String WinningAmount;

            @SerializedName("ScreenshotFlag")
            public boolean ScreenshotFlag;

            @SerializedName("ScreenshotURL")
            public String ScreenshotURL;
        }

        public  class UsersData
        {
            @SerializedName("ContestUserID")
            public String ContestUserID;

            @SerializedName("UserID")
            public String UserID;

            @SerializedName("UserProfileIcon")
            public String UserProfileIcon;

            @SerializedName("Kills")
            public String Kills;

            @SerializedName("MobileNumber")
            public String MobileNumber;

            @SerializedName("Name")
            public String Name;

            @SerializedName("Rank")
            public String Rank;

            @SerializedName("ScreenshotFlag")
            public boolean ScreenshotFlag;

            @SerializedName("ScreenshotURL")
            public String ScreenshotURL;

            @SerializedName("WinningAmount")
            public String WinningAmount;
        }

        public class RatingsData
        {
            @SerializedName("RatingID")
            public String RatingID;

            @SerializedName("RatingText")
            public String RatingText;

            @SerializedName("FeaturedImage")
            public String FeaturedImage;

            @SerializedName("OptionsCount")
            public String OptionsCount;

            @SerializedName("OptionsData")
            public ArrayList<OptionsData> optionsDataArrayList;

            public class OptionsData
            {
                @SerializedName("ROptionID")
                public String ROptionID;

                @SerializedName("OptionText")
                public String OptionText;
            }
        }
    }
}
