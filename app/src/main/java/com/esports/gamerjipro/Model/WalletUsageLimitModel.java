package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

public class WalletUsageLimitModel
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data data;

    public class Data
    {
        @SerializedName("UserID")
        public String UserID;

        @SerializedName("WalletBalance")
        public String WalletBalance;

        @SerializedName("EntryFee")
        public String EntryFee;

        @SerializedName("CashBalance")
        public String CashBalance;

        @SerializedName("ToPay")
        public String ToPay;

        @SerializedName("JoinFlag")
        public boolean JoinFlag;

        @SerializedName("JoinButtonFlag")
        public String JoinButtonFlag;

    }
}
