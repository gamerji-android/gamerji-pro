package com.esports.gamerjipro.Model;

public class NotificationModelLocalModelInside {
    int notification_id, notify_to, notification_type;
    String notification_from, created_date, message;
    long created_timestamp;
    int league_id;

    public NotificationModelLocalModelInside(int notification_id, String notification_from, int notify_to, int notification_type, String message, String created_date, long created_timestamp, int league_id) {
        this.notification_id = notification_id;
        this.notification_from = notification_from;
        this.notify_to = notify_to;
        this.notification_type = notification_type;
        this.message = message;
        this.created_date = created_date;
        this.created_timestamp = created_timestamp;
        this.league_id = league_id;
    }

    public int getLeague_id() {
        return league_id;
    }

    public void setLeague_id(int league_id) {
        this.league_id = league_id;
    }

    public int getNotification_id() {
        return notification_id;
    }

    public void setNotification_id(int notification_id) {
        this.notification_id = notification_id;
    }

    public int getNotify_to() {
        return notify_to;
    }

    public void setNotify_to(int notify_to) {
        this.notify_to = notify_to;
    }

    public int getNotification_type() {
        return notification_type;
    }

    public void setNotification_type(int notification_type) {
        this.notification_type = notification_type;
    }

    public String getNotification_from() {
        return notification_from;
    }

    public void setNotification_from(String notification_from) {
        this.notification_from = notification_from;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getCreated_timestamp() {
        return created_timestamp;
    }

    public void setCreated_timestamp(long created_timestamp) {
        this.created_timestamp = created_timestamp;
    }
}