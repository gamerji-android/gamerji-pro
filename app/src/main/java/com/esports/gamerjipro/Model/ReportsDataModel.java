package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ReportsDataModel
{
    @SerializedName("status")
    public String status;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public ArrayList<ReportsData> reportsData;

    public static class ReportsData
    {
        @SerializedName("ReportID")
        public String ReportID;

        @SerializedName("ReportTitle")
        public String ReportTitle;

    }
}
