package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class JoinedContestTimeSlotsModel
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data TimeData;

    public class Data
    {
        @SerializedName("TimeSlotsData")
        public ArrayList<TimeSlotsData> timeSlotsDataArrayList;

        @SerializedName("TimeSlotsCount")
        public String TimeSlotsCount;

        public class TimeSlotsData
        {
            @SerializedName("ContestID")
            public String ContestID;

            @SerializedName("Time")
            public String Time;
        }
    }
}
