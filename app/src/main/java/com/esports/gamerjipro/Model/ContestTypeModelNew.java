package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ContestTypeModelNew
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data DataClass;

    public  class Data
    {
        @SerializedName("IsLast")
        public boolean IsLast;

        @SerializedName("FeaturedImage")
        public String FeaturedImage;

        @SerializedName("ContestsCount")
        public String ContestsCount;

        @SerializedName("ContestsData")
        public ArrayList<ContestsData> contestsDataArrayList;

        public  class ContestsData
        {
            @SerializedName("TypeID")
            public String TypeID;

            @SerializedName("TypeName")
            public String TypeName;

            @SerializedName("TypeFeaturedImage")
            public String TypeFeaturedImage;

            @SerializedName("ContestID")
            public String ContestID;

            @SerializedName("GameID")
            public String GameID;

            @SerializedName("GameTypeID")
            public String GameTypeID;

            @SerializedName("Date")
            public String Date;

            @SerializedName("Time")
            public String Time;

            @SerializedName("Map_Length")
            public String Map_Length;

            @SerializedName("Perspective_LevelCap")
            public String Perspective_LevelCap;

            @SerializedName("Map_LengthTitle")
            public String Map_LengthTitle;

            @SerializedName("Perspective_LevelCapTitle")
            public String Perspective_LevelCapTitle;

            @SerializedName("WinningAmount")
            public String WinningAmount;

            @SerializedName("PerKill_MaxLosesTitle")
            public String PerKill_MaxLosesTitle;

            @SerializedName("PerKill_MaxLosesCurrency")
            public boolean PerKill_MaxLosesCurrency;

            @SerializedName("WinnersCount")
            public String WinnersCount;

            @SerializedName("PerKill_MaxLoses")
            public String PerKill_MaxLoses;

            @SerializedName("EntryFee")
            public String EntryFee;

            @SerializedName("TotalSpots")
            public String TotalSpots;

            @SerializedName("JoinedSpots")
            public String JoinedSpots;

            @SerializedName("CanJoinPlayers")
            public int CanJoinPlayers;

            @SerializedName("CanJoinExtraPlayers")
            public int CanJoinExtraPlayers;

            @SerializedName("Joined")
            public boolean Joined;

            @SerializedName("PrivateStatus")
            public boolean PrivateStatus;

            @SerializedName("ConfirmStatus")
            public String ConfirmStatus;

            @SerializedName("Status")
            public boolean Status;
        }

    }
}
