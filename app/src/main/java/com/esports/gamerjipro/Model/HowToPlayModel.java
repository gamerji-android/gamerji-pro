package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class HowToPlayModel
{
    public ArrayList<HowToPlayResponse> howToPlayResponseArrayList ;

    public class HowToPlayResponse
    {
        @SerializedName("type")
        public String type;

        @SerializedName("data")
        public ArrayList<LanguageSubList> languageSubListArrayList ;

        public class LanguageSubList
        {
            @SerializedName("title")
            public String title;

            @SerializedName("content")
            public String content;
        }
    }
}
