package com.esports.gamerjipro.Model;

public class SectionItemModel
{
    private int type;

    public String getHeader_name() {
        return Header_name;
    }

    public void setHeader_name(String header_name) {
        Header_name = header_name;
    }

    private String Header_name;

    public int getType()
    {
        return type;
    }


    public void setType(int type) {
        this.type = type;
    }

    public ContestTypeModelNew.Data.ContestsData getContestsData()
    {
        return contestsData;
    }

    public void setContestsData(ContestTypeModelNew.Data.ContestsData contestsData)
    {
        this.contestsData = contestsData;
    }

    private ContestTypeModelNew.Data.ContestsData contestsData;

}
