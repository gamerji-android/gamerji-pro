package com.esports.gamerjipro.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ViewAllPopularVideosModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("IsLast")
        @Expose
        private Boolean isLast;
        @SerializedName("VideosCount")
        @Expose
        private Integer videosCount;
        @SerializedName("VideosData")
        @Expose
        private List<VideosData> videosData = null;
        @SerializedName("VideosTotalCount")
        @Expose
        private Integer videosTotalCount;
        @SerializedName("YTChannelName")
        @Expose
        private String yTChannelName;
        @SerializedName("YTChannelLink")
        @Expose
        private String yTChannelLink;

        public Boolean getIsLast() {
            return isLast;
        }

        public void setIsLast(Boolean isLast) {
            this.isLast = isLast;
        }

        public Integer getVideosCount() {
            return videosCount;
        }

        public void setVideosCount(Integer videosCount) {
            this.videosCount = videosCount;
        }

        public List<VideosData> getVideosData() {
            return videosData;
        }

        public void setVideosData(List<VideosData> videosData) {
            this.videosData = videosData;
        }

        public Integer getVideosTotalCount() {
            return videosTotalCount;
        }

        public void setVideosTotalCount(Integer videosTotalCount) {
            this.videosTotalCount = videosTotalCount;
        }

        public String getYTChannelName() {
            return yTChannelName;
        }

        public void setYTChannelName(String yTChannelName) {
            this.yTChannelName = yTChannelName;
        }

        public String getYTChannelLink() {
            return yTChannelLink;
        }

        public void setYTChannelLink(String yTChannelLink) {
            this.yTChannelLink = yTChannelLink;
        }

        public class VideosData {

            @SerializedName("VideoID")
            @Expose
            private String videoID;
            @SerializedName("Title")
            @Expose
            private String title;
            @SerializedName("YoutubeLink")
            @Expose
            private String youtubeLink;
            @SerializedName("Views")
            @Expose
            private String views;
            @SerializedName("Likes")
            @Expose
            private String likes;

            public String getVideoID() {
                return videoID;
            }

            public void setVideoID(String videoID) {
                this.videoID = videoID;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getYoutubeLink() {
                return youtubeLink;
            }

            public void setYoutubeLink(String youtubeLink) {
                this.youtubeLink = youtubeLink;
            }

            public String getViews() {
                return views;
            }

            public void setViews(String views) {
                this.views = views;
            }

            public String getLikes() {
                return likes;
            }

            public void setLikes(String likes) {
                this.likes = likes;
            }
        }
    }
}
