package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public  class LeaderboardModel
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data DataClass;

    public class Data
    {
        @SerializedName("UserLevelsCount")
        public String UserLevelsCount;

        @SerializedName("UserLevelsData")
        public ArrayList<UserLevelsData> userLevelsData;

        public  class UserLevelsData
        {
            @SerializedName("UserID")
            public String UserID;

            @SerializedName("Name")
            public String Name;

            @SerializedName("MobileNumber")
            public String MobileNumber;

            @SerializedName("FeaturedIcon")
            public String FeaturedIcon;

            @SerializedName("Points")
            public String Points;

            @SerializedName("LevelNumber")
            public String LevelNumber;

            @SerializedName("LevelName")
            public String LevelName;

            @SerializedName("Rank")
            public String Rank;

        }

        @SerializedName("CurrentUserData")
        public CurrentUserData CurrentUserData;

        public class CurrentUserData
        {
            @SerializedName("UserID")
            public String UserID;

            @SerializedName("Name")
            public String Name;

            @SerializedName("MobileNumber")
            public String MobileNumber;

            @SerializedName("FeaturedIcon")
            public String FeaturedIcon;

            @SerializedName("Points")
            public String Points;

            @SerializedName("LevelNumber")
            public String LevelNumber;

            @SerializedName("LevelName")
            public String LevelName;

            @SerializedName("Rank")
            public String Rank;
        }
    }
}
