package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CustomerTicketsModel
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data DataClass;

    public  class Data
    {

        @SerializedName("IsLast")
        public boolean IsLast;

        @SerializedName("TicketsCount")
        public String TicketsCount;

        @SerializedName("TicketsData")
        public ArrayList<TicketsData> ticketsDataArrayList= new ArrayList<>();

        public  class TicketsData
        {
            @SerializedName("TicketID")
            public String TicketID;

            @SerializedName("Code")
            public String Code;

            @SerializedName("Issue")
            public String Issue;

            @SerializedName("Subject")
            public String Subject;

            @SerializedName("Date")
            public String Date;

            @SerializedName("Status")
            public String Status;

            @SerializedName("StatusValue")
            public String StatusValue;
        }
    }
}
