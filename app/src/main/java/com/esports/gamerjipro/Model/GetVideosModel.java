package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetVideosModel
{

    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public VideoData videoData;


    public  class VideoData
    {
        @SerializedName("ChannelName")
        public String ChannelName;

        @SerializedName("ChannelLogo")
        public String ChannelLogo;

        @SerializedName("IsLast")
        public boolean IsLast;

        @SerializedName("VideosCount")
        public String VideosCount;

        @SerializedName("ChannelSubscribers")
        public String ChannelSubscribers;

        @SerializedName("ChannelLink")
        public String ChannelLink;

        @SerializedName("VideosData")
        public ArrayList<VideosData> videosDataArrayList ;

        public  class VideosData
        {
            @SerializedName("VideoID")
            public String VideoID;

            @SerializedName("Name")
            public String Name;

            @SerializedName("YoutubeLink")
            public String YoutubeLink;

            @SerializedName("YoutubeID")
            public String YoutubeID;

            @SerializedName("Views")
            public String Views;
        }
    }
}
