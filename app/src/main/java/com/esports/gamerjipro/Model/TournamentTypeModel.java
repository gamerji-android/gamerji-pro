package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TournamentTypeModel
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data DataClass;

    public  class Data
    {

        @SerializedName("FeaturedImage")
        public String FeaturedImage;

        @SerializedName("TypesCount")
        public String TypesCount;

        @SerializedName("TypesData")
        public ArrayList<TypesData> typesDataArrayList= new ArrayList<>();

        public  class TypesData
        {
            @SerializedName("TypeID")
            public String TypeID;

            @SerializedName("Name")
            public String Name;

            @SerializedName("FeaturedImage")
            public String FeaturedImage;

            @SerializedName("TournamentsCount")
            public String TournamentsCount;

            @SerializedName("TournamentsData")
            public ArrayList<TournamentsData> tournamentsDataArrayList;


            public  class TournamentsData
            {
                @SerializedName("TournamentID")
                public String TournamentID;

                @SerializedName("Title")
                public String Title;

                @SerializedName("TypeTitle")
                public String TypeTitle;

                @SerializedName("GameID")
                public String GameID;

                @SerializedName("GameTypeID")
                public String GameTypeID;

                @SerializedName("GameTypeName")
                public String GameTypeName;

                @SerializedName("Date")
                public String Date;

                @SerializedName("Time")
                public String Time;

                @SerializedName("Map_Length")
                public String Map_Length;

                @SerializedName("Perspective_LevelCap")
                public String Perspective_LevelCap;

                @SerializedName("Map_LengthTitle")
                public String Map_LengthTitle;

                @SerializedName("Perspective_LevelCapTitle")
                public String Perspective_LevelCapTitle;

                @SerializedName("WinningAmount")
                public String WinningAmount;

                @SerializedName("PerKill_MaxLosesTitle")
                public String PerKill_MaxLosesTitle;

                @SerializedName("PerKill_MaxLosesCurrency")
                public boolean PerKill_MaxLosesCurrency;

                @SerializedName("WinnersCount")
                public String WinnersCount;

                @SerializedName("PerKill_MaxLoses")
                public String PerKill_MaxLoses;

                @SerializedName("EntryFee")
                public String EntryFee;

                @SerializedName("TotalSpots")
                public String TotalSpots;

                @SerializedName("JoinedSpots")
                public String JoinedSpots;

                @SerializedName("CanJoinPlayers")
                public int CanJoinPlayers;

                @SerializedName("CanJoinExtraPlayers")
                public int CanJoinExtraPlayers;

                @SerializedName("Joined")
                public boolean Joined;

                @SerializedName("ConfirmStatus")
                public String ConfirmStatus;

                @SerializedName("Status")
                public boolean Status;
            }
        }
    }
}
