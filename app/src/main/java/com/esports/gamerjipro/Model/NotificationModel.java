package com.esports.gamerjipro.Model;

import java.util.ArrayList;

public class NotificationModel
{

   String date;

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public ArrayList<NotificationDetailModel> getNotificationDetailModels()
    {
        return notificationDetailModels;
    }

    public void setNotificationDetailModels(ArrayList<NotificationDetailModel> notificationDetailModels)
    {
        this.notificationDetailModels = notificationDetailModels;
    }

    ArrayList<NotificationDetailModel> notificationDetailModels = new ArrayList<NotificationDetailModel>();

    public static class NotificationDetailModel
    {
        String NotificationID;
        String Type;
        String RelationID;
        String UserID;
        String Description;
        String CreationDate;

        public String getNotificationID()
        {
            return NotificationID;
        }

        public void setNotificationID(String notificationID) {
            NotificationID = notificationID;
        }

        public String getType() {
            return Type;
        }

        public void setType(String type) {
            Type = type;
        }

        public String getRelationID() {
            return RelationID;
        }

        public void setRelationID(String relationID) {
            RelationID = relationID;
        }

        public String getUserID() {
            return UserID;
        }

        public void setUserID(String userID) {
            UserID = userID;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public String getCreationDate() {
            return CreationDate;
        }

        public void setCreationDate(String creationDate) {
            CreationDate = creationDate;
        }

        public String getCreationDateAgo() {
            return CreationDateAgo;
        }

        public void setCreationDateAgo(String creationDateAgo) {
            CreationDateAgo = creationDateAgo;
        }

        String CreationDateAgo;
    }
}
