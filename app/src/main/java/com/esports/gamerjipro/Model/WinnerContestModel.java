package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class WinnerContestModel
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data data;

    public class Data
    {
        @SerializedName("PrizePoolsCount")
        public String PrizePoolsCount;

        @SerializedName("TotalPrice")
        public String TotalPrice;

        @SerializedName("PrizePoolsData")
        public ArrayList<PrizePoolsData> prizePoolsData;

        public class PrizePoolsData
        {
            @SerializedName("PrizePoolID")
            public String PrizePoolID;

            @SerializedName("Title")
            public String Title;

            @SerializedName("PrizeSign")
            public String PrizeSign;

            @SerializedName("Prize")
            public String Prize;
        }
    }
}
