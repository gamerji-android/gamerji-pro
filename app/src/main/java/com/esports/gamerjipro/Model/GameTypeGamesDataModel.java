package com.esports.gamerjipro.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GameTypeGamesDataModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("UserGameID")
        @Expose
        private String userGameID;
        @SerializedName("GameID")
        @Expose
        private String gameID;
        @SerializedName("UserID")
        @Expose
        private String userID;
        @SerializedName("GameName")
        @Expose
        private String gameName;
        @SerializedName("UniqueName")
        @Expose
        private String uniqueName;
        @SerializedName("DaysLimit")
        @Expose
        private String daysLimit;
        @SerializedName("LastUpdatedDttm")
        @Expose
        private String lastUpdatedDttm;
        @SerializedName("HelpText")
        @Expose
        private String helpText;

        public String getUserGameID() {
            return userGameID;
        }

        public void setUserGameID(String userGameID) {
            this.userGameID = userGameID;
        }

        public String getGameID() {
            return gameID;
        }

        public void setGameID(String gameID) {
            this.gameID = gameID;
        }

        public String getUserID() {
            return userID;
        }

        public void setUserID(String userID) {
            this.userID = userID;
        }

        public String getGameName() {
            return gameName;
        }

        public void setGameName(String gameName) {
            this.gameName = gameName;
        }

        public String getUniqueName() {
            return uniqueName;
        }

        public void setUniqueName(String uniqueName) {
            this.uniqueName = uniqueName;
        }

        public String getDaysLimit() {
            return daysLimit;
        }

        public void setDaysLimit(String daysLimit) {
            this.daysLimit = daysLimit;
        }

        public String getLastUpdatedDttm() {
            return lastUpdatedDttm;
        }

        public void setLastUpdatedDttm(String lastUpdatedDttm) {
            this.lastUpdatedDttm = lastUpdatedDttm;
        }

        public String getHelpText() {
            return helpText;
        }

        public void setHelpText(String helpText) {
            this.helpText = helpText;
        }
    }
}
