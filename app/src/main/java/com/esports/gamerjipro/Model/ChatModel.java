package com.esports.gamerjipro.Model;

public class ChatModel
{
    String id;
    String sender_id;
    String contest_id;
    String sender_full_name;
    String chat_message;

    public String getId()
    {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public String getContest_id() {
        return contest_id;
    }

    public void setContest_id(String contest_id) {
        this.contest_id = contest_id;
    }

    public String getSender_full_name() {
        return sender_full_name;
    }

    public void setSender_full_name(String sender_full_name) {
        this.sender_full_name = sender_full_name;
    }

    public String getChat_message() {
        return chat_message;
    }

    public void setChat_message(String chat_message) {
        this.chat_message = chat_message;
    }

    public String getChat_datetime() {
        return chat_datetime;
    }

    public void setChat_datetime(String chat_datetime) {
        this.chat_datetime = chat_datetime;
    }

    String chat_datetime;

    public String getLevel_icon()
    {
        return level_icon;
    }

    public void setLevel_icon(String level_icon) {
        this.level_icon = level_icon;
    }

    String level_icon;
}
