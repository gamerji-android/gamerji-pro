package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class AccountDetailsModel implements Serializable
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data DataClass;

    public class Data implements Serializable
    {

        @SerializedName("UserID")
        public String UserID;

        @SerializedName("TotalBalance")
        public String TotalBalance;

        @SerializedName("DepositedBalance")
        public String DepositedBalance;

        @SerializedName("WinningBalance")
        public String WinningBalance;

        @SerializedName("BonusBalance")
        public String BonusBalance;

        @SerializedName("MobileVerified")
        public String MobileVerified;

        @SerializedName("MobileNumber")
        public String MobileNumber;

            @SerializedName("EmailVerified")
            public String EmailVerified;

            @SerializedName("Email")
            public String Email;

            @SerializedName("PanVerified")
            public String PanVerified;

        @SerializedName("BankVerified")
        public String BankVerified;

        @SerializedName("UPIVerified")
        public String UPIVerified;

        @SerializedName("PanData")
        public PanData PanData;

        @SerializedName("BankData")
        public BankData BankData;

        @SerializedName("UPIData")
        public UPIData UPIData;

        @SerializedName("AdsCount")
        public int AdsCount;

        @SerializedName("WithdrawalMinAmount")
        public String WithdrawalMinAmount;

        @SerializedName("WithdrawalMaxAmount")
        public String WithdrawalMaxAmount;

        @SerializedName("WithdrawalEnabled")
        public Boolean WithdrawalEnabled;

        @SerializedName("AdsData")
        public ArrayList<AdsDataModel> adsDataArrayList;


            public class PanData implements Serializable
            {
                @SerializedName("PanName")
                public String PanName;

                @SerializedName("PanNumber")
                public String PanNumber;

                @SerializedName("PanImage")
                public String PanImage;

                @SerializedName("PanRejectedReason")
                public String PanRejectedReason;
            }

        public class BankData implements Serializable
        {
            @SerializedName("BankAccountNumber")
            public String BankAccountNumber;

            @SerializedName("BankAccountName")
            public String BankAccountName;

            @SerializedName("BankName")
            public String BankName;

            @SerializedName("BankBranch")
            public String BankBranch;

            @SerializedName("BankIFSC")
            public String BankIFSC;

            @SerializedName("BankImage")
            public String BankImage;

            @SerializedName("BankRejectedReason")
            public String BankRejectedReason;
        }

        public class UPIData implements Serializable
        {
            @SerializedName("UPIID")
            public String UPIID;

            @SerializedName("UPIAccountName")
            public String UPIAccountName;

            @SerializedName("BankName")
            public String BankName;

            @SerializedName("UPIRejectedReason")
            public String UPIRejectedReason;
        }
    }

}
