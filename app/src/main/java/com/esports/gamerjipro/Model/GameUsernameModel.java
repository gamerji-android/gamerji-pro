package com.esports.gamerjipro.Model;

public class GameUsernameModel
{
    private String UserGameID ,GameID,GameName,UniqueName;
    private boolean enable;

    public String getUserGameID()
    {
        return UserGameID;
    }

    public void setUserGameID(String userGameID) {
        UserGameID = userGameID;
    }

    public String getGameID() {
        return GameID;
    }

    public void setGameID(String gameID) {
        GameID = gameID;
    }

    public String getGameName() {
        return GameName;
    }

    public void setGameName(String gameName) {
        GameName = gameName;
    }

    public String getUniqueName() {
        return UniqueName;
    }

    public void setUniqueName(String uniqueName) {
        UniqueName = uniqueName;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
}
