package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class SingleContestModel implements Serializable
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data DataClass;

    public  class Data implements Serializable
    {
        @SerializedName("DataType")
        public String DataType;

        @SerializedName("ContestID")
        public String ContestID;

        @SerializedName("TournamentID")
        public String TournamentID;

        @SerializedName("Title")
        public String Title;

        @SerializedName("CaptainID")
        public String CaptainID;


        @SerializedName("GameID")
        public String GameID;

        @SerializedName("GameName")
        public String GameName;

        @SerializedName("GameTypeID")
        public String GameTypeID;

        @SerializedName("GameTypeName")
        public String GameTypeName;

        @SerializedName("GameTypeFeaturedImage")
        public String GameTypeFeaturedImage;

        @SerializedName("TypeName")
        public String TypeName;

        @SerializedName("Date")
        public String Date;

        @SerializedName("Time")
        public String Time;

        @SerializedName("Map_Length")
        public String Map_Length;

        @SerializedName("Perspective_LevelCap")
        public String Perspective_LevelCap;

        @SerializedName("Map_LengthTitle")
        public String Map_LengthTitle;

        @SerializedName("Perspective_LevelCapTitle")
        public String Perspective_LevelCapTitle;

        @SerializedName("WinningAmount")
        public String WinningAmount;

        @SerializedName("PerKill_MaxLosesTitle")
        public String PerKill_MaxLosesTitle;

        @SerializedName("PerKill_MaxLosesCurrency")
        public boolean PerKill_MaxLosesCurrency;

        @SerializedName("WinnersCount")
        public String WinnersCount;

        @SerializedName("PerKill_MaxLoses")
        public String PerKill_MaxLoses;

        @SerializedName("EntryFee")
        public String EntryFee;

        @SerializedName("TotalSpots")
        public String TotalSpots;

        @SerializedName("JoinedSpots")
        public String JoinedSpots;

        @SerializedName("CanJoinPlayers")
        public int CanJoinPlayers;

        @SerializedName("CanJoinExtraPlayers")
        public int CanJoinExtraPlayers;

        @SerializedName("RoomID")
        public String RoomID;

        @SerializedName("RoomPassword")
        public String RoomPassword;

        @SerializedName("RoomColumn")
        public String RoomColumn;

        @SerializedName("Rules")
        public String Rules;

        @SerializedName("UsersCount")
        public String UsersCount;

        @SerializedName("UsersData")
        public ArrayList<UserData> UsersData;

        @SerializedName("ContestsData")
        public ArrayList<TournamentDetailModel.TournamentData.ContestsData> contestsDataArrayList;

        @SerializedName("CurrentUserData")
        public ContestDetailsModel.ContestsData.CurrentUserData currentUserData;


        @SerializedName("Joined")
        public boolean Joined;

        @SerializedName("PrivateStatus")
        public boolean PrivateStatus;

        @SerializedName("ConfirmStatus")
        public String ConfirmStatus;

        @SerializedName("Status")
        public boolean Status;

        @SerializedName("ChannelLink")
        public String ChannelLink;

        @SerializedName("DiscordLink")
        public boolean DiscordLink;

        @SerializedName("AdsCount")
        public int AdsCount;

        @SerializedName("AdsData")
        public ArrayList<AdsDataModel> adsDataArrayList;

        public class UserData implements Serializable
        {

        }
    }
}
