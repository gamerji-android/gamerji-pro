package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

public class ResendOTPModel
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public OTPData otpData;

    public class OTPData
    {
        @SerializedName("OTPCode")
        public String OTPCode;

    }
}
