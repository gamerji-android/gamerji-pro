package com.esports.gamerjipro.Model;

public class SquadModel
{
    private String mobile_number;
    private String user_name;

    public String getUser_id()
    {
        return user_id;
    }

    public void setUser_id(String user_id)
    {
        this.user_id = user_id;
    }

    private String user_id;

    public boolean isAddplayer()
    {
        return addplayer;
    }

    public void setAddplayer(boolean addplayer)
    {
        this.addplayer = addplayer;
    }

    private boolean addplayer;

    public String getMobile_number()
    {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getGamerjiName() {
        return gamerji_name;
    }

    public void setGamerjiName(String gamerji_name) {
        this.gamerji_name = gamerji_name;
    }

    private String gamerji_name;
}
