package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

public class AdsDataModel
{
    @SerializedName("AdID")
    public String AdID;

    @SerializedName("AdName")
    public String AdName;

    @SerializedName("AdType")
    public String AdType;

    @SerializedName("AdImage")
    public String AdImage;

    @SerializedName("AdImageName")
    public String AdImageName;

    @SerializedName("AdURL")
    public String AdURL;

    @SerializedName("AdVideo")
    public String AdVideo;

    @SerializedName("AdVideoName")
    public String AdVideoName;

    @SerializedName("VideoAutoplay")
    public boolean VideoAutoplay;
}
