package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

public class JoinContestModel
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data DataClass;

    public class Data
    {
        @SerializedName("Type")
        public String Type;
    }
}
