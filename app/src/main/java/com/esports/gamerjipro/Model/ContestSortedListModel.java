package com.esports.gamerjipro.Model;

import java.util.ArrayList;

public class ContestSortedListModel
{
    public String getContest_Name() {
        return Contest_Name;
    }

    public void setContest_Name(String contest_Name)
    {
        Contest_Name = contest_Name;
    }

    public String getContest_Image() {
        return Contest_Image;
    }

    public void setContest_Image(String contest_Image) {
        Contest_Image = contest_Image;
    }

    public ArrayList<ContestTypeModelNew.Data.ContestsData> getContestsDataArrayList() {
        return contestsDataArrayList;
    }

    public void setContestsDataArrayList(ArrayList<ContestTypeModelNew.Data.ContestsData> contestsDataArrayList) {
        this.contestsDataArrayList = contestsDataArrayList;
    }

    public  String Contest_Name;
   public String Contest_Image;
    public  ArrayList<ContestTypeModelNew.Data.ContestsData> contestsDataArrayList= new ArrayList<>();
}
