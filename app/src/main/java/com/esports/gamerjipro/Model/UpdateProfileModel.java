package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

public class UpdateProfileModel
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data data;

    public class Data
    {
        @SerializedName("UserID")
        public String UserID;
    }
}
