package com.esports.gamerjipro.Model;

public class GameContestDetailsModel
{
    private String date;
    private String time;
    private String map;
    private String perspective;
    private String winning_amount;
    private String winner;
    private String per_kill;
    private String entry_fees;
    private String available_spots;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }

    public String getPerspective() {
        return perspective;
    }

    public void setPerspective(String perspective) {
        this.perspective = perspective;
    }

    public String getWinning_amount() {
        return winning_amount;
    }

    public void setWinning_amount(String winning_amount) {
        this.winning_amount = winning_amount;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public String getPer_kill() {
        return per_kill;
    }

    public void setPer_kill(String per_kill) {
        this.per_kill = per_kill;
    }

    public String getEntry_fees() {
        return entry_fees;
    }

    public void setEntry_fees(String entry_fees) {
        this.entry_fees = entry_fees;
    }

    public String getAvailable_spots() {
        return available_spots;
    }

    public void setAvailable_spots(String available_spots) {
        this.available_spots = available_spots;
    }

    public String getRemaining_spots() {
        return remaining_spots;
    }

    public void setRemaining_spots(String remaining_spots) {
        this.remaining_spots = remaining_spots;
    }

    String remaining_spots;
}
