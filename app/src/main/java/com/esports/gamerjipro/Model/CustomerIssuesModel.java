package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CustomerIssuesModel
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data issuesDataClass;

    public class Data
    {
        @SerializedName("IssuesData")
        public ArrayList<Data.IssuesData> issuesDataArrayList;

        @SerializedName("IssuesCount")
        public String IssuesCount;

        @SerializedName("HelpText")
        public String HelpText;


        public class IssuesData
        {
            @SerializedName("IssueID")
            public String IssueID;

            @SerializedName("Name")
            public String Name;

            @SerializedName("IsContestDependent")
            public String IsContestDependent;

            @SerializedName("SubIssuesCount")
            public String SubIssuesCount;

            @SerializedName("SubIssuesData")
            public ArrayList<SubIssuesData> SubIssuesData;

            public class SubIssuesData
            {
                @SerializedName("IssueID")
                public String IssueID;

                @SerializedName("Name")
                public String Name;

                @SerializedName("IsContestDependent")
                public String IsContestDependent;
            }
        }
    }
}
