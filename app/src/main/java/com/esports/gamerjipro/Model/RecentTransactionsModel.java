package com.esports.gamerjipro.Model;

import java.util.ArrayList;

public class RecentTransactionsModel
{


    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public ArrayList<TransactionDetail> getTransactionDetailArrayList()
    {
        return transactionDetailArrayList;
    }

    public void setTransactionDetailArrayList(ArrayList<TransactionDetail> transactionDetailArrayList)
    {
        this.transactionDetailArrayList = transactionDetailArrayList;
    }

    private ArrayList<TransactionDetail> transactionDetailArrayList;
    private String date;



    public static class TransactionDetail
    {
        String TransactionID;
        String Title;
        String Amount;
        String AmountType;
        String DateTime;
        String ContestID;

        public String getTransactionID()
        {
            return TransactionID;
        }

        public void setTransactionID(String transactionID) {
            TransactionID = transactionID;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        public String getAmount() {
            return Amount;
        }

        public void setAmount(String amount) {
            Amount = amount;
        }

        public String getAmountType() {
            return AmountType;
        }

        public void setAmountType(String amountType) {
            AmountType = amountType;
        }

        public String getDateTime() {
            return DateTime;
        }

        public void setDateTime(String dateTime) {
            DateTime = dateTime;
        }

        public String getContestID() {
            return ContestID;
        }

        public void setContestID(String contestID) {
            ContestID = contestID;
        }

        public String getGameName() {
            return GameName;
        }

        public void setGameName(String gameName) {
            GameName = gameName;
        }

        public String getGTypeName() {
            return GTypeName;
        }

        public void setGTypeName(String GTypeName) {
            this.GTypeName = GTypeName;
        }

        public String getCouponCode() {
            return CouponCode;
        }

        public void setCouponCode(String couponCode) {
            CouponCode = couponCode;
        }

        public String getInvoiceFile() {
            return InvoiceFile;
        }

        public void setInvoiceFile(String invoiceFile) {
            InvoiceFile = invoiceFile;
        }

        String GameName;
        String GTypeName;
        String CouponCode;
        String InvoiceFile;

        public String getUniqueID() {
            return UniqueID;
        }

        public void setUniqueID(String uniqueID) {
            UniqueID = uniqueID;
        }

        String UniqueID;
    }
}
