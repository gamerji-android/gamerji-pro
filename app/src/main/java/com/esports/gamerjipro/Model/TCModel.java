package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

public class TCModel
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public String data;
}
