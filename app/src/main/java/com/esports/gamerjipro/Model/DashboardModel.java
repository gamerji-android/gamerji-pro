package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class DashboardModel implements Serializable
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public  Data DataClass;

    public class Data implements Serializable
    {
        @SerializedName("UserData")
        public UserData UserData;

        @SerializedName("BannersData")
        public BannersData BannersData;

        @SerializedName("GamesData")
        public GamesData GamesData;

        @SerializedName("IsDailyLogin")
        public boolean IsDailyLogin;

        @SerializedName("IsDailyLoginPoint")
        public String IsDailyLoginPoint;

        @SerializedName("IsFirstTime")
        public boolean IsFirstTime;

        @SerializedName("IntroVideoTitle")
        public String IntroVideoTitle;

        @SerializedName("IntroVideoLink")
        public String IntroVideoLink;

        @SerializedName("AdsCount")
        public int AdsCount;

        @SerializedName("AdsData")
        public AdsDataModel adsDataModel;
    }

    public class UserData implements Serializable
    {
        @SerializedName("Ustatus")
    public String Ustatus;

        @SerializedName("Uflag")
        public String Uflag;

        @SerializedName("Umessage")
        public String Umessage;

        @SerializedName("Udata")
        public Udata Udata;

        public class Udata implements Serializable
        {
            @SerializedName("UserID")
            public String UserID;

            @SerializedName("CountryCode")
            public String CountryCode;

            @SerializedName("MobileNumber")
            public String MobileNumber;

            @SerializedName("EmailAddress")
            public String EmailAddress;

            @SerializedName("Status")
            public String Status;
        }
    }

    public class BannersData implements Serializable
    {
        @SerializedName("Bstatus")
        public String Bstatus;

        @SerializedName("Bflag")
        public String Bflag;

        @SerializedName("Bmessage")
        public String Bmessage;

        @SerializedName("Bdata")
        public ArrayList<Bdata> bdataArrayList = new ArrayList<>();

        public class Bdata implements Serializable
        {
            @SerializedName("BannerID")
            public String BannerID;

            @SerializedName("Name")
            public String Name;

            @SerializedName("Type")
            public String Type;

            @SerializedName("GameID")
            public String GameID;

            @SerializedName("GName")
            public String GName;

            @SerializedName("GTypeID")
            public String GTypeID;

            @SerializedName("GTypeName")
            public String GTypeName;

            @SerializedName("RedirectURL")
            public String RedirectURL;

            @SerializedName("GTypeFeaturedImage")
            public String GTypeFeaturedImage;

            @SerializedName("FeaturedImage")
            public String FeaturedImage;
        }
    }

    public class GamesData implements Serializable
    {
        @SerializedName("Gstatus")
        public String Gstatus;

        @SerializedName("Gflag")
        public String Gflag;

        @SerializedName("Gmessage")
        public String Gmessage;

        @SerializedName("Gdata")
        public ArrayList<Gdata> gdataArrayList= new ArrayList<>();

        public class Gdata implements Serializable
        {
            @SerializedName("GameID")
            public String GameID;

            @SerializedName("Name")
            public String Name;

            @SerializedName("FeaturedImage")
            public String FeaturedImage;
        }
    }
}
