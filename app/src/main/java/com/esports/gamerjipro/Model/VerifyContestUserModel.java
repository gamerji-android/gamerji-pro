package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

public class VerifyContestUserModel
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data data;

    public class Data
    {
        @SerializedName("UserID")
        public String UserID;

        @SerializedName("InGameName")
        public String InGameName;

        @SerializedName("CountryCode")
        public String CountryCode;

        @SerializedName("MobileNumber")
        public String MobileNumber;

        @SerializedName("TeamName")
        public String TeamName;
    }
}
