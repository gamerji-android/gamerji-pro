package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GamerjiPointsModel
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;


    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data data;

    public class Data
    {
        @SerializedName("StatesCount")
        public String StatesCount;

        @SerializedName("StatesData")
        public ArrayList<StatesData> StatesData;

        public class StatesData
        {
            @SerializedName("PointID")
            public String PointID;

            @SerializedName("Name")
            public String Name;

            @SerializedName("Points")
            public String Points;
        }
    }
}
