package com.esports.gamerjipro.Model;

import java.util.ArrayList;

public class GameContestListModel
{
    public String getContest()
    {
        return contest;
    }

    public void setContest(String contest)
    {
        this.contest = contest;
    }

    public ArrayList<GameContestDetailsModel> getGameContestDetailsModels()
    {
        return gameContestDetailsModels;
    }

    public void setGameContestDetailsModels(ArrayList<GameContestDetailsModel> gameContestDetailsModels)
    {
        this.gameContestDetailsModels = gameContestDetailsModels;
    }

    private String contest;
    private ArrayList<GameContestDetailsModel> gameContestDetailsModels = new ArrayList<>();
}
