package com.esports.gamerjipro.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerCareModel {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("ChatBotURLFlag")
        @Expose
        private Boolean chatBotURLFlag;
        @SerializedName("ChatBotURL")
        @Expose
        private String chatBotURL;
        @SerializedName("ChatBotURLAndroid")
        @Expose
        private String chatBotURLAndroid;
        @SerializedName("ChatBotURLiOS")
        @Expose
        private String chatBotURLiOS;

        public Boolean getChatBotURLFlag() {
            return chatBotURLFlag;
        }

        public void setChatBotURLFlag(Boolean chatBotURLFlag) {
            this.chatBotURLFlag = chatBotURLFlag;
        }

        public String getChatBotURL() {
            return chatBotURL;
        }

        public void setChatBotURL(String chatBotURL) {
            this.chatBotURL = chatBotURL;
        }

        public String getChatBotURLAndroid() {
            return chatBotURLAndroid;
        }

        public void setChatBotURLAndroid(String chatBotURLAndroid) {
            this.chatBotURLAndroid = chatBotURLAndroid;
        }

        public String getChatBotURLiOS() {
            return chatBotURLiOS;
        }

        public void setChatBotURLiOS(String chatBotURLiOS) {
            this.chatBotURLiOS = chatBotURLiOS;
        }
    }
}
