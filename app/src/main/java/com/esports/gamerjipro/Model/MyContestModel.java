package com.esports.gamerjipro.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MyContestModel
{
    @SerializedName("status")
    public String status;

    @SerializedName("flag")
    public String flag;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data DataClass;

    public class Data
    {

        @SerializedName("GameID")
        public String GameID;

        @SerializedName("Name")
        public String Name  ;

        @SerializedName("ContestsCount")
        public String ContestsCount ;

        @SerializedName("ContestsData")
        public ArrayList<ContestsData> contestsDataArrayList ;

        public class ContestsData
        {
            @SerializedName("Type")
            public String Type;

            @SerializedName("TournamentID")
            public String TournamentID;

            @SerializedName("Title")
            public String Title;

            @SerializedName("ContestID")
            public String ContestID;

            @SerializedName("GameTypeID")
            public String GameTypeID  ;

            @SerializedName("GameTypeName")
            public String GameTypeName ;

            @SerializedName("Date")
            public String Date;

            @SerializedName("Time")
            public String Time  ;

            @SerializedName("Map_LengthTitle")
            public String Map_LengthTitle ;

            @SerializedName("Map_Length")
            public String Map_Length;

            @SerializedName("Perspective_LevelCap")
            public String Perspective_LevelCap  ;

            @SerializedName("Perspective_LevelCapTitle")
            public String Perspective_LevelCapTitle ;

            @SerializedName("WinningAmount")
            public String WinningAmount;

            @SerializedName("WinnersCount")
            public String WinnersCount    ;

            @SerializedName("PerKill_MaxLosesTitle")
            public String PerKill_MaxLosesTitle    ;

            @SerializedName("PerKill_MaxLoses")
            public String PerKill_MaxLoses ;

            @SerializedName("EntryFee")
            public String EntryFee ;

            @SerializedName("TotalSpots")
            public String TotalSpots;

            @SerializedName("JoinedSpots")
            public String JoinedSpots  ;

            @SerializedName("RoomID")
            public String RoomID ;

            @SerializedName("RoomPassword")
            public String RoomPassword;

            @SerializedName("RoomColumn")
            public String RoomColumn;

            @SerializedName("Rules")
            public String Rules    ;

            @SerializedName("Joined")
            public boolean Joined ;

            @SerializedName("Status")
            public String Status ;

        }

        @SerializedName("AdsCount")
        public int AdsCount;

        @SerializedName("AdsData")
        public ArrayList<AdsDataModel> adsDataArrayList;
    }
}
