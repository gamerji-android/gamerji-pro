package com.esports.gamerjipro.notification_services;

import android.content.Intent;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.esports.gamerjipro.adGyde.CommonEvents;
import com.google.firebase.messaging.FirebaseMessagingService;


public class MyFirebaseInstanceIDService extends FirebaseMessagingService
{

    private static final String TAG = "MyFirebaseIIDService";
    public static final String REGISTRATION_SUCCESS = "RegistrationSuccess";
    public static final String REGISTRATION_ERROR = "RegistrationError";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    /*@Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Common.insertLog("Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }*/
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        registerFirebase(token);
        // TODO: Implement this method to send token to your app server.
    }

    private void registerFirebase(String token) {
        //Registration complete intent initially null
        Intent registrationComplete = null;
        try {
            //You can also extend the app by storing the token in to your server
            //on registration complete creating intent with success
            registrationComplete = new Intent(REGISTRATION_SUCCESS);
            //Putting the token to the intent
            registrationComplete.putExtra("token", token);
            CommonEvents.setFirebaseTokenEvent(token);
        } catch (Exception e) {
            //If any error occurred
            registrationComplete = new Intent(REGISTRATION_ERROR);
        }
        //Sending the broadcast that registration is completed
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    @Override
    public void onNewToken(String s)
    {
        super.onNewToken(s);
    }
}
