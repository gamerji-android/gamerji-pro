package com.esports.gamerjipro.cashFree;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.esports.gamerjipro.Model.GeneralResponseModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.Pref;
import com.esports.gamerjipro.activity.ActivityGameContestNew;
import com.esports.gamerjipro.activity.ActivityGameTournament;
import com.gocashfree.cashfreesdk.CFClientInterface;
import com.gocashfree.cashfreesdk.CFPaymentService;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_APP_ID;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_BANK_CODE;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CARD_CVV;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CARD_HOLDER;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CARD_MM;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CARD_NUMBER;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CARD_YYYY;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_EMAIL;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_NAME;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_PHONE;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_NOTIFY_URL;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_AMOUNT;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_CURRENCY;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_ID;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_NOTE;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_PAYMENT_OPTION;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_UPI_VPA;

public class ActivityPayment extends AppCompatActivity implements CFClientInterface
{

    APIInterface apiInterface;
    String card_number,card_name,year,month,type,amount,cvv,bank_id,upi;
    LinearLayout ll_payment;
    TextView tv_payment_status,tv_dialog_amount,tv_payment_id,tv_payment_time;
    CardView cv_payment_done;
    ImageView iv_payment_type;
    RelativeLayout rl_animation;
     ProgressDialog progressDialog;
     Timer timer ;
    boolean is_tournament=false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getIntent().getExtras()!=null)
        {
            card_number=getIntent().getStringExtra("card_number");
            card_name=getIntent().getStringExtra("card_name");
            year=getIntent().getStringExtra("exp_year");
            month=getIntent().getStringExtra("exp_month");
            type=getIntent().getStringExtra("type");
            amount=getIntent().getStringExtra("amount");
            cvv=getIntent().getStringExtra("cvv");
            bank_id=getIntent().getStringExtra("bank_id");
            upi=getIntent().getStringExtra("upi");
            is_tournament=getIntent().getBooleanExtra("is_tournament",false);
        }

        setContentView(R.layout.activity_payment_layout);
        timer= new Timer();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.setCancelable(false);
        progressDialog.show();

        rl_animation=findViewById(R.id.rl_animation);
        iv_payment_type=findViewById(R.id.iv_payment_type);
        ll_payment=findViewById(R.id.ll_payment);
        tv_payment_status=findViewById(R.id.tv_payment_status);
        tv_dialog_amount=findViewById(R.id.tv_dialog_amount);
        tv_payment_time=findViewById(R.id.tv_payment_time);
        tv_payment_id=findViewById(R.id.tv_payment_id);
        cv_payment_done=findViewById(R.id.cv_payment_done);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        getPaymentToken(amount);

    }


    private void getPaymentToken(String amount)
    {
        Call<GeneralResponseModel> generalResponseModelCall;

        generalResponseModelCall = apiInterface.getPaymentToken(Constants.GET_PAYMENT_TOKEN, Pref.getValue(ActivityPayment.this, Constants.UserID,"", Constants.FILENAME),amount);

        generalResponseModelCall.enqueue(new Callback<GeneralResponseModel>()
        {
            @Override
            public void onResponse(@NonNull Call<GeneralResponseModel> call, @NonNull Response<GeneralResponseModel> response)
            {
                progressDialog.dismiss();
                GeneralResponseModel generalResponseModel = response.body();
                assert generalResponseModel != null;
                if (generalResponseModel.status.equalsIgnoreCase("success"))
                {
                    dopayment(generalResponseModel.DataClass.OrderID,type,generalResponseModel.DataClass.PaymentToken);
                }
                else
                {
                    Toast.makeText(ActivityPayment.this, generalResponseModel.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<GeneralResponseModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void dopayment(String orderID, String type, String paymentToken)
    {
        HashMap<String,String> params = new HashMap<>();
        if (type.equalsIgnoreCase(Constants.CASHFREE_CARD))
        {
            params.put(PARAM_PAYMENT_OPTION, type);
            params.put(PARAM_CARD_NUMBER, card_number);//Replace Card number
            params.put(PARAM_CARD_MM,month); // Card Expiry Month in MM
            params.put(PARAM_CARD_YYYY, "20"+year); // Card Expiry Year in YYYY
            params.put(PARAM_CARD_HOLDER, card_name); // Card Holder name
            params.put(PARAM_CARD_CVV,cvv);
        }
        else if (type.equalsIgnoreCase(Constants.CASHFREE_NB))
        {
            params.put(PARAM_PAYMENT_OPTION, type);
            params.put(PARAM_BANK_CODE, bank_id);
        }
        else if (type.equalsIgnoreCase(Constants.CASHFREE_WALLET))
        {
            params.put(PARAM_PAYMENT_OPTION, type);
            params.put(PARAM_BANK_CODE, bank_id);
        }
        else if (type.equalsIgnoreCase(Constants.CASHFREE_UPI))
        {
            params.put(PARAM_PAYMENT_OPTION, type);
            params.put(PARAM_UPI_VPA, upi);
        }

        params.put(PARAM_ORDER_AMOUNT, amount);
        params.put(PARAM_APP_ID, Constants.CASHFREE_APP_ID);

        params.put(PARAM_ORDER_ID,orderID);

        params.put(PARAM_ORDER_CURRENCY, "INR");
        params.put(PARAM_ORDER_NOTE,"Recharge");
        params.put(PARAM_CUSTOMER_NAME,Pref.getValue(ActivityPayment.this,Constants.firstname,"", Constants.FILENAME)+" "+
                Pref.getValue(ActivityPayment.this,Constants.lastname,"", Constants.FILENAME));
        params.put(PARAM_CUSTOMER_PHONE,Pref.getValue(ActivityPayment.this,Constants.MobileNumber,"", Constants.FILENAME));
        params.put(PARAM_CUSTOMER_EMAIL,Pref.getValue(ActivityPayment.this,Constants.EmailAddress,"", Constants.FILENAME));
        params.put(PARAM_NOTIFY_URL, Constants.NOTIFY_URL);

        if (paymentToken != null)
        {
            CFPaymentService cfPaymentService = CFPaymentService.getCFPaymentServiceInstance();
            cfPaymentService.setOrientation(0);
            try
            {
                    cfPaymentService.doPayment(ActivityPayment.this, params,paymentToken, ActivityPayment.this, Constants.CASHFREE_TYPE);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(ActivityPayment.this,"Unable to proceed",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSuccess(Map<String, String> map)
    {

        timer.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                getStatusApi(map);
            }
        },0,3000);

        /*new Timer().schedule(new TimerTask()
        {
            @Override
            public void run()
            {
               runOnUiThread(() -> getStatusApi(map));
            }
        },0,3000);*/
    }

    private void getStatusApi(Map<String, String> map)
    {
        Call<GeneralResponseModel> generalResponseModelCall;
        generalResponseModelCall = apiInterface.getPaymentStatus(Constants.GET_PAYMENT_STATUS, Pref.getValue(ActivityPayment.this, Constants.UserID,"", Constants.FILENAME),map.get("orderId"));

        generalResponseModelCall.enqueue(new Callback<GeneralResponseModel>()
        {
            @Override
            public void onResponse(@NonNull Call<GeneralResponseModel> call, @NonNull Response<GeneralResponseModel> response)
            {
                GeneralResponseModel generalResponseModel = response.body();
                assert generalResponseModel != null;
                Intent intent = new Intent();
                if (generalResponseModel.status.equalsIgnoreCase("success"))
                {
                    if (generalResponseModel.DataClass.PaymentStatus.equalsIgnoreCase("Success"))
                    {
                        progressDialog.dismiss();
                        rl_animation.setVisibility(View.VISIBLE);
                        tv_payment_status.setText("SUCCESS");
                        intent.putExtra(Constants.PAYMENT_STATUS_SUCCESS,true);
                        iv_payment_type.setImageResource(R.drawable.ic_payment_success);
                        ll_payment.setBackground(getResources().getDrawable(R.drawable.ic_bg_payment_success));
                        cv_payment_done.setVisibility(View.VISIBLE);
                    }
                    else if (generalResponseModel.DataClass.PaymentStatus.equalsIgnoreCase("PENDING"))
                    {
                        rl_animation.setVisibility(View.VISIBLE);
                        tv_payment_status.setText("PENDING");
                        iv_payment_type.setImageResource(R.drawable.ic_payment_pending);
                        ll_payment.setBackground(getResources().getDrawable(R.drawable.ic_bg_payment_pending));
                    }
                    else
                    {
                        progressDialog.dismiss();
                        rl_animation.setVisibility(View.VISIBLE);
                        tv_payment_status.setText("FAILED");
                        iv_payment_type.setImageResource(R.drawable.ic_payment_failed);
                        ll_payment.setBackground(getResources().getDrawable(R.drawable.ic_bg_payment_failed));
                        intent.putExtra(Constants.PAYMENT_STATUS_SUCCESS,false);
                        cv_payment_done.setVisibility(View.VISIBLE);
                    }

                    tv_payment_id.setText(map.get("orderId"));
                    tv_dialog_amount.setText(map.get("orderAmount"));
                    tv_payment_time.setText(map.get("txTime"));

                    cv_payment_done.setOnClickListener(v ->
                    {
                        if (getIntent().getStringExtra("game_id")!=null)
                        {
                            timer.cancel();
                            Intent i ;
                            if (is_tournament)
                            {
                                i = new Intent(ActivityPayment.this, ActivityGameTournament.class);
                            }
                            else
                            {
                                i = new Intent(ActivityPayment.this, ActivityGameContestNew.class);
                            }
                            i.putExtra("game_id",getIntent().getStringExtra("game_id"));
                            i.putExtra("game_type_id",getIntent().getStringExtra("game_type_id"));
                            i.putExtra("game_name",getIntent().getStringExtra("game_name"));
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            finish();
                        }
                        else
                        {
                            intent.putExtra(Constants.PAYMENT_AMOUNT,map.get("orderAmount"));
                            setResult(Constants.PAYMENT_RESULT,intent);
                            finish();
                        }
                    });

                }
                else
                {
                    Constants.SnakeMessageYellow(rl_animation,generalResponseModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<GeneralResponseModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();

               /* rl_animation.setVisibility(View.VISIBLE);
                tv_payment_status.setText("FAILED");
                iv_payment_type.setImageResource(R.drawable.ic_payment_failed);
                ll_payment.setBackground(getResources().getDrawable(R.drawable.ic_bg_payment_failed));
                cv_payment_done.setVisibility(View.VISIBLE);


                cv_payment_done.setOnClickListener(v ->
                {
                    Intent intent = new Intent();
                    intent.putExtra(PAYMENT_STATUS_SUCCESS,false);
                    setResult(PAYMENT_RESULT,intent);
                    t.printStackTrace();
                    Common.insertLog("FAILURE RESPONE", call.toString());
                });*/

            }
        });
    }

    @Override
    public void onFailure(Map<String, String> map)
    {
        Intent intent = new Intent();
        intent.putExtra(Constants.PAYMENT_STATUS_SUCCESS,false);
        setResult(Constants.PAYMENT_RESULT,intent);
        finish();
        Constants.SnakeMessageYellow(rl_animation,"Payment failed.");
    }

    @Override
    public void onNavigateBack()
    {
           progressDialog.dismiss();
        Intent intent = new Intent();
        intent.putExtra(Constants.PAYMENT_STATUS_SUCCESS,false);
        setResult(Constants.PAYMENT_RESULT,intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {
        Constants.SnakeMessageYellow(rl_animation,"Payment failed.");
        Intent intent = new Intent();
        intent.putExtra(Constants.PAYMENT_STATUS_SUCCESS,false);
        setResult(Constants.PAYMENT_RESULT,intent);
        finish();
    }

}
