package com.esports.gamerjipro.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.esports.gamerjipro.R;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.FileUtils;

public class ActivityAddBalance extends AppCompatActivity implements View.OnClickListener {
    ImageView img_back;
    CardView cv_add_balance, cv_amount;
    LinearLayout rs500, rs100, rs1000;
    EditText et_add_balance;
    TextView tv_wallet_balance;
    RelativeLayout parent_layout;
    String game_id, game_type_id, game_name;
    WebView webView;
    boolean is_tournament = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_balance);
        cv_add_balance=findViewById(R.id.cv_add_balance);
        cv_amount=findViewById(R.id.cv_amount);
        img_back=findViewById(R.id.img_back);
        parent_layout=findViewById(R.id.parent_layout);
        rs500=findViewById(R.id.rs500);
        rs100=findViewById(R.id.rs100);
        rs1000=findViewById(R.id.rs1000);
        if (getIntent().getExtras()!=null) {
            game_id = getIntent().getStringExtra("game_id");
            game_name = getIntent().getStringExtra("game_name");
            game_type_id = getIntent().getStringExtra("game_type_id");
            is_tournament=getIntent().getBooleanExtra("is_tournament",false);
        }
        et_add_balance=findViewById(R.id.et_add_balance);
        tv_wallet_balance=findViewById(R.id.tv_wallet_balance);
        tv_wallet_balance.setText(getIntent().getStringExtra("amount"));
        if (getIntent().getStringExtra("amount_to_add")!=null)
            et_add_balance.setText(getIntent().getStringExtra("amount_to_add"));
        et_add_balance.setSelection(et_add_balance.getText().length());
        img_back.setOnClickListener(v -> finish());
        cv_amount.setOnClickListener(v -> FileUtils.requestfoucs(ActivityAddBalance.this,et_add_balance));
        cv_add_balance.setOnClickListener(v ->
        {
            if (Constants.CheckRestrictedStates(ActivityAddBalance.this)) {
                Constants.SnakeMessageYellow(parent_layout,getResources().getString(R.string.restricted_states_msg));
            } else if (!Constants.isCitizenOfIndia(ActivityAddBalance.this)) {
                Constants.SnakeMessageYellow(parent_layout,getResources().getString(R.string.restricted_country_msg));
            } else {
                if (et_add_balance.getText().toString().isEmpty()) {
                    Constants.SnakeMessageYellow(parent_layout,"Please enter some amount.");
                } else if (Float.parseFloat(et_add_balance.getText().toString())>200000) {
                    Constants.SnakeMessageYellow(parent_layout,"You can add a maximum of ₹ 200000 in single transaction.");
                } else if (Float.parseFloat(et_add_balance.getText().toString())==0) {
                    Constants.SnakeMessageYellow(parent_layout,"Enter amount greater than 0");
                } else {
                    Intent i = new Intent(ActivityAddBalance.this,ActivityPaymentOption.class);
                    i.putExtra("amount",et_add_balance.getText().toString());
                    i.putExtra("current_balance",tv_wallet_balance.getText().toString());
                    i.putExtra("game_id",game_id);
                    i.putExtra("game_name",game_name);
                    i.putExtra("game_type_id",game_type_id);
                    i.putExtra("is_tournament",is_tournament);
                    startActivity(i);
                }
            }
        });

        rs500.setOnClickListener(this);
        rs100.setOnClickListener(this);
        rs1000.setOnClickListener(this);
        webView = findViewById(R.id.wv_youtube);
        webView.setWebChromeClient(new WebChromeClient());
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setDefaultFontSize(18);
        webView.loadUrl("https://www.youtube.com/embed/Q4X7ftXxNZI");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rs100: {
                et_add_balance.setText("100");
                et_add_balance.setSelection(et_add_balance.getText().length());
                rs500.setBackgroundColor(Color.TRANSPARENT);
                rs1000.setBackgroundColor(Color.TRANSPARENT);
                rs100.setBackground(getResources().getDrawable(R.drawable.rounded_corner_stroke));
                break;
            }

            case R.id.rs500:
            {
                et_add_balance.setText("500");
                et_add_balance.setSelection(et_add_balance.getText().length());
                rs100.setBackgroundColor(Color.TRANSPARENT);
                rs1000.setBackgroundColor(Color.TRANSPARENT);
                rs500.setBackground(getResources().getDrawable(R.drawable.rounded_corner_stroke));
                break;
            }

            case R.id.rs1000:
            {
                et_add_balance.setText("1000");
                et_add_balance.setSelection(et_add_balance.getText().length());
                rs500.setBackgroundColor(Color.TRANSPARENT);
                rs100.setBackgroundColor(Color.TRANSPARENT);
                rs1000.setBackground(getResources().getDrawable(R.drawable.rounded_corner_stroke));
                break;
            }
        }
    }
}
