package com.esports.gamerjipro.activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.esports.gamerjipro.R;

public class Full_Image_Activity extends AppCompatActivity
{

    ImageView FullImage;
    ProgressBar progressBar;
    ImageView img_back;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_image_screen);

        String image_url =getIntent().getStringExtra("image");

        FullImage = findViewById(R.id.img_full);
        progressBar = findViewById(R.id.Progress);
        img_back = findViewById(R.id.img_back);
        progressBar.setVisibility(View.VISIBLE);
        Glide.with(Full_Image_Activity.this).load(image_url).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource)
            {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource)
            {
                progressBar.setVisibility(View.GONE);
                return false;
            }
        }).into(FullImage);
        img_back.setOnClickListener(view -> onBackPressed());

    }
}
