package com.esports.gamerjipro.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Adapter.RecentTransactionAdapter;
import com.esports.gamerjipro.Model.RecentTransactionsModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.Pref;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import cz.msebera.android.httpclient.Header;

public class ActivityRecentTransactions extends AppCompatActivity {
    ArrayList<RecentTransactionsModel> recent_transaction_list = new ArrayList<>();
    public RelativeLayout layout_parent;
    RecyclerView rv_transactions;
    ImageView img_back;
    TextView txt_title;
    RecentTransactionAdapter recentTransactionAdapter;
    int count = 1;
    LinearLayoutManager linearLayoutManager;
    boolean isLoading, isLastPage = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_transactions);
        rv_transactions = findViewById(R.id.rv_transactions);
        layout_parent = findViewById(R.id.layout_parent);
        txt_title = findViewById(R.id.txt_title);
        txt_title.setText("RECENT TRANSACTIONS");
        linearLayoutManager = new LinearLayoutManager(this);
        rv_transactions.setLayoutManager(linearLayoutManager);
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(v -> finish());
        rv_transactions.setItemAnimator(new DefaultItemAnimator());
        getRecentTransactions(count);
        rv_transactions.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = linearLayoutManager.getItemCount();
                int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();

                if (!isLoading && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= 10) {
                        getRecentTransactions(++count);
                    }
                }
            }
        });
    }

    private void getRecentTransactions(int count) {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityRecentTransactions.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        RequestParams requestParams = new RequestParams();
        requestParams.put("Action", Constants.GET_TRANSCATIONS);
        requestParams.put("Val_Page", count);
        requestParams.put("Val_Limit", 10);
        requestParams.put("Val_Userid", Pref.getValue(this, Constants.UserID, "", Constants.FILENAME));

        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        asyncHttpClient.setTimeout(40 * 1000);
        asyncHttpClient.post(this, Constants.BASE_API + Constants.USER_FETCH, requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                isLoading = false;
                super.onSuccess(statusCode, headers, response);
                progressDialog.dismiss();
                if (response.optString("status").equalsIgnoreCase("success")) {
                    JSONObject dataJSON = response.optJSONObject("data");
                    if (dataJSON.optBoolean("IsLast"))
                        isLastPage = true;

                    JSONObject transcationdataJSON = dataJSON.optJSONObject("TransactionsData");
                    for (String key : iterate(transcationdataJSON.keys())) {
                        RecentTransactionsModel recentTransactionsModel = new RecentTransactionsModel();
                        JSONArray transaction_data = transcationdataJSON.optJSONArray(key);
                        ArrayList<RecentTransactionsModel.TransactionDetail> detailArrayList = new ArrayList<>();
                        for (int i = 0; i < transaction_data.length(); i++) {
                            JSONObject jsonObject = transaction_data.optJSONObject(i);
                            RecentTransactionsModel.TransactionDetail transactionDetail = new RecentTransactionsModel.TransactionDetail();
                            transactionDetail.setAmount(jsonObject.optString("Amount"));
                            transactionDetail.setAmountType(jsonObject.optString("AmountType"));
                            transactionDetail.setContestID(jsonObject.optString("ContestID"));
                            transactionDetail.setCouponCode(jsonObject.optString("CouponCode"));
                            transactionDetail.setDateTime(jsonObject.optString("DateTime"));
                            transactionDetail.setGameName(jsonObject.optString("GameName"));
                            transactionDetail.setGTypeName(jsonObject.optString("GTypeName"));
                            transactionDetail.setInvoiceFile(jsonObject.optString("InvoiceFile"));
                            transactionDetail.setTitle(jsonObject.optString("Title"));
                            transactionDetail.setTransactionID(jsonObject.optString("TransactionID"));
                            transactionDetail.setUniqueID(jsonObject.optString("UniqueID"));
                            detailArrayList.add(transactionDetail);
                        }
                        recentTransactionsModel.setDate(key);
                        recentTransactionsModel.setTransactionDetailArrayList(detailArrayList);
                        recent_transaction_list.add(recentTransactionsModel);
                    }
                    recentTransactionAdapter = new RecentTransactionAdapter(ActivityRecentTransactions.this, recent_transaction_list);
                    rv_transactions.setAdapter(recentTransactionAdapter);
                } else {
                    Constants.SnakeMessageYellow(layout_parent, response.optString("status"));
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                progressDialog.dismiss();
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                progressDialog.dismiss();
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progressDialog.dismiss();
                super.onFailure(statusCode, headers, responseString, throwable);
            }
        });
    }

    private <T> Iterable<T> iterate(final Iterator<T> i) {
        return () -> i;
    }
}
