package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.FileUtils;
import com.esports.gamerjipro.cashFree.ActivityPayment;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ActivityCardDetails extends AppCompatActivity
{
    ImageView img_back;
    CardView proceed;
    ImageView img_info;
    APIInterface apiInterface;
    EditText et_card_name,et_card_number,et_exp_date,et_cvv;
    String amount;
    RelativeLayout parent_layout;
    RelativeLayout cv_cvv_info;
    BottomSheetBehavior sheetBehavior;
    TextView txt_title_bs,txt_bottom_msg;
    View bg;
    ImageView img_close;
    String game_id,game_type_id,game_name;
    CardView cv_cvv,cv_exp,rl_balance,cv_card_name;
    boolean is_tournament=false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        if (getIntent().getExtras()!=null)
        {
            game_id = getIntent().getStringExtra("game_id");
            game_name = getIntent().getStringExtra("game_name");
            game_type_id = getIntent().getStringExtra("game_type_id");
            is_tournament=getIntent().getBooleanExtra("is_tournament",false);
        }
        super.onCreate(savedInstanceState);
        amount=getIntent().getStringExtra("amount");
        setContentView(R.layout.activity_card_details);
        img_back=findViewById(R.id.img_back);
        cv_cvv_info=findViewById(R.id.bottom_how_to);
        sheetBehavior = BottomSheetBehavior.from(cv_cvv_info);
        txt_title_bs=findViewById(R.id.txt_title_bs);
        img_close=findViewById(R.id.img_close);
        txt_bottom_msg=findViewById(R.id.txt_bottom_msg);
        cv_cvv=findViewById(R.id.cv_cvv);
        cv_exp=findViewById(R.id.cv_exp);
        rl_balance=findViewById(R.id.rl_balance);
        cv_card_name=findViewById(R.id.cv_card_name);


        bg=findViewById(R.id.bg);
        et_card_name=findViewById(R.id.et_card_name);
        et_card_number=findViewById(R.id.et_card_number);
        et_exp_date=findViewById(R.id.et_exp_date);
        et_cvv=findViewById(R.id.et_cvv);
        parent_layout=findViewById(R.id.parent_layout);
        img_close.setOnClickListener(v -> {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            bg.setVisibility(View.GONE);
        });

        proceed=findViewById(R.id.proceed);
        img_info=findViewById(R.id.img_info);
        img_back.setOnClickListener(v -> finish());
        img_info.setOnClickListener(v ->
        {
            txt_title_bs.setText("CVV");
            txt_bottom_msg.setText(getResources().getString(R.string.a_3_digit_number_printed_on_the_back_of_your_credit_debit_card));
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        });
        apiInterface = APIClient.getClient().create(APIInterface.class);

        cv_cvv.setOnClickListener(v -> FileUtils.requestfoucs(ActivityCardDetails.this,et_cvv));

        cv_exp.setOnClickListener(v -> FileUtils.requestfoucs(ActivityCardDetails.this,et_exp_date));

        rl_balance.setOnClickListener(v -> FileUtils.requestfoucs(ActivityCardDetails.this,et_card_number));

        cv_card_name.setOnClickListener(v -> FileUtils.requestfoucs(ActivityCardDetails.this,et_card_name));

        et_exp_date.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (s.length() > 0 && (s.length() % 3) == 0)
                {
                    final char c = s.charAt(s.length() - 1);
                    if ('/' == c) {
                        s.delete(s.length() - 1, s.length());
                    }
                }
                if (s.length() > 0 && (s.length() % 3) == 0) {
                    char c = s.charAt(s.length() - 1);
                    if (Character.isDigit(c) && TextUtils.split(s.toString(), "/").length <= 2) {
                        s.insert(s.length() - 1, "/");
                    }
                }
            }
        });
        et_card_number.addTextChangedListener(new TextWatcher()
        {
            private static final int TOTAL_SYMBOLS = 19; // size of pattern 0000-0000-0000-0000
            private static final int TOTAL_DIGITS = 16; // max numbers of digits in pattern: 0000 x 4
            private static final int DIVIDER_MODULO = 5; // means divider position is every 5th symbol beginning with 1
            private static final int DIVIDER_POSITION = DIVIDER_MODULO - 1; // means divider position is every 4th symbol beginning with 0
            private static final char DIVIDER = ' ';
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (!isInputCorrect(s, TOTAL_SYMBOLS, DIVIDER_MODULO, DIVIDER))
                {
                    s.replace(0, s.length(), buildCorrectString(getDigitArray(s, TOTAL_DIGITS), DIVIDER_POSITION, DIVIDER));
                }
            }
            private boolean isInputCorrect(Editable s, int totalSymbols, int dividerModulo, char divider) {
                boolean isCorrect = s.length() <= totalSymbols; // check size of entered string
                for (int i = 0; i < s.length(); i++) { // check that every element is right
                    if (i > 0 && (i + 1) % dividerModulo == 0) {
                        isCorrect &= divider == s.charAt(i);
                    } else {
                        isCorrect &= Character.isDigit(s.charAt(i));
                    }
                }
                return isCorrect;
            }

            private String buildCorrectString(char[] digits, int dividerPosition, char divider) {
                final StringBuilder formatted = new StringBuilder();

                for (int i = 0; i < digits.length; i++) {
                    if (digits[i] != 0) {
                        formatted.append(digits[i]);
                        if ((i > 0) && (i < (digits.length - 1)) && (((i + 1) % dividerPosition) == 0)) {
                            formatted.append(divider);
                        }
                    }
                }

                return formatted.toString();
            }

            private char[] getDigitArray(final Editable s, final int size) {
                char[] digits = new char[size];
                int index = 0;
                for (int i = 0; i < s.length() && index < size; i++) {
                    char current = s.charAt(i);
                    if (Character.isDigit(current)) {
                        digits[index] = current;
                        index++;
                    }
                }
                return digits;
            }

        });
        proceed.setOnClickListener(v ->
        {
            try
            {
                validate();
            }
            catch (ParseException e)
            {
                e.printStackTrace();
            }
        });
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback()
        {
            @Override
            public void onStateChanged(@NonNull View view, int newState)
            {
                switch (newState)
                {
                    case BottomSheetBehavior.STATE_HIDDEN:
                    {
                        bg.setVisibility(View.GONE);
                        break;
                    }

                    case BottomSheetBehavior.STATE_EXPANDED:
                    {
                        bg.setVisibility(View.VISIBLE);
                        break;
                    }
                    case BottomSheetBehavior.STATE_COLLAPSED:
                    {
                        bg.setVisibility(View.GONE);
                        break;
                    }
                    case BottomSheetBehavior.STATE_DRAGGING:
                    {
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v)
            {

            }
        });
    }


    private boolean checkExpDate(String date) throws ParseException
    {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/yy");
        simpleDateFormat.setLenient(false);
        Date expiry = simpleDateFormat.parse(date);
        boolean expired = expiry.before(new Date());
        return expired;
    }

    private void validate() throws ParseException
    {
        if (et_card_name.getText().length()<4)
        {
            Constants.SnakeMessageYellow(parent_layout,"Enter valid card name");
        }
        else if (et_card_number.getText().length()<19)
        {
            Constants.SnakeMessageYellow(parent_layout,"Enter valid card number");
        }
        else if (et_exp_date.getText().length()<5)
        {
            Constants.SnakeMessageYellow(parent_layout,"Enter expiry date");
        }
        else if (checkExpDate(et_exp_date.getText().toString()))
        {
            Constants.SnakeMessageYellow(parent_layout,"Enter valid expiry date");
        }
        else if (et_cvv.getText().length()<3)
        {
            Constants.SnakeMessageYellow(parent_layout,"Enter valid cvv");
        }else
        {
            String[] card=et_exp_date.getText().toString().split("/");
            String month=card[0];
            String year=card[1];
            Intent i = new Intent(this, ActivityPayment.class);
            i.putExtra("type", Constants.CASHFREE_CARD);
            i.putExtra("card_number",et_card_number.getText().toString().replace(" ",""));
            i.putExtra("card_name",et_card_name.getText().toString());
            i.putExtra("exp_month",month);
            i.putExtra("exp_year",year);
            i.putExtra("cvv",et_cvv.getText().toString());
            i.putExtra("amount",amount);
            i.putExtra("game_id",game_id);
            i.putExtra("game_type_id",game_type_id);
            i.putExtra("game_name",game_name);
            i.putExtra("is_tournament",is_tournament);
            startActivityForResult(i, Constants.PAYMENT_RESULT);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode== Constants.PAYMENT_RESULT)
        {
            assert data != null;
            if (data.getBooleanExtra(Constants.PAYMENT_STATUS_SUCCESS,true))
            {
                    Intent i = new Intent(ActivityCardDetails.this,ActivityWallet.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
            }
            else
            {
                Constants.SnakeMessageYellow(parent_layout,"Payment failed .Please try again.");
            }

        }
    }
}
