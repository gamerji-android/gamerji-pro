package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.esports.gamerjipro.Adapter.ChatAdapter;
import com.esports.gamerjipro.Adapter.FeedbackAdapter;
import com.esports.gamerjipro.Adapter.FeedbackReasonAdapter;
import com.esports.gamerjipro.Adapter.PlayerListAdapter;
import com.esports.gamerjipro.Adapter.SubmitReportsAdapter;
import com.esports.gamerjipro.Adapter.WinnerPoolAdapter;
import com.esports.gamerjipro.Interface.FeedbackListener;
import com.esports.gamerjipro.Interface.FeedbackReasonListener;
import com.esports.gamerjipro.Interface.TournamentTimeSelectedListener;
import com.esports.gamerjipro.Model.ChatModel;
import com.esports.gamerjipro.Model.ContestDetailsModel;
import com.esports.gamerjipro.Model.ContestPlayersModel;
import com.esports.gamerjipro.Model.GeneralResponseModel;
import com.esports.gamerjipro.Model.ReportsDataModel;
import com.esports.gamerjipro.Model.TournamentTimeSlotModel;
import com.esports.gamerjipro.Model.WinnerContestModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.AnalyticsSocket;
import com.esports.gamerjipro.Utils.Common;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.FileUtils;
import com.esports.gamerjipro.Utils.Pref;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.listeners.IPickResult;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityContestDetails extends AppCompatActivity implements View.OnClickListener, IPickResult, FeedbackListener, FeedbackReasonListener, TournamentTimeSelectedListener {
    RecyclerView rv_players, rv_chat, rv_pool_price, rv_reasons, rv_gif, rv_reports;
    PlayerListAdapter playerListAdapter;
    ChatAdapter chatAdapter;
    RelativeLayout img_ss;
    WinnerPoolAdapter winnerPoolAdapter;
    public TextView txt_rules, txt_ss_info, txt_ss, tv_pool_price, txt_column;
    public LinearLayout lyt_room, reports_bottom;
    //    public CardView cv_ss;
    int page = 1;
    ProgressBar progress_bar;
    APIInterface apiInterface;
    String contest_id, reports_id = "", mRatings;
    ImageView uploaded_ss, iv_close_report;
    String ss_status;
    CoordinatorLayout parent_lyt;
    ArrayList<ChatModel> chatModelArrayList = new ArrayList<>();
    ArrayList<ContestDetailsModel.ContestsData.UsersData> usersDataArrayList = new ArrayList<>();
    String ContestUserID;
    TextView txt_date, txt_time, txt_map, txt_persp, winning_amount, txt_winners, txt_winner_title, txt_perkill, txt_entry_fees, txt_available_spots,
            txt_available_remaining, txt_join_contest, txt_room_id, txt_room_pass, txt_submit_report;

    LinearLayout pool_price_bottom, lyt_winners, lyt_entry_fees, lyt_winnings, lyt_kills, mLinearRatingsMainView;
    RelativeLayout lyt_send_message, feedback_bottom;
    BottomSheetBehavior pool_price_bottomsheet, feedback_bottomsheet, submit_report_bottomsheet, mBottomSheetRatings;
    ImageView img_back, img_close_feedback;
    View bg;
    EditText et_chat;
    MySocket mSocket;
    FrameLayout lyt_send;
    TextView txrupee_symbol, txt_per_kill_title, txt_person_name;
    LinearLayout lyt_columns, lyt_password;
    RelativeLayout lyt_review;
    RelativeLayout lyt_details, txt_details, txt_players, txt_room_chat, txt_rules_tab;
    LinearLayout lyt_player;
    View view_detail, view_player, view_rules, view_chat, txt_slot_view;
    TextView txt_name, txt_acc_number, txt_kill, txt_rank, txt_winning_amount, txt_slot_info;
    ImageView img_pic, mImageCloseRatings;
    LinearLayout lyt_congratulartions;
    ImageView iv_close_pool_price, img_discord, img_youtube_sub, img_fb, img_insta, img_telegram;
    SwipeRefreshLayout swipeRefreshLayout, swipe_refresh_player;
    TextView txt_chat, txt_rule, txt_player, txt_detail, rupees_symbol, txt_entry_rupee, txt_pool_price, txt_winning_rupee;
    Typeface typeface_normal;
    Typeface typeface_semibold;
    String winning_amt, CaptainID;
    LinearLayoutManager linearLayoutManager, player_linearLaoutManager;
    CardView cv_user, card_submit_number, cv_invite_contest, cv_submit_rating;
    TextView txt_name_user, txt_acc_number_user, txt_rank_user, txt_kill_user, txt_winning_amount_user, txt_slots, txt_invite, mTextReportIssue;
    //    ImageView img_view_ss, img_view_ss_user;
    LinearLayout lyt_congratulartions_user, mLinearReportIssue, mLinearRatings, mLinearFeedback;
    RatingBar mRatingBar, rating_bar;
    ImageView img_user, img_user_in_list;
    String screenShotUrl;
    FeedbackAdapter feedbackAdapter;
    FeedbackReasonAdapter feedbackReasonAdapter;
    ArrayList<String> feedback_reason_id = new ArrayList<>();
    EditText et_fb_other;
    String RatingID, discord_link, youtubre_link, fb_link, insta_link, telegram_link;
    TextView img_news;
    String Status;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    String mTotalRatings, mReview;
    private boolean isLastPage = false, isLoading, mRatingsSubmitted, mReviewSubmitted;
    private int currentPage = 0;
    Timer swipeTimer;
    private int NUM_PAGES = 0;
    SubmitReportsAdapter submitReportsAdapter;
    AnalyticsSocket analyticsSocket = new AnalyticsSocket();
    CardView cv_reports_submiit, cv_report_issue, cv_ratings;
    TextView reports_title, reports_issue_title, tv_tournament_time_type, txt_tournament_name, txt_tournament_date, txt_tournamnet_time_notes;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_contest_details);
        img_news = findViewById(R.id.img_news);
        img_news.setOnClickListener(v -> FileUtils.OpenNewsWebview(ActivityContestDetails.this, "https://www.gamerji.com"));
        mSocket = new MySocket();
        contest_id = getIntent().getStringExtra("contest_id");
        rv_players = findViewById(R.id.rv_players);
        et_fb_other = findViewById(R.id.et_fb_other);
        rv_reasons = findViewById(R.id.rv_reasons);
        cv_invite_contest = findViewById(R.id.cv_invite_contest);
        txt_invite = findViewById(R.id.txt_invite);
        rv_gif = findViewById(R.id.rv_gif);
        card_submit_number = findViewById(R.id.card_submit_number);
        iv_close_pool_price = findViewById(R.id.iv_close_pool_price);
        img_youtube_sub = findViewById(R.id.img_youtube_sub);
        img_fb = findViewById(R.id.img_fb);
        img_insta = findViewById(R.id.img_insta);
        img_telegram = findViewById(R.id.img_telegram);
        img_discord = findViewById(R.id.img_discord);
        lyt_player = findViewById(R.id.lyt_player);
        txt_entry_rupee = findViewById(R.id.txt_entry_rupee);
        img_pic = findViewById(R.id.img_pic);
        txt_pool_price = findViewById(R.id.txt_pool_price);
        txt_winning_rupee = findViewById(R.id.txt_winning_rupee);
        txrupee_symbol = findViewById(R.id.txrupee_symbol);
        txt_players = findViewById(R.id.txt_players);
        txt_details = findViewById(R.id.txt_details);
        lyt_details = findViewById(R.id.lyt_details);
        txt_rules_tab = findViewById(R.id.txt_rules_tab);
        txt_room_chat = findViewById(R.id.txt_room_chat);
        lyt_send_message = findViewById(R.id.lyt_send_message);
        view_detail = findViewById(R.id.view_detail);
        view_player = findViewById(R.id.view_player);
        view_rules = findViewById(R.id.view_rules);
        view_chat = findViewById(R.id.view_chat);
        txt_slot_view = findViewById(R.id.txt_slot_view);
        lyt_congratulartions = findViewById(R.id.lyt_congratulartions);
        txt_winning_amount = findViewById(R.id.txt_winning_amount);
        txt_slot_info = findViewById(R.id.txt_slot_info);
        swipeRefreshLayout = findViewById(R.id.swipe_refresh);
        swipe_refresh_player = findViewById(R.id.swipe_refresh_player);
        txt_chat = findViewById(R.id.txt_chat);
        txt_rule = findViewById(R.id.txt_rule);
        txt_player = findViewById(R.id.txt_player);
        txt_detail = findViewById(R.id.txt_detail);
        img_user = findViewById(R.id.img_user);
        txt_person_name = findViewById(R.id.txt_person_name);
        swipeTimer = new Timer();
        img_user_in_list = findViewById(R.id.img_user_in_list);

        cv_user = findViewById(R.id.cv_user);
        txt_name_user = findViewById(R.id.txt_name_user);
        txt_acc_number_user = findViewById(R.id.txt_acc_number_user);
        txt_rank_user = findViewById(R.id.txt_rank_user);
        txt_kill_user = findViewById(R.id.txt_kill_user);
//        img_view_ss_user = findViewById(R.id.img_view_ss_user);
        lyt_congratulartions_user = findViewById(R.id.lyt_congratulartions_user);
        txt_slots = findViewById(R.id.txt_slots);
        txt_winning_amount_user = findViewById(R.id.txt_winning_amount_user);
        cv_report_issue = findViewById(R.id.cv_report_issue);
        mLinearReportIssue = findViewById(R.id.linear_contest_detail_report_issue);
        cv_ratings = findViewById(R.id.cv_ratings);
        mLinearRatings = findViewById(R.id.linear_contest_detail_ratings);
        mLinearFeedback = findViewById(R.id.linear_contest_details_feedback);
        mRatingBar = findViewById(R.id.rating_bar_contest_details);
        mTextReportIssue = findViewById(R.id.text_contest_details_report_issue);
        typeface_normal = ResourcesCompat.getFont(ActivityContestDetails.this, R.font.poppins);
        typeface_semibold = ResourcesCompat.getFont(ActivityContestDetails.this, R.font.poppins_semibold);


        lyt_send = findViewById(R.id.lyt_send);
        et_chat = findViewById(R.id.et_chat);
        rv_chat = findViewById(R.id.rv_chat);
        parent_lyt = findViewById(R.id.parent_lyt);
        rv_pool_price = findViewById(R.id.rv_pool_price);
        bg = findViewById(R.id.bg);
        lyt_review = findViewById(R.id.lyt_review);
//        cv_ss = findViewById(R.id.cv_ss);
        uploaded_ss = findViewById(R.id.uploaded_ss);
        img_ss = findViewById(R.id.img_ss);
        txt_ss_info = findViewById(R.id.txt_ss_info);
        txt_ss = findViewById(R.id.txt_ss);
        txt_rules = findViewById(R.id.txt_rules);
        progress_bar = findViewById(R.id.progress_bar);
        lyt_room = findViewById(R.id.lyt_room);
        txt_room_id = findViewById(R.id.txt_room_id);
        tv_pool_price = findViewById(R.id.tv_pool_price);
        txt_room_pass = findViewById(R.id.txt_room_pass);
        lyt_winners = findViewById(R.id.lyt_winners);
        lyt_entry_fees = findViewById(R.id.lyt_entry_fees);
        lyt_winnings = findViewById(R.id.lyt_winnings);
        lyt_kills = findViewById(R.id.lyt_kills);
        img_back = findViewById(R.id.img_back);
        txt_column = findViewById(R.id.txt_column);
        img_close_feedback = findViewById(R.id.img_close_feedback);
        txt_per_kill_title = findViewById(R.id.txt_per_kill_title);
        lyt_columns = findViewById(R.id.lyt_columns);
        lyt_password = findViewById(R.id.lyt_password);
        rupees_symbol = findViewById(R.id.rupees_symbol);
        img_back.setOnClickListener(v ->
        {
            if (getIntent().hasExtra("invite")) {
                if (getIntent().getBooleanExtra("invite", false)) {
//                    Intent i = new Intent(this, ActivityMain.class);
                    Intent i = new Intent(this, BottomNavigationActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    finish();
                } else {
                    finish();
                }
            } else {
                finish();
            }
        });
        img_close_feedback.setOnClickListener(v -> feedback_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED));

        txt_date = findViewById(R.id.txt_date);
        txt_time = findViewById(R.id.txt_time);
        txt_map = findViewById(R.id.txt_map);
        txt_persp = findViewById(R.id.txt_persp);
        winning_amount = findViewById(R.id.winning_amount);
        txt_winners = findViewById(R.id.txt_winners);
        txt_perkill = findViewById(R.id.txt_perkill);
        txt_entry_fees = findViewById(R.id.txt_entry_fees);
        txt_available_spots = findViewById(R.id.txt_available_spots);
        txt_available_remaining = findViewById(R.id.txt_available_remaining);
        txt_join_contest = findViewById(R.id.txt_join_contest);
        progress_bar = findViewById(R.id.progress_bar);
        txt_winner_title = findViewById(R.id.txt_winner_title);
        txt_rules.setMovementMethod(new ScrollingMovementMethod());
        pool_price_bottom = findViewById(R.id.pool_price_bottom);
        feedback_bottom = findViewById(R.id.feedback_layout);
        mLinearRatingsMainView = findViewById(R.id.linear_ratings_main_view);
        pool_price_bottomsheet = BottomSheetBehavior.from(pool_price_bottom);
        feedback_bottomsheet = BottomSheetBehavior.from(feedback_bottom);
        mBottomSheetRatings = BottomSheetBehavior.from(mLinearRatingsMainView);
        mImageCloseRatings = mLinearRatingsMainView.findViewById(R.id.img_close_rating);
        cv_submit_rating = mLinearRatingsMainView.findViewById(R.id.cv_submit_rating);
        rating_bar = mLinearRatingsMainView.findViewById(R.id.rating);

        reports_bottom = findViewById(R.id.tournament_time_bottomsheet);
        submit_report_bottomsheet = BottomSheetBehavior.from(reports_bottom);


        reports_title = reports_bottom.findViewById(R.id.txt_tournament_time_title);
        reports_title.setText("Report Issue");
        reports_title.setTextColor(Color.RED);
        reports_issue_title = reports_bottom.findViewById(R.id.txt_time_title);
        reports_issue_title.setText("Select Issue");
        iv_close_report = reports_bottom.findViewById(R.id.iv_close_tournament_time);
        tv_tournament_time_type = reports_bottom.findViewById(R.id.tv_tournament_time_type);

        tv_tournament_time_type.setVisibility(View.GONE);
        txt_tournament_name = reports_bottom.findViewById(R.id.txt_tournament_name);
        txt_tournament_name.setVisibility(View.GONE);
        txt_tournament_date = reports_bottom.findViewById(R.id.txt_tournament_date);
        txt_tournament_date.setVisibility(View.GONE);
        txt_tournamnet_time_notes = reports_bottom.findViewById(R.id.txt_tournamnet_time_notes);
        txt_tournamnet_time_notes.setVisibility(View.GONE);

        rv_reports = reports_bottom.findViewById(R.id.rv_tournament_time);
        txt_submit_report = reports_bottom.findViewById(R.id.txt_join_contest);
        txt_submit_report.setText(R.string.submit);
        cv_reports_submiit = reports_bottom.findViewById(R.id.cv_tournament_time_next);

        txt_name = findViewById(R.id.txt_name);
        txt_acc_number = findViewById(R.id.txt_acc_number);
        txt_kill = findViewById(R.id.txt_kill);
        txt_rank = findViewById(R.id.txt_rank);
//        img_view_ss = findViewById(R.id.img_view_ss);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        player_linearLaoutManager = new LinearLayoutManager(this);
        rv_players.setLayoutManager(player_linearLaoutManager);
        rv_players.setItemAnimator(new DefaultItemAnimator());


        linearLayoutManager = new LinearLayoutManager(this);
        rv_chat.setLayoutManager(linearLayoutManager);
        rv_chat.setItemAnimator(new DefaultItemAnimator());

        rv_pool_price.setLayoutManager(new LinearLayoutManager(this));
        rv_pool_price.setItemAnimator(new DefaultItemAnimator());

//        cv_ss.setOnClickListener(this);
        getData();
        txt_details.setOnClickListener(this);
        txt_room_chat.setOnClickListener(this);
        txt_rules_tab.setOnClickListener(this);
        lyt_details.setOnClickListener(this);
        uploaded_ss.setOnClickListener(this);
        txt_players.setOnClickListener(this);
        cv_invite_contest.setOnClickListener(this);
        cv_reports_submiit.setOnClickListener(this);
        mImageCloseRatings.setOnClickListener(this);
        cv_submit_rating.setOnClickListener(this);

//        img_view_ss.setOnClickListener(this);
        card_submit_number.setOnClickListener(this);
        iv_close_pool_price.setOnClickListener(this);
        img_discord.setOnClickListener(this);
        img_youtube_sub.setOnClickListener(this);
        img_telegram.setOnClickListener(this);
        img_fb.setOnClickListener(this);
        img_insta.setOnClickListener(this);
        cv_ratings.setOnClickListener(this);
        iv_close_report.setOnClickListener(this);

        cv_report_issue.setOnClickListener(v -> {
            submit_report_bottomsheet.setState(BottomSheetBehavior.STATE_EXPANDED);
            bg.setVisibility(View.VISIBLE);
        });

        lyt_winners.setOnClickListener(v ->
                getWinnerPool());
        Glide.with(this).load(Pref.getValue(ActivityContestDetails.this, Constants.ProfileImage, "", Constants.FILENAME)).placeholder(getDrawable(R.drawable.logo)).into(img_pic);

        pool_price_bottomsheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                switch (i) {
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        bg.setVisibility(View.GONE);
                        break;
                    }
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        bg.setVisibility(View.VISIBLE);
                    }
                    break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });

        chatAdapter = new ChatAdapter(ActivityContestDetails.this, chatModelArrayList);
        rv_chat.setAdapter(chatAdapter);


        mSocket.connect();
        mSocket.joinChatRoom();
        mSocket.on();
        mSocket.requestallChatMessage();

        new Handler().postDelayed(() ->
        {


        }, 500);

        lyt_send.setOnClickListener(v ->
                {
                    if (!et_chat.getText().toString().trim().isEmpty())
                        mSocket.sendMessage(et_chat.getText().toString());
                    else
                        Constants.SnakeMessageYellow(parent_lyt, "Enter message to send");
                }
        );

        /*rv_chat.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
            if (bottom < oldBottom)
            {
                if(chatModelArrayList.size()>1)
                {
                    rv_chat.postDelayed(() -> rv_chat.smoothScrollToPosition(
                            Objects.requireNonNull(rv_chat.getAdapter()).getItemCount()), 100);
                }
            }
        });*/

        txt_person_name.setText(Pref.getValue(ActivityContestDetails.this, Constants.firstname, "", Constants.FILENAME) + " " +
                Pref.getValue(ActivityContestDetails.this, Constants.firstname, "", Constants.FILENAME));

        swipeRefreshLayout.setOnRefreshListener(this::getData);
        swipe_refresh_player.setOnRefreshListener(this::getData);

        rv_players.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                Common.insertLog("Last Item Wow !");
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = player_linearLaoutManager.getChildCount();
                    totalItemCount = player_linearLaoutManager.getItemCount();
                    pastVisiblesItems = player_linearLaoutManager.findFirstVisibleItemPosition();

                    if (!isLastPage && !isLoading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            Common.insertLog("Last Item Wow !");
                            isLoading = true;
                            getContestPlayers(++page);
                        }
                    }
                }
            }
        });

        submit_report_bottomsheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        bg.setVisibility(View.VISIBLE);
                    }
                    break;

                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        bg.setVisibility(View.GONE);
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:

                    case BottomSheetBehavior.STATE_SETTLING: {
                        break;
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        mBottomSheetRatings.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        bg.setVisibility(View.VISIBLE);
                    }
                    break;

                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        bg.setVisibility(View.GONE);
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:

                    case BottomSheetBehavior.STATE_SETTLING: {
                        break;
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (getIntent().hasExtra("invite")) {
            if (getIntent().getBooleanExtra("invite", false)) {
//                Intent i = new Intent(this, ActivityMain.class);
                Intent i = new Intent(this, BottomNavigationActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            } else {
                finish();
            }
        } else {
            finish();
        }

    }

    private void getWinnerPool() {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityContestDetails.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<WinnerContestModel> contestDetailsModelCall = apiInterface.getContestWinner(Constants.GETSINGLECONTESTWINNER, contest_id);

        contestDetailsModelCall.enqueue(new Callback<WinnerContestModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<WinnerContestModel> call, @NonNull Response<WinnerContestModel> response) {
                progressDialog.dismiss();
                WinnerContestModel winnerContestModel = response.body();
                assert winnerContestModel != null;
                if (winnerContestModel.status.equalsIgnoreCase("success")) {
                    winnerPoolAdapter = new WinnerPoolAdapter(ActivityContestDetails.this, winnerContestModel.data.prizePoolsData);
                    rv_pool_price.setAdapter(winnerPoolAdapter);

                    if (winning_amt.equalsIgnoreCase("") || winning_amt.equalsIgnoreCase("0")) {
                        txt_pool_price.setVisibility(View.GONE);
                        txt_winning_rupee.setVisibility(View.GONE);
                    } else {
                        txt_pool_price.setVisibility(View.VISIBLE);
                        txt_winning_rupee.setVisibility(View.VISIBLE);
                    }

                    pool_price_bottomsheet.setState(BottomSheetBehavior.STATE_EXPANDED);
                    bg.setVisibility(View.VISIBLE);
                } else {
                    Constants.SnakeMessageYellow(parent_lyt, winnerContestModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<WinnerContestModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void getData() {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityContestDetails.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<ContestDetailsModel> contestDetailsModelCall = apiInterface.getContestDetails(Constants.GET_SINGLE_CONTEST, Pref.getValue(ActivityContestDetails.this, Constants.UserID, "", Constants.FILENAME), contest_id);

        contestDetailsModelCall.enqueue(new Callback<ContestDetailsModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<ContestDetailsModel> call, @NonNull Response<ContestDetailsModel> response) {

                try {
                    Common.insertLog("Response:::> " + response.toString());
                    progressDialog.dismiss();
                    swipeRefreshLayout.setRefreshing(false);
                    swipe_refresh_player.setRefreshing(false);
                    ContestDetailsModel contestDetailsModel = response.body();
                    assert contestDetailsModel != null;
                    if (contestDetailsModel.status.equalsIgnoreCase("success")) {
                        @SuppressLint("SimpleDateFormat") DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                        @SuppressLint("SimpleDateFormat") DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
                        String inputDateStr = contestDetailsModel.contestsData.Date;
                        CaptainID = contestDetailsModel.contestsData.CaptainID;

                    /*if(Pref.getValue(ActivityContestDetails.this,Constants.UserID,"",Constants.FILENAME)
                            .equalsIgnoreCase(contestDetailsModel.contestsData.CaptainID))
                        cv_ss.setVisibility(View.VISIBLE);
                    else
                        cv_ss.setVisibility(View.GONE);*/

                        Date date = null;
                        try {
                            date = inputFormat.parse(inputDateStr);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        String outputDateStr = outputFormat.format(date);
                        txt_date.setText(outputDateStr);
                        txt_time.setText(FileUtils.getFormatedDateTime(contestDetailsModel.contestsData.Time, "HH:mm:ss", "hh:mm a"));

                        Date c = Calendar.getInstance().getTime();
                        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
                        Date current_date, game_date;
                        String formattedDate = df.format(c);
                        try {
                            current_date = df.parse(formattedDate);
                            game_date = df.parse(outputDateStr + " " + FileUtils.getFormatedDateTime(contestDetailsModel.contestsData.Time, "HH:mm:ss", "hh:mm a"));

                            if (FileUtils.checkDate(game_date, current_date)) {
                                cv_invite_contest.setVisibility(View.VISIBLE);
                                txt_invite.setVisibility(View.VISIBLE);
                            } else {
//                                cv_invite_contest.setVisibility(View.INVISIBLE);
//                                txt_invite.setVisibility(View.INVISIBLE);
                                cv_invite_contest.setVisibility(View.GONE);
                                txt_invite.setVisibility(View.GONE);
                            }

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        NUM_PAGES = contestDetailsModel.contestsData.AdsCount;
                        final Handler handler = new Handler();
                        final Runnable Update = () ->
                        {
                            if (currentPage == NUM_PAGES) {
                                currentPage = 0;
                                //swipeTimer.cancel();
                            }
                        };

                        swipeTimer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                handler.post(Update);
                            }
                        }, 0, 5000);

                        txt_map.setText(contestDetailsModel.contestsData.Map_Length);
                        txt_persp.setText(contestDetailsModel.contestsData.Perspective_LevelCap);
                        winning_amt = contestDetailsModel.contestsData.WinningAmount;
                        if (contestDetailsModel.contestsData.WinningAmount.equalsIgnoreCase("") || contestDetailsModel.contestsData.WinningAmount.equalsIgnoreCase("0")) {
                            winning_amount.setText("-");
                            rupees_symbol.setVisibility(View.GONE);
                        } else
                            winning_amount.setText(" " + contestDetailsModel.contestsData.WinningAmount);

                        discord_link = contestDetailsModel.contestsData.DiscordLink;
                        youtubre_link = contestDetailsModel.contestsData.ChannelLink;
                        fb_link = contestDetailsModel.contestsData.FacebookLink;
                        insta_link = contestDetailsModel.contestsData.InstagramLink;
                        telegram_link = contestDetailsModel.contestsData.TelegramLink;
                        mRatingsSubmitted = contestDetailsModel.contestsData.RatingSubmitted;
                        mReviewSubmitted = contestDetailsModel.contestsData.ReviewSubmitted;
                        mTotalRatings = contestDetailsModel.contestsData.RatingSubmittedValue;
                        mReview = contestDetailsModel.contestsData.ReviewSubmittedValue;

                        tv_pool_price.setText(contestDetailsModel.contestsData.WinningAmount);
                        txt_winners.setText(contestDetailsModel.contestsData.WinnersCount);
                        txt_per_kill_title.setText(contestDetailsModel.contestsData.PerKill_MaxLosesTitle);


                        if (contestDetailsModel.contestsData.PerKill_MaxLosesCurrency)
                            txrupee_symbol.setVisibility(View.VISIBLE);
                        else
                            txrupee_symbol.setVisibility(View.GONE);

                        if (contestDetailsModel.contestsData.WinnersCount.isEmpty() && contestDetailsModel.contestsData.PerKill_MaxLoses.isEmpty()) {
                            lyt_winners.setVisibility(View.GONE);
                            lyt_kills.setVisibility(View.GONE);
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 3.0f);
                            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 1.0f);
                            lyt_winnings.setLayoutParams(params);
                            lyt_entry_fees.setLayoutParams(params1);

                        } else if (contestDetailsModel.contestsData.WinnersCount.isEmpty()) {
                            lyt_winners.setVisibility(View.GONE);
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 2.0f);
                            lyt_kills.setLayoutParams(params);
                        } else if (contestDetailsModel.contestsData.PerKill_MaxLoses.isEmpty()) {
                            lyt_kills.setVisibility(View.GONE);
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 2.0f);
                            lyt_winners.setLayoutParams(params);
                        } else {
                            if (Integer.parseInt(contestDetailsModel.contestsData.WinnersCount) == 1)
                                txt_winner_title.setText("Winner");
                        }

                        if (!contestDetailsModel.contestsData.RoomColumn.isEmpty()) {
                            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 1.0f);
                            lyt_columns.setVisibility(View.VISIBLE);
                            lyt_columns.setLayoutParams(params1);
                            lyt_room.setLayoutParams(params1);
                            lyt_password.setLayoutParams(params1);
                            txt_column.setText(contestDetailsModel.contestsData.RoomColumn);
                            lyt_password.setLayoutParams(params1);
                        } else {
                            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 1.5f);
                            lyt_columns.setVisibility(View.GONE);
                            txt_slot_info.setVisibility(View.GONE);
                            lyt_password.setLayoutParams(params1);
                            lyt_room.setLayoutParams(params1);
                        }


                        txt_perkill.setText(" " + contestDetailsModel.contestsData.PerKill_MaxLoses);
                        if (contestDetailsModel.contestsData.EntryFee.equalsIgnoreCase("0")) {
                            txt_entry_fees.setText("Free");
                            txt_entry_rupee.setVisibility(View.GONE);
                        } else
                            txt_entry_fees.setText(" " + contestDetailsModel.contestsData.EntryFee);

                        if (contestDetailsModel.contestsData.RoomID.isEmpty())
                            txt_room_id.setText("-");
                        else
                            txt_room_id.setText(contestDetailsModel.contestsData.RoomID);

                        if (contestDetailsModel.contestsData.RoomPassword.isEmpty())

                            txt_room_pass.setText("-");
                        else
                            txt_room_pass.setText(contestDetailsModel.contestsData.RoomPassword);


                        int available_spots = Integer.parseInt(contestDetailsModel.contestsData.TotalSpots);
                        int joined_spots = Integer.parseInt(contestDetailsModel.contestsData.JoinedSpots);
                        int remaining_spots = available_spots - joined_spots;
                        if (remaining_spots > 1)
                            txt_available_spots.setText(remaining_spots + " players remaining");
                        else
                            txt_available_spots.setText(remaining_spots + " player remaining");
                        if (joined_spots > 1)
                            txt_available_remaining.setText(joined_spots + " players joined");
                        else
                            txt_available_remaining.setText(joined_spots + " player joined");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            txt_rules.setText(Html.fromHtml(contestDetailsModel.contestsData.Rules, Html.FROM_HTML_SEPARATOR_LINE_BREAK_DIV));
                        } else {
                            txt_rules.setText(Html.fromHtml(contestDetailsModel.contestsData.Rules));
                        }
                        progress_bar.setMax(available_spots);
                        progress_bar.setProgress(joined_spots);
                        ss_status = contestDetailsModel.contestsData.Status;
                        setLayout(contestDetailsModel.contestsData);

                        txt_name.setText(contestDetailsModel.contestsData.CurrentUserData.Name);
                        ContestUserID = contestDetailsModel.contestsData.CurrentUserData.ContestUserID;
                        txt_name_user.setText(contestDetailsModel.contestsData.CurrentUserData.Name);
                        if (!contestDetailsModel.contestsData.CurrentUserData.MobileNumber.isEmpty()) {
                            txt_acc_number.setText("*****" + contestDetailsModel.contestsData.CurrentUserData.MobileNumber.substring(5));
                            txt_acc_number_user.setText("*****" + contestDetailsModel.contestsData.CurrentUserData.MobileNumber.substring(5));
                        }
                        if (contestDetailsModel.contestsData.CurrentUserData.Kills.isEmpty()) {
                            txt_kill.setText("-");
                            txt_kill_user.setText("-");
                        } else {
                            txt_kill.setText(contestDetailsModel.contestsData.CurrentUserData.Kills);
                            txt_kill_user.setText(contestDetailsModel.contestsData.CurrentUserData.Kills);
                        }

                        if (contestDetailsModel.contestsData.CurrentUserData.Rank.isEmpty()) {
                            txt_rank.setText("-");
                            txt_rank_user.setText("-");
                        } else {
                            txt_rank.setText(contestDetailsModel.contestsData.CurrentUserData.Rank);
                            txt_rank_user.setText(contestDetailsModel.contestsData.CurrentUserData.Rank);
                        }

                        if (contestDetailsModel.contestsData.Status.equalsIgnoreCase(Constants.COMPLETED) && !contestDetailsModel.contestsData.CurrentUserData.WinningAmount.isEmpty()) {
                            lyt_congratulartions.setVisibility(View.VISIBLE);
                            lyt_congratulartions_user.setVisibility(View.VISIBLE);
                            txt_winning_amount.setText(" " + contestDetailsModel.contestsData.CurrentUserData.WinningAmount);
                            txt_winning_amount_user.setText(" " + contestDetailsModel.contestsData.CurrentUserData.WinningAmount);
                        }

                        Glide.with(ActivityContestDetails.this).load(contestDetailsModel.contestsData.CurrentUserData.ScreenshotURL).into(uploaded_ss);
                        Glide.with(ActivityContestDetails.this).load(contestDetailsModel.contestsData.CurrentUserData.UserProfileIcon).into(img_user);
                        Glide.with(ActivityContestDetails.this).load(contestDetailsModel.contestsData.CurrentUserData.UserProfileIcon).into(img_user_in_list);
                        /*if (contestDetailsModel.contestsData.CurrentUserData.ScreenshotFlag) {
                            img_view_ss.setImageDrawable(getResources().getDrawable(R.drawable.ic_check));
                            img_view_ss_user.setImageDrawable(getResources().getDrawable(R.drawable.ic_check));
                            img_view_ss.setEnabled(true);
                            img_view_ss_user.setEnabled(true);

                            screenShotUrl = contestDetailsModel.contestsData.CurrentUserData.ScreenshotURL;
                            img_view_ss.setOnClickListener(v ->
                            {
                                Intent i1 = new Intent(ActivityContestDetails.this, Full_Image_Activity.class);
                                i1.putExtra("image", screenShotUrl);
                                startActivity(i1);
                            });

                            img_view_ss_user.setOnClickListener(v ->
                            {
                                Intent i1 = new Intent(ActivityContestDetails.this, Full_Image_Activity.class);
                                i1.putExtra("image", screenShotUrl);
                                startActivity(i1);
                            });

                        } else {
                            img_view_ss.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
                            img_view_ss_user.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
                            img_view_ss.setEnabled(false);
                            img_view_ss_user.setEnabled(false);
                        }*/

                        for (int i = 0; i < contestDetailsModel.contestsData.usersDataArrayList.size(); i++) {
                            if (ContestUserID.equalsIgnoreCase(contestDetailsModel.contestsData.usersDataArrayList.get(i).ContestUserID)) {
                                contestDetailsModel.contestsData.usersDataArrayList.remove(i);
                                break;
                            }
                        }
                        usersDataArrayList = contestDetailsModel.contestsData.usersDataArrayList;
                        Status = contestDetailsModel.contestsData.Status;
                        playerListAdapter = new PlayerListAdapter(ActivityContestDetails.this, usersDataArrayList, Status);
                        rv_players.setAdapter(playerListAdapter);
                    /*for (int i=0;i<usersDataArrayList.size();i++)
                    {
                        if (CaptainID.equalsIgnoreCase(usersDataArrayList.get(i).UserID))
                        {
                            usersDataArrayList.remove(i);
                            playerListAdapter=new PlayerListAdapter(ActivityContestDetails.this,usersDataArrayList,contestDetailsModel.contestsData.Status);
                            rv_players.setAdapter(playerListAdapter);
                            break;
                        }
                    }*/
                        /*if (contestDetailsModel.contestsData.RatingEnabled)
                            cv_ratings.setVisibility(View.VISIBLE);*/

                        Common.insertLog("Ratings Enabled:::> " +contestDetailsModel.contestsData.RatingEnabled);
                        Common.insertLog("Reports Enabled:::> " +contestDetailsModel.contestsData.ReportEnabled);

                        if (contestDetailsModel.contestsData.RatingEnabled && contestDetailsModel.contestsData.ReportEnabled) {
                            mLinearFeedback.setVisibility(View.VISIBLE);

                            if (contestDetailsModel.contestsData.RatingEnabled) {
                                if (mRatingsSubmitted) {
                                    cv_ratings.setVisibility(View.GONE);
                                    mLinearRatings.setVisibility(View.VISIBLE);
                                    mRatingBar.setRating(Float.parseFloat(contestDetailsModel.contestsData.RatingSubmittedValue));
                                } else {
                                    cv_ratings.setVisibility(View.VISIBLE);
                                    mLinearRatings.setVisibility(View.GONE);
                                }
                            } else {
                                cv_ratings.setVisibility(View.GONE);
                            }

                            if (contestDetailsModel.contestsData.ReportEnabled) {
                                SetReportsList(contestDetailsModel.contestsData.ReportsData);
                                if (mReviewSubmitted) {
                                    cv_report_issue.setVisibility(View.GONE);
                                    mLinearReportIssue.setVisibility(View.VISIBLE);
                                    mTextReportIssue.setSelected(true);
                                    mTextReportIssue.setText(contestDetailsModel.contestsData.ReviewSubmittedValue);
                                } else {
                                    cv_report_issue.setVisibility(View.VISIBLE);
                                    mLinearReportIssue.setVisibility(View.GONE);
                                }
                            } else {
                                cv_report_issue.setVisibility(View.GONE);
                            }
                        } else {
                            mLinearFeedback.setVisibility(View.GONE);
                        }

                        if (Integer.parseInt(contestDetailsModel.contestsData.RatingsCount) > 0) {
                            showFeedback(contestDetailsModel.contestsData.ratingsDataArrayList, contestDetailsModel.contestsData.RatingsComment);
                        }
                    } else {
                        Constants.SnakeMessageYellow(parent_lyt, contestDetailsModel.message);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ContestDetailsModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
                swipe_refresh_player.setRefreshing(false);
            }
        });
    }

    private void SetReportsList(ArrayList<ReportsDataModel.ReportsData> reportsData) {
        submitReportsAdapter = new SubmitReportsAdapter(ActivityContestDetails.this, reportsData, ActivityContestDetails.this);
        rv_reports.setAdapter(submitReportsAdapter);
        rv_reports.addItemDecoration(new DividerItemDecoration(ActivityContestDetails.this,
                DividerItemDecoration.VERTICAL));

    }


    private void getContestPlayers(int page) {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityContestDetails.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<ContestPlayersModel> contestPlayersModelCall;

        contestPlayersModelCall = apiInterface.getContestsPlayers(Constants.GET_PLAYERS, Pref.getValue(ActivityContestDetails.this, Constants.UserID, "", Constants.FILENAME), contest_id, page, 20);

        contestPlayersModelCall.enqueue(new Callback<ContestPlayersModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<ContestPlayersModel> call, @NonNull Response<ContestPlayersModel> response) {
                progressDialog.dismiss();
                isLoading = false;
                ContestPlayersModel contestPlayersModel = response.body();
                feedback_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);

                assert contestPlayersModel != null;
                if (contestPlayersModel.status.equalsIgnoreCase("success")) {
                    for (int i = 0; i < contestPlayersModel.Data.userDataArrayList.size(); i++) {
                        if (ContestUserID.equalsIgnoreCase(contestPlayersModel.Data.userDataArrayList.get(i).ContestUserID)) {
                            contestPlayersModel.Data.userDataArrayList.remove(i);
                            break;
                        }
                    }

                    usersDataArrayList.addAll(player_linearLaoutManager.getItemCount(), contestPlayersModel.Data.userDataArrayList);
                    playerListAdapter.notifyDataSetChanged();
                    isLastPage = contestPlayersModel.Data.IsLast;
                } else
                    Constants.SnakeMessageYellow(parent_lyt, contestPlayersModel.message);

            }

            @Override
            public void onFailure(@NonNull Call<ContestPlayersModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
                feedback_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });
    }

    private void showFeedback(ArrayList<ContestDetailsModel.ContestsData.RatingsData> ratingsDataArrayList, String ratingsComment) {

        if (Integer.parseInt(ratingsComment) == 2)
            et_fb_other.setVisibility(View.VISIBLE);
        else
            et_fb_other.setVisibility(View.GONE);


        feedback_bottomsheet.setState(BottomSheetBehavior.STATE_EXPANDED);
        feedbackAdapter = new FeedbackAdapter(ActivityContestDetails.this, ratingsDataArrayList, ActivityContestDetails.this);
        rv_gif.setAdapter(feedbackAdapter);

        feedback_bottomsheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                switch (i) {
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        bg.setVisibility(View.GONE);
                        img_discord.setClickable(true);
                        img_youtube_sub.setClickable(true);
                        img_fb.setClickable(true);
                        img_insta.setClickable(true);
                        img_telegram.setClickable(true);
                        cv_invite_contest.setClickable(true);
                        break;
                    }
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        bg.setVisibility(View.VISIBLE);
                        img_discord.setClickable(false);
                        img_youtube_sub.setClickable(false);
                        img_fb.setClickable(false);
                        img_insta.setClickable(false);
                        img_telegram.setClickable(false);
                        cv_invite_contest.setClickable(false);

                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_HALF_EXPANDED:
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void setLayout(ContestDetailsModel.ContestsData contestsData) {
        if (contestsData.Status.equals(Constants.REVIEW)) {
            lyt_review.setVisibility(View.VISIBLE);
        }
//        else if (contestsData.Status.equals(Constants.COMPLETED))
//            cv_ss.setEnabled(false);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_details: {
                swipeRefreshLayout.setVisibility(View.VISIBLE);
                swipe_refresh_player.setVisibility(View.GONE);
                txt_rules.setVisibility(View.GONE);
                rv_chat.setVisibility(View.GONE);
                lyt_player.setVisibility(View.GONE);
                lyt_send_message.setVisibility(View.GONE);
                lyt_details.setVisibility(View.VISIBLE);
                view_chat.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                view_detail.setBackgroundColor(getResources().getColor(R.color.orange_color));
                view_player.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                view_rules.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

                txt_detail.setTypeface(typeface_semibold);
                txt_player.setTypeface(typeface_normal);
                txt_rule.setTypeface(typeface_normal);
                txt_chat.setTypeface(typeface_normal);
                break;
            }

            case R.id.txt_rules_tab: {
                swipeRefreshLayout.setVisibility(View.GONE);
                swipe_refresh_player.setVisibility(View.GONE);
                txt_rules.setVisibility(View.VISIBLE);
                lyt_player.setVisibility(View.GONE);
                lyt_details.setVisibility(View.GONE);
                rv_chat.setVisibility(View.GONE);
                lyt_send_message.setVisibility(View.GONE);
                view_chat.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                view_rules.setBackgroundColor(getResources().getColor(R.color.orange_color));
                view_player.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                view_detail.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

                txt_rule.setTypeface(typeface_semibold);
                txt_player.setTypeface(typeface_normal);
                txt_detail.setTypeface(typeface_normal);
                txt_chat.setTypeface(typeface_normal);
                break;
            }

            case R.id.txt_room_chat: {
                swipeRefreshLayout.setVisibility(View.GONE);
                swipe_refresh_player.setVisibility(View.GONE);
                txt_rules.setVisibility(View.GONE);
                rv_chat.setVisibility(View.VISIBLE);
                lyt_player.setVisibility(View.GONE);
                lyt_send_message.setVisibility(View.VISIBLE);
                lyt_details.setVisibility(View.GONE);
                view_detail.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                view_chat.setBackgroundColor(getResources().getColor(R.color.orange_color));
                view_player.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                view_rules.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

                txt_chat.setTypeface(typeface_semibold);
                txt_player.setTypeface(typeface_normal);
                txt_rule.setTypeface(typeface_normal);
                txt_detail.setTypeface(typeface_normal);


                break;
            }
            case R.id.txt_players: {
                swipeRefreshLayout.setVisibility(View.GONE);
                swipe_refresh_player.setVisibility(View.VISIBLE);
                txt_rules.setVisibility(View.GONE);
                lyt_player.setVisibility(View.VISIBLE);
                rv_chat.setVisibility(View.GONE);
                lyt_send_message.setVisibility(View.GONE);
                lyt_details.setVisibility(View.GONE);
                view_chat.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                view_player.setBackgroundColor(getResources().getColor(R.color.orange_color));
                view_detail.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                view_rules.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

                txt_player.setTypeface(typeface_semibold);
                txt_detail.setTypeface(typeface_normal);
                txt_chat.setTypeface(typeface_normal);
                txt_rule.setTypeface(typeface_normal);

                /*if (usersDataArrayList.size()>=20)
                {
                    getContestPlayers(++page);
                }*/
                break;
            }

            /*case R.id.cv_ss: {
                if (ss_status.equals(Constants.WAITING))
                    Constants.SnakeMessageYellow(parent_lyt, "You can upload a screenshot once game is in progress or review mode.");
                else if (ss_status.equals(Constants.STARTED))
                    Constants.SnakeMessageYellow(parent_lyt, "You can upload a screenshot once game is in progress or review mode.");
                else if (ss_status.equals(Constants.IN_PROGRESS))
                    PickImageDialog.build(new PickSetup()).show(this);
                else if (ss_status.equals(Constants.REVIEW))
                    PickImageDialog.build(new PickSetup()).show(this);
                else if (ss_status.equals(Constants.COMPLETED))
                    cv_ss.setVisibility(View.GONE);

                break;
            }*/
            case R.id.iv_close_pool_price: {
                pool_price_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
                break;
            }
            case R.id.card_submit_number: {
                if (!feedback_reason_id.isEmpty())
                    sendFeedbackToServer();
                else
                    Constants.SnakeMessageYellow(parent_lyt, "Please select a reason before submitting.");
                break;
            }
            case R.id.cv_invite_contest: {
                DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
                        .setLink(Uri.parse("https://www.gamerji.com/ContestDetail?category_id=" + contest_id))
                        .setDomainUriPrefix("https://gamerji.page.link")
                        .setAndroidParameters(
                                new DynamicLink.AndroidParameters.Builder("com.esports.gamerjipro")
                                        .setMinimumVersion(125)
                                        .setFallbackUrl(Uri.parse("https://www.gamerji.com"))
                                        .build())
                        .setIosParameters(
                                new DynamicLink.IosParameters.Builder("com.fantasyjiesport")
                                        .setAppStoreId("1466052584")
                                        .setMinimumVersion("1.0")
                                        .build())
                        .setSocialMetaTagParameters(
                                new DynamicLink.SocialMetaTagParameters.Builder()
                                        .setTitle("Hey! Join me on Gamerji for this exciting match. Click on the following link and let's play together.")
                                        .setDescription("Gamerji")
                                        .build())
                        .buildDynamicLink();  // Or buildShortDynamicLink()


                Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                        .setLongLink(Uri.parse(dynamicLink.getUri().toString()))
                        .buildShortDynamicLink()
                        .addOnCompleteListener(this, task -> {
                            if (task.isSuccessful()) {
                                // Short link created
                                Uri shortLink = Objects.requireNonNull(task.getResult()).getShortLink();
                                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                                sharingIntent.setType("text/plain");
                                sharingIntent.putExtra(Intent.EXTRA_TEXT, shortLink.toString());
                                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Gamerji Contest");
                                startActivity(Intent.createChooser(sharingIntent, "Gamerji"));
                            } else {
                                Constants.SnakeMessageYellow(parent_lyt, "Something went wrong.Please try again later.");
                            }
                        });
                break;
            }

            case R.id.img_discord: {
                PackageManager pm = getPackageManager();
                if (FileUtils.isPackageInstalled("com.discord", pm)) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(discord_link));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setPackage("com.discord");
                    startActivity(intent);
                } else {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(discord_link));
                    startActivity(i);

                }

                break;
            }
            case R.id.img_youtube_sub: {
                PackageManager pm = getPackageManager();
                if (FileUtils.isPackageInstalled("com.google.android.youtube", pm)) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(youtubre_link));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setPackage("com.google.android.youtube");
                    startActivity(intent);
                } else {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(youtubre_link));
                    startActivity(i);
                }

                break;
            }

            case R.id.img_fb: {
                PackageManager pm = getPackageManager();
                if (FileUtils.isPackageInstalled("com.facebook.katana", pm)) {
                    startActivity(newFacebookIntent(pm, fb_link));
                } else {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(fb_link));
                    startActivity(i);
                }

                break;
            }

            case R.id.img_insta: {
                Uri uri = Uri.parse(insta_link);
                Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

                likeIng.setPackage("com.instagram.android");

                try {
                    startActivity(likeIng);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse(insta_link)));
                }
                break;
            }

            case R.id.img_telegram: {
                PackageManager pm = getPackageManager();
                startActivity(telegramIntent(pm));
                break;
            }

            case R.id.cv_tournament_time_next: {
                if (reports_id.isEmpty()) {
                    Constants.SnakeMessageYellow(parent_lyt, "Please select Issue");
                } else {
                    submitReportIssue();
                }

                break;
            }
            case R.id.iv_close_tournament_time: {
                submit_report_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
                bg.setVisibility(View.GONE);
                break;
            }

            case R.id.img_close_rating: {
                mBottomSheetRatings.setState(BottomSheetBehavior.STATE_COLLAPSED);
                bg.setVisibility(View.GONE);
                break;
            }

            case R.id.cv_ratings: {
                // ToDo: New > Bottom Sheet Code
                mBottomSheetRatings.setState(BottomSheetBehavior.STATE_EXPANDED);
                bg.setVisibility(View.VISIBLE);

                // ToDo: Old > Dialog Code
                /*final Dialog dialog = new Dialog(ActivityContestDetails.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
                dialog.setContentView(R.layout.ratings_layout);
                dialog.show();
                ImageView close = dialog.findViewById(R.id.img_close_rating);
                CardView cv_submit_rating = dialog.findViewById(R.id.cv_submit_rating);
                RatingBar rating = dialog.findViewById(R.id.rating);
                close.setOnClickListener(v1 -> dialog.cancel());
                cv_submit_rating.setOnClickListener(v12 ->
                {
                    submitRating(String.valueOf(rating.getRating()), dialog);
                });*/
                break;
            }

            case R.id.cv_submit_rating: {
                submitRating();
                break;
            }
        }
    }

    private void submitRating() {
        mRatings = String.valueOf(rating_bar.getRating());
        Common.insertLog("Ratings:::> " + mRatings);
        final ProgressDialog progressDialog = new ProgressDialog(ActivityContestDetails.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<GeneralResponseModel> generalResponseModelCall;

        generalResponseModelCall = apiInterface.submitRating(Constants.SUBMUT_RATING, Pref.getValue(ActivityContestDetails.this, Constants.UserID, "", Constants.FILENAME)
                , contest_id, mRatings);

        generalResponseModelCall.enqueue(new Callback<GeneralResponseModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<GeneralResponseModel> call, @NonNull Response<GeneralResponseModel> response) {
                progressDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                swipe_refresh_player.setRefreshing(false);
                GeneralResponseModel generalResponseModel = response.body();
                submit_report_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
                mBottomSheetRatings.setState(BottomSheetBehavior.STATE_COLLAPSED);
                assert generalResponseModel != null;
                Constants.SnakeMessageYellow(parent_lyt, generalResponseModel.message);
                getData();
            }

            @Override
            public void onFailure(@NonNull Call<GeneralResponseModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                swipe_refresh_player.setRefreshing(false);
                t.printStackTrace();
                feedback_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
                mBottomSheetRatings.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });
    }

    private void submitReportIssue() {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityContestDetails.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<GeneralResponseModel> generalResponseModelCall;

        generalResponseModelCall = apiInterface.submitReportIssue(Constants.SUBMUT_ISSUE_REPORT, Pref.getValue(ActivityContestDetails.this, Constants.UserID, "", Constants.FILENAME)
                , contest_id, reports_id);

        generalResponseModelCall.enqueue(new Callback<GeneralResponseModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<GeneralResponseModel> call, @NonNull Response<GeneralResponseModel> response) {
                progressDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                swipe_refresh_player.setRefreshing(false);
                GeneralResponseModel generalResponseModel = response.body();
                submit_report_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);

                assert generalResponseModel != null;
                Constants.SnakeMessageYellow(parent_lyt, generalResponseModel.message);
                getData();
            }

            @Override
            public void onFailure(@NonNull Call<GeneralResponseModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                swipe_refresh_player.setRefreshing(false);
                t.printStackTrace();
                feedback_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });
    }

    private Intent telegramIntent(PackageManager packageManager) {
        Intent intent;
        try {
            try {
                packageManager.getPackageInfo("org.telegram.messenger", 0);//Check for Telegram Messenger App
            } catch (Exception e) {
                packageManager.getPackageInfo("org.thunderdog.challegram", 0);//Check for Telegram X App
            }
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tg://resolve?domain=" + telegram_link));
        } catch (Exception e) { //App not found open in browser
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.telegram.me/" + telegram_link));
        }
        return intent;
    }

    public static Intent newFacebookIntent(PackageManager pm, String url) {
        Uri uri = Uri.parse(url);
        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo("com.facebook.katana", 0);
            if (applicationInfo.enabled) {
                uri = Uri.parse("fb://facewebmodal/f?href=" + url);
            }
        } catch (PackageManager.NameNotFoundException ignored) {
        }
        return new Intent(Intent.ACTION_VIEW, uri);
    }

    private void sendFeedbackToServer() {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityContestDetails.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<GeneralResponseModel> generalResponseModelCall;

        if (et_fb_other.getText().toString().isEmpty())
            generalResponseModelCall = apiInterface.addFeedback(Constants.ADD_FEEDBACK, Pref.getValue(ActivityContestDetails.this, Constants.UserID, "", Constants.FILENAME), RatingID,
                    feedback_reason_id.toString(), "");
        else
            generalResponseModelCall = apiInterface.addFeedback(Constants.ADD_FEEDBACK, Pref.getValue(ActivityContestDetails.this, Constants.UserID, "", Constants.FILENAME), RatingID,
                    feedback_reason_id.toString(), et_fb_other.getText().toString());

        generalResponseModelCall.enqueue(new Callback<GeneralResponseModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<GeneralResponseModel> call, @NonNull Response<GeneralResponseModel> response) {
                progressDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                swipe_refresh_player.setRefreshing(false);
                GeneralResponseModel generalResponseModel = response.body();
                feedback_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);

                assert generalResponseModel != null;
                if (generalResponseModel.status.equalsIgnoreCase("success"))
                    Constants.SnakeMessageYellow(parent_lyt, generalResponseModel.message);
                else
                    Constants.SnakeMessageYellow(parent_lyt, generalResponseModel.message);

            }

            @Override
            public void onFailure(@NonNull Call<GeneralResponseModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                swipe_refresh_player.setRefreshing(false);
                t.printStackTrace();
                feedback_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });
    }

    @Override
    public void onPickResult(PickResult pickResult) {
        if (pickResult.getError() == null) {
            try {
                sendSStoServer(pickResult);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Constants.SnakeMessageYellow(parent_lyt, pickResult.getError().getMessage());
        }

    }

    private void sendSStoServer(PickResult pickResult) throws IOException {

        final ProgressDialog progressDialog = new ProgressDialog(ActivityContestDetails.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        File file = new File(pickResult.getPath());
        try {
            OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
            pickResult.getBitmap().compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestParams requestParams = new RequestParams();
        try {
            requestParams.put("Action", Constants.UPLOAD_SS);
            requestParams.put("Val_Userid", Pref.getValue(ActivityContestDetails.this, Constants.UserID, "", Constants.FILENAME));
            requestParams.put("Val_Contestid", contest_id);
            //requestParams.put("Val_Contestuserid",Constants.getUserContestId(usersDataArrayList,ActivityContestDetails.this));
            requestParams.put("Val_Contestuserid", ContestUserID);
            requestParams.put("Val_CUscreenshotimage", file);
            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            asyncHttpClient.setTimeout(60 * 1000);
            asyncHttpClient.post(ActivityContestDetails.this, Constants.BASE_API + Constants.JOIN_CONTESTS, requestParams, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    progressDialog.dismiss();
                    if (response.optString("status").equalsIgnoreCase("success")) {
                        Constants.SnakeMessageYellow(parent_lyt, response.optString("message"));
//                        img_view_ss.setImageDrawable(getResources().getDrawable(R.drawable.ic_check));
//                        img_view_ss_user.setImageDrawable(getResources().getDrawable(R.drawable.ic_check));
                        screenShotUrl = response.optJSONObject("data").optString("ScreenshotURL");
                        Glide.with(ActivityContestDetails.this).load(response.optJSONObject("data").optString("ScreenshotURL")).into(uploaded_ss);

                        /*for (int i=0;i<usersDataArrayList.size();i++)
                        {
                            if (Pref.getValue(ActivityContestDetails.this,Constants.UserID,"", Constants.FILENAME).
                                    equalsIgnoreCase(usersDataArrayList.get(i).UserID))
                            {
                                Glide.with(ActivityContestDetails.this).load(response.optJSONObject("data").optString("ScreenshotURL")).into(uploaded_ss);
                                usersDataArrayList.get(i).ScreenshotFlag=true;
                                usersDataArrayList.get(i).ScreenshotURL=response.optJSONObject("data").optString("ScreenshotURL");
                            }
                        }
                        playerListAdapter.notifyDataSetChanged();*/
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Constants.SnakeMessageYellow(parent_lyt, "Something went wrong please try again");
                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {

                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    progressDialog.dismiss();
                    Constants.SnakeMessageYellow(parent_lyt, "Something went wrong please try again");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                    super.onFailure(statusCode, headers, responseString, throwable);
                    progressDialog.dismiss();
                    Constants.SnakeMessageYellow(parent_lyt, "Something went wrong please try again");
                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFeedbackClickListener(ContestDetailsModel.ContestsData.RatingsData ratingsData) {
        feedback_reason_id.clear();
        card_submit_number.setVisibility(View.VISIBLE);
        RatingID = ratingsData.RatingID;
        feedbackReasonAdapter = new FeedbackReasonAdapter(ActivityContestDetails.this, ratingsData.optionsDataArrayList, false, ActivityContestDetails.this);
        rv_reasons.setAdapter(feedbackReasonAdapter);
    }

    @Override
    public void onFeedbackReasonClick(String ROptionID, boolean checked) {
        if (feedback_reason_id.contains(ROptionID)) {
            if (checked)
                feedback_reason_id.add(ROptionID);
            else
                feedback_reason_id.remove(ROptionID);
        } else
            feedback_reason_id.add(ROptionID);
    }

    @Override
    public void onTournamentTimeSelectedListener(TournamentTimeSlotModel.Data.TimeSlots timeSlots) {
    }

    @Override
    public void onReportSelected(ReportsDataModel.ReportsData reportsData) {
        reports_id = reportsData.ReportID;
    }

    class MySocket {
        Socket mSocket;

        MySocket() {
            try {
                mSocket = IO.socket(Constants.SOCKET_API);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }

        void off() {
            mSocket.off(Constants.RECEIVECHAT, receivelivechat);
            mSocket.off(Constants.RECEIVEALLCHATMESSAGE, receiveallchat);
            mSocket.off(Socket.EVENT_DISCONNECT, mSetOnSocketDisconnectListener);
            mSocket.off(Socket.EVENT_CONNECT, mSetOnSocketConnecttListener);
        }

        public void on() {
            mSocket.on(Socket.EVENT_CONNECT, mSetOnSocketConnecttListener);
            mSocket.on(Constants.RECEIVEALLCHATMESSAGE, receiveallchat);
            mSocket.on(Constants.RECEIVECHAT, receivelivechat);
            mSocket.on(Socket.EVENT_DISCONNECT, mSetOnSocketDisconnectListener);
        }


        public void Disconnect() {
            mSocket.disconnect();
        }

        boolean connected() {
            return mSocket.connected();
        }


        void connect() {
            mSocket.connect();
        }

        void requestallChatMessage() {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("user_id", Pref.getValue(ActivityContestDetails.this, Constants.UserID, "", Constants.FILENAME));
                jsonObject.put("contest_id", contest_id);
                jsonObject.put("tournament_id", "0");
                mSocket.emit(Constants.REQUESTALLCHATMESSAGE, jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        void sendMessage(String message) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("user_id", Pref.getValue(ActivityContestDetails.this, Constants.UserID, "", Constants.FILENAME));
                jsonObject.put("contest_id", contest_id);
                jsonObject.put("first_name", Pref.getValue(ActivityContestDetails.this, Constants.TeamName, "", Constants.FILENAME));
                jsonObject.put("last_name", " ");
                jsonObject.put("chat_message", message);
                jsonObject.put("tournament_id", "0");
                jsonObject.put("level_icon", Pref.getValue(ActivityContestDetails.this, Constants.LevelIcon, "", Constants.FILENAME));
                ChatModel chatModel = new ChatModel();
                chatModel.setSender_id(Pref.getValue(ActivityContestDetails.this, Constants.UserID, "", Constants.FILENAME));
                chatModel.setContest_id(contest_id);
                chatModel.setChat_datetime(String.valueOf(System.currentTimeMillis() / 1000));
                chatModel.setChat_message(et_chat.getText().toString());
                chatModel.setSender_full_name(Pref.getValue(ActivityContestDetails.this, Constants.TeamName, "", Constants.FILENAME));

                chatModelArrayList.add(chatModel);
                mSocket.emit(Constants.SENDCHAT, jsonObject);
                et_chat.setText("");
                linearLayoutManager.smoothScrollToPosition(rv_chat, null, chatModelArrayList.size() - 1);
                chatAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        void joinChatRoom() {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("user_id", Pref.getValue(ActivityContestDetails.this, Constants.UserID, "", Constants.FILENAME));
                jsonObject.put("contest_id", contest_id);
                jsonObject.put("tournament_id", "0");
                mSocket.emit(Constants.JOINCHATROOM, jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public Emitter.Listener mSetOnSocketDisconnectListener = args -> runOnUiThread(() ->
    {
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
    });

    public Emitter.Listener mSetOnSocketConnecttListener = args -> runOnUiThread(() ->
    {
        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
    });


    public Emitter.Listener receivelivechat = args -> runOnUiThread(() ->
    {
        try {
            if (!args[0].toString().isEmpty()) {
                JSONObject jsonObject = new JSONObject(args[0].toString());

                ChatModel chatModel = new ChatModel();
                chatModel.setId("");
                chatModel.setSender_id(jsonObject.optString("user_id"));
                chatModel.setChat_message(jsonObject.optString("chat_message"));
                chatModel.setSender_full_name(jsonObject.optString("first_name") + " " + jsonObject.optString("last_name"));
                chatModel.setContest_id(jsonObject.optString("contest_id"));
                chatModel.setChat_datetime(jsonObject.optString("chat_datetime"));
                chatModel.setLevel_icon(jsonObject.optString("level_icon"));
                chatModelArrayList.add(chatModel);
                chatAdapter.notifyDataSetChanged();
                linearLayoutManager.smoothScrollToPosition(rv_chat, null, chatModelArrayList.size() - 1);
                //rv_chat.scrollToPosition(chatModelArrayList.size()-1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    });

    @Override
    protected void onDestroy() {
        mSocket.off();
        super.onDestroy();
    }

    public Emitter.Listener receiveallchat = args -> runOnUiThread(() ->
    {
        try {
            if (!args[0].toString().isEmpty()) {
                JSONArray jsonArray = new JSONArray(args[0].toString());
                for (int i = 0; i < jsonArray.length(); i++) {
                    ChatModel chatModel = new ChatModel();
                    chatModel.setChat_datetime(jsonArray.optJSONObject(i).optString("chat_datetime"));
                    chatModel.setChat_message(jsonArray.optJSONObject(i).optString("chat_message"));
                    chatModel.setSender_full_name(jsonArray.optJSONObject(i).optString("sender_full_name"));
                    chatModel.setContest_id(jsonArray.optJSONObject(i).optString("contest_id"));
                    chatModel.setSender_id(jsonArray.optJSONObject(i).optString("sender_id"));
                    chatModel.setId(jsonArray.optJSONObject(i).optString("id"));
                    chatModel.setLevel_icon(jsonArray.optJSONObject(i).optString("level_icon"));
                    chatModelArrayList.add(chatModel);
                }
                chatAdapter.notifyDataSetChanged();
                rv_chat.scrollToPosition(chatModelArrayList.size() - 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    });

}
