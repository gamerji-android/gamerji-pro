/*
package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSize;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.gun0912.tedpermission.PermissionListener;
import com.onesignal.OneSignal;
import com.tbuonomo.viewpagerdotsindicator.WormDotsIndicator;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import com.esports.gamerjipro.Adapter.GamesListAdapter;
import com.esports.gamerjipro.Adapter.ImageSliderAdapter;
import com.esports.gamerjipro.Model.AdsDataModel;
import com.esports.gamerjipro.Model.ContactsModel;
import com.esports.gamerjipro.Model.DashboardModel;
import com.esports.gamerjipro.Model.GeneralResponseModel;
import com.esports.gamerjipro.Model.SignInModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.AnalyticsSocket;
import com.esports.gamerjipro.Utils.Common;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.Pref;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.esports.gamerjipro.Utils.Constants.AD_PLAY;
import static com.esports.gamerjipro.Utils.Constants.APK;
import static com.esports.gamerjipro.Utils.Constants.Apptype;

public class ActivityMain  extends AppCompatActivity implements View.OnClickListener
{

    GamesListAdapter gamesListAdapter ;
    private  int currentPage = 0;
    private  int NUM_PAGES = 0;
    ViewPager slider_pager;
    RecyclerView rv_games;
    LinearLayout lyt_contest,lyt_home,lyt_wallet,lyt_more;
    ImageView img_contest,img_profile;
    APIInterface apiInterface;
    ImageView img_notification;
    RelativeLayout parent_lyt;
    Timer swipeTimer;
    String mLeagueInviteLink;
    AnalyticsSocket analyticsSocket = new AnalyticsSocket();
    SwipeRefreshLayout swipe_refresh;
    boolean userProfile=false;
    LinearLayout adContainer;
    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        InputMethodManager im = (InputMethodManager) this.getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        im.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        setContentView(R.layout.activity_main);
        swipe_refresh=findViewById(R.id.swipe_refresh);
        slider_pager = findViewById(R.id.slider_pager);
        rv_games = findViewById(R.id.rv_games);
        lyt_contest = findViewById(R.id.lyt_contest);
        lyt_home = findViewById(R.id.lyt_home);
        parent_lyt = findViewById(R.id.parent_lyt);
        lyt_wallet = findViewById(R.id.lyt_wallet);
        img_contest=findViewById(R.id.img_contest);
        img_notification=findViewById(R.id.img_notification);

        img_notification.setOnClickListener(v ->
        {
            Intent i = new Intent(ActivityMain.this,NotificationActivity.class);
            startActivity(i);
        });
        img_profile=findViewById(R.id.img_profile);
        lyt_more=findViewById(R.id.lyt_more);
        swipeTimer = new Timer();
        apiInterface = APIClient.getClient().create(APIInterface.class);
        rv_games.setLayoutManager(new GridLayoutManager(this,3));
        rv_games.setItemAnimator(new DefaultItemAnimator());

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task ->
                {
                    if (!task.isSuccessful())
                    {
                        Common.insertLog("getInstanceId failed" +task.getException());
                        return;
                    }

                    // Get new Instance ID token
                    if (!Pref.getValue(ActivityMain.this, Constants.UserID,"",Constants.FILENAME).isEmpty())
                    {
                        sendTokenServer(Objects.requireNonNull(task.getResult()).getToken());
                    }
                    // Log and toast/
                });

        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

        rv_games.setNestedScrollingEnabled(false);

        lyt_contest.setOnClickListener(this);
        img_profile.setOnClickListener(this);
        lyt_wallet.setOnClickListener(this);
        lyt_more.setOnClickListener(this);

        getDashboard();

        if (!userProfile)
        {
            getUserProfile();
        }


if (Pref.getValue(ActivityMain.this,Constants.isIsFirsttimeApp,true,Constants.FILENAME))
        {
            TedPermission.with(this)
                    .setPermissionListener(permissionlistener)
                    .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                    .setPermissions(Manifest.permission.READ_CONTACTS)
                    .check();
        }

        swipe_refresh.setOnRefreshListener(this::getDashboard);


    }

    @SuppressLint("StaticFieldLeak")
    public  class FetchContacts extends AsyncTask<Void, Void, ArrayList<ContactsModel>>
    {
        @SuppressLint("StaticFieldLeak")
        private Context activity;

        FetchContacts(Context context)
        {
            activity = context;
        }
        @Override
        protected ArrayList<ContactsModel> doInBackground(Void... params)
        {
            ArrayList<ContactsModel> contactsArrayList = new ArrayList<>();
            ContentResolver cr = getContentResolver();
            Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                    null, null, null, null);

            if ((cur != null ? cur.getCount() : 0) > 0)
            {

                while (cur != null && cur.moveToNext())
                {

                    String id = cur.getString(
                            cur.getColumnIndex(ContactsContract.Contacts._ID));
                    String name = cur.getString(cur.getColumnIndex(
                            ContactsContract.Contacts.DISPLAY_NAME));

                    if (cur.getInt(cur.getColumnIndex(
                            ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0)
                    {
                        ContactsModel contactsModel = new ContactsModel();
                        Cursor pCur = cr.query(
                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                new String[]{id}, null);

                        Cursor cur1 = cr.query(
                                ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                                new String[]{id}, null);

                        while (pCur.moveToNext())
                        {
                            String phoneNo = pCur.getString(pCur.getColumnIndex(
                                    ContactsContract.CommonDataKinds.Phone.NUMBER));
                            contactsModel.setName(name);
                            contactsModel.setPhone(phoneNo);
                        }

                        while (cur1.moveToNext())
                        {
                            //to get the contact names
                            String email = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                            contactsModel.setEmail(email);
                        }
                        contactsArrayList.add(contactsModel);
                        pCur.close();
                    }
                }
            }
            if(cur!=null){
                cur.close();
            }
            return contactsArrayList;
        }

        @Override
        protected void onPostExecute(ArrayList<ContactsModel> list)
        {
            super.onPostExecute(list);
            ArrayList<ContactsModel> contactsModels = new ArrayList<>();

            for (int i=0;i<list.size();i++)
            {
                ContactsModel contactsModel  = new ContactsModel();

                contactsModel.setName(list.get(i).getName());
                contactsModel.setPhone(list.get(i).getPhone());

                if (list.get(i).getEmail()!=null)
                contactsModel.setEmail(list.get(i).getEmail());
                else
                contactsModel.setEmail("");
                contactsModels.add(contactsModel);
            }

            JsonArray jsonElements = (JsonArray) new Gson().toJsonTree(contactsModels);
            sendContactToserver(jsonElements);

        }

        private void sendContactToserver(JsonArray jsonElements)
        {
            Call<GeneralResponseModel> generalResponseModelCall;

            generalResponseModelCall = apiInterface.sendContacts(Constants.SAVE_CONTACT,Pref.getValue(ActivityMain.this,Constants.UserID,"",Constants.FILENAME),jsonElements.toString());

            generalResponseModelCall.enqueue(new Callback<GeneralResponseModel>()
            {
                @Override
                public void onResponse(@NonNull Call<GeneralResponseModel> call, @NonNull Response<GeneralResponseModel> response)
                {
                    GeneralResponseModel generalResponseModel = response.body();
                    assert generalResponseModel != null;
                    if (generalResponseModel.status.equalsIgnoreCase("success"))
                    {
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GeneralResponseModel> call, @NonNull Throwable t)
                {
                    t.printStackTrace();
                }
            });
        }
    }

    PermissionListener permissionlistener = new PermissionListener()
    {
        @Override
        public void onPermissionGranted()
        {
            Pref.setValue(ActivityMain.this,Constants.isIsFirsttimeApp,true,Constants.FILENAME);
            new FetchContacts(ActivityMain.this).execute();
            //startService(new Intent(ActivityMain.this, ContactsService.class));
        }

        @Override
        public void onPermissionDenied(List<String> deniedPermissions)
        {
            Pref.setValue(ActivityMain.this,Constants.isIsFirsttimeApp,false,Constants.FILENAME);
        }
    };

    private void getUserProfile()
    {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<SignInModel> signInModelCall;

            signInModelCall = apiInterface.getProfile(Constants.GET_PROFILE,Pref.getValue(ActivityMain.this,Constants.UserID,"",Constants.FILENAME));

        signInModelCall.enqueue(new Callback<SignInModel>()
        {
            @Override
            public void onResponse(@NonNull Call<SignInModel> call, @NonNull Response<SignInModel> response)
            {
                progressDialog.dismiss();
                SignInModel signInModel = response.body();
                assert signInModel != null;

                if (signInModel.status.equalsIgnoreCase("success"))
                {
                        userProfile=true;
                        Pref.setValue(ActivityMain.this, Constants.UserID, signInModel.userDataClass.UserID, Constants.FILENAME);
                        Pref.setValue(ActivityMain.this, Constants.FullName, signInModel.userDataClass.FullName, Constants.FILENAME);
                        Pref.setValue(ActivityMain.this, Constants.lastname, signInModel.userDataClass.LastName, Constants.FILENAME);
                        Pref.setValue(ActivityMain.this, Constants.BirthDate, signInModel.userDataClass.BirthDate, Constants.FILENAME);
                        Pref.setValue(ActivityMain.this, Constants.firstname, signInModel.userDataClass.FirstName, Constants.FILENAME);
                        Pref.setValue(ActivityMain.this, Constants.TeamName, signInModel.userDataClass.TeamName, Constants.FILENAME);
                        Pref.setValue(ActivityMain.this, Constants.CountryCode, signInModel.userDataClass.CountryCode, Constants.FILENAME);
                        Pref.setValue(ActivityMain.this, Constants.MobileNumber, signInModel.userDataClass.MobileNumber, Constants.FILENAME);
                        Pref.setValue(ActivityMain.this, Constants.EmailAddress,signInModel.userDataClass.EmailAddress, Constants.FILENAME);
                        Pref.setValue(ActivityMain.this, Constants.LevelIcon,signInModel.userDataClass.LevelIcon, Constants.FILENAME);
                    if(!signInModel.userDataClass.BirthDate.isEmpty())
                    {
                        @SuppressLint("SimpleDateFormat") DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                        @SuppressLint("SimpleDateFormat") DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
                        String inputDateStr=signInModel.userDataClass.BirthDate;
                        Date date = null;
                        try
                        {
                            date = inputFormat.parse(inputDateStr);
                        }
                        catch (ParseException e)
                        {
                            e.printStackTrace();
                        }

                        String outputDateStr = outputFormat.format(date);
                        Pref.setValue(ActivityMain.this, Constants.BirthDate,outputDateStr, Constants.FILENAME);
                    }
                        Pref.setValue(ActivityMain.this, Constants.State,signInModel.userDataClass.State, Constants.FILENAME);
                        Pref.setValue(ActivityMain.this, Constants.ProfileImage,signInModel.userDataClass.ProfileImage, Constants.FILENAME);
                        Constants.setUserGameData(signInModel.userDataClass.GamesData.gamesDataArrayList,ActivityMain.this);
                        Pref.setValue(ActivityMain.this, Constants.IS_LOGIN,true, Constants.FILENAME);
                        Pref.setValue(ActivityMain.this, Constants.SOCIAL_LOGIN, true, Constants.FILENAME);
                 }
                else
                {
                    Intent i = new Intent(ActivityMain.this,Activity_Login.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    Pref.ClearAllPref(ActivityMain.this, Constants.FILENAME);
                    Pref.setValue(ActivityMain.this,Constants.IS_LOGIN,false,Constants.FILENAME);
                    startActivity(i);
                    finish();
                }
             }

            @Override
            public void onFailure(@NonNull Call<SignInModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
                userProfile=false;
            }
        });
    }

    private void sendTokenServer(String token)
    {

        Call<GeneralResponseModel> loginModelCall = apiInterface.sendToken(Constants.REGISTER_DEVICE,Pref.getValue(ActivityMain.this,Constants.UserID,"", Constants.FILENAME),"Android",token);

        loginModelCall.enqueue(new Callback<GeneralResponseModel>()
        {
            @Override
            public void onResponse(@NonNull Call<GeneralResponseModel> call, @NonNull Response<GeneralResponseModel> response)
            {
                GeneralResponseModel generalResponseModel = response.body();
                assert generalResponseModel != null;
            }

            @Override
            public void onFailure(Call<GeneralResponseModel> call, Throwable t)
            {
                t.printStackTrace();
            }
        });
    }

    private void getDashboard()
    {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityMain.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        PackageInfo info = null;
        String version = "";
        try
        {
            info = ActivityMain.this.getPackageManager().getPackageInfo(ActivityMain.this.getPackageName(), PackageManager.GET_ACTIVITIES);
            version = info.versionName;
        }
        catch (PackageManager.NameNotFoundException e)
        {
            e.printStackTrace();
        }



        Call<DashboardModel>  dashboardModelCall =
                apiInterface.getDashboard("GetData",Pref.getValue(ActivityMain.this,Constants.UserID,"", Constants.FILENAME),
                        Apptype,version);

        dashboardModelCall.enqueue(new Callback<DashboardModel>()
        {
            @Override
            public void onResponse(@NonNull Call<DashboardModel> call, @NonNull Response<DashboardModel> response)
            {
                progressDialog.dismiss();

                if(swipe_refresh.isRefreshing())
                    swipe_refresh.setRefreshing(false);

                DashboardModel dashboardModel = response.body();
                assert dashboardModel != null;
                if (dashboardModel.status.equalsIgnoreCase("success"))
                {
                    slider_pager.setAdapter(new ImageSliderAdapter(ActivityMain.this,dashboardModel.DataClass.BannersData.bdataArrayList));
                    slider_pager.setPadding(50, 0, 50, 0);
                    slider_pager.setClipToPadding(false);
                    slider_pager.setPageMargin(25);

                    final WormDotsIndicator indicator = findViewById(R.id.image_slider_indicator);
                    indicator.setViewPager(slider_pager);

                    NUM_PAGES =dashboardModel.DataClass.BannersData.bdataArrayList.size();
                    final Handler handler = new Handler();
                    final Runnable Update = () ->
                    {
                        if (currentPage == NUM_PAGES)
                        {
                            currentPage = 0;
                             //swipeTimer.cancel();
                        }
                        slider_pager.setCurrentItem(currentPage++, true);
                    };

                    swipeTimer.schedule(new TimerTask()
                    {
                        @Override
                        public void run()
                        {
                            handler.post(Update);
                        }
                    }, 0,4000);

                    if (dashboardModel.DataClass.IsDailyLogin)
                    {
                        final Dialog dialog = new Dialog(ActivityMain.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
                        dialog.setContentView(R.layout.daily_points_dialog);
                        dialog.show();
                        TextView points= dialog.findViewById(R.id.txt_points);
                        points.setText(dashboardModel.DataClass.IsDailyLoginPoint);
                        ImageView close=dialog.findViewById(R.id.img_close);
                        close.setOnClickListener(v -> dialog.cancel());
                    }

                    Constants.gdataArrayList=dashboardModel.DataClass.GamesData.gdataArrayList;
                    gamesListAdapter= new GamesListAdapter(ActivityMain.this,dashboardModel.DataClass.GamesData.gdataArrayList);
                    rv_games.setAdapter(gamesListAdapter);


                    if(dashboardModel.DataClass.IsFirstTime)
                    {
                        if(!dashboardModel.DataClass.IntroVideoLink.isEmpty())
                        showWalkthroughVideo(dashboardModel.DataClass.IntroVideoTitle,dashboardModel.DataClass.IntroVideoLink,dashboardModel.DataClass.adsDataModel,dashboardModel.DataClass.AdsCount);
                    }

                    if(!dashboardModel.DataClass.IsFirstTime && dashboardModel.DataClass.AdsCount>0 && !dashboardModel.DataClass.adsDataModel.AdVideo.isEmpty())
                    {
                        showVideoAds(dashboardModel.DataClass.adsDataModel);
                    }
                }
                else
                {
                    Constants.SnakeMessageYellow(parent_lyt,dashboardModel.message);
                    Intent i = new Intent(ActivityMain.this,Activity_Login.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    Pref.ClearAllPref(ActivityMain.this, Constants.FILENAME);
                    Pref.setValue(ActivityMain.this,Constants.IS_LOGIN,false,Constants.FILENAME);
                    startActivity(i);
                    finish();
                }

            }

            @Override
            public void onFailure(@NonNull Call<DashboardModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }

        });

    }

    int play_time=0;
    @SuppressLint("SetJavaScriptEnabled")
    private void showVideoAds(AdsDataModel adsDataModel)
    {
        final BottomSheetDialog dialog = new BottomSheetDialog(ActivityMain.this);
        dialog.setContentView(R.layout.walkthrough_bottomsheet);
        dialog.setCanceledOnTouchOutside(false);

        @SuppressLint("CutPasteId")
        ImageView btnClose = dialog.findViewById(R.id.img_close);
        TextView txt_intro_title = dialog.findViewById(R.id.txt_intro_title);
        Objects.requireNonNull(txt_intro_title).setText(adsDataModel.AdName);
       ProgressBar progressBar =  dialog.findViewById(R.id.progrss);
        WebView webView =dialog.findViewById(R.id.wv_walkthrough);
        Objects.requireNonNull(webView).setVisibility(View.GONE);

        RelativeLayout lyt_video_ads= dialog.findViewById(R.id.lyt_video_ads);
        Objects.requireNonNull(lyt_video_ads).setVisibility(View.VISIBLE);

        VideoView videoView = dialog.findViewById(R.id.vv);
        MediaController mediacontroller;
        mediacontroller = new MediaController(ActivityMain.this);
        mediacontroller.setAnchorView(videoView);

        assert videoView != null;
        videoView.setVideoPath(adsDataModel.AdVideo);

        videoView.setOnCompletionListener(mp ->
        {
            if(adsDataModel.VideoAutoplay)
            {
                play_time=videoView.getDuration()/1000;
                videoView.stopPlayback();
                analyticsSocket.sendAnalytics(adsDataModel.AdType,adsDataModel.AdID,adsDataModel.AdVideoName,AD_PLAY,Pref.getValue(ActivityMain.this,Constants.UserID,"", Constants.FILENAME),
                        play_time);
                dialog.dismiss();
            }
        });

        // Close the progress bar and play the video
        videoView.setOnPreparedListener(mp ->
        {
            assert progressBar != null;
            progressBar.setVisibility(View.GONE);
            videoView.start();
        });

        assert btnClose != null;
        btnClose.setOnClickListener(v ->
        {
            play_time=videoView.getCurrentPosition()/1000;
            dialog.dismiss();
            videoView.stopPlayback();
            analyticsSocket.sendAnalytics(adsDataModel.AdType,adsDataModel.AdID,adsDataModel.AdVideoName,AD_PLAY,Pref.getValue(ActivityMain.this,Constants.UserID,"", Constants.FILENAME),
                    play_time);
        });
        dialog.show();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void showWalkthroughVideo(String introVideoTitle, String introVideoLink, AdsDataModel adsDataModel, int adsCount)
    {
        final BottomSheetDialog dialog = new BottomSheetDialog(ActivityMain.this);
        dialog.setContentView(R.layout.walkthrough_bottomsheet);
        dialog.setCanceledOnTouchOutside(false);

        @SuppressLint("CutPasteId")
        ImageView btnClose = dialog.findViewById(R.id.img_close);
        TextView txt_intro_title = dialog.findViewById(R.id.txt_intro_title);
        Objects.requireNonNull(txt_intro_title).setText(introVideoTitle);

        WebView webView =dialog.findViewById(R.id.wv_walkthrough);
        assert webView != null;
        webView.setWebChromeClient(new WebChromeClient());
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setDefaultFontSize(18);
        webView.loadUrl(introVideoLink);


        assert btnClose != null;
        btnClose.setOnClickListener(v ->
        {
            dialog.dismiss();
            if (adsCount>0)
            {
                if(!adsDataModel.AdVideo.isEmpty())
                showVideoAds(adsDataModel);
            }
        });
        dialog.show();
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.lyt_contest:
            {
                Intent i= new Intent(this,ActivityMyContestNew.class);
                startActivity(i);
                finish();
                break;
            }

            case R.id.img_profile:
            {
                Intent i= new Intent(this,ActivityProfile.class);
                startActivity(i);
                finish();
                break;
            }

            case R.id.lyt_wallet:
            {
                Intent i= new Intent(this, ActivityWallet.class);
                startActivity(i);
                finish();
                break;
            }
            case R.id.lyt_more:
            {
                Intent i= new Intent(this, ActivityMore.class);
                startActivity(i);
                finish();
                break;
            }

        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }
}
*/
