package com.esports.gamerjipro.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Adapter.BankListAdapter;
import com.esports.gamerjipro.Interface.OnBankClikcListener;
import com.esports.gamerjipro.Model.BankDetail;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.cashFree.ActivityPayment;

import java.util.ArrayList;

public class ActivityNetbanking extends AppCompatActivity implements OnBankClikcListener
{
    ImageView img_back;
    RecyclerView cv_bank_list;
    BankListAdapter bankListAdapter ;
    TextView tv_wallet_balance;
    ArrayList<BankDetail> bankDetailArrayList  = new ArrayList<>();
    String amount;
    RelativeLayout parent_layout;
    String game_id,game_type_id,game_name;
    boolean is_tournament=false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        if (getIntent().getExtras()!=null);
        {
            game_id = getIntent().getStringExtra("game_id");
            game_type_id = getIntent().getStringExtra("game_type_id");
            game_name = getIntent().getStringExtra("game_name");
            is_tournament=getIntent().getBooleanExtra("is_tournament",false);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_net_banking);
        img_back=findViewById(R.id.img_back);
        cv_bank_list=findViewById(R.id.cv_bank_list);
        tv_wallet_balance=findViewById(R.id.tv_wallet_balance);
        tv_wallet_balance.setText(getIntent().getStringExtra("current_amount"));
        amount=getIntent().getStringExtra("amount");
        parent_layout=findViewById(R.id.parent_layout);
        bankListAdapter= new BankListAdapter(this,bankDetailArrayList, this);
        cv_bank_list.setAdapter(bankListAdapter);
        img_back.setOnClickListener(v -> finish());
        setAllBanks();
    }


    private void setAllBanks()
    {
        bankDetailArrayList.add(new BankDetail("3001", "Allahabad Bank"));
        bankDetailArrayList.add(new BankDetail("3002", "Andhra Bank"));
        bankDetailArrayList.add(new BankDetail("3003", "Axis Bank"));
        bankDetailArrayList.add(new BankDetail("3060", "Bank of Baroda - Corporate"));
        bankDetailArrayList.add(new BankDetail("3005", "Bank of Baroda - Retail"));
        bankDetailArrayList.add(new BankDetail("0000", "Header"));
        bankDetailArrayList.add(new BankDetail("3006", "Bank of India"));
        bankDetailArrayList.add(new BankDetail("3007", "Bank of Maharashtra"));
        bankDetailArrayList.add(new BankDetail("3009", "Canara Bank"));
        bankDetailArrayList.add(new BankDetail("3010", "Catholic Syrian Bank"));
        bankDetailArrayList.add(new BankDetail("3011", "Central Bank of India"));
        bankDetailArrayList.add(new BankDetail("3012", "City Union Bank"));
        bankDetailArrayList.add(new BankDetail("3013", "Corporation Bank"));
        bankDetailArrayList.add(new BankDetail("3017", "DBS Bank Ltd"));
        bankDetailArrayList.add(new BankDetail("3062", "DCB Bank - Corporate"));
        bankDetailArrayList.add(new BankDetail("3018", "DCB Bank - Personal"));
        bankDetailArrayList.add(new BankDetail("3016", "Deutsche Bank"));
        bankDetailArrayList.add(new BankDetail("3019", "Dhanlakshmi Bank"));
        bankDetailArrayList.add(new BankDetail("3020", "Federal Bank"));
        bankDetailArrayList.add(new BankDetail("3021", "HDFC Bank"));
        bankDetailArrayList.add(new BankDetail("3022", "ICICI Bank"));
        bankDetailArrayList.add(new BankDetail("3023", "IDBI Bank"));
        bankDetailArrayList.add(new BankDetail("3026", "Indian Bank"));
        bankDetailArrayList.add(new BankDetail("3027", "Indian Overseas Bank"));
        bankDetailArrayList.add(new BankDetail("3028", "IndusInd Bank"));
        bankDetailArrayList.add(new BankDetail("3029", "Jammu and Kashmir Bank"));
        bankDetailArrayList.add(new BankDetail("3030", "Karnataka Bank Ltd"));
        bankDetailArrayList.add(new BankDetail("3031", "Karur Vysya Bank"));
        bankDetailArrayList.add(new BankDetail("3032", "Kotak Mahindra Bank"));
        bankDetailArrayList.add(new BankDetail("3033", "Laxmi Vilas Bank"));
        bankDetailArrayList.add(new BankDetail("3035", "Oriental Bank of Commerce"));
        bankDetailArrayList.add(new BankDetail("3037", "Punjab & Sind Bank"));
        bankDetailArrayList.add(new BankDetail("3065", "Punjab National Bank - Corporate"));
        bankDetailArrayList.add(new BankDetail("3038", "Punjab National Bank - Retail"));
        bankDetailArrayList.add(new BankDetail("3040", "Saraswat Bank"));
        bankDetailArrayList.add(new BankDetail("3042", "South Indian Bank"));
        bankDetailArrayList.add(new BankDetail("3043", "Standard Chartered Bank"));
        bankDetailArrayList.add(new BankDetail("3044", "State Bank Of India"));
        bankDetailArrayList.add(new BankDetail("3052", "Tamilnad Mercantile Bank Ltd"));
        bankDetailArrayList.add(new BankDetail("3054", "UCO Bank"));
        bankDetailArrayList.add(new BankDetail("3055", "Union Bank of India"));
        bankDetailArrayList.add(new BankDetail("3056", "United Bank of India"));
        bankDetailArrayList.add(new BankDetail("3057", "Vijaya Bank"));
        bankDetailArrayList.add(new BankDetail("3058", "Yes Bank Ltd"));
        bankDetailArrayList.add(new BankDetail("3333", "TEST Bank"));
        bankListAdapter.notifyDataSetChanged();
    }

    @Override
    public void OnBankItemClickListener(BankDetail bankDetail)
    {
        Intent i = new Intent(ActivityNetbanking.this, ActivityPayment.class);
        i.putExtra("type", Constants.CASHFREE_NB);
        i.putExtra("bank_id",bankDetail.id);
        i.putExtra("amount",amount);
        i.putExtra("game_id",game_id);
        i.putExtra("game_type_id",game_type_id);
        i.putExtra("game_name",game_name);
        i.putExtra("is_tournament",is_tournament);
        startActivityForResult(i, Constants.PAYMENT_RESULT);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode== Constants.PAYMENT_RESULT)
        {
            assert data != null;
            if (data.getBooleanExtra(Constants.PAYMENT_STATUS_SUCCESS,true))
            {
                Intent i = new Intent(ActivityNetbanking.this,ActivityWallet.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
            else
            {
                Constants.SnakeMessageYellow(parent_layout,"Payment failed .Please try again.");
            }

        }
    }
}
