package com.esports.gamerjipro.activity;

import android.os.Bundle;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Adapter.LeaderboardAdapter;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;

public class ActivityLeadership extends AppCompatActivity
{

    RecyclerView rv_leader ;
    APIInterface apiInterface;
    LeaderboardAdapter leaderboardAdapter ;
    ImageView img_back;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leadership_board);
        rv_leader=findViewById(R.id.rv_leader);
        img_back=findViewById(R.id.img_back);
        img_back.setOnClickListener(v -> finish());
        rv_leader.setLayoutManager(new LinearLayoutManager(this));
        rv_leader.setItemAnimator(new DefaultItemAnimator());
        apiInterface = APIClient.getClient().create(APIInterface.class);
       // getLeaderShip();
    }

    /*private void getLeaderShip()
    {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityLeadership.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<LeaderboardModel> leaderboardModelCall = apiInterface.getLeaderBoard(Constants.GETDATA, Pref.getValue(ActivityLeadership.this, Constants.UserID, "", Constants.FILENAME));

        leaderboardModelCall.enqueue(new Callback<LeaderboardModel>()
        {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<LeaderboardModel> call, @NonNull Response<LeaderboardModel> response)
            {
                progressDialog.dismiss();
                LeaderboardModel leaderboardModel = response.body();
                assert leaderboardModel != null;
                if (leaderboardModel.status.equalsIgnoreCase("success"))
                {
                    leaderboardAdapter = new LeaderboardAdapter(ActivityLeadership.this,leaderboardModel.DataClass.userLevelsData);
                    rv_leader.setAdapter(leaderboardAdapter);
                }
                else
                {

                }
            }
            @Override
            public void onFailure(@NonNull Call<LeaderboardModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }*/
}
