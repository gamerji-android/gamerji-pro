package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.esports.gamerjipro.Model.AccountDetailsModel;
import com.esports.gamerjipro.Model.StatesListModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.CustomWheelView;
import com.esports.gamerjipro.Utils.FileUtils;
import com.esports.gamerjipro.Utils.Pref;
import com.esports.gamerjipro.Utils.datepicker.DatePicker;
import com.esports.gamerjipro.Utils.datepicker.DatePickerDialog;
import com.esports.gamerjipro.Utils.datepicker.SpinnerDatePickerDialogBuilder;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;

import cz.msebera.android.httpclient.Header;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ActivityUploadPan extends AppCompatActivity implements View.OnClickListener, IPickResult, DatePickerDialog.OnDateSetListener {

    CardView card_pan_upload,card_submit_pan_for_verification,card_pan_verified;
    EditText et_card_name,et_pan_card_num;
    TextView txt_date_birth,img_cancel;
    TextView sp_states;
    APIInterface apiInterface;
    ImageView img_back,img_upload_hint,img_close;
    ArrayList<String> stateslist= new ArrayList<>();
    ArrayList<String> statesId= new ArrayList<>();
    PickResult pickResult ;
    int mYear, mMonth, mDay;
    RelativeLayout root_verify_pan;
    TextView txt_mobile;
    TextView txt_pan_verified,txt_pan_number;
    LinearLayout ln_upload_pan_view;
    BottomSheetBehavior sheetBehavior;
    RelativeLayout layoutBottomSheet;
    View bg;
    TextView txt_title_bs,txt_bottom_msg;
    TextView txt_submit_pan_info;
    AccountDetailsModel.Data.PanData panData;
    RelativeLayout lyt_rejected;
    TextView txt_reject_title,txt_reject_reason;
    CardView cv_pan_name,cv_pan_number;
    String dob;
    TextView img_news;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_pan);
        img_news=findViewById(R.id.img_news);
        img_news.setOnClickListener(v -> FileUtils.OpenNewsWebview(this,"https://www.gamerji.com"));
        card_pan_upload=findViewById(R.id.card_pan_upload);
        txt_submit_pan_info=findViewById(R.id.txt_submit_pan_info);
        card_pan_verified=findViewById(R.id.card_pan_verified);
        panData= (AccountDetailsModel.Data.PanData) getIntent().getSerializableExtra("panData");
        txt_pan_number=findViewById(R.id.txt_pan_number);
        ln_upload_pan_view=findViewById(R.id.ln_upload_pan_view);
        card_submit_pan_for_verification=findViewById(R.id.card_submit_pan_for_verification);
        et_card_name=findViewById(R.id.et_card_name);
        img_close=findViewById(R.id.img_close);
        et_pan_card_num=findViewById(R.id.et_pan_card_num);
        img_back=findViewById(R.id.img_back);
        txt_date_birth=findViewById(R.id.txt_date_birth);
        sp_states=findViewById(R.id.sp_states);
        txt_pan_verified=findViewById(R.id.txt_pan_verified);
        root_verify_pan=findViewById(R.id.root_verify_pan_card);
        img_cancel=findViewById(R.id.img_cancel);
        img_upload_hint=findViewById(R.id.img_upload_hint);
        txt_mobile=findViewById(R.id.txt_mobile);
        txt_title_bs=findViewById(R.id.txt_title_bs);
        txt_bottom_msg=findViewById(R.id.txt_bottom_msg);
        lyt_rejected=findViewById(R.id.lyt_rejected);
        txt_reject_title=findViewById(R.id.txt_reject_title);
        txt_reject_reason=findViewById(R.id.txt_reject_reason);
        cv_pan_number=findViewById(R.id.cv_pan_number);
        cv_pan_name=findViewById(R.id.cv_pan_name);

        bg = findViewById(R.id.bg);

        layoutBottomSheet=findViewById(R.id.bottom_how_to);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        if (getIntent().getStringExtra("pan_verified").equalsIgnoreCase("2"))
        {
            txt_pan_number.setText("PAN : "+panData.PanNumber);
            txt_pan_verified.setVisibility(View.VISIBLE);
            card_pan_upload.setVisibility(View.GONE);
            ln_upload_pan_view.setVisibility(View.GONE);
        }
        else if (getIntent().getStringExtra("pan_verified").equalsIgnoreCase("3"))
        {
            txt_pan_number.setText("PAN : "+panData.PanNumber);
            txt_pan_verified.setVisibility(View.VISIBLE);
            txt_pan_verified.setText("PAN VERIFICATION IS PENDING,your PAN verification takes 3-4 days");
            card_pan_upload.setVisibility(View.GONE);
            ln_upload_pan_view.setVisibility(View.GONE);
        }
        else if (getIntent().getStringExtra("pan_verified").equalsIgnoreCase("4"))
        {
            lyt_rejected.setVisibility(View.VISIBLE);
            txt_reject_reason.setVisibility(View.VISIBLE);
            txt_reject_reason.setText(panData.PanRejectedReason);
            card_pan_verified.setVisibility(View.GONE);
            /*et_card_name.setText(panData.PanName);
            et_pan_card_num.setText(panData.PanNumber);*/
            getAllStates();
        }
        else
        {
            getAllStates();
        }

        card_pan_upload.setOnClickListener(this);
        txt_date_birth.setOnClickListener(this);
        img_back.setOnClickListener(this);
        card_submit_pan_for_verification.setOnClickListener(this);
        img_cancel.setOnClickListener(this);
        txt_submit_pan_info.setOnClickListener(this);
        img_close.setOnClickListener(this);

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback()
        {
            @Override
            public void onStateChanged(@NonNull View view, int newState)
            {
                switch (newState)
                {
                    case BottomSheetBehavior.STATE_HIDDEN:
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        bg.setVisibility(View.GONE);
                        break;
                    }

                    case BottomSheetBehavior.STATE_EXPANDED:
                    {
                        bg.setVisibility(View.VISIBLE);
                        break;
                    }
                    case BottomSheetBehavior.STATE_DRAGGING:
                    {
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v)
            {


            }
        });

        cv_pan_name.setOnClickListener(v ->
        {
            FileUtils.requestfoucs(ActivityUploadPan.this,et_card_name);

        });

        cv_pan_number.setOnClickListener(v ->
        {
            FileUtils.requestfoucs(ActivityUploadPan.this,et_pan_card_num);

        });
    }

    private void getAllStates()
    {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityUploadPan.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<StatesListModel> signUpModelCall = apiInterface.getAllStates(Constants.GET_ALL_STATES);

        signUpModelCall.enqueue(new Callback<StatesListModel>()
        {
            @Override
            public void onResponse(@NonNull Call<StatesListModel> call, @NonNull Response<StatesListModel> response)
            {
                progressDialog.dismiss();
                StatesListModel statesListModel = response.body();
                assert statesListModel != null;
                if (statesListModel.status.equalsIgnoreCase("success"))
                {
                    for (int i=0;i<statesListModel.userDataClass.statesDataArrayList.size();i++)
                    {
                        stateslist.add(statesListModel.userDataClass.statesDataArrayList.get(i).Name);
                        statesId.add(statesListModel.userDataClass.statesDataArrayList.get(i).StateID);
                    }
                    sp_states.setText(statesListModel.userDataClass.statesDataArrayList.get(0).Name);
                    sp_states.setOnClickListener(v -> selectState());
                }

                else
                {
                    Constants.SnakeMessageYellow(root_verify_pan, "Something went wrong.Please try again.");
                }
            }

            @Override
            public void onFailure(@NonNull Call<StatesListModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void selectState()
    {
        if (ActivityUploadPan.this == null)
            return;
        if (stateslist.size() == 0)
        {
            Constants.SnakeMessageYellow(root_verify_pan, "no States found.");
            return;
        }
        Dialog dialog = new Dialog(ActivityUploadPan.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View outerView = LayoutInflater.from(ActivityUploadPan.this).inflate(R.layout.wheelview, null);
        dialog.setContentView(outerView);
        if (dialog.getWindow() != null)
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        CustomWheelView wv = outerView.findViewById(R.id.wh_view);
        TextView txt_ok = outerView.findViewById(R.id.txt_ok);
        TextView title = outerView.findViewById(R.id.title);
        title.setText("Select state");
        TextView txt_cancel = outerView.findViewById(R.id.txt_cancel);
        txt_ok.setOnClickListener(v ->
        {
            sp_states.setText(wv.getSeletedItem().toString());
            dialog.dismiss();

        });

        txt_cancel.setOnClickListener(v -> dialog.dismiss());
        wv.setOffset(1);
        wv.setItems(stateslist);
        wv.setSeletion(0);
        dialog.show();
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.card_pan_upload:
            {
                PickImageDialog.build(new PickSetup()).show(this);
                break;
            }
            case R.id.card_submit_pan_for_verification:
            {
                validate();
                break;
            }
            case R.id.txt_date_birth:
            {

                try
                {
                    Calendar calendar = Calendar.getInstance();
                    Calendar mMaxDate = Calendar.getInstance();
                    Calendar mMinDate = Calendar.getInstance();
                    mMaxDate.add(Calendar.YEAR, -18);
                    mMaxDate.setTimeInMillis(mMaxDate.getTimeInMillis());
                    mMinDate.set(Calendar.YEAR, 1920);
                    mMinDate.set(Calendar.MONTH, 1);
                    mMinDate.set(Calendar.DATE, 1);

                    int mMaxDateYear = mMaxDate.get(Calendar.YEAR);
                    int mMaxDateMonth = mMaxDate.get(Calendar.MONTH);
                    int mMaxDateDay = mMaxDate.get(Calendar.DAY_OF_MONTH);

                    int mMinDateYear = mMinDate.get(Calendar.YEAR);
                    int mMinDateMonth = mMinDate.get(Calendar.MONTH);
                    int mMinDateDay = mMinDate.get(Calendar.DAY_OF_MONTH);


                    new SpinnerDatePickerDialogBuilder().context(ActivityUploadPan.this).callback(this).spinnerTheme(R.style.NumberPickerStyle)
                            .showTitle(true).showDaySpinner(true).defaultDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                            .maxDate(mMaxDateYear, mMaxDateMonth, mMaxDateDay)
                            .minDate(mMinDateYear, mMinDateMonth, mMinDateDay).build().show();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;
            }

            case R.id.img_back:
            {
                finish();
                break;
            }
            case R.id.img_cancel:
            {
                pickResult=null;
                img_upload_hint.setImageDrawable(getResources().getDrawable(R.drawable.ic_id_card));
                img_upload_hint.setVisibility(View.VISIBLE);
                txt_mobile.setText(getResources().getString(R.string.upload_pan_card_image));
                img_cancel.setVisibility(View.GONE);
                break;
            }
            case R.id.txt_submit_pan_info:
            {
                txt_title_bs.setText(R.string.why_should_i_submit_my_pan_card);
                txt_bottom_msg.setText(R.string.pan_info);
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                bg.setVisibility(View.VISIBLE);
                break;
            }
            case R.id.img_close:
            {
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                bg.setVisibility(View.GONE);
                break;
            }


        }

    }

    private void validate()
    {
        if (pickResult==null)
        {
            Toast.makeText(ActivityUploadPan.this,"Select image of PAN card.",Toast.LENGTH_SHORT).show();
        }
        else if (et_card_name.getText().length()<3)
            Constants.SnakeMessageYellow(root_verify_pan, "Enter valid PAN Name.");
        else if (et_pan_card_num.getText().length()<8)
            Constants.SnakeMessageYellow(root_verify_pan, "Enter valid PAN Number.");
        else if(txt_date_birth.getText().length()<4)
        {
            Constants.SnakeMessageYellow(root_verify_pan, "Enter date of birth.");
        }
        else
        {
            try
            {
                sendToServer();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

    }

    private void sendToServer() throws IOException
    {

        final ProgressDialog progressDialog = new ProgressDialog(ActivityUploadPan.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        File file = new File(pickResult.getPath());
        OutputStream os ;
        try
        {
            os = new BufferedOutputStream(new FileOutputStream(file));
            pickResult.getBitmap().compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.close();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }



        int state_id= stateslist.indexOf(sp_states.getText().toString());
        RequestParams requestParams  = new RequestParams();
        try
        {
            requestParams.put("Action", Constants.UPDATE_PAN);
            requestParams.put("Val_Userid", Pref.getValue(this, Constants.UserID,"", Constants.FILENAME));
            requestParams.put("Val_UIpanname",et_card_name.getText().toString());
            requestParams.put("Val_UIpannumber",et_pan_card_num.getText().toString());
            requestParams.put("Val_UIpanimage", file);
            requestParams.put("Val_Ubirthdate", dob);
            requestParams.put("Val_Ustate", statesId.get(state_id));
            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();

            asyncHttpClient.post(this,Constants.BASE_API+Constants.UPDATE_PROFILE,requestParams,new JsonHttpResponseHandler()
            {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response)
                {
                    progressDialog.dismiss();
                    super.onSuccess(statusCode, headers, response);
                    if (response.optString("status").equalsIgnoreCase("success"))
                    {
                        Constants.SnakeMessageYellow(root_verify_pan, response.optString("message"));
                        new Handler().postDelayed(() -> finish(),1000);

                    }
                    else
                    {
                        Constants.SnakeMessageYellow(root_verify_pan, response.optString("message"));
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse)
                {
                    progressDialog.dismiss();
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse)
                {
                    progressDialog.dismiss();
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable)
                {
                    progressDialog.dismiss();
                    super.onFailure(statusCode, headers, responseString, throwable);
                }
            });
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onPickResult(PickResult pickResult)
    {
        if (pickResult.getError() == null)
        {
            this.pickResult=pickResult;
            txt_mobile.setText("IMAGE ATTACHED");
            img_upload_hint.setImageDrawable(getResources().getDrawable(R.drawable.ic_check));
            img_cancel.setVisibility(View.VISIBLE);
        }
        else
        {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Constants.SnakeMessageYellow(root_verify_pan, pickResult.getError().getMessage());
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
    {
        txt_date_birth.setText(dayOfMonth+"/"+(monthOfYear+1)+"/"+year);
        dob=year+"/"+(monthOfYear+1)+"/"+dayOfMonth;
    }
}
