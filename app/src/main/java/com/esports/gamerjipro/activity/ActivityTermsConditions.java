package com.esports.gamerjipro.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.text.method.ScrollingMovementMethod;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.esports.gamerjipro.Model.TCModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityTermsConditions extends AppCompatActivity {
    TextView txt_text, txt_title;
    APIInterface apiInterface;
    String from;
    RelativeLayout parent_layout;
    ImageView img_back;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activiy_terms_and_conditions);
        from = getIntent().getStringExtra("from");
        txt_text = findViewById(R.id.txt_text);
        txt_title = findViewById(R.id.txt_title);
        parent_layout = findViewById(R.id.parent_layout);
        img_back = findViewById(R.id.img_back);
        txt_title.setText(from);
        txt_text.setMovementMethod(new ScrollingMovementMethod());
        apiInterface = APIClient.getClient().create(APIInterface.class);
        if (from.equalsIgnoreCase("Terms and Conditions"))
            getData(Constants.GET_TERMS);
        else
            getData(Constants.GET_PRIVACY);
        img_back.setOnClickListener(v -> finish());
    }

    private void getData(String getPrivacy) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<TCModel> tcModelCall;

        tcModelCall = apiInterface.getTerms(getPrivacy);

        tcModelCall.enqueue(new Callback<TCModel>() {
            @Override
            public void onResponse(@NonNull Call<TCModel> call, @NonNull Response<TCModel> response) {
                progressDialog.dismiss();
                TCModel tcModel = response.body();
                assert tcModel != null;
                if (tcModel.status.equalsIgnoreCase("success")) {
                    Spanned htmlAsSpanned = Html.fromHtml(tcModel.data);
                    txt_text.setText(htmlAsSpanned);
                } else {
                    Constants.SnakeMessageYellow(parent_layout, tcModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<TCModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }
}