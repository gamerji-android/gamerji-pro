package com.esports.gamerjipro.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esports.gamerjipro.Adapter.GameContestListAdapter;
import com.esports.gamerjipro.Interface.ContestJoinClickListener;
import com.esports.gamerjipro.Interface.WinnerClickListener;
import com.esports.gamerjipro.Model.ContestTypeModelNew;
import com.esports.gamerjipro.Model.ContestTypesModel;
import com.esports.gamerjipro.Model.MyContestModel;
import com.esports.gamerjipro.Model.SignInModel;
import com.esports.gamerjipro.Model.TournamentDetailModel;
import com.esports.gamerjipro.Model.TournamentTypeModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.Pref;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityMyContestList extends AppCompatActivity implements WinnerClickListener, ContestJoinClickListener
{
    GameContestListAdapter gameContestListAdapter ;
    public RecyclerView rv_games;
    LinearLayout layoutBottomSheet;
    public BottomSheetBehavior sheetBehavior;
    View bg;
    ImageView img_image;
    TextView txt_join_contest,txt_title;
    CoordinatorLayout parent_lyt;
    public TextView txt_entry_fees,txt_usable_cash,txt_to_pay;
    APIInterface apiInterface;
    public String game_id,game_type_id,contest_id;
    public static ArrayList<SignInModel.UserData.GamesData.GameData> userGameDataarrayList= new ArrayList<>();
    public String UniqueName,game_name,userGameId;
    ImageView img_back;

    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_contest_activity);
        rv_games=findViewById(R.id.rv_games);
        bg=findViewById(R.id.bg);
        parent_lyt=findViewById(R.id.parent_lyt);
        img_back=findViewById(R.id.img_back);
        img_back.setOnClickListener(v -> finish());

        txt_title=findViewById(R.id.txt_title);
        if (getIntent().getExtras()!=null)
        {
            txt_title.setText(getIntent().getStringExtra("game_name"));
            game_name=getIntent().getStringExtra("game_name");
            game_id=getIntent().getStringExtra("game_id");

            game_type_id=getIntent().getStringExtra("game_type_id");
        }
        apiInterface = APIClient.getClient().create(APIInterface.class);
        img_image=findViewById(R.id.img_image);
        layoutBottomSheet=findViewById(R.id.bottom_sheet);
        txt_join_contest=findViewById(R.id.txt_join_contest);
        txt_entry_fees=findViewById(R.id.txt_entry_fees);
        txt_usable_cash=findViewById(R.id.txt_usable_cash);
        txt_to_pay=findViewById(R.id.txt_to_pay);
        rv_games.setLayoutManager(new LinearLayoutManager(this));
        rv_games.setItemAnimator(new DefaultItemAnimator());
        userGameDataarrayList= Constants.getUserData(ActivityMyContestList.this);
        getUserData();
        setMyContestList();
    }

    private void getUserData()
    {
        assert userGameDataarrayList != null;
        for (int i = 0; i<userGameDataarrayList.size(); i++)
        {
            if (userGameDataarrayList.get(i).GameID.equalsIgnoreCase(game_id))
            {
                userGameId=userGameDataarrayList.get(i).UserGameID;
                UniqueName=userGameDataarrayList.get(i).UniqueName;
            }
        }
    }

    private void setMyContestList()
    {

        final ProgressDialog progressDialog = new ProgressDialog(ActivityMyContestList.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<ContestTypesModel> contestTypesModelCall = apiInterface.getMyContests(Constants.GET_ALL_JOINED_CONTESTS, Pref.getValue(ActivityMyContestList.this, Constants.UserID,"", Constants.FILENAME),game_id,game_type_id);

        contestTypesModelCall.enqueue(new Callback<ContestTypesModel>()
        {
            @Override
            public void onResponse(@NonNull Call<ContestTypesModel> call, @NonNull Response<ContestTypesModel> response)
            {
                new Handler().postDelayed(progressDialog::dismiss,500);

                ContestTypesModel  contestTypesModel = response.body();
                assert contestTypesModel != null;
                if (contestTypesModel.status.equalsIgnoreCase("success"))
                {
                    Glide.with(ActivityMyContestList.this).load(contestTypesModel.DataClass.FeaturedImage).into(img_image);
                    gameContestListAdapter= new GameContestListAdapter(ActivityMyContestList.this,contestTypesModel.DataClass.typesDataArrayList,ActivityMyContestList.this,ActivityMyContestList.this);
                    rv_games.setAdapter(gameContestListAdapter);
                    Constants.SnakeMessageYellow(parent_lyt,contestTypesModel.message);
                }
                else
                {
                    Constants.SnakeMessageYellow(parent_lyt,contestTypesModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ContestTypesModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onWinnersClick(ContestTypesModel.Data.TypesData.ContestsData contestsData)
    {

    }

    @Override
    public void onWinnersClick(ContestTypeModelNew.Data.ContestsData contestsData) {

    }

    @Override
    public void onMyContestWinnerClick(MyContestModel.Data.ContestsData contestsData)
    {

    }

    @Override
    public void onTournamentWinnerClick(TournamentTypeModel.Data.TypesData.TournamentsData tournamentsData) {

    }

    @Override
    public void onTournamentContestWinnersClickListener(TournamentDetailModel.TournamentData.ContestsData contestsData) {

    }

    @Override
    public void onContestJoinClickLitener(ContestTypesModel.Data.TypesData.ContestsData contestsData)
    {

    }

    @Override
    public void onContestJoinClickLitener(ContestTypeModelNew.Data.ContestsData contestsData) {

    }
}
