package com.esports.gamerjipro.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.esports.gamerjipro.Adapter.DynamicGameContestAdapter;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.Utils.Constants;
import com.google.android.material.tabs.TabLayout;

public class GameContesTemp  extends AppCompatActivity
{
    TabLayout tabs;
    ViewPager viewPager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_contest);
        tabs=findViewById(R.id.tabs);
        viewPager=findViewById(R.id.frameLayout);

        for (int k = 0; k <10; k++)
        {
            tabs.addTab(tabs.newTab().setText("" + k));
        }

        DynamicGameContestAdapter adapter = new DynamicGameContestAdapter
                (getSupportFragmentManager(), tabs.getTabCount(), Constants.gdataArrayList);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(1);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));

        if (tabs.getTabCount() == 2)
        {
            tabs.setTabMode(TabLayout.MODE_FIXED);
        }
        else
        {
            tabs.setTabMode(TabLayout.MODE_SCROLLABLE);
        }
    }
}
