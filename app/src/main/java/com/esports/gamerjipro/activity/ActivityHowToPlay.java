package com.esports.gamerjipro.activity;

import android.os.Bundle;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.esports.gamerjipro.Fragment.HowToPlayKotlin;
import com.esports.gamerjipro.R;

public class ActivityHowToPlay extends FragmentActivity
{
    ImageView img_back;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_to_play);
        img_back=findViewById(R.id.img_back);
        img_back.setOnClickListener(v -> finish());

        getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, new HowToPlayKotlin()).commit();
    }
}