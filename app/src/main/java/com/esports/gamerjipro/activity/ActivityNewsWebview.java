package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.esports.gamerjipro.R;

public class ActivityNewsWebview extends AppCompatActivity
{
    WebView webView ;
    ImageView img_back;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_webview);
        webView=findViewById(R.id.wv);
        img_back=findViewById(R.id.img_back);
        img_back.setOnClickListener(v -> finish());

        webView.setWebChromeClient(new WebChromeClient());
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setDefaultFontSize(18);
        webView.loadUrl(getIntent().getStringExtra("url"));
    }

}
