package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.esports.gamerjipro.Model.ResendOTPModel;
import com.esports.gamerjipro.Model.SignInModel;
import com.esports.gamerjipro.Model.SignUpModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Common;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.FileUtils;
import com.esports.gamerjipro.Utils.Pref;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.hbb20.CountryCodePicker;

import org.json.JSONException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Activity_SignUp extends AppCompatActivity implements View.OnClickListener
{
    TextView txt_sign_in,txt_show,promo_applied_text,promo_applied_text_social,txt_tc,txt_privacy;
    CountryCodePicker ccp;
    EditText et_phone,et_email,et_pwd,et_coupon,et_phnone_botom,et_coupon_social;
    RelativeLayout lyt_signup;
    APIInterface apiInterface;
    LinearLayout layoutBottomSheet;
    BottomSheetBehavior sheetBehavior;
    View bg;
    ImageView img_close;
    AccessToken accessToken;
    String uid;
    CardView cv_fb,cv_google,card_submit_number,cv_apply_coupon,cv_apply_coupon_social;
    private FirebaseAuth mAuth;
    CallbackManager callbackManager = CallbackManager.Factory.create();
    private static final String TAG = Activity_Login.class.getSimpleName();
    GoogleSignInClient mGoogleSignInClient;
    int RC_SIGN_IN = 100;
    boolean apply_promo = false;
    boolean promo_success= false;
    boolean is_Social = false;
    TextView img_remove_coupon,img_remove_coupon_social;
    CoordinatorLayout parent_lyt;
    ImageView img_check,img_check_social;
    CardView cv_promo,cv_pass,cv_email,cv_phone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        txt_sign_in=findViewById(R.id.txt_sign_in);
        cv_apply_coupon=findViewById(R.id.cv_apply_coupon);
        promo_applied_text=findViewById(R.id.promo_applied_text);
        txt_show=findViewById(R.id.txt_show);
        ccp=findViewById(R.id.ccp);
        et_phone=findViewById(R.id.et_phn);
        img_check_social=findViewById(R.id.img_check_social);
        et_email=findViewById(R.id.et_email);
        img_check=findViewById(R.id.img_check);
        et_pwd=findViewById(R.id.et_pwd);
        et_coupon=findViewById(R.id.et_coupon);
        parent_lyt=findViewById(R.id.parent_lyt);
       // et_coupon.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        lyt_signup=findViewById(R.id.lyt_signup);
        img_close = findViewById(R.id.img_close);
        layoutBottomSheet=findViewById(R.id.bottom_sheet_number);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        cv_fb = findViewById(R.id.cv_fb);
        cv_google = findViewById(R.id.cv_google);
        card_submit_number = findViewById(R.id.card_submit_number);
        et_phnone_botom = findViewById(R.id.et_phnone_botom);
        img_remove_coupon = findViewById(R.id.img_remove_coupon);
        et_coupon_social = findViewById(R.id.et_coupon_social);
        cv_apply_coupon_social = findViewById(R.id.cv_apply_coupon_social);
        img_remove_coupon_social = findViewById(R.id.img_remove_coupon_social);
        promo_applied_text_social = findViewById(R.id.promo_applied_text_social);
        txt_tc = findViewById(R.id.txt_tc);
        txt_privacy = findViewById(R.id.txt_privacy);
        cv_phone = findViewById(R.id.cv_phone);
        cv_email = findViewById(R.id.cv_email);
        cv_pass = findViewById(R.id.cv_pass);
        cv_promo = findViewById(R.id.cv_promo);
        mAuth = FirebaseAuth.getInstance();
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        apiInterface = APIClient.getClient().create(APIInterface.class);
        txt_sign_in.setOnClickListener(v ->
        {
            Intent i= new Intent(Activity_SignUp.this,Activity_Login.class);
            startActivity(i);
            finish();
        });
        txt_tc.setOnClickListener(v ->
        {
            Intent i = new Intent(Activity_SignUp.this,ActivityTermsConditions.class);
            i.putExtra("from","Terms and Conditions");
            startActivity(i);

        });
        txt_privacy.setOnClickListener(v ->
        {
            Intent i = new Intent(Activity_SignUp.this,ActivityTermsConditions.class);
            i.putExtra("from","Privacy Policy");
            startActivity(i);

        });

        cv_promo.setOnClickListener(v -> FileUtils.requestfoucs(Activity_SignUp.this,et_coupon));

        cv_pass.setOnClickListener(v -> FileUtils.requestfoucs(Activity_SignUp.this,et_pwd));

        cv_email.setOnClickListener(v -> FileUtils.requestfoucs(Activity_SignUp.this,et_email));

        cv_phone.setOnClickListener(v -> FileUtils.requestfoucs(Activity_SignUp.this,et_phone));


        lyt_signup.setOnClickListener(this);
        txt_show.setOnClickListener(this);
        cv_fb.setOnClickListener(this);
        cv_google.setOnClickListener(this);
        card_submit_number.setOnClickListener(this);
        img_close.setOnClickListener(this);
        cv_apply_coupon.setOnClickListener(this);
        img_remove_coupon.setOnClickListener(this);
        et_coupon_social.setOnClickListener(this);
        cv_apply_coupon_social.setOnClickListener(this);
        img_remove_coupon_social.setOnClickListener(this);
        promo_applied_text_social.setOnClickListener(this);

        bg = findViewById(R.id.bg);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        accessToken = loginResult.getAccessToken();
                        uid = String.valueOf(accessToken.getUserId());

                        // LoginManager.getInstance().logOut();
                        getUserInformation(loginResult);
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception)
                    {
                        // App code
                    }
                });

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState)
            {
                switch (newState)
                {
                    case BottomSheetBehavior.STATE_HIDDEN:
                    {
                        bg.setVisibility(View.GONE);
                        break;
                    }

                    case BottomSheetBehavior.STATE_EXPANDED:
                    {
                        bg.setVisibility(View.VISIBLE);
                        break;
                    }
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        bg.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v)
            {


            }
        });

        et_coupon_social.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                if (et_coupon_social.getText().length()<1)
                {
                    apply_promo=false;
                }
            }
        });
    }

    private void getUserInformation(final LoginResult loginResul)
    {
        GraphRequest request = GraphRequest.newMeRequest(loginResul.getAccessToken(),
                (object, response) -> {

                    // Application code
                    if (object.has("email"))
                    {
                        try
                        {
                            String email = object.getString("email");
                            String name = object.getString("name"); // 01/31/1980 format
                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                        checkSocialSignIn(loginResul.getAccessToken().getUserId(),object.optString("email"), Constants.FACEBOOK,object.optString("first_name"),object.optString("last_name"));
                    }
                    else
                    {
                        Constants.SnakeMessageYellow(parent_lyt,"Email not found.");
                    }
                    // downloadFBFile(object.getJSONObject("picture").getJSONObject("data").getString("url"),object);
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,first_name,last_name");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void checkSocialSignIn(String social_id, final String email, String social_type, String firstname, String lastname)
    {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
            progressDialog.setCancelable(false);
            Call<SignInModel> signInModelCall;

        if(social_type.equalsIgnoreCase(Constants.GOOGLE))
            signInModelCall = apiInterface.doSignUp(Constants.REQUEST,"","",email,"",firstname,lastname,"", Constants.GOOGLE,"",social_id);
        else
            signInModelCall = apiInterface.doSignUp(Constants.REQUEST,"","",email,"",firstname,lastname,"", Constants.FACEBOOK,social_id,"");

        signInModelCall.enqueue(new Callback<SignInModel>()
        {
            @Override
            public void onResponse(@NonNull Call<SignInModel> call, @NonNull Response<SignInModel> response)
            {
                progressDialog.dismiss();
                SignInModel signInModel = response.body();
                assert signInModel != null;
                if (signInModel.status.equalsIgnoreCase("success"))
                {
                    if (signInModel.flag.equalsIgnoreCase(Constants.MOBILE_VERIFICATION_PENDING))
                    {
                        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED)
                        {
                            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                            Constants.setUserGameData(signInModel.userDataClass.GamesData.gamesDataArrayList,Activity_SignUp.this);
                            Pref.setValue(Activity_SignUp.this, Constants.UserID, signInModel.userDataClass.UserID, Constants.FILENAME);
                            is_Social=true;
                        }
                    }
                    else
                    {
                        Constants.SnakeMessageYellow(parent_lyt, signInModel.message);
                        Pref.setValue(Activity_SignUp.this, Constants.UserID, signInModel.userDataClass.UserID, Constants.FILENAME);
                        Pref.setValue(Activity_SignUp.this, Constants.FullName, signInModel.userDataClass.FullName, Constants.FILENAME);
                        Pref.setValue(Activity_SignUp.this, Constants.firstname, signInModel.userDataClass.FirstName, Constants.FILENAME);
                        Pref.setValue(Activity_SignUp.this, Constants.lastname, signInModel.userDataClass.LastName, Constants.FILENAME);
                        Pref.setValue(Activity_SignUp.this, Constants.CountryCode, signInModel.userDataClass.CountryCode, Constants.FILENAME);
                        Pref.setValue(Activity_SignUp.this, Constants.MobileNumber, signInModel.userDataClass.MobileNumber, Constants.FILENAME);
                        Pref.setValue(Activity_SignUp.this, Constants.EmailAddress,signInModel.userDataClass.EmailAddress, Constants.FILENAME);
                        if(!signInModel.userDataClass.BirthDate.isEmpty())
                        {
                            @SuppressLint("SimpleDateFormat") DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                            @SuppressLint("SimpleDateFormat") DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
                            String inputDateStr=signInModel.userDataClass.BirthDate;
                            Date date = null;
                            try
                            {
                                date = inputFormat.parse(inputDateStr);
                            }
                            catch (ParseException e)
                            {
                                e.printStackTrace();
                            }

                            String outputDateStr = outputFormat.format(date);
                            Pref.setValue(Activity_SignUp.this, Constants.BirthDate,outputDateStr, Constants.FILENAME);
                        }
                        Pref.setValue(Activity_SignUp.this, Constants.State,signInModel.userDataClass.State, Constants.FILENAME);
                        Pref.setValue(Activity_SignUp.this, Constants.IS_LOGIN,true, Constants.FILENAME);
                        Pref.setValue(Activity_SignUp.this, Constants.SOCIAL_LOGIN,true, Constants.FILENAME);


//                        Intent i = new Intent(Activity_SignUp.this,ActivityMain.class);
                        Intent i = new Intent(Activity_SignUp.this, BottomNavigationActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();
                    }
                }
                else
                {
                    Constants.SnakeMessageYellow(parent_lyt, signInModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<SignInModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void sendOTP()
    {
        final ProgressDialog progressDialog = new ProgressDialog(Activity_SignUp.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<ResendOTPModel> resendOTPModelCall = apiInterface.resendOTP(Constants.REQUEST,ccp.getSelectedCountryCodeWithPlus(),et_phnone_botom.getText().toString());

        resendOTPModelCall.enqueue(new Callback<ResendOTPModel>()
        {
            @Override
            public void onResponse(@NonNull Call<ResendOTPModel> call, @NonNull Response<ResendOTPModel> response)
            {
                progressDialog.dismiss();
                ResendOTPModel  resendOTPModel = response.body();
                assert resendOTPModel != null;
                if (resendOTPModel.status.equalsIgnoreCase("success"))
                {
                    Intent i = new Intent(Activity_SignUp.this,ActivityOTP.class);
                    i.putExtra("OTP",resendOTPModel.otpData.OTPCode);
                    i.putExtra("country_code",ccp.getSelectedCountryCodeWithPlus());
                    i.putExtra("phone",et_phnone_botom.getText().toString());
                    i.putExtra("coupon",et_coupon_social.getText().toString());
                    i.putExtra("from_login",true);
                    i.putExtra("is_social",true);
                    startActivity(i);
                }
                else
                {
                    Constants.SnakeMessageYellow(parent_lyt, resendOTPModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResendOTPModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }

        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN)
        {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try
            {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            }
            catch (ApiException e)
            {
                // Google Sign In failed, update UI appropriately
                Common.insertLog("Google sign in failed" +e);
                // ...
            }
        }
        else
        {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct)
    {
        Common.insertLog("firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task ->
                {
                    if (task.isSuccessful())
                    {
                        // Sign in success, update UI with the signed-in user's information
                        FirebaseUser user = mAuth.getCurrentUser();
                        String firstname,lastname;
                        firstname=acct.getDisplayName().split(" ")[0];
                        lastname=acct.getDisplayName().split(" ")[1];
                        checkSocialSignIn(acct.getId(),acct.getEmail(), Constants.GOOGLE,firstname,lastname);
                    }
                    else
                    {
                        // If sign in fails, display a message to the user.
                        Common.insertLog("signInWithCredential:failure" +task.getException());
                    }


                });
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId()) {
            case R.id.lyt_signup: {
                validate();
                break;
            }

            case R.id.cv_google: {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
            }

            case R.id.cv_fb: {
                LoginManager.getInstance().logInWithReadPermissions(this,
                        Arrays.asList("email", "public_profile"));
                break;
            }
            case R.id.img_close:
            {
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                is_Social=false;
                apply_promo=false;
                promo_success=false;
                img_remove_coupon_social.setVisibility(View.GONE);

                cv_apply_coupon_social.setVisibility(View.VISIBLE);
                promo_applied_text_social.setVisibility(View.GONE);
                break;
            }



            case R.id.txt_show:
            {
                if (txt_show.getText().toString().equalsIgnoreCase("Show"))
                {
                    et_pwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    et_pwd.setSelection(et_pwd.getText().length());
                    txt_show.setText("Hide");
                }
                else
                {
                    et_pwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    et_pwd.setSelection(et_pwd.getText().length());
                    txt_show.setText("Show");
                    break;
                }
                break;
            }

            case R.id.card_submit_number:
            {

                if (et_phnone_botom.getText().length() < 10)
                    Constants.SnakeMessageYellow(parent_lyt,"Please enter a valid mobile number" );
                else
                 {
                     if(apply_promo)
                     {
                         if (promo_success)
                         {
                             sendOTP();
                         }
                         else
                         {
                             Constants.SnakeMessageYellow(parent_lyt,"Enter valid coupon code." );
                         }
                     }
                     else
                     {
                         if (et_coupon_social.getText().toString().isEmpty())
                         sendOTP();
                         else
                             Constants.SnakeMessageYellow(parent_lyt, "Remove coupon or Enter valid coupon code.");
                     }

                }
                break;
            }

            case R.id.cv_apply_coupon:
            {
                if (et_coupon.getText().toString().isEmpty())
                    Constants.SnakeMessageYellow(parent_lyt," Please enter a valid signup code.");
                else
                applyPromo(et_coupon.getText().toString());
                break;
            }
            case R.id.cv_apply_coupon_social:
            {
                if (et_coupon_social.getText().toString().isEmpty())
                {
                    Constants.SnakeMessageYellow(parent_lyt,"Enter coupon code.");
                }
                else
                {
                    applyPromo(et_coupon_social.getText().toString());
                }

                break;
            }
            case R.id.img_remove_coupon:
            {
                img_remove_coupon.setVisibility(View.GONE);
                cv_apply_coupon.setVisibility(View.VISIBLE);
                promo_success= false;
                apply_promo=false;
                promo_applied_text.setVisibility(View.GONE);
                et_coupon.setText("");
                img_check.setVisibility(View.GONE);
                break;
            }
            case R.id.img_remove_coupon_social:
            {
                img_remove_coupon_social.setVisibility(View.GONE);
                img_check_social.setVisibility(View.GONE);
                cv_apply_coupon_social.setVisibility(View.VISIBLE);
                promo_success= false;
                apply_promo=false;
                promo_applied_text_social.setVisibility(View.GONE);
                et_coupon_social.setText("");
                break;
            }
        }

    }

    private void applyPromo(String s)
    {
        final ProgressDialog progressDialog = new ProgressDialog(Activity_SignUp.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<SignUpModel> signUpModelCall = apiInterface.applyPromo(Constants.ValidateSignupCode,s);

        signUpModelCall.enqueue(new Callback<SignUpModel>()
        {
            @Override
            public void onResponse(@NonNull Call<SignUpModel> call, @NonNull Response<SignUpModel> response)
            {
                progressDialog.dismiss();
                SignUpModel signUpModelCall = response.body();
                assert signUpModelCall != null;
                if (signUpModelCall.status.equalsIgnoreCase("success"))
                {
                    if (is_Social)
                    {
                        apply_promo=true;
                        promo_success=true;
                        Constants.SnakeMessageYellow(parent_lyt, signUpModelCall.message);
                        img_remove_coupon_social.setVisibility(View.VISIBLE);
                        img_check_social.setVisibility(View.VISIBLE);
                        cv_apply_coupon_social.setVisibility(View.GONE);
                        promo_applied_text_social.setText(et_coupon_social.getText().toString()+" Coupon Applied");
                        promo_applied_text_social.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        apply_promo=true;
                        promo_success=true;
                        Constants.SnakeMessageYellow(parent_lyt, signUpModelCall.message);
                        img_remove_coupon.setVisibility(View.VISIBLE);
                        img_check.setVisibility(View.VISIBLE);
                        cv_apply_coupon.setVisibility(View.GONE);
                        promo_applied_text.setText(et_coupon.getText().toString()+" Coupon Applied");
                        promo_applied_text.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    if(is_Social)
                    {
                        promo_success=false;
                        apply_promo=true;
                        Constants.SnakeMessageYellow(parent_lyt, signUpModelCall.message);
                        promo_applied_text_social.setVisibility(View.GONE);
                    }
                    else
                    {
                        promo_success=false;
                        apply_promo=true;
                        Constants.SnakeMessageYellow(parent_lyt, signUpModelCall.message);
                        promo_applied_text.setVisibility(View.GONE);
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<SignUpModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void validate()
    {
        if(et_phone.getText().toString().isEmpty())
        {
            Constants.SnakeMessageYellow(parent_lyt,"Please enter mobile number" );
        }
        else if (et_phone.getText().length()<10)
        {
            Constants.SnakeMessageYellow(parent_lyt, "Please enter a valid mobile number");
        }
        if (et_email.getText().toString().isEmpty())
        {
            Constants.SnakeMessageYellow(parent_lyt,"Please enter email address" );
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(et_email.getText()).matches())
        {
            Constants.SnakeMessageYellow(parent_lyt, "Please enter a valid email address");
        }
        else if (et_pwd.getText().toString().isEmpty())
        {
            Constants.SnakeMessageYellow(parent_lyt,"Please enter a password");
        }
        else if (et_pwd.getText().length()<8)
        {
            Constants.SnakeMessageYellow(parent_lyt, "Password length must be 8-20 characters");
        }
        else if(apply_promo)
        {
            if (!promo_success)
            {
                Constants.SnakeMessageYellow(parent_lyt,"Remove invalid coupon." );
            }
            else
            {
                checkMobile();
            }
        }
        else
        {
            if (et_coupon.getText().toString().isEmpty())

                checkMobile();
            else
                Constants.SnakeMessageYellow(parent_lyt,"Remove coupon or Enter valid coupon code." );
        }

                // sendToServer();
    }

    private void checkMobile()
    {
        final ProgressDialog progressDialog = new ProgressDialog(Activity_SignUp.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<SignUpModel> signUpModelCall = apiInterface.checkMobile(Constants.Check,ccp.getSelectedCountryCodeWithPlus(),et_phone.getText().toString().trim(),et_email.getText().toString());

        signUpModelCall.enqueue(new Callback<SignUpModel>()
        {
            @Override
            public void onResponse(@NonNull Call<SignUpModel> call, @NonNull Response<SignUpModel> response)
            {
                progressDialog.dismiss();
                SignUpModel signUpModelCall = response.body();
                assert signUpModelCall != null;
                if (signUpModelCall.status.equalsIgnoreCase("success"))
                {
                    Constants.SnakeMessageYellow(parent_lyt, signUpModelCall.message);
                    Intent i = new Intent(Activity_SignUp.this,ActivityOTP.class);
                    Pref.setValue(Activity_SignUp.this,Constants.UserID,signUpModelCall.userDataClass.UserID, Constants.FILENAME);
                    i.putExtra("OTP",signUpModelCall.userDataClass.OTP);
                    i.putExtra("country_code",ccp.getSelectedCountryCodeWithPlus());
                    i.putExtra("phone",et_phone.getText().toString().trim());
                    i.putExtra("email",et_email.getText().toString().trim());
                    i.putExtra("password",et_pwd.getText().toString());
                    i.putExtra("coupon",et_coupon.getText().toString());
                    i.putExtra("from_login",false);
                    i.putExtra("is_social",false);
                    startActivity(i);
                }
                else
                {
                    Constants.SnakeMessageYellow(parent_lyt, signUpModelCall.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<SignUpModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }


}
