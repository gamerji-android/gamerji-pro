package com.esports.gamerjipro.activity;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Adapter.ComaplintChatAdapter;
import com.esports.gamerjipro.BuildConfig;
import com.esports.gamerjipro.Model.GetMessagesModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.Pref;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityComplainTicketDetail extends AppCompatActivity
{
    RecyclerView rv_complain_chat;
    ComaplintChatAdapter comaplintChatAdapter ;
    FrameLayout lyt_send;
    EditText et_chat;
    int page=1;
    APIInterface apiInterface;
    String ticket_id;
    ImageView img_back;
    LinearLayoutManager  linearLayoutManager;
    private boolean isLastPage=false;
    RelativeLayout lyt_parent;
    ArrayList<GetMessagesModel.Data.MessagesData>  messagesDataArrayList = new ArrayList<>();
    int lastposition = 0;
    private boolean isLoading=false;
    TextView txt_subject,txt_status,txt_date;
    Timer timer ;
    String StatusValue;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_complaint_detail);
        txt_subject=findViewById(R.id.txt_subject);
        txt_status=findViewById(R.id.txt_status);
        txt_date=findViewById(R.id.txt_date);
        if (getIntent().getExtras()!=null)
        {
            ticket_id=getIntent().getStringExtra("ticket_id");
            txt_subject.setText(getIntent().getStringExtra("Subject"));
            txt_status.setText(getIntent().getStringExtra("Status"));
            txt_date.setText(getIntent().getStringExtra("Date"));
            StatusValue = getIntent().getStringExtra("StatusValue");
        }

        if(StatusValue.equalsIgnoreCase("1"))
        {
            txt_status.setTextColor(Color.BLACK);
        }
        else if (StatusValue.equalsIgnoreCase("2"))
        {
            txt_status.setTextColor(Color.RED);
        }
        else if (StatusValue.equalsIgnoreCase("3"))
        {
            txt_status.setTextColor(Color.YELLOW);
        }
        else if (StatusValue.equalsIgnoreCase("4"))
        {
            txt_status.setTextColor(getResources().getColor(R.color.green_color));
        }
        else if (StatusValue.equalsIgnoreCase("5"))
        {
            txt_status.setTextColor(Color.RED);
        }

        rv_complain_chat=findViewById(R.id.rv_complain_chat);
        lyt_send=findViewById(R.id.lyt_send);
        lyt_parent=findViewById(R.id.lyt_parent);
        img_back=findViewById(R.id.img_back);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        et_chat=findViewById(R.id.et_chat);
        linearLayoutManager= new LinearLayoutManager(this);
        rv_complain_chat.setLayoutManager(linearLayoutManager);
        comaplintChatAdapter= new ComaplintChatAdapter(ActivityComplainTicketDetail.this,messagesDataArrayList);
        rv_complain_chat.setAdapter(comaplintChatAdapter);
        timer= new Timer();
        getMessages(page);
        img_back.setOnClickListener(v -> finish());
        rv_complain_chat.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy)
            {
                    if (linearLayoutManager.findFirstVisibleItemPosition() == 0 && !isLoading)
                    {
                        if (!isLastPage)
                        getOldChats(++page);
                    }
            }
        });

        lyt_send.setOnClickListener(v ->
                {
                    if(!et_chat.getText().toString().trim().isEmpty())
                    sendMessage(et_chat.getText().toString());
                    else
                    Constants.SnakeMessageYellow(lyt_parent,"Please enter message.");
                }
        );
    }

    public void getOldChats(int page)
    {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityComplainTicketDetail.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<GetMessagesModel> resendOTPModelCall = apiInterface.getMessages(Constants.GET_CUSTOMER_TICKET_HISTORY, Pref.getValue(ActivityComplainTicketDetail.this,
                Constants.UserID,"",Constants.FILENAME), ticket_id,page,20);
        isLoading=true;
        resendOTPModelCall.enqueue(new Callback<GetMessagesModel>()
        {
            @Override
            public void onResponse(@NonNull Call<GetMessagesModel> call, @NonNull Response<GetMessagesModel> response)
            {
                progressDialog.dismiss();
                GetMessagesModel   getMessagesModel = response.body();
                assert getMessagesModel != null;
                if (getMessagesModel.status.equalsIgnoreCase("success"))
                {
                    if (messagesDataArrayList!= null && getMessagesModel.DataClass.messagesDataArrayList.size() > 0)
                    {
                        messagesDataArrayList.addAll(0,getMessagesModel.DataClass.messagesDataArrayList);
                        comaplintChatAdapter.notifyDataSetChanged();
                        rv_complain_chat.smoothScrollToPosition(20);
                        isLoading = false;
                        isLastPage=getMessagesModel.DataClass.IsLast;
                    }
                    else
                    {
                        isLoading=false;
                    }
                }
                else
                {
                    Constants.SnakeMessageYellow(lyt_parent,getMessagesModel.message);
                    isLoading=false;
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetMessagesModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
                isLoading=false;
            }
        });
    }

    private void sendMessage(String message)
    {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityComplainTicketDetail.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<GetMessagesModel.Data> resendOTPModelCall = apiInterface.sendMessages(Constants.CUSTOMER_SEND_MESSAGE, Pref.getValue(ActivityComplainTicketDetail.this,
                Constants.UserID,"",Constants.FILENAME), ticket_id,message, BuildConfig.VERSION_NAME,"Android");

        resendOTPModelCall.enqueue(new Callback<GetMessagesModel.Data>()
        {
            @Override
            public void onResponse(@NonNull Call<GetMessagesModel.Data> call, @NonNull Response<GetMessagesModel.Data> response)
            {
                progressDialog.dismiss();
                GetMessagesModel.Data   getMessagesModel = response.body();
                assert getMessagesModel != null;
                if (getMessagesModel.status.equalsIgnoreCase("success"))
                {
                    messagesDataArrayList.add(linearLayoutManager.getItemCount(),getMessagesModel.messagesData);
                    comaplintChatAdapter.notifyDataSetChanged();
                    et_chat.setText("");
                    linearLayoutManager.smoothScrollToPosition(rv_complain_chat,null,messagesDataArrayList.size()-1);
                }
                else
                {
                    Constants.SnakeMessageYellow(lyt_parent,getMessagesModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetMessagesModel.Data> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void getMessages(int page)
    {

        final ProgressDialog progressDialog = new ProgressDialog(ActivityComplainTicketDetail.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<GetMessagesModel> resendOTPModelCall = apiInterface.getMessages(Constants.GET_CUSTOMER_TICKET_HISTORY, Pref.getValue(ActivityComplainTicketDetail.this,
                Constants.UserID,"",Constants.FILENAME), ticket_id,page,20);

        resendOTPModelCall.enqueue(new Callback<GetMessagesModel>()
        {
            @Override
            public void onResponse(@NonNull Call<GetMessagesModel> call, @NonNull Response<GetMessagesModel> response)
            {
                progressDialog.dismiss();
                GetMessagesModel   getMessagesModel = response.body();
                assert getMessagesModel != null;
                if (getMessagesModel.status.equalsIgnoreCase("success"))
                {
                    messagesDataArrayList.addAll(getMessagesModel.DataClass.messagesDataArrayList);
                    comaplintChatAdapter.notifyDataSetChanged();
                    isLastPage=getMessagesModel.DataClass.IsLast;
                    rv_complain_chat.scrollToPosition(messagesDataArrayList.size()-1);

                    timer.schedule(new TimerTask()
                    {
                        @Override
                        public void run()
                        {
                            getNewMessage();
                        }
                    },0,5000);
                }
                else
                {
                    Constants.SnakeMessageYellow(lyt_parent,getMessagesModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetMessagesModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void getNewMessage()
    {
        Call<GetMessagesModel> getMessagesModelCall = apiInterface.getNewCustomerCareMessage(Constants.GET_CUSTOMER_TICKET_NEW_MSG, Pref.getValue(ActivityComplainTicketDetail.this,
                Constants.UserID,"",Constants.FILENAME), ticket_id,messagesDataArrayList.get(messagesDataArrayList.size()-1).MessageID);

        getMessagesModelCall.enqueue(new Callback<GetMessagesModel>()
        {
            @Override
            public void onResponse(@NonNull Call<GetMessagesModel> call, @NonNull Response<GetMessagesModel> response)
            {
                GetMessagesModel   getMessagesModel = response.body();
                assert getMessagesModel != null;
                if (getMessagesModel.status.equalsIgnoreCase("success"))
                {
                    messagesDataArrayList.addAll(messagesDataArrayList.size(),getMessagesModel.DataClass.messagesDataArrayList);
                    comaplintChatAdapter.notifyDataSetChanged();
                    rv_complain_chat.smoothScrollToPosition(messagesDataArrayList.size()-1);
                }
                else
                {
                    Constants.SnakeMessageYellow(lyt_parent,getMessagesModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetMessagesModel> call, @NonNull Throwable t)
            {
                t.printStackTrace();
            }
        });
    }

    @Override
    protected void onDestroy()
    {
        timer.cancel();
        super.onDestroy();
    }
}
