package com.esports.gamerjipro.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.esports.gamerjipro.R;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.FileUtils;
import com.esports.gamerjipro.cashFree.ActivityPayment;

public class ActivityUpiPayment extends AppCompatActivity
{
    EditText et_upi_address;
    CardView cv_add_balance,cv_upi;
    String amount;
    String game_id,game_type_id,game_name;
    ImageView img_back;
    RelativeLayout lyt_parent;
    boolean is_tournament=false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        if (getIntent().getExtras()!=null)
        {
            game_id = getIntent().getStringExtra("game_id");
            game_type_id = getIntent().getStringExtra("game_type_id");
            game_name = getIntent().getStringExtra("game_name");
            is_tournament=getIntent().getBooleanExtra("is_tournament",false);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upi_layout);
        et_upi_address=findViewById(R.id.et_upi_address);
        cv_upi=findViewById(R.id.cv_upi);
        lyt_parent=findViewById(R.id.lyt_parent);
        cv_add_balance=findViewById(R.id.cv_add_balance);
        img_back=findViewById(R.id.img_back);
        img_back.setOnClickListener(v -> finish());
        amount=getIntent().getStringExtra("amount");
        cv_add_balance.setOnClickListener(v ->
        {
            if (!et_upi_address.getText().toString().matches("[a-zA-Z0-9._-]+@[a-z]+"))
            {
                Constants.SnakeMessageYellow(lyt_parent,"Enter proper UPI address");
            }
            else
            {
                Intent i = new Intent(this, ActivityPayment.class);
                i.putExtra("type", Constants.CASHFREE_UPI);
                i.putExtra("upi",et_upi_address.getText().toString());
                i.putExtra("amount",amount);
                i.putExtra("game_id",game_id);
                i.putExtra("game_type_id",game_type_id);
                i.putExtra("game_name",game_name);
                i.putExtra("is_tournament",is_tournament);
                startActivityForResult(i, Constants.PAYMENT_RESULT);
            }

        });
        cv_upi.setOnClickListener(v -> FileUtils.requestfoucs(ActivityUpiPayment.this,et_upi_address));

    }
}
