package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Adapter.NotificationAdapter;
import com.esports.gamerjipro.Model.NotificationModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.Pref;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import cz.msebera.android.httpclient.Header;

public class NotificationActivity extends AppCompatActivity {

    RecyclerView mRecyclerNotifications;
    APIInterface apiInterface;
    ArrayList<NotificationModel> notificationModelArrayList = new ArrayList<>();
    ImageView img_back;
    LinearLayoutManager mLinearLayoutManager;
    RelativeLayout mNotificationRootView;
    boolean isLoading,isLastPage=false;
    NotificationAdapter notificationAdapter ;
    int count=1;
    LinearLayout mLinearNoNotification;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        img_back = findViewById(R.id.img_back);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        mLinearNoNotification = findViewById(R.id.mLinearNoNotification);
        mNotificationRootView = findViewById(R.id.mNotificationRootView);
        //   mTxtClear.setOnClickListener(v -> showAlert());
        // getNotification(count);
        img_back.setOnClickListener(v -> finish());
        Initialization();
    }

    @SuppressLint("WrongConstant")
    void Initialization() {
        mRecyclerNotifications = findViewById(R.id.mRecyclerNotifications);
        mLinearLayoutManager = new LinearLayoutManager(NotificationActivity.this, LinearLayoutManager.VERTICAL, false);
        mRecyclerNotifications.setLayoutManager(mLinearLayoutManager);
        mRecyclerNotifications.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState)
            {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = mLinearLayoutManager.getChildCount();
                int totalItemCount = mLinearLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLinearLayoutManager.findFirstVisibleItemPosition();

                if (!isLoading && !isLastPage)
                {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= 10)
                    {
                        getNotification(++count);
                    }
                }
            }
        });
        getNotification(count);
    }

    private void getNotification(int count)
    {
        final ProgressDialog progressDialog = new ProgressDialog(NotificationActivity.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        RequestParams requestParams  = new RequestParams();
        requestParams.put("Action", Constants.GET_NOTIFICATIONS);
        requestParams.put("Val_Page",count);
        requestParams.put("Val_Limit",10);
        requestParams.put("Val_Userid", Pref.getValue(this, Constants.UserID,"", Constants.FILENAME));
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();

        asyncHttpClient.post(this,Constants.BASE_API+Constants.USER_FETCH,requestParams,new JsonHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response)
            {
                isLoading = false;
                super.onSuccess(statusCode, headers, response);
                progressDialog.dismiss();
                if (response.optString("status").equalsIgnoreCase("success"))
                {
                    JSONObject dataJSON= response.optJSONObject("data");
                    if (dataJSON.optBoolean("IsLast"))
                        isLastPage=true;

                    JSONObject notificationdataJSON = dataJSON.optJSONObject("NotificationsData");
                    for (String key : iterate(notificationdataJSON.keys()))
                    {
                        NotificationModel notificationModel = new NotificationModel();
                        JSONArray notification_data= notificationdataJSON.optJSONArray(key);
                        ArrayList<NotificationModel.NotificationDetailModel> detailArrayList= new ArrayList<>();
                        for (int i=0;i<notification_data.length();i++)
                        {
                            JSONObject jsonObject = notification_data.optJSONObject(i);
                            NotificationModel.NotificationDetailModel notificationDetailModel = new NotificationModel.NotificationDetailModel();
                            notificationDetailModel.setCreationDate(jsonObject.optString("CreationDate"));
                            notificationDetailModel.setCreationDateAgo(jsonObject.optString("CreationDateAgo"));
                            notificationDetailModel.setDescription(jsonObject.optString("Description"));
                            notificationDetailModel.setNotificationID(jsonObject.optString("NotificationID"));
                            notificationDetailModel.setRelationID(jsonObject.optString("RelationID"));
                            notificationDetailModel.setType(jsonObject.optString("Type"));
                            notificationDetailModel.setUserID(jsonObject.optString("UserID"));
                            detailArrayList.add(notificationDetailModel);
                        }
                        notificationModel.setDate(key);
                        notificationModel.setNotificationDetailModels(detailArrayList);
                        notificationModelArrayList.add(notificationModel);
                    }
                    notificationAdapter= new NotificationAdapter(NotificationActivity.this,notificationModelArrayList);
                    mRecyclerNotifications.setAdapter(notificationAdapter);
                }
                else
                {
                    Constants.SnakeMessageYellow(mNotificationRootView,response.optString("status"));
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse)
            {
                progressDialog.dismiss();
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse)
            {
                progressDialog.dismiss();
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable)
            {
                progressDialog.dismiss();
                super.onFailure(statusCode, headers, responseString, throwable);
            }
        });
    }

    private <T> Iterable<T> iterate(final Iterator<T> i)
    {
        return () -> i;
    }

}
