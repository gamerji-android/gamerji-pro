package com.esports.gamerjipro.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Constants;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityLinkPaytm extends AppCompatActivity
{
    EditText et_mobile;
    CardView cv_link;
    RelativeLayout lyt_parent;
    APIInterface apiInterface ;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_link_paytm);
        et_mobile=findViewById(R.id.et_mobile);
        cv_link=findViewById(R.id.cv_link);
        lyt_parent=findViewById(R.id.lyt_parent);
        apiInterface = APIClient.getPaytmClient().create(APIInterface.class);

        cv_link.setOnClickListener(v ->
        {
            if (et_mobile.getText().length()<10)
            {
                Constants.SnakeMessageYellow(lyt_parent,"Enter valid mobile number");
            }
            else
            {
                validatePaytm();
                /*Intent i = new Intent(ActivityLinkPaytm.this,ActivityOTP.class);
                i.putExtra("phone",et_mobile.getText().toString());
                startActivity(i);*/
            }

        });

    }

    private void validatePaytm()
    {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityLinkPaytm.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<JsonObject> signUpModelCall = apiInterface.validatePaytm(et_mobile.getText().toString(),"wallet","Token");

        signUpModelCall.enqueue(new Callback<JsonObject>()
        {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response)
            {
                progressDialog.dismiss();
                JsonObject jsonObject= response.body();

                assert jsonObject != null;
            }

            @Override
            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }
}
