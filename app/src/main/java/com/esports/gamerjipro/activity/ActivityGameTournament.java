package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.esports.gamerjipro.Adapter.GameTournamentListAdapter;
import com.esports.gamerjipro.Adapter.TournamentTimeAdapter;
import com.esports.gamerjipro.Adapter.WinnerPoolAdapter;
import com.esports.gamerjipro.Interface.OnTournamentShowTimeListener;
import com.esports.gamerjipro.Interface.RulesClickListener;
import com.esports.gamerjipro.Interface.TournamentTimeSelectedListener;
import com.esports.gamerjipro.Interface.WinnerClickListener;
import com.esports.gamerjipro.Model.ContestTypeModelNew;
import com.esports.gamerjipro.Model.ContestTypesModel;
import com.esports.gamerjipro.Model.JoinContestModel;
import com.esports.gamerjipro.Model.MyContestModel;
import com.esports.gamerjipro.Model.ReportsDataModel;
import com.esports.gamerjipro.Model.RulesModel;
import com.esports.gamerjipro.Model.SignInModel;
import com.esports.gamerjipro.Model.TournamentDetailModel;
import com.esports.gamerjipro.Model.TournamentTimeSlotModel;
import com.esports.gamerjipro.Model.TournamentTypeModel;
import com.esports.gamerjipro.Model.WalletUsageLimitModel;
import com.esports.gamerjipro.Model.WinnerContestModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.AppConstants;
import com.esports.gamerjipro.Utils.Common;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.FileUtils;
import com.esports.gamerjipro.Utils.Pref;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityGameTournament extends AppCompatActivity implements
        WinnerClickListener, View.OnClickListener, OnTournamentShowTimeListener, TournamentTimeSelectedListener, RulesClickListener {
    GameTournamentListAdapter gameContestListAdapter;
    public static ArrayList<SignInModel.UserData.GamesData.GameData> userGameDataarrayList = new ArrayList<>();
    public RelativeLayout lyt_empty, bottom_how_to;
    public BottomSheetBehavior sheetBehavior, bottom_sheet_cash_bonus, pool_price_bottomsheet, rules_bottomsheet, tournamnet_time_bottomsheet;
    public TextView txt_entry_fees, txt_usable_cash, txt_to_pay, txt_game_type, txt_title_bs, txt_bottom_msg;
    public String game_id = "", game_type_id = "", tournament_id = "", contest_id = "";
    String main_game_name;
    RelativeLayout lyt_sub_main;
    View bg;
    public String UniqueName = "", game_name, userGameId;
    public boolean isEmpty = true;
    CardView cv_join_contest, cv_next, cv_tournament_time_next;
    APIInterface apiInterface;
    RecyclerView rv_games, rv_pool_price, rv_tournament_time;
    LinearLayout layoutBottomSheet, bonus_bottom_layout, pool_price_bottom, tournament_time_bottom;
    ImageView img_image, img_close, img_info, img_close_cash, img_close_rules, iv_close_pool_price, img_back;
    WinnerPoolAdapter winnerPoolAdapter;
    TextView txt_title, tv_pool_price, txt_my_balance, txt_notes;
    CoordinatorLayout parent_lyt;
    TournamentTimeAdapter tournamentTimeAdapter;
    boolean JoinFlag = true;
    String JoinButtonFlag;
    public SwipeRefreshLayout swipe_refresh_home;
    float add_balance, my_balance, topay;
    TextView txt_pool_price, txt_winning_rupee;
    int CanJoinPlayers, CanJoinExtraPlayers;
    TextView txt_warning;
    TournamentTypeModel.Data.TypesData.TournamentsData tournamentsData;
    TextView img_news, tv_title;
    ImageView img_close_join;
    TextView txt_tournament_time_title, tv_tournament_time_type, txt_tournament_name, txt_tournament_date, txt_tournamnet_time_notes;
    ImageView iv_close_tournament_time;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_contest_activity);

        img_news = findViewById(R.id.img_news);
        img_news.setOnClickListener(v -> FileUtils.OpenNewsWebview(this, "https://www.gamerji.com"));

        rv_games = findViewById(R.id.rv_games);
        lyt_sub_main = findViewById(R.id.lyt_sub_main);
        parent_lyt = findViewById(R.id.parent_lyt);
        tv_pool_price = findViewById(R.id.tv_pool_price);
        rv_pool_price = findViewById(R.id.rv_pool_price);
        lyt_empty = findViewById(R.id.lyt_empty);
        swipe_refresh_home = findViewById(R.id.swipe_refresh_home);

        iv_close_pool_price = findViewById(R.id.iv_close_pool_price);
        txt_pool_price = findViewById(R.id.txt_pool_price);
        txt_winning_rupee = findViewById(R.id.txt_winning_rupee);
        txt_my_balance = findViewById(R.id.txt_my_balance);
        img_info = findViewById(R.id.img_info);
        bg = findViewById(R.id.bg);
        img_close = findViewById(R.id.img_close);
        txt_warning = findViewById(R.id.txt_warning);

        img_close_cash = findViewById(R.id.img_close_cash);
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(v -> finish());

        bottom_how_to = findViewById(R.id.bottom_how_to);
        img_close_rules = bottom_how_to.findViewById(R.id.img_close);
        rules_bottomsheet = BottomSheetBehavior.from(bottom_how_to);

        txt_title_bs = findViewById(R.id.txt_title_bs);
        txt_bottom_msg = findViewById(R.id.txt_bottom_msg);

        txt_title = findViewById(R.id.txt_title);
        if (getIntent().getExtras() != null) {
            if (getIntent().getStringExtra("game_type_name") == null) {
                txt_title.setText(getIntent().getStringExtra("game_name"));
            } else {
                txt_title.setText(getIntent().getStringExtra("game_name") + " - " + getIntent().getStringExtra("game_type_name"));
            }
            game_name = getIntent().getStringExtra("game_name");
            game_id = getIntent().getStringExtra("game_id");
            main_game_name = getIntent().getStringExtra("main_game_name");

            if (getIntent().getStringExtra("main_game_name") == null)
                main_game_name = Constants.Main_game_name;

            game_type_id = getIntent().getStringExtra("game_type_id");
        }

        Common.insertLog("Main Game Name:::> " +main_game_name);
        if (Constants.Main_game_name.equalsIgnoreCase(AppConstants.GAME_P_MOBILE)) {
            txt_warning.setVisibility(View.VISIBLE);
        }
        apiInterface = APIClient.getClient().create(APIInterface.class);
        img_image = findViewById(R.id.img_image);
        layoutBottomSheet = findViewById(R.id.bottom_sheet);

        cv_join_contest = layoutBottomSheet.findViewById(R.id.cv_join_contest);

        // ToDo: New -> Ads
        bonus_bottom_layout = findViewById(R.id.bottom_sheet_cash_bonus);
        pool_price_bottom = findViewById(R.id.pool_price_bottom);
        tournament_time_bottom = findViewById(R.id.tournament_time_bottomsheet);

        txt_tournament_time_title = tournament_time_bottom.findViewById(R.id.txt_tournament_time_title);
        iv_close_tournament_time = tournament_time_bottom.findViewById(R.id.iv_close_tournament_time);
        tv_tournament_time_type = tournament_time_bottom.findViewById(R.id.tv_tournament_time_type);
        txt_tournament_name = tournament_time_bottom.findViewById(R.id.txt_tournament_name);
        txt_tournament_date = tournament_time_bottom.findViewById(R.id.txt_tournament_date);
        txt_tournamnet_time_notes = tournament_time_bottom.findViewById(R.id.txt_tournamnet_time_notes);
        rv_tournament_time = tournament_time_bottom.findViewById(R.id.rv_tournament_time);
        cv_tournament_time_next = tournament_time_bottom.findViewById(R.id.cv_tournament_time_next);


        txt_entry_fees = findViewById(R.id.txt_entry_fees);
        txt_game_type = findViewById(R.id.txt_game_type);
        cv_next = findViewById(R.id.cv_next);
        txt_notes = findViewById(R.id.txt_notes);
        txt_game_type.setText("  " + main_game_name + "-" + game_name);
        txt_usable_cash = findViewById(R.id.txt_usable_cash);
        txt_to_pay = findViewById(R.id.txt_to_pay);
        rv_games.setLayoutManager(new LinearLayoutManager(this));
        rv_games.setItemAnimator(new DefaultItemAnimator());
        rv_games.setHasFixedSize(true);
        img_close_join = findViewById(R.id.img_close_join);

        rv_pool_price.setLayoutManager(new LinearLayoutManager(this));
        rv_pool_price.setItemAnimator(new DefaultItemAnimator());

        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        bottom_sheet_cash_bonus = BottomSheetBehavior.from(bonus_bottom_layout);
        pool_price_bottomsheet = BottomSheetBehavior.from(pool_price_bottom);
        tournamnet_time_bottomsheet = BottomSheetBehavior.from(tournament_time_bottom);
        tv_title = findViewById(R.id.tv_title);
        userGameDataarrayList = Constants.getUserData(ActivityGameTournament.this);

        getUserData();
        setGameTournamentList();
        cv_join_contest.setOnClickListener(this);

        img_close_rules.setOnClickListener(v ->
        {
            rules_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
            bg.setVisibility(View.GONE);
        });

        iv_close_tournament_time.setOnClickListener(v ->
        {
            tournamnet_time_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
            bg.setVisibility(View.GONE);
        });

        img_close.setOnClickListener(v ->
        {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            bottom_sheet_cash_bonus.setState(BottomSheetBehavior.STATE_COLLAPSED);
            rules_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            bg.setVisibility(View.GONE);
        });

        img_close_cash.setOnClickListener(v ->
        {
            bottom_sheet_cash_bonus.setState(BottomSheetBehavior.STATE_COLLAPSED);
            bg.setVisibility(View.GONE);
        });

        iv_close_pool_price.setOnClickListener(v ->
        {
            pool_price_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
            cv_next.setVisibility(View.GONE);
            txt_notes.setVisibility(View.GONE);
        });

        img_close_join.setOnClickListener(v ->
        {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            bg.setVisibility(View.GONE);
        });

        img_info.setOnClickListener(v -> bottom_sheet_cash_bonus.setState(BottomSheetBehavior.STATE_EXPANDED));

        pool_price_bottomsheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        bg.setVisibility(View.VISIBLE);
                    }
                    break;

                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        bg.setVisibility(View.GONE);
                        cv_next.setVisibility(View.GONE);
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:

                    case BottomSheetBehavior.STATE_SETTLING: {
                        break;
                    }
                }

            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });

        tournamnet_time_bottomsheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        bg.setVisibility(View.VISIBLE);
                    }
                    break;

                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        bg.setVisibility(View.GONE);
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:

                    case BottomSheetBehavior.STATE_SETTLING: {
                        break;
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        bg.setVisibility(View.VISIBLE);
                    }
                    break;

                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        bg.setVisibility(View.GONE);
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:

                    case BottomSheetBehavior.STATE_SETTLING: {
                        break;
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });

        img_close.setOnClickListener(v -> rules_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED));

        rules_bottomsheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        bg.setVisibility(View.VISIBLE);
                    }
                    break;

                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        bg.setVisibility(View.GONE);
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:

                    case BottomSheetBehavior.STATE_SETTLING: {
                        break;
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });

        swipe_refresh_home.setOnRefreshListener(this::setGameTournamentList);
    }

    private void joinTournament() {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityGameTournament.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        Call<JoinContestModel> joinContestModelCall = apiInterface.joinTournament(Constants.JOIN_TOURNAMENT, Pref.getValue(ActivityGameTournament.this, Constants.UserID, "", Constants.FILENAME), tournament_id, contest_id);

        joinContestModelCall.enqueue(new Callback<JoinContestModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<JoinContestModel> call, @NonNull Response<JoinContestModel> response) {
                progressDialog.dismiss();
                JoinContestModel joinContestModel = response.body();
                assert joinContestModel != null;
                if (joinContestModel.status.equalsIgnoreCase("success")) {
                    Intent i = new Intent(ActivityGameTournament.this, ActivityTournamentDetail.class);
                    i.putExtra("contest_id", tournament_id);
                    startActivity(i);
                    finish();
                } else {
                    Constants.SnakeMessageYellow(parent_lyt, joinContestModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<JoinContestModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void getUserData() {
        assert userGameDataarrayList != null;
        for (int i = 0; i < userGameDataarrayList.size(); i++) {
            if (userGameDataarrayList.get(i).GameID.equalsIgnoreCase(game_id)) {
                userGameId = userGameDataarrayList.get(i).UserGameID;
                UniqueName = userGameDataarrayList.get(i).UniqueName;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void setGameTournamentList() {

        final ProgressDialog progressDialog = new ProgressDialog(ActivityGameTournament.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<TournamentTypeModel> contestTypesModelCall = apiInterface.getAllTournent(Constants.GET_ALL_TOURNAMENT, Pref.getValue(ActivityGameTournament.this, Constants.UserID, "", Constants.FILENAME), game_id, game_type_id);

        contestTypesModelCall.enqueue(new Callback<TournamentTypeModel>() {
            @Override
            public void onResponse(@NonNull Call<TournamentTypeModel> call, @NonNull Response<TournamentTypeModel> response) {
                progressDialog.dismiss();
                swipe_refresh_home.setRefreshing(false);
                TournamentTypeModel contestTypesModel = response.body();
                assert contestTypesModel != null;

                if (contestTypesModel.status.equalsIgnoreCase("success")) {
                    Glide.with(ActivityGameTournament.this).load(contestTypesModel.DataClass.FeaturedImage).into(img_image);
                    lyt_empty.setVisibility(View.GONE);
                    swipe_refresh_home.setVisibility(View.VISIBLE);
                    ArrayList<TournamentTypeModel.Data.TypesData> typesData = new ArrayList<>();
                    for (int i = 0; i < contestTypesModel.DataClass.typesDataArrayList.size(); i++) {
                        if (Integer.parseInt(contestTypesModel.DataClass.typesDataArrayList.get(i).TournamentsCount) > 0) {
                            typesData.add(contestTypesModel.DataClass.typesDataArrayList.get(i));
                        }
                    }
                    gameContestListAdapter = new GameTournamentListAdapter(ActivityGameTournament.this, typesData, ActivityGameTournament.this, ActivityGameTournament.this, ActivityGameTournament.this);
                    rv_games.setAdapter(gameContestListAdapter);
                } else {
                    Constants.SnakeMessageYellow(parent_lyt, contestTypesModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<TournamentTypeModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                swipe_refresh_home.setRefreshing(false);
                t.printStackTrace();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.PROFILE_UPDATE_STATUS_REQUEST_CODE) {
            assert data != null;
            userGameDataarrayList = Constants.getUserData(ActivityGameTournament.this);
            getUserData();
            if (data.getBooleanExtra("profile_update_status", false)) {
                getWalletUsageLimit(tournamentsData);
                    /*if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED)
                    {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    }
                    else
                    {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    }*/
            }
        }
    }

    @Override
    public void onWinnersClick(ContestTypesModel.Data.TypesData.ContestsData contestsData) {
    }

    @Override
    public void onWinnersClick(ContestTypeModelNew.Data.ContestsData contestsData) {

    }

    @Override
    public void onTournamentWinnerClick(TournamentTypeModel.Data.TypesData.TournamentsData tournamentsData) {
        if (tournamentsData.WinningAmount.equalsIgnoreCase("0") || tournamentsData.WinningAmount.equalsIgnoreCase("")) {
            txt_pool_price.setVisibility(View.GONE);
            txt_winning_rupee.setVisibility(View.GONE);
        } else {
            txt_pool_price.setVisibility(View.VISIBLE);
            txt_winning_rupee.setVisibility(View.VISIBLE);
            tv_pool_price.setText(tournamentsData.WinningAmount);
        }

        getWinnerPool(tournamentsData);
    }

    @Override
    public void onTournamentContestWinnersClickListener(TournamentDetailModel.TournamentData.ContestsData contestsData) {

    }

    private void getWinnerPool(TournamentTypeModel.Data.TypesData.TournamentsData contestsData) {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityGameTournament.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<WinnerContestModel> contestDetailsModelCall = apiInterface.getTournamentWinners(Constants.GET_TOURNAMENT_WINNING_POOL, contestsData.TournamentID);

        contestDetailsModelCall.enqueue(new Callback<WinnerContestModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<WinnerContestModel> call, @NonNull Response<WinnerContestModel> response) {
                progressDialog.dismiss();
                tv_title.setText(R.string.winning_breakup);
                txt_pool_price.setText(R.string.prize_pool);
                txt_winning_rupee.setVisibility(View.VISIBLE);
                tv_pool_price.setVisibility(View.VISIBLE);
                WinnerContestModel winnerContestModel = response.body();
                assert winnerContestModel != null;
                if (winnerContestModel.status.equalsIgnoreCase("success")) {
                    tv_pool_price.setText(winnerContestModel.data.TotalPrice);
                    winnerPoolAdapter = new WinnerPoolAdapter(ActivityGameTournament.this, winnerContestModel.data.prizePoolsData);
                    rv_pool_price.setAdapter(winnerPoolAdapter);
                    pool_price_bottomsheet.setState(BottomSheetBehavior.STATE_EXPANDED);
                    bg.setVisibility(View.VISIBLE);
                } else {
                    Constants.SnakeMessageYellow(parent_lyt, winnerContestModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<WinnerContestModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onMyContestWinnerClick(MyContestModel.Data.ContestsData contestsData) {
    }

    private void getWalletUsageLimit(TournamentTypeModel.Data.TypesData.TournamentsData contestsData) {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityGameTournament.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<WalletUsageLimitModel> contestDetailsModelCall = apiInterface.getUserWalletLimitTournament(Constants.WALLET_USAGE_LIMIT, Pref.getValue(ActivityGameTournament.this, Constants.UserID, "", Constants.FILENAME), contestsData.TournamentID);

        contestDetailsModelCall.enqueue(new Callback<WalletUsageLimitModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<WalletUsageLimitModel> call, @NonNull Response<WalletUsageLimitModel> response) {
                progressDialog.dismiss();
                if (tournamnet_time_bottomsheet.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    tournamnet_time_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    bg.setVisibility(View.GONE);
                }
                WalletUsageLimitModel walletUsageLimitModel = response.body();
                assert walletUsageLimitModel != null;
                if (walletUsageLimitModel.status.equalsIgnoreCase("success")) {
                    tournament_id = contestsData.TournamentID;
                    txt_entry_fees.setText(walletUsageLimitModel.data.EntryFee);
                    txt_usable_cash.setText(walletUsageLimitModel.data.CashBalance);
                    txt_to_pay.setText(walletUsageLimitModel.data.ToPay);
                    JoinFlag = walletUsageLimitModel.data.JoinFlag;
                    JoinButtonFlag = walletUsageLimitModel.data.JoinButtonFlag;
                    txt_my_balance.setText(walletUsageLimitModel.data.WalletBalance);
                    my_balance = Float.parseFloat(walletUsageLimitModel.data.WalletBalance);
                    topay = Float.parseFloat(walletUsageLimitModel.data.ToPay);

                    if (JoinButtonFlag.equals(Constants.DEFAULT_JOIN)) {
                        // ToDo: New -> Ads
//                        cv_video_join.setVisibility(View.GONE);
                        cv_join_contest.setVisibility(View.VISIBLE);
                    } else if (JoinButtonFlag.equals(Constants.VIDEO_JOIN)) {
                        cv_join_contest.setVisibility(View.GONE);
                        // ToDo: New -> Ads
//                        cv_video_join.setVisibility(View.VISIBLE);
                    } else {
                        cv_join_contest.setVisibility(View.VISIBLE);
                        // ToDo: New -> Ads
//                        cv_video_join.setVisibility(View.VISIBLE);
                    }

                    if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    } else {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    }
                } else {
                    Constants.SnakeMessageYellow(parent_lyt, walletUsageLimitModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<WalletUsageLimitModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cv_join_contest: {
                getUserData();
                if (JoinFlag) {
                    if (Constants.CheckRestrictedStates(ActivityGameTournament.this))
                        Constants.SnakeMessageYellow(parent_lyt, getResources().getString(R.string.restricted_states_msg));
                    else if (!Constants.isCitizenOfIndia(ActivityGameTournament.this))
                        Constants.SnakeMessageYellow(parent_lyt, getResources().getString(R.string.restricted_country_msg));
                    else {
                        if (CanJoinPlayers == 1) {
                            joinTournament();
                        } else if (CanJoinPlayers > 1) {
                            Intent i = new Intent(ActivityGameTournament.this, ActivitySquadRegistration.class);
                            i.putExtra("CanJoinPlayers", CanJoinPlayers);
                            i.putExtra("CanJoinExtraPlayers", CanJoinExtraPlayers);
                            i.putExtra("UniqueName", UniqueName);
                            i.putExtra("contest_id", contest_id);
                            i.putExtra("main_game_name", main_game_name);
                            i.putExtra("tournament_id", tournament_id);
                            i.putExtra("JoinButtonFlag", JoinButtonFlag);
                            i.putExtra("game_id", game_id);
                            startActivity(i);
                        }
                    }
                } else {
                    if (UniqueName.equalsIgnoreCase("")) {
                        Intent i = new Intent(ActivityGameTournament.this, ActivityDOB.class);
                        i.putExtra("game_name", game_name);
                        i.putExtra("user_game_id", userGameId);
                        i.putExtra("game_id", game_id);
                        i.putExtra("main_game_name", main_game_name);
                        startActivityForResult(i, Constants.PROFILE_UPDATE_STATUS_REQUEST_CODE);
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    } else if (!(Pref.getValue(ActivityGameTournament.this, Constants.State, "", Constants.FILENAME).equalsIgnoreCase(""))
                            && !(Pref.getValue(ActivityGameTournament.this, Constants.BirthDate, "", Constants.FILENAME).equalsIgnoreCase(""))) {
                        Intent i = new Intent(ActivityGameTournament.this, ActivityAddBalance.class);
                        add_balance = topay - my_balance;
                        i.putExtra("amount_to_add", String.valueOf(add_balance));
                        i.putExtra("amount", String.valueOf(my_balance));
                        i.putExtra("game_id", game_id);
                        i.putExtra("game_name", game_name);
                        i.putExtra("game_type_id", game_type_id);
                        i.putExtra("is_tournament", true);
                        startActivity(i);

                    }
                }
                break;
            }
        }
    }

    @Override
    public void OnTournamentTimeSelected(TournamentTypeModel.Data.TypesData.TournamentsData tournamentsData) {
        if (tournamentsData.Joined) {
            Intent i = new Intent(ActivityGameTournament.this, ActivityTournamentDetail.class);
            i.putExtra("contest_id", tournamentsData.TournamentID);
            startActivity(i);
            finish();
        } else {
            getTimings(tournamentsData);
        }

    }

    private void getTimings(TournamentTypeModel.Data.TypesData.TournamentsData tournamentsData) {
        txt_tournament_time_title.setText(main_game_name);
        tv_tournament_time_type.setText(tournamentsData.GameTypeName);
        txt_tournament_name.setText(tournamentsData.Title);

        @SuppressLint("SimpleDateFormat") DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        @SuppressLint("SimpleDateFormat") DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
        String inputDateStr = tournamentsData.Date;
        Date date = null;
        try {
            date = inputFormat.parse(inputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String outputDateStr = outputFormat.format(date);

        txt_tournament_date.setText(outputDateStr);

        this.tournamentsData = tournamentsData;

        final ProgressDialog progressDialog = new ProgressDialog(ActivityGameTournament.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<TournamentTimeSlotModel> tournamentTimeSlotModelCall = apiInterface.getTimeSlots(Constants.GET_TIME_SLOTS, Pref.getValue(ActivityGameTournament.this, Constants.UserID, "",
                Constants.FILENAME), tournamentsData.TournamentID);

        tournamentTimeSlotModelCall.enqueue(new Callback<TournamentTimeSlotModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<TournamentTimeSlotModel> call, @NonNull Response<TournamentTimeSlotModel> response) {
                progressDialog.dismiss();
                TournamentTimeSlotModel tournamentTimeSlotModel = response.body();
                assert tournamentTimeSlotModel != null;
                if (tournamentTimeSlotModel.status.equalsIgnoreCase("success")) {
                    tournamentTimeAdapter = new TournamentTimeAdapter(ActivityGameTournament.this, tournamentTimeSlotModel.DataClass.timeSlotsArrayList, ActivityGameTournament.this);
                    rv_tournament_time.setAdapter(tournamentTimeAdapter);
                    rv_tournament_time.addItemDecoration(new DividerItemDecoration(ActivityGameTournament.this,
                            DividerItemDecoration.VERTICAL));
                    tournamnet_time_bottomsheet.setState(BottomSheetBehavior.STATE_EXPANDED);
                    bg.setVisibility(View.VISIBLE);
                    txt_tournamnet_time_notes.setText(tournamentTimeSlotModel.DataClass.InfoText);
                } else {
                    Constants.SnakeMessageYellow(parent_lyt, tournamentTimeSlotModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<TournamentTimeSlotModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });

    }

    @Override
    public void onTournamentTimeSelectedListener(TournamentTimeSlotModel.Data.TimeSlots timeSlots) {
        contest_id = timeSlots.ContestID;
        cv_tournament_time_next.setOnClickListener(v ->
        {
            tournament_id = tournamentsData.TournamentID;
            txt_entry_fees.setText(tournamentsData.EntryFee);
            txt_usable_cash.setText("0");
            txt_to_pay.setText(tournamentsData.EntryFee);
            CanJoinPlayers = tournamentsData.CanJoinPlayers;
            CanJoinExtraPlayers = tournamentsData.CanJoinExtraPlayers;

            if (UniqueName.equalsIgnoreCase("") || Pref.getValue(ActivityGameTournament.this, Constants.BirthDate, "", Constants.FILENAME).equalsIgnoreCase("")) {
                Intent i = new Intent(ActivityGameTournament.this, ActivityDOB.class);
                i.putExtra("game_name", game_name);
                i.putExtra("user_game_id", userGameId);
                i.putExtra("game_id", game_id);
                i.putExtra("main_game_name", main_game_name);
                startActivityForResult(i, Constants.PROFILE_UPDATE_STATUS_REQUEST_CODE);
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            } else {
                getWalletUsageLimit(tournamentsData);
            }
        });
    }

    @Override
    public void onReportSelected(ReportsDataModel.ReportsData reportsData) {

    }

    @Override
    public void OnRulesClickListener(TournamentTypeModel.Data.TypesData.TournamentsData tournamentsData) {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityGameTournament.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<RulesModel> rulesModelCall = apiInterface.getRules(Constants.GET_RULES, Pref.getValue(ActivityGameTournament.this, Constants.UserID, "",
                Constants.FILENAME), tournamentsData.TournamentID);

        rulesModelCall.enqueue(new Callback<RulesModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<RulesModel> call, @NonNull Response<RulesModel> response) {
                progressDialog.dismiss();
                RulesModel rulesModel = response.body();
                assert rulesModel != null;

                if (rulesModel.status.equalsIgnoreCase("success")) {
                    txt_title_bs.setText("Rules");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        txt_bottom_msg.setText(Html.fromHtml(rulesModel.Data.Rules, Html.FROM_HTML_SEPARATOR_LINE_BREAK_DIV));
                    } else {
                        txt_bottom_msg.setText(Html.fromHtml(rulesModel.Data.Rules));
                    }
                    rules_bottomsheet.setState(BottomSheetBehavior.STATE_EXPANDED);
                    bg.setVisibility(View.VISIBLE);
                } else {
                    Constants.SnakeMessageYellow(parent_lyt, rulesModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<RulesModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });

    }

    @Override
    public void OnMyContestRulesClickListener(MyContestModel.Data.ContestsData contestsData) {
    }
}
