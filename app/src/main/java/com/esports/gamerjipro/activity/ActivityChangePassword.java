package com.esports.gamerjipro.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.esports.gamerjipro.Model.GeneralResponseModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.FileUtils;
import com.esports.gamerjipro.Utils.Pref;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityChangePassword extends AppCompatActivity implements View.OnClickListener {
    EditText et_pwd, et_new_pwd, et_cnf_pwd;
    TextView txt_curernt_show, txt_new_show, txt_cnf_new_show;
    RelativeLayout parent_lyt;
    CardView proceed, cv_cnf_pass, cv_new_pass, cv_cur_pass;
    APIInterface apiInterface;
    ImageView img_back;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        img_back = findViewById(R.id.img_back);
        et_pwd = findViewById(R.id.et_pwd);
        et_new_pwd = findViewById(R.id.et_new_pwd);
        et_cnf_pwd = findViewById(R.id.et_cnf_pwd);
        txt_curernt_show = findViewById(R.id.txt_curernt_show);
        txt_new_show = findViewById(R.id.txt_new_show);
        txt_cnf_new_show = findViewById(R.id.txt_cnf_new_show);
        proceed = findViewById(R.id.proceed);
        parent_lyt = findViewById(R.id.parent_lyt);
        cv_cnf_pass = findViewById(R.id.cv_cnf_pass);
        cv_new_pass = findViewById(R.id.cv_new_pass);
        cv_cur_pass = findViewById(R.id.cv_cur_pass);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        img_back.setOnClickListener(v -> finish());
        txt_cnf_new_show.setOnClickListener(this);
        txt_curernt_show.setOnClickListener(this);
        txt_new_show.setOnClickListener(this);
        txt_new_show.setOnClickListener(this);
        proceed.setOnClickListener(this);

        cv_cur_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FileUtils.requestfoucs(ActivityChangePassword.this, et_pwd);

            }
        });

        cv_new_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FileUtils.requestfoucs(ActivityChangePassword.this, et_new_pwd);

            }
        });

        cv_cnf_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FileUtils.requestfoucs(ActivityChangePassword.this, et_cnf_pwd);

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_curernt_show: {
                if (txt_curernt_show.getText().toString().equalsIgnoreCase("Show")) {
                    et_pwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    txt_curernt_show.setText("Hide");
                } else {
                    et_pwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    txt_curernt_show.setText("Show");
                    break;
                }
                break;
            }

            case R.id.txt_new_show: {
                if (txt_new_show.getText().toString().equalsIgnoreCase("Show")) {
                    et_new_pwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    txt_new_show.setText("Hide");
                } else {
                    et_new_pwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    txt_new_show.setText("Show");
                    break;
                }
                break;
            }

            case R.id.txt_cnf_new_show: {
                if (txt_cnf_new_show.getText().toString().equalsIgnoreCase("Show")) {
                    et_cnf_pwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    txt_cnf_new_show.setText("Hide");
                } else {
                    et_cnf_pwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    txt_cnf_new_show.setText("Show");
                    break;
                }
                break;
            }
            case R.id.proceed: {
                validate();
                break;
            }

        }
    }

    private void validate() {
        if (et_pwd.getText().toString().length() < 8) {
            Constants.SnakeMessageYellow(parent_lyt, "Enter current password.");
        } else if (et_new_pwd.getText().length() < 8) {
            Constants.SnakeMessageYellow(parent_lyt, "New password should be between 8 to 10 characters.");
        } else if (!(et_cnf_pwd.getText().toString().equals(et_cnf_pwd.getText().toString()))) {
            Constants.SnakeMessageYellow(parent_lyt, "Password and Confirm password doesn't match.");
        } else
            sendToServer();
    }

    private void sendToServer() {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityChangePassword.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<GeneralResponseModel> generalResponseModelCall = apiInterface.changePassword(Constants.CHANGE_PASSWORD, Pref.getValue(ActivityChangePassword.this, Constants.UserID, "", Constants.FILENAME),
                et_pwd.getText().toString(), et_new_pwd.getText().toString());

        generalResponseModelCall.enqueue(new Callback<GeneralResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<GeneralResponseModel> call, @NonNull Response<GeneralResponseModel> response) {
                progressDialog.dismiss();
                GeneralResponseModel generalResponseModel = response.body();
                assert generalResponseModel != null;
                if (generalResponseModel.status.equalsIgnoreCase("success"))
                    Constants.SnakeMessageYellow(parent_lyt, generalResponseModel.message);
                else
                    Constants.SnakeMessageYellow(parent_lyt, generalResponseModel.message);
            }

            @Override
            public void onFailure(@NonNull Call<GeneralResponseModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }
}
