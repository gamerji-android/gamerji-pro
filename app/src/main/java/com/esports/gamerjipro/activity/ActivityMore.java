/*
package com.esports.gamerjipro.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.esports.gamerjipro.R;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.Pref;

public class ActivityMore extends AppCompatActivity implements View.OnClickListener
{
    CardView cv_legality,cv_invite,cv_cc,cv_promo,cv_friend,card_logout,cv_leaderboard,cv_play,cv_pasword,cv_gamerji_points,cv_youtube;
    LinearLayout child_legal;
    ImageView img_back,img_nxt;
    TextView txt_privacy,txt_tc;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more);
        cv_legality=findViewById(R.id.cv_legality);
        cv_play=findViewById(R.id.cv_play);
        child_legal=findViewById(R.id.child_legal);
        cv_gamerji_points=findViewById(R.id.cv_gamerji_points);
        cv_invite=findViewById(R.id.cv_invite);
        cv_cc=findViewById(R.id.cv_cc);
        cv_promo=findViewById(R.id.cv_promo);
        cv_friend=findViewById(R.id.cv_friend);
        txt_privacy=findViewById(R.id.txt_privacy);
        img_nxt=findViewById(R.id.img_nxt);
        txt_tc=findViewById(R.id.txt_tc);
        card_logout=findViewById(R.id.card_logout);
        cv_leaderboard=findViewById(R.id.cv_leaderboard);
        cv_pasword=findViewById(R.id.cv_pasword);
        img_back=findViewById(R.id.img_back);
        cv_youtube=findViewById(R.id.cv_youtube);
        cv_legality.setOnClickListener(this);
        cv_cc.setOnClickListener(this);
        cv_promo.setOnClickListener(this);
        cv_friend.setOnClickListener(this);
        card_logout.setOnClickListener(this);
        cv_play.setOnClickListener(this);
        txt_privacy.setOnClickListener(this);
        txt_tc.setOnClickListener(this);
        cv_pasword.setOnClickListener(this);
        cv_gamerji_points.setOnClickListener(this);
        img_back.setOnClickListener(v -> finish());
      //  child_legal.getLayoutTransition().enableTransitionType(LayoutTransition.CHANGING);


        cv_invite.setOnClickListener(this);
        cv_leaderboard.setOnClickListener(this);
        cv_youtube.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.cv_legality:
            {
                if (child_legal.getVisibility()==View.VISIBLE)
                {
                    child_legal.animate().setDuration(1000);
                    new Handler().postDelayed(() ->
                    {
                        child_legal.setVisibility(View.GONE);
                    },100);
                    img_nxt.setImageResource(R.drawable.ic_next_arrow);
                }
                else
                {
                    child_legal.setVisibility(View.VISIBLE);
                    img_nxt.setImageResource(R.drawable.ic_down_arrow);
                }
                break;
            }
            case R.id.cv_invite:
            {
                Intent i= new Intent(this, ActivityJoinInvite.class);
                startActivity(i);
                break;
            }
            case R.id.cv_cc:
            {
                Intent i= new Intent(this, ActivityCustomerCareTicket.class);
                startActivity(i);
                break;
            }
            case R.id.cv_promo:
            {
                Intent i= new Intent(this, ActivityPromoCode.class);
                startActivity(i);
                break;
            }
            case R.id.cv_friend:
            {
                Intent i= new Intent(this, ActivityReferral.class);
                startActivity(i);
                break;
            }
            case R.id.cv_leaderboard:
            {
                Intent i= new Intent(this, ActivityLeaderbaordNew.class);
                startActivity(i);
                break;
            }

            case R.id.cv_play:
            {
                Intent i= new Intent(this, ActivityHowToPlay.class);
                startActivity(i);
                break;
            }

            case R.id.txt_tc:
            {
                Intent i= new Intent(this, ActivityTermsConditions.class);
                i.putExtra("from","Terms and Conditions");
                startActivity(i);
                break;
            }

            case R.id.txt_privacy:
            {
                Intent i= new Intent(this, ActivityTermsConditions.class);
                i.putExtra("from","Privacy Policy");
                startActivity(i);
                break;
            }
            case R.id.cv_gamerji_points:
            {
                Intent i= new Intent(this, ActivityGamerjiPoints.class);
                startActivity(i);
                break;
            }

            case R.id.cv_youtube:
            {
                Intent i= new Intent(this, ActivityYoutubePlayer.class);
                startActivity(i);
                break;
            }



            case R.id.card_logout:
            {
                Intent i = new Intent(ActivityMore.this,Activity_Login.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                Pref.ClearAllPref(ActivityMore.this, Constants.FILENAME);
                startActivity(i);
                finish();
                break;
            }
            case R.id.cv_pasword:
            {
                Intent i= new Intent(this, ActivityChangePassword.class);
                startActivity(i);
                break;
            }
        }

    }
}
*/
