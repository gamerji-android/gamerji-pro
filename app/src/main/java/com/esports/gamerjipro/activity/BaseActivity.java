package com.esports.gamerjipro.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AppCompatActivity;

import com.esports.gamerjipro.R;

public class BaseActivity extends AppCompatActivity {

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init();
    }

    /**
     * Initialize progress dialog
     */
    private void init() {
        try {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            mProgressDialog.setIndeterminate(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Show progress dialog
     *
     * @param context      - Context
     * @param isCancelable - Is Cancelable
     */
    public void showDialog(Context context, boolean isCancelable) {
        try {
            mProgressDialog.setMessage(context.getResources().getString(R.string.dialog_please_wait_message));
            mProgressDialog.setCancelable(isCancelable);
            mProgressDialog.setCanceledOnTouchOutside(isCancelable);
            mProgressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Hide progress dialog
     */
    public void hideDialog() {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Hide the Soft Keyboard
     */
    public void hideSoftKeyboard() {
        try {
            if (getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
