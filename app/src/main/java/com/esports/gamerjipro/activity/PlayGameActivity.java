package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.esports.gamerjipro.Model.QuickGameEndGameModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.RestApis.WebFields;
import com.esports.gamerjipro.Utils.AppConstants;
import com.esports.gamerjipro.Utils.Common;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.SessionManager;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlayGameActivity extends Activity {

    private RelativeLayout mRelativeMain;
    private WebView mWebView;
    private ImageView mImageBack;
    private String mGameId, mCategoryId, mGameURL;
    private int mGameDisplayMode, mGameJoinedId;
    private SessionManager mSessionManager;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_play_game);
        mSessionManager = new SessionManager();

        getBundle();
        getIds();
        setRegListeners();
        setOrientation();
        loadGameToWebView();
    }

    /**
     * Get the keys from the Quick Game Sub Types Activity
     */
    private void getBundle() {
        try {
            mGameId = getIntent().getStringExtra(AppConstants.BUNDLE_QUICK_GAME_ID);
            mCategoryId = getIntent().getStringExtra(AppConstants.BUNDLE_QUICK_GAME_CATEGORY_ID);
            mGameURL = getIntent().getStringExtra(AppConstants.BUNDLE_QUICK_GAME_URL);
            mGameDisplayMode = getIntent().getIntExtra(AppConstants.BUNDLE_QUICK_GAME_DISPLAY_MODE, 0);
            mGameJoinedId = getIntent().getIntExtra(AppConstants.BUNDLE_QUICK_GAME_JOINED_ID, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Relative Layout
            mRelativeMain = findViewById(R.id.relative_play_game_main_view);

            // Web View
            mWebView = findViewById(R.id.web_view_play_game);

            // Image View
            mImageBack = findViewById(R.id.image_play_game_back);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    @SuppressLint("SetTextI18n")
    private void setRegListeners() {
        try {
            // ToDo: On Click Listener
            mImageBack.setOnClickListener(clickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.image_play_game_back:
                    callToEndGameAPI();
                    break;
            }
        }
    };

    /**
     * Sets the data
     */
    private void setOrientation() {
        try {
            if (mGameDisplayMode == 1) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should load the game to the web view
     */
    @SuppressLint("SetJavaScriptEnabled")
    private void loadGameToWebView() {
        mWebView.setWebChromeClient(new WebChromeClient());
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl(mGameURL);
    }

    @Override
    public void onBackPressed() {
        callToEndGameAPI();
    }

    /**
     * This method should call the End Game API
     */
    private void callToEndGameAPI() {
        try {
            progressDialog = new ProgressDialog(PlayGameActivity.this);
            progressDialog.setMessage("Please wait...."); // Setting Message
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
            progressDialog.show(); // Display Progress Dialog
            progressDialog.setCancelable(false);

            String mUserId = mSessionManager.getUserId(PlayGameActivity.this);

            Call<QuickGameEndGameModel> callRepos = APIClient.getClient().create(APIInterface.class).quickGameEndGame(WebFields.QUICK_GAME_END_GAME.MODE, mUserId, mGameId, mCategoryId, String.valueOf(mGameJoinedId));
            callRepos.enqueue(new Callback<QuickGameEndGameModel>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(@NonNull Call<QuickGameEndGameModel> call, @NonNull Response<QuickGameEndGameModel> response) {
                    progressDialog.dismiss();

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        assert response.body() != null;
                        if (response.body().getStatus().equalsIgnoreCase(Constants.API_SUCCESS)) {
                            finish();
                        } else {
                            Constants.SnakeMessageYellow(mRelativeMain, response.body().getMessage());
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<QuickGameEndGameModel> call, @NonNull Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}