package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.esports.gamerjipro.Adapter.SearchResultAdapter;
import com.esports.gamerjipro.Fragment.FragmentAllTimeLeaderboard;
import com.esports.gamerjipro.Fragment.FragmentByWeekLeaderboard;
import com.esports.gamerjipro.Fragment.FragmentMonthlyLeaderboard;
import com.esports.gamerjipro.Fragment.FragmentTodayLeaderboard;
import com.esports.gamerjipro.Model.SearchModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.Pref;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityLeaderbaordNew extends AppCompatActivity
{
    APIInterface apiInterface;
    TextView txt_title;
    ImageView img_back,img_search;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    ImageView img_profile;
    View bg;
    public LinearLayout parent_layout;
    RelativeLayout lyt_search_et;
    LinearLayout lyt_search;
    EditText et_search;
    RecyclerView rv_search;
    private SearchResultAdapter searchResultAdapter ;
    boolean isSearch=false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_leaderboard);

        viewPager = findViewById(R.id.viewPager);
        parent_layout = findViewById(R.id.parent_layout);
        img_search = findViewById(R.id.img_search);
        img_back = findViewById(R.id.img_back);
        txt_title = findViewById(R.id.txt_title);
        tabLayout = findViewById(R.id.tabLayout);
        lyt_search_et = findViewById(R.id.lyt_search_et);
        lyt_search = findViewById(R.id.lyt_search);
        et_search = findViewById(R.id.et_search);
        rv_search = findViewById(R.id.rv_search);
        rv_search.setLayoutManager(new LinearLayoutManager(this));
        rv_search.setItemAnimator(new DefaultItemAnimator());
        setupViewPager(viewPager);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        img_profile = findViewById(R.id.img_profile);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabTextColors(ColorStateList.valueOf(Color.WHITE));
        tabLayout.setSelectedTabIndicatorHeight(10);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.orange_color));

        int wantedTabIndex = 0;
        Typeface font = null;
        font = ResourcesCompat.getFont(ActivityLeaderbaordNew.this,R.font.poppins_semibold);
        TextView tv = (TextView)(((LinearLayout)((LinearLayout)tabLayout.getChildAt(0)).getChildAt(wantedTabIndex)).getChildAt(1));
        tv.setTypeface(font);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener()
        {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
                ViewGroup vgTab = (ViewGroup) vg.getChildAt(tab.getPosition());
                int tabChildsCount = vgTab.getChildCount();
                for (int i = 0; i < tabChildsCount; i++)
                {
                    View tabViewChild = vgTab.getChildAt(i);
                    if (tabViewChild instanceof TextView)
                    {
                        Typeface font = null;
                        font = ResourcesCompat.getFont(ActivityLeaderbaordNew.this,R.font.poppins_semibold);
                        ((TextView) tabViewChild).setTypeface(font);
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab)
            {
                ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
                ViewGroup vgTab = (ViewGroup) vg.getChildAt(tab.getPosition());
                int tabChildsCount = vgTab.getChildCount();
                for (int i = 0; i < tabChildsCount; i++)
                {
                    View tabViewChild = vgTab.getChildAt(i);
                    if (tabViewChild instanceof TextView)
                    {
                        Typeface font = null;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                        {
                            font = getResources().getFont(R.font.poppins);
                            ((TextView) tabViewChild).setTypeface(font);
                        }
                    }
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab)
            {

            }
        });
        
        bg=findViewById(R.id.bg);
        img_back.setOnClickListener(v ->
        {
            if(isSearch)
            {
                isSearch=false;
                tabLayout.setVisibility(View.VISIBLE);
                viewPager.setVisibility(View.VISIBLE);
                lyt_search.setVisibility(View.GONE);
                lyt_search_et.setVisibility(View.GONE);
                img_back.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_back));
                img_search.setVisibility(View.VISIBLE);
            }
            else
            {
                finish();
            }
        });

        img_search.setOnClickListener(v ->
        {
            tabLayout.setVisibility(View.GONE);
            viewPager.setVisibility(View.GONE);
            lyt_search.setVisibility(View.VISIBLE);
            lyt_search_et.setVisibility(View.VISIBLE);
            img_search.setVisibility(View.GONE);
            isSearch=true;
            img_back.setImageDrawable(getResources().getDrawable(R.drawable.ic_close_white));
            et_search.addTextChangedListener(new TextWatcher()
            {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after)
                {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count)
                {

                }

                @Override
                public void afterTextChanged(Editable s)
                {
                    new Handler().postDelayed(() -> getSearchResult(s.toString()),1500);
                }
            });
        });
    }

    private void getSearchResult(String s)
    {

        Call<SearchModel> searchModelCall = apiInterface.searchUser(Constants.SEARCH_USER, Pref.getValue(ActivityLeaderbaordNew.this, Constants.UserID, "", Constants.FILENAME),s);

        searchModelCall.enqueue(new Callback<SearchModel>()
        {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<SearchModel> call, @NonNull Response<SearchModel> response)
            {
                SearchModel searchModel = response.body();
                assert searchModel != null;
                if (searchModel.status.equalsIgnoreCase("success"))
                {
                    searchResultAdapter = new SearchResultAdapter(ActivityLeaderbaordNew.this,searchModel.DataClass.userLevelsData);
                    rv_search.setAdapter(searchResultAdapter);
                }
                else
                {
                    Constants.SnakeMessageYellow(parent_layout,searchModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<SearchModel> call, @NonNull Throwable t)
            {
                t.printStackTrace();
            }
        });
    }

    private void setupViewPager(ViewPager viewPager)
    {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new FragmentTodayLeaderboard("Today",rv_search), "Today");
        adapter.addFrag(new FragmentByWeekLeaderboard("Weekly",rv_search), "Weekly");
        adapter.addFrag(new FragmentMonthlyLeaderboard("Monthly",rv_search), "Monthly");
        adapter.addFrag(new FragmentAllTimeLeaderboard("All Time",rv_search), "All Time");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter
    {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager)
        {
            super(manager);
        }

        @Override
        public Fragment getItem(int position)
        {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }


        //adding fragments and title method
        public void addFrag(Fragment fragment, String title)
        {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBackPressed()
    {
        if(isSearch)
        {
            isSearch=false;
            tabLayout.setVisibility(View.VISIBLE);
            viewPager.setVisibility(View.VISIBLE);
            lyt_search.setVisibility(View.GONE);
            lyt_search_et.setVisibility(View.GONE);
            img_back.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_back));
            img_search.setVisibility(View.VISIBLE);
        }
        else
        {
            finish();
        }
    }
}
