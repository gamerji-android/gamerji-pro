package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.esports.gamerjipro.Adapter.PoolPriceAdapter;
import com.esports.gamerjipro.Adapter.WinnerPoolAdapter;
import com.esports.gamerjipro.Fragment.CRContestTabFragment;
import com.esports.gamerjipro.Fragment.PubgContestTabFragment;
import com.esports.gamerjipro.Interface.WinnerClickListener;
import com.esports.gamerjipro.Model.ContestTypeModelNew;
import com.esports.gamerjipro.Model.ContestTypesModel;
import com.esports.gamerjipro.Model.MyContestModel;
import com.esports.gamerjipro.Model.TournamentDetailModel;
import com.esports.gamerjipro.Model.TournamentTypeModel;
import com.esports.gamerjipro.Model.WinnerContestModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.AppConstants;
import com.esports.gamerjipro.Utils.Constants;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityMyContest  extends AppCompatActivity implements WinnerClickListener
{
    APIInterface apiInterface;
    TextView txt_title;
    ImageView img_back;
    private  ViewPager viewPager;
    private  TabLayout tabLayout;
    BottomSheetBehavior pool_price_bottomsheet;
    View bg;
    public LinearLayout pool_price_bottom;

    RecyclerView rv_pool_price;
    PoolPriceAdapter poolPriceAdapter;
    TextView tv_pool_price,txt_winning_rupee,txt_pool_price;
    public CoordinatorLayout parent_layout;
    WinnerPoolAdapter winnerPoolAdapter ;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_my_contest);


        viewPager = findViewById(R.id.viewPager);
        tv_pool_price = findViewById(R.id.tv_pool_price);
        txt_winning_rupee = findViewById(R.id.txt_winning_rupee);
        txt_pool_price = findViewById(R.id.txt_pool_price);
        parent_layout = findViewById(R.id.parent_layout);
        img_back = findViewById(R.id.img_back);
        txt_title = findViewById(R.id.txt_title);
        tabLayout = findViewById(R.id.tabLayout);




        setupViewPager(viewPager);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabTextColors(ColorStateList.valueOf(Color.WHITE));
        tabLayout.setSelectedTabIndicatorHeight(10);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.orange_color));

        /*img_notification.setOnClickListener(v -> {
            Intent i = new Intent(ActivityMyContest.this,NotificationActivity.class);
            startActivity(i);
        });*/

        int wantedTabIndex = 0;
        Typeface font = null;
        font = ResourcesCompat.getFont(ActivityMyContest.this,R.font.poppins_semibold);
        TextView tv = (TextView)(((LinearLayout)((LinearLayout)tabLayout.getChildAt(0)).getChildAt(wantedTabIndex)).getChildAt(1));
        tv.setTypeface(font);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener()
        {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
                ViewGroup vgTab = (ViewGroup) vg.getChildAt(tab.getPosition());
                int tabChildsCount = vgTab.getChildCount();
                for (int i = 0; i < tabChildsCount; i++)
                {
                    View tabViewChild = vgTab.getChildAt(i);
                    if (tabViewChild instanceof TextView)
                    {
                        Typeface font = null;
                        font = ResourcesCompat.getFont(ActivityMyContest.this,R.font.poppins_semibold);
                        ((TextView) tabViewChild).setTypeface(font);
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab)
            {
                ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
                ViewGroup vgTab = (ViewGroup) vg.getChildAt(tab.getPosition());
                int tabChildsCount = vgTab.getChildCount();
                for (int i = 0; i < tabChildsCount; i++)
                {
                    View tabViewChild = vgTab.getChildAt(i);
                    if (tabViewChild instanceof TextView)
                    {
                        Typeface font = null;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                        {
                            font = getResources().getFont(R.font.poppins);
                            ((TextView) tabViewChild).setTypeface(font);
                        }
                    }
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab)
            {

            }
        });
        rv_pool_price=findViewById(R.id.rv_pool_price);
        bg=findViewById(R.id.bg);
        img_back.setOnClickListener(v -> finish());
        pool_price_bottom=findViewById(R.id.pool_price_bottom);

        pool_price_bottomsheet = BottomSheetBehavior.from(pool_price_bottom);

        pool_price_bottomsheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback()
        {
            @Override
            public void onStateChanged(@NonNull View view, int newState)
            {
                switch (newState)
                {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                    {
                        bg.setVisibility(View.VISIBLE);
                    }
                    break;

                    case BottomSheetBehavior.STATE_COLLAPSED:
                    {
                        bg.setVisibility(View.GONE);
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:

                    case BottomSheetBehavior.STATE_SETTLING: {
                        break;
                    }
                }

            }

            @Override
            public void onSlide(@NonNull View view, float v)
            {

            }
        });



    }

    private void setupViewPager(ViewPager viewPager)
    {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new PubgContestTabFragment(AppConstants.GAME_P_MOBILE), AppConstants.GAME_P_MOBILE);
        adapter.addFrag(new CRContestTabFragment(), AppConstants.GAME_CLASH_ROYALE);
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onWinnersClick(ContestTypesModel.Data.TypesData.ContestsData contestsData)
    {


    }

    @Override
    public void onWinnersClick(ContestTypeModelNew.Data.ContestsData contestsData) {

    }

    private void getWinnerPool(MyContestModel.Data.ContestsData contestsData)
    {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityMyContest.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<WinnerContestModel> contestDetailsModelCall = apiInterface.getContestWinner(Constants.GETSINGLECONTESTWINNER,contestsData.ContestID);

        contestDetailsModelCall.enqueue(new Callback<WinnerContestModel>()
        {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<WinnerContestModel> call, @NonNull Response<WinnerContestModel> response)
            {
                progressDialog.dismiss();
                WinnerContestModel winnerContestModel = response.body();
                assert winnerContestModel != null;
                if (winnerContestModel.status.equalsIgnoreCase("success"))
                {
                    winnerPoolAdapter= new WinnerPoolAdapter(ActivityMyContest.this,winnerContestModel.data.prizePoolsData);
                    rv_pool_price.setAdapter(winnerPoolAdapter);
                    pool_price_bottomsheet.setState(BottomSheetBehavior.STATE_EXPANDED);
                    bg.setVisibility(View.VISIBLE);
                }
                else
                {
                    Constants.SnakeMessageYellow(parent_layout,winnerContestModel.message);
                }
            }
            @Override
            public void onFailure(@NonNull Call<WinnerContestModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onMyContestWinnerClick(MyContestModel.Data.ContestsData contestsData)
    {
        tv_pool_price.setText(contestsData.WinningAmount);
        if (contestsData.WinningAmount.equalsIgnoreCase("") || contestsData.WinningAmount.equalsIgnoreCase("0"))
        {
            txt_pool_price.setVisibility(View.GONE);
            txt_winning_rupee.setVisibility(View.GONE);
        }
        else
        {
            txt_pool_price.setVisibility(View.VISIBLE);
            txt_winning_rupee.setVisibility(View.VISIBLE);
        }
        getWinnerPool(contestsData);
    }

    @Override
    public void onTournamentWinnerClick(TournamentTypeModel.Data.TypesData.TournamentsData tournamentsData) {

    }

    @Override
    public void onTournamentContestWinnersClickListener(TournamentDetailModel.TournamentData.ContestsData contestsData) {

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();//fragment arraylist
        private final List<String> mFragmentTitleList = new ArrayList<>();//title arraylist

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }


        //adding fragments and title method
        public void addFrag(Fragment fragment, String title)
        {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
