package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.esports.gamerjipro.Model.AccountDetailsModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.FileUtils;
import com.esports.gamerjipro.Utils.Pref;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import cz.msebera.android.httpclient.Header;

public class ActivityUploadBankDetails extends AppCompatActivity implements View.OnClickListener, IPickResult
{
    CardView card_bank_upload,card_submit_bank;
    EditText et_acc_number,et_acc_name,et_bank_name,et_branch,et_ifsc;
    PickResult pickResult ;
    ImageView img_back,img_upload_hint,img_cancel;
    RelativeLayout root_verify_pan;
    TextView txt_mobile,txt_verified_status;
    String bank_verified;
    LinearLayout nested_view;
    TextView txt_bank;
    CardView card_bank_verified;
    AccountDetailsModel.Data.BankData bankData ;
    TextView txt_submit_for_verification;
    ImageView img_submit;
    RelativeLayout lyt_rejected;
    TextView txt_reject_title,txt_reject_reason,txt_account_number;
    CardView cv_ifsc,cv_branch_name,cv_bank_name,cv_acc_name,cv_acc_number;
    TextView img_news;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        bank_verified=getIntent().getStringExtra("bank_verified");
        bankData= (AccountDetailsModel.Data.BankData) getIntent().getSerializableExtra("bankdata");
        setContentView(R.layout.activity_bank_details);

        img_news=findViewById(R.id.img_news);
        img_news.setOnClickListener(v -> FileUtils.OpenNewsWebview(this,"https://www.gamerji.com"));

        card_bank_upload=findViewById(R.id.card_bank_upload);
        card_bank_verified=findViewById(R.id.card_bank_verified);
        img_submit=findViewById(R.id.img_submit);
        card_submit_bank=findViewById(R.id.card_submit_bank);
        txt_submit_for_verification=findViewById(R.id.txt_submit_for_verification);
        txt_verified_status=findViewById(R.id.txt_verified_status);
        nested_view=findViewById(R.id.nested_view);
        txt_account_number=findViewById(R.id.txt_account_number);
        txt_bank=findViewById(R.id.txt_bank);
        et_acc_number=findViewById(R.id.et_acc_number);
        root_verify_pan=findViewById(R.id.root_verify_pan);
        et_acc_name=findViewById(R.id.et_acc_name);
        et_bank_name=findViewById(R.id.et_bank_name);
        et_branch=findViewById(R.id.et_branch);
        img_back=findViewById(R.id.img_back);
        et_ifsc=findViewById(R.id.et_ifsc);
        img_cancel=findViewById(R.id.img_cancel);
        img_upload_hint=findViewById(R.id.img_upload_hint);
        txt_mobile=findViewById(R.id.txt_mobile);
        lyt_rejected=findViewById(R.id.lyt_rejected);
        txt_reject_title=findViewById(R.id.txt_reject_title);
        txt_reject_reason=findViewById(R.id.txt_reject_reason);

        cv_ifsc=findViewById(R.id.cv_ifsc);
        cv_acc_number=findViewById(R.id.cv_acc_number);
        cv_acc_name=findViewById(R.id.cv_acc_name);
        cv_bank_name=findViewById(R.id.cv_bank_name);
        cv_branch_name=findViewById(R.id.cv_branch_name);

        if (bank_verified.equalsIgnoreCase("2"))
        {
            txt_account_number.setText("Bank account no. : "+bankData.BankAccountNumber);
            txt_verified_status.setText("Bank account is verified.");
            card_bank_verified.setVisibility(View.VISIBLE);
            txt_verified_status.setVisibility(View.VISIBLE);
            txt_bank.setVisibility(View.GONE);
            nested_view.setVisibility(View.GONE);
            card_bank_upload.setVisibility(View.GONE);
            card_submit_bank.setVisibility(View.GONE);
        }
        else if (bank_verified.equalsIgnoreCase("3"))
        {
            card_bank_verified.setVisibility(View.VISIBLE);
            txt_account_number.setText("Bank account no. : "+bankData.BankAccountNumber);
            txt_verified_status.setText("Bank account verification is pending.");
            txt_verified_status.setVisibility(View.VISIBLE);
            card_bank_upload.setVisibility(View.GONE);
            et_acc_number.setText(bankData.BankAccountNumber);
            card_submit_bank.setVisibility(View.GONE);
            et_acc_name.setText(bankData.BankAccountName);
            et_branch.setText(bankData.BankBranch);
            et_ifsc.setText(bankData.BankIFSC);
            et_bank_name.setText(bankData.BankName);
            disableEdittext();
        }
        else if (bank_verified.equalsIgnoreCase("4"))
        {
            et_acc_number.setText(bankData.BankAccountNumber);
            et_acc_name.setText(bankData.BankAccountName);
            et_branch.setText(bankData.BankBranch);
            et_ifsc.setText(bankData.BankIFSC);
            et_bank_name.setText(bankData.BankName);
            txt_account_number.setText("Bank account verification is rejected.");
            txt_verified_status.setText(bankData.BankRejectedReason);
        }

        card_bank_upload.setOnClickListener(this);
        card_submit_bank.setOnClickListener(this);
        img_back.setOnClickListener(this);
        img_cancel.setOnClickListener(this);

        cv_acc_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                FileUtils.requestfoucs(ActivityUploadBankDetails.this,et_acc_number);

            }
        });

        cv_acc_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                FileUtils.requestfoucs(ActivityUploadBankDetails.this,et_acc_name);

            }
        });

        cv_bank_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                FileUtils.requestfoucs(ActivityUploadBankDetails.this,et_bank_name);

            }
        });

        cv_branch_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                FileUtils.requestfoucs(ActivityUploadBankDetails.this,et_branch);

            }
        });

        cv_ifsc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                FileUtils.requestfoucs(ActivityUploadBankDetails.this,et_ifsc);
            }
        });
    }

    private void enableEdittext()
    {
        et_acc_number.setEnabled(true);
        et_acc_name.setEnabled(true);
        et_bank_name.setEnabled(true);
        et_branch.setEnabled(true);
        et_ifsc.setEnabled(true);
    }

    private void disableEdittext()
    {
        et_acc_number.setEnabled(false);
        et_acc_name.setEnabled(false);
        et_bank_name.setEnabled(false);
        et_branch.setEnabled(false);
        et_ifsc.setEnabled(false);
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.card_bank_upload:
            {
                PickImageDialog.build(new PickSetup()).show(this);
                break;
            }

            case R.id.card_submit_bank:
            {
                validate();
                break;
            }
            case R.id.img_back:
            {
               finish();
                break;
            }
            case R.id.img_cancel:
            {
                pickResult=null;
                txt_mobile.setText(getResources().getString(R.string.upload_bank_account_proof));
                img_upload_hint.setVisibility(View.VISIBLE);
                img_cancel.setVisibility(View.GONE);
                break;
            }

        }

    }

    private void validate()
    {
        if (pickResult==null)
        {
            Constants.SnakeMessageYellow(root_verify_pan,"Please upload your bank account proof." );
        }
        else if (et_acc_name.getText().length()<3)
            et_acc_name.setError("Enter valid account name.");

        else if (et_acc_number.getText().length()<8)
            et_acc_number.setError("Enter valid account number.");

        else if(et_bank_name.getText().length()<3)
        {
            et_bank_name.setError("Enter bank name.");
        }
        else if (et_branch.getText().length()<4)
            et_branch.setError("Enter valid branch name.");

        else if(et_ifsc.getText().length()<6)
        {
            et_ifsc.setError("Enter valid IFSC code.");
        }
        else
        {
            try
            {
                sendToServer();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    private void sendToServer() throws IOException
    {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        File file = new File(pickResult.getPath());
        OutputStream os = null;
        try
        {
            os = new BufferedOutputStream(new FileOutputStream(file));
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        pickResult.getBitmap().compress(Bitmap.CompressFormat.JPEG, 100, os);
        assert os != null;
        os.close();



        RequestParams requestParams  = new RequestParams();
        try
        {
            requestParams.put("Action", Constants.UPDATE_BANK);
            requestParams.put("Val_Userid", Pref.getValue(this, Constants.UserID,"", Constants.FILENAME));
            requestParams.put("Val_UIbankaccountnumber",et_acc_number.getText().toString());
            requestParams.put("Val_UIbankaccountname",et_acc_name.getText().toString());
            requestParams.put("Val_UIbankname", et_bank_name.getText().toString());
            requestParams.put("Val_UIbankbranch", et_branch.getText().toString());
            requestParams.put("Val_UIbankifsc", et_ifsc.getText().toString());
            requestParams.put("Val_UIbankimage", file);

            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();

            asyncHttpClient.post(this,Constants.BASE_API+Constants.UPDATE_PROFILE,requestParams,new JsonHttpResponseHandler()
            {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response)
                {
                    super.onSuccess(statusCode, headers, response);
                    if (response.optString("status").equalsIgnoreCase("success"))
                        Constants.SnakeMessageYellow(root_verify_pan, response.optString("message"));
                    txt_submit_for_verification.setText("Verification pending");
                   /* img_submit.setVisibility(View.GONE);
                    img_submit.setVisibility(View.GONE);*/
                    disableEdittext();
                    card_bank_upload.setVisibility(View.GONE);
                    card_bank_verified.setVisibility(View.VISIBLE);
                    card_submit_bank.setEnabled(false);
                    finish();
                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse)
                {
                    progressDialog.dismiss();
                    Constants.SnakeMessageYellow(root_verify_pan, "Something went wrong.Please try again.");
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse)
                {
                    progressDialog.dismiss();
                    Constants.SnakeMessageYellow(root_verify_pan, "Something went wrong.Please try again.");
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable)
                {
                    progressDialog.dismiss();
                    Constants.SnakeMessageYellow(root_verify_pan, "Something went wrong.Please try again.");
                    super.onFailure(statusCode, headers, responseString, throwable);
                }
            });
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            Constants.SnakeMessageYellow(root_verify_pan, "Something went wrong.Please try again.");
        }

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onPickResult(PickResult pickResult)
    {
        if (pickResult.getError() == null)
        {
            this.pickResult=pickResult;
            txt_mobile.setText("Image Attached");
            //img_upload_hint.setVisibility(View.GONE);
            img_cancel.setVisibility(View.VISIBLE);

        }
        else
        {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Constants.SnakeMessageYellow(root_verify_pan, pickResult.getError().getMessage());
        }
    }
}
