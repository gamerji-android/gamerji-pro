package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esports.gamerjipro.Adapter.GameProfileAdapter;
import com.esports.gamerjipro.Adapter.MyBadgesAdapter;
import com.esports.gamerjipro.Model.GamerProfileModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.Pref;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityViewPlayerProfile extends AppCompatActivity implements View.OnClickListener
{
    SeekBar pb_level;
    LinearLayout lyt_progress_text,lyt_personal_profile,lyt_game_profile;
    TextView txt_gamer,txt_personal,txt_date_birth,txt_save,et_states,txt_end_lvl,txt_start_lvl;
    APIInterface apiInterface;
    TextView sp_states;
    EditText /*et_cr_name,et_pubg_name,*/et_team_name,et_email,et_phone,et_lname,et_fname;
    ImageView profile_image,img_close;
    RelativeLayout mRelativeEditProfileRootView;
    boolean editable=false;
    RecyclerView rv_badges;
    MyBadgesAdapter myBadgesAdapter ;
    ImageView img_back,img_pubg_bg,img_cr_bg,info_team/*,info_pubg,info_cr*/;
    BottomSheetBehavior sheetBehavior;
    RelativeLayout bottom_badge;
    TextView txt_pubg_id,txt_cr_id,txt_total_points,txt_pubg_played,txt_pubg_won,txt_pubg_points,txt_cr_played,txt_cr_won,txt_cr_points,txt_team_name,txt_level_number,txt_end_points,txt_current_points;
    View bg;
    ImageView level_image,mImgEditImage;
    TextView txt_level_name,txt_title,txt_bottom_msg,txt_title_bs;
    CoordinatorLayout parent_layout;
    public   ImageView img_badge;
    public  TextView txt_badge_msg,txt_points;
    public BottomSheetBehavior badgesheet;
    TextView txt_level_numnber;
    ImageView img_close_badges,front_arrow,back_arrow;
    LinearLayoutManager badgesLinearLayout;
    String Val_Viewerid;
    RecyclerView rv_game_profile;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_user_profile);
        Val_Viewerid=getIntent().getStringExtra("player_id");
        rv_game_profile=findViewById(R.id.rv_game_profile);
        sp_states = findViewById(R.id.sp_states);
        img_badge = findViewById(R.id.img_badge);
        front_arrow = findViewById(R.id.front_arrow);
        back_arrow = findViewById(R.id.back_arrow);
        bg = findViewById(R.id.bg);
        img_close_badges = findViewById(R.id.img_close_badges);
        txt_title_bs = findViewById(R.id.txt_title_bs);
        txt_badge_msg = findViewById(R.id.txt_badge_msg);
        badgesLinearLayout= new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        parent_layout = findViewById(R.id.parent_layout);
        txt_end_points = findViewById(R.id.txt_end_points);
        txt_current_points = findViewById(R.id.txt_current_points);
        txt_team_name = findViewById(R.id.txt_team_name);
        pb_level = findViewById(R.id.pb_level);
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(v -> finish());
        txt_save = findViewById(R.id.txt_save);
        profile_image = findViewById(R.id.profile_image);
        img_close = findViewById(R.id.img_close);
        lyt_game_profile = findViewById(R.id.lyt_game_profile);
        lyt_personal_profile = findViewById(R.id.lyt_personal_profile);
        txt_personal = findViewById(R.id.txt_personal);
        txt_gamer = findViewById(R.id.txt_gamer);
        et_states = findViewById(R.id.et_states);
        txt_pubg_id = findViewById(R.id.txt_pubg_id);
        txt_cr_id = findViewById(R.id.txt_cr_id);
        txt_total_points = findViewById(R.id.txt_total_points);
        txt_end_lvl = findViewById(R.id.txt_end_lvl);
        txt_start_lvl = findViewById(R.id.txt_start_lvl);
        lyt_progress_text = findViewById(R.id.lyt_progress_text);
       /* et_cr_name = findViewById(R.id.et_cr_name);
        et_pubg_name = findViewById(R.id.et_pubg_name);*/
        et_team_name = findViewById(R.id.et_team_name);
        et_email = findViewById(R.id.et_email);
        mImgEditImage = findViewById(R.id.mImgEditImage);
        et_phone = findViewById(R.id.et_phone);
        bg = findViewById(R.id.bg);
        et_lname = findViewById(R.id.et_lname);
        et_fname = findViewById(R.id.et_fname);

        info_team = findViewById(R.id.info_team);
      //  info_pubg = findViewById(R.id.info_pubg);
        txt_bottom_msg = findViewById(R.id.txt_bottom_msg);
        txt_title = findViewById(R.id.txt_title);
      //  info_cr = findViewById(R.id.info_cr);
        txt_level_name = findViewById(R.id.txt_level_name);
        level_image = findViewById(R.id.level_image);
        txt_level_number = findViewById(R.id.txt_level_number);
        txt_level_numnber = findViewById(R.id.txt_level_numnber);

        txt_date_birth = findViewById(R.id.txt_date_birth);
        rv_badges = findViewById(R.id.rv_badges);


        txt_total_points = findViewById(R.id.txt_total_points);
        txt_pubg_won = findViewById(R.id.txt_pubg_won);
        txt_pubg_played = findViewById(R.id.txt_pubg_played);
        txt_pubg_points = findViewById(R.id.txt_pubg_points);
       /* txt_cr_played = findViewById(R.id.txt_cr_played);
        txt_cr_won = findViewById(R.id.txt_cr_won);
        txt_cr_points = findViewById(R.id.txt_cr_points);*/

        rv_game_profile.setLayoutManager(new GridLayoutManager(this,2));


        back_arrow.setOnClickListener(this);
        front_arrow.setOnClickListener(this);


        bottom_badge = findViewById(R.id.bottom_badge);
        badgesheet = BottomSheetBehavior.from(bottom_badge);
        txt_points=bottom_badge.findViewById(R.id.txt_points);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        getUserProfile();

        badgesheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback()
        {
            @Override
            public void onStateChanged(@NonNull View view, int i)
            {
                switch (i)
                {
                    case BottomSheetBehavior.STATE_HIDDEN:
                    case BottomSheetBehavior.STATE_COLLAPSED:
                    {
                        bg.setVisibility(View.GONE);
                        break;
                    }

                    case BottomSheetBehavior.STATE_EXPANDED:
                    {
                        bg.setVisibility(View.VISIBLE);
                        break;
                    }
                    case BottomSheetBehavior.STATE_DRAGGING:
                    {
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v)
            {

            }
        });
    }

    private void getUserProfile()
    {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityViewPlayerProfile.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<GamerProfileModel> gamerProfileModelCall = apiInterface.viewUserProfile(Constants.GET_GAMER_PROFILE,Val_Viewerid, Pref.getValue(ActivityViewPlayerProfile.this,Constants.UserID,"", Constants.FILENAME));

        gamerProfileModelCall.enqueue(new Callback<GamerProfileModel>()
        {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<GamerProfileModel> call, @NonNull Response<GamerProfileModel> response)
            {
                progressDialog.dismiss();
                GamerProfileModel  gamerProfileModel = response.body();
                assert gamerProfileModel != null;
                if (gamerProfileModel.status.equalsIgnoreCase("success"))
                {
                    txt_team_name.setText(gamerProfileModel.DataClass.TeamName);
                    txt_title.setText(gamerProfileModel.DataClass.TeamName);
                    /*txt_pubg_id.setText(gamerProfileModel.DataClass.gamesData.gamesDataArrayList.get(0).UniqueName);
                    txt_cr_id.setText(gamerProfileModel.DataClass.gamesData.gamesDataArrayList.get(1).UniqueName);*/
                    txt_total_points.setText(gamerProfileModel.DataClass.TotalPoints);
                    /*txt_pubg_played.setText(gamerProfileModel.DataClass.gamesData.gamesDataArrayList.get(0).GamePlayed);
                    txt_pubg_won.setText(gamerProfileModel.DataClass.gamesData.gamesDataArrayList.get(0).GameWon);
                    txt_pubg_points.setText(gamerProfileModel.DataClass.gamesData.gamesDataArrayList.get(0).GamePoints);
                    txt_cr_played.setText(gamerProfileModel.DataClass.gamesData.gamesDataArrayList.get(1).GamePlayed);
                    txt_cr_won.setText(gamerProfileModel.DataClass.gamesData.gamesDataArrayList.get(1).GameWon);
                    txt_cr_points.setText(gamerProfileModel.DataClass.gamesData.gamesDataArrayList.get(1).GamePoints);*/
                    Glide.with(ActivityViewPlayerProfile.this).load(gamerProfileModel.DataClass.LevelIcon).into(level_image);
                    txt_level_name.setText(gamerProfileModel.DataClass.LevelName);
                    txt_level_number.setText(gamerProfileModel.DataClass.LevelNumber);
                    txt_level_numnber.setText(gamerProfileModel.DataClass.CurrentLevelPoints+" Points");
                    setBadges(gamerProfileModel.DataClass.LevelsData.levelsDataArrayList);

                    GameProfileAdapter gameProfileAdapter = new GameProfileAdapter(ActivityViewPlayerProfile.this,
                            gamerProfileModel.DataClass.gamesData.gamesDataArrayList);
                    rv_game_profile.setAdapter(gameProfileAdapter);

                    /*pb_level.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        int progressChanged = Integer.parseInt(gamerProfileModel.DataClass.StartLevelPoints);

                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
                        {
                            progressChanged = Integer.parseInt(gamerProfileModel.DataClass.StartLevelPoints)+ progress;
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {

                        }
                    });*/
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                    {
                        pb_level.setMin(Integer.parseInt(gamerProfileModel.DataClass.StartLevelPoints));
                    }

                    pb_level.setMax(Integer.parseInt(gamerProfileModel.DataClass.EndLevelPoints));
                    pb_level.setProgress(Integer.parseInt(gamerProfileModel.DataClass.CurrentLevelPoints));
                    txt_start_lvl.setText(gamerProfileModel.DataClass.StartLevelNumber);
                    txt_end_lvl.setText(gamerProfileModel.DataClass.EndLevelNumber);
                    txt_current_points.setText("Points:"+gamerProfileModel.DataClass.StartLevelPoints);
                    txt_end_points.setText("Points:"+gamerProfileModel.DataClass.EndLevelPoints);

                    /*Glide.with(ActivityProfile.this).load(gamerProfileModel.DataClass.gamesData.gamesDataArrayList.get(1).GameFeaturedImage).into(img_cr_bg);*/
                }
                else
                {
                    Constants.SnakeMessageYellow(parent_layout,gamerProfileModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<GamerProfileModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }

        });
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.img_close_badges:
            {
                break;
            }
        }

    }

    private void setBadges(ArrayList<GamerProfileModel.Data.LevelsData.LevelData> levelsDataArrayList)
    {
        rv_badges.setLayoutManager(badgesLinearLayout);
        rv_badges.setItemAnimator(new DefaultItemAnimator());
        myBadgesAdapter = new MyBadgesAdapter(this,levelsDataArrayList);
        rv_badges.setAdapter(myBadgesAdapter);

    }
}
