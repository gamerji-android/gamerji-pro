package com.esports.gamerjipro.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.esports.gamerjipro.Model.AccountDetailsModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.FileUtils;
import com.esports.gamerjipro.Utils.Pref;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Objects;

import cz.msebera.android.httpclient.Header;

public class ActivityVerifyBankDetails extends AppCompatActivity implements View.OnClickListener
{
    CardView card_bank_upload,card_submit_bank;
    EditText et_acc_number,et_acc_name,et_reenter_acc_no,et_bank_name,et_ifsc;
    ImageView img_back,img_upload_hint,img_cancel;
    RelativeLayout root_verify_pan;
    TextView txt_mobile,txt_verified_status,txt_repeat_acc_number_title,txt_account_number_title;
    String bank_verified,upi_verified;
    LinearLayout nested_view;
    TextView txt_bank;
    CardView card_bank_verified;
    AccountDetailsModel.Data.BankData bankData ;
    AccountDetailsModel.Data.UPIData upiData ;
    TextView txt_submit_for_verification;
    ImageView img_submit;
    RelativeLayout lyt_rejected;
    TextView txt_reject_title,txt_reject_reason,txt_account_number,txt_title;
    CardView cv_ifsc,cv_branch_name,cv_bank_name,cv_acc_name,cv_acc_number;
    TextView img_news;
    String type;
    boolean account_link=false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(getIntent().hasExtra("upi_verified"))
        {
            type=getIntent().getStringExtra(Constants.type);
            upi_verified=getIntent().getStringExtra("upi_verified");
            upiData= (AccountDetailsModel.Data.UPIData) getIntent().getSerializableExtra("upidata");
        }
        else
        {
            type=getIntent().getStringExtra(Constants.type);
            bank_verified=getIntent().getStringExtra("bank_verified");
            bankData= (AccountDetailsModel.Data.BankData) getIntent().getSerializableExtra("bankdata");
        }

        setContentView(R.layout.activity_bank_details);

        img_news=findViewById(R.id.img_news);
        img_news.setOnClickListener(v -> FileUtils.OpenNewsWebview(this,"https://www.gamerji.com"));

        card_bank_upload=findViewById(R.id.card_bank_upload);
        card_bank_verified=findViewById(R.id.card_bank_verified);
        img_submit=findViewById(R.id.img_submit);
        card_submit_bank=findViewById(R.id.card_submit_bank);
        txt_submit_for_verification=findViewById(R.id.txt_submit_for_verification);

        txt_verified_status=findViewById(R.id.txt_verified_status);
        nested_view=findViewById(R.id.nested_view);
        txt_account_number=findViewById(R.id.txt_account_number);
        txt_bank=findViewById(R.id.txt_bank);
        et_acc_number=findViewById(R.id.et_acc_number);
        root_verify_pan=findViewById(R.id.root_verify_pan);
        et_acc_name=findViewById(R.id.et_acc_name);
        et_reenter_acc_no=findViewById(R.id.et_bank_name);
        et_bank_name=findViewById(R.id.et_branch);
        img_back=findViewById(R.id.img_back);
        et_ifsc=findViewById(R.id.et_ifsc);
        img_cancel=findViewById(R.id.img_cancel);
        img_upload_hint=findViewById(R.id.img_upload_hint);
        txt_mobile=findViewById(R.id.txt_mobile);
        lyt_rejected=findViewById(R.id.lyt_rejected);
        txt_reject_title=findViewById(R.id.txt_reject_title);
        txt_reject_reason=findViewById(R.id.txt_reject_reason);
        txt_account_number_title=findViewById(R.id.txt_account_number_title);
        txt_repeat_acc_number_title=findViewById(R.id.txt_repeat_acc_number_title);
        txt_title=findViewById(R.id.txt_title);

        cv_ifsc=findViewById(R.id.cv_ifsc);
        cv_acc_number=findViewById(R.id.cv_acc_number);
        cv_acc_name=findViewById(R.id.cv_acc_name);
        cv_bank_name=findViewById(R.id.cv_bank_name);
        cv_branch_name=findViewById(R.id.cv_branch_name);

        if(getIntent().getStringExtra(Constants.type).equals("upi"))
        {
            txt_submit_for_verification.setText("Link UPI");
            txt_repeat_acc_number_title.setText("Repeat UPI ID");
            txt_repeat_acc_number_title.setHint("Repeat UPI ID");

            txt_account_number.setHint("UPI ID");
            txt_account_number_title.setText("UPI ID");

            et_acc_number.setInputType(InputType.TYPE_CLASS_TEXT);
            et_reenter_acc_no.setInputType(InputType.TYPE_CLASS_TEXT);
            cv_ifsc.setVisibility(View.GONE);
            txt_title.setText("UPI Details");

            if (upi_verified.equalsIgnoreCase("2"))
            {
                txt_account_number.setText("UPI id : "+upiData.UPIID);
                txt_verified_status.setText("UPI is verified.");
                card_bank_verified.setVisibility(View.VISIBLE);
                txt_verified_status.setVisibility(View.VISIBLE);
                txt_bank.setVisibility(View.GONE);
                nested_view.setVisibility(View.GONE);
                card_bank_upload.setVisibility(View.GONE);
                card_submit_bank.setVisibility(View.GONE);
            }
            else if (upi_verified.equalsIgnoreCase("3"))
            {
                card_bank_verified.setVisibility(View.VISIBLE);
                txt_account_number.setText("UPI id : "+upiData.UPIID);
                txt_verified_status.setText("UPI verification is pending.");
                txt_verified_status.setVisibility(View.VISIBLE);
                card_bank_upload.setVisibility(View.GONE);
                et_acc_number.setText(bankData.BankAccountNumber);
                card_submit_bank.setVisibility(View.GONE);
                et_acc_name.setText(bankData.BankAccountName);
                et_bank_name.setText(bankData.BankBranch);
                et_reenter_acc_no.setText(bankData.BankName);
                disableEdittext();
            }
            else if (upi_verified.equalsIgnoreCase("4"))
            {
                et_acc_number.setText(upiData.UPIID);
                et_acc_name.setText(upiData.UPIAccountName);
                et_bank_name.setText(upiData.BankName);
                et_reenter_acc_no.setText(upiData.UPIID);
                txt_account_number.setText("UPI verification is rejected.");
                txt_verified_status.setText(bankData.BankRejectedReason);
                txt_verified_status.setVisibility(View.VISIBLE);
            }
        }
        else
        {
            txt_submit_for_verification.setText("Link Bank Account");

            et_acc_number.setInputType(InputType.TYPE_CLASS_PHONE);
            et_reenter_acc_no.setInputType(InputType.TYPE_CLASS_PHONE);

            if (bank_verified.equalsIgnoreCase("2"))
            {
                txt_account_number.setText("Bank account no. : "+bankData.BankAccountNumber);
                txt_verified_status.setText("Bank account is verified.");
                card_bank_verified.setVisibility(View.VISIBLE);
                txt_verified_status.setVisibility(View.VISIBLE);
                txt_bank.setVisibility(View.GONE);
                nested_view.setVisibility(View.GONE);
                card_bank_upload.setVisibility(View.GONE);
                card_submit_bank.setVisibility(View.GONE);
            }
            else if (bank_verified.equalsIgnoreCase("3"))
            {
                card_bank_verified.setVisibility(View.VISIBLE);
                txt_account_number.setText("Bank account no. : "+bankData.BankAccountNumber);
                txt_verified_status.setText("Bank account verification is pending.");
                txt_verified_status.setVisibility(View.VISIBLE);
                card_bank_upload.setVisibility(View.GONE);
                et_acc_number.setText(bankData.BankAccountNumber);
                card_submit_bank.setVisibility(View.GONE);
                et_acc_name.setText(bankData.BankAccountName);
                et_bank_name.setText(bankData.BankBranch);
                et_ifsc.setText(bankData.BankIFSC);
                et_reenter_acc_no.setText(bankData.BankName);
                disableEdittext();
            }
            else if (bank_verified.equalsIgnoreCase("4"))
            {
                et_acc_number.setText(bankData.BankAccountNumber);
                et_acc_name.setText(bankData.BankAccountName);
                et_bank_name.setText(bankData.BankBranch);
                et_ifsc.setText(bankData.BankIFSC);
                et_reenter_acc_no.setText(bankData.BankName);
                txt_account_number.setText("Bank account verification is rejected.");
                txt_verified_status.setText(bankData.BankRejectedReason);
                txt_verified_status.setVisibility(View.VISIBLE);
            }
        }


        card_bank_upload.setVisibility(View.GONE);
        card_submit_bank.setOnClickListener(this);
        img_cancel.setOnClickListener(this);

        cv_acc_number.setOnClickListener(v -> FileUtils.requestfoucs(ActivityVerifyBankDetails.this,et_acc_number));

        cv_acc_name.setOnClickListener(v -> FileUtils.requestfoucs(ActivityVerifyBankDetails.this,et_acc_name));

        cv_bank_name.setOnClickListener(v -> FileUtils.requestfoucs(ActivityVerifyBankDetails.this,et_reenter_acc_no));

        cv_branch_name.setOnClickListener(v -> FileUtils.requestfoucs(ActivityVerifyBankDetails.this,et_bank_name));

        cv_ifsc.setOnClickListener(v -> FileUtils.requestfoucs(ActivityVerifyBankDetails.this,et_ifsc));

        img_back.setOnClickListener(view ->
        {
            Intent intent=new Intent();
            setResult(Constants.ACCOUNT_LINK_CANCEL,intent);
            finish();
        });
    }

    private void enableEdittext()
    {
        et_acc_number.setEnabled(true);
        et_acc_name.setEnabled(true);
        et_reenter_acc_no.setEnabled(true);
        et_bank_name.setEnabled(true);
        et_ifsc.setEnabled(true);
    }

    private void disableEdittext()
    {
        et_acc_number.setEnabled(false);
        et_acc_name.setEnabled(false);
        et_reenter_acc_no.setEnabled(false);
        et_bank_name.setEnabled(false);
        et_ifsc.setEnabled(false);
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.card_submit_bank:
            {
                if (type.equals("upi"))
                validateUpi();
                else
                    validateBank();
                break;
            }
            case R.id.img_back:
            {
                finish();
                break;
            }
        }

    }

    private void validateUpi()
    {

        if (!et_acc_number.getText().toString().matches(Constants.UPI_REGEX))
            et_acc_number.setError("Enter valid UPI ID");
        else if (!et_reenter_acc_no.getText().toString().equals(et_acc_number.getText().toString()))
            et_reenter_acc_no.setError("UPI ID doesn't match.");
        else if (et_bank_name.getText().length()<3)
            et_bank_name.setError("Enter valid branch name.");
        else if (et_acc_name.getText().length()<3)
            et_acc_name.setError("Enter valid account name.");
        else
        {
            sendUpiToServer();
        }
    }

    private void sendUpiToServer()
    {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        RequestParams requestParams  = new RequestParams();
        requestParams.put("Action", Constants.LINK_UPI);
        requestParams.put("Val_Userid", Pref.getValue(this, Constants.UserID,"", Constants.FILENAME));
        requestParams.put("Val_UPIid",et_acc_number.getText().toString());
        requestParams.put("Val_Accountname",et_acc_name.getText().toString());
        requestParams.put("Val_Bankname", et_bank_name.getText().toString());

        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        asyncHttpClient.post(this,Constants.BASE_API+Constants.UPDATE_PROFILE_V3,requestParams,new JsonHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response)
            {
                super.onSuccess(statusCode, headers, response);
                progressDialog.dismiss();
                if (response.optString("status").equalsIgnoreCase("success"))
                {
                    Constants.SnakeMessageYellow(root_verify_pan, response.optString("message"));
                    Intent intent=new Intent();
                    intent.putExtra("response", Objects.requireNonNull(response.optJSONObject("data")).toString());
                    setResult(Constants.UPI_LINK,intent);
                    finish();
                }
                else
                {
                    Constants.SnakeMessageYellow(root_verify_pan, response.optString("message"));
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse)
            {
                progressDialog.dismiss();
                Constants.SnakeMessageYellow(root_verify_pan, "Something went wrong.Please try again.");
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse)
            {
                progressDialog.dismiss();
                Constants.SnakeMessageYellow(root_verify_pan, "Something went wrong.Please try again.");
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable)
            {
                progressDialog.dismiss();
                Constants.SnakeMessageYellow(root_verify_pan, "Something went wrong.Please try again.");
                super.onFailure(statusCode, headers, responseString, throwable);
            }
        });

    }

    private void validateBank()
    {

        if (et_acc_number.getText().length()<8)
            et_acc_number.setError("Enter valid account number.");
        else if (!et_reenter_acc_no.getText().toString().equals(et_acc_number.getText().toString()))
            et_reenter_acc_no.setError("Account Number doesn't match.");
        else if (et_bank_name.getText().length()<3)
            et_bank_name.setError("Enter valid branch name.");
        else if(et_ifsc.getText().length()<6)
            et_ifsc.setError("Enter valid IFSC code.");
        else if (et_acc_name.getText().length()<3)
            et_acc_name.setError("Enter valid account name.");
        else
        {
            sendBankToServer();
        }
    }

    private void sendBankToServer()
    {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        RequestParams requestParams  = new RequestParams();
        requestParams.put("Action", Constants.LINK_BANK);
        requestParams.put("Val_Userid", Pref.getValue(this, Constants.UserID,"", Constants.FILENAME));
        requestParams.put("Val_Accountnumber",et_acc_number.getText().toString());
        requestParams.put("Val_Accountname",et_acc_name.getText().toString());
        requestParams.put("Val_Bankname", et_bank_name.getText().toString());
        requestParams.put("Val_IFSCcode", et_ifsc.getText().toString());

        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();

        asyncHttpClient.post(this,Constants.BASE_API+Constants.UPDATE_PROFILE_V3,requestParams,new JsonHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response)
            {
                progressDialog.dismiss();
                if (response.optString("status").equalsIgnoreCase("success"))
                {
                    Constants.SnakeMessageYellow(root_verify_pan, response.optString("message"));
                    Intent intent=new Intent();
                    intent.putExtra("response", Objects.requireNonNull(response.optJSONObject("data")).toString());
                    setResult(Constants.ACCOUNT_LINK,intent);
                    finish();
                }
                else
                {
                    Constants.SnakeMessageYellow(root_verify_pan, response.optString("message"));
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse)
            {
                progressDialog.dismiss();
                Constants.SnakeMessageYellow(root_verify_pan, "Something went wrong.Please try again.");
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse)
            {
                progressDialog.dismiss();
                Constants.SnakeMessageYellow(root_verify_pan, "Something went wrong.Please try again.");
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable)
            {
                progressDialog.dismiss();
                Constants.SnakeMessageYellow(root_verify_pan, "Something went wrong.Please try again.");
                super.onFailure(statusCode, headers, responseString, throwable);
            }
        });

    }

    @Override
    public void onBackPressed()
    {
        Intent intent=new Intent();
        setResult(Constants.ACCOUNT_LINK_CANCEL,intent);
        finish();
        super.onBackPressed();
    }
}
