package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.esports.gamerjipro.Model.AccountDetailsModel;
import com.esports.gamerjipro.Model.GeneralResponseModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.FileUtils;
import com.esports.gamerjipro.Utils.Pref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityWithdraw extends AppCompatActivity
{
    TextView tv_wallet_balance,txt_bank_name,txt_acc_number,txt_owner_name,txt_link_upi,txt_link_bank,txt_acc_upi,txt_amazon_no,txt_paytm_no,txt_info;
    EditText et_amount;
    CardView cv_withdraw,cv_amount;
    RelativeLayout lyt_parent;
    APIInterface apiInterface;
    AccountDetailsModel.Data.BankData bankData ;
    AccountDetailsModel.Data.UPIData upiData ;
    ImageView img_back;
    String bank_verified="",upi_verified="",transfer_type="";
    String min_amount="";
    String max_amount="";
    RadioButton rb_bank,rb_upi,rb_amazon,rb_paytm;
    LinearLayout lyt_bank_details;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw);

        if (getIntent().getExtras()!=null)
        {
            bankData= (AccountDetailsModel.Data.BankData) getIntent().getSerializableExtra("bankdata");
            bank_verified=getIntent().getStringExtra("bank_verified");
            upi_verified=getIntent().getStringExtra(Constants.UPI_Verified);
            upiData=(AccountDetailsModel.Data.UPIData)getIntent().getSerializableExtra(Constants.UPI_DATA);
            min_amount=getIntent().getStringExtra(Constants.MIN_WITHDRAW);
            max_amount=getIntent().getStringExtra(Constants.MAX_WITHDRAW);
        }


        tv_wallet_balance=findViewById(R.id.tv_wallet_balance);
        txt_owner_name=findViewById(R.id.txt_owner_name);
        txt_bank_name=findViewById(R.id.txt_bank_name);
        txt_acc_number=findViewById(R.id.txt_acc_number);
        lyt_parent=findViewById(R.id.lyt_parent);
        img_back=findViewById(R.id.img_back);
        et_amount=findViewById(R.id.et_amount);
        txt_link_upi=findViewById(R.id.txt_link_upi);
        txt_link_bank=findViewById(R.id.txt_link_bank);
        cv_amount=findViewById(R.id.cv_amount);
        rb_bank=findViewById(R.id.rb_bank);
        rb_upi=findViewById(R.id.rb_upi);
        rb_amazon=findViewById(R.id.rb_amazon);
        rb_paytm=findViewById(R.id.rb_paytm);
        txt_acc_upi=findViewById(R.id.txt_acc_upi);
        lyt_bank_details=findViewById(R.id.lyt_bank_details);
        txt_amazon_no=findViewById(R.id.txt_amazon_no);
        txt_paytm_no=findViewById(R.id.txt_paytm_no);
        txt_info=findViewById(R.id.txt_info);
        tv_wallet_balance.setText(getIntent().getStringExtra("amount"));
        txt_paytm_no.setText("Mobile No:"+Pref.getValue(this,Constants.MobileNumber,"",Constants.FILENAME));
        txt_amazon_no.setText("Mobile No:"+Pref.getValue(this,Constants.MobileNumber,"",Constants.FILENAME));
        txt_info.setText("Minimum Rs. "+min_amount+" and Maximum Rs. "+max_amount+"\nallowed per day");

        apiInterface = APIClient.getClient().create(APIInterface.class);
        img_back.setOnClickListener(v -> finish());
        cv_withdraw=findViewById(R.id.cv_withdraw);
        cv_withdraw.setOnClickListener(v ->
        {

            if (et_amount.getText().toString().isEmpty())
            {
                Constants.SnakeMessageYellow(lyt_parent,"Enter amount to proceed.");
            }
            else
            {
                if(Float.parseFloat(et_amount.getText().toString())>Float.parseFloat(Objects.requireNonNull(getIntent().getStringExtra("amount")).trim()))
                {
                    Constants.SnakeMessageYellow(lyt_parent,getResources().getString(R.string.withdraw_msg));
                }
                else if (Float.parseFloat(et_amount.getText().toString())<Integer.parseInt(min_amount))
                {
                    Constants.SnakeMessageYellow(lyt_parent,"Minimum withdrawal amount in"+ getResources().getString(R.string.rupee)+min_amount);
                }
                else if (Float.parseFloat(et_amount.getText().toString())>Integer.parseInt(max_amount))
                {
                    Constants.SnakeMessageYellow(lyt_parent,"Maximum withdrawal amount is"+ getResources().getString(R.string.rupee)+max_amount);
                }
                else if (transfer_type.isEmpty())
                {
                    Constants.SnakeMessageYellow(lyt_parent,getResources().getString(R.string.transfer_type));
                }
                else
                {
                    withdrawRequest();
                }
            }
        });

        txt_link_upi.setOnClickListener(v ->
        {
            Intent i = new Intent(ActivityWithdraw.this,ActivityVerifyBankDetails.class);
            i.putExtra(Constants.type, "upi");
            i.putExtra(Constants.UPI_Verified, upi_verified);
            i.putExtra(Constants.UPI_DATA, upiData);
            startActivityForResult(i, Constants.LINK_ACCOUNTS);
        });
        txt_link_bank.setOnClickListener(v ->
        {
            Intent i = new Intent(ActivityWithdraw.this,ActivityVerifyBankDetails.class);
            i.putExtra("bankdata", bankData);
            i.putExtra("bank_verified", bank_verified);
            i.putExtra(Constants.type, "bank");
            startActivityForResult(i, Constants.LINK_ACCOUNTS);
        });

        rb_upi.setOnClickListener(v ->
        {
            rb_bank.setChecked(false);
            rb_paytm.setChecked(false);
            rb_amazon.setChecked(false);
            transfer_type="2";

        });

        rb_bank.setOnClickListener(v ->
        {
            rb_upi.setChecked(false);
            rb_paytm.setChecked(false);
            rb_amazon.setChecked(false);
            transfer_type="1";

        });

        rb_paytm.setOnClickListener(view ->
        {
            rb_amazon.setChecked(false);
            rb_bank.setChecked(false);
            rb_upi.setChecked(false);
            transfer_type="3";
        });

        rb_amazon.setOnClickListener(view ->
        {
            rb_paytm.setChecked(false);
            rb_bank.setChecked(false);
            rb_upi.setChecked(false);
            transfer_type="4";
        });


        setDetails();

        cv_amount.setOnClickListener(v -> FileUtils.requestfoucs(ActivityWithdraw.this,et_amount));
    }

    private void setDetails()
    {
        txt_bank_name.setText("Bank Name :"+bankData.BankName);
        txt_acc_number.setText("A/C : "+bankData.BankAccountNumber);
        txt_owner_name.setText("Name : "+bankData.BankAccountName);

        if (bank_verified.equals("2"))
        {
            txt_link_bank.setText("Linked");
            txt_link_bank.setTextColor(getResources().getColor(R.color.orange_color));
            rb_bank.setVisibility(View.VISIBLE);
            rb_bank.setChecked(true);
            transfer_type="1";
            lyt_bank_details.setVisibility(View.VISIBLE);
        }
        else if (bank_verified.equals("3"))
        {
            txt_link_bank.setText("PENDING");
            txt_link_bank.setTextColor(getResources().getColor(R.color.orange_color));
            lyt_bank_details.setVisibility(View.VISIBLE);
        }
        else if(bank_verified.equals("4"))
        {
            txt_link_bank.setText("REJECTED");
            txt_link_bank.setTextColor(Color.RED);
        }

        if (upi_verified.equals("2"))
        {
            txt_link_upi.setText("Linked");
            txt_link_upi.setTextColor(getResources().getColor(R.color.orange_color));
            rb_upi.setVisibility(View.VISIBLE);
            txt_acc_upi.setVisibility(View.VISIBLE);
            txt_acc_upi.setText(upiData.UPIID);
            if (rb_bank.getVisibility()!=View.VISIBLE)
            {
                rb_upi.setChecked(true);
                transfer_type="2";
            }

            if(rb_bank.isChecked())
                rb_upi.setChecked(false);
        }
        else if (upi_verified.equals("3"))
        {
            txt_link_upi.setText("PENDING");
            txt_link_upi.setTextColor(getResources().getColor(R.color.orange_color));
        }
        else if(upi_verified.equals("4"))
        {
            txt_link_upi.setText("REJECTED");
            txt_link_upi.setTextColor(Color.RED);
        }
    }

    private void withdrawRequest()
    {

        final ProgressDialog progressDialog = new ProgressDialog(ActivityWithdraw.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        Call<GeneralResponseModel> gameTypesModelCall = apiInterface.withdrawRequest(Constants.WITHDRAW_REQUEST,
                Pref.getValue(ActivityWithdraw.this, Constants.UserID,"", Constants.FILENAME),et_amount.getText().toString(),transfer_type);

        gameTypesModelCall.enqueue(new Callback<GeneralResponseModel>()
        {
            @Override
            public void onResponse(@NonNull Call<GeneralResponseModel> call, @NonNull Response<GeneralResponseModel> response)
            {
                progressDialog.dismiss();
                GeneralResponseModel accountDetailsModel = response.body();
                assert accountDetailsModel != null;
                if (accountDetailsModel.status.equalsIgnoreCase("success"))
                {

//                    Constants.SnakeMessageYellow(lyt_parent, accountDetailsModel.message);
                    final Dialog dialog = new Dialog(ActivityWithdraw.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.success_dialog);
                    Window window = dialog.getWindow();
                    assert window != null;
                    window.setLayout(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.show();
                    TextView mTextMessage = dialog.findViewById(R.id.txt_success_message);
                    mTextMessage.setText(accountDetailsModel.message);
                    RelativeLayout lyt_done = dialog.findViewById(R.id.lyt_done);
                    lyt_done.setOnClickListener(v ->
                    {
                        dialog.cancel();
                        finish();
                    });


                }
                else
                {
                    Constants.SnakeMessageYellow(lyt_parent, accountDetailsModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<GeneralResponseModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode== Constants.LINK_ACCOUNTS)
        {
            if(resultCode==Constants.UPI_LINK)
            {
                try
                {
                    JSONObject jsonObject = new JSONObject(Objects.requireNonNull(data.getStringExtra("response")));
                    upiData.UPIID=jsonObject.optString("UPIID");
                    upiData.BankName=jsonObject.optString("BankName");
                    upiData.UPIAccountName=jsonObject.optString("BankAccountName");
                    upi_verified=jsonObject.optString("UPIVerified");
                    rb_upi.setVisibility(View.VISIBLE);
                    transfer_type="2";
                    setDetails();
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
            else if(resultCode==Constants.ACCOUNT_LINK)
            {

                try
                {
                    JSONObject jsonObject = new JSONObject(Objects.requireNonNull(data.getStringExtra("response")));
                    bankData.BankAccountName=jsonObject.optString("BankAccountName");
                    bankData.BankName=jsonObject.optString("BankName");
                    bankData.BankAccountNumber=jsonObject.optString("BankAccountNumber");
                    bankData.BankIFSC=jsonObject.optString("BankIFSC");
                    bank_verified=jsonObject.optString("BankVerified");
                    transfer_type="1";
                    rb_bank.setVisibility(View.VISIBLE);
                    lyt_bank_details.setVisibility(View.VISIBLE);
                    setDetails();
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
}
