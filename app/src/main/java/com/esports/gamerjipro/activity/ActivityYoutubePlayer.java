package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esports.gamerjipro.Adapter.YoutubeVideoAdapter;
import com.esports.gamerjipro.Model.GetVideosModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Common;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.FileUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityYoutubePlayer extends AppCompatActivity {

    RecyclerView rv_youtube;
    YoutubeVideoAdapter youtubeVideoAdapter;
    ImageView img_back, img_logo;
    APIInterface apiInterface;
    RelativeLayout parent_layout;
    TextView txt_channel_name, txt_channel_sub;
    CardView cv_subscribe;
    TextView img_news;
    int page = 0;
    LinearLayoutManager mLayoutManager;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private boolean isLastPage = false, isLoading;
    ArrayList<GetVideosModel.VideoData.VideosData> videosDataArrayList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.youtube_player_layout);

        img_news = findViewById(R.id.img_news);
        img_news.setOnClickListener(v -> FileUtils.OpenNewsWebview(this, "https://www.gamerji.com"));
        rv_youtube = findViewById(R.id.rv_youtube);
        parent_layout = findViewById(R.id.parent_layout);
        img_back = findViewById(R.id.img_back);
        img_logo = findViewById(R.id.img_logo);
        txt_channel_name = findViewById(R.id.txt_channel_name);
        txt_channel_sub = findViewById(R.id.txt_channel_sub);
        cv_subscribe = findViewById(R.id.cv_subscribe);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv_youtube.setLayoutManager(mLayoutManager);
        rv_youtube.setItemAnimator(new DefaultItemAnimator());
        img_back.setOnClickListener(v -> finish());

        youtubeVideoAdapter = new YoutubeVideoAdapter(ActivityYoutubePlayer.this, videosDataArrayList);
        rv_youtube.setAdapter(youtubeVideoAdapter);
        getVideos(++page);
        rv_youtube.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (!isLastPage && !isLoading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            Common.insertLog("Last Item Wow !");
                            isLoading = true;
                            getVideos(++page);
                        }
                    }
                }
            }
        });
    }

    private void getVideos(int page) {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityYoutubePlayer.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        Call<GetVideosModel> getVideosModelCall = apiInterface.getVideos(Constants.GET_VIDEOS, page, 5);

        getVideosModelCall.enqueue(new Callback<GetVideosModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<GetVideosModel> call, @NonNull Response<GetVideosModel> response) {
                isLoading = false;
                progressDialog.dismiss();
                GetVideosModel getVideosModel = response.body();
                assert getVideosModel != null;
                if (getVideosModel.status.equalsIgnoreCase("success")) {
                    txt_channel_name.setText(getVideosModel.videoData.ChannelName);
                    txt_channel_sub.setText(getVideosModel.videoData.ChannelSubscribers + " subscribers");
                    Glide.with(ActivityYoutubePlayer.this).load(getVideosModel.videoData.ChannelLogo).into(img_logo);
                    isLastPage = getVideosModel.videoData.IsLast;
                    videosDataArrayList.addAll(mLayoutManager.getItemCount(), getVideosModel.videoData.videosDataArrayList);
                    youtubeVideoAdapter.notifyDataSetChanged();

                    cv_subscribe.setOnClickListener(v ->
                    {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getVideosModel.videoData.ChannelLink));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setPackage("com.google.android.youtube");
                        startActivity(intent);
                    });
                } else {
                    Constants.SnakeMessageYellow(parent_layout, getVideosModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetVideosModel> call, @NonNull Throwable t) {
                isLoading = false;
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }
}
