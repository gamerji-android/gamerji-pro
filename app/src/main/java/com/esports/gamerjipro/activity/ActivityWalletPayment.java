package com.esports.gamerjipro.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Adapter.BankListAdapter;
import com.esports.gamerjipro.Interface.OnBankClikcListener;
import com.esports.gamerjipro.Model.BankDetail;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.cashFree.ActivityPayment;

import java.util.ArrayList;

public class ActivityWalletPayment extends AppCompatActivity implements OnBankClikcListener
{
    ImageView img_back;
    RecyclerView cv_bank_list;
    BankListAdapter bankListAdapter ;
    TextView txt_title;
    ArrayList<BankDetail> bankDetailArrayList  = new ArrayList<>();
    String amount;
    RelativeLayout parent_layout;
    String game_id,game_type_id,game_name;
    TextView tv_wallet_balance;
    boolean is_tournament=false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        if (getIntent().getExtras()!=null);
        {
            game_id = getIntent().getStringExtra("game_id");
            game_type_id = getIntent().getStringExtra("game_type_id");
            game_name = getIntent().getStringExtra("game_name");
            is_tournament=getIntent().getBooleanExtra("is_tournament",false);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_net_banking);
        img_back=findViewById(R.id.img_back);
        tv_wallet_balance=findViewById(R.id.tv_wallet_balance);
        cv_bank_list=findViewById(R.id.cv_bank_list);
        parent_layout=findViewById(R.id.parent_layout);
        txt_title=findViewById(R.id.txt_title);
        tv_wallet_balance.setText(getIntent().getStringExtra("current_amount"));
        txt_title.setText("WALLET");
        amount=getIntent().getStringExtra("amount");
        bankListAdapter= new BankListAdapter(this,bankDetailArrayList, this);
        cv_bank_list.setAdapter(bankListAdapter);
        img_back.setOnClickListener(v -> finish());
        setAllWallets();
    }


    private void setAllWallets()
    {
        bankDetailArrayList.add(new BankDetail("4007", "Paytm"));
        bankDetailArrayList.add(new BankDetail("4009", "PhonePe"));
        bankDetailArrayList.add(new BankDetail("4008", "Amazon Pay"));
        bankDetailArrayList.add(new BankDetail("4001", "FreeCharge"));
        bankDetailArrayList.add(new BankDetail("4002", "MobiKwik"));
        bankDetailArrayList.add(new BankDetail("4003", "OLA Money"));
        bankDetailArrayList.add(new BankDetail("4004", "Reliance Jio Money"));
        bankDetailArrayList.add(new BankDetail("4006", "Airtel Money"));
        bankListAdapter.notifyDataSetChanged();
    }

    @Override
    public void OnBankItemClickListener(BankDetail bankDetail)
    {
        Intent i = new Intent(ActivityWalletPayment.this, ActivityPayment.class);
        i.putExtra("type", Constants.CASHFREE_WALLET);
        i.putExtra("bank_id",bankDetail.id);
        i.putExtra("amount",amount);
        i.putExtra("game_id",game_id);
        i.putExtra("game_type_id",game_type_id);
        i.putExtra("game_name",game_name);
        i.putExtra("is_tournament",is_tournament);
        startActivityForResult(i, Constants.PAYMENT_RESULT);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode== Constants.PAYMENT_RESULT)
        {
            assert data != null;
            if (data.getBooleanExtra(Constants.PAYMENT_STATUS_SUCCESS,true))
            {
                Intent i = new Intent(ActivityWalletPayment.this,ActivityWallet.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
            else
            {
                Constants.SnakeMessageYellow(parent_layout,"Payment failed .Please try again.");
            }

        }
    }
}
