package com.esports.gamerjipro.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.esports.gamerjipro.Model.GeneralResponseModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.FileUtils;
import com.esports.gamerjipro.Utils.Pref;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityReferral extends AppCompatActivity
{
    TextView txt_earn_text,txt_ref_code,txt_copy_code;
    CardView cv_whatsappd,cv_ref_frnd;
    APIInterface apiInterface;
    ImageView img_back;
    ScrollView parent_layout;
    String earn;
    TextView img_news;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_refer_friend);
        img_news=findViewById(R.id.img_news);
        img_news.setOnClickListener(v -> FileUtils.OpenNewsWebview(this,"https://www.gamerji.com"));
        txt_earn_text=findViewById(R.id.txt_earn_text);
        txt_ref_code=findViewById(R.id.txt_ref_code);
        parent_layout=findViewById(R.id.parent_layout);
        txt_copy_code=findViewById(R.id.txt_copy_code);
        cv_whatsappd=findViewById(R.id.cv_whatsappd);
        cv_ref_frnd=findViewById(R.id.cv_ref_frnd);
        img_back=findViewById(R.id.img_back);
        img_back.setOnClickListener(v -> finish());
        apiInterface = APIClient.getClient().create(APIInterface.class);
        getInfo();
        txt_copy_code.setOnClickListener(v -> FileUtils.setClipboard(ActivityReferral.this,txt_ref_code.getText().toString()));
        cv_whatsappd.setOnClickListener(v ->
        {
            Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
            whatsappIntent.setType("text/plain");
            whatsappIntent.setPackage("com.whatsapp");
            whatsappIntent.putExtra(Intent.EXTRA_TEXT, "Hey, download this fantastic app GamerJi  and get ₹ 100 bonus using my referral code. "+txt_ref_code.getText().toString()+" You can download it by visiting https://www.gamerji.com");
            try {
                startActivity(whatsappIntent);
            }
            catch (android.content.ActivityNotFoundException ex)
            {
                Constants.SnakeMessageYellow(parent_layout,"Whatsapp have not been installed.");
            }

        });

        cv_ref_frnd.setOnClickListener(v ->
        {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    "Hey, download this fantastic app GamerJi  and get ₹ "+earn+" bonus using my referral code "+txt_ref_code.getText().toString()+". You can download it by visiting https://www.gamerji.com");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        });
    }

    private void getInfo()
    {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<GeneralResponseModel> generalResponseModelCall;


        generalResponseModelCall = apiInterface.getUserRefferal(Constants.GET_REFERRAL, Pref.getValue(ActivityReferral.this, Constants.UserID,"", Constants.FILENAME));


        generalResponseModelCall.enqueue(new Callback<GeneralResponseModel>()
        {
            @Override
            public void onResponse(@NonNull Call<GeneralResponseModel> call, @NonNull Response<GeneralResponseModel> response)
            {
                progressDialog.dismiss();
                GeneralResponseModel generalResponseModel = response.body();
                assert generalResponseModel != null;
                if (generalResponseModel.status.equalsIgnoreCase("success"))
                {
                    earn=generalResponseModel.DataClass.InviteeBonus;
                    txt_earn_text.setText("Friend sign up, you earn\n₹"+generalResponseModel.DataClass.ReferralBonus+" & friends earns ₹ "+generalResponseModel.DataClass.InviteeBonus);
                    txt_ref_code.setText(generalResponseModel.DataClass.ReferralCode);
                }
                else
                {
                    Constants.SnakeMessageYellow(parent_layout,generalResponseModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<GeneralResponseModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }
}
