package com.esports.gamerjipro.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.esports.gamerjipro.Interface.SetOnDownloadProgressListner;
import com.esports.gamerjipro.Model.AppMaintenanceStatusModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.AnalyticsSocket;
import com.esports.gamerjipro.Utils.Common;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.FileUtils;
import com.esports.gamerjipro.Utils.Pref;
import com.esports.gamerjipro.adGyde.AdGydeConstants;
import com.esports.gamerjipro.adGyde.CommonEvents;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.gson.Gson;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.List;
import java.util.Set;

import cz.msebera.android.httpclient.Header;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    private RelativeLayout mRelativeMain, mRelativeSplashView;
    private AnalyticsSocket analyticsSocket;
    private LinearLayout mLinearMaintenanceView;
    private TextView mTextMaintenanceTitle, mTextMaintenanceMessage;

    @SuppressLint({"PackageManagerGetSignatures", "BatteryLife"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Constants.ChangeEnvironment(Constants.ENVIRONMENT);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        analyticsSocket = new AnalyticsSocket();

        CommonEvents.setSimpleEvent(AdGydeConstants.KEY_APP_LAUNCH);
        getIds();
        callToGetAppMaintenanceStatusAPI();
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Relative Layouts
            mRelativeMain = findViewById(R.id.parent_layout);
            mRelativeSplashView = findViewById(R.id.relative_splash_main_view);

            // Linear Layouts
            mLinearMaintenanceView = findViewById(R.id.linear_splash_maintenance_view);

            // Text Views
            mTextMaintenanceTitle = findViewById(R.id.relative_splash_maintenance_title);
            mTextMaintenanceMessage = findViewById(R.id.relative_splash_maintenance_message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This should get the App Maintenance Status
     */
    private void callToGetAppMaintenanceStatusAPI() {
        try {
//            final ProgressDialog progressDialog = new ProgressDialog(SplashActivity.this);
//            progressDialog.setMessage("Please wait...."); // Setting Message
//            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
//            progressDialog.show(); // Display Progress Dialog
//            progressDialog.setCancelable(false);

            Call<AppMaintenanceStatusModel> call = APIClient.getClient().create(APIInterface.class).getAppMaintenanceStatus();
            call.enqueue(new Callback<AppMaintenanceStatusModel>() {
                @Override
                public void onResponse(@NonNull Call<AppMaintenanceStatusModel> call, @NonNull Response<AppMaintenanceStatusModel> response) {

//                    progressDialog.dismiss();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        assert response.body() != null;
                        if (response.body().getStatus().equalsIgnoreCase(Constants.API_SUCCESS)) {
                            mRelativeSplashView.setVisibility(View.GONE);
                            mLinearMaintenanceView.setVisibility(View.VISIBLE);
                            mTextMaintenanceTitle.setText(response.body().getData().getTitle());
                            mTextMaintenanceMessage.setText(response.body().getData().getSubTitle());
                        } else {
                            mRelativeSplashView.setVisibility(View.VISIBLE);
                            mLinearMaintenanceView.setVisibility(View.GONE);
                            checkAppVersion();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AppMaintenanceStatusModel> call, @NonNull Throwable t) {
//                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void checkAppVersion() {
        Constants.Apptype = Constants.APK;
        RequestParams requestParams = new RequestParams();

        requestParams.put("Action", Constants.APP_VERSION);

        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();

        asyncHttpClient.post(this, Constants.BASE_API + Constants.GENERAL_FETCH, requestParams, new JsonHttpResponseHandler() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                Common.insertLog("Response:::> " + response.toString());
                if (response.optString("status").equalsIgnoreCase("success")) {
                    JSONObject data = response.optJSONObject("data");
                    JSONArray jsonArray = data.optJSONArray("VersionsData");
                    JSONObject versionData = null;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            versionData = jsonArray.getJSONObject(i);
                            if (versionData.optString("OSType").equalsIgnoreCase("android")) {
                                String version = "";
                                try {
                                    PackageInfo info = SplashActivity.this.getPackageManager().getPackageInfo(SplashActivity.this.getPackageName(), PackageManager.GET_ACTIVITIES);
                                    version = info.versionName;
                                } catch (PackageManager.NameNotFoundException e) {
                                    e.printStackTrace();
                                }

                                if (!versionData.opt("Version").equals(version)) {
                                    boolean forceful;
                                    if (Integer.parseInt(versionData.optString("ForcefullyUpdate")) == 0)
                                        forceful = true;
                                    else
                                        forceful = false;
                                    showUpdateAlert(versionData.optString("DownloadLink"), versionData.optString("NewUpdateDetail"), forceful, versionData.optString("DownloadLink"));
                                } else {
                                    TedPermission.with(SplashActivity.this)
                                            .setPermissionListener(permissionlistener)
                                            .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                                            .setPermissions(Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS)
                                            .check();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            //jsonObject =jsonArray.optJSONObject(1);
                        }
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                TedPermission.with(SplashActivity.this)
                        .setPermissionListener(permissionlistener)
                        .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                        .setPermissions(Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS)
                        .check();
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                TedPermission.with(SplashActivity.this)
                        .setPermissionListener(permissionlistener)
                        .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                        .setPermissions(Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS)
                        .check();
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                TedPermission.with(SplashActivity.this)
                        .setPermissionListener(permissionlistener)
                        .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                        .setPermissions(Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS)
                        .check();
                super.onFailure(statusCode, headers, responseString, throwable);
            }
        });
    }

    ProgressBar pb_download_dialog;
    TextView txt_progress_update;
    Dialog mDownloadDialog;

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void showUpdateAlert(String mUrl, String mNewUpdateDetails, boolean hasCancel, String downloadLink) {
        Dialog dialog = new Dialog(SplashActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.app_update_dialog);
        // checkFirebaseDynamicLinks();
        dialog.setOnCancelListener(DialogInterface::dismiss);
        dialog.setCancelable(hasCancel);
        dialog.setCanceledOnTouchOutside(hasCancel);
        ImageView mImgBackUpdateDialog = dialog.findViewById(R.id.mImgBackUpdateDialog);
        TextView mTxtNewUpdateDetails = dialog.findViewById(R.id.mTxtNewUpdateDetails);
        mTxtNewUpdateDetails.setText(mNewUpdateDetails);
        if (hasCancel) {
            mImgBackUpdateDialog.setVisibility(View.VISIBLE);
        } else {
            mImgBackUpdateDialog.setVisibility(View.GONE);
        }

        mImgBackUpdateDialog.setOnClickListener(v ->
        {
            dialog.dismiss();
            TedPermission.with(SplashActivity.this)
                    .setPermissionListener(permissionlistener)
                    .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                    .setPermissions(Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS)
                    .check();
            //checkFirebaseDynamicLinks();
        });

        if (dialog.getWindow() != null)
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        CardView card_View_update_now = dialog.findViewById(R.id.card_View_update_now);

        if (downloadLink.equalsIgnoreCase("")) {
            card_View_update_now.setVisibility(View.GONE);
        } else {
            card_View_update_now.setVisibility(View.VISIBLE);
        }

        card_View_update_now.setOnClickListener(v ->
        {
            //startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("https://play.google.com/store/apps/details?id=com.esports.gamerjipro&hl=en")));

            TedPermission.with(SplashActivity.this)
                    .setPermissionListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted() {
                            dialog.dismiss();
                            File mDirectory = new File(Environment.getExternalStorageDirectory() + "/Download");

                            if (!mDirectory.exists()) {
                                if (!mDirectory.mkdir())
                                    return;
                            }
                            File mOutputPath = new File(mDirectory, "gamerji.apk");
                            if (mOutputPath.exists())
                                mOutputPath.delete();

                            new FileUtils.DownloadAPK(mUrl, mOutputPath.getAbsolutePath(), new SetOnDownloadProgressListner() {
                                @Override
                                public void OnDownloadStarted() {
                                    DownloadDialog();
                                }

                                @Override
                                public void OnProgressUpdate(Integer mValue) {

                                    if (pb_download_dialog != null && txt_progress_update != null) {
                                        pb_download_dialog.setProgress(mValue);
                                        txt_progress_update.setText("" + mValue + " %");
                                    }
                                }

                                @Override
                                public void OnDownloadComplete(String mOutPutPath) {
                                    if (mDownloadDialog != null && mDownloadDialog.isShowing())
                                        mDownloadDialog.dismiss();
                                    startActivity(new Intent(DownloadManager.ACTION_VIEW_DOWNLOADS));
                                    FileUtils.InstallAPK(getApplicationContext(), mOutPutPath);
                                    finish();
                                }

                                @Override
                                public void OnErrorOccure(String mMessage) {
                                    if (mDownloadDialog != null && mDownloadDialog.isShowing())
                                        mDownloadDialog.dismiss();
                                    Common.insertLog("Error:::> " +mMessage);
                                    Toast.makeText(SplashActivity.this, "" + mMessage, Toast.LENGTH_SHORT).show();
                                }
                            }).execute();
                        }

                        @Override
                        public void onPermissionDenied(List<String> deniedPermissions) {

                        }
                    })
                    .setDeniedMessage("To Download the update you must have to allow us a permission")
                    .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                    .check();
        });
        dialog.show();

        dialog.setOnCancelListener(dialog1 ->

        {
            TedPermission.with(SplashActivity.this)
                    .setPermissionListener(permissionlistener)
                    .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                    .setPermissions(Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS)
                    .check();
            checkFirebaseDynamicLinks();
        });
    }

    void DownloadDialog() {
        mDownloadDialog = new Dialog(SplashActivity.this);
        mDownloadDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDownloadDialog.setContentView(R.layout.download_progress_dialog);
        mDownloadDialog.setCanceledOnTouchOutside(false);
        mDownloadDialog.setCancelable(false);
        if (mDownloadDialog.getWindow() != null)
            mDownloadDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        txt_progress_update = mDownloadDialog.findViewById(R.id.txt_progress_update);
        pb_download_dialog = mDownloadDialog.findViewById(R.id.pb_download_dialog);
        pb_download_dialog.setMax(100);
        mDownloadDialog.show();
    }


    PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            checkFirebaseDynamicLinks();
        }

        @Override
        public void onPermissionDenied(List<String> deniedPermissions) {
            Constants.SnakeMessageYellow(mRelativeMain, "Permission Denied\n" + deniedPermissions.toString());
            checkFirebaseDynamicLinks();
        }
    };


    void checkFirebaseDynamicLinks() {
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, pendingDynamicLinkData ->
                {
                    Uri deepLink = null;
                    String contest_id = null;
                    if (pendingDynamicLinkData != null) {
                        deepLink = pendingDynamicLinkData.getLink();
                        assert deepLink != null;
                        Set<String> key = deepLink.getQueryParameterNames();
                        contest_id = deepLink.getQueryParameter(key.iterator().next());
                    }
                    if (deepLink == null) {
                        getIntroScreen();
                    } else {
                        if (!Pref.getValue(SplashActivity.this, Constants.IS_LOGIN, false, Constants.FILENAME)) {
                            try {
                                Constants.SnakeMessageYellow(mRelativeMain, "Please login to join a league");
                                getIntroScreen();
                            } catch (Exception e) {
                                Constants.SnakeMessageYellow(mRelativeMain, "Please login to join a league");
                                getIntroScreen();
                            }
                        } else {
                            Intent i = new Intent(SplashActivity.this, JoinContestViaShare.class);
                            i.putExtra("contest_id", contest_id);
                            i.putExtra("invite", true);
                            startActivity(i);
                            finish();
                        }
                    }
                })
                .addOnFailureListener(this, e -> getIntroScreen());
    }

    void getIntroScreen() {
        if (Pref.getValue(SplashActivity.this, Constants.IS_LOGIN, false, Constants.FILENAME)) {
            analyticsSocket.connect();
//            Intent mainIntent = new Intent(SplashActivity.this, ActivityMain.class);
            Intent mainIntent = new Intent(SplashActivity.this, BottomNavigationActivity.class);
            startActivity(mainIntent);
            finish();
        } else {
            Intent mainIntent = new Intent(SplashActivity.this, IntroActivity.class);
            startActivity(mainIntent);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
