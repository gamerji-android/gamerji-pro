package com.esports.gamerjipro.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.esports.gamerjipro.Model.GeneralResponseModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.FileUtils;
import com.esports.gamerjipro.Utils.Pref;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityPromoCode extends AppCompatActivity
{
    TextView txt_content_title,txt_title,txt_promo_title;
    EditText et_code;
    CardView proceed;
    APIInterface apiInterface;
    ImageView img_back;
    RelativeLayout parent_layout;
    CardView cv_promo;
    TextView img_news;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_code);
        img_news=findViewById(R.id.img_news);
        img_news.setOnClickListener(v -> FileUtils.OpenNewsWebview(this,"https://www.gamerji.com"));
        txt_title=findViewById(R.id.txt_title);
        txt_content_title=findViewById(R.id.txt_content_title);
        txt_promo_title=findViewById(R.id.txt_promo_title);
        parent_layout=findViewById(R.id.parent_layout);
        et_code=findViewById(R.id.et_code);
        proceed=findViewById(R.id.proceed);
        img_back=findViewById(R.id.img_back);
        cv_promo=findViewById(R.id.cv_promo);
        img_back.setOnClickListener(v -> finish());
        apiInterface = APIClient.getClient().create(APIInterface.class);
        txt_title.setText("PROMO CODE");
        txt_content_title.setText("Enter Promo Code.");
        proceed.setOnClickListener(v ->
        {
            if (et_code.getText().toString().isEmpty())
            {
                Constants.SnakeMessageYellow(parent_layout,"Please enter promo code.");
            }
           else if (et_code.getText().length()<4)
                Constants.SnakeMessageYellow(parent_layout,"Enter valid code.");
            else
                sendToServer();
        });
        txt_promo_title.setVisibility(View.VISIBLE);
        cv_promo.setOnClickListener(v -> {
            FileUtils.requestfoucs(ActivityPromoCode.this,et_code);
        });
    }

    private void sendToServer()
    {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<GeneralResponseModel> signInModelCall;


        signInModelCall = apiInterface.applyPromo(Constants.APPLY_PROMO, Pref.getValue(ActivityPromoCode.this,Constants.UserID,"", Constants.FILENAME),et_code.getText().toString());


        signInModelCall.enqueue(new Callback<GeneralResponseModel>()
        {
            @Override
            public void onResponse(@NonNull Call<GeneralResponseModel> call, @NonNull Response<GeneralResponseModel> response)
            {
                progressDialog.dismiss();
                GeneralResponseModel signInModel = response.body();
                assert signInModel != null;
                if (signInModel.status.equalsIgnoreCase("success"))
                {
                    Constants.SnakeMessageYellow(parent_layout,signInModel.message);
                }
                else
                {
                    Constants.SnakeMessageYellow(parent_layout,signInModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<GeneralResponseModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }
}
