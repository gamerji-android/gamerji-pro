package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.esports.gamerjipro.Adapter.ChatAdapter;
import com.esports.gamerjipro.Adapter.FeedbackAdapter;
import com.esports.gamerjipro.Adapter.FeedbackReasonAdapter;
import com.esports.gamerjipro.Adapter.TournamentContestAdapter;
import com.esports.gamerjipro.Adapter.TournamentPlayerListAdapter;
import com.esports.gamerjipro.Adapter.WinnerPoolAdapter;
import com.esports.gamerjipro.Interface.FeedbackListener;
import com.esports.gamerjipro.Interface.FeedbackReasonListener;
import com.esports.gamerjipro.Interface.ScreenshotClickListener;
import com.esports.gamerjipro.Interface.WinnerClickListener;
import com.esports.gamerjipro.Model.ChatModel;
import com.esports.gamerjipro.Model.ContestDetailsModel;
import com.esports.gamerjipro.Model.ContestTypeModelNew;
import com.esports.gamerjipro.Model.ContestTypesModel;
import com.esports.gamerjipro.Model.GeneralResponseModel;
import com.esports.gamerjipro.Model.MyContestModel;
import com.esports.gamerjipro.Model.TournamentDetailModel;
import com.esports.gamerjipro.Model.TournamentPlayersModel;
import com.esports.gamerjipro.Model.TournamentTypeModel;
import com.esports.gamerjipro.Model.WinnerContestModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.AppConstants;
import com.esports.gamerjipro.Utils.Common;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.FileUtils;
import com.esports.gamerjipro.Utils.Pref;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityTournamentDetail extends AppCompatActivity implements View.OnClickListener, IPickResult, FeedbackListener, FeedbackReasonListener, WinnerClickListener, ScreenshotClickListener {
    public RecyclerView rv_players, rv_chat, rv_pool_price, rv_reasons, rv_gif, rv_tournament_contests;
    TournamentPlayerListAdapter playerListAdapter;
    ChatAdapter chatAdapter;
    RelativeLayout img_ss;
    WinnerPoolAdapter winnerPoolAdapter;
    public TextView txt_rules, txt_ss_info, txt_ss, tv_pool_price;

    ProgressBar progress_bar;
    APIInterface apiInterface;
    String tournament_id, screenshot_contest_id;

    CoordinatorLayout parent_lyt;
    ArrayList<ChatModel> chatModelArrayList = new ArrayList<>();
    ArrayList<TournamentDetailModel.TournamentData.UsersData> usersDataArrayList = new ArrayList<>();
    String ContestUserID;
    TextView txt_date, txt_time, txt_map, txt_persp, winning_amount, txt_winners, txt_winner_title, txt_perkill, txt_entry_fees, txt_available_spots,
            txt_available_remaining, txt_join_contest, txt_room_id, txt_room_pass;

    LinearLayout pool_price_bottom, lyt_winners, lyt_entry_fees, lyt_winnings, lyt_kills, mLinearSeparator;
    RelativeLayout lyt_send_message, feedback_bottom;
    BottomSheetBehavior pool_price_bottomsheet, feedback_bottomsheet;
    ImageView img_back, img_close_feedback;
    View bg;
    EditText et_chat;
    MySocket mSocket;
    FrameLayout lyt_send;
    TextView txt_person_name;

    RelativeLayout txt_details, txt_players, txt_room_chat, txt_rules_tab;
    LinearLayout lyt_player, lyt_details;
    View view_detail, view_player, view_rules, view_chat, txt_slot_view;
    TextView txt_name, txt_acc_number, txt_kill, txt_rank, txt_winning_amount;
    ImageView img_pic;
    LinearLayout lyt_congratulartions;
    ImageView iv_close_pool_price, img_discord, img_youtube_sub, img_fb, img_insta, img_telegram;
    SwipeRefreshLayout swipeRefreshLayout, swipe_refresh_player;
    TextView txt_chat, txt_rule, txt_player, txt_detail, rupees_symbol, txt_entry_rupee, txt_pool_price, txt_winning_rupee;
    Typeface typeface_normal;
    Typeface typeface_semibold;
    String CaptainID, tournament_winning_amt;
    LinearLayoutManager linearLayoutManager;
    CardView cv_user, card_submit_number, cv_invite_contest;
    TextView txt_name_user, txt_acc_number_user, txt_rank_user, txt_kill_user, txt_winning_amount_user, txt_slots, txt_invite, txrupee_symbol, txt_per_kill_title;
    LinearLayout lyt_congratulartions_user;
    ImageView img_user, img_user_in_list;
    String screenShotUrl;
    FeedbackAdapter feedbackAdapter;
    FeedbackReasonAdapter feedbackReasonAdapter;
    ArrayList<String> feedback_reason_id = new ArrayList<>();
    EditText et_fb_other;
    String RatingID, discord_link, youtubre_link, fb_link, insta_link, telegram_link;
    TextView img_news, txt_title_type, txt_tournamnet_name, txt_live;

    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private boolean isLastPage = false, isLoading;
    LinearLayoutManager players_linearLayoutManager;
    int page = 1;

    @SuppressLint("SetTextI18n")

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_tournament_detail);
        img_news = findViewById(R.id.img_news);
        img_news.setOnClickListener(v -> FileUtils.OpenNewsWebview(ActivityTournamentDetail.this, "https://www.gamerji.com"));
        mSocket = new MySocket();
        tournament_id = getIntent().getStringExtra("contest_id");
        rv_players = findViewById(R.id.rv_players);
        et_fb_other = findViewById(R.id.et_fb_other);
        rv_reasons = findViewById(R.id.rv_reasons);
        txt_title_type = findViewById(R.id.txt_title_type);
        txt_live = findViewById(R.id.txt_live);
        cv_invite_contest = findViewById(R.id.cv_invite_contest);
        txt_invite = findViewById(R.id.txt_invite);
        rv_gif = findViewById(R.id.rv_gif);
        rv_tournament_contests = findViewById(R.id.rv_tournament_contests);
        card_submit_number = findViewById(R.id.card_submit_number);
        iv_close_pool_price = findViewById(R.id.iv_close_pool_price);
        img_youtube_sub = findViewById(R.id.img_youtube_sub);
        img_discord = findViewById(R.id.img_discord);
        img_fb = findViewById(R.id.img_fb);
        img_insta = findViewById(R.id.img_insta);
        img_telegram = findViewById(R.id.img_telegram);
        lyt_player = findViewById(R.id.lyt_player);
        txt_per_kill_title = findViewById(R.id.txt_per_kill_title);
        img_pic = findViewById(R.id.img_pic);
        txt_pool_price = findViewById(R.id.txt_pool_price);
        txt_winning_rupee = findViewById(R.id.txt_winning_rupee);
        txrupee_symbol = findViewById(R.id.txrupee_symbol);
        txt_players = findViewById(R.id.txt_players);
        txt_details = findViewById(R.id.txt_details);
        lyt_details = findViewById(R.id.lyt_details);
        txt_tournamnet_name = findViewById(R.id.txt_tournamnet_name);
        txt_rules_tab = findViewById(R.id.txt_rules_tab);
        txt_room_chat = findViewById(R.id.txt_room_chat);
        lyt_send_message = findViewById(R.id.lyt_send_message);
        view_detail = findViewById(R.id.view_detail);
        view_player = findViewById(R.id.view_player);
        view_rules = findViewById(R.id.view_rules);
        view_chat = findViewById(R.id.view_chat);
        txt_slot_view = findViewById(R.id.txt_slot_view);
        lyt_congratulartions = findViewById(R.id.lyt_congratulartions);
        txt_winning_amount = findViewById(R.id.txt_winning_amount);
        swipeRefreshLayout = findViewById(R.id.swipe_refresh);
        swipe_refresh_player = findViewById(R.id.swipe_refresh_player);
        txt_chat = findViewById(R.id.txt_chat);
        txt_rule = findViewById(R.id.txt_rule);
        txt_player = findViewById(R.id.txt_player);
        txt_detail = findViewById(R.id.txt_detail);
        img_user = findViewById(R.id.img_user);
        txt_person_name = findViewById(R.id.txt_person_name);

        img_user_in_list = findViewById(R.id.img_user_in_list);

        cv_user = findViewById(R.id.cv_user);
        txt_name_user = findViewById(R.id.txt_name_user);
        txt_acc_number_user = findViewById(R.id.txt_acc_number_user);
        txt_rank_user = findViewById(R.id.txt_rank_user);
        txt_kill_user = findViewById(R.id.txt_kill_user);
        lyt_congratulartions_user = findViewById(R.id.lyt_congratulartions_user);
        txt_slots = findViewById(R.id.txt_slots);
        txt_winning_amount_user = findViewById(R.id.txt_winning_amount_user);

        typeface_normal = ResourcesCompat.getFont(ActivityTournamentDetail.this, R.font.poppins);
        typeface_semibold = ResourcesCompat.getFont(ActivityTournamentDetail.this, R.font.poppins_semibold);


        lyt_send = findViewById(R.id.lyt_send);
        et_chat = findViewById(R.id.et_chat);
        rv_chat = findViewById(R.id.rv_chat);
        parent_lyt = findViewById(R.id.parent_lyt);
        rv_pool_price = findViewById(R.id.rv_pool_price);
        bg = findViewById(R.id.bg);

        img_ss = findViewById(R.id.img_ss);
        txt_ss_info = findViewById(R.id.txt_ss_info);
        txt_ss = findViewById(R.id.txt_ss);
        txt_rules = findViewById(R.id.txt_rules);
        progress_bar = findViewById(R.id.progress_bar);

        txt_room_id = findViewById(R.id.txt_room_id);
        tv_pool_price = findViewById(R.id.tv_pool_price);
        txt_room_pass = findViewById(R.id.txt_room_pass);


        img_back = findViewById(R.id.img_back);
        img_close_feedback = findViewById(R.id.img_close_feedback);

        rupees_symbol = findViewById(R.id.rupees_symbol);
        txt_entry_rupee = findViewById(R.id.txt_entry_rupee);
        img_back.setOnClickListener(v ->
        {
            if (getIntent().hasExtra("invite")) {
                if (getIntent().getBooleanExtra("invite", false)) {
//                    Intent i = new Intent(this,ActivityMain.class);
                    Intent i = new Intent(this, BottomNavigationActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    finish();
                } else {
                    finish();
                }
            } else {
                finish();
            }
        });
        img_close_feedback.setOnClickListener(v -> feedback_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED));

        txt_date = findViewById(R.id.txt_date);
        txt_time = findViewById(R.id.txt_time);
        txt_map = findViewById(R.id.txt_map);
        txt_persp = findViewById(R.id.txt_persp);
        winning_amount = findViewById(R.id.winning_amount);
        txt_winners = findViewById(R.id.txt_winners);
        txt_perkill = findViewById(R.id.txt_perkill);
        txt_entry_fees = findViewById(R.id.txt_entry_fees);
        txt_available_spots = findViewById(R.id.txt_available_spots);
        txt_available_remaining = findViewById(R.id.txt_available_remaining);
        txt_join_contest = findViewById(R.id.txt_join_contest);
        progress_bar = findViewById(R.id.progress_bar);
        txt_winner_title = findViewById(R.id.txt_winner_title);
        txt_rules.setMovementMethod(new ScrollingMovementMethod());
        pool_price_bottom = findViewById(R.id.pool_price_bottom);
        feedback_bottom = findViewById(R.id.feedback_layout);
        pool_price_bottomsheet = BottomSheetBehavior.from(pool_price_bottom);
        feedback_bottomsheet = BottomSheetBehavior.from(feedback_bottom);
        lyt_winners = findViewById(R.id.lyt_winners);
        lyt_entry_fees = findViewById(R.id.lyt_entry_fees);
        lyt_winnings = findViewById(R.id.lyt_winnings);
        lyt_kills = findViewById(R.id.lyt_kills);
        mLinearSeparator = findViewById(R.id.linear_tournament_details_separator);
        txt_name = findViewById(R.id.txt_name);
        txt_acc_number = findViewById(R.id.txt_acc_number);
        txt_kill = findViewById(R.id.txt_kill);
        txt_rank = findViewById(R.id.txt_rank);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        players_linearLayoutManager = new LinearLayoutManager(this);
        rv_players.setLayoutManager(players_linearLayoutManager);
        rv_players.setItemAnimator(new DefaultItemAnimator());

        rv_players.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                Common.insertLog("Last Item Wow !");
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = players_linearLayoutManager.getChildCount();
                    totalItemCount = players_linearLayoutManager.getItemCount();
                    pastVisiblesItems = players_linearLayoutManager.findFirstVisibleItemPosition();

                    if (!isLastPage && !isLoading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            Common.insertLog("Last Item Wow !");
                            isLoading = true;
                            getTournamentPlayers(++page);
                        }
                    }
                }
            }
        });

        rv_tournament_contests.setLayoutManager(new LinearLayoutManager(this));
        rv_tournament_contests.setItemAnimator(new DefaultItemAnimator());


        linearLayoutManager = new LinearLayoutManager(this);
        rv_chat.setLayoutManager(linearLayoutManager);
        rv_chat.setItemAnimator(new DefaultItemAnimator());

        rv_pool_price.setLayoutManager(new LinearLayoutManager(this));
        rv_pool_price.setItemAnimator(new DefaultItemAnimator());

        getData();
        txt_details.setOnClickListener(this);
        txt_room_chat.setOnClickListener(this);
        txt_rules_tab.setOnClickListener(this);
        lyt_details.setOnClickListener(this);
        txt_players.setOnClickListener(this);
        cv_invite_contest.setOnClickListener(this);
        card_submit_number.setOnClickListener(this);
        iv_close_pool_price.setOnClickListener(this);
        img_discord.setOnClickListener(this);
        img_youtube_sub.setOnClickListener(this);
        img_telegram.setOnClickListener(this);
        img_fb.setOnClickListener(this);
        img_insta.setOnClickListener(this);

        Glide.with(this).load(Pref.getValue(ActivityTournamentDetail.this, Constants.ProfileImage, "", Constants.FILENAME)).placeholder(getDrawable(R.drawable.logo)).into(img_pic);

        pool_price_bottomsheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                switch (i) {
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        bg.setVisibility(View.GONE);
                        break;
                    }
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        bg.setVisibility(View.VISIBLE);
                    }
                    break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });

        chatAdapter = new ChatAdapter(ActivityTournamentDetail.this, chatModelArrayList);
        rv_chat.setAdapter(chatAdapter);

        mSocket.connect();
        mSocket.on();
        mSocket.joinChatRoom();
        mSocket.requestallChatMessage();
        lyt_send.setOnClickListener(v ->
                {
                    if (!et_chat.getText().toString().trim().isEmpty())
                        mSocket.sendMessage(et_chat.getText().toString());
                    else
                        Constants.SnakeMessageYellow(parent_lyt, "Enter message to send");
                }
        );

        /*rv_chat.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
            if (bottom < oldBottom)
            {
                if(chatModelArrayList.size()>1)
                {
                    rv_chat.postDelayed(() -> rv_chat.smoothScrollToPosition(
                            Objects.requireNonNull(rv_chat.getAdapter()).getItemCount()), 100);
                }
            }
        });*/

        txt_person_name.setText(Pref.getValue(ActivityTournamentDetail.this, Constants.firstname, "", Constants.FILENAME) + " " +
                Pref.getValue(ActivityTournamentDetail.this, Constants.firstname, "", Constants.FILENAME));

        swipeRefreshLayout.setOnRefreshListener(this::getData);
        swipe_refresh_player.setOnRefreshListener(this::getData);

    }


    private void getTournamentPlayers(int page) {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityTournamentDetail.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<TournamentPlayersModel> tournamentDetailModelCall;

        tournamentDetailModelCall = apiInterface.getTournamentPlayers(Constants.GET_PLAYERS, Pref.getValue(ActivityTournamentDetail.this, Constants.UserID, "", Constants.FILENAME), tournament_id, page, 20);

        tournamentDetailModelCall.enqueue(new Callback<TournamentPlayersModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<TournamentPlayersModel> call, @NonNull Response<TournamentPlayersModel> response) {
                progressDialog.dismiss();
                isLoading = false;
                TournamentPlayersModel tournamentPlayersModel = response.body();
                feedback_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);

                assert tournamentPlayersModel != null;
                if (tournamentPlayersModel.status.equalsIgnoreCase("success")) {
                    for (int i = 0; i < tournamentPlayersModel.Data.userDataArrayList.size(); i++) {
                        if (CaptainID.equalsIgnoreCase(tournamentPlayersModel.Data.userDataArrayList.get(i).UserID)) {
                            tournamentPlayersModel.Data.userDataArrayList.remove(i);
                            break;
                        }
                    }

                    usersDataArrayList.addAll(players_linearLayoutManager.getItemCount(), tournamentPlayersModel.Data.userDataArrayList);
                    playerListAdapter.notifyDataSetChanged();
                    isLastPage = tournamentPlayersModel.Data.IsLast;
                } else
                    Constants.SnakeMessageYellow(parent_lyt, tournamentPlayersModel.message);

            }

            @Override
            public void onFailure(@NonNull Call<TournamentPlayersModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
                feedback_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (getIntent().hasExtra("invite")) {
            if (getIntent().getBooleanExtra("invite", false)) {
//                Intent i = new Intent(this,ActivityMain.class);
                Intent i = new Intent(this, BottomNavigationActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            } else {
                finish();
            }
        } else {
            finish();
        }

    }

    private void getWinnerPool(TournamentDetailModel.TournamentData.ContestsData contestsData) {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityTournamentDetail.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<WinnerContestModel> contestDetailsModelCall = apiInterface.getContestWinner(Constants.GETSINGLECONTESTWINNER, contestsData.ContestID);

        contestDetailsModelCall.enqueue(new Callback<WinnerContestModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<WinnerContestModel> call, @NonNull Response<WinnerContestModel> response) {
                progressDialog.dismiss();
                WinnerContestModel winnerContestModel = response.body();
                assert winnerContestModel != null;
                if (winnerContestModel.status.equalsIgnoreCase("success")) {
                    winnerPoolAdapter = new WinnerPoolAdapter(ActivityTournamentDetail.this, winnerContestModel.data.prizePoolsData);
                    rv_pool_price.setAdapter(winnerPoolAdapter);

                    if (contestsData.WinningAmount.equalsIgnoreCase("") || contestsData.WinningAmount.equalsIgnoreCase("0")) {
                        txt_pool_price.setVisibility(View.GONE);
                        txt_winning_rupee.setVisibility(View.GONE);
                    } else {
                        txt_pool_price.setVisibility(View.VISIBLE);
                        txt_winning_rupee.setVisibility(View.VISIBLE);
                    }

                    pool_price_bottomsheet.setState(BottomSheetBehavior.STATE_EXPANDED);
                    bg.setVisibility(View.VISIBLE);
                } else {
                    Constants.SnakeMessageYellow(parent_lyt, winnerContestModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<WinnerContestModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void getData() {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityTournamentDetail.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<TournamentDetailModel> tournamentDetailModelCall = apiInterface.getTournamentDetails(Constants.GET_SINGLE_TOURNAMENT, Pref.getValue(ActivityTournamentDetail.this, Constants.UserID, "", Constants.FILENAME), tournament_id);

        tournamentDetailModelCall.enqueue(new Callback<TournamentDetailModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<TournamentDetailModel> call, @NonNull Response<TournamentDetailModel> response) {
                progressDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                swipe_refresh_player.setRefreshing(false);

                TournamentDetailModel tournamentDetailModel = response.body();
                assert tournamentDetailModel != null;
                if (tournamentDetailModel.status.equalsIgnoreCase("success")) {
                    CaptainID = tournamentDetailModel.tournamentData.CaptainID;
                    rv_tournament_contests.setAdapter(new TournamentContestAdapter(ActivityTournamentDetail.this, tournamentDetailModel.tournamentData.contestsDataArrayList,
                            tournamentDetailModel.tournamentData.CaptainID, ActivityTournamentDetail.this, ActivityTournamentDetail.this));
                    discord_link = tournamentDetailModel.tournamentData.DiscordLink;
                    txt_title_type.setText(tournamentDetailModel.tournamentData.GameTypeName);

                    // ToDo: Old -> Game Status
                    /*if (tournamentDetailModel.tournamentData.Status.equalsIgnoreCase(Constants.IN_PROGRESS)) {
                        txt_live.setText("Live");
                    } else if (tournamentDetailModel.tournamentData.Status.equalsIgnoreCase(Constants.REVIEW)) {
                        txt_live.setText("Review");
                    } else if (tournamentDetailModel.tournamentData.Status.equalsIgnoreCase(Constants.COMPLETED)) {
                        txt_live.setText("Completed");
                    }*/

                    // ToDo: New -> Game Status
                    if (tournamentDetailModel.tournamentData.Status.equalsIgnoreCase(AppConstants.GAME_STATUS_ID_WAITING)) {
                        txt_live.setText(AppConstants.GAME_STATUS_WAITING);
                        mLinearSeparator.setVisibility(View.VISIBLE);
                    } else if (tournamentDetailModel.tournamentData.Status.equalsIgnoreCase(AppConstants.GAME_STATUS_ID_STARTED)) {
                        txt_live.setText(AppConstants.GAME_STATUS_STARTED);
                        mLinearSeparator.setVisibility(View.VISIBLE);
                    } else if (tournamentDetailModel.tournamentData.Status.equalsIgnoreCase(AppConstants.GAME_STATUS_ID_IN_PROGRESS)) {
                        txt_live.setText(AppConstants.GAME_STATUS_LIVE);
                        mLinearSeparator.setVisibility(View.VISIBLE);
                    } else if (tournamentDetailModel.tournamentData.Status.equalsIgnoreCase(AppConstants.GAME_STATUS_ID_REVIEWING)) {
                        txt_live.setText(AppConstants.GAME_STATUS_REVIEWING);
                        mLinearSeparator.setVisibility(View.VISIBLE);
                    } else if (tournamentDetailModel.tournamentData.Status.equalsIgnoreCase(AppConstants.GAME_STATUS_ID_COMPLETED)) {
                        txt_live.setText(AppConstants.GAME_STATUS_COMPLETED);
                        mLinearSeparator.setVisibility(View.VISIBLE);
                    } else if (tournamentDetailModel.tournamentData.Status.equalsIgnoreCase(AppConstants.GAME_STATUS_ID_CANCELLED)) {
                        txt_live.setText(AppConstants.GAME_STATUS_CANCELLED);
                        mLinearSeparator.setVisibility(View.VISIBLE);
                    } else {
                        txt_live.setVisibility(View.GONE);
                        mLinearSeparator.setVisibility(View.GONE);
                    }

                    youtubre_link = tournamentDetailModel.tournamentData.ChannelLink;
                    fb_link = tournamentDetailModel.tournamentData.FacebookLink;
                    insta_link = tournamentDetailModel.tournamentData.InstagramLink;
                    telegram_link = tournamentDetailModel.tournamentData.TelegramLink;


                    txt_tournamnet_name.setText(tournamentDetailModel.tournamentData.Title);
                    @SuppressLint("SimpleDateFormat") DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                    @SuppressLint("SimpleDateFormat") DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
                    String inputDateStr = tournamentDetailModel.tournamentData.Date;
                    Date date = null;
                    try {
                        date = inputFormat.parse(inputDateStr);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String outputDateStr = outputFormat.format(date);
                    Date c = Calendar.getInstance().getTime();
                    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm a");

                    txt_date.setText(outputDateStr);
                    txt_time.setText(FileUtils.getFormatedDateTime(tournamentDetailModel.tournamentData.Time, "HH:mm:ss", "hh:mm a"));

                    txt_map.setText(tournamentDetailModel.tournamentData.Map_Length);
                    txt_persp.setText(tournamentDetailModel.tournamentData.Perspective_LevelCap);
                    tournament_winning_amt = tournamentDetailModel.tournamentData.WinningAmount;
                    if (tournamentDetailModel.tournamentData.WinningAmount.equalsIgnoreCase("") || tournamentDetailModel.tournamentData.WinningAmount.equalsIgnoreCase("0")) {
                        winning_amount.setText("-");
                        rupees_symbol.setVisibility(View.GONE);
                    } else
                        winning_amount.setText(" " + tournamentDetailModel.tournamentData.WinningAmount);

                    txt_winners.setText(tournamentDetailModel.tournamentData.WinnersCount);
                    txt_per_kill_title.setText(tournamentDetailModel.tournamentData.PerKill_MaxLosesTitle);

                    if (tournamentDetailModel.tournamentData.PerKill_MaxLosesCurrency)
                        txrupee_symbol.setVisibility(View.VISIBLE);
                    else
                        txrupee_symbol.setVisibility(View.GONE);


                    lyt_winners.setOnClickListener(v -> getTournamentWinnerPool(tournamentDetailModel.tournamentData, tournamentDetailModel.tournamentData.WinningAmount));
                    if (tournamentDetailModel.tournamentData.WinnersCount.isEmpty() && tournamentDetailModel.tournamentData.PerKill_MaxLoses.isEmpty()) {
                        lyt_winners.setVisibility(View.GONE);
                        lyt_kills.setVisibility(View.GONE);
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 3.0f);
                        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 1.0f);
                        lyt_winnings.setLayoutParams(params);
                        lyt_entry_fees.setLayoutParams(params1);

                    } else if (tournamentDetailModel.tournamentData.WinnersCount.isEmpty()) {
                        lyt_winners.setVisibility(View.GONE);
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 2.0f);
                        lyt_kills.setLayoutParams(params);
                    } else if (tournamentDetailModel.tournamentData.PerKill_MaxLoses.isEmpty()) {
                        lyt_kills.setVisibility(View.GONE);
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 2.0f);
                        lyt_winners.setLayoutParams(params);
                    } else {
                        if (Integer.parseInt(tournamentDetailModel.tournamentData.WinnersCount) == 1)
                            txt_winner_title.setText("Winner");
                    }

                    txt_perkill.setText(" " + tournamentDetailModel.tournamentData.PerKill_MaxLoses);
                    if (tournamentDetailModel.tournamentData.EntryFee.equalsIgnoreCase("0")) {
                        txt_entry_fees.setText("Free");
                        txt_entry_rupee.setVisibility(View.GONE);
                    } else
                        txt_entry_fees.setText(" " + tournamentDetailModel.tournamentData.EntryFee);

                    int available_spots = Integer.parseInt(tournamentDetailModel.tournamentData.TotalSpots);
                    int joined_spots = Integer.parseInt(tournamentDetailModel.tournamentData.JoinedSpots);
                    int remaining_spots = available_spots - joined_spots;
                    if (remaining_spots > 1)
                        txt_available_spots.setText(remaining_spots + " players remaining");
                    else
                        txt_available_spots.setText(remaining_spots + " player remaining");
                    if (joined_spots > 1)
                        txt_available_remaining.setText(joined_spots + " players joined");
                    else
                        txt_available_remaining.setText(joined_spots + " player joined");

                    progress_bar.setMax(available_spots);
                    progress_bar.setProgress(joined_spots);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        txt_rules.setText(Html.fromHtml(tournamentDetailModel.tournamentData.Rules, Html.FROM_HTML_SEPARATOR_LINE_BREAK_DIV));
                    } else {
                        txt_rules.setText(Html.fromHtml(tournamentDetailModel.tournamentData.Rules));
                    }

                    usersDataArrayList = tournamentDetailModel.tournamentData.usersDataArrayList;


                    for (int i = 0; i < usersDataArrayList.size(); i++) {
                        if (CaptainID.equalsIgnoreCase(usersDataArrayList.get(i).UserID)) {
                            usersDataArrayList.remove(i);
                            break;
                        }
                    }

                    txt_name_user.setText(tournamentDetailModel.tournamentData.currentUserData.Name);
                    if (!tournamentDetailModel.tournamentData.currentUserData.MobileNumber.isEmpty()) {
                        txt_acc_number_user.setText("*****" + tournamentDetailModel.tournamentData.currentUserData.MobileNumber.substring(5));
                    }
                    if (tournamentDetailModel.tournamentData.currentUserData.Kills.isEmpty()) {
                        txt_kill_user.setText("-");
                    } else {
                        txt_kill_user.setText(tournamentDetailModel.tournamentData.currentUserData.Kills);
                    }

                    if (tournamentDetailModel.tournamentData.currentUserData.Rank.isEmpty()) {
                        txt_rank_user.setText("-");
                    } else {
                        txt_rank_user.setText(tournamentDetailModel.tournamentData.currentUserData.Rank);
                    }

                    if (tournamentDetailModel.tournamentData.Status.equalsIgnoreCase(Constants.COMPLETED) && !tournamentDetailModel.tournamentData.currentUserData.WinningAmount.isEmpty()) {
                        lyt_congratulartions_user.setVisibility(View.VISIBLE);
                        txt_winning_amount_user.setText(" " + tournamentDetailModel.tournamentData.currentUserData.WinningAmount);
                    }
                    Glide.with(ActivityTournamentDetail.this).load(tournamentDetailModel.tournamentData.currentUserData.UserProfileIcon).into(img_user_in_list);

                    playerListAdapter = new TournamentPlayerListAdapter(ActivityTournamentDetail.this, usersDataArrayList, tournamentDetailModel.tournamentData.Status);
                    rv_players.setAdapter(playerListAdapter);


                    if (Integer.parseInt(tournamentDetailModel.tournamentData.RatingsCount) > 0) {
                        showFeedback(tournamentDetailModel.tournamentData.ratingsDataArrayList, tournamentDetailModel.tournamentData.RatingsComment);
                    }

                } else {
                    Constants.SnakeMessageYellow(parent_lyt, tournamentDetailModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<TournamentDetailModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void getTournamentWinnerPool(TournamentDetailModel.TournamentData tournamentData, String winningAmount) {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityTournamentDetail.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<WinnerContestModel> contestDetailsModelCall = apiInterface.getTournamentWinners(Constants.GET_TOURNAMENT_WINNING_POOL, tournamentData.TournamentID);

        contestDetailsModelCall.enqueue(new Callback<WinnerContestModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<WinnerContestModel> call, @NonNull Response<WinnerContestModel> response) {
                progressDialog.dismiss();
                WinnerContestModel winnerContestModel = response.body();
                assert winnerContestModel != null;
                if (winnerContestModel.status.equalsIgnoreCase("success")) {
                    winnerPoolAdapter = new WinnerPoolAdapter(ActivityTournamentDetail.this, winnerContestModel.data.prizePoolsData);
                    rv_pool_price.setAdapter(winnerPoolAdapter);
                    tv_pool_price.setText(winningAmount);

                    if (winningAmount.equalsIgnoreCase(winningAmount) || winningAmount.equalsIgnoreCase("0")) {
                        txt_pool_price.setVisibility(View.GONE);
                        txt_winning_rupee.setVisibility(View.GONE);
                    } else {
                        txt_pool_price.setVisibility(View.VISIBLE);
                        txt_winning_rupee.setVisibility(View.VISIBLE);
                    }

                    pool_price_bottomsheet.setState(BottomSheetBehavior.STATE_EXPANDED);
                    bg.setVisibility(View.VISIBLE);
                } else {
                    Constants.SnakeMessageYellow(parent_lyt, winnerContestModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<WinnerContestModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void showFeedback(ArrayList<ContestDetailsModel.ContestsData.RatingsData> ratingsDataArrayList, String ratingsComment) {

        if (Integer.parseInt(ratingsComment) == 2)
            et_fb_other.setVisibility(View.VISIBLE);
        else
            et_fb_other.setVisibility(View.GONE);


        feedback_bottomsheet.setState(BottomSheetBehavior.STATE_EXPANDED);
        feedbackAdapter = new FeedbackAdapter(ActivityTournamentDetail.this, ratingsDataArrayList, ActivityTournamentDetail.this);
        rv_gif.setAdapter(feedbackAdapter);

        feedback_bottomsheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                switch (i) {
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        bg.setVisibility(View.GONE);
                        img_discord.setClickable(true);
                        img_youtube_sub.setClickable(true);
                        img_fb.setClickable(true);
                        img_insta.setClickable(true);
                        img_telegram.setClickable(true);
                        cv_invite_contest.setClickable(true);

                        break;
                    }
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        bg.setVisibility(View.VISIBLE);
                        img_discord.setClickable(false);
                        img_youtube_sub.setClickable(false);
                        img_fb.setClickable(false);
                        img_insta.setClickable(false);
                        img_telegram.setClickable(false);
                        cv_invite_contest.setClickable(false);

                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_HALF_EXPANDED:
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_details: {
                swipeRefreshLayout.setVisibility(View.VISIBLE);
                swipe_refresh_player.setVisibility(View.GONE);
                txt_rules.setVisibility(View.GONE);
                rv_chat.setVisibility(View.GONE);
                lyt_player.setVisibility(View.GONE);
                lyt_send_message.setVisibility(View.GONE);
                lyt_details.setVisibility(View.VISIBLE);
                view_chat.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                view_detail.setBackgroundColor(getResources().getColor(R.color.orange_color));
                view_player.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                view_rules.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

                txt_detail.setTypeface(typeface_semibold);
                txt_player.setTypeface(typeface_normal);
                txt_rule.setTypeface(typeface_normal);
                txt_chat.setTypeface(typeface_normal);
                break;
            }

            case R.id.txt_rules_tab: {
                swipeRefreshLayout.setVisibility(View.GONE);
                swipe_refresh_player.setVisibility(View.GONE);
                txt_rules.setVisibility(View.VISIBLE);
                lyt_player.setVisibility(View.GONE);
                lyt_details.setVisibility(View.GONE);
                rv_chat.setVisibility(View.GONE);
                lyt_send_message.setVisibility(View.GONE);
                view_chat.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                view_rules.setBackgroundColor(getResources().getColor(R.color.orange_color));
                view_player.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                view_detail.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

                txt_rule.setTypeface(typeface_semibold);
                txt_player.setTypeface(typeface_normal);
                txt_detail.setTypeface(typeface_normal);
                txt_chat.setTypeface(typeface_normal);
                break;
            }

            case R.id.txt_room_chat: {
                swipeRefreshLayout.setVisibility(View.GONE);
                swipe_refresh_player.setVisibility(View.GONE);
                txt_rules.setVisibility(View.GONE);
                rv_chat.setVisibility(View.VISIBLE);
                lyt_player.setVisibility(View.GONE);
                lyt_send_message.setVisibility(View.VISIBLE);
                lyt_details.setVisibility(View.GONE);
                view_detail.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                view_chat.setBackgroundColor(getResources().getColor(R.color.orange_color));
                view_player.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                view_rules.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

                txt_chat.setTypeface(typeface_semibold);
                txt_player.setTypeface(typeface_normal);
                txt_rule.setTypeface(typeface_normal);
                txt_detail.setTypeface(typeface_normal);


                break;
            }
            case R.id.txt_players: {
                swipeRefreshLayout.setVisibility(View.GONE);
                swipe_refresh_player.setVisibility(View.VISIBLE);
                txt_rules.setVisibility(View.GONE);
                lyt_player.setVisibility(View.VISIBLE);
                rv_chat.setVisibility(View.GONE);
                lyt_send_message.setVisibility(View.GONE);
                lyt_details.setVisibility(View.GONE);
                view_chat.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                view_player.setBackgroundColor(getResources().getColor(R.color.orange_color));
                view_detail.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                view_rules.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

                txt_player.setTypeface(typeface_semibold);
                txt_detail.setTypeface(typeface_normal);
                txt_chat.setTypeface(typeface_normal);
                txt_rule.setTypeface(typeface_normal);

                break;
            }
            case R.id.iv_close_pool_price: {
                pool_price_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
                break;
            }
            case R.id.card_submit_number: {
                if (!feedback_reason_id.isEmpty())
                    sendFeedbackToServer();
                else
                    Constants.SnakeMessageYellow(parent_lyt, "Please select a reason before submitting.");
                break;
            }
            case R.id.cv_invite_contest: {
                DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
                        .setLink(Uri.parse("https://www.gamerji.com/ContestDetail?category_id=" + tournament_id))
                        .setDomainUriPrefix("https://gamerji.page.link")
                        .setAndroidParameters(
                                new DynamicLink.AndroidParameters.Builder("com.esports.gamerjipro")
                                        .setMinimumVersion(125)
                                        .setFallbackUrl(Uri.parse("https://www.gamerji.com"))
                                        .build())
                        .setIosParameters(
                                new DynamicLink.IosParameters.Builder("com.fantasyjiesport")
                                        .setAppStoreId("1466052584")
                                        .setMinimumVersion("1.0")
                                        .build())
                        .setSocialMetaTagParameters(
                                new DynamicLink.SocialMetaTagParameters.Builder()
                                        .setTitle("Hey! Join me on Gamerji for this exciting match. Click on the following link and let's play together.")
                                        .setDescription("Gamerji")
                                        .build())
                        .buildDynamicLink();  // Or buildShortDynamicLink()


                Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                        .setLongLink(Uri.parse(dynamicLink.getUri().toString()))
                        .buildShortDynamicLink()
                        .addOnCompleteListener(this, task -> {
                            if (task.isSuccessful()) {
                                // Short link created
                                Uri shortLink = Objects.requireNonNull(task.getResult()).getShortLink();
                                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                                sharingIntent.setType("text/plain");
                                sharingIntent.putExtra(Intent.EXTRA_TEXT, shortLink.toString());
                                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Gamerji Contest");
                                startActivity(Intent.createChooser(sharingIntent, "Gamerji"));
                            } else {
                                Constants.SnakeMessageYellow(parent_lyt, "Something went wrong.Please try again later.");
                            }
                        });
                break;
            }

            case R.id.img_discord: {
                PackageManager pm = getPackageManager();
                if (FileUtils.isPackageInstalled("com.discord", pm)) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(discord_link));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setPackage("com.discord");
                    startActivity(intent);
                } else {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(discord_link));
                    startActivity(i);

                }

                break;
            }
            case R.id.img_youtube_sub: {
                PackageManager pm = getPackageManager();
                if (FileUtils.isPackageInstalled("com.google.android.youtube", pm)) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(youtubre_link));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setPackage("com.google.android.youtube");
                    startActivity(intent);
                } else {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(youtubre_link));
                    startActivity(i);
                }

                break;
            }

            case R.id.img_fb: {
                PackageManager pm = getPackageManager();
                if (FileUtils.isPackageInstalled("com.facebook.katana", pm)) {
                    startActivity(newFacebookIntent(pm, fb_link));
                } else {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(fb_link));
                    startActivity(i);
                }

                break;
            }

            case R.id.img_insta: {
                Uri uri = Uri.parse(insta_link);
                Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

                likeIng.setPackage("com.instagram.android");

                try {
                    startActivity(likeIng);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse(insta_link)));
                }
                break;
            }

            case R.id.img_telegram: {
                PackageManager pm = getPackageManager();
                startActivity(telegramIntent(pm));
                break;
            }
        }
    }

    private Intent telegramIntent(PackageManager packageManager) {
        Intent intent;
        try {
            try {
                packageManager.getPackageInfo("org.telegram.messenger", 0);//Check for Telegram Messenger App
            } catch (Exception e) {
                packageManager.getPackageInfo("org.thunderdog.challegram", 0);//Check for Telegram X App
            }
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tg://resolve?domain=" + telegram_link));
        } catch (Exception e) { //App not found open in browser
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.telegram.me/" + telegram_link));
        }
        return intent;
    }

    public static Intent newFacebookIntent(PackageManager pm, String url) {
        Uri uri = Uri.parse(url);
        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo("com.facebook.katana", 0);
            if (applicationInfo.enabled) {
                uri = Uri.parse("fb://facewebmodal/f?href=" + url);
            }
        } catch (PackageManager.NameNotFoundException ignored) {
        }
        return new Intent(Intent.ACTION_VIEW, uri);
    }


    private void sendFeedbackToServer() {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityTournamentDetail.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<GeneralResponseModel> generalResponseModelCall;

        if (et_fb_other.getText().toString().isEmpty())
            generalResponseModelCall = apiInterface.addFeedback(Constants.ADD_FEEDBACK, Pref.getValue(ActivityTournamentDetail.this, Constants.UserID, "", Constants.FILENAME), RatingID,
                    feedback_reason_id.toString(), "");
        else
            generalResponseModelCall = apiInterface.addFeedback(Constants.ADD_FEEDBACK, Pref.getValue(ActivityTournamentDetail.this, Constants.UserID, "", Constants.FILENAME), RatingID,
                    feedback_reason_id.toString(), et_fb_other.getText().toString());

        generalResponseModelCall.enqueue(new Callback<GeneralResponseModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<GeneralResponseModel> call, @NonNull Response<GeneralResponseModel> response) {
                progressDialog.dismiss();
                swipeRefreshLayout.setRefreshing(false);
                swipe_refresh_player.setRefreshing(false);
                GeneralResponseModel generalResponseModel = response.body();
                feedback_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);

                assert generalResponseModel != null;
                if (generalResponseModel.status.equalsIgnoreCase("success"))
                    Constants.SnakeMessageYellow(parent_lyt, generalResponseModel.message);
                else
                    Constants.SnakeMessageYellow(parent_lyt, generalResponseModel.message);

            }

            @Override
            public void onFailure(@NonNull Call<GeneralResponseModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
                feedback_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });
    }

    @Override
    public void onPickResult(PickResult pickResult) {
        if (pickResult.getError() == null) {
            sendSStoServer(pickResult);
        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Constants.SnakeMessageYellow(parent_lyt, pickResult.getError().getMessage());
        }

    }

    private void sendSStoServer(PickResult pickResult) {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityTournamentDetail.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        File file = new File(pickResult.getPath());
        try {
            OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
            pickResult.getBitmap().compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestParams requestParams = new RequestParams();
        try {
            requestParams.put("Action", Constants.UPLOAD_SS);
            requestParams.put("Val_Userid", Pref.getValue(ActivityTournamentDetail.this, Constants.UserID, "", Constants.FILENAME));
            requestParams.put("Val_Contestid", screenshot_contest_id);
            requestParams.put("Val_Contestuserid", ContestUserID);
            requestParams.put("Val_TUscreenshotimage", file);
            requestParams.put("Val_Tournamentid", tournament_id);
            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            asyncHttpClient.setTimeout(60 * 1000);
            asyncHttpClient.post(ActivityTournamentDetail.this, Constants.BASE_API + Constants.V_3_TOURNAMENT_DETAILS, requestParams, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    progressDialog.dismiss();
                    if (response.optString("status").equalsIgnoreCase("success")) {
                        Constants.SnakeMessageYellow(parent_lyt, response.optString("message"));
                        getData();
                        //Glide.with(ActivityTournamentDetail.this).load(response.optJSONObject("data").optString("ScreenshotURL")).into(uploaded_ss);

                        /*for (int i=0;i<usersDataArrayList.size();i++)
                        {
                            if (Pref.getValue(ActivityContestDetails.this,Constants.UserID,"", Constants.FILENAME).
                                    equalsIgnoreCase(usersDataArrayList.get(i).UserID))
                            {
                                Glide.with(ActivityContestDetails.this).load(response.optJSONObject("data").optString("ScreenshotURL")).into(uploaded_ss);
                                usersDataArrayList.get(i).ScreenshotFlag=true;
                                usersDataArrayList.get(i).ScreenshotURL=response.optJSONObject("data").optString("ScreenshotURL");
                            }
                        }
                        playerListAdapter.notifyDataSetChanged();*/
                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    super.onSuccess(statusCode, headers, response);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    super.onSuccess(statusCode, headers, responseString);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    Constants.SnakeMessageYellow(parent_lyt, "Something went wrong please try again");
                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    progressDialog.dismiss();
                    Constants.SnakeMessageYellow(parent_lyt, "Something went wrong please try again");
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    progressDialog.dismiss();
                    Constants.SnakeMessageYellow(parent_lyt, "Something went wrong please try again");
                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFeedbackClickListener(ContestDetailsModel.ContestsData.RatingsData ratingsData) {
        feedback_reason_id.clear();
        card_submit_number.setVisibility(View.VISIBLE);
        RatingID = ratingsData.RatingID;
        feedbackReasonAdapter = new FeedbackReasonAdapter(ActivityTournamentDetail.this, ratingsData.optionsDataArrayList, false, ActivityTournamentDetail.this);
        rv_reasons.setAdapter(feedbackReasonAdapter);
    }

    @Override
    public void onFeedbackReasonClick(String ROptionID, boolean checked) {
        if (feedback_reason_id.contains(ROptionID)) {
            if (checked)
                feedback_reason_id.add(ROptionID);
            else
                feedback_reason_id.remove(ROptionID);
        } else
            feedback_reason_id.add(ROptionID);
    }

    @Override
    public void onWinnersClick(ContestTypesModel.Data.TypesData.ContestsData contestsData) {
    }

    @Override
    public void onWinnersClick(ContestTypeModelNew.Data.ContestsData contestsData) {

    }

    @Override
    public void onMyContestWinnerClick(MyContestModel.Data.ContestsData contestsData) {
    }

    @Override
    public void onTournamentWinnerClick(TournamentTypeModel.Data.TypesData.TournamentsData tournamentsData) {
    }

    @Override
    public void onTournamentContestWinnersClickListener(TournamentDetailModel.TournamentData.ContestsData contestsData) {
        getWinnerPool(contestsData);
    }

    @Override
    public void OnTournamnetContestScreenShotClickListener(TournamentDetailModel.TournamentData.ContestsData contestsData) {
        screenshot_contest_id = contestsData.ContestID;
        ContestUserID = contestsData.currentContestUserData.ContestUserID;
        PickImageDialog.build(new PickSetup()).show(this);
    }

    class MySocket {
        Socket mSocket;

        MySocket() {
            try {
                mSocket = IO.socket(Constants.SOCKET_API);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }

        void off() {
            mSocket.off(Constants.RECEIVECHAT, receivelivechat);
            mSocket.off(Constants.RECEIVEALLCHATMESSAGE, receiveallchat);
            mSocket.off(Socket.EVENT_DISCONNECT, mSetOnSocketDisconnectListener);
            mSocket.off(Socket.EVENT_CONNECT, mSetOnSocketConnecttListener);
        }

        public void on() {
            mSocket.on(Socket.EVENT_CONNECT, mSetOnSocketConnecttListener);
            mSocket.on(Constants.RECEIVEALLCHATMESSAGE, receiveallchat);
            mSocket.on(Constants.RECEIVECHAT, receivelivechat);
            mSocket.on(Socket.EVENT_DISCONNECT, mSetOnSocketDisconnectListener);
        }


        public void Disconnect() {
            mSocket.disconnect();
        }

        boolean connected() {
            return mSocket.connected();
        }


        void connect() {
            mSocket.connect();
        }

        void requestallChatMessage() {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("user_id", Pref.getValue(ActivityTournamentDetail.this, Constants.UserID, "", Constants.FILENAME));
                jsonObject.put("contest_id", "0");
                jsonObject.put("tournament_id", tournament_id);
                mSocket.emit(Constants.REQUESTALLCHATMESSAGE, jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        void sendMessage(String message) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("user_id", Pref.getValue(ActivityTournamentDetail.this, Constants.UserID, "", Constants.FILENAME));
                jsonObject.put("contest_id", "0");
                jsonObject.put("tournament_id", tournament_id);
                jsonObject.put("first_name", Pref.getValue(ActivityTournamentDetail.this, Constants.TeamName, "", Constants.FILENAME));
                jsonObject.put("last_name", " ");
                jsonObject.put("chat_message", message);
                jsonObject.put("level_icon", Pref.getValue(ActivityTournamentDetail.this, Constants.LevelIcon, "", Constants.FILENAME));
                ChatModel chatModel = new ChatModel();
                chatModel.setSender_id(Pref.getValue(ActivityTournamentDetail.this, Constants.UserID, "", Constants.FILENAME));
                chatModel.setContest_id(tournament_id);
                chatModel.setChat_datetime(String.valueOf(System.currentTimeMillis() / 1000));
                chatModel.setChat_message(et_chat.getText().toString());
                chatModel.setSender_full_name(Pref.getValue(ActivityTournamentDetail.this, Constants.TeamName, "", Constants.FILENAME));

                chatModelArrayList.add(chatModel);
                mSocket.emit(Constants.SENDCHAT, jsonObject);
                et_chat.setText("");
                linearLayoutManager.smoothScrollToPosition(rv_chat, null, chatModelArrayList.size() - 1);
                chatAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        void joinChatRoom() {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("user_id", Pref.getValue(ActivityTournamentDetail.this, Constants.UserID, "", Constants.FILENAME));
                jsonObject.put("contest_id", "0");
                jsonObject.put("tournament_id", tournament_id);
                mSocket.emit(Constants.JOINCHATROOM, jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public Emitter.Listener mSetOnSocketDisconnectListener = args -> runOnUiThread(() ->
    {
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
    });

    public Emitter.Listener mSetOnSocketConnecttListener = args -> runOnUiThread(() ->
    {
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
    });


    public Emitter.Listener receivelivechat = args -> runOnUiThread(() ->
    {
        try {
            if (!args[0].toString().isEmpty()) {
                JSONObject jsonObject = new JSONObject(args[0].toString());

                ChatModel chatModel = new ChatModel();
                chatModel.setId("");
                chatModel.setSender_id(jsonObject.optString("user_id"));
                chatModel.setChat_message(jsonObject.optString("chat_message"));
                chatModel.setSender_full_name(jsonObject.optString("first_name") + " " + jsonObject.optString("last_name"));
                chatModel.setContest_id(jsonObject.optString("contest_id"));
                chatModel.setChat_datetime(jsonObject.optString("chat_datetime"));
                chatModel.setLevel_icon(jsonObject.optString("level_icon"));
                chatModelArrayList.add(chatModel);
                chatAdapter.notifyDataSetChanged();
                linearLayoutManager.smoothScrollToPosition(rv_chat, null, chatModelArrayList.size() - 1);
                //rv_chat.scrollToPosition(chatModelArrayList.size()-1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    });

    @Override
    protected void onDestroy() {
        mSocket.off();
        super.onDestroy();
    }

    public Emitter.Listener receiveallchat = args -> runOnUiThread(() ->
    {
        try {
            if (!args[0].toString().isEmpty()) {
                JSONArray jsonArray = new JSONArray(args[0].toString());
                for (int i = 0; i < jsonArray.length(); i++) {
                    ChatModel chatModel = new ChatModel();
                    chatModel.setChat_datetime(jsonArray.optJSONObject(i).optString("chat_datetime"));
                    chatModel.setChat_message(jsonArray.optJSONObject(i).optString("chat_message"));
                    chatModel.setSender_full_name(jsonArray.optJSONObject(i).optString("sender_full_name"));
                    chatModel.setContest_id(jsonArray.optJSONObject(i).optString("contest_id"));
                    chatModel.setSender_id(jsonArray.optJSONObject(i).optString("sender_id"));
                    chatModel.setId(jsonArray.optJSONObject(i).optString("id"));
                    chatModel.setLevel_icon(jsonArray.optJSONObject(i).optString("level_icon"));
                    chatModelArrayList.add(chatModel);
                }
                chatAdapter.notifyDataSetChanged();
                rv_chat.scrollToPosition(chatModelArrayList.size() - 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    });

}

