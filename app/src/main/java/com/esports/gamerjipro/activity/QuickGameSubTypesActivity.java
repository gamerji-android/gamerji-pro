package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.esports.gamerjipro.Adapter.QuickGameSubTypesAdapter;
import com.esports.gamerjipro.Model.QuickGameJoinGameModel;
import com.esports.gamerjipro.Model.QuickGameJoinGamePopupModel;
import com.esports.gamerjipro.Model.QuickGameSubTypesModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.RestApis.WebFields;
import com.esports.gamerjipro.Utils.AppConstants;
import com.esports.gamerjipro.Utils.Common;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.SessionManager;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuickGameSubTypesActivity extends AppCompatActivity implements QuickGameSubTypesAdapter.SingleClickListener {

    private CoordinatorLayout mCoordinatorMain;
    private RelativeLayout mRelativeNoData;
    private String mQuickGameType, mCategoryId;
    private ImageView mImageBack, mImageClose;
    private TextView mTextQuickGameSubType, mTextBottomSheetGameHeader, mTextBottomSheetGameBalance, mTextBottomSheetGameEntryAmount, mTextBottomSheetGameNotes;
    private LinearLayoutManager mLayoutManager;
    private SwipeRefreshLayout mSwipeRefreshView;
    private RecyclerView mRecyclerView;
    private ArrayList<QuickGameSubTypesModel.Data.GamesData> mArrQuickGameSubTypes;
    private QuickGameSubTypesAdapter mAdapterQuickGameSubTypes;
    private SessionManager mSessionManager;
    private View mViewBottomSheetBackground;
    private BottomSheetBehavior mBottomSheetQuickGameJoinGame;
    private LinearLayout mLinearBottomSheetEntryFree, mLinearBottomSheetEntryAmount, mLinearBottomSheetJoinGame;
    private String mGameId, mGameName, mGameURL;
    private int mGameDisplayMode;
    private ProgressDialog progressDialog;

    private int pageIndex = 15, page = 1, pastVisibleItems, visibleItemCount, totalItemCount;
    private boolean isLastPage = false, isLoading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_game_sub_types);
        mArrQuickGameSubTypes = new ArrayList<>();
        mSessionManager = new SessionManager();
        mLayoutManager = new GridLayoutManager(QuickGameSubTypesActivity.this, 3);

        getBundle();
        getIds();
        setRegListeners();
        setData();
        callToGetHTMLGameSubTypesAPI(page);
    }

    /**
     * Get the keys from the Quick Game Types Activity
     */
    private void getBundle() {
        try {
            mQuickGameType = getIntent().getStringExtra(AppConstants.BUNDLE_QUICK_GAME_TYPE);
            mCategoryId = getIntent().getStringExtra(AppConstants.BUNDLE_QUICK_GAME_CATEGORY_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Relative Layouts
            mCoordinatorMain = findViewById(R.id.relative_quick_game_sub_types_main_view);
            mRelativeNoData = findViewById(R.id.relative_quick_game_sub_types_no_data);

            // Image View
            mImageBack = findViewById(R.id.image_quick_game_sub_types_back);
            mImageClose = findViewById(R.id.image_bottom_sheet_quick_game_join_game_close);

            // Image View
            mTextQuickGameSubType = findViewById(R.id.text_quick_game_sub_type);
            mTextBottomSheetGameHeader = findViewById(R.id.text_bottom_sheet_quick_game_join_game_name);
            mTextBottomSheetGameBalance = findViewById(R.id.text_bottom_sheet_quick_game_join_game_balance);
            mTextBottomSheetGameEntryAmount = findViewById(R.id.text_bottom_sheet_quick_game_join_game_entry_amount);
            mTextBottomSheetGameNotes = findViewById(R.id.text_bottom_sheet_quick_game_join_game_notes);

            // Swipe Refresh View
            mSwipeRefreshView = findViewById(R.id.swipe_refresh_view_quick_game_sub_types);

            // Recycler View
            mRecyclerView = findViewById(R.id.recycler_view_quick_game_sub_types);
            LinearLayout mLinearBottomSheetQuickGameJoinGame = findViewById(R.id.linear_bottom_sheet_quick_game_join_game_main_view);
            mLinearBottomSheetEntryFree = findViewById(R.id.linear_bottom_sheet_quick_game_join_game_entry_free);
            mLinearBottomSheetEntryAmount = findViewById(R.id.linear_bottom_sheet_quick_game_join_game_entry_amount);
            mLinearBottomSheetJoinGame = findViewById(R.id.linear_bottom_sheet_quick_game_join_game);

            // View
            mViewBottomSheetBackground = findViewById(R.id.view_quick_game_sub_types);

            // Bottom Sheet Behaviours
            mBottomSheetQuickGameJoinGame = BottomSheetBehavior.from(mLinearBottomSheetQuickGameJoinGame);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: On Click Listener
            mImageBack.setOnClickListener(clickListener);
            mImageClose.setOnClickListener(clickListener);
            mLinearBottomSheetJoinGame.setOnClickListener(clickListener);

            // ToDo: Swipe Refresh Click Listener
            mSwipeRefreshView.setOnRefreshListener(refreshListener);

            // ToDo: How To Bottom Sheet Call Back
            mBottomSheetQuickGameJoinGame.setBottomSheetCallback(quickGameJoinGameBottomSheetCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        try {
            mTextQuickGameSubType.setText(mQuickGameType);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.image_quick_game_sub_types_back:
                    finish();
                    break;

                case R.id.image_bottom_sheet_quick_game_join_game_close:
                    mBottomSheetQuickGameJoinGame.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    mViewBottomSheetBackground.setVisibility(View.GONE);
                    break;

                case R.id.linear_bottom_sheet_quick_game_join_game:
                    callToJoinGameAPI();
                    break;
            }
        }
    };

    /**
     * Set On Refresh Listeners
     */
    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            page = 1;
            isLastPage = false;
            if (mArrQuickGameSubTypes != null)
                mArrQuickGameSubTypes.clear();
            if (mAdapterQuickGameSubTypes != null)
                mAdapterQuickGameSubTypes.notifyDataSetChanged();
            callToGetHTMLGameSubTypesAPI(page);
        }
    };

    /**
     * Call back listener (Quick Game Join Game)
     */
    BottomSheetBehavior.BottomSheetCallback quickGameJoinGameBottomSheetCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            switch (newState) {
                case BottomSheetBehavior.STATE_EXPANDED:
                    mViewBottomSheetBackground.setVisibility(View.VISIBLE);
                    break;

                case BottomSheetBehavior.STATE_COLLAPSED:
                    mViewBottomSheetBackground.setVisibility(View.GONE);
                    break;
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    /**
     * This method should get all the HTML 5 Game Sub Types API
     */
    private void callToGetHTMLGameSubTypesAPI(int page) {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(QuickGameSubTypesActivity.this);
            progressDialog.setMessage("Please wait...."); // Setting Message
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
            progressDialog.show(); // Display Progress Dialog
            progressDialog.setCancelable(false);

            String mUserId = mSessionManager.getUserId(QuickGameSubTypesActivity.this);

            Call<QuickGameSubTypesModel> callRepos = APIClient.getClient().create(APIInterface.class).getQuickGameSubTypes(WebFields.QUICK_GAME_SUB_TYPES.MODE, mUserId, mCategoryId, String.valueOf(page), String.valueOf(pageIndex));
            callRepos.enqueue(new Callback<QuickGameSubTypesModel>() {
                @Override
                public void onResponse(@NonNull Call<QuickGameSubTypesModel> call, @NonNull Response<QuickGameSubTypesModel> response) {
                    isLoading = false;
                    mSwipeRefreshView.setRefreshing(false);
                    progressDialog.dismiss();

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        assert response.body() != null;
                        if (response.body().getStatus().equalsIgnoreCase(Constants.API_SUCCESS)) {

                            ArrayList<QuickGameSubTypesModel.Data.GamesData> gameTypesModels = (ArrayList<QuickGameSubTypesModel.Data.GamesData>)
                                    response.body().getData().getGamesData();

                            if (response.body().getData().getGamesCount() > 0) {
                                if (mArrQuickGameSubTypes != null && mArrQuickGameSubTypes.size() > 0 &&
                                        mAdapterQuickGameSubTypes != null) {
                                    mArrQuickGameSubTypes.addAll(gameTypesModels);
                                    mAdapterQuickGameSubTypes.notifyDataSetChanged();
                                } else {
                                    mArrQuickGameSubTypes = gameTypesModels;
                                    setAdapterData();
                                    setLoadMoreClickListener();
                                }
                            } else {
                                isLastPage = true;
                            }
                        } else {
                            setAdapterData();
                            Constants.SnakeMessageYellow(mCoordinatorMain, response.body().getMessage());
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<QuickGameSubTypesModel> call, @NonNull Throwable t) {
                    mSwipeRefreshView.setRefreshing(false);
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());

            mAdapterQuickGameSubTypes = new QuickGameSubTypesAdapter(QuickGameSubTypesActivity.this, mArrQuickGameSubTypes);
            mRecyclerView.setAdapter(mAdapterQuickGameSubTypes);
            mAdapterQuickGameSubTypes.setOnCardViewClickListener(this);

            setViewVisibility();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the view visibility as per the array size
     */
    private void setViewVisibility() {
        if (mArrQuickGameSubTypes.size() != 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            mRelativeNoData.setVisibility(View.GONE);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            mRelativeNoData.setVisibility(View.VISIBLE);
        }
    }

    /**
     * On load more click listener
     */
    private void setLoadMoreClickListener() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (!isLastPage && !isLoading) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                            isLoading = true;
                            Common.insertLog("Last Item Wow !");
                            callToGetHTMLGameSubTypesAPI(++page);
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onCardViewClickListener(QuickGameSubTypesModel.Data.GamesData quickGameTypesModel) {
        mGameId = quickGameTypesModel.getGameID();
        mGameName = quickGameTypesModel.getName();
        mGameURL = quickGameTypesModel.getUrl();
        mGameDisplayMode = quickGameTypesModel.getDisplayMode();
        callToJoinGameForPopupAPI();
    }

    /**
     * This method should get all the HTML 5 Game Types API
     */
    private void callToJoinGameForPopupAPI() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(QuickGameSubTypesActivity.this);
            progressDialog.setMessage("Please wait...."); // Setting Message
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
            progressDialog.show(); // Display Progress Dialog
            progressDialog.setCancelable(false);

            String mUserId = mSessionManager.getUserId(QuickGameSubTypesActivity.this);

            Call<QuickGameJoinGamePopupModel> callRepos = APIClient.getClient().create(APIInterface.class).quickGameJoinGamePopup(WebFields.QUICK_GAME_JOIN_GAME_POPUP.MODE, mUserId, mGameId);
            callRepos.enqueue(new Callback<QuickGameJoinGamePopupModel>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(@NonNull Call<QuickGameJoinGamePopupModel> call, @NonNull Response<QuickGameJoinGamePopupModel> response) {
                    progressDialog.dismiss();

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        assert response.body() != null;
                        if (response.body().getStatus().equalsIgnoreCase(Constants.API_SUCCESS)) {

                            mTextBottomSheetGameHeader.setText(mQuickGameType + " - " + mGameName);
                            mTextBottomSheetGameBalance.setText(response.body().getData().getWalletBalance());
                            mTextBottomSheetGameNotes.setText(response.body().getData().getHelpText());

                            if(response.body().getData().getEntryFee().equalsIgnoreCase("0") || response.body().getData().getEntryFee().equalsIgnoreCase("")) {
                                mLinearBottomSheetEntryFree.setVisibility(View.VISIBLE);
                                mLinearBottomSheetEntryAmount.setVisibility(View.GONE);
                            } else {
                                mLinearBottomSheetEntryFree.setVisibility(View.GONE);
                                mLinearBottomSheetEntryAmount.setVisibility(View.VISIBLE);
                                mTextBottomSheetGameEntryAmount.setText(response.body().getData().getEntryFee());
                            }

                            if (mBottomSheetQuickGameJoinGame.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                                mBottomSheetQuickGameJoinGame.setState(BottomSheetBehavior.STATE_EXPANDED);
                            } else {
                                mBottomSheetQuickGameJoinGame.setState(BottomSheetBehavior.STATE_COLLAPSED);
                            }
                        } else {
                            Constants.SnakeMessageYellow(mCoordinatorMain, response.body().getMessage());
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<QuickGameJoinGamePopupModel> call, @NonNull Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the Join Game API
     */
    private void callToJoinGameAPI() {
        try {
            progressDialog = new ProgressDialog(QuickGameSubTypesActivity.this);
            progressDialog.setMessage("Please wait...."); // Setting Message
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
            progressDialog.show(); // Display Progress Dialog
            progressDialog.setCancelable(false);

            String mUserId = mSessionManager.getUserId(QuickGameSubTypesActivity.this);

            Call<QuickGameJoinGameModel> callRepos = APIClient.getClient().create(APIInterface.class).quickGameJoinGame(WebFields.QUICK_GAME_JOIN_GAME.MODE, mUserId, mGameId, mCategoryId);
            callRepos.enqueue(new Callback<QuickGameJoinGameModel>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(@NonNull Call<QuickGameJoinGameModel> call, @NonNull Response<QuickGameJoinGameModel> response) {
                    progressDialog.dismiss();

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        assert response.body() != null;
                        if (response.body().getStatus().equalsIgnoreCase(Constants.API_SUCCESS)) {
                            int mGameJoinedId = response.body().getData().getJoinedID();
                            callToPlayGameActivity(mGameJoinedId);
                        } else {
                            Constants.SnakeMessageYellow(mCoordinatorMain, response.body().getMessage());
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<QuickGameJoinGameModel> call, @NonNull Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should open the Play Game Activity
     */
    private void callToPlayGameActivity(int mGameJoinedId) {
        try {
            Intent intent = new Intent(QuickGameSubTypesActivity.this, PlayGameActivity.class);
            intent.putExtra(AppConstants.BUNDLE_QUICK_GAME_ID, mGameId);
            intent.putExtra(AppConstants.BUNDLE_QUICK_GAME_CATEGORY_ID, mCategoryId);
            intent.putExtra(AppConstants.BUNDLE_QUICK_GAME_URL, mGameURL);
            intent.putExtra(AppConstants.BUNDLE_QUICK_GAME_DISPLAY_MODE, mGameDisplayMode);
            intent.putExtra(AppConstants.BUNDLE_QUICK_GAME_JOINED_ID, mGameJoinedId);
            startActivity(intent);
            mBottomSheetQuickGameJoinGame.setState(BottomSheetBehavior.STATE_COLLAPSED);
            mViewBottomSheetBackground.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (mViewBottomSheetBackground.getVisibility() == View.VISIBLE) {
            mBottomSheetQuickGameJoinGame.setState(BottomSheetBehavior.STATE_COLLAPSED);
            mViewBottomSheetBackground.setVisibility(View.GONE);
        } else {
            finish();
        }
    }
}