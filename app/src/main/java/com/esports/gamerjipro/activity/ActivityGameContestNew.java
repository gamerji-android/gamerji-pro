package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.esports.gamerjipro.Adapter.SectionAdapter;
import com.esports.gamerjipro.Adapter.WinnerPoolAdapter;
import com.esports.gamerjipro.Interface.ContestJoinClickListener;
import com.esports.gamerjipro.Interface.WinnerClickListener;
import com.esports.gamerjipro.Model.ContestTypeModelNew;
import com.esports.gamerjipro.Model.ContestTypesModel;
import com.esports.gamerjipro.Model.JoinContestModel;
import com.esports.gamerjipro.Model.MyContestModel;
import com.esports.gamerjipro.Model.SignInModel;
import com.esports.gamerjipro.Model.TournamentDetailModel;
import com.esports.gamerjipro.Model.TournamentTypeModel;
import com.esports.gamerjipro.Model.WalletUsageLimitModel;
import com.esports.gamerjipro.Model.WinnerContestModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.AppConstants;
import com.esports.gamerjipro.Utils.Common;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.FileUtils;
import com.esports.gamerjipro.Utils.Pref;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityGameContestNew extends AppCompatActivity implements WinnerClickListener, ContestJoinClickListener, View.OnClickListener {
    CardView cv_join_contest;
    RecyclerView rv_games, rv_pool_price;
    public RelativeLayout lyt_empty;
    LinearLayout layoutBottomSheet, bonus_bottom_layout, pool_price_bottom;
    public BottomSheetBehavior sheetBehavior, bottom_sheet_cash_bonus, pool_price_bottomsheet;
    String main_game_name;
    RelativeLayout lyt_sub_main;
    View bg;
    ImageView img_image, img_close, img_info, img_close_cash, iv_close_pool_price, img_back;
    TextView txt_title, tv_pool_price, txt_my_balance;
    public TextView txt_entry_fees, txt_usable_cash, txt_to_pay, txt_game_type;
    APIInterface apiInterface;
    public String game_id = "", game_type_id = "", contest_id = "";
    public static ArrayList<SignInModel.UserData.GamesData.GameData> userGameDataarrayList = new ArrayList<>();
    public String UniqueName = "", game_name, userGameId;
    WinnerPoolAdapter winnerPoolAdapter;
    CoordinatorLayout parent_lyt;
    boolean JoinFlag = true;
    String JoinButtonFlag;
    float add_balance, my_balance, topay;
    public SwipeRefreshLayout swipe_refresh_home;
    public boolean isEmpty = true;
    TextView txt_pool_price, txt_winning_rupee;
    int CanJoinPlayers, CanJoinExtraPlayers;
    TextView txt_warning;
    ContestTypeModelNew.Data.ContestsData contestsData;
    TextView img_news;
    int page = 1;
    ArrayList<ContestTypeModelNew.Data.ContestsData> contestsDataArrayList = new ArrayList<>();
    private boolean isLoading = false, isLastPage = false;
    int entry_fee;
    SectionAdapter sectionAdapter;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_contest_activity);

        img_news = findViewById(R.id.img_news);
        img_news.setOnClickListener(v -> FileUtils.OpenNewsWebview(this, "https://www.gamerji.com"));

        rv_games = findViewById(R.id.rv_games);
        lyt_sub_main = findViewById(R.id.lyt_sub_main);
        parent_lyt = findViewById(R.id.parent_lyt);
        tv_pool_price = findViewById(R.id.tv_pool_price);
        rv_pool_price = findViewById(R.id.rv_pool_price);
        lyt_empty = findViewById(R.id.lyt_empty);
        swipe_refresh_home = findViewById(R.id.swipe_refresh_home);

        iv_close_pool_price = findViewById(R.id.iv_close_pool_price);
        txt_pool_price = findViewById(R.id.txt_pool_price);
        txt_winning_rupee = findViewById(R.id.txt_winning_rupee);
        txt_my_balance = findViewById(R.id.txt_my_balance);
        img_info = findViewById(R.id.img_info);
        bg = findViewById(R.id.bg);
        img_close = findViewById(R.id.img_close_join);
        txt_warning = findViewById(R.id.txt_warning);

        img_close_cash = findViewById(R.id.img_close_cash);
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(v -> finish());

        txt_title = findViewById(R.id.txt_title);
        if (getIntent().getExtras() != null) {
            txt_title.setText(getIntent().getStringExtra("game_name"));
            game_name = getIntent().getStringExtra("game_name");
            game_id = getIntent().getStringExtra("game_id");
            main_game_name = getIntent().getStringExtra("main_game_name");

            if (getIntent().getStringExtra("main_game_name") == null)
                main_game_name = Constants.Main_game_name;
            game_type_id = getIntent().getStringExtra("game_type_id");
        }

        Common.insertLog("Main Game Name:::> " + main_game_name);
        if (Constants.Main_game_name.equalsIgnoreCase(AppConstants.GAME_P_MOBILE)) {
            txt_warning.setVisibility(View.VISIBLE);
        }
        apiInterface = APIClient.getClient().create(APIInterface.class);
        img_image = findViewById(R.id.img_image);
        layoutBottomSheet = findViewById(R.id.bottom_sheet);
        cv_join_contest = layoutBottomSheet.findViewById(R.id.cv_join_contest);

        // ToDo: New -> Ads
        bonus_bottom_layout = findViewById(R.id.bottom_sheet_cash_bonus);
        pool_price_bottom = findViewById(R.id.pool_price_bottom);
        txt_entry_fees = findViewById(R.id.txt_entry_fees);
        txt_game_type = findViewById(R.id.txt_game_type);
        txt_game_type.setText("  " + main_game_name + "-" + game_name);
        txt_usable_cash = findViewById(R.id.txt_usable_cash);
        txt_to_pay = findViewById(R.id.txt_to_pay);


        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mLinearLayoutManager.setAutoMeasureEnabled(false);

        rv_pool_price.setLayoutManager(new LinearLayoutManager(this));
        rv_pool_price.setItemAnimator(new DefaultItemAnimator());

        rv_games.setLayoutManager(mLinearLayoutManager);
        rv_games.setItemAnimator(new DefaultItemAnimator());

        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        bottom_sheet_cash_bonus = BottomSheetBehavior.from(bonus_bottom_layout);
        pool_price_bottomsheet = BottomSheetBehavior.from(pool_price_bottom);

        userGameDataarrayList = Constants.getUserData(ActivityGameContestNew.this);

        sectionAdapter = new SectionAdapter(ActivityGameContestNew.this, contestsDataArrayList, ActivityGameContestNew.this, ActivityGameContestNew.this);
        rv_games.setAdapter(sectionAdapter);


        getUserData();
        setGameContestList(page);
        cv_join_contest.setOnClickListener(this);

        // ToDo: New -> Ads
        img_close.setOnClickListener(v ->
        {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            bottom_sheet_cash_bonus.setState(BottomSheetBehavior.STATE_COLLAPSED);
            bg.setVisibility(View.GONE);
        });

        img_close_cash.setOnClickListener(v -> bottom_sheet_cash_bonus.setState(BottomSheetBehavior.STATE_COLLAPSED));
        iv_close_pool_price.setOnClickListener(v -> pool_price_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED));

        img_info.setOnClickListener(v -> bottom_sheet_cash_bonus.setState(BottomSheetBehavior.STATE_EXPANDED));

        pool_price_bottomsheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                    case BottomSheetBehavior.STATE_HALF_EXPANDED:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        bg.setVisibility(View.VISIBLE);
                    }
                    break;

                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        bg.setVisibility(View.GONE);
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:

                    case BottomSheetBehavior.STATE_SETTLING: {
                        break;
                    }
                }

            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                    case BottomSheetBehavior.STATE_HALF_EXPANDED:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        bg.setVisibility(View.VISIBLE);
                    }
                    break;

                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        bg.setVisibility(View.GONE);
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:

                    case BottomSheetBehavior.STATE_SETTLING: {
                        break;
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });

        swipe_refresh_home.setOnRefreshListener(() ->
        {
            page = 1;
            contestsDataArrayList.clear();
            setGameContestList(page);

        });

        rv_games.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = mLinearLayoutManager.getChildCount();
                int totalItemCount = mLinearLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLinearLayoutManager.findFirstVisibleItemPosition();

                if (!isLoading && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= 10) {
                        if (page == 1)
                            contestsDataArrayList.clear();

                        setGameContestList(page);
                    }
                }
            }
        });
    }

    private void joinContest() {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityGameContestNew.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        Call<JoinContestModel> joinContestModelCall = apiInterface.joinContest(Constants.JOIN_CONTEST, Pref.getValue(ActivityGameContestNew.this, Constants.UserID, "", Constants.FILENAME), contest_id, JoinButtonFlag);

        joinContestModelCall.enqueue(new Callback<JoinContestModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<JoinContestModel> call, @NonNull Response<JoinContestModel> response) {
                Common.insertLog("Response:::> " + response.toString());
                progressDialog.dismiss();
                JoinContestModel joinContestModel = response.body();
                assert joinContestModel != null;
                if (joinContestModel.status.equalsIgnoreCase("success")) {
                    Intent i = new Intent(ActivityGameContestNew.this, ActivityContestDetails.class);
                    i.putExtra("contest_id", contest_id);
                    startActivity(i);
                    finish();
                } else {
                    Constants.SnakeMessageYellow(parent_lyt, joinContestModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<JoinContestModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void getUserData() {
        assert userGameDataarrayList != null;
        for (int i = 0; i < userGameDataarrayList.size(); i++) {
            if (userGameDataarrayList.get(i).GameID.equalsIgnoreCase(game_id)) {
                userGameId = userGameDataarrayList.get(i).UserGameID;
                UniqueName = userGameDataarrayList.get(i).UniqueName;
            }
        }
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    private void setGameContestList(int page) {

        isLoading = true;
        final ProgressDialog progressDialog = new ProgressDialog(ActivityGameContestNew.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<ContestTypeModelNew> contestTypesModelCall = apiInterface.getContests(Constants.GET_CONTESTS_LIST, Pref.getValue(ActivityGameContestNew.this, Constants.UserID, "", Constants.FILENAME), game_id, game_type_id, page, "10");
        contestTypesModelCall.enqueue(new Callback<ContestTypeModelNew>() {
            @Override
            public void onResponse(@NonNull Call<ContestTypeModelNew> call, @NonNull Response<ContestTypeModelNew> response) {

                //WITHOUT LIBRARY IN ONE RECYCLER VIEW

                isLoading = false;
                progressDialog.dismiss();
                swipe_refresh_home.setRefreshing(false);
                ContestTypeModelNew contestTypesModel = response.body();
                assert contestTypesModel != null;

                if (contestTypesModel.status.equalsIgnoreCase("success")) {
//                    Glide.with(ActivityGameContestNew.this).load(contestTypesModel.DataClass.FeaturedImage).into(img_image);
                    lyt_empty.setVisibility(View.GONE);
                    swipe_refresh_home.setVisibility(View.VISIBLE);


                    if (Integer.parseInt(contestTypesModel.DataClass.ContestsCount) > 0) {
                        contestsDataArrayList.addAll(contestTypesModel.DataClass.contestsDataArrayList);
                        sectionAdapter.notifyDataSetChanged();
                    } else {
                        Constants.SnakeMessageYellow(parent_lyt, contestTypesModel.message);
                    }

                    isLastPage = contestTypesModel.DataClass.IsLast;

                    if (!isLastPage)
                        ActivityGameContestNew.this.page++;

                    /*if(isLastPage)
                        swipe_refresh_home.setEnabled(false);*/
                } else {
                    swipe_refresh_home.setVisibility(View.GONE);
                    lyt_empty.setVisibility(View.VISIBLE);
                    Constants.SnakeMessageYellow(parent_lyt, contestTypesModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ContestTypeModelNew> call, @NonNull Throwable t) {
                isLoading = false;
                progressDialog.dismiss();
                swipe_refresh_home.setRefreshing(false);
                t.printStackTrace();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.PROFILE_UPDATE_STATUS_REQUEST_CODE) {
            assert data != null;
            userGameDataarrayList = Constants.getUserData(ActivityGameContestNew.this);
            getUserData();
            if (data.getBooleanExtra("profile_update_status", false)) {
                getWalletUsageLimit(contestsData);
                    /*if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED)
                    {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    }
                    else
                    {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    }*/
            }
        }
    }

    @Override
    public void onWinnersClick(ContestTypesModel.Data.TypesData.ContestsData contestsData) {

    }

    @Override
    public void onWinnersClick(ContestTypeModelNew.Data.ContestsData contestsData) {
        if (contestsData.WinningAmount.equalsIgnoreCase("0") || contestsData.WinningAmount.equalsIgnoreCase("")) {
            txt_pool_price.setVisibility(View.GONE);
            txt_winning_rupee.setVisibility(View.GONE);
        } else {
            txt_pool_price.setVisibility(View.VISIBLE);
            txt_winning_rupee.setVisibility(View.VISIBLE);
            tv_pool_price.setText(contestsData.WinningAmount);
        }

        getWinnerPool(contestsData);
    }

    private void getWinnerPool(ContestTypeModelNew.Data.ContestsData contestsData) {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityGameContestNew.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<WinnerContestModel> contestDetailsModelCall = apiInterface.getContestWinner(Constants.GETSINGLECONTESTWINNER, contestsData.ContestID);

        contestDetailsModelCall.enqueue(new Callback<WinnerContestModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<WinnerContestModel> call, @NonNull Response<WinnerContestModel> response) {
                progressDialog.dismiss();
                WinnerContestModel winnerContestModel = response.body();
                assert winnerContestModel != null;
                if (winnerContestModel.status.equalsIgnoreCase("success")) {
                    winnerPoolAdapter = new WinnerPoolAdapter(ActivityGameContestNew.this, winnerContestModel.data.prizePoolsData);
                    rv_pool_price.setAdapter(winnerPoolAdapter);
                    pool_price_bottomsheet.setState(BottomSheetBehavior.STATE_EXPANDED);
                    bg.setVisibility(View.VISIBLE);
                } else {
                    Constants.SnakeMessageYellow(parent_lyt, winnerContestModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<WinnerContestModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onMyContestWinnerClick(MyContestModel.Data.ContestsData contestsData) {
    }

    @Override
    public void onTournamentWinnerClick(TournamentTypeModel.Data.TypesData.TournamentsData tournamentsData) {

    }

    @Override
    public void onTournamentContestWinnersClickListener(TournamentDetailModel.TournamentData.ContestsData contestsData) {

    }

    @Override
    public void onContestJoinClickLitener(ContestTypesModel.Data.TypesData.ContestsData contestsData) {
    }

    @Override
    public void onContestJoinClickLitener(ContestTypeModelNew.Data.ContestsData contestsData) {
        if (contestsData.Joined) {
            Intent i = new Intent(ActivityGameContestNew.this, ActivityContestDetails.class);
            i.putExtra("contest_id", contestsData.ContestID);
            startActivity(i);
            finish();
        } else {
            contest_id = contestsData.ContestID;
            txt_entry_fees.setText(contestsData.EntryFee);
            txt_usable_cash.setText("0");
            txt_to_pay.setText(contestsData.EntryFee);
            CanJoinPlayers = contestsData.CanJoinPlayers;
            CanJoinExtraPlayers = contestsData.CanJoinExtraPlayers;
            this.contestsData = contestsData;

            if (UniqueName.equalsIgnoreCase("") || Pref.getValue(ActivityGameContestNew.this, Constants.BirthDate, "", Constants.FILENAME).equalsIgnoreCase("")) {
                Intent i = new Intent(ActivityGameContestNew.this, ActivityDOB.class);
                i.putExtra("game_name", game_name);
                i.putExtra("user_game_id", userGameId);
                i.putExtra("game_id", game_id);
                i.putExtra("main_game_name", main_game_name);
                startActivityForResult(i, Constants.PROFILE_UPDATE_STATUS_REQUEST_CODE);
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            } else {
                getWalletUsageLimit(contestsData);
            }
        }

    }

    private void getWalletUsageLimit(ContestTypeModelNew.Data.ContestsData contestsData) {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityGameContestNew.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<WalletUsageLimitModel> contestDetailsModelCall = apiInterface.getUserWalletLimit(Constants.WALLET_USAGE_LIMIT, Pref.getValue(ActivityGameContestNew.this, Constants.UserID, "", Constants.FILENAME), contestsData.ContestID);

        contestDetailsModelCall.enqueue(new Callback<WalletUsageLimitModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<WalletUsageLimitModel> call, @NonNull Response<WalletUsageLimitModel> response) {
                progressDialog.dismiss();
                WalletUsageLimitModel walletUsageLimitModel = response.body();
                assert walletUsageLimitModel != null;
                if (walletUsageLimitModel.status.equalsIgnoreCase("success")) {
                    contest_id = contestsData.ContestID;
                    txt_entry_fees.setText(walletUsageLimitModel.data.EntryFee);
                    entry_fee = Integer.parseInt(walletUsageLimitModel.data.EntryFee);
                    txt_usable_cash.setText(walletUsageLimitModel.data.CashBalance);
                    txt_to_pay.setText(walletUsageLimitModel.data.ToPay);
                    JoinFlag = walletUsageLimitModel.data.JoinFlag;
                    JoinButtonFlag = walletUsageLimitModel.data.JoinButtonFlag;
                    txt_my_balance.setText(walletUsageLimitModel.data.WalletBalance);
                    my_balance = Float.parseFloat(walletUsageLimitModel.data.WalletBalance);
                    topay = Float.parseFloat(walletUsageLimitModel.data.ToPay);

                    if (JoinButtonFlag.equals(Constants.DEFAULT_JOIN)) {
                        // ToDo: New -> Ads
//                        cv_video_join.setVisibility(View.GONE);
                        cv_join_contest.setVisibility(View.VISIBLE);
                    } else if (JoinButtonFlag.equals(Constants.VIDEO_JOIN)) {
                        cv_join_contest.setVisibility(View.GONE);
                        // ToDo: New -> Ads
//                        cv_video_join.setVisibility(View.VISIBLE);
                    } else {
                        cv_join_contest.setVisibility(View.VISIBLE);
                        // ToDo: New -> Ads
//                        cv_video_join.setVisibility(View.VISIBLE);
                    }

                    if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    } else {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    }
                } else {
                    Constants.SnakeMessageYellow(parent_lyt, walletUsageLimitModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<WalletUsageLimitModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cv_join_contest: {
                getUserData();
                if (JoinFlag) {
                    if (Constants.CheckRestrictedStates(ActivityGameContestNew.this))
                        Constants.SnakeMessageYellow(parent_lyt, getResources().getString(R.string.restricted_states_msg));
                    else if (!Constants.isCitizenOfIndia(ActivityGameContestNew.this))
                        Constants.SnakeMessageYellow(parent_lyt, getResources().getString(R.string.restricted_country_msg));
                    else {

                        if (CanJoinPlayers == 1) {
                            joinContest();
                        } else if (CanJoinPlayers > 1) {
                            Intent i = new Intent(ActivityGameContestNew.this, ActivitySquadRegistration.class);
                            i.putExtra("CanJoinPlayers", CanJoinPlayers);
                            i.putExtra("CanJoinExtraPlayers", CanJoinExtraPlayers);
                            i.putExtra("UniqueName", UniqueName);
                            i.putExtra("contest_id", contest_id);
                            i.putExtra("JoinButtonFlag", JoinButtonFlag);
                            i.putExtra("game_id", game_id);
                            i.putExtra("main_game_name", main_game_name);
                            startActivity(i);
                        }
                    }
                } else {
                    if (UniqueName.equalsIgnoreCase("")) {
                        Intent i = new Intent(ActivityGameContestNew.this, ActivityDOB.class);
                        i.putExtra("game_name", game_name);
                        i.putExtra("user_game_id", userGameId);
                        i.putExtra("game_id", game_id);
                        i.putExtra("main_game_name", main_game_name);
                        startActivityForResult(i, Constants.PROFILE_UPDATE_STATUS_REQUEST_CODE);
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    } else if (!(Pref.getValue(ActivityGameContestNew.this, Constants.State, "", Constants.FILENAME).equalsIgnoreCase(""))
                            && !(Pref.getValue(ActivityGameContestNew.this, Constants.BirthDate, "", Constants.FILENAME).equalsIgnoreCase(""))) {
                        Intent i = new Intent(ActivityGameContestNew.this, ActivityAddBalance.class);
                        add_balance = topay - my_balance;
                        i.putExtra("amount_to_add", String.valueOf(add_balance));
                        i.putExtra("amount", String.valueOf(my_balance));
                        i.putExtra("game_id", game_id);
                        i.putExtra("game_name", game_name);
                        i.putExtra("game_type_id", game_type_id);
                        startActivity(i);
                    }
                }
                break;
            }
        }
    }
}
