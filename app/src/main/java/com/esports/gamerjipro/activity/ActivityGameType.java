package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.esports.gamerjipro.Adapter.GameTypeListAdapter;
import com.esports.gamerjipro.Model.AddChangeUsernameModel;
import com.esports.gamerjipro.Model.GameTypeGamesDataModel;
import com.esports.gamerjipro.Model.GameTypesModel;
import com.esports.gamerjipro.Model.StatisticsModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APICommonMethods;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.RestApis.WebFields;
import com.esports.gamerjipro.Utils.AppCallbackListener;
import com.esports.gamerjipro.Utils.AppUtil;
import com.esports.gamerjipro.Utils.Common;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.SessionManager;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityGameType extends BaseActivity implements AppCallbackListener.CallBackListener {

    private RelativeLayout mRelativeMain;
    private RelativeLayout mRelativeNoData;
    private RelativeLayout mRelativeSubmit;
    public String mLoginGameId, mGameName, mEditNewUsername, mUserGameId, mNewUsername;
    private ImageView mImageBack, mImageBottomSheetClose;
    public TextView mTextHeaderGameName;
    private TextView mTextMatchesPlayed, mTextLeaguesJoined, mTextWinningsDistributed, mTextAddChangeUsername, mTextClickHere,
            mTextBottomSheetHeader, mTextBottomSheetCurrentUsername, mTextBottomSheetNewUsername, mTextBottomSheetNote, mTextSubmit;
    private EditText mEditBottomSheetCurrentUsername, mEditBottomSheetNewUsername;
    private RecyclerView mRecyclerView;
    private GameTypeListAdapter mAdapterGameTypes;
    private ArrayList<GameTypesModel.Data.TypesData> mArrGameTypes;
    private BottomSheetBehavior mBottomSheetAddChangeUsername;
    private View mViewBottomSheetBackground;
    private LinearLayout mLinearCurrentUsername;
    private AppUtil mAppUtils;
    private SessionManager mSessionManager;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_type);
        mArrGameTypes = new ArrayList<>();
        mAppUtils = new AppUtil(ActivityGameType.this);
        mSessionManager = new SessionManager();

        getBundle();
        getIds();
        setRegListeners();
        setData();
        callToGetAllStatsAPI();
        callToGetAllGameTypesAPI();
    }

    /**
     * Get the keys from the home fragment
     */
    private void getBundle() {
        try {
            mLoginGameId = getIntent().getStringExtra("game_type");
            mGameName = getIntent().getStringExtra("game_name");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Relative Layouts
            mRelativeMain = findViewById(R.id.relative_game_type_main_view);
            mRelativeNoData = findViewById(R.id.relative_game_type_no_data);
            mRelativeSubmit = findViewById(R.id.relative_bottom_sheet_add_change_game_username_submit);
            RelativeLayout mRelativeBottomSheetAddChangeUsername = findViewById(R.id.relative_bottom_sheet_add_change_game_username_main);

            // Bottom Sheet Behaviours
            mBottomSheetAddChangeUsername = BottomSheetBehavior.from(mRelativeBottomSheetAddChangeUsername);

            // Image Views
            mImageBack = findViewById(R.id.image_game_type_back);
            mImageBottomSheetClose = findViewById(R.id.image_bottom_sheet_add_change_game_username_close);

            // Text Views
            mTextHeaderGameName = findViewById(R.id.text_game_type_game_name_header);
            mTextMatchesPlayed = findViewById(R.id.text_game_type_matches_played);
            mTextLeaguesJoined = findViewById(R.id.text_game_type_leagues_joined);
            mTextWinningsDistributed = findViewById(R.id.text_game_type_winnings_distributed);
            mTextBottomSheetHeader = findViewById(R.id.text_bottom_sheet_add_change_game_username_header);
            mTextBottomSheetCurrentUsername = findViewById(R.id.text_bottom_sheet_add_change_game_current_username);
            mTextBottomSheetNewUsername = findViewById(R.id.text_bottom_sheet_add_change_game_new_username);
            mTextBottomSheetNote = findViewById(R.id.text_bottom_sheet_add_change_game_username_note);
            mTextAddChangeUsername = findViewById(R.id.text_game_type_change_username);
            mTextClickHere = findViewById(R.id.text_game_type_click_here);
            mTextSubmit = findViewById(R.id.text_bottom_sheet_add_change_game_username_submit);

            // Edit Texts
            mEditBottomSheetCurrentUsername = findViewById(R.id.edit_bottom_sheet_add_change_game_current_username);
            mEditBottomSheetNewUsername = findViewById(R.id.edit_bottom_sheet_add_change_game_new_username);

            // View
            mViewBottomSheetBackground = findViewById(R.id.view_game_type_background);

            // Linear Layouts
            mLinearCurrentUsername = findViewById(R.id.linear_bottom_sheet_add_change_game_current_username);

            // Recycler View
            mRecyclerView = findViewById(R.id.recycler_view_game_types);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    @SuppressLint("SetTextI18n")
    private void setRegListeners() {
        try {
            // ToDo: On Click Listener
            mImageBack.setOnClickListener(clickListener);
            mTextClickHere.setOnClickListener(clickListener);
            mImageBottomSheetClose.setOnClickListener(clickListener);
            mRelativeSubmit.setOnClickListener(clickListener);

            // ToDo: How To Bottom Sheet Call Back
            mBottomSheetAddChangeUsername.setBottomSheetCallback(addChangeUsernameBottomSheetCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            mTextHeaderGameName.setText(mGameName);
            mTextAddChangeUsername.setText(getResources().getString(R.string.text_add_change_username) + " " +
                    mGameName + " " + getResources().getString(R.string.text_username) + "?");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        public void onClick(View v) {
            hideSoftKeyboard();
            switch (v.getId()) {
                case R.id.image_game_type_back:
                    finish();
                    break;

                case R.id.text_game_type_click_here:
                    callToGetGamesDataAPI();
                    break;

                case R.id.relative_bottom_sheet_add_change_game_username_submit:
                    doAddChangeUsername();
                    break;

                case R.id.image_bottom_sheet_add_change_game_username_close:
                    hideSoftKeyboard();
                    mEditBottomSheetNewUsername.setText("");
                    mBottomSheetAddChangeUsername.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    mViewBottomSheetBackground.setVisibility(View.GONE);

                    if (mViewBottomSheetBackground.getVisibility() == View.VISIBLE) {
                        mBottomSheetAddChangeUsername.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        mViewBottomSheetBackground.setVisibility(View.GONE);
                    } else {
                        mBottomSheetAddChangeUsername.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    }
                    break;
            }
        }
    };

    /**
     * Call back listener (Add Change Username)
     */
    BottomSheetBehavior.BottomSheetCallback addChangeUsernameBottomSheetCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            switch (newState) {
                case BottomSheetBehavior.STATE_COLLAPSED:
                    mViewBottomSheetBackground.setVisibility(View.GONE);
                    mEditBottomSheetNewUsername.setText("");
                    hideSoftKeyboard();
                    break;

                case BottomSheetBehavior.STATE_EXPANDED:
                    mViewBottomSheetBackground.setVisibility(View.VISIBLE);
                    mViewBottomSheetBackground.bringToFront();
                    break;
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    /**
     * This should get all the stats of the respected game
     */
    private void callToGetAllStatsAPI() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(ActivityGameType.this);
            progressDialog.setMessage("Please wait...."); // Setting Message
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
            progressDialog.show(); // Display Progress Dialog
            progressDialog.setCancelable(false);

            Call<StatisticsModel> gameTypesModelCall = APIClient.getClient().create(APIInterface.class).getStats(Constants.GET_STATISTICS, mLoginGameId);
            gameTypesModelCall.enqueue(new Callback<StatisticsModel>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(@NonNull Call<StatisticsModel> call, @NonNull Response<StatisticsModel> response) {
                    progressDialog.dismiss();

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        assert response.body() != null;
                        if (response.body().getStatus().equalsIgnoreCase(Constants.API_SUCCESS)) {
                            mTextMatchesPlayed.setText(response.body().getData().getContestsCreated());
                            mTextLeaguesJoined.setText(response.body().getData().getPlayersJoined());
                            mTextWinningsDistributed.setText(response.body().getData().getWinnigDistributions());
                        } else {
                            Constants.SnakeMessageYellow(mRelativeMain, response.body().getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<StatisticsModel> call, @NonNull Throwable t) {
                    progressDialog.dismiss();
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This should get all the respected game types of a particular game
     */
    private void callToGetAllGameTypesAPI() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(ActivityGameType.this);
            progressDialog.setMessage("Please wait...."); // Setting Message
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
            progressDialog.show(); // Display Progress Dialog
            progressDialog.setCancelable(false);

            Call<GameTypesModel> gameTypesModelCall = APIClient.getClient().create(APIInterface.class).getGameTypes("GetAllTypes",
                    mSessionManager.getUserId(ActivityGameType.this), mLoginGameId);
            gameTypesModelCall.enqueue(new Callback<GameTypesModel>() {
                @Override
                public void onResponse(@NonNull Call<GameTypesModel> call, @NonNull Response<GameTypesModel> response) {
                    progressDialog.dismiss();

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        assert response.body() != null;
                        if (response.body().getStatus().equalsIgnoreCase(Constants.API_SUCCESS)) {
                            ArrayList<GameTypesModel.Data.TypesData> gameTypesModels = (ArrayList<GameTypesModel.Data.TypesData>)
                                    response.body().getData().getTypesData();
                            if (mArrGameTypes != null && mArrGameTypes.size() > 0 &&
                                    mArrGameTypes != null) {
                                mArrGameTypes.addAll(gameTypesModels);
                                mAdapterGameTypes.notifyDataSetChanged();
                            } else {
                                mArrGameTypes = gameTypesModels;
                                setAdapterData();
                            }
                        } else {
                            Constants.SnakeMessageYellow(mRelativeMain, response.body().getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GameTypesModel> call, @NonNull Throwable t) {
                    progressDialog.dismiss();
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());

            mAdapterGameTypes = new GameTypeListAdapter(ActivityGameType.this, mArrGameTypes, mTextHeaderGameName.getText().toString());
            mRecyclerView.setAdapter(mAdapterGameTypes);

            setViewVisibility();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the view visibility as per the array size
     */
    private void setViewVisibility() {
        if (mArrGameTypes.size() != 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            mRelativeNoData.setVisibility(View.GONE);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            mRelativeNoData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        if (mViewBottomSheetBackground.getVisibility() == View.VISIBLE) {
            mBottomSheetAddChangeUsername.setState(BottomSheetBehavior.STATE_COLLAPSED);
            mViewBottomSheetBackground.setVisibility(View.GONE);
        } else {
            finish();
        }
    }

    /**
     * This should get all the games data
     */
    private void callToGetGamesDataAPI() {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setGameTypeGetGamesDataJson(mLoginGameId, mSessionManager.getUserId(ActivityGameType.this)));

            Call<GameTypeGamesDataModel> call = APIClient.getClient().create(APIInterface.class).getGameTypeGamesData(body);
            call.enqueue(new Callback<GameTypeGamesDataModel>() {
                @Override
                public void onResponse(@NonNull Call<GameTypeGamesDataModel> call, @NonNull Response<GameTypeGamesDataModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);

                        String mStatus = jsonObject.getString(WebFields.STATUS);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);

                        assert response.body() != null;
                        if (mStatus.equalsIgnoreCase(Constants.API_SUCCESS)) {
                            openBottomSheet(jsonObject);
                        } else {
                            Constants.SnakeMessageYellow(mRelativeMain, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GameTypeGamesDataModel> call, @NonNull Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This should open the bottom sheet for Add or Change username
     */
    @SuppressLint("SetTextI18n")
    private void openBottomSheet(JSONObject jsonObject) {
        try {
            JSONObject jsonData = jsonObject.getJSONObject(WebFields.DATA);
            mUserGameId = jsonData.getString(WebFields.GAME_TYPE_GET_GAMES_DATA.RESPONSE_USER_GAME_ID);
            String mNewGameName = jsonData.getString(WebFields.GAME_TYPE_GET_GAMES_DATA.RESPONSE_GAME_NAME);
            String mUniqueUsername = jsonData.getString(WebFields.GAME_TYPE_GET_GAMES_DATA.RESPONSE_UNIQUE_NAME);
            String mNote = jsonData.getString(WebFields.GAME_TYPE_GET_GAMES_DATA.RESPONSE_HELP_TEXT);

            mBottomSheetAddChangeUsername.setState(BottomSheetBehavior.STATE_EXPANDED);
            mViewBottomSheetBackground.setVisibility(View.VISIBLE);

            /*mArrGamesData = Constants.getUserData(ActivityGameType.this);
            String mGameId = "", mGameUsername = "";
            for (int i = 0; i < mArrGamesData.size(); i++) {
                mGameId = mArrGamesData.get(i).GameID;

                if (mLoginGameId.contains(mGameId)) {
                    mGameUsername = mArrGamesData.get(i).UniqueName;
                    mUserGameId = mArrGamesData.get(i).UserGameID;
                }
            }*/

            String mCurrent = getResources().getString(R.string.text_current);
            String mNew = getResources().getString(R.string.text_new);
            String mGameType = " " + mNewGameName + " " + getResources().getString(R.string.text_username);
            String mAddChangeGameType = getResources().getString(R.string.text_add_change) + mGameType;
            String mCurrentUsername = mCurrent + mGameType;
            mNewUsername = mNew + mGameType;

            mTextBottomSheetHeader.setText(mAddChangeGameType);
            mTextBottomSheetCurrentUsername.setText(mCurrentUsername);
            mTextBottomSheetNewUsername.setText(mNewUsername);

            if (mUniqueUsername.equalsIgnoreCase("")) {
                mLinearCurrentUsername.setVisibility(View.GONE);
                mEditBottomSheetCurrentUsername.setHint("Enter " + mCurrentUsername);
                mTextSubmit.setText(getResources().getString(R.string.action_submit));
            } else {
                mLinearCurrentUsername.setVisibility(View.VISIBLE);
                mEditBottomSheetCurrentUsername.setText(mUniqueUsername);
                mEditBottomSheetCurrentUsername.setFocusable(false);
                mTextSubmit.setText(getResources().getString(R.string.action_update));
            }
            mEditBottomSheetNewUsername.setHint("Enter " + mNewUsername);
            mTextBottomSheetNote.setText(mNote);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * This Method Check Validation Add Change Username
     */
    private void doAddChangeUsername() {
        try {
            hideSoftKeyboard();
            if (checkValidation()) {
                if (!mAppUtils.getConnectionState()) {
                    mAppUtils.displayNoInternetSnackBar(mRelativeMain, new AppCallbackListener(this));
                } else {
                    callToAddChangeUsernameAPI();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return - returns true if all validations are correct otherwise return false and shows error
     */
    private boolean checkValidation() {
        boolean status = true;

        mEditNewUsername = mEditBottomSheetNewUsername.getText().toString().trim();

        if (TextUtils.isEmpty(mEditNewUsername)) {
            Constants.SnakeMessageYellow(mRelativeMain, "Enter " + mNewUsername);

            status = false;
        }
        return status;
    }

    /**
     * This should add or change username
     */
    private void callToAddChangeUsernameAPI() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(ActivityGameType.this);
            progressDialog.setMessage("Please wait...."); // Setting Message
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
            progressDialog.show(); // Display Progress Dialog
            progressDialog.setCancelable(false);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setAddChangeUsernameJson(mUserGameId, mLoginGameId, mSessionManager.getUserId(ActivityGameType.this), mEditNewUsername));

            Call<AddChangeUsernameModel> call = APIClient.getClient().create(APIInterface.class).addChangeUsername(body);
            call.enqueue(new Callback<AddChangeUsernameModel>() {
                @Override
                public void onResponse(@NonNull Call<AddChangeUsernameModel> call, @NonNull Response<AddChangeUsernameModel> response) {
                    progressDialog.dismiss();

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);

                        String mStatus = jsonObject.getString(WebFields.STATUS);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);

                        mEditBottomSheetNewUsername.setText("");
                        mBottomSheetAddChangeUsername.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        mViewBottomSheetBackground.setVisibility(View.GONE);
                        assert response.body() != null;
                        if (mStatus.equalsIgnoreCase(Constants.API_SUCCESS)) {
                            Constants.SnakeMessageYellow(mRelativeMain, mMessage);
                        } else {
                            Constants.SnakeMessageYellow(mRelativeMain, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddChangeUsernameModel> call, @NonNull Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAppCallback(int Code) {
        doAddChangeUsername();
    }
}
