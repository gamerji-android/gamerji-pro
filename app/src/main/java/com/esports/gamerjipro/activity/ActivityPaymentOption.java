package com.esports.gamerjipro.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.esports.gamerjipro.R;

public class ActivityPaymentOption extends AppCompatActivity
{
    CardView cv_wallet,cv_net_banking,cv_card,cv_upi;
   String amount;
   TextView txt_title,tv_wallet_balance;
   ImageView img_back;
    String game_id,game_type_id,game_name;
    boolean is_tournament=false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_option);
        cv_card=findViewById(R.id.cv_card);
        cv_wallet=findViewById(R.id.cv_wallet);
        txt_title=findViewById(R.id.txt_title);
        cv_upi=findViewById(R.id.cv_upi);
        tv_wallet_balance=findViewById(R.id.tv_wallet_balance);
        txt_title.setText("Payment OPtions");
        amount=getIntent().getStringExtra("amount");
        tv_wallet_balance.setText(amount);
        cv_net_banking=findViewById(R.id.cv_net_banking);
        img_back=findViewById(R.id.img_back);

        if (getIntent().getExtras()!=null)
        {
            game_id = getIntent().getStringExtra("game_id");
            game_name = getIntent().getStringExtra("game_name");
            game_type_id = getIntent().getStringExtra("game_type_id");
            is_tournament=getIntent().getBooleanExtra("is_tournament",false);
        }

        img_back.setOnClickListener(v -> finish());
        cv_card.setOnClickListener(v ->
        {
            Intent i = new Intent(ActivityPaymentOption.this,ActivityCardDetails.class);
            i.putExtra("amount",amount);
            i.putExtra("game_id",game_id);
            i.putExtra("game_name",game_name);
            i.putExtra("game_type_id",game_type_id);
            i.putExtra("is_tournament",is_tournament);
            startActivity(i);
        });

        cv_net_banking.setOnClickListener(v ->
        {
            Intent i = new Intent(ActivityPaymentOption.this,ActivityNetbanking.class);
            i.putExtra("amount",amount);
            i.putExtra("game_id",game_id);
            i.putExtra("game_name",game_name);
            i.putExtra("game_type_id",game_type_id);
            i.putExtra("current_amount",getIntent().getStringExtra("current_balance"));
            i.putExtra("is_tournament",is_tournament);
            startActivity(i);
        });

        cv_wallet.setOnClickListener(v ->
        {
            Intent i = new Intent(ActivityPaymentOption.this,ActivityWalletPayment.class);
            i.putExtra("amount",amount);
            i.putExtra("game_id",game_id);
            i.putExtra("game_name",game_name);
            i.putExtra("game_type_id",game_type_id);
            i.putExtra("current_amount",getIntent().getStringExtra("current_balance"));
            i.putExtra("is_tournament",is_tournament);
            startActivity(i);
        });

        cv_upi.setOnClickListener(v ->
        {
            Intent i = new Intent(ActivityPaymentOption.this,ActivityUpiPayment.class);
            i.putExtra("amount",amount);
            i.putExtra("game_id",game_id);
            i.putExtra("game_name",game_name);
            i.putExtra("game_type_id",game_type_id);
            i.putExtra("is_tournament",is_tournament);
            startActivity(i);
        });


    }


}
