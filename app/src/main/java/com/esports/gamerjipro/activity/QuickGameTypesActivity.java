package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.esports.gamerjipro.Adapter.QuickGameTypesAdapter;
import com.esports.gamerjipro.Model.QuickGameTypesModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.RestApis.WebFields;
import com.esports.gamerjipro.Utils.AppConstants;
import com.esports.gamerjipro.Utils.Common;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.SessionManager;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuickGameTypesActivity extends AppCompatActivity {

    private RelativeLayout mRelativeMain, mRelativeNoData;
    private ImageView mImageBack;
    private TextView mTextGameTitle;
    private RecyclerView mRecyclerView;
    private String mGameType;
    private ArrayList<QuickGameTypesModel.Data.CategoriesData> mArrQuickGameTypes;
    private QuickGameTypesAdapter mAdapterQuickGameTypes;
    private SessionManager mSessionManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_game_types);
        mArrQuickGameTypes = new ArrayList<>();
        mSessionManager = new SessionManager();

        getBundle();
        getIds();
        setRegListeners();
        setData();
        callToGetHTMLGameTypesAPI();
    }

    /**
     * Get the keys from the more fragment
     */
    private void getBundle() {
        try {
            mGameType = getIntent().getStringExtra(AppConstants.BUNDLE_QUICK_GAME_TYPE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Relative Layouts
            mRelativeMain = findViewById(R.id.relative_quick_game_types_main_view);
            mRelativeNoData = findViewById(R.id.relative_quick_game_types_no_data);

            // Image View
            mImageBack = findViewById(R.id.image_quick_game_types_back);

            mTextGameTitle = findViewById(R.id.text_quick_game_types_title);

            // Recycler View
            mRecyclerView = findViewById(R.id.recycler_view_quick_game_types);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: On Click Listener
            mImageBack.setOnClickListener(clickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data
     */
    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            mTextGameTitle.setText(mGameType);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.image_quick_game_types_back:
                    finish();
                    break;
            }
        }
    };

    /**
     * This method should get all the HTML 5 Game Types API
     */
    private void callToGetHTMLGameTypesAPI() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(QuickGameTypesActivity.this);
            progressDialog.setMessage("Please wait...."); // Setting Message
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
            progressDialog.show(); // Display Progress Dialog
            progressDialog.setCancelable(false);

            String mUserId = mSessionManager.getUserId(QuickGameTypesActivity.this);

            Call<QuickGameTypesModel> callRepos = APIClient.getClient().create(APIInterface.class).getQuickGameTypes(WebFields.QUICK_GAME_TYPES.MODE, mUserId);
            callRepos.enqueue(new Callback<QuickGameTypesModel>() {
                @Override
                public void onResponse(@NonNull Call<QuickGameTypesModel> call, @NonNull Response<QuickGameTypesModel> response) {
                    progressDialog.dismiss();

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        assert response.body() != null;
                        if (response.body().getStatus().equalsIgnoreCase(Constants.API_SUCCESS)) {
                            ArrayList<QuickGameTypesModel.Data.CategoriesData> gameTypesModels = (ArrayList<QuickGameTypesModel.Data.CategoriesData>)
                                    response.body().getData().getCategoriesData();

                            if (mArrQuickGameTypes != null && mArrQuickGameTypes.size() > 0 &&
                                    mAdapterQuickGameTypes != null) {
                                mArrQuickGameTypes.addAll(gameTypesModels);
                                mAdapterQuickGameTypes.notifyDataSetChanged();
                            } else {
                                mArrQuickGameTypes = gameTypesModels;
                                setAdapterData();
                            }
                        } else {
                            setAdapterData();
                            Constants.SnakeMessageYellow(mRelativeMain, response.body().getMessage());
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<QuickGameTypesModel> call, @NonNull Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());

            mAdapterQuickGameTypes = new QuickGameTypesAdapter(QuickGameTypesActivity.this, mArrQuickGameTypes);
            mRecyclerView.setAdapter(mAdapterQuickGameTypes);

            setViewVisibility();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the view visibility as per the array size
     */
    private void setViewVisibility() {
        if (mArrQuickGameTypes.size() != 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            mRelativeNoData.setVisibility(View.GONE);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            mRelativeNoData.setVisibility(View.VISIBLE);
        }
    }
}