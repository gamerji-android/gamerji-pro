package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.esports.gamerjipro.Model.AccountDetailsModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.AnalyticsSocket;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.Pref;
import com.esports.gamerjipro.Utils.SessionManager;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityWallet extends AppCompatActivity implements View.OnClickListener {
    TextView txt_deposited, txt_withdraw, txt_winnings, txt_bonus, txt_balance, txt_name, txt_title, txt_bottom_msg;
    ImageView img_user;
    CardView cv_email_mobile, cv_pan, cv_bank, cv_recent_transaction, cv_manage_payments, cv_add_balance, cv_withdraw;
    APIInterface apiInterface;
    RelativeLayout lyt_recent_transaction;
    BottomSheetBehavior sheetBehavior;
    RelativeLayout layoutBottomSheet;
    View bg;
    CoordinatorLayout parent_layout;
    ImageView img_close, img_info1, img_info2, img_info3, img_back;
    String email_verfified, phone_verified, pan_verified, bank_verified, pan_number, upi_verified;
    AccountDetailsModel.Data.BankData bankData;
    AccountDetailsModel.Data.PanData panData;
    AccountDetailsModel.Data.UPIData upiData;


    ImageView img_verified_email, img_verified_pan, img_verified_bank;
    TextView txt_reason_email, txt_reason_pan, txt_reason_account;
    WebView webView;
    private int currentPage = 0;
    Timer swipeTimer;
    private int NUM_PAGES = 0;
    AnalyticsSocket analyticsSocket = new AnalyticsSocket();
    boolean sendSwipeImpression = true, isWithdrawal;
    LinearLayout adContainer;
    String min_amount = "";
    String max_amount = "";
    private SessionManager mSessionManager;

    @SuppressLint({"SetTextI18n", "SetJavaScriptEnabled"})
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_account);
        mSessionManager = new SessionManager();
        txt_deposited = findViewById(R.id.txt_deposited);
        img_back = findViewById(R.id.img_back);
        parent_layout = findViewById(R.id.parent_layout);
        txt_title = findViewById(R.id.txt_title_bs);
        txt_bottom_msg = findViewById(R.id.txt_bottom_msg);
        img_back.setOnClickListener(v -> finish());
        lyt_recent_transaction = findViewById(R.id.lyt_recent_transaction);
        txt_withdraw = findViewById(R.id.txt_withdraw);
        txt_winnings = findViewById(R.id.txt_winnings);
        txt_bonus = findViewById(R.id.txt_bonus);
        cv_withdraw = findViewById(R.id.cv_withdraw);
        txt_balance = findViewById(R.id.txt_balance);
        txt_name = findViewById(R.id.txt_name);
        img_user = findViewById(R.id.img_user);
        cv_email_mobile = findViewById(R.id.cv_email_mobile);
        cv_pan = findViewById(R.id.cv_pan);
        cv_bank = findViewById(R.id.cv_bank);
        swipeTimer = new Timer();
        img_verified_email = findViewById(R.id.img_verified_email);
        img_verified_pan = findViewById(R.id.img_verified_pan);
        img_verified_bank = findViewById(R.id.img_verified_bank);
        txt_reason_email = findViewById(R.id.txt_reason_email);
        txt_reason_pan = findViewById(R.id.txt_reason_pan);
        txt_reason_account = findViewById(R.id.txt_reason_account);


        cv_recent_transaction = findViewById(R.id.cv_recent_transaction);
        cv_manage_payments = findViewById(R.id.cv_manage_payments);
        cv_add_balance = findViewById(R.id.cv_add_balance);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        layoutBottomSheet = findViewById(R.id.bottom_how_to);

        webView = findViewById(R.id.wv_youtube);
        webView.setWebChromeClient(new WebChromeClient());
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setDefaultFontSize(18);
        webView.loadUrl("https://www.youtube.com/embed/Q4X7ftXxNZI");

        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);

        img_close = findViewById(R.id.img_close);
        bg = findViewById(R.id.bg);

        img_info1 = findViewById(R.id.img_info1);
        img_info2 = findViewById(R.id.img_info2);
        img_info3 = findViewById(R.id.img_info3);
        txt_name.setText(Pref.getValue(this, Constants.firstname, "", Constants.FILENAME) + " " + Pref.getValue(this, Constants.lastname, "", Constants.FILENAME));

        cv_email_mobile.setOnClickListener(this);
        cv_pan.setOnClickListener(this);
        cv_bank.setOnClickListener(this);
        cv_recent_transaction.setOnClickListener(this);
        cv_manage_payments.setOnClickListener(this);
        cv_add_balance.setOnClickListener(this);
        lyt_recent_transaction.setOnClickListener(this);
        img_close.setOnClickListener(this);

        img_info1.setOnClickListener(this);
        img_info2.setOnClickListener(this);
        img_info3.setOnClickListener(this);
        cv_withdraw.setOnClickListener(this);

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        bg.setVisibility(View.GONE);
                        break;
                    }

                    case BottomSheetBehavior.STATE_EXPANDED: {
                        bg.setVisibility(View.VISIBLE);
                        break;
                    }
                    case BottomSheetBehavior.STATE_DRAGGING: {
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {


            }
        });
    }

    @Override
    protected void onResume() {
        getData();
        super.onResume();
    }

    private void getData() {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityWallet.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        Call<AccountDetailsModel> gameTypesModelCall = apiInterface.getAccount(Constants.WALLEET_VERIFICATION_INFO,
                Pref.getValue(ActivityWallet.this, Constants.UserID, "", Constants.FILENAME));

        gameTypesModelCall.enqueue(new Callback<AccountDetailsModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<AccountDetailsModel> call, @NonNull Response<AccountDetailsModel> response) {
                progressDialog.dismiss();
                AccountDetailsModel accountDetailsModel = response.body();
                assert accountDetailsModel != null;
                if (accountDetailsModel.status.equalsIgnoreCase("success")) {
                    txt_deposited.setText(" " + accountDetailsModel.DataClass.DepositedBalance);
                    txt_winnings.setText(" " + accountDetailsModel.DataClass.WinningBalance);
                    txt_bonus.setText(" " + accountDetailsModel.DataClass.BonusBalance);
                    txt_balance.setText(accountDetailsModel.DataClass.TotalBalance);
                    email_verfified = accountDetailsModel.DataClass.EmailVerified;
                    upi_verified = accountDetailsModel.DataClass.UPIVerified;
                    min_amount = accountDetailsModel.DataClass.WithdrawalMinAmount;
                    max_amount = accountDetailsModel.DataClass.WithdrawalMaxAmount;
                    isWithdrawal = accountDetailsModel.DataClass.WithdrawalEnabled;
                    cv_withdraw.setVisibility(View.VISIBLE);
                    cv_bank.setVisibility(View.VISIBLE);

                    NUM_PAGES = accountDetailsModel.DataClass.AdsCount;
                    final Handler handler = new Handler();
                    final Runnable Update = () ->
                    {
                        if (currentPage == NUM_PAGES) {
                            currentPage = 0;
                            sendSwipeImpression = false;
                            //swipeTimer.cancel();
                        } else {
                            if (sendSwipeImpression) {
                                analyticsSocket.sendAnalytics(accountDetailsModel.DataClass.adsDataArrayList.get(currentPage).AdType, accountDetailsModel.DataClass.adsDataArrayList.get(currentPage).AdID,
                                        accountDetailsModel.DataClass.adsDataArrayList.get(currentPage).AdImageName, Constants.AD_IMPRESSION, Pref.getValue(ActivityWallet.this, Constants.UserID, "", Constants.FILENAME),
                                        0);
                            }
                        }
                    };

                    swipeTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            handler.post(Update);
                        }
                    }, 0, 5000);

                    if (accountDetailsModel.DataClass.EmailVerified.equalsIgnoreCase("2") &&
                            accountDetailsModel.DataClass.MobileVerified.equalsIgnoreCase("2")) {
                        img_verified_email.setVisibility(View.VISIBLE);
                        txt_reason_email.setText("VERIFIED");
                        txt_reason_email.setTextColor(getResources().getColor(R.color.green_color));
                    } else if (accountDetailsModel.DataClass.EmailVerified.equalsIgnoreCase("3")) {
                        img_verified_email.setVisibility(View.VISIBLE);
                        txt_reason_email.setText("PENDING");
                        txt_reason_email.setTextColor(getResources().getColor(R.color.orange_color));
                    } else if (accountDetailsModel.DataClass.MobileVerified.equalsIgnoreCase("3")) {
                        img_verified_email.setVisibility(View.VISIBLE);
                        txt_reason_email.setText("PENDING");
                        txt_reason_email.setTextColor(getResources().getColor(R.color.orange_color));
                    }


                    if (accountDetailsModel.DataClass.BankVerified.equalsIgnoreCase("2")) {
                        img_verified_bank.setVisibility(View.VISIBLE);
                        txt_reason_account.setText("VERIFIED");
                        txt_reason_account.setTextColor(getResources().getColor(R.color.green_color));
                    } else if (accountDetailsModel.DataClass.BankVerified.equalsIgnoreCase("3")) {
                        txt_reason_account.setText("PENDING");
                        txt_reason_account.setTextColor(getResources().getColor(R.color.orange_color));
                    } else if (accountDetailsModel.DataClass.BankVerified.equalsIgnoreCase("4")) {
                        txt_reason_account.setText("REJECTED");
                        txt_reason_account.setTextColor(Color.RED);
                    }


                    if (accountDetailsModel.DataClass.PanVerified.equalsIgnoreCase("2")) {
                        img_verified_pan.setVisibility(View.VISIBLE);
                        txt_reason_pan.setText("VERIFIED");
                        txt_reason_pan.setTextColor(getResources().getColor(R.color.green_color));
                    } else if (accountDetailsModel.DataClass.PanVerified.equalsIgnoreCase("3")) {
                        txt_reason_pan.setText("PENDING");
                        txt_reason_pan.setTextColor(getResources().getColor(R.color.orange_color));
                    } else if (accountDetailsModel.DataClass.PanVerified.equalsIgnoreCase("4")) {
                        txt_reason_pan.setText("REJECTED");
                        txt_reason_pan.setTextColor(Color.RED);
                    }

                    phone_verified = accountDetailsModel.DataClass.MobileVerified;
                    bank_verified = accountDetailsModel.DataClass.BankVerified;
                    pan_verified = accountDetailsModel.DataClass.PanVerified;
                    pan_number = accountDetailsModel.DataClass.PanData.PanNumber;
                    txt_balance.setText(accountDetailsModel.DataClass.TotalBalance);
                    txt_balance.setText(accountDetailsModel.DataClass.TotalBalance);
                    bankData = accountDetailsModel.DataClass.BankData;
                    panData = accountDetailsModel.DataClass.PanData;
                    upiData = accountDetailsModel.DataClass.UPIData;
                } else {
                    Constants.SnakeMessageYellow(parent_layout, accountDetailsModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<AccountDetailsModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }

        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cv_email_mobile: {
                Intent i = new Intent(ActivityWallet.this, ActivityEmailPhoneVerification.class);
                i.putExtra("email_verified", email_verfified);
                i.putExtra("phone_verified", phone_verified);
                startActivity(i);
                break;
            }

            case R.id.cv_pan: {
                if (email_verfified.equalsIgnoreCase("2")) {
                    Intent i = new Intent(ActivityWallet.this, ActivityUploadPan.class);
                    i.putExtra("panData", panData);
                    i.putExtra("pan_verified", pan_verified);
                    startActivity(i);
                } else
                    Constants.SnakeMessageYellow(parent_layout, getResources().getString(R.string.email_verification));

                break;
            }
            case R.id.cv_bank: {
               /* if (pan_verified.equalsIgnoreCase("2"))
                {*/

                Intent i = new Intent(ActivityWallet.this, ActivityWithdraw.class);
                i.putExtra("amount", txt_winnings.getText().toString());
                i.putExtra("bankdata", bankData);
                i.putExtra("bank_verified", bank_verified);
                i.putExtra(Constants.UPI_DATA, upiData);
                i.putExtra(Constants.UPI_Verified, upi_verified);
                i.putExtra(Constants.MIN_WITHDRAW, min_amount);
                i.putExtra(Constants.MAX_WITHDRAW, max_amount);
                startActivity(i);

                        /*Intent i = new Intent(ActivityWallet.this,ActivityUploadBankDetails.class);
                        i.putExtra("bankdata", bankData);
                        i.putExtra("bank_verified", bank_verified);
                        startActivity(i);*/
                /*}
                else
                Constants.SnakeMessageYellow(parent_layout, getResources().getString(R.string.pan_verification));*/
                break;
            }
            case R.id.cv_add_balance: {
                if (Pref.getValue(ActivityWallet.this, Constants.State, "", Constants.FILENAME).isEmpty() ||
                        Pref.getValue(ActivityWallet.this, Constants.BirthDate, "", Constants.FILENAME).isEmpty()) {
                    Intent i = new Intent(ActivityWallet.this, ActivityDob_State.class);
                    startActivity(i);
                } else {
                    Intent i = new Intent(ActivityWallet.this, ActivityAddBalance.class);
                    i.putExtra("amount", txt_balance.getText().toString());
                    startActivity(i);
                }

                break;

            }
            case R.id.lyt_recent_transaction: {
                Intent i = new Intent(ActivityWallet.this, ActivityRecentTransactions.class);
                startActivity(i);
                break;

            }

            case R.id.img_close: {
                bg.setVisibility(View.GONE);
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                break;
            }

            case R.id.img_info1: {
                txt_title.setText(R.string.deposited);
                txt_bottom_msg.setText(R.string.deposite_msg);
                bg.setVisibility(View.VISIBLE);
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;
            }

            case R.id.img_info2: {
                txt_title.setText(R.string.winnings);
                txt_bottom_msg.setText(R.string.winnings_msg);
                bg.setVisibility(View.VISIBLE);
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;
            }

            case R.id.img_info3: {
                txt_title.setText(R.string.bonus);
                txt_bottom_msg.setText(R.string.bonus_msg);
                bg.setVisibility(View.VISIBLE);
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;
            }
            case R.id.cv_withdraw: {
                if (email_verfified.equalsIgnoreCase("2") && phone_verified.equalsIgnoreCase("2")) {
                    Intent i = new Intent(ActivityWallet.this, ActivityWithdraw.class);
                    i.putExtra("amount", txt_winnings.getText().toString());
                    i.putExtra("bankdata", bankData);
                    i.putExtra("bank_verified", bank_verified);
                    i.putExtra(Constants.UPI_DATA, upiData);
                    i.putExtra(Constants.UPI_Verified, upi_verified);
                    i.putExtra(Constants.MIN_WITHDRAW, min_amount);
                    i.putExtra(Constants.MAX_WITHDRAW, max_amount);
                    startActivity(i);
                } else {
                    Constants.SnakeMessageYellow(parent_layout, getResources().getString(R.string.withdraw_verification));
                }
                break;
            }
        }

    }
}