/*
package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSettings;
import com.facebook.ads.AdSize;
import com.facebook.ads.AudienceNetworkAds;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Timer;

import com.esports.gamerjipro.Adapter.DynamicGameContestAdapter;
import com.esports.gamerjipro.Adapter.ItemFeedbackAdapter;
import com.esports.gamerjipro.Adapter.PoolPriceAdapter;
import com.esports.gamerjipro.Adapter.WinnerPoolAdapter;
import com.esports.gamerjipro.Fragment.CRContestTabFragment;
import com.esports.gamerjipro.Fragment.PubgContestTabFragment;
import com.esports.gamerjipro.Interface.RulesClickListener;
import com.esports.gamerjipro.Interface.WinnerClickListener;
import com.esports.gamerjipro.Model.ContestTypeModelNew;
import com.esports.gamerjipro.Model.ContestTypesModel;
import com.esports.gamerjipro.Model.MyContestModel;
import com.esports.gamerjipro.Model.RulesModel;
import com.esports.gamerjipro.Model.TournamentDetailModel;
import com.esports.gamerjipro.Model.TournamentTypeModel;
import com.esports.gamerjipro.Model.WinnerContestModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.AnalyticsSocket;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.Pref;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.esports.gamerjipro.Utils.Constants.gdataArrayList;
import static com.esports.gamerjipro.Utils.FileUtils.OpenNewsWebview;

public class ActivityMyContestNew extends AppCompatActivity implements WinnerClickListener, RulesClickListener
{
    APIInterface apiInterface;
    TextView txt_title,txt_title_bs,txt_bottom_msg;
    ImageView img_back,img_close,iv_close_pool_price;
    private TabLayout tabLayout;
    BottomSheetBehavior pool_price_bottomsheet,rules_bottomsheet;
    View bg;
    public LinearLayout pool_price_bottom;
    RecyclerView rv_pool_price;
    TextView tv_pool_price,txt_winning_rupee,txt_pool_price;
    public CoordinatorLayout parent_layout;
    WinnerPoolAdapter winnerPoolAdapter ;
    TextView img_news;
    RelativeLayout bottom_how_to;

    public ViewPager slider_pager_ads;
    public AnalyticsSocket analyticsSocket = new AnalyticsSocket();
    public boolean sendSwipeImpression= true;
   public  View lyt_ads;
   public  LinearLayout adContainer;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_my_contest);

        img_news=findViewById(R.id.img_news);
        lyt_ads=findViewById(R.id.lyt_ads);
        img_news.setOnClickListener(v -> OpenNewsWebview(ActivityMyContestNew.this,"https://www.gamerji.com"));

        tabLayout = findViewById(R.id.tabLayout);
        ViewPager viewPager = findViewById(R.id.viewPager);
        slider_pager_ads = findViewById(R.id.slider_pager_ads);

        tv_pool_price = findViewById(R.id.tv_pool_price);
        txt_winning_rupee = findViewById(R.id.txt_winning_rupee);
        txt_pool_price = findViewById(R.id.txt_pool_price);
        parent_layout = findViewById(R.id.parent_layout);
        //  img_notification = findViewById(R.id.img_notification);
        img_back = findViewById(R.id.img_back);
        txt_title = findViewById(R.id.txt_title);


        bottom_how_to = findViewById(R.id.bottom_how_to);
        img_close = findViewById(R.id.img_close);
        iv_close_pool_price = findViewById(R.id.iv_close_pool_price);


        txt_title_bs=findViewById(R.id.txt_title_bs);
        txt_bottom_msg=findViewById(R.id.txt_bottom_msg);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        rv_pool_price=findViewById(R.id.rv_pool_price);
        bg=findViewById(R.id.bg);
        img_back.setOnClickListener(v -> finish());

        pool_price_bottom=findViewById(R.id.pool_price_bottom);
        pool_price_bottomsheet = BottomSheetBehavior.from(pool_price_bottom);
        rules_bottomsheet = BottomSheetBehavior.from(bottom_how_to);

        pool_price_bottomsheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback()
        {
            @Override
            public void onStateChanged(@NonNull View view, int newState)
            {
                switch (newState)
                {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                    {
                        bg.setVisibility(View.VISIBLE);
                        break;
                    }

                    case BottomSheetBehavior.STATE_COLLAPSED:
                    {
                        bg.setVisibility(View.GONE);
                        break;
                    }

                    case BottomSheetBehavior.STATE_DRAGGING:

                    case BottomSheetBehavior.STATE_SETTLING:
                    {
                        break;
                    }
                    case BottomSheetBehavior.STATE_HALF_EXPANDED:
                        break;
                }

            }

            @Override
            public void onSlide(@NonNull View view, float v)
            {

            }
        });
        img_close.setOnClickListener(v -> rules_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED));
        iv_close_pool_price.setOnClickListener(v -> pool_price_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED));

        rules_bottomsheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback()
        {
            @Override
            public void onStateChanged(@NonNull View view, int newState)
            {
                switch (newState)
                {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                    {
                        bg.setVisibility(View.VISIBLE);
                    }
                    break;

                    case BottomSheetBehavior.STATE_COLLAPSED:
                    {
                        bg.setVisibility(View.GONE);
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:

                    case BottomSheetBehavior.STATE_SETTLING:
                    {
                        break;
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v)
            {

            }
        });

        setupViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager)
    {
        if(gdataArrayList.size()>0)
        {
            for (int k = 0; k <gdataArrayList.size(); k++)
            {
                tabLayout.addTab(tabLayout.newTab().setText(gdataArrayList.get(k).Name));
            }

            DynamicGameContestAdapter adapter = new DynamicGameContestAdapter
                    (getSupportFragmentManager(), gdataArrayList.size(), gdataArrayList);
            viewPager.setAdapter(adapter);
            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            tabLayout.setTabTextColors(ColorStateList.valueOf(Color.WHITE));
            tabLayout.setSelectedTabIndicatorHeight(10);
            tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.orange_color));

        if (tabLayout.getTabCount() == 2)
        {
            tabLayout.setTabMode(TabLayout.MODE_FIXED);
        }
        else
        {
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        }

            */
/*int wantedTabIndex = 0;
            Typeface font = null;
            font = ResourcesCompat.getFont(ActivityMyContestNew.this,R.font.poppins_semibold);
            TextView tv = (TextView)(((LinearLayout)((LinearLayout)tabLayout.getChildAt(0)).getChildAt(wantedTabIndex)).getChildAt(1));
            tv.setTypeface(font);*//*


            for (int i = 0; i < tabLayout.getTabCount(); i++)
            {

                TabLayout.Tab tab = tabLayout.getTabAt(i);
                if (tab != null)
                {
                    TextView tabTextView = new TextView(this);
                    if (i == 0)
                    {
                        Typeface font = null;
                        font = ResourcesCompat.getFont(ActivityMyContestNew.this,R.font.poppins_semibold);
                        tabTextView.setTypeface(font);
                    }
                }

            }

            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener()
            {
                @Override
                public void onTabSelected(TabLayout.Tab tab)
                {

                    ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
                    ViewGroup vgTab = (ViewGroup) vg.getChildAt(tab.getPosition());
                    int tabChildsCount = vgTab.getChildCount();
                    for (int i = 0; i < tabChildsCount; i++)
                    {
                        View tabViewChild = vgTab.getChildAt(i);
                        if (tabViewChild instanceof TextView)
                        {
                            Typeface font = null;
                            font = ResourcesCompat.getFont(ActivityMyContestNew.this,R.font.poppins_semibold);
                            ((TextView) tabViewChild).setTypeface(font);
                            ((TextView) tabViewChild).setTextColor(Color.WHITE);
                        }

                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab)
                {
                    ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
                    ViewGroup vgTab = (ViewGroup) vg.getChildAt(tab.getPosition());
                    int tabChildsCount = vgTab.getChildCount();
                    for (int i = 0; i < tabChildsCount; i++)
                    {
                        View tabViewChild = vgTab.getChildAt(i);
                        if (tabViewChild instanceof TextView)
                        {
                            Typeface font = null;
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                            {
                                font = getResources().getFont(R.font.poppins);
                                ((TextView) tabViewChild).setTypeface(font);
                            }
                            ((TextView) tabViewChild).setTextColor(Color.WHITE);
                        }
                    }
                }

                @Override
                public void onTabReselected(TabLayout.Tab tab)
                { }
            });
        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener()
        {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }

    @Override
    public void onWinnersClick(ContestTypesModel.Data.TypesData.ContestsData contestsData)
    {}

    @Override
    public void onWinnersClick(ContestTypeModelNew.Data.ContestsData contestsData) {

    }

    private void getWinnerPool(MyContestModel.Data.ContestsData contestsData)
    {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityMyContestNew.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        if (contestsData.Type.equalsIgnoreCase("1"))
        {
            Call<WinnerContestModel> contestDetailsModelCall = apiInterface.getContestWinner(Constants.GETSINGLECONTESTWINNER,contestsData.ContestID);
            contestDetailsModelCall.enqueue(new Callback<WinnerContestModel>()
            {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(@NonNull Call<WinnerContestModel> call, @NonNull Response<WinnerContestModel> response)
                {
                    progressDialog.dismiss();
                    WinnerContestModel winnerContestModel = response.body();
                    assert winnerContestModel != null;
                    if (winnerContestModel.status.equalsIgnoreCase("success"))
                    {
                        winnerPoolAdapter= new WinnerPoolAdapter(ActivityMyContestNew.this,winnerContestModel.data.prizePoolsData);
                        rv_pool_price.setAdapter(winnerPoolAdapter);
                        pool_price_bottomsheet.setState(BottomSheetBehavior.STATE_EXPANDED);
                        bg.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        Constants.SnakeMessageYellow(parent_layout,winnerContestModel.message);
                    }
                }
                @Override
                public void onFailure(@NonNull Call<WinnerContestModel> call, @NonNull Throwable t)
                {
                    progressDialog.dismiss();
                    t.printStackTrace();
                }
            });
        }
        else
        {
            Call<WinnerContestModel> contestDetailsModelCall = apiInterface.getTournamentWinners(Constants.GET_TOURNAMENT_WINNING_POOL,contestsData.TournamentID);

            contestDetailsModelCall.enqueue(new Callback<WinnerContestModel>()
            {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(@NonNull Call<WinnerContestModel> call, @NonNull Response<WinnerContestModel> response)
                {
                    progressDialog.dismiss();
                    WinnerContestModel winnerContestModel = response.body();
                    assert winnerContestModel != null;
                    if (winnerContestModel.status.equalsIgnoreCase("success"))
                    {
                        winnerPoolAdapter= new WinnerPoolAdapter(ActivityMyContestNew.this,winnerContestModel.data.prizePoolsData);
                        rv_pool_price.setAdapter(winnerPoolAdapter);
                        pool_price_bottomsheet.setState(BottomSheetBehavior.STATE_EXPANDED);
                        bg.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        Constants.SnakeMessageYellow(parent_layout,winnerContestModel.message);
                    }
                }
                @Override
                public void onFailure(@NonNull Call<WinnerContestModel> call, @NonNull Throwable t)
                {
                    progressDialog.dismiss();
                    t.printStackTrace();
                }
            });
        }

    }

    @Override
    public void onMyContestWinnerClick(MyContestModel.Data.ContestsData contestsData)
    {
        tv_pool_price.setText(contestsData.WinningAmount);
        if (contestsData.WinningAmount.equalsIgnoreCase("") || contestsData.WinningAmount.equalsIgnoreCase("0"))
        {
            txt_pool_price.setVisibility(View.GONE);
            txt_winning_rupee.setVisibility(View.GONE);
        }
        else
        {
            txt_pool_price.setVisibility(View.VISIBLE);
            txt_winning_rupee.setVisibility(View.VISIBLE);
        }
        getWinnerPool(contestsData);
    }

    @Override
    public void onTournamentWinnerClick(TournamentTypeModel.Data.TypesData.TournamentsData tournamentsData) {}

    @Override
    public void onTournamentContestWinnersClickListener(TournamentDetailModel.TournamentData.ContestsData contestsData) {}

    @Override
    public void OnRulesClickListener(TournamentTypeModel.Data.TypesData.TournamentsData tournamentsData){}

    @Override
    public void OnMyContestRulesClickListener(MyContestModel.Data.ContestsData contestsData)
    {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityMyContestNew.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<RulesModel> rulesModelCall = apiInterface.getRules(Constants.GET_RULES, Pref.getValue(ActivityMyContestNew.this,Constants.UserID,"",
                Constants.FILENAME),contestsData.TournamentID);

        rulesModelCall.enqueue(new Callback<RulesModel>()
        {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<RulesModel> call, @NonNull Response<RulesModel> response)
            {
                progressDialog.dismiss();
                RulesModel rulesModel = response.body();
                assert rulesModel != null;

                if (rulesModel.status.equalsIgnoreCase("success"))
                {
                    txt_title_bs.setText("Rules");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                    {
                        txt_bottom_msg.setText(Html.fromHtml(rulesModel.Data.Rules, Html.FROM_HTML_SEPARATOR_LINE_BREAK_DIV));
                    }
                    else
                    {
                        txt_bottom_msg.setText(Html.fromHtml(rulesModel.Data.Rules));
                    }
                    rules_bottomsheet.setState(BottomSheetBehavior.STATE_EXPANDED);
                    bg.setVisibility(View.VISIBLE);
                }
                else
                {
                    Constants.SnakeMessageYellow(parent_layout,rulesModel.message);
                }
            }
            @Override
            public void onFailure(@NonNull Call<RulesModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }
}
*/
