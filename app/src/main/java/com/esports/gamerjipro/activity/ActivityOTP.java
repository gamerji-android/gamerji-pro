package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.esports.gamerjipro.Model.ResendOTPModel;
import com.esports.gamerjipro.Model.SignInModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.Pref;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityOTP extends AppCompatActivity
{
    EditText et_otp1,et_otp2,et_otp3,et_otp4;
    TextView txt_resend,txt_phone_number,txt_country_code;
    String OTP,phone,country_code,email,pwd,coupon;
    APIInterface apiInterface;
    ImageView img_back;
    boolean from_login=false;
    boolean is_social=false;
    boolean fgt_pass=false;
    ScrollView parent_layout;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getIntent().getExtras()!=null)
        {
            OTP=getIntent().getStringExtra("OTP");
            phone=getIntent().getStringExtra("phone");
            country_code=getIntent().getStringExtra("country_code");
            email=getIntent().getStringExtra("email");
            pwd=getIntent().getStringExtra("password");
            coupon=getIntent().getStringExtra("coupon");
            from_login=getIntent().getBooleanExtra("from_login",false);
            is_social=getIntent().getBooleanExtra("is_social",false);
            fgt_pass=getIntent().getBooleanExtra("fgt_pass",false);
        }



        setContentView(R.layout.activity_otp);
        txt_resend=findViewById(R.id.txt_resend);
        et_otp1=findViewById(R.id.et_otp1);
        et_otp2=findViewById(R.id.et_otp2);
        parent_layout=findViewById(R.id.parent_layout);
        et_otp3=findViewById(R.id.et_otp3);
        img_back=findViewById(R.id.img_back);
        img_back.setOnClickListener(v -> finish());
        et_otp4=findViewById(R.id.et_otp4);
        txt_phone_number=findViewById(R.id.txt_phone_number);
        txt_country_code=findViewById(R.id.txt_country_code);
        txt_country_code.setText(country_code);
        txt_phone_number.setText("-"+phone);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        startTimer();


        et_otp1.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if(et_otp1.getText().toString().length()==1)     //size as per your requirement
                {
                    et_otp2.requestFocus();
                }
            }
            @Override
            public void afterTextChanged(Editable s)
            {}
        });
        et_otp2.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if(et_otp2.getText().toString().length()==1)     //size as per your requirement
                {
                    et_otp3.requestFocus();
                }
            }
            @Override
            public void afterTextChanged(Editable s)
            {}
        });
        et_otp3.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if(et_otp3.getText().toString().length()==1)     //size as per your requirement
                {
                    et_otp4.requestFocus();
                }
            }
            @Override
            public void afterTextChanged(Editable s)
            {}
        });
        et_otp4.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if(et_otp4.getText().toString().length()==1)     //size as per your requirement
                {
                    String OTPString=et_otp1.getText().toString()+et_otp2.getText().toString()+et_otp3.getText().toString()+et_otp4.getText().toString();
                    if (OTP.equals(OTPString))
                    {
                        if (!fgt_pass)
                        {
                            if (is_social)
                            {
                                updateMobile();
                            }
                            else
                            {
                                if (from_login)
                                {
                                    Pref.setValue(ActivityOTP.this, Constants.IS_LOGIN, true, Constants.FILENAME);

//                                    Intent i = new Intent(ActivityOTP.this, ActivityMain.class);
                                    Intent i = new Intent(ActivityOTP.this, BottomNavigationActivity.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(i);
                                    finish();
                                }
                                else
                                    sendToServer();
                            }
                        }
                        else
                        {
                            Intent i = new Intent(ActivityOTP.this,ActivityResetPassword.class);
                            i.putExtra("fgt_pass",true);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            finish();
                        }

                    }
                    else
                    {
                        Constants.SnakeMessageYellow(parent_layout,"Please enter a valid OTP");
                    }

                }
            }
            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });
        txt_resend.setOnClickListener(v ->
        {
            resendOTP();
            startTimer();
        });
    }

    private void startTimer()
    {
        new CountDownTimer(30000, 1000)
        {

            public void onTick(long millisUntilFinished)
            {
                txt_resend.setText( " "+millisUntilFinished/1000+" Seconds");
                txt_resend.setEnabled(false);
            }

            public void onFinish()
            {
                txt_resend.setText(" Resend OTP");
                txt_resend.setEnabled(true);
            }

        }.start();
    }

    private void resendOTP()
    {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityOTP.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<ResendOTPModel> resendOTPModelCall = apiInterface.resendOTP(Constants.REQUEST,country_code,phone);

        resendOTPModelCall.enqueue(new Callback<ResendOTPModel>()
        {
            @Override
            public void onResponse(@NonNull Call<ResendOTPModel> call, @NonNull Response<ResendOTPModel> response)
            {
                progressDialog.dismiss();
                ResendOTPModel  resendOTPModel = response.body();
                assert resendOTPModel != null;
                if (resendOTPModel.status.equalsIgnoreCase("success"))
                {
                    OTP=resendOTPModel.otpData.OTPCode;
                }
                else
                {
                    Constants.SnakeMessageYellow(parent_layout,resendOTPModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResendOTPModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }

        });
    }

    @Override
    protected void onResume()
    {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    protected void onPause()
    {
       LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onPause();
    }

    private BroadcastReceiver receiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            if (Objects.requireNonNull(intent.getAction()).equalsIgnoreCase("otp"))
            {
                String otp = intent.getStringExtra("message");
                otp = otp.replaceAll("[^0-9]+", " ");
                otp=otp.trim();
                if (otp.equals(OTP))
                {
                    if (!fgt_pass)
                    {
                        if (is_social)
                        {
                            updateMobile();
                        }
                        else
                        {
                            if (from_login)
                            {
                                Pref.setValue(ActivityOTP.this, Constants.IS_LOGIN, true, Constants.FILENAME);
//                                Intent i = new Intent(ActivityOTP.this, ActivityMain.class);
                                Intent i = new Intent(ActivityOTP.this, BottomNavigationActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);
                                finish();
                            }
                            else
                                sendToServer();
                        }
                    }
                    else
                    {
                        Intent i = new Intent(ActivityOTP.this,ActivityResetPassword.class);
                        i.putExtra("fgt_pass",true);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }


                }
                else
                {

                }
            }
        }
    };

    private void updateMobile()
    {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<SignInModel> signInModelCall = apiInterface.updateMobile(Constants.UPDATEMOBILE,phone,country_code,Pref.getValue(ActivityOTP.this,Constants.UserID,"", Constants.FILENAME),coupon);

        signInModelCall.enqueue(new Callback<SignInModel>()
        {
            @Override
            public void onResponse(@NonNull Call<SignInModel> call, @NonNull Response<SignInModel> response)
            {
                progressDialog.dismiss();
                SignInModel signInModel = response.body();
                assert signInModel != null;
                if (signInModel.status.equalsIgnoreCase("success"))
                {
                    Constants.SnakeMessageYellow(parent_layout,signInModel.message);
                    Constants.setUserGameData(signInModel.userDataClass.GamesData.gamesDataArrayList,ActivityOTP.this);
                    Pref.setValue(ActivityOTP.this, Constants.UserID, signInModel.userDataClass.UserID, Constants.FILENAME);
                    Pref.setValue(ActivityOTP.this, Constants.CountryCode, signInModel.userDataClass.CountryCode, Constants.FILENAME);
                    Pref.setValue(ActivityOTP.this, Constants.MobileNumber, signInModel.userDataClass.MobileNumber, Constants.FILENAME);
                    Pref.setValue(ActivityOTP.this, Constants.EmailAddress,signInModel.userDataClass.EmailAddress, Constants.FILENAME);
                    Pref.setValue(ActivityOTP.this, Constants.firstname,signInModel.userDataClass.FirstName, Constants.FILENAME);
                    Pref.setValue(ActivityOTP.this, Constants.lastname,signInModel.userDataClass.LastName, Constants.FILENAME);
                    if(!signInModel.userDataClass.BirthDate.isEmpty())
                    {
                        @SuppressLint("SimpleDateFormat") DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                        @SuppressLint("SimpleDateFormat") DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
                        String inputDateStr=signInModel.userDataClass.BirthDate;
                        Date date = null;
                        try
                        {
                            date = inputFormat.parse(inputDateStr);
                        }
                        catch (ParseException e)
                        {
                            e.printStackTrace();
                        }

                        String outputDateStr = outputFormat.format(date);
                        Pref.setValue(ActivityOTP.this, Constants.BirthDate,outputDateStr, Constants.FILENAME);
                    }
                    Pref.setValue(ActivityOTP.this, Constants.State,signInModel.userDataClass.State, Constants.FILENAME);
                    Pref.setValue(ActivityOTP.this, Constants.TeamName,signInModel.userDataClass.TeamName, Constants.FILENAME);
                    Pref.setValue(ActivityOTP.this, Constants.IS_LOGIN, true, Constants.FILENAME);
                    Pref.setValue(ActivityOTP.this, Constants.SOCIAL_LOGIN, true, Constants.FILENAME);
                    Constants.setUserGameData(signInModel.userDataClass.GamesData.gamesDataArrayList,ActivityOTP.this);
//                    Intent i = new Intent(ActivityOTP.this, ActivityMain.class);
                    Intent i = new Intent(ActivityOTP.this, BottomNavigationActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();

                }
                else
                {
                    Constants.SnakeMessageYellow(parent_layout,signInModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<SignInModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }


    private void sendToServer()
    {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<SignInModel> signInModelCall = apiInterface.doSignUp(Constants.REQUEST,country_code,phone,email,pwd,"","",coupon, Constants.BASIC,"","");

        signInModelCall.enqueue(new Callback<SignInModel>()
        {
            @Override
            public void onResponse(@NonNull Call<SignInModel> call, @NonNull Response<SignInModel> response)
            {
                progressDialog.dismiss();
                SignInModel signInModel = response.body();
                assert signInModel != null;
                if (signInModel.status.equalsIgnoreCase("success"))
                {
                    Constants.SnakeMessageYellow(parent_layout,signInModel.message);
                    Constants.setUserGameData(signInModel.userDataClass.GamesData.gamesDataArrayList,ActivityOTP.this);
                    Pref.setValue(ActivityOTP.this, Constants.UserID, signInModel.userDataClass.UserID, Constants.FILENAME);
                    Pref.setValue(ActivityOTP.this, Constants.CountryCode, signInModel.userDataClass.CountryCode, Constants.FILENAME);
                    Pref.setValue(ActivityOTP.this, Constants.MobileNumber, signInModel.userDataClass.MobileNumber, Constants.FILENAME);
                    Pref.setValue(ActivityOTP.this, Constants.EmailAddress,signInModel.userDataClass.EmailAddress, Constants.FILENAME);
                    Pref.setValue(ActivityOTP.this, Constants.firstname,signInModel.userDataClass.FirstName, Constants.FILENAME);
                    Pref.setValue(ActivityOTP.this, Constants.lastname,signInModel.userDataClass.LastName, Constants.FILENAME);
                    if(!signInModel.userDataClass.BirthDate.isEmpty())
                    {
                        @SuppressLint("SimpleDateFormat") DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                        @SuppressLint("SimpleDateFormat") DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
                        String inputDateStr=signInModel.userDataClass.BirthDate;
                        Date date = null;
                        try
                        {
                            date = inputFormat.parse(inputDateStr);
                        }
                        catch (ParseException e)
                        {
                            e.printStackTrace();
                        }

                        String outputDateStr = outputFormat.format(date);
                        Pref.setValue(ActivityOTP.this, Constants.BirthDate,outputDateStr, Constants.FILENAME);
                    }
                    Pref.setValue(ActivityOTP.this, Constants.State,signInModel.userDataClass.State, Constants.FILENAME);
                    Pref.setValue(ActivityOTP.this, Constants.IS_LOGIN, true, Constants.FILENAME);

//                    Intent i = new Intent(ActivityOTP.this, ActivityMain.class);
                    Intent i = new Intent(ActivityOTP.this, BottomNavigationActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                }
                else
                {
                    Constants.SnakeMessageYellow(parent_layout,signInModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<SignInModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }
}
