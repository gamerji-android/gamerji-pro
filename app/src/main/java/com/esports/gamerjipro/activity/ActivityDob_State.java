package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.esports.gamerjipro.Model.StatesListModel;
import com.esports.gamerjipro.Model.UpdateProfileModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.CustomWheelView;
import com.esports.gamerjipro.Utils.Pref;
import com.esports.gamerjipro.Utils.datepicker.DatePicker;
import com.esports.gamerjipro.Utils.datepicker.DatePickerDialog;
import com.esports.gamerjipro.Utils.datepicker.SpinnerDatePickerDialogBuilder;

import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityDob_State extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener
{
    TextView txt_date_birth,txt_name,et_states;
    APIInterface apiInterface;
    ArrayList<String> stateslist= new ArrayList<>();
    ArrayList<String> statesId= new ArrayList<>();
    ArrayList<String> isRestricted= new ArrayList<>();
    RelativeLayout lyt_signup;
    LinearLayout mRelativeEditProfileRootView;
    ImageView img_back;
    RelativeLayout parent_layout;
    String dob;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dob_states);
        txt_date_birth=findViewById(R.id.txt_date_birth);
        parent_layout=findViewById(R.id.parent_layout);
        img_back = findViewById(R.id.img_back);
        mRelativeEditProfileRootView=findViewById(R.id.mRelativeEditProfileRootView);
        txt_name=findViewById(R.id.txt_name);
        et_states=findViewById(R.id.et_states);
        lyt_signup=findViewById(R.id.lyt_signup);
        txt_date_birth.setOnClickListener(this);
        lyt_signup.setOnClickListener(this);
        img_back.setOnClickListener(v -> finish());
        apiInterface = APIClient.getClient().create(APIInterface.class);
        getAllStates();
    }



    private void getAllStates()
    {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityDob_State.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<StatesListModel> signUpModelCall = apiInterface.getAllStates(Constants.GET_ALL_STATES);

        signUpModelCall.enqueue(new Callback<StatesListModel>()
        {
            @Override
            public void onResponse(@NonNull Call<StatesListModel> call, @NonNull Response<StatesListModel> response)
            {
                progressDialog.dismiss();
                StatesListModel statesListModel = response.body();
                assert statesListModel != null;
                if (statesListModel.status.equalsIgnoreCase("success"))
                {
                    for (int i=0;i<statesListModel.userDataClass.statesDataArrayList.size();i++)
                    {
                        stateslist.add(statesListModel.userDataClass.statesDataArrayList.get(i).Name);
                        statesId.add(statesListModel.userDataClass.statesDataArrayList.get(i).StateID);
                        isRestricted.add(statesListModel.userDataClass.statesDataArrayList.get(i).IsRestricted);
                    }
                    et_states.setText(stateslist.get(0));
                    et_states.setOnClickListener(v -> selectState());
                }

                else
                {
                    Constants.SnakeMessageYellow(parent_layout, statesListModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<StatesListModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void selectState()
    {
        if (stateslist.size() == 0)
        {
            Constants.SnakeMessageYellow(mRelativeEditProfileRootView, "no States found.");
            return;
        }
        Dialog dialog = new Dialog(ActivityDob_State.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View outerView = LayoutInflater.from(ActivityDob_State.this).inflate(R.layout.wheelview, null);
        dialog.setContentView(outerView);
        if (dialog.getWindow() != null)
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        CustomWheelView wv = outerView.findViewById(R.id.wh_view);
        TextView txt_ok = outerView.findViewById(R.id.txt_ok);
        TextView title = outerView.findViewById(R.id.title);
        title.setText("Select state");
        TextView txt_cancel = outerView.findViewById(R.id.txt_cancel);
        txt_ok.setOnClickListener(v ->
        {
            et_states.setText(wv.getSeletedItem().toString());
            dialog.dismiss();

        });

        txt_cancel.setOnClickListener(v -> dialog.dismiss());
        wv.setOffset(1);
        wv.setItems(stateslist);
        wv.setSeletion(0);
        dialog.show();
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.txt_date_birth:
            {
                try
                {
                    Calendar calendar = Calendar.getInstance();
                    Calendar mMaxDate = Calendar.getInstance();
                    Calendar mMinDate = Calendar.getInstance();
                    mMaxDate.add(Calendar.YEAR, -18);
                    mMaxDate.setTimeInMillis(mMaxDate.getTimeInMillis());
                    mMinDate.set(Calendar.YEAR, 1920);
                    mMinDate.set(Calendar.MONTH, 1);
                    mMinDate.set(Calendar.DATE, 1);

                    int mMaxDateYear = mMaxDate.get(Calendar.YEAR);
                    int mMaxDateMonth = mMaxDate.get(Calendar.MONTH);
                    int mMaxDateDay = mMaxDate.get(Calendar.DAY_OF_MONTH);

                    int mMinDateYear = mMinDate.get(Calendar.YEAR);
                    int mMinDateMonth = mMinDate.get(Calendar.MONTH);
                    int mMinDateDay = mMinDate.get(Calendar.DAY_OF_MONTH);


                    new SpinnerDatePickerDialogBuilder().context(ActivityDob_State.this).callback(this).spinnerTheme(R.style.NumberPickerStyle)
                            .showTitle(true).showDaySpinner(true).defaultDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                            .maxDate(mMaxDateYear, mMaxDateMonth, mMaxDateDay)
                            .minDate(mMinDateYear, mMinDateMonth, mMinDateDay).build().show();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;
            }

            case R.id.lyt_signup:
            {
                validate();
                break;
            }
        }

    }

    private void validate()
    {

        int position= stateslist.indexOf(et_states.getText().toString());
        if (txt_date_birth.getText().toString().isEmpty())
        {
            Constants.SnakeMessageYellow(parent_layout,"Enter date of birth");
        }
        else
        {
                updateProfile();
        }
    }

    private void updateProfile()
    {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityDob_State.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<UpdateProfileModel> signUpModelCall;
        int state_id= stateslist.indexOf(et_states.getText().toString());


            signUpModelCall = apiInterface.updateProfile(Constants.UPDATE_GAME_PROFILE, Pref.getValue(ActivityDob_State.this, Constants.UserID,"", Constants.FILENAME),
                    dob,statesId.get(state_id)," ",
                    "","");


        signUpModelCall.enqueue(new Callback<UpdateProfileModel>()
        {
            @Override
            public void onResponse(@NonNull Call<UpdateProfileModel> call, @NonNull Response<UpdateProfileModel> response)
            {
                progressDialog.dismiss();
                UpdateProfileModel updateProfileModel = response.body();
                assert updateProfileModel != null;
                if (updateProfileModel.status.equalsIgnoreCase("success"))
                {
                    Pref.setValue(ActivityDob_State.this,Constants.BirthDate,txt_date_birth.getText().toString(), Constants.FILENAME);
                    Pref.setValue(ActivityDob_State.this,Constants.State,et_states.getText().toString(), Constants.FILENAME);
                    Constants.SnakeMessageYellow(parent_layout,updateProfileModel.message );
                    finish();
                }
                else
                {
                    Constants.SnakeMessageYellow(parent_layout,updateProfileModel.message );
                }
            }

            @Override
            public void onFailure(@NonNull Call<UpdateProfileModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }

        });
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
    {
        txt_date_birth.setText(dayOfMonth+"/"+(monthOfYear+1)+"/"+year);
        dob=year+"/"+(monthOfYear+1)+"/"+dayOfMonth;
    }
}
