package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esports.gamerjipro.Adapter.GameProfileAdapter;
import com.esports.gamerjipro.Adapter.MyBadgesAdapter;
import com.esports.gamerjipro.Model.GamerProfileModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.AnalyticsSocket;
import com.esports.gamerjipro.Utils.AppConstants;
import com.esports.gamerjipro.Utils.Common;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.Pref;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlayerProfileActivity extends AppCompatActivity {

    private RelativeLayout mRelativeMain;
    private ImageView mImageBack, mImageGameLevel, mImageLeftArrow, mImageRightArrow;
    private TextView mTextPlayerName, mTextLevelRank, mTextLevelName, mTextTeamName, mTextPubgId, mTextClashId,
            mTextTotalPoints, mTextProgressTotalPoints, mTextStartingLevel, mTextEndingLevel, mTextStartingPoints,
            mTextEndingPoints;
    private SeekBar mSeekBarGameProgress;
    private RecyclerView mRecyclerViewBadges, mRecyclerViewGames;
    private MyBadgesAdapter myBadgesAdapter;
    private LinearLayoutManager badgesLinearLayout;
    private int currentPage = 0, NUM_PAGES = 0;
    private String mPlayerName;
    private Timer swipeTimer;
    boolean sendSwipeImpression = true;
    private AnalyticsSocket analyticsSocket = new AnalyticsSocket();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_profile);
        swipeTimer = new Timer();
        badgesLinearLayout = new LinearLayoutManager(PlayerProfileActivity.this, LinearLayoutManager.HORIZONTAL, false);

        getBundle();
        getIds();
        setData();
        setRegListeners();
        getGamerProfile();
    }

    /**
     * Get the keys from the home fragment
     */
    private void getBundle() {
        try {
            mPlayerName = getIntent().getStringExtra(AppConstants.BUNDLE_PLAYER_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Relative Layout
            mRelativeMain = findViewById(R.id.relative_player_profile_main);

            // Text Views
            mTextPlayerName = findViewById(R.id.text_player_profile_player_name);
            mTextLevelRank = findViewById(R.id.text_player_profile_game_level_rank);
            mTextLevelName = findViewById(R.id.text_player_profile_game_level_name);
            mTextTeamName = findViewById(R.id.text_player_profile_game_team_name);
            mTextPubgId = findViewById(R.id.text_player_profile_game_pubg_id);
            mTextClashId = findViewById(R.id.text_player_profile_game_clash_id);
            mTextTotalPoints = findViewById(R.id.text_player_profile_game_total_points);
            mTextProgressTotalPoints = findViewById(R.id.text_player_profile_game_progress_total_points);
            mTextStartingLevel = findViewById(R.id.text_player_profile_game_starting_level);
            mTextEndingLevel = findViewById(R.id.text_player_profile_game_ending_level);
            mTextStartingPoints = findViewById(R.id.text_player_profile_game_starting_points);
            mTextEndingPoints = findViewById(R.id.text_player_profile_game_ending_points);

            // Image Views
            mImageBack = findViewById(R.id.image_player_profile_back);
            mImageGameLevel = findViewById(R.id.image_player_profile_game_level);
            mImageLeftArrow = findViewById(R.id.image_player_profile_game_badges_left_arrow);
            mImageRightArrow = findViewById(R.id.image_player_profile_game_badges_right_arrow);

            // Seek Bar
            mSeekBarGameProgress = findViewById(R.id.seekbar_player_profile_game_progress);

            // Recycler Views
            mRecyclerViewBadges = findViewById(R.id.recycler_view_player_profile_game_badges);
            mRecyclerViewGames = findViewById(R.id.recycler_view_player_profile_game_games);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: On Click Listener
            mImageBack.setOnClickListener(clickListener);
            mImageLeftArrow.setOnClickListener(clickListener);
            mImageRightArrow.setOnClickListener(clickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        try {
            mTextPubgId.setSelected(true);
            mTextClashId.setSelected(true);
            mTextTotalPoints.setSelected(true);
            mTextPlayerName.setText(mPlayerName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.image_player_profile_back:
                    finish();
                    break;

                case R.id.image_player_profile_game_badges_left_arrow:
                    Objects.requireNonNull(mRecyclerViewBadges.getLayoutManager()).scrollToPosition(badgesLinearLayout.findFirstVisibleItemPosition() - 1);
                    break;

                case R.id.image_player_profile_game_badges_right_arrow:
                    Objects.requireNonNull(mRecyclerViewBadges.getLayoutManager()).scrollToPosition(badgesLinearLayout.findLastVisibleItemPosition() + 1);
                    break;
            }
        }
    };

    /**
     * Call to get the Gamer Profile API
     */
    private void getGamerProfile() {
        final ProgressDialog progressDialog = new ProgressDialog(PlayerProfileActivity.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<GamerProfileModel> gamerProfileModelCall = APIClient.getClient().create(APIInterface.class).getGamerProfile
                (Constants.GET_GAMER_PROFILE, Pref.getValue(PlayerProfileActivity.this, Constants.UserID, "", Constants.FILENAME));

        gamerProfileModelCall.enqueue(new Callback<GamerProfileModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<GamerProfileModel> call, @NonNull Response<GamerProfileModel> response) {
                progressDialog.dismiss();

                GamerProfileModel gamerProfileModel = response.body();
                assert gamerProfileModel != null;
                if (gamerProfileModel.status.equalsIgnoreCase("success")) {
                    mTextTeamName.setText(gamerProfileModel.DataClass.TeamName);
                    mRecyclerViewGames.setLayoutManager(new GridLayoutManager(PlayerProfileActivity.this, 2));
                    GameProfileAdapter gameProfileAdapter = new GameProfileAdapter(PlayerProfileActivity.this, gamerProfileModel.DataClass.gamesData.gamesDataArrayList);
                    mRecyclerViewGames.setAdapter(gameProfileAdapter);

                    NUM_PAGES = gamerProfileModel.DataClass.AdsCount;
                    final Handler handler = new Handler();
                    final Runnable Update = () ->
                    {
                        if (currentPage == NUM_PAGES) {
                            currentPage = 0;
                            sendSwipeImpression = false;
                            //swipeTimer.cancel();
                        } else {
                            if (sendSwipeImpression) {
                                analyticsSocket.sendAnalytics(gamerProfileModel.DataClass.adsDataArrayList.get(currentPage).AdType, gamerProfileModel.DataClass.adsDataArrayList.get(currentPage).AdID,
                                        gamerProfileModel.DataClass.adsDataArrayList.get(currentPage).AdImageName, Constants.AD_IMPRESSION, Pref.getValue(PlayerProfileActivity.this, Constants.UserID, "", Constants.FILENAME),
                                        0);
                            }
                        }
                    };

                    swipeTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            handler.post(Update);
                        }
                    }, 0, 5000);

                    for (int i = 0; i < gamerProfileModel.DataClass.gamesData.gamesDataArrayList.size(); i++) {
                        if (gamerProfileModel.DataClass.gamesData.gamesDataArrayList.get(i).GameName.equalsIgnoreCase(AppConstants.GAME_P_MOBILE)) {
                            Common.insertLog("P Mobile Name:::> " + gamerProfileModel.DataClass.gamesData.gamesDataArrayList.get(i).UniqueName);

                            if(gamerProfileModel.DataClass.gamesData.gamesDataArrayList.get(i).UniqueName.equalsIgnoreCase("")) {
                                mTextPubgId.setText("-");
                            } else{
                                mTextPubgId.setText(gamerProfileModel.DataClass.gamesData.gamesDataArrayList.get(i).UniqueName);
                            }
                        } else if (gamerProfileModel.DataClass.gamesData.gamesDataArrayList.get(i).GameName.equalsIgnoreCase(AppConstants.GAME_CLASH_ROYALE)) {
                            Common.insertLog("Clash Royale Name:::> " + gamerProfileModel.DataClass.gamesData.gamesDataArrayList.get(i).UniqueName);

                            if(gamerProfileModel.DataClass.gamesData.gamesDataArrayList.get(i).UniqueName.equalsIgnoreCase("")) {
                                mTextClashId.setText("-");
                            } else{
                                mTextClashId.setText(gamerProfileModel.DataClass.gamesData.gamesDataArrayList.get(i).UniqueName);
                            }
                        }
                    }
                    mTextTotalPoints.setText(gamerProfileModel.DataClass.TotalPoints);
                    Glide.with(PlayerProfileActivity.this).load(gamerProfileModel.DataClass.LevelIcon).into(mImageGameLevel);
                    mTextLevelName.setText(gamerProfileModel.DataClass.LevelName);
                    mTextLevelRank.setText(gamerProfileModel.DataClass.LevelNumber);
                    mTextProgressTotalPoints.setText(gamerProfileModel.DataClass.CurrentLevelPoints + " Points");
                    setBadges(gamerProfileModel.DataClass.LevelsData.levelsDataArrayList);

                    int difference = Integer.parseInt(gamerProfileModel.DataClass.EndLevelPoints) - Integer.parseInt(gamerProfileModel.DataClass.StartLevelPoints);
                    mSeekBarGameProgress.setMax(difference);
                    mSeekBarGameProgress.setProgress(Integer.parseInt(gamerProfileModel.DataClass.CurrentLevelPoints) - Integer.parseInt(gamerProfileModel.DataClass.StartLevelPoints));

                    mTextStartingLevel.setText(gamerProfileModel.DataClass.StartLevelNumber);
                    mTextEndingLevel.setText(gamerProfileModel.DataClass.EndLevelNumber);
                    mTextStartingPoints.setText("Points:" + gamerProfileModel.DataClass.StartLevelPoints);
                    mTextEndingPoints.setText("Points:" + gamerProfileModel.DataClass.EndLevelPoints);
                } else {
                    Constants.SnakeMessageYellow(mRelativeMain, gamerProfileModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<GamerProfileModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void setBadges(ArrayList<GamerProfileModel.Data.LevelsData.LevelData> levelsDataArrayList) {
        mRecyclerViewBadges.setLayoutManager(badgesLinearLayout);
        mRecyclerViewBadges.setItemAnimator(new DefaultItemAnimator());
        myBadgesAdapter = new MyBadgesAdapter(PlayerProfileActivity.this, levelsDataArrayList);
        mRecyclerViewBadges.setAdapter(myBadgesAdapter);
    }
}