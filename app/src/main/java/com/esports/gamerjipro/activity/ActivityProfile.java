/*
package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;

import com.esports.gamerjipro.Adapter.AdsSliderAdapter;
import com.esports.gamerjipro.Adapter.GameProfileAdapter;
import com.esports.gamerjipro.Adapter.GameUserNameAdapter;
import com.esports.gamerjipro.Adapter.MyBadgesAdapter;
import com.esports.gamerjipro.Model.GameUsernameModel;
import com.esports.gamerjipro.Model.GamerProfileModel;
import com.esports.gamerjipro.Model.GamesDataModel;
import com.esports.gamerjipro.Model.SignInModel;
import com.esports.gamerjipro.Model.StatesListModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.AnalyticsSocket;
import com.esports.gamerjipro.Utils.AppConstants;
import com.esports.gamerjipro.Utils.Common;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.CustomWheelView;
import com.esports.gamerjipro.Utils.Pref;
import com.esports.gamerjipro.Utils.datepicker.DatePicker;
import com.esports.gamerjipro.Utils.datepicker.DatePickerDialog;
import com.esports.gamerjipro.Utils.datepicker.SpinnerDatePickerDialogBuilder;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdSettings;
import com.facebook.ads.AdSize;
import com.facebook.ads.AudienceNetworkAds;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.esports.gamerjipro.Utils.Constants.AD_IMPRESSION;
import static com.esports.gamerjipro.Utils.FileUtils.OpenNewsWebview;
import static com.esports.gamerjipro.Utils.FileUtils.requestfoucs;

public class ActivityProfile extends AppCompatActivity implements IPickResult, View.OnClickListener, DatePickerDialog.OnDateSetListener {
    SeekBar pb_level;
    LinearLayout lyt_progress_text, lyt_personal_profile, lyt_game_profile;
    TextView txt_gamer, txt_personal, txt_date_birth, txt_save, et_states, txt_end_lvl, txt_start_lvl;
    ArrayList<String> stateslist = new ArrayList<>();
    ArrayList<String> statesId = new ArrayList<>();
    ArrayList<String> isRestricteed = new ArrayList<>();
    APIInterface apiInterface;
    TextView sp_states;
    EditText et_team_name, et_email, et_phone, et_lname, et_fname;
    ImageView profile_image, img_close;
    PickResult pickResult;
    ArrayList<SignInModel.UserData.GamesData.GameData> gamesDataArrayList = new ArrayList<>();
    ArrayList<GameUsernameModel> gameUsernameModelArrayList = new ArrayList<>();
    RelativeLayout mRelativeEditProfileRootView;
    boolean editable = false;
    RecyclerView rv_badges;
    MyBadgesAdapter myBadgesAdapter;
    ImageView img_back, img_info3, img_info2, img_info1, info_team;
    public BottomSheetBehavior sheetBehavior;
    RelativeLayout layoutBottomSheet, bottom_badge;
    TextView txt_pubg_id, txt_cr_id, txt_total_points, txt_pubg_played, txt_pubg_won, txt_pubg_points, txt_cr_played, txt_cr_won, txt_cr_points, txt_team_name, txt_level_number, txt_end_points, txt_current_points;
    public View bg;
    ImageView level_image, mImgEditImage;
    TextView txt_level_name, txt_title;
    public TextView txt_title_bs, txt_bottom_msg;
    CoordinatorLayout parent_layout;
    public ImageView img_badge;
    public TextView txt_badge_msg, txt_points;
    public BottomSheetBehavior badgesheet;
    TextView txt_level_numnber;
    ImageView img_close_badges, front_arrow, back_arrow;
    LinearLayoutManager badgesLinearLayout;
    CardView cv_fname, cv_lname, cv_phone, cv_email, cv_team_name, cv_pubg;
    String dob;
    TextView img_news;
    RecyclerView rv_game_profile, rv_game_username;
    JSONArray player_games_username_list, player_games_usergameid;
    GameUserNameAdapter gameUserNameAdapter;

    ViewPager slider_pager_ads;
    View lyt_ads;
    private int currentPage = 0;
    Timer swipeTimer;
    private int NUM_PAGES = 0;
    AnalyticsSocket analyticsSocket = new AnalyticsSocket();
    boolean sendSwipeImpression = true;
    LinearLayout adContainer;
    private SwipeRefreshLayout swipe_refresh;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layput_profile);

        swipe_refresh = findViewById(R.id.swipe_refresh);
        img_news = findViewById(R.id.img_news);
        rv_game_profile = findViewById(R.id.rv_game_profile);
        rv_game_username = findViewById(R.id.rv_game_username);
        lyt_ads = findViewById(R.id.lyt_ads);
        img_news.setOnClickListener(v -> OpenNewsWebview(this, "https://www.gamerji.com"));
        sp_states = findViewById(R.id.sp_states);
        img_badge = findViewById(R.id.img_badge);
        front_arrow = findViewById(R.id.front_arrow);
        back_arrow = findViewById(R.id.back_arrow);
        img_close_badges = findViewById(R.id.img_close_badges);
        txt_title_bs = findViewById(R.id.txt_title_bs);
        txt_badge_msg = findViewById(R.id.txt_badge_msg);
        badgesLinearLayout = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        parent_layout = findViewById(R.id.parent_layout);
        txt_end_points = findViewById(R.id.txt_end_points);
        txt_current_points = findViewById(R.id.txt_current_points);
        txt_team_name = findViewById(R.id.txt_team_name);
        pb_level = findViewById(R.id.pb_level);
        img_back = findViewById(R.id.img_back);
        txt_save = findViewById(R.id.txt_save);
        profile_image = findViewById(R.id.profile_image);
        img_close = findViewById(R.id.img_close);
        lyt_game_profile = findViewById(R.id.lyt_game_profile);
        lyt_personal_profile = findViewById(R.id.lyt_personal_profile);
        txt_personal = findViewById(R.id.txt_personal);
        txt_gamer = findViewById(R.id.txt_gamer);
        et_states = findViewById(R.id.et_states);
        txt_end_lvl = findViewById(R.id.txt_end_lvl);
        txt_start_lvl = findViewById(R.id.txt_start_lvl);
        lyt_progress_text = findViewById(R.id.lyt_progress_text);

        et_team_name = findViewById(R.id.et_team_name);
        et_email = findViewById(R.id.et_email);
        mImgEditImage = findViewById(R.id.mImgEditImage);
        et_phone = findViewById(R.id.et_phone);
        bg = findViewById(R.id.bg);
        et_lname = findViewById(R.id.et_lname);
        et_fname = findViewById(R.id.et_fname);
        img_info3 = findViewById(R.id.img_info3);
        img_info2 = findViewById(R.id.img_info2);
        img_info1 = findViewById(R.id.img_info1);
        info_team = findViewById(R.id.info_team);
        txt_bottom_msg = findViewById(R.id.txt_bottom_msg);
        txt_title = findViewById(R.id.txt_title);
        txt_level_name = findViewById(R.id.txt_level_name);
        level_image = findViewById(R.id.level_image);
        txt_level_number = findViewById(R.id.txt_level_number);
        txt_level_numnber = findViewById(R.id.txt_level_numnber);

        txt_date_birth = findViewById(R.id.txt_date_birth);
        rv_badges = findViewById(R.id.rv_badges);
        layoutBottomSheet = findViewById(R.id.bottom_how_to);
        bottom_badge = findViewById(R.id.bottom_badge);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        badgesheet = BottomSheetBehavior.from(bottom_badge);
        txt_points = bottom_badge.findViewById(R.id.txt_points);

        cv_pubg = findViewById(R.id.cv_pubg);
        cv_team_name = findViewById(R.id.cv_team_name);
        cv_email = findViewById(R.id.cv_email);
        cv_phone = findViewById(R.id.cv_phone);
        cv_lname = findViewById(R.id.cv_lname);
        cv_fname = findViewById(R.id.cv_fname);

        rv_game_profile.setLayoutManager(new GridLayoutManager(this, 2));
        rv_game_username.setLayoutManager(new LinearLayoutManager(this));


        et_email.setText(Pref.getValue(this, Constants.EmailAddress, "", Constants.FILENAME));
        et_phone.setText(Pref.getValue(ActivityProfile.this, Constants.MobileNumber, "", Constants.FILENAME));
        et_fname.setText(Pref.getValue(ActivityProfile.this, Constants.firstname, "", Constants.FILENAME));
        et_lname.setText(Pref.getValue(ActivityProfile.this, Constants.lastname, "", Constants.FILENAME));
        et_team_name.setText(Pref.getValue(ActivityProfile.this, Constants.TeamName, "", Constants.FILENAME));
        txt_date_birth.setText(Pref.getValue(ActivityProfile.this, Constants.BirthDate, "", Constants.FILENAME));
        if (!Pref.getValue(ActivityProfile.this, Constants.State, "", Constants.FILENAME).equalsIgnoreCase(""))
            et_states.setText(Pref.getValue(ActivityProfile.this, Constants.State, "", Constants.FILENAME));

        txt_pubg_id = findViewById(R.id.txt_pubg_id);
        txt_cr_id = findViewById(R.id.txt_cr_id);
        txt_total_points = findViewById(R.id.txt_total_points);
        txt_pubg_won = findViewById(R.id.txt_pubg_won);
        txt_pubg_played = findViewById(R.id.txt_pubg_played);
        txt_pubg_points = findViewById(R.id.txt_pubg_points);
       */
/* txt_cr_played = findViewById(R.id.txt_cr_played);
        txt_cr_won = findViewById(R.id.txt_cr_won);
        txt_cr_points = findViewById(R.id.txt_cr_points);*//*

        if (!(Pref.getValue(ActivityProfile.this, Constants.ProfileImage, "", Constants.FILENAME).equalsIgnoreCase(""))
                || !Pref.getValue(ActivityProfile.this, Constants.ProfileImage, "", Constants.FILENAME).isEmpty()) {
            Glide.with(ActivityProfile.this).load(Pref.getValue(ActivityProfile.this, Constants.ProfileImage, "", Constants.FILENAME)).into(profile_image);
        }

       */
/* img_pubg_bg = findViewById(R.id.img_pubg_bg);
        img_cr_bg = findViewById(R.id.img_cr_bg);*//*



        txt_date_birth.setOnClickListener(this);
        img_info1.setOnClickListener(this);
        img_info2.setOnClickListener(this);
        img_info3.setOnClickListener(this);
        info_team.setOnClickListener(this);
        img_close.setOnClickListener(this);
        et_states.setOnClickListener(this);
        img_close_badges.setOnClickListener(this);
        back_arrow.setOnClickListener(this);
        front_arrow.setOnClickListener(this);

        cv_fname.setOnClickListener(v -> requestfoucs(ActivityProfile.this, et_fname));

        cv_lname.setOnClickListener(v -> requestfoucs(ActivityProfile.this, et_lname));

        cv_phone.setOnClickListener(v -> requestfoucs(ActivityProfile.this, et_phone));

        cv_email.setOnClickListener(v -> requestfoucs(ActivityProfile.this, et_email));

        cv_team_name.setOnClickListener(v -> requestfoucs(ActivityProfile.this, et_team_name));

        slider_pager_ads = findViewById(R.id.slider_pager_ads);
        swipeTimer = new Timer();


        gamesDataArrayList = Constants.getUserData(ActivityProfile.this);
        txt_gamer.setOnClickListener(v ->
        {
            lyt_personal_profile.setVisibility(View.GONE);
            lyt_game_profile.setVisibility(View.VISIBLE);
            txt_gamer.setBackground(getDrawable(R.drawable.bottom_rounded));
            txt_gamer.setTextColor(Color.WHITE);
            txt_personal.setTextColor(Color.BLACK);
            txt_personal.setBackgroundColor(Color.TRANSPARENT);
            txt_save.setVisibility(View.GONE);
            // img_news.setVisibility(View.VISIBLE);
            clearEditText();
            setDisableEditText();
            editable = false;
            swipe_refresh.setEnabled(true);
        });


        */
/*if (!(gamesDataArrayList.get(0).UniqueName.isEmpty()))
            et_pubg_name.setText(gamesDataArrayList.get(0).UniqueName);
        if (!(gamesDataArrayList.get(1).UniqueName.isEmpty()))
            et_cr_name.setText(gamesDataArrayList.get(1).UniqueName);*//*


        txt_personal.setOnClickListener(v ->
        {
            lyt_personal_profile.setVisibility(View.VISIBLE);
            lyt_game_profile.setVisibility(View.GONE);
            txt_personal.setBackground(getDrawable(R.drawable.bottom_rounded));
            txt_personal.setTextColor(Color.WHITE);
            txt_gamer.setTextColor(Color.BLACK);
            txt_gamer.setBackgroundColor(Color.TRANSPARENT);
            txt_save.setVisibility(View.VISIBLE);
            //  img_news.setVisibility(View.GONE);
            setDisableEditText();
            txt_save.setText("Edit");
            swipe_refresh.setEnabled(false);
        });

        profile_image.setOnClickListener(v -> PickImageDialog.build(new PickSetup()).show(ActivityProfile.this));
        apiInterface = APIClient.getClient().create(APIInterface.class);

        getAllStates();

        txt_save.setOnClickListener(v ->
        {
            if (editable) {
                printAllEditTextValues();
                //gameUserNameAdapter.disableEdittext();
                try {
                    validate();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                editable = true;
                setEnable();
                gameUserNameAdapter.enalbleEdittext();
                txt_save.setText("SAVE");
            }

        });
        // writeOnDrawable(R.drawable.)
        getGamerProfile();

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState) {

                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        bg.setVisibility(View.GONE);
                        break;
                    }

                    case BottomSheetBehavior.STATE_EXPANDED: {
                        bg.setVisibility(View.VISIBLE);
                        break;
                    }
                    case BottomSheetBehavior.STATE_DRAGGING: {
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {


            }
        });

        badgesheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                switch (i) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        bg.setVisibility(View.GONE);
                        break;
                    }

                    case BottomSheetBehavior.STATE_EXPANDED: {
                        bg.setVisibility(View.VISIBLE);
                        bg.bringToFront();
                        //  mRelativeEditProfileRootView.setEnabled(false);
                        break;
                    }//  mRelativeEditProfileRootView.setEnabled(true);
                    case BottomSheetBehavior.STATE_DRAGGING: {
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });

        swipe_refresh.setOnRefreshListener(() ->
        {
            getGamerProfile();
        });
    }

    public boolean printAllEditTextValues() {
        int childCount = rv_game_username.getChildCount();
        boolean isEmpty = false;

        player_games_username_list = new JSONArray();
        player_games_usergameid = new JSONArray();

        for (int i = 0; i <= childCount; i++) {
            if (rv_game_username.findViewHolderForLayoutPosition(i) instanceof GameUserNameAdapter.ViewHolder) {
                GameUserNameAdapter.ViewHolder childHolder = (GameUserNameAdapter.ViewHolder) rv_game_username.findViewHolderForLayoutPosition(i);
                assert childHolder != null;
                player_games_username_list.put(childHolder.et_game_username.getText().toString());
                player_games_usergameid.put(gameUsernameModelArrayList.get(i).getUserGameID());
            }
        }
        return isEmpty;
    }

    private void clearEditText() {

    }

    private void getGamerProfile() {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityProfile.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<GamerProfileModel> gamerProfileModelCall = apiInterface.getGamerProfile(Constants.GET_GAMER_PROFILE, Pref.getValue(ActivityProfile.this, Constants.UserID, "", Constants.FILENAME));

        gamerProfileModelCall.enqueue(new Callback<GamerProfileModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<GamerProfileModel> call, @NonNull Response<GamerProfileModel> response) {
                progressDialog.dismiss();

                if (swipe_refresh.isRefreshing())
                    swipe_refresh.setRefreshing(false);

                GamerProfileModel gamerProfileModel = response.body();
                assert gamerProfileModel != null;
                if (gamerProfileModel.status.equalsIgnoreCase("success")) {
                    txt_team_name.setText(gamerProfileModel.DataClass.TeamName);
                    GameProfileAdapter gameProfileAdapter = new GameProfileAdapter(ActivityProfile.this, gamerProfileModel.DataClass.gamesData.gamesDataArrayList);
                    rv_game_profile.setAdapter(gameProfileAdapter);


                    slider_pager_ads.setAdapter(new AdsSliderAdapter(ActivityProfile.this, gamerProfileModel.DataClass.adsDataArrayList, analyticsSocket));

                    if (gamerProfileModel.DataClass.AdsCount == 0)
                        lyt_ads.setVisibility(View.GONE);
                    else
                        adContainer.setVisibility(View.GONE);

                    NUM_PAGES = gamerProfileModel.DataClass.AdsCount;
                    final Handler handler = new Handler();
                    final Runnable Update = () ->
                    {
                        if (currentPage == NUM_PAGES) {
                            currentPage = 0;
                            sendSwipeImpression = false;
                            //swipeTimer.cancel();
                        } else {
                            if (sendSwipeImpression) {
                                analyticsSocket.sendAnalytics(gamerProfileModel.DataClass.adsDataArrayList.get(currentPage).AdType, gamerProfileModel.DataClass.adsDataArrayList.get(currentPage).AdID,
                                        gamerProfileModel.DataClass.adsDataArrayList.get(currentPage).AdImageName, AD_IMPRESSION, Pref.getValue(ActivityProfile.this, Constants.UserID, "", Constants.FILENAME),
                                        0);
                            }
                        }
                        slider_pager_ads.setCurrentItem(currentPage++, true);
                    };

                    swipeTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            handler.post(Update);
                        }
                    }, 0, 5000);


                    for (int i = 0; i < gamerProfileModel.DataClass.gamesData.gamesDataArrayList.size(); i++) {
                        if (gamerProfileModel.DataClass.gamesData.gamesDataArrayList.get(i).GameName.equalsIgnoreCase(AppConstants.GAME_P_MOBILE)) {
                            txt_pubg_id.setText(gamerProfileModel.DataClass.gamesData.gamesDataArrayList.get(i).UniqueName);
                        } else if (gamerProfileModel.DataClass.gamesData.gamesDataArrayList.get(i).GameName.equalsIgnoreCase(AppConstants.GAME_CLASH_ROYALE)) {
                            txt_cr_id.setText(gamerProfileModel.DataClass.gamesData.gamesDataArrayList.get(i).UniqueName);
                        }
                    }


                    setGameUserName(gamerProfileModel.DataClass.gamesData.gamesDataArrayList);

                    txt_total_points.setText(gamerProfileModel.DataClass.TotalPoints);
                    Glide.with(ActivityProfile.this).load(gamerProfileModel.DataClass.LevelIcon).into(level_image);
                    txt_level_name.setText(gamerProfileModel.DataClass.LevelName);
                    txt_level_number.setText(gamerProfileModel.DataClass.LevelNumber);
                    txt_level_numnber.setText(gamerProfileModel.DataClass.CurrentLevelPoints + " Points");
                    setBadges(gamerProfileModel.DataClass.LevelsData.levelsDataArrayList);

                    int difference = Integer.parseInt(gamerProfileModel.DataClass.EndLevelPoints) - Integer.parseInt(gamerProfileModel.DataClass.StartLevelPoints);
                    pb_level.setMax(difference);
                    pb_level.setProgress(Integer.parseInt(gamerProfileModel.DataClass.CurrentLevelPoints) - Integer.parseInt(gamerProfileModel.DataClass.StartLevelPoints));

                    txt_start_lvl.setText(gamerProfileModel.DataClass.StartLevelNumber);
                    txt_end_lvl.setText(gamerProfileModel.DataClass.EndLevelNumber);
                    txt_current_points.setText("Points:" + gamerProfileModel.DataClass.StartLevelPoints);
                    txt_end_points.setText("Points:" + gamerProfileModel.DataClass.EndLevelPoints);

                    */
/*Glide.with(ActivityProfile.this).load(gamerProfileModel.DataClass.gamesData.gamesDataArrayList.get(1).GameFeaturedImage).into(img_cr_bg);*//*

                } else {
                    Constants.SnakeMessageYellow(parent_layout, gamerProfileModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<GamerProfileModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }

        });

    }

    private void setGameUserName(ArrayList<GamerProfileModel.Data.GamesData.GameData> gamesDataArrayList) {
        for (int i = 0; i < gamesDataArrayList.size(); i++) {
            GameUsernameModel gameUsernameModel = new GameUsernameModel();
            gameUsernameModel.setGameID(gamesDataArrayList.get(i).GameID);
            gameUsernameModel.setGameName(gamesDataArrayList.get(i).GameName);
            gameUsernameModel.setUniqueName(gamesDataArrayList.get(i).UniqueName);
            gameUsernameModel.setUserGameID(gamesDataArrayList.get(i).UserGameID);
            if (gamesDataArrayList.get(i).UniqueName.equalsIgnoreCase("")) {
                gameUsernameModel.setEnable(true);
            } else {
                gameUsernameModel.setEnable(false);
            }
            gameUsernameModelArrayList.add(gameUsernameModel);
        }

        gameUserNameAdapter = new GameUserNameAdapter(ActivityProfile.this, gameUsernameModelArrayList);
        rv_game_username.setAdapter(gameUserNameAdapter);
    }

    private void setBadges(ArrayList<GamerProfileModel.Data.LevelsData.LevelData> levelsDataArrayList) {
        rv_badges.setLayoutManager(badgesLinearLayout);
        rv_badges.setItemAnimator(new DefaultItemAnimator());
        myBadgesAdapter = new MyBadgesAdapter(this, levelsDataArrayList);
        rv_badges.setAdapter(myBadgesAdapter);

    }

    private void setEnable() {
        et_fname.setEnabled(true);
        et_lname.setEnabled(true);
        et_states.setEnabled(true);
        profile_image.setClickable(true);
        if (Pref.getValue(ActivityProfile.this, Constants.EmailAddress, "", Constants.FILENAME).isEmpty())
            et_email.setEnabled(true);
        */
/*if (gamesDataArrayList.get(0).UniqueName.isEmpty())
        {
            et_pubg_name.setEnabled(true);
        }
        else
        {
        }*//*



        */
/*if (gamesDataArrayList.get(1).UniqueName.isEmpty())
            et_cr_name.setEnabled(true);*//*


        if (Pref.getValue(ActivityProfile.this, Constants.TeamName, "", Constants.FILENAME).isEmpty())
            et_team_name.setEnabled(true);

        txt_date_birth.setEnabled(true);
        mImgEditImage.setVisibility(View.VISIBLE);
    }


    private void selectState() {
        if (ActivityProfile.this == null)
            return;
        if (stateslist.size() == 0) {
            Constants.SnakeMessageYellow(mRelativeEditProfileRootView, "no States found.");
            return;
        }
        Dialog dialog = new Dialog(ActivityProfile.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View outerView = LayoutInflater.from(ActivityProfile.this).inflate(R.layout.wheelview, null);
        dialog.setContentView(outerView);
        if (dialog.getWindow() != null)
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        CustomWheelView wv = outerView.findViewById(R.id.wh_view);
        TextView txt_ok = outerView.findViewById(R.id.txt_ok);
        TextView title = outerView.findViewById(R.id.title);
        title.setText("Select state");
        TextView txt_cancel = outerView.findViewById(R.id.txt_cancel);
        txt_ok.setOnClickListener(v ->
        {
            et_states.setText(wv.getSeletedItem().toString());
            dialog.dismiss();

        });

        txt_cancel.setOnClickListener(v -> dialog.dismiss());
        wv.setOffset(1);
        wv.setItems(stateslist);
        wv.setSeletion(0);
        dialog.show();
    }


    private void validate() throws IOException {
       */
/* if (pickResult==null)
        {
            Toast.makeText(this,"Select image of Profile image.",Toast.LENGTH_SHORT).show();
        }*//*

        int position = stateslist.indexOf(et_states.getText().toString());
        if (et_fname.getText().length() < 3)

            Constants.SnakeMessageYellow(parent_layout, "Enter valid first name");

        else if (et_lname.getText().length() < 3)
            Constants.SnakeMessageYellow(parent_layout, "Enter valid last name.");

        else if (et_states.getText().toString().isEmpty())
            Constants.SnakeMessageYellow(parent_layout, "Select State");

        */
/*else if (isRestricteed.get(position).equalsIgnoreCase("2"))
             Constants.SnakeMessageYellow(parent_layout,getResources().getString(R.string.restricted_states_msg));*//*


        else if (et_team_name.getText().length() < 4)
            Constants.SnakeMessageYellow(parent_layout, "Enter Team name");

         */
/*else if(et_pubg_name.getText().length()<3)
        {
            Constants.SnakeMessageYellow(parent_layout,"Enter PUBG name");
        }
        else if(et_cr_name.getText().length()<3)
        {
            Constants.SnakeMessageYellow(parent_layout,"Enter Clash Royal name");
        }*//*

        else {
            sendToServer();
        }
    }

    private void sendToServer() throws IOException {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        File file = null;
        if (pickResult != null) {
            file = new File(pickResult.getPath());
            OutputStream os = null;
            try {
                os = new BufferedOutputStream(new FileOutputStream(file));
                pickResult.getBitmap().compress(Bitmap.CompressFormat.JPEG, 100, os);
                os.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        RequestParams requestParams = new RequestParams();
        try {
            Common.insertLog("Filepath:::> " + file);
            requestParams.put("Action", Constants.UPDATE_USER_PROFILE);
            requestParams.put("Val_Userid", Pref.getValue(this, Constants.UserID, "", Constants.FILENAME));
            if (pickResult != null)
                requestParams.put("Val_Uprofileimage", file);
            requestParams.put("Val_Ufirstname", et_fname.getText().toString());
            requestParams.put("Val_Ulastname", et_lname.getText().toString());

            requestParams.put("Val_Ubirthdate", dob);
            requestParams.put("Val_Ustate", statesId.get(stateslist.indexOf(et_states.getText().toString())));
            requestParams.put("Val_Uteamname", et_team_name.getText().toString());
            requestParams.put("Val_Uemailaddress", et_email.getText().toString());
            requestParams.put("Val_Uusergameid", player_games_usergameid.toString());
            requestParams.put("Val_Uuniquename", player_games_username_list.toString());

            Common.insertLog("Request:::> " + requestParams.toString());
            editable = false;
            txt_save.setText("Edit");

            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            asyncHttpClient.setTimeout(40 * 1000);
            asyncHttpClient.post(this, Constants.BASE_API + Constants.UPDATE_PROFILE_V2, requestParams, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Common.insertLog("Response:::> " + response.toString());
                    progressDialog.dismiss();
                    if (response.optString("status").equalsIgnoreCase("success")) {

                        Pref.setValue(ActivityProfile.this, Constants.FullName, response.optJSONObject("data").optString("FullName"), Constants.FILENAME);
                        Pref.setValue(ActivityProfile.this, Constants.TeamName, response.optJSONObject("data").optString("TeamName"), Constants.FILENAME);
                        Pref.setValue(ActivityProfile.this, Constants.firstname, response.optJSONObject("data").optString("FirstName"), Constants.FILENAME);
                        Pref.setValue(ActivityProfile.this, Constants.lastname, response.optJSONObject("data").optString("LastName"), Constants.FILENAME);
                        Pref.setValue(ActivityProfile.this, Constants.CountryCode, response.optJSONObject("data").optString("CountryCode"), Constants.FILENAME);
                        Pref.setValue(ActivityProfile.this, Constants.EmailAddress, response.optJSONObject("data").optString("EmailAddress"), Constants.FILENAME);
                        Pref.setValue(ActivityProfile.this, Constants.MobileNumber, response.optJSONObject("data").optString("MobileNumber"), Constants.FILENAME);
                        Pref.setValue(ActivityProfile.this, Constants.State, response.optJSONObject("data").optString("State"), Constants.FILENAME);
                        Pref.setValue(ActivityProfile.this, Constants.BirthDate, txt_date_birth.getText().toString(), Constants.FILENAME);
                        Pref.setValue(ActivityProfile.this, Constants.ProfileImage, response.optJSONObject("data").optString("ProfileImage"), Constants.FILENAME);
                        Glide.with(ActivityProfile.this).load(Pref.getValue(ActivityProfile.this, Constants.ProfileImage, "", Constants.FILENAME)).into(profile_image);

                        JSONArray gamesDataArray = response.optJSONObject("data").optJSONObject("GamesData").optJSONArray("GamesData");
                        gameUsernameModelArrayList.clear();

                        ArrayList<GamesDataModel> gamesDataArrayList = new ArrayList<>();

                        for (int i = 0; i < gamesDataArray.length(); i++) {
                            GamesDataModel gameData = new GamesDataModel();
                            gameData.UniqueName = gamesDataArray.optJSONObject(i).optString("UniqueName");
                            gameData.Status = gamesDataArray.optJSONObject(i).optString("Status");
                            gameData.GameID = gamesDataArray.optJSONObject(i).optString("GameID");
                            gameData.UserGameID = gamesDataArray.optJSONObject(i).optString("UserGameID");
                            gamesDataArrayList.add(gameData);

                            GameUsernameModel gameUsernameModel = new GameUsernameModel();
                            gameUsernameModel.setGameID(gamesDataArray.optJSONObject(i).optString("GameID"));
                            gameUsernameModel.setGameName(gamesDataArray.optJSONObject(i).optString("GameName"));
                            gameUsernameModel.setUniqueName(gamesDataArray.optJSONObject(i).optString("UniqueName"));
                            gameUsernameModel.setUserGameID(gamesDataArray.optJSONObject(i).optString("UserGameID"));
                            gameUsernameModel.setEnable(false);

                            gameUsernameModelArrayList.add(gameUsernameModel);
                        }

                        gameUserNameAdapter = new GameUserNameAdapter(ActivityProfile.this, gameUsernameModelArrayList);
                        rv_game_username.setAdapter(gameUserNameAdapter);
                        setDisableEditText();

                        Constants.setUserGameDataLoopj(gamesDataArrayList, ActivityProfile.this);
                        Constants.SnakeMessageYellow(parent_layout, response.optString("message"));
                        Constants.getUserData(ActivityProfile.this);
                    } else {
                        Constants.SnakeMessageYellow(parent_layout, response.optString("message"));
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    progressDialog.dismiss();
                    Constants.SnakeMessageYellow(parent_layout, "Something went wrong.Please try again");
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    progressDialog.dismiss();
                    Constants.SnakeMessageYellow(parent_layout, "Something went wrong.Please try again");
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progressDialog.dismiss();
                    Constants.SnakeMessageYellow(parent_layout, "Something went wrong.Please try again");
                    super.onFailure(statusCode, headers, responseString, throwable);
                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Constants.SnakeMessageYellow(parent_layout, "Something went wrong.Please try again");
        }
    }

    private void setDisableEditText() {
        // et_pubg_name.setEnabled(false);
        et_fname.setEnabled(false);
        et_lname.setEnabled(false);
        et_email.setEnabled(false);
        et_phone.setEnabled(false);
        et_team_name.setEnabled(false);
        //et_cr_name.setEnabled(false);
        et_states.setEnabled(false);
        txt_date_birth.setEnabled(false);
        profile_image.setClickable(false);
        mImgEditImage.setVisibility(View.GONE);
    }

    @Override
    public void onPickResult(PickResult pickResult) {
        if (pickResult.getError() == null) {
            Glide.with(ActivityProfile.this).load(pickResult.getUri()).into(profile_image);
            this.pickResult = pickResult;
        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, pickResult.getError().getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void getAllStates() {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityProfile.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<StatesListModel> signUpModelCall = apiInterface.getAllStates(Constants.GET_ALL_STATES);

        signUpModelCall.enqueue(new Callback<StatesListModel>() {
            @Override
            public void onResponse(@NonNull Call<StatesListModel> call, @NonNull Response<StatesListModel> response) {
                progressDialog.dismiss();
                StatesListModel statesListModel = response.body();
                assert statesListModel != null;
                if (statesListModel.status.equalsIgnoreCase("success")) {
                    for (int i = 0; i < statesListModel.userDataClass.statesDataArrayList.size(); i++) {
                        stateslist.add(statesListModel.userDataClass.statesDataArrayList.get(i).Name);
                        statesId.add(statesListModel.userDataClass.statesDataArrayList.get(i).StateID);
                        isRestricteed.add(statesListModel.userDataClass.statesDataArrayList.get(i).IsRestricted);
                        if (Pref.getValue(ActivityProfile.this, Constants.State, "", Constants.FILENAME).equalsIgnoreCase("")) {
                            et_states.setHint("Select a State");
                            et_states.setEnabled(true);
                        } else {
                            et_states.setText(Pref.getValue(ActivityProfile.this, Constants.State, "", Constants.FILENAME));
                            et_states.setEnabled(false);
                        }
                    }

                } else {
                    Constants.SnakeMessageYellow(parent_layout, statesListModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<StatesListModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    public BitmapDrawable writeOnDrawable(int drawableId, String text) {

        Bitmap bm = BitmapFactory.decodeResource(getResources(), drawableId).copy(Bitmap.Config.ARGB_8888, true);

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.BLACK);
        paint.setTextSize(20);

        Canvas canvas = new Canvas(bm);
        canvas.drawText(text, 0, bm.getHeight() / 2, paint);

        return new BitmapDrawable(bm);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_info1: {

                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                txt_bottom_msg.setText(getResources().getString(R.string.pubg_info));
                bg.setVisibility(View.VISIBLE);
                break;
            }

            case R.id.img_info2: {
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                txt_bottom_msg.setText(getResources().getString(R.string.cr_info));
                bg.setVisibility(View.VISIBLE);
                break;
            }
            case R.id.img_info3: {
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                txt_bottom_msg.setText(getResources().getString(R.string.points_info));
                bg.setVisibility(View.VISIBLE);
                break;
            }
            case R.id.info_team: {
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                txt_title_bs.setText(getResources().getString(R.string.team_name));
                txt_bottom_msg.setText(getResources().getString(R.string.team_info));
                bg.setVisibility(View.VISIBLE);
                break;
            }

            */
/*case R.id.info_pubg:
            {
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                txt_title_bs.setText(getResources().getString(R.string.pubg_user_id));
                txt_bottom_msg.setText(getResources().getString(R.string.pubgid_info));
                bg.setVisibility(View.VISIBLE);
                break;
            }
            case R.id.info_cr:
            {
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                txt_title_bs.setText(getResources().getString(R.string.clash_royal_user_id));
                txt_bottom_msg.setText(getResources().getString(R.string.crid_info));
                bg.setVisibility(View.VISIBLE);
                break;
            }*//*


            case R.id.img_close: {
                sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                bg.setVisibility(View.GONE);
                break;
            }
            case R.id.img_close_badges: {
                badgesheet.setState(BottomSheetBehavior.STATE_HIDDEN);
                bg.setVisibility(View.GONE);
                break;
            }

            case R.id.back_arrow: {
                Objects.requireNonNull(rv_badges.getLayoutManager()).scrollToPosition(badgesLinearLayout.findFirstVisibleItemPosition() - 1);
                break;
            }
            case R.id.front_arrow: {
                Objects.requireNonNull(rv_badges.getLayoutManager()).scrollToPosition(badgesLinearLayout.findLastVisibleItemPosition() + 1);
                break;
            }
            case R.id.txt_date_birth: {

                try {
                    Calendar calendar = Calendar.getInstance();
                    Calendar mMaxDate = Calendar.getInstance();
                    Calendar mMinDate = Calendar.getInstance();
                    mMaxDate.add(Calendar.YEAR, -18);
                    mMaxDate.setTimeInMillis(mMaxDate.getTimeInMillis());
                    mMinDate.set(Calendar.YEAR, 1920);
                    mMinDate.set(Calendar.MONTH, 1);
                    mMinDate.set(Calendar.DATE, 1);

                    int mMaxDateYear = mMaxDate.get(Calendar.YEAR);
                    int mMaxDateMonth = mMaxDate.get(Calendar.MONTH);
                    int mMaxDateDay = mMaxDate.get(Calendar.DAY_OF_MONTH);

                    int mMinDateYear = mMinDate.get(Calendar.YEAR);
                    int mMinDateMonth = mMinDate.get(Calendar.MONTH);
                    int mMinDateDay = mMinDate.get(Calendar.DAY_OF_MONTH);


                    new SpinnerDatePickerDialogBuilder().context(ActivityProfile.this).callback(this).spinnerTheme(R.style.NumberPickerStyle)
                            .showTitle(true).showDaySpinner(true).defaultDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                            .maxDate(mMaxDateYear, mMaxDateMonth, mMaxDateDay)
                            .minDate(mMinDateYear, mMinDateMonth, mMinDateDay).build().show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
            case R.id.et_states: {
                selectState();
                break;
            }
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        txt_date_birth.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
        dob = year + "/" + (monthOfYear + 1) + "/" + dayOfMonth;
    }
}
*/
