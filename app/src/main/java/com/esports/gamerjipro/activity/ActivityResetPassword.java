package com.esports.gamerjipro.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.esports.gamerjipro.Model.SignInModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.Pref;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityResetPassword extends AppCompatActivity {
    EditText et_conf_pwd, et_password;
    RelativeLayout lyt_save_pwd;
    LinearLayout parent_layout;
    APIInterface apiInterface;
    boolean fgt_pass = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        et_conf_pwd = findViewById(R.id.et_conf_pwd);
        et_password = findViewById(R.id.et_password);
        parent_layout = findViewById(R.id.parent_layout);
        lyt_save_pwd = findViewById(R.id.lyt_save_pwd);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        lyt_save_pwd.setOnClickListener(v ->
                validate());
    }

    private void validate() {
        if (et_password.getText().length() < 6) {
            et_password.setError("Password must be 6 character long.");
        } else if (!(et_password.getText().toString().equals(et_conf_pwd.getText().toString()))) {
            et_conf_pwd.setError("Password and Confirm password doesn't match.");
        } else
            sendToServer();

    }

    private void sendToServer() {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityResetPassword.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<SignInModel> signUpModelCall = apiInterface.resetPassword(Constants.RESET_PWD, Pref.getValue(ActivityResetPassword.this, Constants.UserID, "", Constants.FILENAME), et_password.getText().toString());

        signUpModelCall.enqueue(new Callback<SignInModel>() {
            @Override
            public void onResponse(@NonNull Call<SignInModel> call, @NonNull Response<SignInModel> response) {
                progressDialog.dismiss();
                SignInModel signInModel = response.body();
                assert signInModel != null;
                if (signInModel.status.equalsIgnoreCase("success")) {
                    Toast.makeText(ActivityResetPassword.this, signInModel.message, Toast.LENGTH_SHORT).show();
                    Pref.setValue(ActivityResetPassword.this, Constants.UserID, signInModel.userDataClass.UserID, Constants.FILENAME);
                    Pref.setValue(ActivityResetPassword.this, Constants.FullName, signInModel.userDataClass.FullName, Constants.FILENAME);
                    Pref.setValue(ActivityResetPassword.this, Constants.CountryCode, signInModel.userDataClass.CountryCode, Constants.FILENAME);
                    Pref.setValue(ActivityResetPassword.this, Constants.MobileNumber, signInModel.userDataClass.MobileNumber, Constants.FILENAME);
                    Pref.setValue(ActivityResetPassword.this, Constants.EmailAddress, signInModel.userDataClass.EmailAddress, Constants.FILENAME);
                    Constants.setUserGameData(signInModel.userDataClass.GamesData.gamesDataArrayList, ActivityResetPassword.this);
                    Pref.setValue(ActivityResetPassword.this, Constants.IS_LOGIN, true, Constants.FILENAME);

//                    Intent i = new Intent(ActivityResetPassword.this, ActivityMain.class);
                    Intent i = new Intent(ActivityResetPassword.this, BottomNavigationActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                } else {
                    Constants.SnakeMessageYellow(parent_layout, signInModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<SignInModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }
}
