package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esports.gamerjipro.Adapter.TournamentTimeAdapter;
import com.esports.gamerjipro.Adapter.WinnerPoolAdapter;
import com.esports.gamerjipro.Interface.TournamentTimeSelectedListener;
import com.esports.gamerjipro.Model.JoinContestModel;
import com.esports.gamerjipro.Model.ReportsDataModel;
import com.esports.gamerjipro.Model.RulesModel;
import com.esports.gamerjipro.Model.SignInModel;
import com.esports.gamerjipro.Model.SingleContestModel;
import com.esports.gamerjipro.Model.TournamentTimeSlotModel;
import com.esports.gamerjipro.Model.WalletUsageLimitModel;
import com.esports.gamerjipro.Model.WinnerContestModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.AnalyticsSocket;
import com.esports.gamerjipro.Utils.AppConstants;
import com.esports.gamerjipro.Utils.Common;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.FileUtils;
import com.esports.gamerjipro.Utils.Pref;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JoinContestViaShare extends AppCompatActivity implements View.OnClickListener, TournamentTimeSelectedListener {
    TextView txt_date, txt_time, txt_map, txt_persp, winning_amount, txt_winners, txt_perkill, txt_entry_fees, txt_available_spots, txt_entry_rupee, txt_contest_title, txt_title,
            txt_available_remaining, txt_game_name, txt_join_contest, txt_perspective_title, txt_map_title, txt_confirm, txt_winner_title, txt_perkill_title, txt_rupee_symbol, rupees_symbol, txt_join;
    LinearLayout lyt_winners_drop, lyt_kills, lyt_winners, ly_entry_fees, lyt_winnings, tournament_time_bottom;
    ProgressBar progress_bar;
    LinearLayout lyt_winning_amount;
    String contest_id, Val_IsWithUsers = "1";
    APIInterface apiInterface;
    RelativeLayout lyt_sub_main, bottom_how_to;
    BottomSheetBehavior sheetBehavior, bottom_sheet_cash_bonus, pool_price_bottomsheet, rules_bottomsheet, tournamnet_time_bottomsheet;
    LinearLayout layoutBottomSheet, bonus_bottom_layout, pool_price_bottom;
    View bg;
    SingleContestModel singleContestModel;
    TextView txt_pool_price, txt_winning_rupee;
    TextView tv_pool_price, txt_my_balance, tv_title;
    WinnerPoolAdapter winnerPoolAdapter;
    RecyclerView rv_pool_price, rv_tournament_time;
    CardView join_contest, cv_join_contest, cv_video_join, cv_contest, lyt_header;
    ArrayList<SignInModel.UserData.GamesData.GameData> userGameDataarrayList = new ArrayList<>();
    public String UniqueName = " ", game_name, userGameId;
    public TextView txt_usable_cash, txt_to_pay;
    int CanJoinPlayers, CanJoinExtraPlayers;
    boolean JoinFlag = true;
    float add_balance, my_balance, topay;
    ImageView img_info, img_close, img_close_cash, img_close_join;
    String JoinButtonFlag;
    ImageView img_back, img_close_rules;
    TextView txt_warning, txt_game_type;
    private int currentPage = 0;
    Timer swipeTimer;
    private int NUM_PAGES = 0;
    AnalyticsSocket analyticsSocket = new AnalyticsSocket();
    boolean sendSwipeImpression = true;
    View lyt_tournament_detail;
    SingleContestModel.Data data;
    ImageView iv_close_tournament_time, img_image;
    TextView txt_date_t, txt_time_t, txt_map_t, txt_persp_t, winning_amount_t, txt_winners_t, txt_perkill_t, txt_entry_fees_t, txt_available_spots_t, txt_entry_rupee_t, txt_title_t,
            txt_available_remaining_t, txt_join_contest_t, txt_perspective_title_t, txt_map_title_t, txt_confirm_t, txt_winner_title_t, txt_perkill_title_t, txt_rupee_symbol_t, rupees_symbol_t, txt_tournament_name, txt_tournament_rules, txt_type;
    LinearLayout lyt_winners_drop_t, lyt_kills_t, lyt_winners_t, ly_entry_fees_t, lyt_winnings_t, lyt_rules;
    ProgressBar progress_bar_t;
    RelativeLayout lyt_organization;
    LinearLayout lyt_winning_amount_t, lyt_contest_item;
    CardView cv_apply_coupon, cv_tournament_time_next;
    FrameLayout lyt_frame;
    TournamentTimeAdapter tournamentTimeAdapter;
    String main_game_name;
    TextView txt_tournament_time_title, tv_tournament_time_type, txt_tournament_name_bottom, txt_tournament_date, txt_tournamnet_time_notes, txt_title_bs, txt_bottom_msg;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_join_contest_share_main);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        lyt_sub_main = findViewById(R.id.lyt_sub_main);
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(v ->
        {
//            Intent intent = new Intent(JoinContestViaShare.this, ActivityMain.class);
            Intent intent = new Intent(JoinContestViaShare.this, BottomNavigationActivity.class);
            startActivity(intent);
            finish();
        });
        txt_date = findViewById(R.id.txt_date);
        txt_contest_title = findViewById(R.id.txt_contest_title);
        txt_type = findViewById(R.id.txt_type);
        txt_game_name = findViewById(R.id.txt_game_name);
        txt_title = findViewById(R.id.txt_title);
        txt_time = findViewById(R.id.txt_time);
        txt_warning = findViewById(R.id.txt_warning);
        txt_game_type = findViewById(R.id.txt_game_type);
        lyt_tournament_detail = findViewById(R.id.lyt_tournament_detail);
        txt_title_bs = findViewById(R.id.txt_title_bs);
        txt_bottom_msg = findViewById(R.id.txt_bottom_msg);

        txt_map = findViewById(R.id.txt_map);
        txt_persp = findViewById(R.id.txt_persp);
        winning_amount = findViewById(R.id.winning_amount);
        txt_winners = findViewById(R.id.txt_winners);
        txt_perkill = findViewById(R.id.txt_perkill);
        txt_entry_fees = findViewById(R.id.txt_entry_fees);
        txt_available_spots = findViewById(R.id.txt_available_spots);
        txt_available_remaining = findViewById(R.id.txt_available_remaining);
        txt_join_contest = findViewById(R.id.txt_join_contest);
        txt_join = findViewById(R.id.txt_join);
        progress_bar = findViewById(R.id.progress_bar);
        txt_perspective_title = findViewById(R.id.txt_perspective_title);
        txt_map_title = findViewById(R.id.txt_map_title);
        txt_confirm = findViewById(R.id.txt_confirm);
        lyt_winners_drop = findViewById(R.id.lyt_winners_drop);
        txt_winner_title = findViewById(R.id.txt_winner_title);
        lyt_winners = findViewById(R.id.lyt_winners);
        lyt_kills = findViewById(R.id.lyt_kills);
        ly_entry_fees = findViewById(R.id.ly_entry_fees);
        lyt_winnings = findViewById(R.id.lyt_winnings);
        txt_perkill_title = findViewById(R.id.txt_perkill_title);
        txt_rupee_symbol = findViewById(R.id.txt_rupee_symbol);
        rupees_symbol = findViewById(R.id.rupees_symbol);
        lyt_winning_amount = findViewById(R.id.lyt_winning_amount);
        txt_entry_rupee = findViewById(R.id.txt_entry_rupee);
        bg = findViewById(R.id.bg);
        txt_pool_price = findViewById(R.id.txt_pool_price);
        txt_winning_rupee = findViewById(R.id.txt_winning_rupee);
        tv_pool_price = findViewById(R.id.tv_pool_price);
        join_contest = findViewById(R.id.join_contest);
        cv_join_contest = findViewById(R.id.cv_join_contest);
        txt_usable_cash = findViewById(R.id.txt_usable_cash);
        txt_to_pay = findViewById(R.id.txt_to_pay);
        txt_my_balance = findViewById(R.id.txt_my_balance);

        txt_date_t = findViewById(R.id.txt_date_t);
        txt_time_t = findViewById(R.id.txt_time_t);
        txt_title = findViewById(R.id.txt_title);
        txt_map_t = findViewById(R.id.txt_map_t);
        txt_persp_t = findViewById(R.id.txt_persp_t);
        winning_amount_t = findViewById(R.id.winning_amount_t);
        txt_winners_t = findViewById(R.id.txt_winners_t);
        txt_perkill_t = findViewById(R.id.txt_perkill_t);
        txt_tournament_name = findViewById(R.id.txt_tournament_name);
        txt_tournament_rules = findViewById(R.id.txt_tournament_rules);
        txt_entry_fees_t = findViewById(R.id.txt_entry_fees_t);
        txt_available_spots_t = findViewById(R.id.txt_available_spots_t);
        txt_available_remaining_t = findViewById(R.id.txt_available_remaining_t);
        txt_join_contest_t = findViewById(R.id.txt_join_contest_t);
        progress_bar_t = findViewById(R.id.progress_bar_t);
        txt_perspective_title_t = findViewById(R.id.txt_perspective_title_t);
        txt_map_title_t = findViewById(R.id.txt_map_title_t);
        txt_confirm_t = findViewById(R.id.txt_confirm_t);
        lyt_winners_drop_t = findViewById(R.id.lyt_winners_drop_t);
        txt_winner_title_t = findViewById(R.id.txt_winner_title_t);
        lyt_winners_t = findViewById(R.id.lyt_winners_t);
        lyt_kills_t = findViewById(R.id.lyt_kills_t);
        ly_entry_fees_t = findViewById(R.id.ly_entry_fees_t);
        lyt_winnings_t = findViewById(R.id.lyt_winnings_t);
        txt_perkill_title_t = findViewById(R.id.txt_perkill_title_t);
        txt_rupee_symbol_t = findViewById(R.id.txt_rupee_symbol_t);
        rupees_symbol = findViewById(R.id.rupees_symbol_t);
        lyt_winning_amount_t = findViewById(R.id.lyt_winning_amount_t);
        txt_entry_rupee_t = findViewById(R.id.txt_entry_rupee_t);
        txt_title_t = findViewById(R.id.txt_title_t);
        cv_apply_coupon = findViewById(R.id.cv_apply_coupon);
        lyt_organization = findViewById(R.id.lyt_organization);
        lyt_contest_item = findViewById(R.id.lyt_contest_item);
        lyt_rules = findViewById(R.id.lyt_rules);
        lyt_frame = findViewById(R.id.lyt_frame);

        img_info = findViewById(R.id.img_info);
        img_close = findViewById(R.id.iv_close_pool_price);
        img_close_join = findViewById(R.id.img_close_join);
        img_close_cash = findViewById(R.id.img_close_cash);

        tv_title = findViewById(R.id.tv_title);

        layoutBottomSheet = findViewById(R.id.bottom_sheet);

        cv_video_join = layoutBottomSheet.findViewById(R.id.cv_video_join);
        cv_contest = findViewById(R.id.cv_contest);
        lyt_header = findViewById(R.id.lyt_header);
        bonus_bottom_layout = findViewById(R.id.bottom_sheet_cash_bonus);
        pool_price_bottom = findViewById(R.id.pool_price_bottom);
        img_image = findViewById(R.id.img_image);

        tournament_time_bottom = findViewById(R.id.tournament_time_bottomsheet);
        txt_tournament_time_title = tournament_time_bottom.findViewById(R.id.txt_tournament_time_title);
        iv_close_tournament_time = tournament_time_bottom.findViewById(R.id.iv_close_tournament_time);
        tv_tournament_time_type = tournament_time_bottom.findViewById(R.id.tv_tournament_time_type);
        txt_tournament_name_bottom = tournament_time_bottom.findViewById(R.id.txt_tournament_name);
        txt_tournament_date = tournament_time_bottom.findViewById(R.id.txt_tournament_date);
        txt_tournamnet_time_notes = tournament_time_bottom.findViewById(R.id.txt_tournamnet_time_notes);
        rv_tournament_time = tournament_time_bottom.findViewById(R.id.rv_tournament_time);
        cv_tournament_time_next = tournament_time_bottom.findViewById(R.id.cv_tournament_time_next);

        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        bottom_sheet_cash_bonus = BottomSheetBehavior.from(bonus_bottom_layout);
        pool_price_bottomsheet = BottomSheetBehavior.from(pool_price_bottom);
        tournamnet_time_bottomsheet = BottomSheetBehavior.from(tournament_time_bottom);

        rv_pool_price = findViewById(R.id.rv_pool_price);
        rv_pool_price.setLayoutManager(new LinearLayoutManager(this));
        rv_pool_price.setItemAnimator(new DefaultItemAnimator());

        bottom_how_to = findViewById(R.id.bottom_how_to);
        img_close_rules = bottom_how_to.findViewById(R.id.img_close);
        rules_bottomsheet = BottomSheetBehavior.from(bottom_how_to);

        img_close_rules.setOnClickListener(v ->
        {
            rules_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
            bg.setVisibility(View.GONE);
        });

        iv_close_tournament_time.setOnClickListener(v ->
        {
            tournamnet_time_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
            bg.setVisibility(View.GONE);
        });

        rules_bottomsheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        bg.setVisibility(View.VISIBLE);
                    }
                    break;

                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        bg.setVisibility(View.GONE);
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:

                    case BottomSheetBehavior.STATE_SETTLING: {
                        break;
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });


        img_info.setOnClickListener(v -> bottom_sheet_cash_bonus.setState(BottomSheetBehavior.STATE_EXPANDED));
        img_close_cash.setOnClickListener(v ->
        {
            bottom_sheet_cash_bonus.setState(BottomSheetBehavior.STATE_COLLAPSED);
            bg.setVisibility(View.GONE);
        });
        img_close.setOnClickListener(v ->
        {
            pool_price_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
            bottom_sheet_cash_bonus.setState(BottomSheetBehavior.STATE_COLLAPSED);
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            bg.setVisibility(View.GONE);
        });

        img_close_join.setOnClickListener(v ->
        {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            bottom_sheet_cash_bonus.setState(BottomSheetBehavior.STATE_COLLAPSED);
            pool_price_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
            bg.setVisibility(View.GONE);
        });

        if (getIntent().hasExtra("contest_id")) {
            contest_id = getIntent().getStringExtra("contest_id");
            getSingleContest(contest_id);
            data = (SingleContestModel.Data) getIntent().getSerializableExtra("tournament_data");
            lyt_tournament_detail.setVisibility(View.GONE);
            lyt_winners.setOnClickListener(this);

            cv_video_join.setOnClickListener(this);
        } else if (getIntent().hasExtra("tournament_id")) {
            cv_contest.setVisibility(View.GONE);
            lyt_tournament_detail.setVisibility(View.VISIBLE);
            data = (SingleContestModel.Data) getIntent().getSerializableExtra("tournament_data");
            setTournamentData(data);
        }

        userGameDataarrayList = Constants.getUserData(JoinContestViaShare.this);


        pool_price_bottomsheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        bg.setVisibility(View.VISIBLE);
                    }
                    break;

                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        bg.setVisibility(View.GONE);
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:

                    case BottomSheetBehavior.STATE_SETTLING: {
                        break;
                    }
                }

            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        bg.setVisibility(View.VISIBLE);
                    }
                    break;

                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        bg.setVisibility(View.GONE);
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:

                    case BottomSheetBehavior.STATE_SETTLING: {
                        break;
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });

        tournamnet_time_bottomsheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        bg.setVisibility(View.VISIBLE);
                    }
                    break;

                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        bg.setVisibility(View.GONE);
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:

                    case BottomSheetBehavior.STATE_SETTLING: {
                        break;
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        swipeTimer = new Timer();

        join_contest.setOnClickListener(this);
        cv_join_contest.setOnClickListener(this);
    }

    private void setTournamentData(SingleContestModel.Data data) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM-yyyy");
        @SuppressLint("SimpleDateFormat") DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        @SuppressLint("SimpleDateFormat") DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
        String inputDateStr = data.Date;
        Date date = null;
        try {
            date = inputFormat.parse(inputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Glide.with(JoinContestViaShare.this).load(data.GameTypeFeaturedImage).into(img_image);
        String outputDateStr = outputFormat.format(date);
        cv_apply_coupon.setVisibility(View.VISIBLE);
        lyt_organization.setVisibility(View.VISIBLE);
        txt_date_t.setText(outputDateStr);
        txt_contest_title.setText(data.Title);
        txt_join_contest_t.setVisibility(View.GONE);
        txt_time_t.setText(FileUtils.getFormatedDateTime(data.Time, "HH:mm:ss", "hh:mm a"));
        txt_map_t.setText(data.Map_Length);
        txt_persp_t.setText(data.Perspective_LevelCap);
        txt_title_t.setText(data.GameTypeName);
        txt_title_t.setTextSize(12);
        txt_title.setText(data.GameName);
        txt_tournament_name.setText(data.Title);

        if (data.WinningAmount.equalsIgnoreCase("") || data.WinningAmount.equalsIgnoreCase("0")) {
            winning_amount_t.setText("-");
            rupees_symbol_t.setVisibility(View.GONE);
        } else
            winning_amount_t.setText(" " + data.WinningAmount);

        txt_winners_t.setText(data.WinnersCount);
        txt_perkill_title_t.setText(data.PerKill_MaxLosesTitle);
        txt_perkill_t.setText(" " + data.PerKill_MaxLoses);

        if (data.PerKill_MaxLosesCurrency) {
            txt_rupee_symbol_t.setVisibility(View.VISIBLE);
        } else {
            txt_rupee_symbol_t.setVisibility(View.GONE);
        }
        if (data.WinnersCount.isEmpty() && data.PerKill_MaxLoses.isEmpty()) {
            lyt_winners_t.setVisibility(View.GONE);
            lyt_kills_t.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 3.0f);
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 1.0f);
            lyt_winnings_t.setLayoutParams(params);
            ly_entry_fees_t.setLayoutParams(params1);

        } else if (data.WinnersCount.isEmpty()) {
            lyt_winners_t.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 2.0f);
            lyt_kills_t.setLayoutParams(params);
        } else if (data.PerKill_MaxLoses.isEmpty()) {
            lyt_kills_t.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 2.0f);
            lyt_winners_t.setLayoutParams(params);
        } else {
            if (Integer.parseInt(data.WinnersCount) == 1)
                txt_winner_title_t.setText("Winner");

            if (Integer.parseInt(data.PerKill_MaxLoses) == 1)
                txt_perkill_t.setText(" " + data.PerKill_MaxLoses);
        }

        if (data.EntryFee.equalsIgnoreCase("0")) {
            txt_entry_fees_t.setText("Free");
            txt_entry_rupee_t.setVisibility(View.GONE);
        } else
            txt_entry_fees_t.setText(" " + data.EntryFee);

        txt_map_title_t.setText(data.Map_LengthTitle);
        txt_perspective_title_t.setText(data.Perspective_LevelCapTitle);
        int available_spots = Integer.parseInt(data.TotalSpots);
        int joined_spots = Integer.parseInt(data.JoinedSpots);
        int remaining_spots = available_spots - joined_spots;

        if (remaining_spots > 1)
            txt_available_spots_t.setText(remaining_spots + " players remaining");
        else
            txt_available_spots_t.setText(remaining_spots + " player remaining");

        if (joined_spots > 1)
            txt_available_remaining_t.setText(joined_spots + " players joined");
        else
            txt_available_remaining_t.setText(joined_spots + " player joined");

        progress_bar_t.setMax(available_spots);
        progress_bar_t.setProgress(joined_spots);
        progress_bar_t.setProgress(joined_spots);

        if (data.ConfirmStatus.equalsIgnoreCase("1"))
            txt_confirm_t.setVisibility(View.VISIBLE);
        else
            txt_confirm_t.setVisibility(View.GONE);


        if (data.Joined) {
            txt_join.setText("JOINED");
            txt_join.setTextColor(getResources().getColor(R.color.orange_color));
        } else {
            txt_join.setText("Join now & Select Time");
        }
        lyt_winners_drop_t.setOnClickListener(this);
        lyt_rules.setOnClickListener(this);

        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) lyt_frame.getLayoutParams();
        params.setMargins(0, -30, 0, 0);
        lyt_frame.setLayoutParams(params);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent intent = new Intent(JoinContestViaShare.this, ActivityMain.class);
            Intent intent = new Intent(JoinContestViaShare.this, BottomNavigationActivity.class);
        startActivity(intent);
        finish();
    }

    private void getSingleContest(String contest_id) {
        final ProgressDialog progressDialog = new ProgressDialog(JoinContestViaShare.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<SingleContestModel> singleContestModelCall = apiInterface.getSingleContest(Constants.GET_CONTEST_DETAIL, Pref.getValue(JoinContestViaShare.this, Constants.UserID, "", Constants.FILENAME), contest_id, Val_IsWithUsers, "");

        singleContestModelCall.enqueue(new Callback<SingleContestModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<SingleContestModel> call, @NonNull Response<SingleContestModel> response) {
                progressDialog.dismiss();
                Common.insertLog("SINGLE CONTEST");
                singleContestModel = response.body();
                assert singleContestModel != null;
                if (singleContestModel.status.equalsIgnoreCase("success")) {
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM-yyyy");
                    @SuppressLint("SimpleDateFormat") DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                    @SuppressLint("SimpleDateFormat") DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
                    Glide.with(JoinContestViaShare.this).load(singleContestModel.DataClass.GameTypeFeaturedImage).into(img_image);
                    txt_contest_title.setText(singleContestModel.DataClass.GameTypeName);
                    txt_game_name.setText(singleContestModel.DataClass.TypeName);
                    lyt_header.setVisibility(View.GONE);
                    if (singleContestModel.DataClass.PrivateStatus)
                        txt_type.setVisibility(View.VISIBLE);
                    else
                        txt_type.setVisibility(View.GONE);

                    txt_title.setText(singleContestModel.DataClass.GameName);
                    String inputDateStr = singleContestModel.DataClass.Date;
                    Date date = null;
                    try {
                        date = inputFormat.parse(inputDateStr);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    String outputDateStr = outputFormat.format(date);

                    txt_date.setText(outputDateStr);

                    NUM_PAGES = singleContestModel.DataClass.AdsCount;
                    final Handler handler = new Handler();
                    final Runnable Update = () ->
                    {
                        if (currentPage == NUM_PAGES) {
                            currentPage = 0;
                            sendSwipeImpression = false;
                        } else {
                            if (sendSwipeImpression) {
                                analyticsSocket.sendAnalytics(singleContestModel.DataClass.adsDataArrayList.get(currentPage).AdType, singleContestModel.DataClass.adsDataArrayList.get(currentPage).AdID,
                                        singleContestModel.DataClass.adsDataArrayList.get(currentPage).AdImageName, Constants.AD_IMPRESSION, Pref.getValue(JoinContestViaShare.this, Constants.UserID, "", Constants.FILENAME),
                                        0);
                            }
                        }
                    };

                    swipeTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            handler.post(Update);
                        }
                    }, 0, 5000);


                    txt_time.setText(FileUtils.getFormatedDateTime(singleContestModel.DataClass.Time, "HH:mm:ss", "hh:mm a"));
                    txt_map.setText(singleContestModel.DataClass.Map_Length);
                    txt_persp.setText(singleContestModel.DataClass.Perspective_LevelCap);
                    txt_game_type.setText(singleContestModel.DataClass.GameName + "-" + singleContestModel.DataClass.GameTypeName);

                    Common.insertLog("Game Name:::>" +singleContestModel.DataClass.GameName);
                    if (singleContestModel.DataClass.GameName.equalsIgnoreCase(AppConstants.GAME_P_MOBILE)) {
                        txt_warning.setVisibility(View.VISIBLE);
                    }

                    if (singleContestModel.DataClass.WinningAmount.equalsIgnoreCase("") ||
                            singleContestModel.DataClass.WinningAmount.equalsIgnoreCase("0")) {
                        winning_amount.setText("-");
                        rupees_symbol.setVisibility(View.GONE);
                    } else
                        winning_amount.setText(" " + singleContestModel.DataClass.WinningAmount);

                    if (singleContestModel.DataClass.Joined)


                        txt_winners.setText(singleContestModel.DataClass.WinnersCount);
                    txt_perkill_title.setText(singleContestModel.DataClass.PerKill_MaxLosesTitle);
                    txt_perkill.setText(" " + singleContestModel.DataClass.PerKill_MaxLoses);

                    if (singleContestModel.DataClass.PerKill_MaxLosesCurrency) {
                        txt_rupee_symbol.setVisibility(View.VISIBLE);
                    } else {
                        txt_rupee_symbol.setVisibility(View.GONE);
                    }
                    if (singleContestModel.DataClass.WinnersCount.isEmpty() && singleContestModel.DataClass.PerKill_MaxLoses.isEmpty()) {
                        lyt_winners.setVisibility(View.GONE);
                        lyt_kills.setVisibility(View.GONE);
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 3.0f);
                        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 1.0f);
                        lyt_winnings.setLayoutParams(params);
                        ly_entry_fees.setLayoutParams(params1);

                    } else if (singleContestModel.DataClass.WinnersCount.isEmpty()) {
                        lyt_winners.setVisibility(View.GONE);
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 2.0f);
                        lyt_kills.setLayoutParams(params);
                    } else if (singleContestModel.DataClass.PerKill_MaxLoses.isEmpty()) {
                        lyt_kills.setVisibility(View.GONE);
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT, 2.0f);
                        lyt_winners.setLayoutParams(params);
                    } else {
                        if (Integer.parseInt(singleContestModel.DataClass.WinnersCount) == 1)
                            txt_winner_title.setText("Winner");

                        if (Integer.parseInt(singleContestModel.DataClass.PerKill_MaxLoses) == 1)
                            txt_perkill.setText(" " + singleContestModel.DataClass.PerKill_MaxLoses);
                    }

                    if (singleContestModel.DataClass.EntryFee.equalsIgnoreCase("0")) {
                        txt_entry_fees.setText("Free");
                        txt_entry_rupee.setVisibility(View.GONE);
                    } else
                        txt_entry_fees.setText(" " + singleContestModel.DataClass.EntryFee);

                    txt_map_title.setText(singleContestModel.DataClass.Map_LengthTitle);
                    txt_perspective_title.setText(singleContestModel.DataClass.Perspective_LevelCapTitle);
                    int available_spots = Integer.parseInt(singleContestModel.DataClass.TotalSpots);
                    int joined_spots = Integer.parseInt(singleContestModel.DataClass.JoinedSpots);
                    int remaining_spots = available_spots - joined_spots;

                    if (remaining_spots > 1)
                        txt_available_spots.setText(remaining_spots + " players remaining");
                    else
                        txt_available_spots.setText(remaining_spots + " player remaining");

                    if (joined_spots > 1)
                        txt_available_remaining.setText(joined_spots + " players joined");
                    else
                        txt_available_remaining.setText(joined_spots + " player joined");

                    progress_bar.setMax(available_spots);
                    progress_bar.setProgress(joined_spots);
                    progress_bar.setProgress(joined_spots);

                    if (singleContestModel.DataClass.ConfirmStatus.equalsIgnoreCase("1"))
                        txt_confirm.setVisibility(View.VISIBLE);
                    else
                        txt_confirm.setVisibility(View.GONE);

                    if (singleContestModel.DataClass.Joined) {
                        txt_join.setText("JOINED");
                        txt_join.setTextColor(getResources().getColor(R.color.orange_color));
                        //join_contest.setClickable(false);
                    }


                    getUserData();

                } else {
                    Constants.SnakeMessageYellow(lyt_sub_main, singleContestModel.message);
                    getUserData();
                }
            }

            @Override
            public void onFailure(@NonNull Call<SingleContestModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lyt_winners: {
                if (singleContestModel.DataClass.WinningAmount.equalsIgnoreCase("0") ||
                        singleContestModel.DataClass.WinningAmount.equalsIgnoreCase("")) {
                    txt_pool_price.setVisibility(View.GONE);
                    txt_winning_rupee.setVisibility(View.GONE);
                } else {
                    txt_pool_price.setVisibility(View.VISIBLE);
                    txt_winning_rupee.setVisibility(View.VISIBLE);
                    tv_pool_price.setText(singleContestModel.DataClass.WinningAmount);
                }

                getWinnerPool(singleContestModel.DataClass);
                break;
            }
            //testrt
            case R.id.lyt_winners_drop_t: {
                if (data.WinningAmount.equalsIgnoreCase("0") || data.WinningAmount.equalsIgnoreCase("")) {
                    txt_pool_price.setVisibility(View.GONE);
                    txt_winning_rupee.setVisibility(View.GONE);
                } else {
                    txt_pool_price.setVisibility(View.VISIBLE);
                    txt_winning_rupee.setVisibility(View.VISIBLE);
                    tv_pool_price.setText(data.WinningAmount);
                }

                getTournamentWinnerPool(data);
                break;
            }
            case R.id.lyt_rules: {
                final ProgressDialog progressDialog = new ProgressDialog(JoinContestViaShare.this);
                progressDialog.setMessage("Please wait...."); // Setting Message
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
                progressDialog.show(); // Display Progress Dialog
                progressDialog.setCancelable(false);
                Call<RulesModel> rulesModelCall = apiInterface.getRules(Constants.GET_RULES, Pref.getValue(JoinContestViaShare.this, Constants.UserID, "",
                        Constants.FILENAME), data.TournamentID);

                rulesModelCall.enqueue(new Callback<RulesModel>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(@NonNull Call<RulesModel> call, @NonNull Response<RulesModel> response) {
                        progressDialog.dismiss();
                        RulesModel rulesModel = response.body();
                        assert rulesModel != null;

                        if (rulesModel.status.equalsIgnoreCase("success")) {
                            txt_title_bs.setText("Rules");
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                txt_bottom_msg.setText(Html.fromHtml(rulesModel.Data.Rules, Html.FROM_HTML_SEPARATOR_LINE_BREAK_DIV));
                            } else {
                                txt_bottom_msg.setText(Html.fromHtml(rulesModel.Data.Rules));
                            }
                            rules_bottomsheet.setState(BottomSheetBehavior.STATE_EXPANDED);
                            bg.setVisibility(View.VISIBLE);
                        } else {
                            Constants.SnakeMessageYellow(lyt_sub_main, rulesModel.message);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<RulesModel> call, @NonNull Throwable t) {
                        progressDialog.dismiss();
                        t.printStackTrace();
                    }
                });
                break;
            }
            case R.id.join_contest: {
                if (data.DataType.equals(Constants.CONTEST)) {
                    if (singleContestModel.DataClass.Joined) {
                        Intent i = new Intent(JoinContestViaShare.this, ActivityContestDetails.class);
                        i.putExtra("contest_id", singleContestModel.DataClass.ContestID);
                        startActivity(i);
                        finish();
                    } else {
                        contest_id = singleContestModel.DataClass.ContestID;
                        txt_entry_fees.setText(singleContestModel.DataClass.EntryFee);
                        txt_usable_cash.setText("0");
                        txt_to_pay.setText(singleContestModel.DataClass.EntryFee);
                        CanJoinPlayers = singleContestModel.DataClass.CanJoinPlayers;
                        CanJoinExtraPlayers = singleContestModel.DataClass.CanJoinExtraPlayers;
                        if (UniqueName.equalsIgnoreCase("") || Pref.getValue(JoinContestViaShare.this, Constants.BirthDate, "", Constants.FILENAME).equalsIgnoreCase("")) {
                            Intent i = new Intent(JoinContestViaShare.this, ActivityDOB.class);
                            i.putExtra("game_name", singleContestModel.DataClass.GameTypeName);
                            i.putExtra("user_game_id", userGameId);
                            i.putExtra("game_id", singleContestModel.DataClass.GameID);
                            i.putExtra("main_game_name", singleContestModel.DataClass.GameName);
                            startActivityForResult(i, Constants.PROFILE_UPDATE_STATUS_REQUEST_CODE);
                            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        } else {
                            getWalletUsageLimit(singleContestModel.DataClass);
                        }
                    }
                } else if (data.DataType.equals(Constants.TOURNAMENT)) {
                    if (data.Joined) {
                        Intent i = new Intent(JoinContestViaShare.this, ActivityTournamentDetail.class);
                        i.putExtra("contest_id", data.TournamentID);
                        startActivity(i);
                        finish();
                    } else {
                        getTimings(data);
                    }
                }

                break;
            }

            case R.id.cv_join_contest: {
                if (data.DataType.equals(Constants.CONTEST)) {
                    if (JoinFlag) {
                        if (Constants.CheckRestrictedStates(JoinContestViaShare.this))
                            Constants.SnakeMessageYellow(lyt_sub_main, getResources().getString(R.string.restricted_states_msg));
                        else if (!Constants.isCitizenOfIndia(JoinContestViaShare.this))
                            Constants.SnakeMessageYellow(lyt_sub_main, getResources().getString(R.string.restricted_country_msg));
                        else {
                            if (CanJoinPlayers == 1) {
                                joinContest();
                            } else if (CanJoinPlayers > 1) {
                                Intent i = new Intent(JoinContestViaShare.this, ActivitySquadRegistration.class);
                                i.putExtra("CanJoinPlayers", CanJoinPlayers);
                                i.putExtra("UniqueName", UniqueName);
                                i.putExtra("contest_id", contest_id);
                                i.putExtra("CanJoinExtraPlayers", CanJoinExtraPlayers);
                                i.putExtra("JoinButtonFlag", JoinButtonFlag);
                                i.putExtra("main_game_name", data.GameName);
                                i.putExtra("game_id", singleContestModel.DataClass.GameID);
                                startActivity(i);
                                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                            }
                        }
                    } else {
                        if (UniqueName.equalsIgnoreCase("")) {

                            Intent i = new Intent(JoinContestViaShare.this, ActivityDOB.class);
                            i.putExtra("game_name", singleContestModel.DataClass.GameTypeName);
                            i.putExtra("user_game_id", userGameId);
                            i.putExtra("game_id", singleContestModel.DataClass.GameID);
                            i.putExtra("main_game_name", singleContestModel.DataClass.GameName);
                            startActivityForResult(i, Constants.PROFILE_UPDATE_STATUS_REQUEST_CODE);
                            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        } else if (!(Pref.getValue(JoinContestViaShare.this, Constants.State, "", Constants.FILENAME).equalsIgnoreCase(""))
                                && !(Pref.getValue(JoinContestViaShare.this, Constants.BirthDate, "", Constants.FILENAME).equalsIgnoreCase(""))) {
                            Intent i = new Intent(JoinContestViaShare.this, ActivityAddBalance.class);
                            add_balance = topay - my_balance;
                            i.putExtra("amount_to_add", String.valueOf(add_balance));
                            i.putExtra("amount", String.valueOf(my_balance));
                            i.putExtra("game_id", singleContestModel.DataClass.GameID);
                            i.putExtra("game_name", singleContestModel.DataClass.GameTypeName);
                            i.putExtra("game_type_id", singleContestModel.DataClass.GameTypeID);
                            startActivity(i);
                        }
                    }
                } else if (data.DataType.equals(Constants.TOURNAMENT)) {
                    getUserData();
                    if (JoinFlag) {
                        if (Constants.CheckRestrictedStates(JoinContestViaShare.this))
                            Constants.SnakeMessageYellow(lyt_sub_main, getResources().getString(R.string.restricted_states_msg));
                        else if (!Constants.isCitizenOfIndia(JoinContestViaShare.this))
                            Constants.SnakeMessageYellow(lyt_sub_main, getResources().getString(R.string.restricted_country_msg));
                        else {
                            if (CanJoinPlayers == 1) {
                                joinTournament();
                            } else if (CanJoinPlayers > 1) {
                                Intent i = new Intent(JoinContestViaShare.this, ActivitySquadRegistration.class);
                                i.putExtra("CanJoinPlayers", data.CanJoinPlayers);
                                i.putExtra("CanJoinExtraPlayers", data.CanJoinExtraPlayers);
                                i.putExtra("UniqueName", UniqueName);
                                i.putExtra("contest_id", contest_id);
                                i.putExtra("tournament_id", data.TournamentID);
                                i.putExtra("main_game_name", data.GameName);
                                i.putExtra("JoinButtonFlag", JoinButtonFlag);
                                i.putExtra("game_id", data.GameID);
                                startActivity(i);
                            }
                        }
                    } else {
                        if (UniqueName.equalsIgnoreCase("")) {
                            Intent i = new Intent(JoinContestViaShare.this, ActivityDOB.class);
                            i.putExtra("game_name", data.GameTypeName);
                            i.putExtra("user_game_id", userGameId);
                            i.putExtra("game_id", data.GameID);
                            i.putExtra("main_game_name", data.GameName);
                            startActivityForResult(i, Constants.PROFILE_UPDATE_STATUS_REQUEST_CODE);
                            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        } else if (!(Pref.getValue(JoinContestViaShare.this, Constants.State, "", Constants.FILENAME).equalsIgnoreCase(""))
                                && !(Pref.getValue(JoinContestViaShare.this, Constants.BirthDate, "", Constants.FILENAME).equalsIgnoreCase(""))) {
                            Intent i = new Intent(JoinContestViaShare.this, ActivityAddBalance.class);
                            add_balance = topay - my_balance;
                            i.putExtra("amount_to_add", String.valueOf(add_balance));
                            i.putExtra("amount", String.valueOf(my_balance));
                            i.putExtra("game_id", data.GameID);
                            i.putExtra("game_name", game_name);
                            i.putExtra("game_type_id", data.GameTypeID);
                            i.putExtra("is_tournament", true);
                            startActivity(i);

                        }
                    }
                }

                break;
            }

            case R.id.cv_video_join: {
                if (data.DataType.equals(Constants.CONTEST)) {
                    getUserData();

                    if (JoinFlag) {
                        if (Constants.CheckRestrictedStates(JoinContestViaShare.this))
                            Constants.SnakeMessageYellow(lyt_sub_main, getResources().getString(R.string.restricted_states_msg));
                        else if (!Constants.isCitizenOfIndia(JoinContestViaShare.this))
                            Constants.SnakeMessageYellow(lyt_sub_main, getResources().getString(R.string.restricted_country_msg));
                        else {
                            if (CanJoinPlayers == 1) {
                            } else if (CanJoinPlayers > 1) {
                                Intent i = new Intent(JoinContestViaShare.this, ActivitySquadRegistration.class);
                                i.putExtra("CanJoinPlayers", CanJoinPlayers);
                                i.putExtra("UniqueName", UniqueName);
                                i.putExtra("CanJoinExtraPlayers", CanJoinExtraPlayers);
                                i.putExtra("contest_id", contest_id);
                                i.putExtra("JoinButtonFlag", JoinButtonFlag);
                                i.putExtra("main_game_name", data.GameName);
                                i.putExtra("game_id", singleContestModel.DataClass.GameID);
                                startActivity(i);

                                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                            }
                        }
                    } else {
                        if (UniqueName.equalsIgnoreCase("")) {
                            Intent i = new Intent(JoinContestViaShare.this, ActivityDOB.class);
                            i.putExtra("game_name", game_name);
                            i.putExtra("user_game_id", userGameId);
                            i.putExtra("game_id", singleContestModel.DataClass.GameID);
                            i.putExtra("main_game_name", singleContestModel.DataClass.GameName);
                            startActivityForResult(i, Constants.PROFILE_UPDATE_STATUS_REQUEST_CODE);
                            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        } else if (!(Pref.getValue(JoinContestViaShare.this, Constants.State, "", Constants.FILENAME).equalsIgnoreCase(""))
                                && !(Pref.getValue(JoinContestViaShare.this, Constants.BirthDate, "", Constants.FILENAME).equalsIgnoreCase(""))) {
                            Intent i = new Intent(JoinContestViaShare.this, ActivityAddBalance.class);
                            add_balance = topay - my_balance;
                            i.putExtra("amount_to_add", String.valueOf(add_balance));
                            i.putExtra("amount", String.valueOf(my_balance));
                            i.putExtra("game_id", singleContestModel.DataClass.GameID);
                            i.putExtra("game_name", game_name);
                            i.putExtra("game_type_id", singleContestModel.DataClass.GameTypeID);
                            startActivity(i);


                        }
                    }
                } else if (data.DataType.equals(Constants.TOURNAMENT)) {
                    getUserData();
                    if (JoinFlag) {
                        if (Constants.CheckRestrictedStates(JoinContestViaShare.this))
                            Constants.SnakeMessageYellow(lyt_sub_main, getResources().getString(R.string.restricted_states_msg));
                        else if (!Constants.isCitizenOfIndia(JoinContestViaShare.this))
                            Constants.SnakeMessageYellow(lyt_sub_main, getResources().getString(R.string.restricted_country_msg));
                        else {
                            if (CanJoinPlayers == 1) {
                                joinTournament();
                            } else if (CanJoinPlayers > 1) {
                                Intent i = new Intent(JoinContestViaShare.this, ActivitySquadRegistration.class);
                                i.putExtra("CanJoinPlayers", data.CanJoinPlayers);
                                i.putExtra("CanJoinExtraPlayers", data.CanJoinExtraPlayers);
                                i.putExtra("UniqueName", UniqueName);
                                i.putExtra("contest_id", contest_id);
                                i.putExtra("main_game_name", data.GameName);
                                i.putExtra("tournament_id", data.TournamentID);
                                i.putExtra("JoinButtonFlag", JoinButtonFlag);
                                i.putExtra("game_id", data.GameID);
                                startActivity(i);
                            }
                        }
                    } else {
                        if (UniqueName.equalsIgnoreCase("")) {
                            Intent i = new Intent(JoinContestViaShare.this, ActivityDOB.class);
                            i.putExtra("game_name", data.GameTypeName);
                            i.putExtra("user_game_id", userGameId);
                            i.putExtra("game_id", data.GameID);
                            i.putExtra("main_game_name", data.GameName);
                            startActivityForResult(i, Constants.PROFILE_UPDATE_STATUS_REQUEST_CODE);
                            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        } else if (!(Pref.getValue(JoinContestViaShare.this, Constants.State, "", Constants.FILENAME).equalsIgnoreCase(""))
                                && !(Pref.getValue(JoinContestViaShare.this, Constants.BirthDate, "", Constants.FILENAME).equalsIgnoreCase(""))) {
                            Intent i = new Intent(JoinContestViaShare.this, ActivityAddBalance.class);
                            add_balance = topay - my_balance;
                            i.putExtra("amount_to_add", String.valueOf(add_balance));
                            i.putExtra("amount", String.valueOf(my_balance));
                            i.putExtra("game_id", data.GameID);
                            i.putExtra("game_name", game_name);
                            i.putExtra("game_type_id", data.GameTypeID);
                            i.putExtra("is_tournament", true);
                            startActivity(i);

                        }
                    }
                }

                break;
            }
        }
    }

    private void joinTournament() {
        final ProgressDialog progressDialog = new ProgressDialog(JoinContestViaShare.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        Call<JoinContestModel> joinContestModelCall = apiInterface.joinTournament(Constants.JOIN_TOURNAMENT, Pref.getValue(JoinContestViaShare.this, Constants.UserID, "", Constants.FILENAME), data.TournamentID, contest_id);

        joinContestModelCall.enqueue(new Callback<JoinContestModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<JoinContestModel> call, @NonNull Response<JoinContestModel> response) {
                progressDialog.dismiss();
                Common.insertLog("TOURNAMNET JOIN");
                JoinContestModel joinContestModel = response.body();
                assert joinContestModel != null;
                if (joinContestModel.status.equalsIgnoreCase("success")) {
                    Intent i = new Intent(JoinContestViaShare.this, ActivityTournamentDetail.class);
                    i.putExtra("contest_id", data.TournamentID);
                    startActivity(i);
                    finish();
                } else {
                    Constants.SnakeMessageYellow(lyt_sub_main, joinContestModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<JoinContestModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void getTimings(SingleContestModel.Data data) {
        txt_tournament_time_title.setText(data.GameName);
        tv_tournament_time_type.setText(data.GameTypeName);
        txt_tournament_name.setText(data.Title);

        @SuppressLint("SimpleDateFormat") DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        @SuppressLint("SimpleDateFormat") DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
        String inputDateStr = data.Date;
        Date date = null;
        try {
            date = inputFormat.parse(inputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String outputDateStr = outputFormat.format(date);

        txt_tournament_date.setText(outputDateStr);

        final ProgressDialog progressDialog = new ProgressDialog(JoinContestViaShare.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<TournamentTimeSlotModel> tournamentTimeSlotModelCall = apiInterface.getTimeSlots(Constants.GET_TIME_SLOTS,
                Pref.getValue(JoinContestViaShare.this, Constants.UserID, "",
                        Constants.FILENAME), data.TournamentID);

        tournamentTimeSlotModelCall.enqueue(new Callback<TournamentTimeSlotModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<TournamentTimeSlotModel> call, @NonNull Response<TournamentTimeSlotModel> response) {
                progressDialog.dismiss();
                TournamentTimeSlotModel tournamentTimeSlotModel = response.body();
                assert tournamentTimeSlotModel != null;
                if (tournamentTimeSlotModel.status.equalsIgnoreCase("success")) {
                    tournamentTimeAdapter = new TournamentTimeAdapter(JoinContestViaShare.this, tournamentTimeSlotModel.DataClass.timeSlotsArrayList, JoinContestViaShare.this);
                    rv_tournament_time.setAdapter(tournamentTimeAdapter);
                    rv_tournament_time.addItemDecoration(new DividerItemDecoration(JoinContestViaShare.this,
                            DividerItemDecoration.VERTICAL));
                    tournamnet_time_bottomsheet.setState(BottomSheetBehavior.STATE_EXPANDED);
                    bg.setVisibility(View.VISIBLE);
                    txt_tournamnet_time_notes.setText(tournamentTimeSlotModel.DataClass.InfoText);
                } else {
                    Constants.SnakeMessageYellow(lyt_sub_main, tournamentTimeSlotModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<TournamentTimeSlotModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void getTournamentWinnerPool(SingleContestModel.Data data) {
        final ProgressDialog progressDialog = new ProgressDialog(JoinContestViaShare.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<WinnerContestModel> contestDetailsModelCall = apiInterface.getTournamentWinners(Constants.GET_TOURNAMENT_WINNING_POOL, data.TournamentID);

        contestDetailsModelCall.enqueue(new Callback<WinnerContestModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<WinnerContestModel> call, @NonNull Response<WinnerContestModel> response) {
                progressDialog.dismiss();
                tv_title.setText(R.string.winning_breakup);
                txt_pool_price.setText(R.string.prize_pool);
                txt_winning_rupee.setVisibility(View.VISIBLE);
                tv_pool_price.setVisibility(View.VISIBLE);
                WinnerContestModel winnerContestModel = response.body();
                assert winnerContestModel != null;
                if (winnerContestModel.status.equalsIgnoreCase("success")) {
                    tv_pool_price.setText(winnerContestModel.data.TotalPrice);
                    winnerPoolAdapter = new WinnerPoolAdapter(JoinContestViaShare.this, winnerContestModel.data.prizePoolsData);
                    rv_pool_price.setAdapter(winnerPoolAdapter);
                    pool_price_bottomsheet.setState(BottomSheetBehavior.STATE_EXPANDED);
                    bg.setVisibility(View.VISIBLE);
                } else {
                    Constants.SnakeMessageYellow(lyt_sub_main, winnerContestModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<WinnerContestModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void joinContest() {
        final ProgressDialog progressDialog = new ProgressDialog(JoinContestViaShare.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        Call<JoinContestModel> joinContestModelCall = apiInterface.joinContest(Constants.JOIN_CONTEST, Pref.getValue(JoinContestViaShare.this, Constants.UserID, "", Constants.FILENAME), contest_id, JoinButtonFlag);

        joinContestModelCall.enqueue(new Callback<JoinContestModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<JoinContestModel> call, @NonNull Response<JoinContestModel> response) {
                Common.insertLog("SINGLE CONTEST JOIN");
                progressDialog.dismiss();
                JoinContestModel joinContestModel = response.body();
                assert joinContestModel != null;
                if (joinContestModel.status.equalsIgnoreCase("success")) {
                    Intent i = new Intent(JoinContestViaShare.this, ActivityContestDetails.class);
                    i.putExtra("contest_id", contest_id);
                    startActivity(i);
                    finish();
                } else {
                    Constants.SnakeMessageYellow(lyt_sub_main, joinContestModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<JoinContestModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.PROFILE_UPDATE_STATUS_REQUEST_CODE) {
            assert data != null;
            userGameDataarrayList = Constants.getUserData(JoinContestViaShare.this);
            getUserData();
            if (data.getBooleanExtra("profile_update_status", false)) {
                if (this.data.DataType.equals(Constants.CONTEST)) {
                    getWalletUsageLimit(singleContestModel.DataClass);
                } else if (this.data.DataType.equals(Constants.TOURNAMENT)) {
                    getTournamentWalletUsageLimit(this.data);
                }

                /*if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED)
                {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
                else
                {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }*/
            }
        }
    }

    private void getWalletUsageLimit(SingleContestModel.Data contestsData) {
        final ProgressDialog progressDialog = new ProgressDialog(JoinContestViaShare.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<WalletUsageLimitModel> contestDetailsModelCall = apiInterface.getUserWalletLimit(Constants.WALLET_USAGE_LIMIT, Pref.getValue(JoinContestViaShare.this, Constants.UserID, "", Constants.FILENAME), contestsData.ContestID);

        contestDetailsModelCall.enqueue(new Callback<WalletUsageLimitModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<WalletUsageLimitModel> call, @NonNull Response<WalletUsageLimitModel> response) {
                progressDialog.dismiss();
                WalletUsageLimitModel walletUsageLimitModel = response.body();
                assert walletUsageLimitModel != null;
                if (walletUsageLimitModel.status.equalsIgnoreCase("success")) {
                    contest_id = contestsData.ContestID;
                    txt_entry_fees.setText(walletUsageLimitModel.data.EntryFee);
                    txt_usable_cash.setText(walletUsageLimitModel.data.CashBalance);
                    txt_to_pay.setText(walletUsageLimitModel.data.ToPay);
                    JoinFlag = walletUsageLimitModel.data.JoinFlag;
                    JoinButtonFlag = walletUsageLimitModel.data.JoinButtonFlag;
                    txt_my_balance.setText(walletUsageLimitModel.data.WalletBalance);
                    my_balance = Float.parseFloat(walletUsageLimitModel.data.WalletBalance);
                    topay = Float.parseFloat(walletUsageLimitModel.data.ToPay);

                    if (JoinButtonFlag.equals(Constants.DEFAULT_JOIN)) {
                        cv_video_join.setVisibility(View.GONE);
                    } else if (JoinButtonFlag.equals(Constants.VIDEO_JOIN)) {
                        cv_join_contest.setVisibility(View.GONE);
                    } else {
                        cv_join_contest.setVisibility(View.VISIBLE);
                        cv_video_join.setVisibility(View.VISIBLE);
                    }

                    if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    } else {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    }
                } else {
                    Constants.SnakeMessageYellow(lyt_sub_main, walletUsageLimitModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<WalletUsageLimitModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void getUserData() {
        assert userGameDataarrayList != null;
        for (int i = 0; i < userGameDataarrayList.size(); i++) {
            if (data.DataType.equals(Constants.CONTEST)) {
                if (userGameDataarrayList.get(i).GameID.equalsIgnoreCase(singleContestModel.DataClass.GameID)) {
                    userGameId = userGameDataarrayList.get(i).UserGameID;
                    UniqueName = userGameDataarrayList.get(i).UniqueName;
                }
            } else if (data.DataType.equals(Constants.TOURNAMENT)) {
                if (userGameDataarrayList.get(i).GameID.equalsIgnoreCase(data.GameID)) {
                    userGameId = userGameDataarrayList.get(i).UserGameID;
                    UniqueName = userGameDataarrayList.get(i).UniqueName;
                }
            }

        }
    }

    private void getWinnerPool(SingleContestModel.Data dataClass) {
        final ProgressDialog progressDialog = new ProgressDialog(JoinContestViaShare.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<WinnerContestModel> contestDetailsModelCall = apiInterface.getContestWinner(Constants.GETSINGLECONTESTWINNER, dataClass.ContestID);

        contestDetailsModelCall.enqueue(new Callback<WinnerContestModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<WinnerContestModel> call, @NonNull Response<WinnerContestModel> response) {
                progressDialog.dismiss();
                WinnerContestModel winnerContestModel = response.body();
                assert winnerContestModel != null;
                if (winnerContestModel.status.equalsIgnoreCase("success")) {
                    winnerPoolAdapter = new WinnerPoolAdapter(JoinContestViaShare.this, winnerContestModel.data.prizePoolsData);
                    rv_pool_price.setAdapter(winnerPoolAdapter);
                    pool_price_bottomsheet.setState(BottomSheetBehavior.STATE_EXPANDED);
                    bg.setVisibility(View.VISIBLE);
                } else {
                    Constants.SnakeMessageYellow(lyt_sub_main, winnerContestModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<WinnerContestModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onTournamentTimeSelectedListener(TournamentTimeSlotModel.Data.TimeSlots timeSlots) {
        contest_id = timeSlots.ContestID;
        cv_tournament_time_next.setOnClickListener(v ->
        {
            txt_entry_fees.setText(data.EntryFee);
            txt_usable_cash.setText("0");
            txt_to_pay.setText(data.EntryFee);
            CanJoinPlayers = data.CanJoinPlayers;
            CanJoinExtraPlayers = data.CanJoinExtraPlayers;

            if (UniqueName.equalsIgnoreCase("") || Pref.getValue(JoinContestViaShare.this, Constants.BirthDate, "", Constants.FILENAME).equalsIgnoreCase("")) {
                Intent i = new Intent(JoinContestViaShare.this, ActivityDOB.class);
                i.putExtra("game_name", data.GameTypeName);
                i.putExtra("user_game_id", userGameId);
                i.putExtra("game_id", data.GameID);
                i.putExtra("main_game_name", data.GameName);
                startActivityForResult(i, Constants.PROFILE_UPDATE_STATUS_REQUEST_CODE);
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            } else {
                getTournamentWalletUsageLimit(data);
            }
        });

    }

    @Override
    public void onReportSelected(ReportsDataModel.ReportsData reportsData) {

    }

    private void getTournamentWalletUsageLimit(SingleContestModel.Data data) {
        final ProgressDialog progressDialog = new ProgressDialog(JoinContestViaShare.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<WalletUsageLimitModel> contestDetailsModelCall = apiInterface.getUserWalletLimitTournament(Constants.WALLET_USAGE_LIMIT, Pref.getValue(JoinContestViaShare.this, Constants.UserID, "", Constants.FILENAME), data.TournamentID);

        contestDetailsModelCall.enqueue(new Callback<WalletUsageLimitModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<WalletUsageLimitModel> call, @NonNull Response<WalletUsageLimitModel> response) {
                progressDialog.dismiss();
                if (tournamnet_time_bottomsheet.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    tournamnet_time_bottomsheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    bg.setVisibility(View.GONE);
                }
                WalletUsageLimitModel walletUsageLimitModel = response.body();
                assert walletUsageLimitModel != null;
                if (walletUsageLimitModel.status.equalsIgnoreCase("success")) {
                    txt_entry_fees.setText(walletUsageLimitModel.data.EntryFee);
                    txt_usable_cash.setText(walletUsageLimitModel.data.CashBalance);
                    txt_to_pay.setText(walletUsageLimitModel.data.ToPay);
                    JoinFlag = walletUsageLimitModel.data.JoinFlag;
                    JoinButtonFlag = walletUsageLimitModel.data.JoinButtonFlag;
                    txt_my_balance.setText(walletUsageLimitModel.data.WalletBalance);
                    my_balance = Float.parseFloat(walletUsageLimitModel.data.WalletBalance);
                    topay = Float.parseFloat(walletUsageLimitModel.data.ToPay);

                    if (JoinButtonFlag.equals(Constants.DEFAULT_JOIN)) {
                        cv_video_join.setVisibility(View.GONE);
                        cv_join_contest.setVisibility(View.VISIBLE);
                    } else if (JoinButtonFlag.equals(Constants.VIDEO_JOIN)) {
                        cv_join_contest.setVisibility(View.GONE);
                        cv_video_join.setVisibility(View.VISIBLE);
                    } else {
                        cv_join_contest.setVisibility(View.VISIBLE);
                        cv_video_join.setVisibility(View.VISIBLE);
                    }

                    if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    } else {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    }
                } else {
                    Constants.SnakeMessageYellow(lyt_sub_main, walletUsageLimitModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<WalletUsageLimitModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }
}
