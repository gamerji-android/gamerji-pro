package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.esports.gamerjipro.Model.SingleContestModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.FileUtils;
import com.esports.gamerjipro.Utils.Pref;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityJoinInvite extends AppCompatActivity
{
    EditText et_code;
    CardView proceed,cv_join_contest,cv_video_join;
    RelativeLayout parent_layout;
    SingleContestModel singleContestModel ;
    APIInterface apiInterface;
    String Val_IsWithUsers="1";
    LinearLayout layoutBottomSheet,bonus_bottom_layout;
    TextView txt_entry_fees,txt_usable_cash,txt_to_pay;
    View bg;
    ImageView img_info,img_close,img_close_cash,img_back;
    TextView img_news;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_invite_code_main);
        img_news=findViewById(R.id.img_news);
        img_news.setOnClickListener(v -> FileUtils.OpenNewsWebview(this,"https://www.gamerji.com"));


        apiInterface = APIClient.getClient().create(APIInterface.class);
        et_code=findViewById(R.id.et_code);
        img_back=findViewById(R.id.img_back);
        img_back.setOnClickListener(v -> finish());
        et_code.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        proceed=findViewById(R.id.proceed);
        parent_layout=findViewById(R.id.parent_layout);
        bg=findViewById(R.id.bg);
        img_info=findViewById(R.id.img_info);
        img_close=findViewById(R.id.img_close);

        layoutBottomSheet=findViewById(R.id.bottom_sheet);
        bonus_bottom_layout=findViewById(R.id.bottom_sheet_cash_bonus);

        txt_usable_cash=findViewById(R.id.txt_usable_cash);
        txt_to_pay=findViewById(R.id.txt_to_pay);
        txt_entry_fees=findViewById(R.id.txt_entry_fees);
        cv_join_contest=findViewById(R.id.cv_join_contest);
        cv_video_join=layoutBottomSheet.findViewById(R.id.cv_video_join);
        img_close_cash=findViewById(R.id.img_close_cash);


        proceed.setOnClickListener(v -> {
            if (et_code.getText().length()<0)
                Constants.SnakeMessageYellow(parent_layout,"Enter Code to Proceed.");
            else
                checkC_Or_T(et_code.getText().toString());
        });
    }


    private void checkC_Or_T(String code)
    {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityJoinInvite.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<SingleContestModel> singleContestModelCall = apiInterface.checkContestOrTournament(Constants.GetCTDetails, Pref.getValue(ActivityJoinInvite.this, Constants.UserID,"", Constants.FILENAME),Val_IsWithUsers,code);

        singleContestModelCall.enqueue(new Callback<SingleContestModel>()
        {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<SingleContestModel> call, @NonNull Response<SingleContestModel> response)
            {
                progressDialog.dismiss();
                singleContestModel = response.body();
                assert singleContestModel != null;
                if (singleContestModel.status.equalsIgnoreCase("success"))
                {
                    if(singleContestModel.DataClass.DataType.equals(Constants.CONTEST))
                    {
                        if (singleContestModel.DataClass.Joined)
                        {
                            Intent i = new Intent(ActivityJoinInvite.this, ActivityContestDetails.class);
                            i.putExtra("contest_id",singleContestModel.DataClass.ContestID);
                            startActivity(i);
                            finish();
                        }
                        else
                        {
                            Intent i = new Intent(ActivityJoinInvite.this,JoinContestViaShare.class);
                            i.putExtra("contest_id",singleContestModel.DataClass.ContestID);
                            i.putExtra("tournament_data", singleContestModel.DataClass);
                            i.putExtra("invite",true);
                            startActivity(i);
                            finish();
                        }
                    }
                    else if(singleContestModel.DataClass.DataType.equals(Constants.TOURNAMENT))
                    {
                        if (singleContestModel.DataClass.Joined)
                        {
                            Intent i = new Intent(ActivityJoinInvite.this, ActivityTournamentDetail.class);
                            i.putExtra("contest_id",singleContestModel.DataClass.TournamentID);
                            startActivity(i);
                            finish();
                        }
                        else
                        {
                            Intent i = new Intent(ActivityJoinInvite.this,JoinContestViaShare.class);
                            i.putExtra("tournament_id",singleContestModel.DataClass.TournamentID);
                            i.putExtra("tournament_data", singleContestModel.DataClass);
                            i.putExtra("invite",true);
                            startActivity(i);
                            finish();
                        }
                    }
                }
                else
                {
                    Constants.SnakeMessageYellow(parent_layout,singleContestModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<SingleContestModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }
}
