package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.esports.gamerjipro.Model.GeneralResponseModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Common;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.FileUtils;
import com.esports.gamerjipro.Utils.Pref;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import org.json.JSONException;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityEmailPhoneVerification extends AppCompatActivity implements View.OnClickListener {
    ImageView img_back, img_google, img_fb, img_email_verified;
    TextView txt_mobile, txt_email_verify, txt_email_verify_info, txt_et_mobile_vf_msg, txt_email, txt_submit_for_verification;
    EditText edt_email;
    CardView btn_verify_email, card_email_not_verified;
    LinearLayout lyt_email;
    LinearLayout lyt_or;
    APIInterface apiInterface;
    RelativeLayout lyt_parent;
    CallbackManager callbackManager = CallbackManager.Factory.create();
    GoogleSignInClient mGoogleSignInClient;
    int RC_SIGN_IN = 100;
    AccessToken accessToken;
    String uid;
    private FirebaseAuth mAuth;
    TextView img_news;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_email_verification);
        img_news = findViewById(R.id.img_news);
        img_news.setOnClickListener(v -> FileUtils.OpenNewsWebview(this, "https://www.gamerji.com"));
        img_back = findViewById(R.id.img_back);
        txt_mobile = findViewById(R.id.txt_mobile);
        edt_email = findViewById(R.id.edt_email);
        btn_verify_email = findViewById(R.id.btn_verify_email);
        img_google = findViewById(R.id.img_google);
        img_email_verified = findViewById(R.id.img_email_verified);
        lyt_or = findViewById(R.id.lyt_or);
        img_fb = findViewById(R.id.img_fb);
        txt_submit_for_verification = findViewById(R.id.txt_submit_for_verification);
        txt_email_verify = findViewById(R.id.txt_email_verify);

        txt_email_verify_info = findViewById(R.id.txt_email_verify_info);
        card_email_not_verified = findViewById(R.id.card_email_not_verified);
        txt_et_mobile_vf_msg = findViewById(R.id.txt_et_mobile_vf_msg);
        txt_email = findViewById(R.id.txt_email);
        lyt_email = findViewById(R.id.lyt_email);
        lyt_parent = findViewById(R.id.lyt_parent);
        txt_email.setText(Pref.getValue(ActivityEmailPhoneVerification.this, Constants.EmailAddress, "", Constants.FILENAME));

        mAuth = FirebaseAuth.getInstance();
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        apiInterface = APIClient.getClient().create(APIInterface.class);

        if (getIntent().getStringExtra("email_verified").equalsIgnoreCase("2")) {
            txt_email_verify.setText("Email is verified");
            txt_email.setTextColor(getResources().getColor(R.color.orange_color));
            img_email_verified.setVisibility(View.VISIBLE);
            img_google.setVisibility(View.GONE);
            img_fb.setVisibility(View.GONE);
            btn_verify_email.setVisibility(View.GONE);
        } else if (getIntent().getStringExtra("email_verified").equalsIgnoreCase("3")) {
            txt_email_verify.setText("Email verification is pending.");
            edt_email.setText(Pref.getValue(ActivityEmailPhoneVerification.this, Constants.EmailAddress, "", Constants.FILENAME));
            txt_submit_for_verification.setText("Resend Verification Link");
        } else if (getIntent().getStringExtra("email_verified").equalsIgnoreCase("1")) {
            txt_email_verify.setText("Verification your Email");
        } else {
            txt_email_verify.setText("Email verification rejected.");
        }


        if (getIntent().getStringExtra("phone_verified").equalsIgnoreCase("1")) {
            txt_et_mobile_vf_msg.setText("Mobile number is pending.");

        } else if (getIntent().getStringExtra("phone_verified").equalsIgnoreCase("2")) {
            txt_et_mobile_vf_msg.setText("Mobile number is verified");
        } else if (getIntent().getStringExtra("phone_verified").equalsIgnoreCase("3")) {
            txt_et_mobile_vf_msg.setText("Mobile number not verified.");
        } else {
            txt_et_mobile_vf_msg.setText("Mobile number rejected.");
        }


        if (getIntent().getStringExtra("email_verified").equalsIgnoreCase("2") &&
                getIntent().getStringExtra("phone_verified").equalsIgnoreCase("2")) {
            lyt_email.setVisibility(View.GONE);
        }
        txt_mobile.setText(Pref.getValue(ActivityEmailPhoneVerification.this, Constants.CountryCode, "", Constants.FILENAME) + " " +
                Pref.getValue(ActivityEmailPhoneVerification.this, Constants.MobileNumber, "", Constants.FILENAME));
        btn_verify_email.setOnClickListener(this);
        img_back.setOnClickListener(this);
        img_google.setOnClickListener(this);
        img_fb.setOnClickListener(this);
        card_email_not_verified.setOnClickListener(v -> FileUtils.requestfoucs(ActivityEmailPhoneVerification.this, edt_email));


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        accessToken = loginResult.getAccessToken();
                        uid = String.valueOf(accessToken.getUserId());

                        // LoginManager.getInstance().logOut();
                        getUserInformation(loginResult);
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });
    }

    private void getUserInformation(final LoginResult loginResul) {
        GraphRequest request = GraphRequest.newMeRequest(loginResul.getAccessToken(),
                (object, response) ->
                {

                    // Application code
                    if (object.has("email")) {
                        try {
                            String email = object.getString("email");
                            String name = object.getString("name"); // 01/31/1980 format
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        sendToServer(object.optString("email"), Constants.FACEBOOK);
                    } else {
                        Constants.SnakeMessageYellow(lyt_parent, "Email not found.");
                    }
                    // downloadFBFile(object.getJSONObject("picture").getJSONObject("data").getString("url"),object);
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,first_name,last_name");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back: {
                finish();
                break;
            }
            case R.id.img_fb: {
                LoginManager.getInstance().logInWithReadPermissions(this,
                        Arrays.asList("email", "public_profile"));
                break;
            }
            case R.id.img_google: {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
            }
            case R.id.btn_verify_email: {
                if (!Patterns.EMAIL_ADDRESS.matcher(edt_email.getText()).matches())
                    edt_email.setError("Enter valid Email.");
                else
                    sendToServer(edt_email.getText().toString(), Constants.BASIC);
                break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Common.insertLog("Google sign in failed" + e);
                // ...
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct) {
        Common.insertLog("firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task ->
                {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        FirebaseUser user = mAuth.getCurrentUser();
                        sendToServer(acct.getEmail(), Constants.GOOGLE);
                    } else {
                        // If sign in fails, display a message to the user.
                        Common.insertLog("signInWithCredential:failure" + task.getException());
                    }
                });
    }

    private void sendToServer(String email, String type) {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityEmailPhoneVerification.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<GeneralResponseModel> resendOTPModelCall = apiInterface.verifyEmail(Constants.VERIFYEMAIL, email, Pref.getValue(ActivityEmailPhoneVerification.this, Constants.UserID, "", Constants.FILENAME), type);

        resendOTPModelCall.enqueue(new Callback<GeneralResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<GeneralResponseModel> call, @NonNull Response<GeneralResponseModel> response) {
                progressDialog.dismiss();
                GeneralResponseModel resendOTPModel = response.body();
                assert resendOTPModel != null;
                if (resendOTPModel.status.equalsIgnoreCase("success")) {
                    if (type.equals(Constants.BASIC)) {
                        txt_submit_for_verification.setText("Resend verification link");
                        Constants.SnakeMessageYellow(lyt_parent, resendOTPModel.message);
                    } else {
                        finish();
                        Constants.SnakeMessageYellow(lyt_parent, resendOTPModel.message);
                    }
                } else {
                    Constants.SnakeMessageYellow(lyt_parent, resendOTPModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<GeneralResponseModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }

        });
    }
}

