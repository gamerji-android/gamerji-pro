package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Adapter.SquadRegistrationAdapter;
import com.esports.gamerjipro.Interface.RemovePlayerListener;
import com.esports.gamerjipro.Interface.SquadMobileListener;
import com.esports.gamerjipro.Model.SquadModel;
import com.esports.gamerjipro.Model.VerifyContestUserModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Common;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.Pref;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ActivitySquadRegistration extends AppCompatActivity implements SquadMobileListener, RemovePlayerListener
{
    TextView txt_mobile,txt_pubg_username,txt_gameji_name,txt_game_username;
    RecyclerView rv_players;
    CardView cv_submit;
    ImageView img_back;
    LinearLayoutManager linearLayoutManager ;
    SquadRegistrationAdapter  squadRegistrationAdapter ;
    ArrayList<SquadModel> squadModelArrayList = new ArrayList<>();
    int CanJoinPlayers,CanJoinExtraPlayers;
    String UniqueName,JoinButtonFlag;
   public RelativeLayout lyt_parent;
    String contest_id,game_id,tournament_id;
    List<List<String>> player_info_list = new ArrayList<>();
    APIInterface apiInterface;
    JSONArray player_user_id = new JSONArray();
    String main_game_name;



    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.squad_contest_layout);
        CanJoinPlayers=getIntent().getIntExtra("CanJoinPlayers",1);
        CanJoinExtraPlayers=getIntent().getIntExtra("CanJoinExtraPlayers",1);
        UniqueName=getIntent().getStringExtra("UniqueName");
        contest_id=getIntent().getStringExtra("contest_id");
        main_game_name=getIntent().getStringExtra("main_game_name");

        if (getIntent().getStringExtra("tournament_id")!= null)
        tournament_id=getIntent().getStringExtra("tournament_id");

        JoinButtonFlag=getIntent().getStringExtra("JoinButtonFlag");
        game_id=getIntent().getStringExtra("game_id");

        txt_mobile=findViewById(R.id.txt_mobile);
        img_back=findViewById(R.id.img_back);
        txt_pubg_username=findViewById(R.id.txt_pubg_username);
        txt_gameji_name=findViewById(R.id.txt_gameji_name);
        txt_game_username=findViewById(R.id.txt_game_username);
        rv_players=findViewById(R.id.rv_players);
        cv_submit=findViewById(R.id.cv_submit);
        lyt_parent=findViewById(R.id.lyt_parent);
        img_back.setOnClickListener(v -> finish());
        txt_game_username.setText(main_game_name+" "+"Name");
        txt_gameji_name.setText(Pref.getValue(ActivitySquadRegistration.this, Constants.TeamName,"",Constants.FILENAME));
        txt_pubg_username.setText(UniqueName);
        txt_mobile.setText(Pref.getValue(ActivitySquadRegistration.this, Constants.MobileNumber,"",Constants.FILENAME));
        linearLayoutManager= new LinearLayoutManager(this);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        rv_players.setLayoutManager(linearLayoutManager);

        for (int i=1;i<CanJoinExtraPlayers;i++)
        {
            SquadModel squadModel = new SquadModel();
            squadModel.setGamerjiName("");
            squadModel.setUser_name("");
            squadModel.setMobile_number("");
            squadModel.setUser_id("");
            squadModel.setAddplayer(false);
            squadModelArrayList.add(squadModel);
        }
        squadRegistrationAdapter= new SquadRegistrationAdapter(ActivitySquadRegistration.this,squadModelArrayList,CanJoinPlayers,ActivitySquadRegistration.this,ActivitySquadRegistration.this,main_game_name);
        rv_players.setAdapter(squadRegistrationAdapter);

        cv_submit.setOnClickListener(v ->
        {
            player_user_id = new JSONArray();

            for (int i=0;i<squadModelArrayList.size();i++)
            {
                if (!squadModelArrayList.get(i).getUser_id().trim().isEmpty())
                player_user_id.put(squadModelArrayList.get(i).getUser_id());
            }

            if (player_user_id.length()<(CanJoinPlayers-1))
            {
                Constants.SnakeMessageYellow(lyt_parent, "Enter atleast " + CanJoinPlayers + " players to proceed");
            }
            else
            {
                if(tournament_id==null)
                joinSquadContest();
                else
                joinTournament();
            }
        });
    }

    private void joinTournament()
    {
        final ProgressDialog progressDialog = new ProgressDialog(ActivitySquadRegistration.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        RequestParams requestParams  = new RequestParams();
        try
        {
            requestParams.put("Action", Constants.JOIN_TOURNAMENT);
            requestParams.put("Val_Userid", Pref.getValue(this, Constants.UserID,"", Constants.FILENAME));
            requestParams.put("Val_Tournamentid", tournament_id);
            requestParams.put("Val_Contestid", contest_id);
            requestParams.put("Val_Jointype", JoinButtonFlag);
            requestParams.put("Val_Players",player_user_id.toString());

            Common.insertLog("PARAMAS TOURNAMNR" +requestParams.toString());
            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            asyncHttpClient.setTimeout(40*1000);
            asyncHttpClient.post(this,Constants.BASE_API+Constants.V_3_TOURNAMENT_DETAILS,requestParams,new JsonHttpResponseHandler()
            {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response)
                {
                    super.onSuccess(statusCode, headers, response);
                    progressDialog.dismiss();
                    if (response.optString("status").equalsIgnoreCase("success"))
                    {
                        Intent i = new Intent(ActivitySquadRegistration.this,ActivityTournamentDetail.class);
                        i.putExtra("contest_id",tournament_id);
                        i.putExtra("invite",true);
                        startActivity(i);
                        finish();
                    }
                    else
                    {
                        Constants.SnakeMessageYellow(lyt_parent,response.optString("message"));
                    }
                }


                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response)
                {
                    super.onSuccess(statusCode, headers, response);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString)
                {
                    super.onSuccess(statusCode, headers, responseString);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse)
                {
                    progressDialog.dismiss();
                    Constants.SnakeMessageYellow(lyt_parent, "Something went wrong.Please try again.");
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse)
                {
                    progressDialog.dismiss();
                    Constants.SnakeMessageYellow(lyt_parent, "Something went wrong.Please try again.");
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable)
                {
                    progressDialog.dismiss();
                    Constants.SnakeMessageYellow(lyt_parent, "Something went wrong.Please try again.");
                    super.onFailure(statusCode, headers, responseString, throwable);
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }

    public boolean printAllEditTextValues()
    {
        int childCount = rv_players.getChildCount();
        boolean isEmpty=false;

        player_info_list.clear();

        for (int i = childCount; i >= 0; i--)
        {
            if (rv_players.findViewHolderForLayoutPosition(i) instanceof SquadRegistrationAdapter.ViewHolder)
            {
                SquadRegistrationAdapter.ViewHolder childHolder = (SquadRegistrationAdapter.ViewHolder) rv_players.findViewHolderForLayoutPosition(i);
                ArrayList<String> single_player = new ArrayList<>();
                assert childHolder != null;
                if(childHolder.txt_player_game_name.getText().length()<2)
                {
                    Constants.SnakeMessageYellow(lyt_parent,"Enter all the fields correctly to proceed.");
                    isEmpty=true;
                    break;
                }
                else if (childHolder.txt_player_mobile.getText().length()<10)
                {
                    Constants.SnakeMessageYellow(lyt_parent,"Enter all the fields correctly to proceed.");
                    isEmpty=true;
                    break;
                }
                else if (!Patterns.EMAIL_ADDRESS.matcher(childHolder.txt_gamerji_name.getText()).matches())
                {
                    Constants.SnakeMessageYellow(lyt_parent,"Enter all the fields correctly to proceed.");
                    isEmpty=true;
                    break;
                }
                else
                {
                    single_player.add(childHolder.player_country_code.getDefaultCountryCodeWithPlus()+childHolder.txt_player_mobile.getText().toString());
                    single_player.add(childHolder.txt_player_game_name.getText().toString());
                    single_player.add(childHolder.txt_gamerji_name.getText().toString());
                    player_info_list.add(single_player);
                }
            }
        }
        return isEmpty;
    }

    private void joinSquadContest()
    {
        final ProgressDialog progressDialog = new ProgressDialog(ActivitySquadRegistration.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        RequestParams requestParams  = new RequestParams();
        try
        {
            requestParams.put("Action", Constants.JOIN_CONTEST);
            requestParams.put("Val_Userid", Pref.getValue(this, Constants.UserID,"", Constants.FILENAME));
            requestParams.put("Val_Contestid", contest_id);
            requestParams.put("Val_Jointype", JoinButtonFlag);
            requestParams.put("Val_Players",player_user_id.toString());

            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
            asyncHttpClient.setTimeout(40*1000);
            Common.insertLog("PARAMAS" +requestParams.toString());
            asyncHttpClient.post(this, Constants.BASE_API + Constants.V3_JOIN_CONTESTS, requestParams, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    progressDialog.dismiss();
                    if (response.optString("status").equalsIgnoreCase("success")) {
                        Intent i = new Intent(ActivitySquadRegistration.this, ActivityContestDetails.class);
                        i.putExtra("contest_id", contest_id);
                        i.putExtra("invite",true);
                        startActivity(i);
                        finish();
                    }
                    else
                    {
                        Constants.SnakeMessageYellow(lyt_parent,response.optString("message"));
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse)
                {
                    progressDialog.dismiss();
                    Constants.SnakeMessageYellow(lyt_parent, "Something went wrong.Please try again.");
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse)
                {
                    progressDialog.dismiss();
                    Constants.SnakeMessageYellow(lyt_parent, "Something went wrong.Please try again.");
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable)
                {
                    progressDialog.dismiss();
                    Constants.SnakeMessageYellow(lyt_parent, "Something went wrong.Please try again.");
                    super.onFailure(statusCode, headers, responseString, throwable);
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onSquadMobileEnteredListener(SquadModel squadModel, int position, String mobile_number, String country_code)
    {
        boolean mobile_exist=false;

        for (int i=0;i<squadModelArrayList.size();i++)
        {
            if(squadModelArrayList.get(i).getMobile_number().equalsIgnoreCase(mobile_number))
             mobile_exist=true;
        }

            if (mobile_exist)
            Constants.SnakeMessageYellow(lyt_parent, "Mobile Number already entered.");
        else
        {
            if (mobile_number.equalsIgnoreCase(Pref.getValue(ActivitySquadRegistration.this,Constants.MobileNumber,"",Constants.FILENAME)))
                Constants.SnakeMessageYellow(lyt_parent, "Enter details other than yours.");
            else
                verifyContestUser(mobile_number,position,country_code);
        }

    }

    private void verifyContestUser(String mobile_number, int position, String country_code)
    {
        final ProgressDialog progressDialog = new ProgressDialog(ActivitySquadRegistration.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<VerifyContestUserModel> verifyContestUserModelCall = apiInterface.verifyContestUser(Constants.VERIFY_CONTEST_USER,country_code,mobile_number,game_id);

        verifyContestUserModelCall.enqueue(new Callback<VerifyContestUserModel>()
        {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<VerifyContestUserModel> call, @NonNull Response<VerifyContestUserModel> response)
            {
                progressDialog.dismiss();
                VerifyContestUserModel verifyContestUserModel = response.body();
                assert verifyContestUserModel != null;
                if (verifyContestUserModel.status.equalsIgnoreCase("success"))
                {
                    squadModelArrayList.get(position).setGamerjiName(verifyContestUserModel.data.TeamName);
                    squadModelArrayList.get(position).setMobile_number(verifyContestUserModel.data.MobileNumber);
                    squadModelArrayList.get(position).setUser_name(verifyContestUserModel.data.InGameName);
                    squadModelArrayList.get(position).setUser_id(verifyContestUserModel.data.UserID);
                    squadModelArrayList.get(position).setAddplayer(true);
                    squadRegistrationAdapter.notifyDataSetChanged();
                }
                else
                {
                    Constants.SnakeMessageYellow(lyt_parent, verifyContestUserModel.message);
                }
            }
            @Override
            public void onFailure(@NonNull Call<VerifyContestUserModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onPlayerRemoveListener(int position)
    {
        squadModelArrayList.get(position).setUser_name("");
        squadModelArrayList.get(position).setMobile_number("");
        squadModelArrayList.get(position).setGamerjiName("");
        squadModelArrayList.get(position).setUser_id("");
        squadModelArrayList.get(position).setAddplayer(false);
        squadRegistrationAdapter.notifyDataSetChanged();
    }
}
