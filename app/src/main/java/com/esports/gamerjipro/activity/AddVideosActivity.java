package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.esports.gamerjipro.Model.AddVideosModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APICommonMethods;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.RestApis.WebFields;
import com.esports.gamerjipro.Utils.AppCallbackListener;
import com.esports.gamerjipro.Utils.AppUtil;
import com.esports.gamerjipro.Utils.Common;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.SessionManager;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddVideosActivity extends BaseActivity implements AppCallbackListener.CallBackListener {

    private RelativeLayout mRelativeMainView;
    private ImageView mImageBack;
    private EditText mEditVideosTitle, mEditVideosLink;
    private Button mButtonSubmit;
    private String mVideosTitle, mVideosLink;
    private AppUtil mAppUtils;
    private SessionManager mSessionManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_videos);
        mAppUtils = new AppUtil(this);
        mSessionManager = new SessionManager();

        getIds();
        setRegListeners();
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Relative Layout
            mRelativeMainView = findViewById(R.id.relative_add_videos_main_view);

            // Image View
            mImageBack = findViewById(R.id.image_add_videos_back);
            mEditVideosTitle = findViewById(R.id.edit_add_videos_title);
            mEditVideosLink = findViewById(R.id.edit_add_videos_link);

            // Buttons
            mButtonSubmit = findViewById(R.id.button_add_videos_submit);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: On Click Listener
            mImageBack.setOnClickListener(clickListener);
            mButtonSubmit.setOnClickListener(clickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.image_add_videos_back:
                    finish();
                    break;

                case R.id.button_add_videos_submit:
                    doAddVideos();
                    break;
            }
        }
    };

    /**
     * This method checks the validation first and then call the API
     */
    @SuppressLint("SetTextI18n")
    private void doAddVideos() {
        hideSoftKeyboard();
        if (checkValidation()) {
            if (!mAppUtils.getConnectionState()) {
                mAppUtils.displayNoInternetSnackBar(mRelativeMainView, new AppCallbackListener(this));
            } else {
                callToAddVideosAPI();
            }
        }
    }

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return - returns true if all validations are correct otherwise return false and shows error
     */
    private boolean checkValidation() {
        boolean status = true;

        mVideosTitle = mEditVideosTitle.getText().toString().trim();
        mVideosLink = mEditVideosLink.getText().toString().trim();

        mEditVideosTitle.setError(null);
        mEditVideosLink.setError(null);

        if (TextUtils.isEmpty(mVideosTitle)) {
            status = false;
            mEditVideosTitle.setError(getResources().getString(R.string.error_field_required));
        }

        if (TextUtils.isEmpty(mVideosLink)) {
            status = false;
            mEditVideosLink.setError(getResources().getString(R.string.error_field_required));
        }

        if (status) {
            if (!Patterns.WEB_URL.matcher(mVideosLink).matches()) {
                status = false;
                Constants.SnakeMessageYellow(mRelativeMainView, getResources().getString(R.string.error_valid_youtube_url));
            }
        }
        return status;
    }

    /**
     * This should add videos
     */
    private void callToAddVideosAPI() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(AddVideosActivity.this);
            progressDialog.setMessage("Please wait...."); // Setting Message
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
            progressDialog.show(); // Display Progress Dialog
            progressDialog.setCancelable(false);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setAddVideosJson(mSessionManager.getUserId(AddVideosActivity.this), mVideosTitle, mVideosLink));

            Call<AddVideosModel> call = APIClient.getClient().create(APIInterface.class).addVideos(body);
            call.enqueue(new Callback<AddVideosModel>() {
                @Override
                public void onResponse(@NonNull Call<AddVideosModel> call, @NonNull Response<AddVideosModel> response) {

                    progressDialog.dismiss();
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);

                        String mStatus = jsonObject.getString(WebFields.STATUS);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);

                        assert response.body() != null;
                        if (mStatus.equalsIgnoreCase(Constants.API_SUCCESS)) {
                            finish();
                            Constants.SnakeMessageYellow(mRelativeMainView, mMessage);
                        } else {
                            finish();
                            Constants.SnakeMessageYellow(mRelativeMainView, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddVideosModel> call, @NonNull Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAppCallback(int Code) {
        doAddVideos();
    }
}