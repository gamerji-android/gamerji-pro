package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.esports.gamerjipro.Model.ResendOTPModel;
import com.esports.gamerjipro.Model.SignInModel;
import com.esports.gamerjipro.Model.SignUpModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.FileUtils;
import com.esports.gamerjipro.Utils.Pref;
import com.esports.gamerjipro.adGyde.AdGydeConstants;
import com.esports.gamerjipro.adGyde.CommonEvents;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.hbb20.CountryCodePicker;

import org.json.JSONException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Activity_Login extends AppCompatActivity implements View.OnClickListener {
    // Today commitsss
    // New
    TextView txt_show, txt_mobile_login, txt_email_login, txt_signup, txt_fgt_pass, promo_applied_text_social, img_remove_coupon_social;
    EditText et_pwd, et_email, et_phn, et_phnone_botom, et_coupon_social;
    LinearLayout lyt_email_signin, lyt_mobile_login;
    RelativeLayout lyt_sign_in;
    int type = 1;
    APIInterface apiInterface;
    CountryCodePicker ccp;
    ImageView img_close;
    GoogleSignInClient mGoogleSignInClient;
    int RC_SIGN_IN = 100;
    private FirebaseAuth mAuth;
    BottomSheetBehavior sheetBehavior;
    LinearLayout layoutBottomSheet;
    CallbackManager callbackManager = CallbackManager.Factory.create();
    View bg;
    AccessToken accessToken;
    String uid, email;
    CardView cv_fb, cv_google, card_submit_number, cv_apply_coupon_social;
    boolean apply_promo = false;
    boolean promo_success = false;
    boolean is_Social = false;
    ScrollView parent_lyt;
    ImageView img_check_social;
    CardView cv_mobile, cv_pass, cv_email;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        txt_show = findViewById(R.id.txt_show);
        lyt_email_signin = findViewById(R.id.lyt_email_signin);
        txt_fgt_pass = findViewById(R.id.txt_fgt_pass);
        ccp = findViewById(R.id.ccp);
        img_check_social = findViewById(R.id.img_check_social);
        et_coupon_social = findViewById(R.id.et_coupon_social);
        bg = findViewById(R.id.bg);
        cv_fb = findViewById(R.id.cv_fb);
        cv_google = findViewById(R.id.cv_google);
        card_submit_number = findViewById(R.id.card_submit_number);
        et_phnone_botom = findViewById(R.id.et_phnone_botom);
        cv_mobile = findViewById(R.id.cv_mobile);
        cv_email = findViewById(R.id.cv_email);
        cv_pass = findViewById(R.id.cv_pass);

        img_close = findViewById(R.id.img_close);
        layoutBottomSheet = findViewById(R.id.bottom_sheet_number);
        lyt_mobile_login = findViewById(R.id.lyt_mobile_login);
        et_email = findViewById(R.id.et_email);
        et_phn = findViewById(R.id.et_phn);
        txt_email_login = findViewById(R.id.txt_email_login);
        txt_mobile_login = findViewById(R.id.txt_mobile_login);
        lyt_sign_in = findViewById(R.id.lyt_sign_in);
        txt_signup = findViewById(R.id.txt_signup);
        parent_lyt = findViewById(R.id.parent_lyt);

        cv_apply_coupon_social = findViewById(R.id.cv_apply_coupon_social);
        img_remove_coupon_social = findViewById(R.id.img_remove_coupon_social);
        promo_applied_text_social = findViewById(R.id.promo_applied_text_social);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        et_pwd = findViewById(R.id.et_pwd);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        mAuth = FirebaseAuth.getInstance();
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        txt_show.setOnClickListener(this);
        txt_email_login.setOnClickListener(this);
        txt_mobile_login.setOnClickListener(this);
        txt_signup.setOnClickListener(this);
        lyt_sign_in.setOnClickListener(this);
        cv_fb.setOnClickListener(this);
        cv_google.setOnClickListener(this);
        card_submit_number.setOnClickListener(this);
        img_close.setOnClickListener(this);
        txt_fgt_pass.setOnClickListener(this);
        cv_apply_coupon_social.setOnClickListener(this);
        img_remove_coupon_social.setOnClickListener(this);
        promo_applied_text_social.setOnClickListener(this);

        cv_mobile.setOnClickListener(v -> FileUtils.requestfoucs(Activity_Login.this, et_phn));

        cv_email.setOnClickListener(v ->
                FileUtils.requestfoucs(Activity_Login.this, et_email));

        cv_pass.setOnClickListener(v ->
                FileUtils.requestfoucs(Activity_Login.this, et_pwd));

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        accessToken = loginResult.getAccessToken();
                        uid = String.valueOf(accessToken.getUserId());

                        // LoginManager.getInstance().logOut();
                        getUserInformation(loginResult);
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });


        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        bg.setVisibility(View.GONE);
                        break;
                    }

                    case BottomSheetBehavior.STATE_EXPANDED: {
                        bg.setVisibility(View.VISIBLE);
                        break;
                    }
                    case BottomSheetBehavior.STATE_DRAGGING: {
                        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {


            }
        });

        et_coupon_social.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (et_coupon_social.getText().length() < 1) {
                    apply_promo = false;
                }
            }
        });
    }


    private void getUserInformation(final LoginResult loginResul) {
        GraphRequest request = GraphRequest.newMeRequest(loginResul.getAccessToken(),
                (object, response) ->
                {
                    // Application code
                    if (object.has("email")) {
                        try {
                            this.email = object.getString("email");
                            String name = object.getString("name"); // 01/31/1980 format
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        checkSocialSignIn(loginResul.getAccessToken().getUserId(), object.optString("email"), Constants.FACEBOOK, object.optString("first_name"), object.optString("last_name"));
                    } else {
                        Constants.SnakeMessageYellow(parent_lyt, "Email not found.");
                    }
                    // downloadFBFile(object.getJSONObject("picture").getJSONObject("data").getString("url"),object);
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,first_name,last_name");
        request.setParameters(parameters);
        request.executeAsync();
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_show: {
                if (txt_show.getText().toString().equalsIgnoreCase("Show")) {
                    et_pwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    txt_show.setText("Hide");
                } else {
                    et_pwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    txt_show.setText("Show");
                    break;
                }
            }
            case R.id.txt_email_login: {
                type = 2;
                lyt_email_signin.setVisibility(View.VISIBLE);
                lyt_mobile_login.setVisibility(View.GONE);
                txt_mobile_login.setVisibility(View.VISIBLE);
                break;
            }
            case R.id.txt_mobile_login: {
                type = 1;
                lyt_mobile_login.setVisibility(View.VISIBLE);
                lyt_email_signin.setVisibility(View.GONE);
                txt_mobile_login.setVisibility(View.GONE);

                break;
            }
            case R.id.txt_signup: {
                Intent i = new Intent(this, Activity_SignUp.class);
                startActivity(i);
                break;
            }

            case R.id.cv_google: {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
            }

            case R.id.cv_fb: {
                LoginManager.getInstance().logInWithReadPermissions(this,
                        Arrays.asList("email", "public_profile"));
                break;
            }


            case R.id.lyt_sign_in: {
                if (type == 1) {
                    validateMobile();
                } else {
                    validateEmail();
                }
                break;
            }

            case R.id.card_submit_number: {

                if (et_phnone_botom.getText().length() < 10)
                    Constants.SnakeMessageYellow(parent_lyt, "Please enter a valid mobile number");
                else {
                    checkMobile(email, et_phnone_botom.getText().toString());
                }
                break;
            }
            case R.id.img_close: {
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                is_Social = false;
                apply_promo = false;
                promo_success = false;
                img_remove_coupon_social.setVisibility(View.GONE);
                img_check_social.setVisibility(View.GONE);
                cv_apply_coupon_social.setVisibility(View.VISIBLE);
                promo_applied_text_social.setVisibility(View.GONE);
                break;
            }

            case R.id.txt_fgt_pass: {
                Intent i = new Intent(this, ActivityForgotPassword.class);
                startActivity(i);
                break;
            }

            case R.id.cv_apply_coupon_social: {
                applyPromo(et_coupon_social.getText().toString());
                break;
            }

            case R.id.img_remove_coupon_social: {
                img_remove_coupon_social.setVisibility(View.GONE);
                cv_apply_coupon_social.setVisibility(View.VISIBLE);
                promo_success = false;
                img_check_social.setVisibility(View.GONE);
                apply_promo = false;
                promo_applied_text_social.setVisibility(View.GONE);
                break;
            }


        }
    }

    private void applyPromo(String s) {
        final ProgressDialog progressDialog = new ProgressDialog(Activity_Login.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<SignUpModel> signUpModelCall = apiInterface.applyPromo(Constants.ValidateSignupCode, s);

        signUpModelCall.enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(@NonNull Call<SignUpModel> call, @NonNull Response<SignUpModel> response) {
                progressDialog.dismiss();
                SignUpModel signUpModelCall = response.body();
                assert signUpModelCall != null;
                if (signUpModelCall.status.equalsIgnoreCase("success")) {
                    if (is_Social) {
                        apply_promo = true;
                        promo_success = true;

                        Constants.SnakeMessageYellow(parent_lyt, signUpModelCall.message);
                        img_remove_coupon_social.setVisibility(View.VISIBLE);
                        cv_apply_coupon_social.setVisibility(View.GONE);
                        promo_applied_text_social.setText(et_coupon_social.getText().toString() + " Coupon Applied");
                        promo_applied_text_social.setVisibility(View.VISIBLE);
                    }

                } else {
                    if (is_Social) {
                        promo_success = false;
                        apply_promo = true;
                        Constants.SnakeMessageYellow(parent_lyt, signUpModelCall.message);
                        promo_applied_text_social.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<SignUpModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void checkMobile(String email, String phone_number) {
        final ProgressDialog progressDialog = new ProgressDialog(Activity_Login.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<SignUpModel> signUpModelCall = apiInterface.checkMobile(Constants.Check, ccp.getSelectedCountryCodeWithPlus(), phone_number, email);

        signUpModelCall.enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(@NonNull Call<SignUpModel> call, @NonNull Response<SignUpModel> response) {
                progressDialog.dismiss();
                SignUpModel signUpModelCall = response.body();
                assert signUpModelCall != null;
                if (signUpModelCall.status.equalsIgnoreCase("success")) {
                    if (apply_promo) {
                        if (promo_success) {
                            sendOTP();
                        } else {
                            Constants.SnakeMessageYellow(parent_lyt, "Enter valid coupon code.");
                        }
                    } else {
                        if (et_coupon_social.getText().toString().isEmpty())
                            sendOTP();
                        else
                            Constants.SnakeMessageYellow(parent_lyt, "Remove coupon or Enter valid coupon code.");
                    }

                } else {
                    Constants.SnakeMessageYellow(parent_lyt, signUpModelCall.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<SignUpModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void sendOTP() {
        final ProgressDialog progressDialog = new ProgressDialog(Activity_Login.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<ResendOTPModel> resendOTPModelCall = apiInterface.resendOTP(Constants.REQUEST, ccp.getSelectedCountryCodeWithPlus(), et_phnone_botom.getText().toString());

        resendOTPModelCall.enqueue(new Callback<ResendOTPModel>() {
            @Override
            public void onResponse(@NonNull Call<ResendOTPModel> call, @NonNull Response<ResendOTPModel> response) {
                progressDialog.dismiss();
                ResendOTPModel resendOTPModel = response.body();
                assert resendOTPModel != null;
                if (resendOTPModel.status.equalsIgnoreCase("success")) {
                    Intent i = new Intent(Activity_Login.this, ActivityOTP.class);
                    i.putExtra("OTP", resendOTPModel.otpData.OTPCode);
                    i.putExtra("country_code", ccp.getSelectedCountryCodeWithPlus());
                    i.putExtra("phone", et_phnone_botom.getText().toString());
                    i.putExtra("coupon", et_coupon_social.getText().toString());
                    i.putExtra("from_login", true);
                    i.putExtra("is_social", true);
                    startActivity(i);
                } else {
                    Constants.SnakeMessageYellow(parent_lyt, resendOTPModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResendOTPModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }

        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                // ...
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct) {

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task ->
                {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        FirebaseUser user = mAuth.getCurrentUser();

                        email = acct.getEmail();

                        String firstname, lastname;
                        firstname = acct.getDisplayName().split(" ")[0];
                        lastname = acct.getDisplayName().split(" ")[1];
                        checkSocialSignIn(acct.getId(), acct.getEmail(), Constants.GOOGLE, firstname, lastname);
                    } else {
                        // If sign in fails, display a message to the user.
                    }
                });
    }

    private void checkSocialSignIn(String social_id, final String email, String social_type, String firstname, String lastname) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<SignInModel> signInModelCall;

        if (social_type.equalsIgnoreCase(Constants.GOOGLE))
            signInModelCall = apiInterface.doSignUp(Constants.REQUEST, "", "", email, "", firstname, lastname, "", Constants.GOOGLE, "", social_id);
        else
            signInModelCall = apiInterface.doSignUp(Constants.REQUEST, "", "", email, "", firstname, lastname, "", Constants.FACEBOOK, social_id, "");

        signInModelCall.enqueue(new Callback<SignInModel>() {
            @Override
            public void onResponse(@NonNull Call<SignInModel> call, @NonNull Response<SignInModel> response) {
                progressDialog.dismiss();
                SignInModel signInModel = response.body();
                assert signInModel != null;
                if (signInModel.status.equalsIgnoreCase("success")) {

                    if (social_type.equalsIgnoreCase(Constants.GOOGLE)) {
                        CommonEvents.setCountingEvent(AdGydeConstants.KEY_LOGIN, AdGydeConstants.VALUE_LOGIN_VIA_GOOGLE);
                    } else {
                        CommonEvents.setCountingEvent(AdGydeConstants.KEY_LOGIN, AdGydeConstants.VALUE_LOGIN_VIA_FACEBOOK);
                    }

                    if (signInModel.flag.equalsIgnoreCase(Constants.MOBILE_VERIFICATION_PENDING)) {
                        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                            is_Social = true;
                            Pref.setValue(Activity_Login.this, Constants.UserID, signInModel.userDataClass.UserID, Constants.FILENAME);
                        }
                    } else {
                        Pref.setValue(Activity_Login.this, Constants.UserID, signInModel.userDataClass.UserID, Constants.FILENAME);
                        Pref.setValue(Activity_Login.this, Constants.FullName, signInModel.userDataClass.FullName, Constants.FILENAME);
                        Pref.setValue(Activity_Login.this, Constants.lastname, signInModel.userDataClass.LastName, Constants.FILENAME);
                        Pref.setValue(Activity_Login.this, Constants.firstname, signInModel.userDataClass.FirstName, Constants.FILENAME);
                        Pref.setValue(Activity_Login.this, Constants.TeamName, signInModel.userDataClass.TeamName, Constants.FILENAME);
                        Pref.setValue(Activity_Login.this, Constants.CountryCode, signInModel.userDataClass.CountryCode, Constants.FILENAME);
                        Pref.setValue(Activity_Login.this, Constants.MobileNumber, signInModel.userDataClass.MobileNumber, Constants.FILENAME);
                        Pref.setValue(Activity_Login.this, Constants.EmailAddress, signInModel.userDataClass.EmailAddress, Constants.FILENAME);
                        if (!signInModel.userDataClass.BirthDate.isEmpty()) {

                            @SuppressLint("SimpleDateFormat") DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                            @SuppressLint("SimpleDateFormat") DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
                            String inputDateStr = signInModel.userDataClass.BirthDate;
                            Date date = null;
                            try {
                                date = inputFormat.parse(inputDateStr);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            String outputDateStr = outputFormat.format(date);
                            Pref.setValue(Activity_Login.this, Constants.BirthDate, outputDateStr, Constants.FILENAME);
                        }
                        Pref.setValue(Activity_Login.this, Constants.State, signInModel.userDataClass.State, Constants.FILENAME);

                        Constants.setUserGameData(signInModel.userDataClass.GamesData.gamesDataArrayList, Activity_Login.this);
                        Pref.setValue(Activity_Login.this, Constants.IS_LOGIN, true, Constants.FILENAME);
                        Pref.setValue(Activity_Login.this, Constants.SOCIAL_LOGIN, true, Constants.FILENAME);

//                        Intent i = new Intent(Activity_Login.this, ActivityMain.class);
                        Intent i = new Intent(Activity_Login.this, BottomNavigationActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        finish();
                    }
                } else {
                    Constants.SnakeMessageYellow(parent_lyt, signInModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<SignInModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void validateEmail() {
        if (!Patterns.EMAIL_ADDRESS.matcher(et_email.getText()).matches())
            Constants.SnakeMessageYellow(parent_lyt, "Please enter valid email address");
        else if (et_pwd.getText().toString().isEmpty())
            Constants.SnakeMessageYellow(parent_lyt, "Please enter a password");
        else if (et_pwd.getText().length() < 6)
            Constants.SnakeMessageYellow(parent_lyt, "Password length must be 8-20 characters");
        else
            sendEmailToServer();
    }

    private void sendEmailToServer() {
        final ProgressDialog progressDialog = new ProgressDialog(Activity_Login.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<SignInModel> signUpModelCall = apiInterface.doEmailSignIn(type, et_email.getText().toString(), et_pwd.getText().toString());

        signUpModelCall.enqueue(new Callback<SignInModel>() {
            @Override
            public void onResponse(@NonNull Call<SignInModel> call, @NonNull Response<SignInModel> response) {
                progressDialog.dismiss();
                SignInModel signInModel = response.body();
                assert signInModel != null;
                if (signInModel.status.equalsIgnoreCase("success")) {
                    CommonEvents.setCountingEvent(AdGydeConstants.KEY_LOGIN, AdGydeConstants.VALUE_LOGIN_VIA_EMAIL);
                    Pref.setValue(Activity_Login.this, Constants.UserID, signInModel.userDataClass.UserID, Constants.FILENAME);
                    Pref.setValue(Activity_Login.this, Constants.firstname, signInModel.userDataClass.FirstName, Constants.FILENAME);
                    Pref.setValue(Activity_Login.this, Constants.TeamName, signInModel.userDataClass.TeamName, Constants.FILENAME);
                    Pref.setValue(Activity_Login.this, Constants.lastname, signInModel.userDataClass.LastName, Constants.FILENAME);
                    Pref.setValue(Activity_Login.this, Constants.CountryCode, signInModel.userDataClass.CountryCode, Constants.FILENAME);
                    Pref.setValue(Activity_Login.this, Constants.MobileNumber, signInModel.userDataClass.MobileNumber, Constants.FILENAME);
                    Pref.setValue(Activity_Login.this, Constants.EmailAddress, signInModel.userDataClass.EmailAddress, Constants.FILENAME);
                    if (!signInModel.userDataClass.BirthDate.isEmpty()) {
                        @SuppressLint("SimpleDateFormat") DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                        @SuppressLint("SimpleDateFormat") DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
                        String inputDateStr = signInModel.userDataClass.BirthDate;
                        Date date = null;
                        try {
                            date = inputFormat.parse(inputDateStr);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        String outputDateStr = outputFormat.format(date);
                        Pref.setValue(Activity_Login.this, Constants.BirthDate, outputDateStr, Constants.FILENAME);
                    }
                    Pref.setValue(Activity_Login.this, Constants.State, signInModel.userDataClass.State, Constants.FILENAME);
                    Pref.setValue(Activity_Login.this, Constants.IS_LOGIN, true, Constants.FILENAME);
                    Constants.setUserGameData(signInModel.userDataClass.GamesData.gamesDataArrayList, Activity_Login.this);

//                    Intent i = new Intent(Activity_Login.this, ActivityMain.class);
                    Intent i = new Intent(Activity_Login.this, BottomNavigationActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    finish();

                } else {
                    Constants.SnakeMessageYellow(parent_lyt, signInModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<SignInModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }

        });
    }

    private void validateMobile() {
        if (et_phn.getText().length() < 10)
            et_phn.setError("Please enter a valid mobile number");
        else
            sendMobileToServer();
    }

    private void sendMobileToServer() {

        final ProgressDialog progressDialog = new ProgressDialog(Activity_Login.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<SignInModel> signUpModelCall = apiInterface.doMobileSignIn(type, ccp.getSelectedCountryCodeWithPlus(), et_phn.getText().toString().trim());

        signUpModelCall.enqueue(new Callback<SignInModel>() {
            @Override
            public void onResponse(@NonNull Call<SignInModel> call, @NonNull Response<SignInModel> response) {
                progressDialog.dismiss();
                SignInModel signInModel = response.body();
                assert signInModel != null;
                if (signInModel.status.equalsIgnoreCase("success")) {
                    CommonEvents.setCountingEvent(AdGydeConstants.KEY_LOGIN, AdGydeConstants.VALUE_LOGIN_VIA_MOBILE);
                    Constants.setUserGameData(signInModel.userDataClass.GamesData.gamesDataArrayList, Activity_Login.this);
                    Pref.setValue(Activity_Login.this, Constants.UserID, signInModel.userDataClass.UserID, Constants.FILENAME);
                    Pref.setValue(Activity_Login.this, Constants.FullName, signInModel.userDataClass.FullName, Constants.FILENAME);
                    Pref.setValue(Activity_Login.this, Constants.CountryCode, signInModel.userDataClass.CountryCode, Constants.FILENAME);
                    Pref.setValue(Activity_Login.this, Constants.firstname, signInModel.userDataClass.FirstName, Constants.FILENAME);
                    Pref.setValue(Activity_Login.this, Constants.lastname, signInModel.userDataClass.LastName, Constants.FILENAME);
                    Pref.setValue(Activity_Login.this, Constants.TeamName, signInModel.userDataClass.TeamName, Constants.FILENAME);
                    Pref.setValue(Activity_Login.this, Constants.MobileNumber, signInModel.userDataClass.MobileNumber, Constants.FILENAME);
                    Pref.setValue(Activity_Login.this, Constants.EmailAddress, signInModel.userDataClass.EmailAddress, Constants.FILENAME);
                    Pref.setValue(Activity_Login.this, Constants.CanPlayGames, signInModel.userDataClass.CanPlayGames, Constants.FILENAME);
                    if (!signInModel.userDataClass.BirthDate.isEmpty()) {
                        @SuppressLint("SimpleDateFormat") DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                        @SuppressLint("SimpleDateFormat") DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
                        String inputDateStr = signInModel.userDataClass.BirthDate;
                        Date date = null;
                        try {
                            date = inputFormat.parse(inputDateStr);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        String outputDateStr = outputFormat.format(date);
                        Pref.setValue(Activity_Login.this, Constants.BirthDate, outputDateStr, Constants.FILENAME);
                    }
                    Pref.setValue(Activity_Login.this, Constants.State, signInModel.userDataClass.State, Constants.FILENAME);

                    Constants.setUserGameData(signInModel.userDataClass.GamesData.gamesDataArrayList, Activity_Login.this);

                    Intent i = new Intent(Activity_Login.this, ActivityOTP.class);
                    i.putExtra("OTP", signInModel.userDataClass.OTPCode);
                    i.putExtra("country_code", signInModel.userDataClass.CountryCode);
                    i.putExtra("phone", signInModel.userDataClass.MobileNumber);
                    i.putExtra("from_login", true);
                    i.putExtra("is_social", false);
                    startActivity(i);

                } else {
                    Constants.SnakeMessageYellow(parent_lyt, signInModel.message);
                    Toast.makeText(Activity_Login.this, signInModel.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<SignInModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }

        });
    }
}
