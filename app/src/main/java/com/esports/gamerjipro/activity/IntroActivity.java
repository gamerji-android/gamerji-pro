package com.esports.gamerjipro.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.esports.gamerjipro.Adapter.SlidingTextAdapter;
import com.esports.gamerjipro.R;
import com.tbuonomo.viewpagerdotsindicator.WormDotsIndicator;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class IntroActivity extends AppCompatActivity implements View.OnClickListener
{
    private ViewPager mPager;
    private  int currentPage = 0;
    private  int NUM_PAGES = 0;
//    private ArrayList<String> TitleArray = new ArrayList<>();
//    private ArrayList<String> InfoArray = new ArrayList<>();
    private ArrayList<Integer> ImagesArray = new ArrayList<>();

    TextView txt_login,txt_signup,txt_skip;
    Timer swipeTimer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_intro);
        init();
    }

    private void init()
    {
//        List<String> slider_titles = Arrays.asList(getResources().getStringArray(R.array.slider_title));
//        List<String> slider_infos = Arrays.asList(getResources().getStringArray(R.array.slider_info));

        ImagesArray.add(R.drawable.intro1);
        ImagesArray.add(R.drawable.intro2);
        ImagesArray.add(R.drawable.intro3);
        ImagesArray.add(R.drawable.intro4);

//        TitleArray.addAll(slider_titles);
//        InfoArray.addAll(slider_infos);

        txt_login=findViewById(R.id.txt_login);
        txt_signup=findViewById(R.id.txt_signup);
        txt_skip=findViewById(R.id.txt_skip);
        txt_login.setOnClickListener(this);
        txt_signup.setOnClickListener(this);
        txt_skip.setOnClickListener(this);


        mPager = findViewById(R.id.pager);

        mPager.setAdapter(new SlidingTextAdapter(IntroActivity.this, ImagesArray));


        WormDotsIndicator indicator = findViewById(R.id.dots_indicator);
        indicator.setViewPager(mPager);
        NUM_PAGES = ImagesArray.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = () -> {
            if (currentPage == NUM_PAGES)
            {
                currentPage = 0;
                swipeTimer.cancel();
            }
            mPager.setCurrentItem(currentPage++, true);
        };
         swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                handler.post(Update);
            }
        }, 0, 4000);

    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.txt_login:
            {
                Intent i = new Intent(this,Activity_Login.class);
                startActivity(i);
                break;
            }

            case R.id.txt_signup:
            {
                Intent i = new Intent(this,Activity_SignUp.class);
                startActivity(i);
                break;
            }

            case R.id.txt_skip:
            {
                Intent i = new Intent(this,Activity_Login.class);
                startActivity(i);
                break;
            }
        }

    }
}
