package com.esports.gamerjipro.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.esports.gamerjipro.Adapter.GamerjiPointsAdapter;
import com.esports.gamerjipro.Model.GamerjiPointsModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Common;
import com.esports.gamerjipro.Utils.Constants;
import com.ironsource.mediationsdk.ISBannerSize;
import com.ironsource.mediationsdk.IronSource;
import com.ironsource.mediationsdk.IronSourceBannerLayout;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.sdk.BannerListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityGamerjiPoints extends AppCompatActivity
{
    TextView txt_title;
    RecyclerView mRecyclerNotifications;
    APIInterface apiInterface;
    RelativeLayout mNotificationRootView;
    GamerjiPointsAdapter gamerjiPointsAdapter ;
    ImageView img_back;
    IronSourceBannerLayout banner;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        txt_title=findViewById(R.id.txt_title);
        img_back=findViewById(R.id.img_back);
        img_back.setOnClickListener(v -> finish());
        mNotificationRootView=findViewById(R.id.mNotificationRootView);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        mRecyclerNotifications=findViewById(R.id.mRecyclerNotifications);
        mRecyclerNotifications.setLayoutManager(new LinearLayoutManager(this));
        txt_title.setText(getResources().getString(R.string.gamerjipoints));
        getGamerjiPoints();

        // Ironsrc Banner Ads
        IronSource.init(this, "d9dedc89", IronSource.AD_UNIT.BANNER);
        final FrameLayout bannerContainer = findViewById(R.id.bannerContainer);
        banner = IronSource.createBanner(this, ISBannerSize.BANNER);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        bannerContainer.addView(banner, 0, layoutParams);

        banner.setBannerListener(new BannerListener() {
            @Override
            public void onBannerAdLoaded() {
                banner.setVisibility(View.VISIBLE);
            }
            @Override
            public void onBannerAdLoadFailed(IronSourceError error) {

            }
            @Override
            public void onBannerAdClicked() {

            }
            @Override
            public void onBannerAdScreenPresented() {

            }
            @Override
            public void onBannerAdScreenDismissed() {

            }
            @Override
            public void onBannerAdLeftApplication() {

            }
        });

        IronSource.loadBanner(banner);
    }

    private void getGamerjiPoints()
    {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityGamerjiPoints.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<GamerjiPointsModel> gamerjiPointsModelCall = apiInterface.getGamerjiPoints(Constants.GET_ALL_POINTS);

        gamerjiPointsModelCall.enqueue(new Callback<GamerjiPointsModel>()
        {
            @Override
            public void onResponse(@NonNull Call<GamerjiPointsModel> call, @NonNull Response<GamerjiPointsModel> response)
            {
                progressDialog.dismiss();
                GamerjiPointsModel gamerjiPointsModel = response.body();
                assert gamerjiPointsModel != null;
                if (gamerjiPointsModel.status.equalsIgnoreCase("success"))
                {
                    gamerjiPointsAdapter= new GamerjiPointsAdapter(ActivityGamerjiPoints.this,gamerjiPointsModel.data.StatesData);
                    mRecyclerNotifications.setAdapter(gamerjiPointsAdapter);
                }
                else
                {
                        Constants.SnakeMessageYellow(mNotificationRootView,gamerjiPointsModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<GamerjiPointsModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    protected void onResume() {
        super.onResume();
        Common.insertLog("onResume");
        IronSource.onResume(this);
    }
    protected void onPause() {
        super.onPause();
        Common.insertLog("onPause");
        IronSource.onPause(this);
    }
    protected void onDestroy() {
        super.onDestroy();
        Common.insertLog("onDestroy");
        IronSource.destroyBanner(banner);
    }
}
