package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.esports.gamerjipro.Model.SignInModel;
import com.esports.gamerjipro.Model.StatesListModel;
import com.esports.gamerjipro.Model.UpdateProfileModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.CustomWheelView;
import com.esports.gamerjipro.Utils.Pref;
import com.esports.gamerjipro.Utils.datepicker.DatePicker;
import com.esports.gamerjipro.Utils.datepicker.DatePickerDialog;
import com.esports.gamerjipro.Utils.datepicker.SpinnerDatePickerDialogBuilder;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityDOB extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener
{
    TextView txt_date_birth,txt_name,et_states,txt_game_name_title,txt_info,txt_info_gmaerji;
    int mYear, mMonth, mDay;
    APIInterface apiInterface;
    ArrayList<String> stateslist= new ArrayList<>();
    ArrayList<String> statesId= new ArrayList<>();
    ArrayList<String> isRestricted= new ArrayList<>();
    RelativeLayout lyt_signup;
    EditText et_sp_text,et_gamerji_username;
    ArrayList<SignInModel.UserData.GamesData.GameData> gamesDataArrayList;
    String game_id,main_game_name;
    LinearLayout mRelativeEditProfileRootView;
    BottomSheetBehavior sheetBehavior;
    RelativeLayout layoutBottomSheet;
    View bg;
    ImageView img_close,img_back;
    CoordinatorLayout parent_layout;
    TextView txt_title_bs,txt_bottom_msg;
    String dob;
    public static ArrayList<SignInModel.UserData.GamesData.GameData> userGameDataarrayList= new ArrayList<>();
    public String UniqueName=" ",game_name,userGameId;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_dob);
        game_id=getIntent().getStringExtra("game_id");
        main_game_name=getIntent().getStringExtra("main_game_name");

        txt_date_birth=findViewById(R.id.txt_date_birth);
        txt_info=findViewById(R.id.txt_info);
        parent_layout=findViewById(R.id.parent_layout);
        txt_info_gmaerji=findViewById(R.id.txt_info_gmaerji);
        bg = findViewById(R.id.bg);
        img_back = findViewById(R.id.img_back);
        mRelativeEditProfileRootView=findViewById(R.id.mRelativeEditProfileRootView);
        et_sp_text=findViewById(R.id.et_sp_text);
        img_close=findViewById(R.id.img_close);
        txt_name=findViewById(R.id.txt_name);
        et_gamerji_username=findViewById(R.id.et_gamerji_username);
        et_states=findViewById(R.id.et_states);
        txt_title_bs=findViewById(R.id.txt_title_bs);
        txt_bottom_msg=findViewById(R.id.txt_bottom_msg);
        lyt_signup=findViewById(R.id.lyt_signup);
        txt_game_name_title =findViewById(R.id.txt_game_name_title);
        txt_name.setText("YOUR "+getIntent().getStringExtra("main_game_name")+" USERNAME?");
        txt_date_birth.setOnClickListener(this);
        lyt_signup.setOnClickListener(this);
        txt_info.setOnClickListener(this);
        txt_info_gmaerji.setOnClickListener(this);
        img_close.setOnClickListener(this);

        img_back.setOnClickListener(v ->
        {
            Intent intent=new Intent();
            intent.putExtra("profile_update_status",false);
            setResult(Constants.PROFILE_UPDATE_FAIL,intent);
            finish();
        });
        gamesDataArrayList= Constants.getUserData(ActivityDOB.this);
        txt_game_name_title.setText("YOUR "+getIntent().getStringExtra("main_game_name")+ " Username");
        et_sp_text.setHint(getIntent().getStringExtra("main_game_name")+" Username");
        /*if (gamesDataArrayList.get(0).UserGameID.equalsIgnoreCase(getIntent().getStringExtra("user_game_id")))
        {
            txt_game_name_title.setText("YOUR"+getIntent().getStringExtra("main_game_name")+ "Username");
        }
        else
        {
            txt_game_name_title.setText("YOUR CLASH ROYALE"+" Username");
            et_sp_text.setHint("CLASH ROYALE Username");
        }*/
        apiInterface = APIClient.getClient().create(APIInterface.class);
        if (!Pref.getValue(ActivityDOB.this,Constants.TeamName,"", Constants.FILENAME).isEmpty())
        {
            et_gamerji_username.setText(Pref.getValue(ActivityDOB.this,Constants.TeamName,"", Constants.FILENAME));
            et_gamerji_username.setEnabled(false);
        }
        getAllStates();
        layoutBottomSheet=findViewById(R.id.bottom_how_to);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        if (!Pref.getValue(ActivityDOB.this,Constants.BirthDate,"", Constants.FILENAME).isEmpty())
        {
            txt_date_birth.setText(Pref.getValue(ActivityDOB.this,Constants.BirthDate,"", Constants.FILENAME));
            txt_date_birth.setEnabled(false);
        }
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback()
        {
            @Override
            public void onStateChanged(@NonNull View view, int newState)
            {
                switch (newState)
                {
                    case BottomSheetBehavior.STATE_HIDDEN:
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        bg.setVisibility(View.GONE);
                        break;
                    }

                    case BottomSheetBehavior.STATE_EXPANDED:
                    {
                        bg.setVisibility(View.VISIBLE);
                        break;
                    }
                    case BottomSheetBehavior.STATE_DRAGGING:
                    {

                    }
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v)
            {


            }
        });
    }

    private void getAllStates()
    {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityDOB.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<StatesListModel> signUpModelCall = apiInterface.getAllStates(Constants.GET_ALL_STATES);

        signUpModelCall.enqueue(new Callback<StatesListModel>()
        {
            @Override
            public void onResponse(@NonNull Call<StatesListModel> call, @NonNull Response<StatesListModel> response)
            {
                progressDialog.dismiss();
                StatesListModel statesListModel = response.body();
                assert statesListModel != null;
                if (statesListModel.status.equalsIgnoreCase("success"))
                {
                    for (int i=0;i<statesListModel.userDataClass.statesDataArrayList.size();i++)
                    {
                        stateslist.add(statesListModel.userDataClass.statesDataArrayList.get(i).Name);
                        statesId.add(statesListModel.userDataClass.statesDataArrayList.get(i).StateID);
                        isRestricted.add(statesListModel.userDataClass.statesDataArrayList.get(i).IsRestricted);
                    }
                    if (Pref.getValue(ActivityDOB.this,Constants.State,"",Constants.FILENAME).equalsIgnoreCase(""))
                    {
                        //et_states.setText(statesListModel.userDataClass.statesDataArrayList.get(0).Name);
                        et_states.setHint("Select a state");
                        et_states.setEnabled(true);
                    }
                    else
                    {
                        et_states.setText(Pref.getValue(ActivityDOB.this,Constants.State,"",Constants.FILENAME));
                        et_states.setEnabled(false);
                    }
                    et_states.setOnClickListener(v -> selectState());

                }

                else
                {
                    Constants.SnakeMessageYellow(parent_layout, statesListModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<StatesListModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void selectState()
    {
        if (ActivityDOB.this == null)
            return;
        if (stateslist.size() == 0)
        {
            Constants.SnakeMessageYellow(mRelativeEditProfileRootView, "no States found.");
            return;
        }
        Dialog dialog = new Dialog(ActivityDOB.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View outerView = LayoutInflater.from(ActivityDOB.this).inflate(R.layout.wheelview, null);
        dialog.setContentView(outerView);
        if (dialog.getWindow() != null)
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        CustomWheelView wv = outerView.findViewById(R.id.wh_view);
        TextView txt_ok = outerView.findViewById(R.id.txt_ok);
                TextView title = outerView.findViewById(R.id.title);
                title.setText("Select state");
        TextView txt_cancel = outerView.findViewById(R.id.txt_cancel);
        txt_ok.setOnClickListener(v ->
        {
            et_states.setText(wv.getSeletedItem().toString());
            dialog.dismiss();

        });




      //  CustomWheelView wv = outerView.findViewById(<CustomWheelView<StateDetail>>);/*<CustomWheelView<StateDetail>>(R.id.wh_view)*/
        /*txt_cancel.setOnClickListener(v ->
        {
            et_states.setText(wv.getSeletedIndex().name)
            selectedStateName = wv.seletedItem.name
            state = wv.seletedItem
            dialog.dismiss()

        });*/
        txt_cancel.setOnClickListener(v -> dialog.dismiss());
        wv.setOffset(1);
        wv.setItems(stateslist);
        wv.setSeletion(0);
        dialog.show();
    }

    /*class StateDetail(String id, String name, String countryId) {
        override fun toString(): String {
            return name
        }
    }*/


    @SuppressLint("SetTextI18n")
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.txt_date_birth:
            {
                try
                {
                    Calendar calendar = Calendar.getInstance();
                    Calendar mMaxDate = Calendar.getInstance();
                    Calendar mMinDate = Calendar.getInstance();
                    mMaxDate.add(Calendar.YEAR, -18);
                    mMaxDate.setTimeInMillis(mMaxDate.getTimeInMillis());
                    mMinDate.set(Calendar.YEAR, 1920);
                    mMinDate.set(Calendar.MONTH, 1);
                    mMinDate.set(Calendar.DATE, 1);

                    int mMaxDateYear = mMaxDate.get(Calendar.YEAR);
                    int mMaxDateMonth = mMaxDate.get(Calendar.MONTH);
                    int mMaxDateDay = mMaxDate.get(Calendar.DAY_OF_MONTH);

                    int mMinDateYear = mMinDate.get(Calendar.YEAR);
                    int mMinDateMonth = mMinDate.get(Calendar.MONTH);
                    int mMinDateDay = mMinDate.get(Calendar.DAY_OF_MONTH);


                    new SpinnerDatePickerDialogBuilder().context(ActivityDOB.this).callback(this).spinnerTheme(R.style.NumberPickerStyle)
                    .showTitle(true).showDaySpinner(true).defaultDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                            .maxDate(mMaxDateYear, mMaxDateMonth, mMaxDateDay)
                            .minDate(mMinDateYear, mMinDateMonth, mMinDateDay).build().show();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;
            }

            case R.id.lyt_signup:
            {
                validate();
                break;
            }

            case R.id.img_close:
            {

                bg.setVisibility(View.GONE);
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                break;
            }



            case R.id.txt_info:
            {
                txt_title_bs.setText("What is this");
                txt_bottom_msg.setText("This is the username that you use on "+getIntent().getStringExtra("main_game_name")+"." +"Once this is entered it cannot be changed.");
                bg.setVisibility(View.VISIBLE);
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                break;
            }
            case R.id.txt_info_gmaerji:
            {
                txt_title_bs.setText("What is this?");
                txt_bottom_msg.setText(R.string.gamerji_info);
                bg.setVisibility(View.VISIBLE);
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;
            }
        }
    }

    private void validate()
    {

        int position= stateslist.indexOf(et_states.getText().toString());
        if (txt_date_birth.getText().toString().isEmpty())
        {
            Constants.SnakeMessageYellow(parent_layout,"Enter date of birth");
        }
        else if (et_sp_text.getText().length()<1)
        {
            Constants.SnakeMessageYellow(parent_layout,"Enter valid name");
        }
        else if (et_gamerji_username.getText().length()<3)
        {
            Constants.SnakeMessageYellow(parent_layout,"Enter GamerJi Teamname");
        }
       /* else if (isRestricted.get(position).equalsIgnoreCase("2"))
        {
            Constants.SnakeMessageYellow(parent_layout,getResources().getString(R.string.restricted_states_msg) );
        }*/
        else
        {
            if (getIntent().getExtras()!=null)
                updateProfile();
            else
                Constants.SnakeMessageYellow(parent_layout,"Something went wrong.Please try again" );
        }
    }

    private void updateProfile()
    {
        final ProgressDialog progressDialog = new ProgressDialog(ActivityDOB.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<UpdateProfileModel> signUpModelCall;
        int state_id= stateslist.indexOf(et_states.getText().toString());

        if (Pref.getValue(ActivityDOB.this,Constants.TeamName,"", Constants.FILENAME).isEmpty())
        {
             signUpModelCall = apiInterface.updateProfile(Constants.UPDATE_GAME_PROFILE, Pref.getValue(ActivityDOB.this, Constants.UserID,"", Constants.FILENAME),
                     dob,statesId.get(state_id),getIntent().getStringExtra("user_game_id"),
                    et_sp_text.getText().toString(),et_gamerji_username.getText().toString());
        }
        else
        {
            signUpModelCall = apiInterface.updateProfile(Constants.UPDATE_GAME_PROFILE, Pref.getValue(ActivityDOB.this, Constants.UserID,"", Constants.FILENAME),
                    dob,statesId.get(state_id),getIntent().getStringExtra("user_game_id"),
                    et_sp_text.getText().toString(),Pref.getValue(ActivityDOB.this,Constants.TeamName,"", Constants.FILENAME));
        }


        signUpModelCall.enqueue(new Callback<UpdateProfileModel>()
        {
            @Override
            public void onResponse(@NonNull Call<UpdateProfileModel> call, @NonNull Response<UpdateProfileModel> response)
            {
                progressDialog.dismiss();
                UpdateProfileModel updateProfileModel = response.body();
                assert updateProfileModel != null;
                if (updateProfileModel.status.equalsIgnoreCase("success"))
                {
                    for (int i=0;i<gamesDataArrayList.size();i++)
                    {
                        if (gamesDataArrayList.get(i).GameID.equalsIgnoreCase(game_id))
                        {
                            gamesDataArrayList.get(i).UniqueName=et_sp_text.getText().toString();
                        }
                    }

                    Constants.setUserGameData(gamesDataArrayList,ActivityDOB.this);
                    userGameDataarrayList.clear();
                    userGameDataarrayList= Constants.getUserData(ActivityDOB.this);
                    Pref.setValue(ActivityDOB.this,Constants.TeamName,et_gamerji_username.getText().toString(), Constants.FILENAME);
                    Pref.setValue(ActivityDOB.this,Constants.BirthDate,txt_date_birth.getText().toString(), Constants.FILENAME);
                    Pref.setValue(ActivityDOB.this,Constants.State,et_states.getText().toString(), Constants.FILENAME);
                    Constants.SnakeMessageYellow(parent_layout,updateProfileModel.message );
                    Intent intent=new Intent();
                    intent.putExtra("profile_update_status",true);
                    setResult(Constants.PROFILE_UPDATE_STATUS_REQUEST_CODE,intent);
                    InputMethodManager imm = null;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M)
                    {
                        imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    }
                    assert imm != null;
                    imm.hideSoftInputFromWindow(et_gamerji_username.getWindowToken(), 0);
                    finish();
                }
                else
                {
                    Constants.SnakeMessageYellow(parent_layout,updateProfileModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<UpdateProfileModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }

        });
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
    {
        txt_date_birth.setText(dayOfMonth+"/"+(monthOfYear+1)+"/"+year);
        dob=year+"/"+(monthOfYear+1)+"/"+dayOfMonth;
    }

    @Override
    public void onBackPressed()
    {
        Intent intent=new Intent();
        intent.putExtra("profile_update_status",false);
        setResult(Constants.PROFILE_UPDATE_FAIL,intent);
        finish();
    }
}
