package com.esports.gamerjipro.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esports.gamerjipro.Adapter.WinnerPoolAdapter;
import com.esports.gamerjipro.Fragment.HomeFragment;
import com.esports.gamerjipro.Fragment.MoreFragment;
import com.esports.gamerjipro.Fragment.MyContestsFragment;
import com.esports.gamerjipro.Fragment.ProfileFragment;
import com.esports.gamerjipro.Fragment.VideosFragment;
import com.esports.gamerjipro.Interface.ProfileGameInfoClickListener;
import com.esports.gamerjipro.Interface.RulesClickListener;
import com.esports.gamerjipro.Interface.WinnerClickListener;
import com.esports.gamerjipro.Model.ContestTypeModelNew;
import com.esports.gamerjipro.Model.ContestTypesModel;
import com.esports.gamerjipro.Model.GameUsernameModel;
import com.esports.gamerjipro.Model.GamerProfileModel;
import com.esports.gamerjipro.Model.MyContestModel;
import com.esports.gamerjipro.Model.RulesModel;
import com.esports.gamerjipro.Model.TournamentDetailModel;
import com.esports.gamerjipro.Model.TournamentTypeModel;
import com.esports.gamerjipro.Model.WinnerContestModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Common;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.Pref;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BottomNavigationActivity extends AppCompatActivity implements RulesClickListener, WinnerClickListener, ProfileGameInfoClickListener {

    private static Fragment fragment;
    private LinearLayout mLinearHome, mLinearMyContests, mLinearProfile, mLinearVideos, mLinearMore;
    private TextView mTextHome, mTextMyContests, mTextVideos, mTextMore;
    public static ImageView mImageHome, mImageMyContests, mImageProfile, mImageVideos, mImageMore, mImageBottomSheetHowToClose,
            mImageBottomSheetPoolPriceClose, mImageBottomSheetProfileBadgeClose, mImageBottomSheetProfileBadge;
    public static int currentTabPosition = 1;
    private long mBackPressed;
    public static String mTabName;
    private static final int TIME_INTERVAL = 2000; // # milliseconds, desired time passed between two back presses.
    private CoordinatorLayout mCoordinatorMain;
    private View mViewBottomSheetBackground;
    private BottomSheetBehavior mBottomSheetHowTo, mBottomSheetPoolPrice, mBottomSheetProfileBadges;
    private TextView mTextBottomSheetHowToTitle, mTextBottomSheetHowToMessage, mTextBottomSheetPoolPriceHeader,
            mTextBottomSheetPoolPriceWinningRupee, mTextBottomSheetPoolPrice, mTextBottomSheetProfileBadgeMessage,
            mTextBottomSheetProfileBadgePoints;
    private RecyclerView mRecyclerView;
    private WinnerPoolAdapter mAdapterWinnerPool;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_navigation);

        getIds();
        setRegListeners();
        setData();
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Linear Layouts
            mLinearHome = findViewById(R.id.linear_tab_home);
            mLinearMyContests = findViewById(R.id.linear_tab_my_contests);
            mLinearProfile = findViewById(R.id.linear_tab_profile);
            mLinearVideos = findViewById(R.id.linear_tab_videos);
            mLinearMore = findViewById(R.id.linear_tab_more);
            LinearLayout mLinearBottomSheetPoolPrice = findViewById(R.id.pool_price_bottom);

            // Text Views
            mTextHome = findViewById(R.id.text_tab_home);
            mTextMyContests = findViewById(R.id.text_tab_my_contests);
            mTextVideos = findViewById(R.id.text_tab_videos);
            mTextMore = findViewById(R.id.text_tab_more);
            mTextBottomSheetHowToTitle = findViewById(R.id.txt_title_bs);
            mTextBottomSheetHowToMessage = findViewById(R.id.txt_bottom_msg);
            mTextBottomSheetPoolPriceHeader = findViewById(R.id.txt_pool_price);
            mTextBottomSheetPoolPriceWinningRupee = findViewById(R.id.txt_winning_rupee);
            mTextBottomSheetPoolPrice = findViewById(R.id.tv_pool_price);
            mTextBottomSheetProfileBadgeMessage = findViewById(R.id.txt_badge_msg);
            mTextBottomSheetProfileBadgePoints = findViewById(R.id.txt_points);

            // Image Views
            mImageHome = findViewById(R.id.image_tab_home);
            mImageMyContests = findViewById(R.id.image_tab_my_contests);
            mImageProfile = findViewById(R.id.image_tab_profile);
            mImageVideos = findViewById(R.id.image_tab_videos);
            mImageMore = findViewById(R.id.image_tab_more);
            mImageBottomSheetHowToClose = findViewById(R.id.img_close);
            mImageBottomSheetPoolPriceClose = findViewById(R.id.iv_close_pool_price);
            mImageBottomSheetProfileBadgeClose = findViewById(R.id.img_close_badges);
            mImageBottomSheetProfileBadge = findViewById(R.id.img_badge);

            // Coordinator Layout
            mCoordinatorMain = findViewById(R.id.coordinator_layout_my_contests_main_view);

            // View
            mViewBottomSheetBackground = findViewById(R.id.view_my_contests_background);

            // Recycler View
            mRecyclerView = findViewById(R.id.rv_pool_price);

            // Relative Layouts
            RelativeLayout mRelativeBottomSheetHowTo = findViewById(R.id.bottom_how_to);
            RelativeLayout mRelativeBottomSheetProfileBadge = findViewById(R.id.bottom_badge);

            // Bottom Sheet Behaviours
            mBottomSheetHowTo = BottomSheetBehavior.from(mRelativeBottomSheetHowTo);
            mBottomSheetPoolPrice = BottomSheetBehavior.from(mLinearBottomSheetPoolPrice);
            mBottomSheetProfileBadges = BottomSheetBehavior.from(mRelativeBottomSheetProfileBadge);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: On Click Listener
            mLinearHome.setOnClickListener(clickListener);
            mLinearMyContests.setOnClickListener(clickListener);
            mLinearProfile.setOnClickListener(clickListener);
            mLinearVideos.setOnClickListener(clickListener);
            mLinearMore.setOnClickListener(clickListener);
            mImageProfile.setOnClickListener(clickListener);
            mImageBottomSheetHowToClose.setOnClickListener(clickListener);
            mImageBottomSheetPoolPriceClose.setOnClickListener(clickListener);
            mImageBottomSheetProfileBadgeClose.setOnClickListener(clickListener);

            // ToDo: How To Bottom Sheet Call Back
            mBottomSheetHowTo.setBottomSheetCallback(howToBottomSheetCallback);
            mBottomSheetPoolPrice.setBottomSheetCallback(poolPriceBottomSheetCallback);
            mBottomSheetProfileBadges.setBottomSheetCallback(profileBadgesBottomSheetCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        try {
            mTabName = getResources().getString(R.string.menu_home);
            currentTabPosition = 1;
            setTab();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.linear_tab_home:
                    currentTabPosition = 1;
                    setTab();
                    break;

                case R.id.linear_tab_my_contests:
                    currentTabPosition = 2;
                    setTab();
                    break;

                case R.id.linear_tab_profile:
                    currentTabPosition = 3;
                    setTab();
                    break;

                case R.id.linear_tab_videos:
                    currentTabPosition = 4;
                    setTab();
                    break;

                case R.id.linear_tab_more:
                    currentTabPosition = 5;
                    setTab();
                    break;

                case R.id.image_tab_profile:
                    currentTabPosition = 3;
                    setTab();
                    break;

                case R.id.img_close:
                    mBottomSheetHowTo.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    mViewBottomSheetBackground.setVisibility(View.GONE);
                    break;

                case R.id.iv_close_pool_price:
                    mBottomSheetPoolPrice.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    mViewBottomSheetBackground.setVisibility(View.GONE);
                    break;

                case R.id.img_close_badges:
                    mBottomSheetProfileBadges.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    mViewBottomSheetBackground.setVisibility(View.GONE);
                    break;
            }
        }
    };

    /**
     * Call back listener (How To)
     */
    BottomSheetBehavior.BottomSheetCallback howToBottomSheetCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            switch (newState) {
                case BottomSheetBehavior.STATE_HIDDEN:
                    break;

                case BottomSheetBehavior.STATE_EXPANDED:
                    mViewBottomSheetBackground.setVisibility(View.VISIBLE);
                    break;

                case BottomSheetBehavior.STATE_COLLAPSED:
                    mViewBottomSheetBackground.setVisibility(View.GONE);
                    break;

                case BottomSheetBehavior.STATE_DRAGGING:
                    break;

                case BottomSheetBehavior.STATE_SETTLING:
                    break;

            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    /**
     * Call back listener (Pool Price)
     */
    BottomSheetBehavior.BottomSheetCallback poolPriceBottomSheetCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            switch (newState) {
                case BottomSheetBehavior.STATE_HIDDEN:
                    break;

                case BottomSheetBehavior.STATE_EXPANDED:
                    mViewBottomSheetBackground.setVisibility(View.VISIBLE);
                    break;

                case BottomSheetBehavior.STATE_COLLAPSED:
                    mViewBottomSheetBackground.setVisibility(View.GONE);
                    break;

                case BottomSheetBehavior.STATE_DRAGGING:

                case BottomSheetBehavior.STATE_SETTLING:
                    break;

                case BottomSheetBehavior.STATE_HALF_EXPANDED:
                    break;
            }
        }

        @Override
        public void onSlide(@NonNull View view, float v) {

        }
    };

    /**
     * Call back listener (Profile Badges)
     */
    BottomSheetBehavior.BottomSheetCallback profileBadgesBottomSheetCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            switch (newState) {
                case BottomSheetBehavior.STATE_HIDDEN:
                    break;

                case BottomSheetBehavior.STATE_COLLAPSED:
                    mViewBottomSheetBackground.setVisibility(View.GONE);
                    break;

                case BottomSheetBehavior.STATE_EXPANDED:
                    mViewBottomSheetBackground.setVisibility(View.VISIBLE);
                    mViewBottomSheetBackground.bringToFront();
                    break;

                case BottomSheetBehavior.STATE_DRAGGING:
                    break;
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    /**
     * Sets the tab
     */
    private void setTab() {
        switch (currentTabPosition) {
            case 1:
                setHomeTab();
                break;

            case 2:
                setMyContestsTab();
                break;

            case 3:
                setProfileTab();
                break;

            case 4:
                setVideosTab();
                break;

            case 5:
                setMoreTab();
                break;
        }
    }

    /**
     * Sets the Home tab data
     */
    @SuppressLint("UseCompatLoadingForDrawables")
    private void setHomeTab() {
        try {
            setDefaultTabs();
            setTabColor(getResources().getDrawable(R.drawable.ic_selected_home), mImageHome);
            setTextColor(mTextHome);
            fragment = new HomeFragment();
            mTabName = getResources().getString(R.string.menu_home);
            changeFragment(fragment, mTabName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the My Contests tab data
     */
    @SuppressLint("UseCompatLoadingForDrawables")
    private void setMyContestsTab() {
        try {
            setDefaultTabs();
            setTabColor(getResources().getDrawable(R.drawable.ic_noun_trophy), mImageMyContests);
            setTextColor(mTextMyContests);
            fragment = new MyContestsFragment();
            mTabName = getResources().getString(R.string.menu_my_contests);
            changeFragment(fragment, mTabName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the Profile tab data
     */
    private void setProfileTab() {
        try {
            setDefaultTabs();
            fragment = new ProfileFragment();
            mTabName = getResources().getString(R.string.menu_profile);
            changeFragment(fragment, mTabName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the Videos tab data
     */
    @SuppressLint("UseCompatLoadingForDrawables")
    private void setVideosTab() {
        try {
            setDefaultTabs();
            setTabColor(getResources().getDrawable(R.drawable.ic_video_selected), mImageVideos);
            setTextColor(mTextVideos);
            fragment = new VideosFragment();
            mTabName = getResources().getString(R.string.menu_videos);
            changeFragment(fragment, mTabName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the more tab data
     */
    @SuppressLint("UseCompatLoadingForDrawables")
    private void setMoreTab() {
        try {
            setDefaultTabs();
            setTabColor(getResources().getDrawable(R.drawable.ic_selected_more), mImageMore);
            setTextColor(mTextMore);
            fragment = new MoreFragment();
            mTabName = getResources().getString(R.string.menu_more);
            changeFragment(fragment, mTabName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the tab image and text color as per the layout clicked
     */
    private void setTabColor(Drawable image, ImageView mImageView) {
        mImageView.setBackground(image);
    }

    /**
     * Sets the tab image and text color as per the layout clicked
     */
    private void setTextColor(TextView mTextView) {
        mTextView.setTextColor(getResources().getColor(R.color.bg_text_color));
    }

    /**
     * Change the fragment
     *
     * @param fragment - Fragment
     * @param title    - Title
     */
    public void changeFragment(Fragment fragment, String title) {
        FragmentManager mFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();

        Fragment currentFragment = mFragmentManager.getPrimaryNavigationFragment();
        if (currentFragment != null) {
            fragmentTransaction.hide(currentFragment);
        }

        Fragment fragmentTemp = mFragmentManager.findFragmentByTag(title);
        if (fragmentTemp == null) {
            fragmentTemp = fragment;
            fragmentTransaction.add(R.id.frame_container, fragmentTemp, title);
        } else {
            fragmentTransaction.show(fragmentTemp);
        }

        fragmentTransaction.setPrimaryNavigationFragment(fragmentTemp);
        fragmentTransaction.setReorderingAllowed(true);
        fragmentTransaction.commitNowAllowingStateLoss();
    }

    /**
     * Change the fragment
     *
     * @param fragment - Fragment
     * @param title    - Title
     */
    /*private void changeFragment(Fragment fragment, String title) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                .beginTransaction().replace(R.id.frame_container, fragment, title);
        fragmentTransaction.commit();
    }*/

    /**
     * Sets the default tab
     */
    @SuppressLint("UseCompatLoadingForDrawables")
    private void setDefaultTabs() {
        try {
            // Default Image Background Image
            mImageHome.setBackground(getResources().getDrawable(R.drawable.ic_noun_home));
            mImageMyContests.setBackground(getResources().getDrawable(R.drawable.ic_contest));
            mImageVideos.setBackground(getResources().getDrawable(R.drawable.ic_video));
            mImageMore.setBackground(getResources().getDrawable(R.drawable.ic_more));

            // Default Text Color
            mTextHome.setTextColor(getResources().getColor(R.color.default_text_color));
            mTextMyContests.setTextColor(getResources().getColor(R.color.default_text_color));
            mTextVideos.setTextColor(getResources().getColor(R.color.default_text_color));
            mTextMore.setTextColor(getResources().getColor(R.color.default_text_color));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (mViewBottomSheetBackground.getVisibility() == View.VISIBLE) {
            mBottomSheetHowTo.setState(BottomSheetBehavior.STATE_COLLAPSED);
            mBottomSheetPoolPrice.setState(BottomSheetBehavior.STATE_COLLAPSED);
            mBottomSheetProfileBadges.setState(BottomSheetBehavior.STATE_COLLAPSED);
            mViewBottomSheetBackground.setVisibility(View.GONE);
        } else {
            if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                super.onBackPressed();
                return;
            } else {
                Common.setCustomToast(getApplicationContext(), getString(R.string.text_press_back_again_to_exit));
            }
            mBackPressed = System.currentTimeMillis();
        }
    }

    @Override
    public void onMyContestWinnerClick(MyContestModel.Data.ContestsData contestsData) {
        mTextBottomSheetPoolPrice.setText(contestsData.WinningAmount);
        if (contestsData.WinningAmount.equalsIgnoreCase("") || contestsData.WinningAmount.equalsIgnoreCase("0")) {
            mTextBottomSheetPoolPriceHeader.setVisibility(View.GONE);
            mTextBottomSheetPoolPriceWinningRupee.setVisibility(View.GONE);
        } else {
            mTextBottomSheetPoolPriceHeader.setVisibility(View.VISIBLE);
            mTextBottomSheetPoolPriceWinningRupee.setVisibility(View.VISIBLE);
        }
        getWinnerPool(contestsData);
    }

    /**
     * This should get Winter Pool data from API
     *
     * @param contestsData - Contests Data Model
     */
    private void getWinnerPool(MyContestModel.Data.ContestsData contestsData) {
        final ProgressDialog progressDialog = new ProgressDialog(BottomNavigationActivity.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        if (contestsData.Type.equalsIgnoreCase("1")) {
            Call<WinnerContestModel> contestDetailsModelCall = APIClient.getClient().create(APIInterface.class)
                    .getContestWinner(Constants.GETSINGLECONTESTWINNER, contestsData.ContestID);
            contestDetailsModelCall.enqueue(new Callback<WinnerContestModel>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(@NonNull Call<WinnerContestModel> call, @NonNull Response<WinnerContestModel> response) {
                    progressDialog.dismiss();
                    WinnerContestModel winnerContestModel = response.body();
                    assert winnerContestModel != null;
                    if (winnerContestModel.status.equalsIgnoreCase("success")) {
                        mAdapterWinnerPool = new WinnerPoolAdapter(BottomNavigationActivity.this, winnerContestModel.data.prizePoolsData);
                        mRecyclerView.setAdapter(mAdapterWinnerPool);
                        mBottomSheetPoolPrice.setState(BottomSheetBehavior.STATE_EXPANDED);
                        mViewBottomSheetBackground.setVisibility(View.VISIBLE);
                    } else {
                        Constants.SnakeMessageYellow(mCoordinatorMain, winnerContestModel.message);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<WinnerContestModel> call, @NonNull Throwable t) {
                    progressDialog.dismiss();
                    t.printStackTrace();
                }
            });
        } else {
            Call<WinnerContestModel> contestDetailsModelCall = APIClient.getClient().create(APIInterface.class)
                    .getTournamentWinners(Constants.GET_TOURNAMENT_WINNING_POOL, contestsData.TournamentID);
            contestDetailsModelCall.enqueue(new Callback<WinnerContestModel>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(@NonNull Call<WinnerContestModel> call, @NonNull Response<WinnerContestModel> response) {
                    progressDialog.dismiss();
                    WinnerContestModel winnerContestModel = response.body();
                    assert winnerContestModel != null;
                    if (winnerContestModel.status.equalsIgnoreCase("success")) {
                        mAdapterWinnerPool = new WinnerPoolAdapter(BottomNavigationActivity.this, winnerContestModel.data.prizePoolsData);
                        mRecyclerView.setAdapter(mAdapterWinnerPool);
                        mBottomSheetPoolPrice.setState(BottomSheetBehavior.STATE_EXPANDED);
                        mViewBottomSheetBackground.setVisibility(View.VISIBLE);
                    } else {
                        Constants.SnakeMessageYellow(mCoordinatorMain, winnerContestModel.message);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<WinnerContestModel> call, @NonNull Throwable t) {
                    progressDialog.dismiss();
                    t.printStackTrace();
                }
            });
        }
    }

    @Override
    public void onWinnersClick(ContestTypesModel.Data.TypesData.ContestsData contestsData) {
    }

    @Override
    public void onWinnersClick(ContestTypeModelNew.Data.ContestsData contestsData) {
    }

    @Override
    public void onTournamentWinnerClick(TournamentTypeModel.Data.TypesData.TournamentsData
                                                tournamentsData) {
    }

    @Override
    public void onTournamentContestWinnersClickListener
            (TournamentDetailModel.TournamentData.ContestsData contestsData) {
    }

    @Override
    public void OnRulesClickListener(TournamentTypeModel.Data.TypesData.TournamentsData tournamentsData) {
        Common.insertLog("Bottom Activity OnRulesClickListener");
    }

    @Override
    public void OnMyContestRulesClickListener(MyContestModel.Data.ContestsData contestsData) {
        Common.insertLog("Bottom Activity OnMyContestRulesClickListener");
        final ProgressDialog progressDialog = new ProgressDialog(BottomNavigationActivity.this);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<RulesModel> rulesModelCall = APIClient.getClient().create(APIInterface.class).getRules(Constants.GET_RULES,
                Pref.getValue(BottomNavigationActivity.this, Constants.UserID, "", Constants.FILENAME), contestsData.TournamentID);

        rulesModelCall.enqueue(new Callback<RulesModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<RulesModel> call, @NonNull Response<RulesModel> response) {
                progressDialog.dismiss();
                RulesModel rulesModel = response.body();
                assert rulesModel != null;

                if (rulesModel.status.equalsIgnoreCase("success")) {
                    mTextBottomSheetHowToTitle.setText("Rules");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        mTextBottomSheetHowToMessage.setText(Html.fromHtml(rulesModel.Data.Rules, Html.FROM_HTML_SEPARATOR_LINE_BREAK_DIV));
                    } else {
                        mTextBottomSheetHowToMessage.setText(Html.fromHtml(rulesModel.Data.Rules));
                    }
                    mBottomSheetHowTo.setState(BottomSheetBehavior.STATE_EXPANDED);
                    mViewBottomSheetBackground.setVisibility(View.VISIBLE);
                } else {
                    Constants.SnakeMessageYellow(mCoordinatorMain, rulesModel.message);
                }
            }

            @Override
            public void onFailure(@NonNull Call<RulesModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    @Override
    public void OnGameInfoClickListener(String mValue) {
        mBottomSheetHowTo.setState(BottomSheetBehavior.STATE_EXPANDED);
        mViewBottomSheetBackground.setVisibility(View.VISIBLE);

        if (mValue.equalsIgnoreCase(Constants.PROFILE_BOTTOM_SHEET_P_MOBILE_INFO)) {
            mTextBottomSheetHowToMessage.setText(getResources().getString(R.string.pubg_info));
        } else if (mValue.equalsIgnoreCase(Constants.PROFILE_BOTTOM_SHEET_CLASH_INFO)) {
            mTextBottomSheetHowToMessage.setText(getResources().getString(R.string.cr_info));
        } else if (mValue.equalsIgnoreCase(Constants.PROFILE_BOTTOM_SHEET_POINTS_INFO)) {
            mTextBottomSheetHowToMessage.setText(getResources().getString(R.string.points_info));
        } else if (mValue.equalsIgnoreCase(Constants.PROFILE_BOTTOM_SHEET_TEAM_INFO)) {
            mTextBottomSheetHowToTitle.setText(getResources().getString(R.string.team_name));
            mTextBottomSheetHowToMessage.setText(getResources().getString(R.string.team_info));
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void OnOtherGameInfoClickListener(GameUsernameModel gameUsernameModel) {
        mBottomSheetHowTo.setState(BottomSheetBehavior.STATE_EXPANDED);
        mViewBottomSheetBackground.setVisibility(View.VISIBLE);
        mTextBottomSheetHowToTitle.setText(gameUsernameModel.getGameName() + " Username");
        mTextBottomSheetHowToMessage.setText("This is the username that you use on " + gameUsernameModel.getGameName()
                + "\n\nNote: Your " + gameUsernameModel.getGameName() + " username cannot be changed once submitted. In case you have entered a wrong username, please contact support@gamerji.com.");
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void OnBadgeClickListener(GamerProfileModel.Data.LevelsData.LevelData badgesList) {
        mTextBottomSheetProfileBadgeMessage.setText(badgesList.EndingPoint);
        mTextBottomSheetProfileBadgePoints.setText("Points  " + badgesList.StartingPoint + "  -  " +
                badgesList.EndingPoint);
        Glide.with(BottomNavigationActivity.this).load(badgesList.FeaturedIcon).into(mImageBottomSheetProfileBadge);
        mBottomSheetProfileBadges.setState(BottomSheetBehavior.STATE_EXPANDED);
    }
}
