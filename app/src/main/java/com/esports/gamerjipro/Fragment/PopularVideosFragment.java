package com.esports.gamerjipro.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.esports.gamerjipro.Adapter.PopularVideosAdapter;
import com.esports.gamerjipro.Model.PopularVideosModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.RestApis.WebFields;
import com.esports.gamerjipro.Utils.Common;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.SessionManager;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PopularVideosFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private RelativeLayout mRelativeMain;
    private LinearLayoutManager mLayoutManager;
    private SwipeRefreshLayout mSwipeRefreshView;
    private RecyclerView mRecyclerView;
    private ArrayList<PopularVideosModel.Data.YTChannelsData> mArrPopularVideos;
    private PopularVideosAdapter mAdapterPopularVideos;
    private SessionManager mSessionManager;

    private int pageIndex = 15, page = 1, pastVisibleItems, visibleItemCount, totalItemCount;
    private boolean isLastPage = false, isLoading;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_popular_videos, container, false);
        mActivity = getActivity();
        mSessionManager = new SessionManager();
        mArrPopularVideos = new ArrayList<>();
        mLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);

        getIds();
        setRegListeners();
        callToGetPopularVideosAPI(page);
        return mView;
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Relative Layout
            mRelativeMain = mView.findViewById(R.id.relative_popular_videos_main_view);

            // Swipe Refresh View
            mSwipeRefreshView = mView.findViewById(R.id.swipe_refresh_view_popular_videos);

            // Recycler View
            mRecyclerView = mView.findViewById(R.id.recycler_popular_videos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Swipe Refresh Click Listener
            mSwipeRefreshView.setOnRefreshListener(refreshListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set On Refresh Listeners
     */
    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            page = 1;
            isLastPage = false;
            if (mArrPopularVideos != null)
                mArrPopularVideos.clear();
            if (mAdapterPopularVideos != null)
                mAdapterPopularVideos.notifyDataSetChanged();
            callToGetPopularVideosAPI(page);
        }
    };

    /**
     * This method should get the Popular Videos API
     */
    private void callToGetPopularVideosAPI(int page) {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(mActivity);
            progressDialog.setMessage("Please wait...."); // Setting Message
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
            progressDialog.show(); // Display Progress Dialog
            progressDialog.setCancelable(false);

            String mUserId = mSessionManager.getUserId(mActivity);

            Call<PopularVideosModel> callRepos = APIClient.getClient().create(APIInterface.class).getPopularVideos(WebFields.GET_POPULAR_VIDEOS.MODE, mUserId, Constants.VIDEOS_POPULAR, String.valueOf(page), String.valueOf(pageIndex));
            callRepos.enqueue(new Callback<PopularVideosModel>() {
                @Override
                public void onResponse(@NonNull Call<PopularVideosModel> call, @NonNull Response<PopularVideosModel> response) {
                    isLoading = false;
                    mSwipeRefreshView.setRefreshing(false);
                    progressDialog.dismiss();

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        assert response.body() != null;
                        if (response.body().getStatus().equalsIgnoreCase(Constants.API_SUCCESS)) {

                            ArrayList<PopularVideosModel.Data.YTChannelsData> gameTypesModels = (ArrayList<PopularVideosModel.Data.YTChannelsData>)
                                    response.body().getData().getYTChannelsData();

                            if (response.body().getData().getYTChannelsCount() > 0) {
                                if (mArrPopularVideos != null && mArrPopularVideos.size() > 0 &&
                                        mAdapterPopularVideos != null) {
                                    mArrPopularVideos.addAll(gameTypesModels);
                                    mAdapterPopularVideos.notifyDataSetChanged();
                                } else {
                                    mArrPopularVideos = gameTypesModels;
                                    setAdapterData();
                                    setLoadMoreClickListener();
                                }
                            } else {
                                isLastPage = true;
                            }
                        } else {
                            setAdapterData();
                            Constants.SnakeMessageYellow(mRelativeMain, response.body().getMessage());
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PopularVideosModel> call, @NonNull Throwable t) {
                    mSwipeRefreshView.setRefreshing(false);
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());

            mAdapterPopularVideos = new PopularVideosAdapter(mActivity, mArrPopularVideos);
            mRecyclerView.setAdapter(mAdapterPopularVideos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * On load more click listener
     */
    private void setLoadMoreClickListener() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (!isLastPage && !isLoading) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                            isLoading = true;
                            Common.insertLog("Last Item Wow !");
                            callToGetPopularVideosAPI(++page);
                        }
                    }
                }
            }
        });
    }
}
