package com.esports.gamerjipro.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.esports.gamerjipro.Adapter.GameTypeListAdapter;
import com.esports.gamerjipro.Adapter.MyContestAdapter;
import com.esports.gamerjipro.Model.GameTypesModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.Pref;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrganizerGameTypeFragment extends Fragment {

    private View view;

    private String title;//String for tab title
    private String gameid, game_name;//String for tab title

    private static RecyclerView recyclerView;
    private MyContestAdapter myContestAdapter;
    private APIInterface apiInterface;
    private int count = 1;
    private RelativeLayout lyt_empty;
    private SwipeRefreshLayout swipe_refresh;
    boolean joined = true;
    private GameTypeListAdapter gamesListAdapter;
    private ArrayList<GameTypesModel.Data.TypesData> mArrGameTypes;

    public OrganizerGameTypeFragment(String title, String gameid, String game_name) {
        this.title = title;//Setting tab title
        this.gameid = gameid;
        this.game_name = game_name;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tab_layout, container, false);
        mArrGameTypes = new ArrayList<>();
        recyclerView = view.findViewById(R.id.recyclerView);
        LinearLayout lyt_tabs = view.findViewById(R.id.lyt_tabs);
        lyt_tabs.setVisibility(View.GONE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        lyt_empty = view.findViewById(R.id.lyt_empty);
        swipe_refresh = view.findViewById(R.id.swipe_refresh);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        // getGameTypes();
        return view;
    }

    private void getGameTypes() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);

        Call<GameTypesModel> gameTypesModelCall = apiInterface.getGameTypes("GetAllTypes",
                Pref.getValue(getActivity(), Constants.UserID, "", Constants.FILENAME), gameid);

        gameTypesModelCall.enqueue(new Callback<GameTypesModel>() {
            @Override
            public void onResponse(@NonNull Call<GameTypesModel> call, @NonNull Response<GameTypesModel> response) {
                progressDialog.dismiss();
                assert response.body() != null;
                if (response.body().getStatus().equalsIgnoreCase(Constants.API_SUCCESS)) {
                    ArrayList<GameTypesModel.Data.TypesData> gameTypesModels = (ArrayList<GameTypesModel.Data.TypesData>)
                            response.body().getData().getTypesData();

                    if (mArrGameTypes != null && mArrGameTypes.size() > 0 &&
                            mArrGameTypes != null) {
                        mArrGameTypes.addAll(gameTypesModels);
                        gamesListAdapter.notifyDataSetChanged();
                    } else {
                        mArrGameTypes = gameTypesModels;
                        setAdapterData();
                    }
                } else {
                    Constants.SnakeMessageYellow(view, response.body().getMessage());
                }

               /*GameTypesModel gameTypesModel = response.body();
                assert gameTypesModel != null;
                if (gameTypesModel.status.equalsIgnoreCase("success")) {
                    if (gameTypesModel.DataClass.typesDataArrayList.size() < 1) {
                        recyclerView.setVisibility(View.GONE);
                        lyt_empty.setVisibility(View.VISIBLE);
                    } else {
                        gamesListAdapter = new GameTypeListAdapter(getActivity(), gameTypesModel.DataClass.typesDataArrayList, game_name);
                        recyclerView.setAdapter(gamesListAdapter);
                    }

                } else {
                    Constants.SnakeMessageYellow(view, gameTypesModel.message);
                }*/
            }

            @Override
            public void onFailure(@NonNull Call<GameTypesModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    /**
     * Sets up the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            gamesListAdapter = new GameTypeListAdapter(getActivity(), mArrGameTypes, game_name);
            recyclerView.setAdapter(gamesListAdapter);

            setViewVisibility();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the view visibility as per the array size
     */
    private void setViewVisibility() {
        if (mArrGameTypes.size() != 0) {
            recyclerView.setVisibility(View.VISIBLE);
            lyt_empty.setVisibility(View.GONE);
        } else {
            recyclerView.setVisibility(View.GONE);
            lyt_empty.setVisibility(View.VISIBLE);
        }
    }
}
