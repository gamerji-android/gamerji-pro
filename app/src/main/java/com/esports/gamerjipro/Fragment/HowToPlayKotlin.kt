package com.esports.gamerjipro.Fragment

import android.app.ProgressDialog
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.esports.gamerjipro.R
import com.esports.gamerjipro.RestApis.APIClient
import com.esports.gamerjipro.RestApis.OnApiResponseListener
import com.esports.gamerjipro.Utils.Constants
import com.esports.gamerjipro.Utils.jsonString
import com.google.android.material.tabs.TabLayout
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import kotlinx.android.synthetic.main.activity_how_to_play.*

class HowToPlayKotlin : Fragment()
{
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        return inflater.inflate(R.layout.activity_how_to_play, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?)
    {
        super.onActivityCreated(savedInstanceState)
        getHowToPlay()
    }

    var pointLeagueData: ArrayList<HowToPlay> = ArrayList()
    private fun getHowToPlay()
    {
        val progressDialog = ProgressDialog(activity)
        progressDialog.setMessage("Please wait....") // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER) // Progress Dialog Style Spinner
        progressDialog.show() // Display Progress Dialog
        progressDialog.setCancelable(false)
        APIClient.getJsonFile(Constants.JSON_HOW_TO_PLAY, object : OnApiResponseListener<JsonElement>
        {
            override fun onResponseComplete(clsGson: JsonElement?, requestCode: Int)
            {

                for (type in clsGson!!.asJsonArray)
                {
                    pointLeagueData.add(HowToPlay(type.jsonString("type"), type.asJsonObject.getAsJsonArray("data")))
                    tabs.addTab(tabs.newTab().setText(type.jsonString("type")))
                }
                if (clsGson.asJsonArray.size() > 1)
                {
                    tabs.visibility = View.VISIBLE
                }
                if (pointLeagueData.size > 0)
                {
                    val pagerAdapter = ViewPagerAdapter(childFragmentManager, pointLeagueData.size)
                    frameLayout.adapter = pagerAdapter
                    tabs.setupWithViewPager(frameLayout)
                    tabs.tabTextColors = ColorStateList.valueOf(Color.WHITE)
                    tabs.setSelectedTabIndicatorColor(resources.getColor(R.color.orange_color))
                    val wantedTabIndex = 0
                    var font: Typeface? = null
                    font = activity?.let { ResourcesCompat.getFont(it, R.font.poppins_semibold) }
                    val tv = ((tabs.getChildAt(0) as LinearLayout).getChildAt(wantedTabIndex) as LinearLayout).getChildAt(1) as TextView
                    tv.typeface = font
                    tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                        override fun onTabSelected(tab: TabLayout.Tab) {
                            val vg = tabs.getChildAt(0) as ViewGroup
                            val vgTab = vg.getChildAt(tab.position) as ViewGroup
                            val tabChildsCount = vgTab.childCount
                            for (i in 0 until tabChildsCount) {
                                val tabViewChild = vgTab.getChildAt(i)
                                if (tabViewChild is TextView) {
                                    var font: Typeface? = null
                                    font = activity?.let { ResourcesCompat.getFont(it, R.font.poppins_semibold) }
                                    tabViewChild.typeface = font
                                }
                            }
                        }

                        override fun onTabUnselected(tab: TabLayout.Tab) {
                            val vg = tabs.getChildAt(0) as ViewGroup
                            val vgTab = vg.getChildAt(tab.position) as ViewGroup
                            val tabChildsCount = vgTab.childCount
                            for (i in 0 until tabChildsCount) {
                                val tabViewChild = vgTab.getChildAt(i)
                                if (tabViewChild is TextView) {
                                    var font: Typeface? = null
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                        font = resources.getFont(R.font.poppins)
                                        tabViewChild.typeface = font
                                    }
                                }
                            }
                        }

                        override fun onTabReselected(tab: TabLayout.Tab) {

                        }
                    })

                }
                progressDialog.dismiss()
            }

            override fun onResponseError(errorMessage: String?, requestCode: Int, responseCode: Int)
            {
                progressDialog.dismiss()
            }
        })
    }

    class HowToPlay(val name: String, val data: JsonArray)

    fun getData(pos: Int): JsonArray
    {
        return pointLeagueData[pos].data
    }

    inner class ViewPagerAdapter(fm: FragmentManager, private var mNumOfTabs: Int) : FragmentStatePagerAdapter(fm)
    {
        override fun getItem(position: Int): Fragment
        {
            return HowToPlayDynamicSubFragment.newInstance(position)
        }

        override fun getCount(): Int
        {
            return mNumOfTabs
        }

        override fun getPageTitle(position: Int): CharSequence?
        {
            return pointLeagueData[position].name
        }
    }
}