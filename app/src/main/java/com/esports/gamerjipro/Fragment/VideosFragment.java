package com.esports.gamerjipro.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.esports.gamerjipro.Adapter.ViewPagerAdapter;
import com.esports.gamerjipro.R;
import com.google.android.material.tabs.TabLayout;

public class VideosFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_videos, container, false);
        mActivity = getActivity();

        getIds();
        setRegListeners();
        setViewPager();
        return mView;
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Tab Layout
            mTabLayout = mView.findViewById(R.id.tab_layout_videos);

            // View Pager
            mViewPager = mView.findViewById(R.id.view_pager_videos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    @SuppressLint("SetTextI18n")
    private void setRegListeners() {
        try {
            // ToDo: Tab Selected Listener
            mTabLayout.setOnTabSelectedListener(onTabSelectedListener());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Tab Selected Listener
     */
    private TabLayout.OnTabSelectedListener onTabSelectedListener() {

        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                setTabSelectedFontStyle(tab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                setTabUnselectedFontStyle(tab);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        };
    }

    /**
     * This should be called when Tab is getting selected
     */
    private void setTabSelectedFontStyle(TabLayout.Tab tab) {
        try {
            ViewGroup vg = (ViewGroup) mTabLayout.getChildAt(0);
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(tab.getPosition());
            int tabChildsCount = vgTab.getChildCount();

            // ToDo: Set Font Style & Color
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    Typeface font = null;
                    font = ResourcesCompat.getFont(mActivity, R.font.poppins_semibold);
                    ((TextView) tabViewChild).setTypeface(font);
                    ((TextView) tabViewChild).setTextColor(Color.WHITE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This should be called when Tab is getting unselected
     */
    private void setTabUnselectedFontStyle(TabLayout.Tab tab) {
        try {
            ViewGroup vg = (ViewGroup) mTabLayout.getChildAt(0);
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(tab.getPosition());
            int tabChildsCount = vgTab.getChildCount();

            // ToDo: Set Font Style & Color
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    Typeface font = null;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        font = getResources().getFont(R.font.poppins);
                        ((TextView) tabViewChild).setTypeface(font);
                    }
                    ((TextView) tabViewChild).setTextColor(Color.WHITE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This should set the view pager tab
     */
    private void setViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFrag(new PopularVideosFragment(), mActivity.getResources().getString(R.string.tab_popular));
        adapter.addFrag(new HostStreamVideosFragment(), mActivity.getResources().getString(R.string.tab_host_streams));
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));

        mTabLayout.setupWithViewPager(mViewPager);
        mTabLayout.setTabTextColors(ColorStateList.valueOf(Color.WHITE));
        mTabLayout.setSelectedTabIndicatorHeight(10);
        mTabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.orange_color));

        // ToDo: Set Font Style & Color
        for (int i = 0; i < mTabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = mTabLayout.getTabAt(i);
            if (tab != null) {
                TextView tabTextView = new TextView(mActivity);
                if (i == 0) {
                    Typeface font = null;
                    font = ResourcesCompat.getFont(mActivity, R.font.poppins_semibold);
                    tabTextView.setTypeface(font);
                }
            }
        }
    }
}
