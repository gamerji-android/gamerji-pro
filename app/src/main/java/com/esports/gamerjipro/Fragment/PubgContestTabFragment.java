package com.esports.gamerjipro.Fragment;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.esports.gamerjipro.Adapter.MyContestAdapter;
import com.esports.gamerjipro.Model.MyContestModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.Pref;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PubgContestTabFragment extends Fragment
{
    private View view;

    private String title;//String for tab title

    private static RecyclerView recyclerView;
    private MyContestAdapter myContestAdapter ;
    private APIInterface apiInterface;
    private int count =1;
    private TextView txt_completed,txt_joined;
    private RelativeLayout lyt_empty;
    SwipeRefreshLayout swipe_refresh;
    boolean joined=true;

    public PubgContestTabFragment()
    {
    }

    public PubgContestTabFragment(String title)
    {
        this.title = title;//Setting tab title
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_tab_layout, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        txt_completed = view.findViewById(R.id.txt_completed);
        lyt_empty = view.findViewById(R.id.lyt_empty);
        swipe_refresh=view.findViewById(R.id.swipe_refresh);
        txt_joined = view.findViewById(R.id.txt_joined);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        getPubgJoinedontest();
        txt_completed.setOnClickListener(v ->
        {
            joined=false;
            getPubgCOmpletedContest();
            final int sdk = android.os.Build.VERSION.SDK_INT;
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN)
            {
                txt_completed.setBackgroundDrawable(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.bottom_rounded) );
                txt_completed.setTextColor(Color.WHITE);
                txt_joined.setTextColor(Color.BLACK);
                txt_joined.setBackgroundColor(Color.TRANSPARENT);
            }
            else
            {
                txt_completed.setBackgroundDrawable(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.bottom_rounded) );
                txt_completed.setTextColor(Color.WHITE);
                txt_joined.setTextColor(Color.BLACK);
                txt_joined.setBackgroundColor(Color.TRANSPARENT);
            }
        });
        txt_joined.setOnClickListener(v ->
        {
            joined=true;
            getPubgJoinedontest();
            final int sdk = android.os.Build.VERSION.SDK_INT;
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN)
            {
                txt_joined.setBackgroundDrawable(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.bottom_rounded) );
                txt_joined.setTextColor(Color.WHITE);
                txt_completed.setTextColor(Color.BLACK);
                txt_completed.setBackgroundColor(Color.TRANSPARENT);
            }
            else
            {
                txt_joined.setBackgroundDrawable(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.bottom_rounded) );
                txt_joined.setTextColor(Color.WHITE);
                txt_completed.setTextColor(Color.BLACK);
                txt_completed.setBackgroundColor(Color.TRANSPARENT);
            }
        });
        swipe_refresh.setOnRefreshListener(() ->
        {
            if (joined)
            getPubgJoinedontest();
            else
             getPubgCOmpletedContest();

        });
        return view;
    }

    private void getPubgCOmpletedContest()
    {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<MyContestModel> dashboardModelCall = apiInterface.getMyContest(Constants.GET_ALL_JOINED_CONTESTS, Pref.getValue(Objects.requireNonNull(getActivity()), Constants.UserID,"", Constants.FILENAME), Constants.PUBG_GAME_ID, Constants.COMPLETED_CONTEST,count);

        dashboardModelCall.enqueue(new Callback<MyContestModel>()
        {
            @Override
            public void onResponse(@NonNull Call<MyContestModel> call, @NonNull Response<MyContestModel> response)
            {
                progressDialog.dismiss();
                swipe_refresh.setRefreshing(false);
                MyContestModel myContestModel = response.body();
                assert myContestModel != null;
                if (myContestModel.status.equalsIgnoreCase("success"))
                {
                    if (myContestModel.DataClass.contestsDataArrayList.size()<1)
                    {
                        recyclerView.setVisibility(View.GONE);
                        lyt_empty.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                       /* myContestAdapter= new MyContestAdapter(getActivity(),myContestModel.DataClass.contestsDataArrayList, (WinnerClickListener) getActivity());
                        recyclerView.setAdapter(myContestAdapter);*/
                        lyt_empty.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }

                }
                else
                {
                    Toast.makeText(getActivity(), myContestModel.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<MyContestModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                swipe_refresh.setRefreshing(false);
                t.printStackTrace();
            }

        });
    }

    private void getPubgJoinedontest()
    {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<MyContestModel> dashboardModelCall = apiInterface.getMyContest(Constants.GET_ALL_JOINED_CONTESTS, Pref.getValue(Objects.requireNonNull(getActivity()), Constants.UserID,"", Constants.FILENAME), Constants.PUBG_GAME_ID, Constants.JOINED_CONTEST,count);

        dashboardModelCall.enqueue(new Callback<MyContestModel>()
        {
            @Override
            public void onResponse(@NonNull Call<MyContestModel> call, @NonNull Response<MyContestModel> response)
            {
                progressDialog.dismiss();
                swipe_refresh.setRefreshing(false);
                MyContestModel myContestModel = response.body();
                assert myContestModel != null;
                if (myContestModel.status.equalsIgnoreCase("success"))
                {
                    if (myContestModel.DataClass.contestsDataArrayList.size()<1)
                    {
                        recyclerView.setVisibility(View.GONE);
                        lyt_empty.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        /*MyContestAdapter myContestAdapter = new MyContestAdapter(getActivity(),myContestModel.DataClass.contestsDataArrayList, (WinnerClickListener) getActivity());
                        recyclerView.setAdapter(myContestAdapter);*/
                        lyt_empty.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                }
                else
                {
                    Toast.makeText(getActivity(), myContestModel.message, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<MyContestModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                swipe_refresh.setRefreshing(false);
                t.printStackTrace();
            }

        });
    }
}
