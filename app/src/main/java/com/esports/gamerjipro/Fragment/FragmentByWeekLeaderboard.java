package com.esports.gamerjipro.Fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.esports.gamerjipro.Adapter.LeaderboardAdapter;
import com.esports.gamerjipro.Model.LeaderboardModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.Utils.Common;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.Pref;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentByWeekLeaderboard extends Fragment
{

    private View view;

    private String title;//String for tab title

    private APIInterface apiInterface;
    private LeaderboardAdapter leaderboardAdapter ;
    private RecyclerView rv_search;
    private int page=1;
    private LinearLayoutManager mLinearLayoutManager;
    private boolean isLoading,isLastPage=false;
    private ArrayList<LeaderboardModel.Data.UserLevelsData> userLevelsData= new ArrayList<>();
    private String rank="0";
    private String points="0";
     private ImageView img_rank2,img_rank3,img_rank1;
     private TextView txt_team_name2,txt_team_mobile2,txt_team_points2,txt_team_name1,txt_team_mobile1,txt_team_points1,txt_team_name3,txt_team_mobile3,txt_team_points3;

    public FragmentByWeekLeaderboard(String title, RecyclerView rv_search)
    {
        this.title = title;//Setting tab title
        this.rv_search=rv_search;
    }

    @Nullable
    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_leaderboard, container, false);
        txt_team_name1=view.findViewById(R.id.txt_team_name1);
        txt_team_mobile1=view.findViewById(R.id.txt_team_mobile1);
        txt_team_points1=view.findViewById(R.id.txt_team_points1);
        txt_team_name2=view.findViewById(R.id.txt_team_name2);
        txt_team_mobile2=view.findViewById(R.id.txt_team_mobile2);
        txt_team_points2=view.findViewById(R.id.txt_team_points2);
        txt_team_name3=view.findViewById(R.id.txt_team_name3);
        txt_team_mobile3=view.findViewById(R.id.txt_team_mobile3);
        txt_team_points3=view.findViewById(R.id.txt_team_points3);

        img_rank2=view.findViewById(R.id.img_rank2);
        img_rank3=view.findViewById(R.id.img_rank3);
        img_rank1=view.findViewById(R.id.img_rank1);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        Initialization();
        return view;
    }
    private int pastVisiblesItems, visibleItemCount, totalItemCount;

    @SuppressLint("WrongConstant")
    void Initialization()
    {
        RecyclerView rv_leader = view.findViewById(R.id.rv_leader);
        rv_leader.setItemAnimator(new DefaultItemAnimator());
        mLinearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rv_leader.setLayoutManager(mLinearLayoutManager);
        leaderboardAdapter = new LeaderboardAdapter(getActivity(),userLevelsData);
        rv_leader.setAdapter(leaderboardAdapter);
        rv_search.setAdapter(leaderboardAdapter);
        rv_leader.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy)
            {
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = mLinearLayoutManager.getChildCount();
                    totalItemCount = mLinearLayoutManager.getItemCount();
                    pastVisiblesItems = mLinearLayoutManager.findFirstVisibleItemPosition();

                    if (!isLastPage)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            Common.insertLog("Last Item Wow !");
                            points=userLevelsData.get(userLevelsData.size()-1).Points;
                            rank=userLevelsData.get(userLevelsData.size()-1).Rank;
                            getLeaderShip(++page,rank,points);
                        }
                    }
                }
            }
        });
        getLeaderShip(page,rank,points);
    }

    private void getLeaderShip(int page, String rank, String points)
    {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        progressDialog.setCancelable(false);
        Call<LeaderboardModel> leaderboardModelCall = apiInterface.getLeaderBoard(Constants.GETDATA, Pref.getValue(Objects.requireNonNull(getActivity()), Constants.UserID, "", Constants.FILENAME), String.valueOf(page),"20", Constants.WEEKLY, rank, points);

        leaderboardModelCall.enqueue(new Callback<LeaderboardModel>()
        {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<LeaderboardModel> call, @NonNull Response<LeaderboardModel> response)
            {
                try {
                    isLoading = false;
                    progressDialog.dismiss();
                    LeaderboardModel leaderboardModel = response.body();
                    assert leaderboardModel != null;
                    if (leaderboardModel.status.equalsIgnoreCase("success"))
                    {
                        if (Float.parseFloat(leaderboardModel.DataClass.UserLevelsCount)>0)
                        {
                            if (mLinearLayoutManager.getItemCount()==0)
                            {
                                LeaderboardModel leaderboard =new LeaderboardModel();
                                LeaderboardModel.Data data = leaderboard.new Data();
                                LeaderboardModel.Data.UserLevelsData userData =data.new UserLevelsData();

                                userData.FeaturedIcon=leaderboardModel.DataClass.CurrentUserData.FeaturedIcon;
                                userData.LevelName=leaderboardModel.DataClass.CurrentUserData.LevelName;
                                userData.LevelNumber=leaderboardModel.DataClass.CurrentUserData.LevelNumber;
                                userData.MobileNumber=leaderboardModel.DataClass.CurrentUserData.MobileNumber;
                                userData.Name=leaderboardModel.DataClass.CurrentUserData.Name;
                                userData.Points=leaderboardModel.DataClass.CurrentUserData.Points;
                                userData.Rank=leaderboardModel.DataClass.CurrentUserData.Rank;
                                userData.UserID=leaderboardModel.DataClass.CurrentUserData.UserID;


                                userLevelsData.add(0,userData);
                                userLevelsData.addAll(leaderboardModel.DataClass.userLevelsData);
                                leaderboardAdapter.notifyDataSetChanged();
                            }
                            else
                            {
                                userLevelsData.addAll(mLinearLayoutManager.getItemCount(),leaderboardModel.DataClass.userLevelsData);
                                leaderboardAdapter.notifyDataSetChanged();
                            }
                        }
                        else
                        {
                            isLastPage=true;
                        }

                        Glide.with(Objects.requireNonNull(getActivity())).load(userLevelsData.get(1).FeaturedIcon).into(img_rank1);
                        Glide.with(getActivity()).load(userLevelsData.get(2).FeaturedIcon).into(img_rank2);
                        Glide.with(getActivity()).load(userLevelsData.get(3).FeaturedIcon).into(img_rank3);

                        if (userLevelsData.get(1).MobileNumber.length()>5 && !userLevelsData.get(1).MobileNumber.isEmpty())
                            txt_team_mobile1.setText("*****"+userLevelsData.get(1).MobileNumber.substring(9));

                        if (userLevelsData.get(2).MobileNumber.length()>5 && !userLevelsData.get(2).MobileNumber.isEmpty())
                            txt_team_mobile2.setText("*****"+userLevelsData.get(2).MobileNumber.substring(9));

                        if (userLevelsData.get(3).MobileNumber.length()>5 && !userLevelsData.get(3).MobileNumber.isEmpty())
                            txt_team_mobile3.setText("*****"+userLevelsData.get(3).MobileNumber.substring(9));

                        txt_team_points1.setText(userLevelsData.get(1).Rank);
                        txt_team_points2.setText(userLevelsData.get(2).Rank);
                        txt_team_points3.setText(userLevelsData.get(3).Rank);

                        txt_team_name1.setText(userLevelsData.get(1).Name);
                        txt_team_name2.setText(userLevelsData.get(2).Name);
                        txt_team_name3.setText(userLevelsData.get(3).Name);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(@NonNull Call<LeaderboardModel> call, @NonNull Throwable t)
            {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }
}
