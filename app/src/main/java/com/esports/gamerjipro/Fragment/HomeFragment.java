package com.esports.gamerjipro.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.esports.gamerjipro.Adapter.GamesListAdapter;
import com.esports.gamerjipro.Adapter.ImageSliderAdapter;
import com.esports.gamerjipro.Model.AdsDataModel;
import com.esports.gamerjipro.Model.DashboardGamesModel;
import com.esports.gamerjipro.Model.DashboardModel;
import com.esports.gamerjipro.Model.DashboardQuickGamesModel;
import com.esports.gamerjipro.Model.GamerjiProPopupModel;
import com.esports.gamerjipro.Model.GeneralResponseModel;
import com.esports.gamerjipro.Model.SignInModel;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.RestApis.APIClient;
import com.esports.gamerjipro.RestApis.APIInterface;
import com.esports.gamerjipro.RestApis.WebFields;
import com.esports.gamerjipro.Utils.AnalyticsSocket;
import com.esports.gamerjipro.Utils.Common;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.Utils.Pref;
import com.esports.gamerjipro.activity.ActivityWallet;
import com.esports.gamerjipro.activity.Activity_Login;
import com.esports.gamerjipro.activity.NotificationActivity;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.onesignal.OneSignal;
import com.tbuonomo.viewpagerdotsindicator.WormDotsIndicator;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private ImageView mImageNotification, mImageWallet;
    private ViewPager mViewPager;
    private RelativeLayout mRelativeMain;
    private Timer mTimerTask;
    private boolean mUserProfile = false;
    private SwipeRefreshLayout mSwipeRefresh;
    private RecyclerView mRecyclerView;
    private GamesListAdapter gamesListAdapter;
    private int currentPage = 0, NUM_PAGES = 0, play_time = 0;
    private String mPopupTitle, mPopupMessage;
    private AnalyticsSocket analyticsSocket = new AnalyticsSocket();
    private ArrayList<DashboardGamesModel> mArrDashboardGames;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_home, container, false);
        mActivity = getActivity();
        mTimerTask = new Timer();
        mArrDashboardGames = new ArrayList<>();

        getIds();
        setRegListeners();
        setFirebaseNotification();
        callToGetDashboardQuickGamesAPI();
        callToGetBannerAPI();

        if (!mUserProfile) {
            callToUserProfileAPI();
        }
        return mView;
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Relative Layout
            mRelativeMain = mView.findViewById(R.id.relative_home_main_view);

            // Image Views
            mImageNotification = mView.findViewById(R.id.image_home_notification);
            mImageWallet = mView.findViewById(R.id.image_home_wallet);

            // View Pager
            mViewPager = mView.findViewById(R.id.view_pager_home);

            // Swipe Refresh Layout
            mSwipeRefresh = mView.findViewById(R.id.swipe_refresh_view_home);

            // Recycler View
            mRecyclerView = mView.findViewById(R.id.recycler_view_home_games);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: On Click Listener
            mImageNotification.setOnClickListener(clickListener);
            mImageWallet.setOnClickListener(clickListener);

            // ToDo: Swipe Refresh Click Listener
            mSwipeRefresh.setOnRefreshListener(refreshListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.image_home_notification:
                    callToNotificationActivity();
                    break;

                case R.id.image_home_wallet:
                    callToAccountActivity();
                    break;
            }
        }
    };

    /**
     * Set On Refresh Listeners
     */
    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            Constants.mArrGames.clear();
            callToGetDashboardQuickGamesAPI();
            callToGetBannerAPI();
        }
    };

    /**
     * This method redirects you to the Notification activity
     */
    private void callToNotificationActivity() {
        try {
            Intent intent = new Intent(mActivity, NotificationActivity.class);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method redirects you to the Account activity
     */
    private void callToAccountActivity() {
        try {
            Intent intent = new Intent(mActivity, ActivityWallet.class);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set firebase notification
     */
    private void setFirebaseNotification() {
        try {
            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(task ->
                    {
                        if (!task.isSuccessful()) {
                            Common.insertLog("getInstanceId failed" + task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        if (!Pref.getValue(mActivity, Constants.UserID, "", Constants.FILENAME).isEmpty()) {
                            sendTokenServer(Objects.requireNonNull(task.getResult()).getToken());
                        }
                    });

            OneSignal.startInit(mActivity)
                    .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                    .unsubscribeWhenNotificationsAreDisabled(true)
                    .init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This should call the registering device API
     *
     * @param token - Token Id
     */
    private void sendTokenServer(String token) {
        try {
            Call<GeneralResponseModel> loginModelCall = APIClient.getClient().create(APIInterface.class).sendToken(Constants.REGISTER_DEVICE,
                    Pref.getValue(mActivity, Constants.UserID, "", Constants.FILENAME), "Android", token);
            loginModelCall.enqueue(new Callback<GeneralResponseModel>() {
                @Override
                public void onResponse(@NonNull Call<GeneralResponseModel> call, @NonNull Response<GeneralResponseModel> response) {
                    GeneralResponseModel generalResponseModel = response.body();
                    assert generalResponseModel != null;
                }

                @Override
                public void onFailure(@NotNull Call<GeneralResponseModel> call, @NotNull Throwable t) {
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This should call the Dashboard data API
     */
    private void callToGetBannerAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(mActivity);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        PackageInfo info = null;
        String version = "";
        try {
            info = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), PackageManager.GET_ACTIVITIES);
            version = info.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        Call<DashboardModel> dashboardModelCall = APIClient.getClient().create(APIInterface.class).getDashboard("GetData",
                Pref.getValue(mActivity, Constants.UserID, "", Constants.FILENAME), Constants.Apptype, version);
        dashboardModelCall.enqueue(new Callback<DashboardModel>() {
            @Override
            public void onResponse(@NonNull Call<DashboardModel> call, @NonNull Response<DashboardModel> response) {
                progressDialog.dismiss();

                if (mSwipeRefresh.isRefreshing())
                    mSwipeRefresh.setRefreshing(false);

                DashboardModel dashboardModel = response.body();
                assert dashboardModel != null;
                if (dashboardModel.status.equalsIgnoreCase("success")) {
                    mViewPager.setAdapter(new ImageSliderAdapter(mActivity, dashboardModel.DataClass.BannersData.bdataArrayList));
                    mViewPager.setPadding(50, 0, 50, 0);
                    mViewPager.setClipToPadding(false);
                    mViewPager.setPageMargin(25);

                    final WormDotsIndicator indicator = mView.findViewById(R.id.worm_dots_indicator);
                    indicator.setViewPager(mViewPager);

                    NUM_PAGES = dashboardModel.DataClass.BannersData.bdataArrayList.size();
                    final Handler handler = new Handler();
                    final Runnable Update = () ->
                    {
                        if (currentPage == NUM_PAGES) {
                            currentPage = 0;
                            //mTimerTask.cancel();
                        }
                        mViewPager.setCurrentItem(currentPage++, true);
                    };

                    mTimerTask.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            handler.post(Update);
                        }
                    }, 0, 4000);

                    if (dashboardModel.DataClass.IsDailyLogin) {
                        final Dialog dialog = new Dialog(mActivity);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
                        dialog.setContentView(R.layout.daily_points_dialog);
                        dialog.show();
                        TextView points = dialog.findViewById(R.id.txt_points);
                        points.setText(dashboardModel.DataClass.IsDailyLoginPoint);
                        ImageView close = dialog.findViewById(R.id.img_close);
                        close.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.cancel();
                                callToGamerJiPopupAPI();
                            }
                        });
                    } else {
                        callToGamerJiPopupAPI();
                    }

                    mRecyclerView.setLayoutManager(new GridLayoutManager(mActivity, 3));
                    mRecyclerView.setItemAnimator(new DefaultItemAnimator());
                    mRecyclerView.setNestedScrollingEnabled(false);
                    Constants.gdataArrayList = dashboardModel.DataClass.GamesData.gdataArrayList;

                    for (int i = 0; i < dashboardModel.DataClass.GamesData.gdataArrayList.size(); i++) {
                        DashboardGamesModel dashboardGamesModel = new DashboardGamesModel();
                        dashboardGamesModel.setGameID(dashboardModel.DataClass.GamesData.gdataArrayList.get(i).GameID);
                        dashboardGamesModel.setName(dashboardModel.DataClass.GamesData.gdataArrayList.get(i).Name);
                        dashboardGamesModel.setFeaturedImage(dashboardModel.DataClass.GamesData.gdataArrayList.get(i).FeaturedImage);
                        mArrDashboardGames.add(dashboardGamesModel);
                    }

                    Constants.mArrGames = mArrDashboardGames;

                    gamesListAdapter = new GamesListAdapter(mActivity, mArrDashboardGames);
                    mRecyclerView.setAdapter(gamesListAdapter);

                    if (dashboardModel.DataClass.IsFirstTime) {
                        if (!dashboardModel.DataClass.IntroVideoLink.isEmpty())
                            showWalkThroughVideo(dashboardModel.DataClass.IntroVideoTitle, dashboardModel.DataClass.IntroVideoLink, dashboardModel.DataClass.adsDataModel, dashboardModel.DataClass.AdsCount);
                    }

                    if (!dashboardModel.DataClass.IsFirstTime && dashboardModel.DataClass.AdsCount > 0 && !dashboardModel.DataClass.adsDataModel.AdVideo.isEmpty()) {
                        showVideoAds(dashboardModel.DataClass.adsDataModel);
                    }
                } else {
                    Constants.SnakeMessageYellow(mRelativeMain, dashboardModel.message);
                    Intent i = new Intent(mActivity, Activity_Login.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    Pref.ClearAllPref(mActivity, Constants.FILENAME);
                    Pref.setValue(mActivity, Constants.IS_LOGIN, false, Constants.FILENAME);
                    startActivity(i);
                    mActivity.finish();
                }
            }

            @Override
            public void onFailure(@NonNull Call<DashboardModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
            }
        });
    }

    /**
     * This should call the walk through video bottom sheet
     *
     * @param introVideoTitle - Intro Video Title
     * @param introVideoLink  -  Intro Video Link
     * @param adsDataModel    - Ads Data Model
     * @param adsCount        - Ads Count
     */
    @SuppressLint("SetJavaScriptEnabled")
    private void showWalkThroughVideo(String introVideoTitle, String introVideoLink, AdsDataModel adsDataModel, int adsCount) {
        final BottomSheetDialog dialog = new BottomSheetDialog(mActivity);
        dialog.setContentView(R.layout.walkthrough_bottomsheet);
        dialog.setCanceledOnTouchOutside(false);

        // Image View
        ImageView mImageClose = dialog.findViewById(R.id.img_close);

        // Text View
        TextView mTextIntroTitle = dialog.findViewById(R.id.txt_intro_title);

        // Web View
        WebView mWebView = dialog.findViewById(R.id.wv_walkthrough);

        // Sets the data
        Objects.requireNonNull(mTextIntroTitle).setText(introVideoTitle);

        mWebView.setWebChromeClient(new WebChromeClient());
        mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setPluginState(WebSettings.PluginState.ON);
        mWebView.getSettings().setAllowFileAccess(true);
        mWebView.getSettings().setDefaultFontSize(18);
        mWebView.loadUrl(introVideoLink);

        // ToDo: Close image click listener
        mImageClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (adsCount > 0) {
                    if (!adsDataModel.AdVideo.isEmpty()) {
                        showVideoAds(adsDataModel);
                    }
                }
            }
        });
        dialog.show();
    }

    /**
     * This should show the Video Ads
     *
     * @param adsDataModel - Ads Data Model
     */
    @SuppressLint("SetJavaScriptEnabled")
    private void showVideoAds(AdsDataModel adsDataModel) {
        final BottomSheetDialog dialog = new BottomSheetDialog(mActivity);
        dialog.setContentView(R.layout.walkthrough_bottomsheet);
        dialog.setCanceledOnTouchOutside(false);

        // Image Views
        ImageView mImageClose = dialog.findViewById(R.id.img_close);

        // Text View
        TextView mTextIntroTitle = dialog.findViewById(R.id.txt_intro_title);

        // Progress Bar
        ProgressBar mProgressBar = dialog.findViewById(R.id.progrss);

        // Web View
        WebView mWebView = dialog.findViewById(R.id.wv_walkthrough);

        // Relative Layouts
        RelativeLayout mRelativeVideosAds = dialog.findViewById(R.id.lyt_video_ads);

        // Video View
        VideoView mVideoView = dialog.findViewById(R.id.vv);

        // Sets the data
        Objects.requireNonNull(mTextIntroTitle).setText(adsDataModel.AdName);

        // Sets the view visibility
        Objects.requireNonNull(mWebView).setVisibility(View.GONE);
        Objects.requireNonNull(mRelativeVideosAds).setVisibility(View.VISIBLE);

        MediaController mediacontroller;
        mediacontroller = new MediaController(mActivity);
        mediacontroller.setAnchorView(mVideoView);

        assert mVideoView != null;
        mVideoView.setVideoPath(adsDataModel.AdVideo);

        // ToDo: Close the video
        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                if (adsDataModel.VideoAutoplay) {
                    play_time = mVideoView.getDuration() / 1000;
                    mVideoView.stopPlayback();
                    analyticsSocket.sendAnalytics(adsDataModel.AdType, adsDataModel.AdID, adsDataModel.AdVideoName, Constants.AD_PLAY,
                            Pref.getValue(mActivity, Constants.UserID, "", Constants.FILENAME), play_time);
                    dialog.dismiss();
                }
            }
        });

        // ToDo: Close the progress bar and play the video
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                assert mProgressBar != null;
                mProgressBar.setVisibility(View.GONE);
                mVideoView.start();
            }
        });

        // ToDo: Close image click listener
        mImageClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                play_time = mVideoView.getCurrentPosition() / 1000;
                dialog.dismiss();
                mVideoView.stopPlayback();
                analyticsSocket.sendAnalytics(adsDataModel.AdType, adsDataModel.AdID, adsDataModel.AdVideoName, Constants.AD_PLAY,
                        Pref.getValue(mActivity, Constants.UserID, "", Constants.FILENAME), play_time);
            }
        });
        dialog.show();
    }

    /**
     * This should call to the user profile API
     */
    private void callToUserProfileAPI() {
        final ProgressDialog progressDialog = new ProgressDialog(mActivity);
        progressDialog.setMessage("Please wait...."); // Setting Message
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        Call<SignInModel> signInModelCall;

        signInModelCall = APIClient.getClient().create(APIInterface.class).getProfile(Constants.GET_PROFILE,
                Pref.getValue(mActivity, Constants.UserID, "", Constants.FILENAME));
        signInModelCall.enqueue(new Callback<SignInModel>() {
            @Override
            public void onResponse(@NonNull Call<SignInModel> call, @NonNull Response<SignInModel> response) {
                progressDialog.dismiss();
                SignInModel signInModel = response.body();
                assert signInModel != null;

                if (signInModel.status.equalsIgnoreCase("success")) {
                    mUserProfile = true;
                    Pref.setValue(mActivity, Constants.UserID, signInModel.userDataClass.UserID, Constants.FILENAME);
                    Pref.setValue(mActivity, Constants.FullName, signInModel.userDataClass.FullName, Constants.FILENAME);
                    Pref.setValue(mActivity, Constants.lastname, signInModel.userDataClass.LastName, Constants.FILENAME);
                    Pref.setValue(mActivity, Constants.BirthDate, signInModel.userDataClass.BirthDate, Constants.FILENAME);
                    Pref.setValue(mActivity, Constants.firstname, signInModel.userDataClass.FirstName, Constants.FILENAME);
                    Pref.setValue(mActivity, Constants.TeamName, signInModel.userDataClass.TeamName, Constants.FILENAME);
                    Pref.setValue(mActivity, Constants.CountryCode, signInModel.userDataClass.CountryCode, Constants.FILENAME);
                    Pref.setValue(mActivity, Constants.MobileNumber, signInModel.userDataClass.MobileNumber, Constants.FILENAME);
                    Pref.setValue(mActivity, Constants.EmailAddress, signInModel.userDataClass.EmailAddress, Constants.FILENAME);
                    Pref.setValue(mActivity, Constants.LevelIcon, signInModel.userDataClass.LevelIcon, Constants.FILENAME);
                    if (!signInModel.userDataClass.BirthDate.isEmpty()) {
                        @SuppressLint("SimpleDateFormat") DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                        @SuppressLint("SimpleDateFormat") DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
                        String inputDateStr = signInModel.userDataClass.BirthDate;
                        Date date = null;
                        try {
                            date = inputFormat.parse(inputDateStr);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        String outputDateStr = outputFormat.format(date);
                        Pref.setValue(mActivity, Constants.BirthDate, outputDateStr, Constants.FILENAME);
                    }
                    Pref.setValue(mActivity, Constants.State, signInModel.userDataClass.State, Constants.FILENAME);
                    Pref.setValue(mActivity, Constants.ProfileImage, signInModel.userDataClass.ProfileImage, Constants.FILENAME);
                    Constants.setUserGameData(signInModel.userDataClass.GamesData.gamesDataArrayList, mActivity);
                    Pref.setValue(mActivity, Constants.IS_LOGIN, true, Constants.FILENAME);
                    Pref.setValue(mActivity, Constants.SOCIAL_LOGIN, true, Constants.FILENAME);
                } else {
                    Intent i = new Intent(mActivity, Activity_Login.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    Pref.ClearAllPref(mActivity, Constants.FILENAME);
                    Pref.setValue(mActivity, Constants.IS_LOGIN, false, Constants.FILENAME);
                    startActivity(i);
                    mActivity.finish();
                }
            }

            @Override
            public void onFailure(@NonNull Call<SignInModel> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
                mUserProfile = false;
            }
        });
    }

    /**
     * This method Call Image Upload API Call
     */
    private void callToGamerJiPopupAPI() {
        try {
            String mUserId = Pref.getValue(mActivity, Constants.UserID, "", Constants.FILENAME);

            Call<GamerjiProPopupModel> callRepos = APIClient.getClient().create(APIInterface.class).getGamerJiProPopup(WebFields.GAMERJI_PRO_POPUP.MODE, mUserId);
            callRepos.enqueue(new Callback<GamerjiProPopupModel>() {
                @Override
                public void onResponse(@NonNull Call<GamerjiProPopupModel> call, @NonNull Response<GamerjiProPopupModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        assert response.body() != null;
                        if (response.body().getStatus().equalsIgnoreCase(Constants.API_SUCCESS)) {
                            mPopupTitle = response.body().getData().getTitle();
                            mPopupMessage = response.body().getData().getContent();
                            String mShowPopup = response.body().getData().getShowPopup();

                            if (mShowPopup.equalsIgnoreCase("1")) {
                                openGamerJiProPopup();
                            }
                        } else {
                            Constants.SnakeMessageYellow(mRelativeMain, response.body().getMessage());
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GamerjiProPopupModel> call, @NonNull Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should open the logout popup and on click of OK it should be redirected to the Login activity and
     * on click of OK to dismiss the logout popup.
     */
    @SuppressLint("SetTextI18n")
    private void openGamerJiProPopup() {
        try {
            final Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_gamerji_pro);

            Window window = dialog.getWindow();
            window.setLayout(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();

            // Text Views
            ImageView mImageClose = dialog.findViewById(R.id.image_dialog_gamerji_pro_close);

            // Linear Layouts
            TextView mTextSubHeader = dialog.findViewById(R.id.text_dialog_gamerji_pro_sub_header);
            TextView mTextMessage = dialog.findViewById(R.id.text_dialog_gamerji_pro_message);

            // Sets up the data
            mTextSubHeader.setText(mPopupTitle);
            mTextMessage.setText(Html.fromHtml(mPopupMessage));

            /*String mBonusCode = Common.getSpannedColouredString("\"VersionTwo1200\"", "#18ABE2");
            String mAmount = Common.getSpannedColouredString("₹1250", "#E29618");
            String mMessage = "Use the code " + mBonusCode + " to add " + mAmount + " bonus in your wallet.";
            mTextMessage.setText(Html.fromHtml(mMessage));*/

            // ToDo: Cancel Button On Click Listener
            mImageClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.cancel();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should get the Dashboard Quick Games API
     */
    private void callToGetDashboardQuickGamesAPI() {
        try {
            ProgressDialog progressDialog = new ProgressDialog(mActivity);
            progressDialog.setMessage("Please wait...."); // Setting Message
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
            progressDialog.show(); // Display Progress Dialog
            progressDialog.setCancelable(false);

            Call<DashboardQuickGamesModel> callRepos = APIClient.getClient().create(APIInterface.class).getDashboardQuickGames(WebFields.DASHBOARD_GET_QUICK_GAMES.MODE,
                    Pref.getValue(mActivity, Constants.UserID, "", Constants.FILENAME));
            callRepos.enqueue(new Callback<DashboardQuickGamesModel>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(@NonNull Call<DashboardQuickGamesModel> call, @NonNull Response<DashboardQuickGamesModel> response) {
                    progressDialog.dismiss();

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        assert response.body() != null;
                        if (response.body().getStatus().equalsIgnoreCase(Constants.API_SUCCESS)) {

                            DashboardGamesModel dashboardGamesModel = new DashboardGamesModel();
                            dashboardGamesModel.setGameID("");
                            dashboardGamesModel.setName(response.body().getData().getTitle());
                            dashboardGamesModel.setFeaturedImage(response.body().getData().getFeaturedImage());
                            mArrDashboardGames.add(dashboardGamesModel);
                        } else {
                            Constants.SnakeMessageYellow(mRelativeMain, response.body().getMessage());
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<DashboardQuickGamesModel> call, @NonNull Throwable t) {
                    progressDialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
