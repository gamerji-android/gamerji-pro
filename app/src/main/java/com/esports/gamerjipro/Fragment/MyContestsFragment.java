package com.esports.gamerjipro.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.esports.gamerjipro.Adapter.DynamicGameContestAdapter;
import com.esports.gamerjipro.Interface.RulesClickListener;
import com.esports.gamerjipro.Interface.WinnerClickListener;
import com.esports.gamerjipro.R;
import com.esports.gamerjipro.Utils.AnalyticsSocket;
import com.esports.gamerjipro.Utils.Constants;
import com.esports.gamerjipro.activity.ActivityWallet;
import com.google.android.material.tabs.TabLayout;

public class MyContestsFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private ImageView mImageWallet;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    //    public static ViewPager mViewPagerAds;
//    public static View mViewSliderAds;
    public static AnalyticsSocket analyticsSocket = new AnalyticsSocket();
    public static boolean sendSwipeImpression = true;
    public static WinnerClickListener winnerClickListener;
    public static RulesClickListener rulesClickListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // ToDo: Initialise Rules Click Listener
        if (context instanceof RulesClickListener) {
            rulesClickListener = (RulesClickListener) context;
        } else {
            throw new ClassCastException(context.toString() + " Please Implement Rules Click Listener");
        }

        // ToDo: Initialise Winner Click Listener
        if (context instanceof WinnerClickListener) {
            winnerClickListener = (WinnerClickListener) context;
        } else {
            throw new ClassCastException(context.toString() + " Please Implement Winner Click Listener");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_my_contests, container, false);
        mActivity = getActivity();

        getIds();
        setRegListeners();
        setViewPager();
        return mView;
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Image Views
            mImageWallet = mView.findViewById(R.id.image_my_contests_wallet);

            // View
//            mViewSliderAds = mView.findViewById(R.id.layout_slider_ads);

            // Tab Layout
            mTabLayout = mView.findViewById(R.id.tab_layout_my_contests);

            // View Pager
            mViewPager = mView.findViewById(R.id.view_pager_my_contests);
//            mViewPagerAds = mView.findViewById(R.id.slider_pager_ads);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: On Click Listener
            mImageWallet.setOnClickListener(clickListener);

            // ToDo: Tab Selected Listener
            mTabLayout.setOnTabSelectedListener(onTabSelectedListener());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.image_my_contests_wallet:
                    callToAccountActivity();
                    break;
            }
        }
    };

    /**
     * This method redirects you to the Account activity
     */
    private void callToAccountActivity() {
        try {
            Intent intent = new Intent(mActivity, ActivityWallet.class);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the view pager and binding the tab layouts
     */
    private void setViewPager() {
        if (Constants.gdataArrayList.size() > 0) {
            for (int k = 0; k < Constants.gdataArrayList.size(); k++) {
                    mTabLayout.addTab(mTabLayout.newTab().setText(Constants.gdataArrayList.get(k).Name));
            }

            DynamicGameContestAdapter adapter = new DynamicGameContestAdapter
                    (getChildFragmentManager(), Constants.gdataArrayList.size(), Constants.gdataArrayList);
            mViewPager.setAdapter(adapter);
            mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
            mTabLayout.setTabTextColors(ColorStateList.valueOf(Color.WHITE));
            mTabLayout.setSelectedTabIndicatorHeight(10);
            mTabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.orange_color));

            if (mTabLayout.getTabCount() == 2) {
                mTabLayout.setTabMode(TabLayout.MODE_FIXED);
            } else {
                mTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
            }

            // ToDo: Set Font Style & Color
            for (int i = 0; i < mTabLayout.getTabCount(); i++) {
                TabLayout.Tab tab = mTabLayout.getTabAt(i);
                if (tab != null) {
                    TextView tabTextView = new TextView(mActivity);
                    if (i == 0) {
                        Typeface font = null;
                        font = ResourcesCompat.getFont(mActivity, R.font.poppins_semibold);
                        tabTextView.setTypeface(font);
                    }
                }
            }
        }
    }

    /**
     * Tab Selected Listener
     */
    private TabLayout.OnTabSelectedListener onTabSelectedListener() {

        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                setTabSelectedFontStyle(tab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                setTabUnselectedFontStyle(tab);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        };
    }

    /**
     * This should be called when Tab is getting selected
     */
    private void setTabSelectedFontStyle(TabLayout.Tab tab) {
        try {
            ViewGroup vg = (ViewGroup) mTabLayout.getChildAt(0);
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(tab.getPosition());
            int tabChildsCount = vgTab.getChildCount();

            // ToDo: Set Font Style & Color
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    Typeface font = null;
                    font = ResourcesCompat.getFont(mActivity, R.font.poppins_semibold);
                    ((TextView) tabViewChild).setTypeface(font);
                    ((TextView) tabViewChild).setTextColor(Color.WHITE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This should be called when Tab is getting unselected
     */
    private void setTabUnselectedFontStyle(TabLayout.Tab tab) {
        try {
            ViewGroup vg = (ViewGroup) mTabLayout.getChildAt(0);
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(tab.getPosition());
            int tabChildsCount = vgTab.getChildCount();

            // ToDo: Set Font Style & Color
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    Typeface font = null;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        font = getResources().getFont(R.font.poppins);
                        ((TextView) tabViewChild).setTypeface(font);
                    }
                    ((TextView) tabViewChild).setTextColor(Color.WHITE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
