package com.esports.gamerjipro.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.esports.gamerjipro.Adapter.BaseAdapter
import com.esports.gamerjipro.R
import com.esports.gamerjipro.Utils.jsonString
import kotlinx.android.synthetic.main.frg_how_to_play.*
import kotlinx.android.synthetic.main.row_how_to_play.view.*
import kotlinx.android.synthetic.main.row_how_to_play_desc.view.*

class HowToPlayDynamicSubFragment : Fragment()
{

    private val list = ArrayList<List>()
    private var pos = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            pos = arguments!!.getInt("pos")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.frg_how_to_play, container, false)
    }

    class List(val title: String, val list: ArrayList<ListSub>)
    class ListSub(val header: String, val desc: String, val image: String)

    override fun onActivityCreated(savedInstanceState: Bundle?)
    {
        super.onActivityCreated(savedInstanceState)
      //   toolbar.visibility = GONE

        val data = (parentFragment as HowToPlayKotlin).getData(pos)
        for (titles in data) {
            val desc = ArrayList<ListSub>()
            for (descs in titles.asJsonObject.getAsJsonArray("content")) {
                desc.add(ListSub(descs.jsonString("header"), descs.jsonString("desc"), descs.jsonString("image")))
            }
            list.add(List(titles.jsonString("title"), desc))
        }
        item_list.adapter = BaseAdapter(
                layoutId = R.layout.row_how_to_play,
                list = list,
                viewHolder = { holder, item ->
                    holder.itemView.apply {
                        holder.itemView.tv_intro.text = item.title
                        holder.itemView.setOnClickListener {
                            holder.itemView.how_to_play_desc_list.visibility = if (holder.itemView.how_to_play_desc_list.visibility == VISIBLE) GONE else VISIBLE
                            holder.itemView.tv_intro.setCompoundDrawablesWithIntrinsicBounds(0, 0, if (holder.itemView.how_to_play_desc_list.visibility == VISIBLE) R.drawable.ic_down_arrow else R.drawable.ic_next_arrow, 0)
                        }
                        holder.itemView.how_to_play_desc_list.adapter = BaseAdapter(
                                layoutId = R.layout.row_how_to_play_desc,
                                list = item.list,
                                viewHolder = { holder, item ->
                                    holder.itemView.apply {
                                        if (item.header.isNotEmpty()) {
                                            holder.itemView.tv_header.visibility = VISIBLE
                                            holder.itemView.tv_header.text = item.header
                                        } else if (item.desc.isNotEmpty()) {
                                            if (item.desc.startsWith("• ")) {
                                                holder.itemView.tablelayout.visibility = VISIBLE
                                                holder.itemView.tv_points.text = item.desc.replace("• ", "")
                                            } else {
                                                holder.itemView.tv_desc.visibility = VISIBLE
                                                holder.itemView.tv_desc.text = item.desc
                                            }
                                        } else {
                                            holder.itemView.iv_image.visibility = VISIBLE
                                            Glide.with(context)
                                                    .load(item.image)
                                                    .apply(RequestOptions().error(R.drawable.main_bg))
                                                    .transition(DrawableTransitionOptions.withCrossFade())
                                                    .into(holder.itemView.iv_image)
                                        }
                                    }
                                }
                        )
                    }
                }
        )
    }

    companion object {
        fun newInstance(pos: Int): HowToPlayDynamicSubFragment {
            val fragment = HowToPlayDynamicSubFragment()
            val bdl = Bundle()
            bdl.putInt("pos", pos)
            fragment.arguments = bdl
            return fragment
        }
    }
}
