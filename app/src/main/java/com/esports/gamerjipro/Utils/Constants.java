package com.esports.gamerjipro.Utils;

import android.app.DownloadManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import com.androidadvance.topsnackbar.TSnackbar;
import com.esports.gamerjipro.Model.ContestDetailsModel;
import com.esports.gamerjipro.Model.DashboardGamesModel;
import com.esports.gamerjipro.Model.DashboardModel;
import com.esports.gamerjipro.Model.GamesDataModel;
import com.esports.gamerjipro.Model.SignInModel;
import com.esports.gamerjipro.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Constants {
    public static final String TEST_BASE_API = "http://3.15.60.207/gamerji/WebServices/";
    public static final String LIVE_APi = "https://www.gamerji.in/gamerji/WebServices/";
    public static final String PAYTM_API = "https://accounts-uat.paytm.com/signin/";
//        public static final String APP_MAINTENANCE_STATUS_API = "http://3.15.60.207/gamerji/maintenance.json";
    public static final String APP_MAINTENANCE_STATUS_API = "https://www.gamerji.in/gamerji/maintenance.json";
    public static boolean ENVIRONMENT = true; //false= development and true means live
    public static String APK = "APK";
    public static String Apptype;

    public static String BASE_API = LIVE_APi;

    public static String SOCKET_API = "http://13.232.28.136:9002/";

    public static final String SOCKET_BASE_API = "http://13.232.28.136:9002/";
    public static final String SOCKET_TEST_API = "http://3.15.60.207:9002/";
    public static final String HOW_TO_PLAY_BASE_API = "https://s3.ap-south-1.amazonaws.com/devfantasyji/json_files/";
    public static final String SIGNUP_USER = "Authentication/SignUp/";
    public static final String SIGNIN_USER = "Authentication/SignIn/";
    public static final String CHECK_MOBILE = "Authentication/UserSettings";
    public static final String UPDATE_MOBILE = "Authentication/UserSettings/";
    public static final String DASHBOARD = "User/Dashboard/";
    public static final String V2_DASHBOARD = "v2/User/Dashboard/";
    public static final String V3_DASHBOARD = "v3/User/Dashboard/";
    public static final String GAMES_TYPE = "Game/Fetch";
    public static final String RESEND_OTP = "Authentication/ResendOTP/";
    public static final String FORGOT_OTP = "Authentication/Forgot/";
    public static final String CHANGE_PASS = "Authentication/Settings/";
    public static final String CONTESTS = "Contest/Fetch/";
    public static final String V2_CONTESTS = "v2/Contest/Fetch/";
    public static final String V3_CONTESTS = "v3/Contest/Fetch/";
    public static final String V3_CONTESTS_DETAIL = "v3/Contest/Details";
    public static final String SINGLE_CONTESTS = "Contest/Fetch";
    public static final String JOIN_CONTESTS = "Contest/Details";
    public static final String V3_JOIN_CONTESTS = "v3/Contest/Details";
    public static final String V_3_TOURNAMENT_DETAILS = "v3/Tournament/Details";
    public static final String GENERAL_FETCH = "General/Fetch";
    public static final String UPDATE_PROFILE = "User/Profile";
    public static final String UPDATE_PROFILE_V2 = "v2/User/Profile";
    public static final String UPDATE_PROFILE_V3 = "v3/User/Profile";
    public static final String USER_FETCH = "User/Fetch";
    public static final String V2_USER_FETCH = "v2/User/Fetch";
    public static final String V3_USER_FETCH = "v3/User/Fetch";
    public static final String RESET_PASSWORD = "Authentication/Forgot/";
    public static final String GENERAL_DETAILS = "v3/General/Details";
    public static final String PAYMENT_DETAILS = "Payment/Details";
    public static final String LEADERBOARD = "User/Leaderboard/";
    public static final String LEADERBOARD_V2 = "v2/User/Leaderboard/";
    public static final String CONTET_FETCH_V2 = "v2/Contest/Fetch";
    public static final String CONTET_FETCH_V3 = "v3/Contest/Fetch";
    public static final String USER_PROFILE_V2 = "v2/User/Profile";
    public static final String GENERAL_FETCH_V2 = "v2/General/Fetch";
    public static final String GENERAL_FETCH_V3 = "v3/General/Fetch";
    public static final String GAME_FETCH_V3 = "v3/Game/Fetch";
    public static final String GENERAL_DETAILS_V3 = "v3/General/Details";
    public static final String SEARCH = "v2/User/Fetch";
    public static final String TOURNAMENT_FETCH = "v3/Tournament/Fetch";
    public static final String REGISTER_DEVICE = "RegisterDevice";
    public static final String HOWTOPLAY = "how_to_play.json";
    public static final String IMAGE_BASE_URl = "https://s3.ap-south-1.amazonaws.com/devfantasyji/";
    public static final String JSON_HOW_TO_PLAY = "https://www.gamerji.in/gamerji/how_to_play.json";
    public static final String OTP = "otp";
    public static final String USER_FETCH_RAW = "v3/User/FetchRaw";
    public static final String USER_DETAILS = "v3/User/Details";
    public static final String GAME_FETCH_RAW = "v3/Game/FetchRaw";
    public static final String GENERAL_DETAILS_RAW = "v3/General/DetailsRaw";
    public static final String STREAMER_FETCH = "v3/Streamer/Fetch";
    public static final String STREAMER_DETAILS = "v3/Streamer/Details";
    public static final String GENERAL_FETCH_RAW = "v3/General/FetchRaw";
    public static final String H5_GAMES_FETCH = "v3/H5G/Fetch";
    public static final String H5_GAMES_DETAILS = "v3/H5G/Details";

    public static final String UserID = "UserID";
    public static final String CountryCode = "CountryCode";
    public static final String FullName = "FullName";
    public static final String firstname = "firstname";
    public static final String lastname = "lastName";
    public static final String MobileNumber = "MobileNumber";
    public static final String EmailAddress = "EmailAddress";
    public static final String LevelIcon = "LevelIcon";
    public static final String TeamName = "TeamName";
    public static final String State = "State";
    public static final String BirthDate = "BirthDate";
    public static final String ProfileImage = "ProfileImage";
    public static final String Status = "Status";
    public static final String CanPlayGames = "CanPlayGames";
    public static final String Check = "Check";
    public static final String ValidateSignupCode = "ValidateSignupCode";
    public static final String IS_LOGIN = "login";
    public static final String SOCIAL_LOGIN = "social_login";
    public static final String FILENAME = "GamerJiPro";
    public static final String REQUEST = "Request";
    public static final String GET_ALL_CONTESTS = "GetAllContests";
    public static final String GET_CONTESTS_LIST = "GetContestsList";
    public static final String GET_ALL_GAMES = "GetAllGames";
    public static final String GET_JOINED_CONTEST_TIME_SLOTS = "GetJoinedContestsTimeSlots";
    public static final String GET_ALL_TOURNAMENT = "GetAllTournaments";
    public static final String GET_ALL_JOINED_CONTESTS = "GetAllJoinedContests";
    public static final String GET_ALL_STATES = "GetAllStates";
    public static final String GET_CUSTOMER_ISSUES = "GetCustomerIssues";
    public static final String USER_GAME_DATA = "UserGameData";
    public static final String UPDATE_GAME_PROFILE = "UpdateGameProfile";
    public static final String GET_SINGLE_CONTEST = "GetSingleContest";
    public static final String GET_SINGLE_TOURNAMENT = "GetSingleTournament";
    public static final String ADD_FEEDBACK = "AddFeedback";
    public static final String GET_PLAYERS = "GetPlayers";
    public static final String GET_CONTEST_DETAIL = "GetContestDetails";
    public static final String GetCTDetails = "GetCTDetails";
    public static final String GET_STATISTICS = "GetStatistics";
    public static final String JOIN_CONTEST = "JoinContest";
    public static final String JOIN_TOURNAMENT = "JoinTournament";
    public static final String ALL_JOINED_CONTEST = "GetAllJoinedTypes";
    public static final String RESET_PWD = "ResetPassword";
    public static final String UPDATEMOBILE = "UpdateMobile";
    public static final String UPLOAD_SS = "UploadScreenshot";
    public static final String UPDATE_PAN = "UpdatePanInfo";
    public static final String UPDATE_BANK = "UpdateBankInfo";
    public static final String LINK_BANK = "LinkBankAccount";
    public static final String LINK_UPI = "LinkUPIAccount";
    public static final String UPDATE_USER_PROFILE = "UpdateProfile";
    public static final String GET_GAMER_PROFILE = "GetGamerProfile";
    public static final String GENERATE_OTP = "GenerateOTP";
    public static final String APPLY_PROMO = "ApplyPromoCode";
    public static final String GET_REFERRAL = "GetReferral";
    public static final String SUBMIT_REQUEST = "SubmitRequest";
    public static final String GET_PAYMENT_TOKEN = "GetPaymentToken";
    public static final String GET_PAYMENT_STATUS = "CheckPaymentStatus";
    public static final String GET_TERMS = "GetTerms";
    public static final String GET_TIME_SLOTS = "GetTimeSlots";
    public static final String GET_PRIVACY = "GetPrivacyPolicy";
    public static final String GETSINGLECONTESTWINNER = "GetSingleContestWinningPool";
    public static final String GET_TOURNAMENT_WINNING_POOL = "GetWinningPool";
    public static final String GET_RULES = "GetRules";
    public static final String VERIFY_CONTEST_USER = "verifyContestUser";
    public static final String GETDATA = "GetData";
    public static final String JOINCHATROOM = "joinChatRoom";
    public static final String SENDCHAT = "sendChat";
    public static final String ADD_HISTORY = "addHistory";
    public static final String RECEIVECHAT = "receiveChat";
    public static final String REQUESTALLCHATMESSAGE = "requestAllChatMessage";
    public static final String RECEIVEALLCHATMESSAGE = "receiveAllChatMessage";
    public static final String WALLET_USAGE_LIMIT = "GetWalletUsageLimit";
    public static final String GET_TRANSCATIONS = "GetTransactions";
    public static final String GET_NOTIFICATIONS = "GetAllNotifications";
    public static final String GET_CUSTOMER_TICKETS = "GetCustomerTickets";
    public static final String GET_CUSTOMER_TICKET_HISTORY = "GetCustomerTicketHistory";
    public static final String GET_CUSTOMER_TICKET_NEW_MSG = "GetCustomerTicketNewHistory";
    public static final String CUSTOMER_SEND_MESSAGE = "CCTSendMessage";
    public static final String SEND_INVOICE = "SendInvoiceEmail";
    public static final String CHANGE_PASSWORD = "ChangePassword";
    public static final String VERIFYEMAIL = "VerifyEmailAddress";
    public static final String WITHDRAW_REQUEST = "WithdrawalRequest";
    public static final String GET_VIDEOS = "GetVideos";
    public static final String GET_ALL_POINTS = "GetAllPoints";
    public static final String APP_VERSION = "CheckAppVersion";
    public static final String GET_PROFILE = "GetProfile";
    public static final String SAVE_CONTACT = "Save";
    public static final String SEARCH_USER = "searchUsers";
    public static final String isIsFirsttimeApp = "appfirsttime";
    public static String Main_game_name = "";
    public static String bankVerified = "bankVerified";
    public static String type = "type";
    public static int LINK_ACCOUNTS = 121;
    public static final int PIC_WIDTH = 512;
    public static final int PIC_HEIGHT = 512;
    public static String UPI_REGEX = "[a-zA-Z0-9.\\-_]{2,256}@[a-zA-Z]{2,64}";
    public static String UPI_DATA = "upidata";
    public static String UPI_Verified = "upi_verified";
    public static String SUBMUT_ISSUE_REPORT = "SubmitReportIssue";
    public static String SUBMUT_RATING = "SubmitRating";

    public static final String WALLEET_VERIFICATION_INFO = "GetWalletVerificationInfo";
    public static final int PROFILE_UPDATE_STATUS_REQUEST_CODE = 2;
    public static final int ENTER_SQUAD_REQUEST_CODE = 100;
    public static final String PAYMENT_STATUS_SUCCESS = "success";
    public static final String PAYMENT_AMOUNT = "amount";
    public static final int PROFILE_UPDATE_FAIL = 3;
    public static final int ACCOUNT_LINK = 4;
    public static final int UPI_LINK = 5;
    public static final int ACCOUNT_LINK_CANCEL = 6;
    public static final String WAITING = "1";
    public static final String STARTED = "2";
    public static final String IN_PROGRESS = "3";
    public static final String REVIEW = "4";
    public static final String COMPLETED = "5";
    public static final String FACEBOOK = "2";
    public static final String GOOGLE = "3";
    public static final String BASIC = "1";
    public static final String MOBILE_VERIFICATION_PENDING = "3";
    public static final String PUBG_GAME_ID = "1";
    public static final String CR_GAME_ID = "2";
    public static final String JOINED_CONTEST = "1";
    public static final String COMPLETED_CONTEST = "2";
    public static final String CASHFREE_CARD = "card";
    public static final String CASHFREE_UPI = "upi";
    public static final String CASHFREE_NB = "nb";
    public static final String CASHFREE_WALLET = "wallet";
    public static final int PAYMENT_RESULT = 1;
    public static final String DEFAULT_JOIN = "1";
    public static final String VIDEO_JOIN = "2";
    public static final String MIN_WITHDRAW = "min_withdraw";
    public static final String MAX_WITHDRAW = "max_withdraw";
    public static String CASHFREE_APP_ID = "14924ffc7ebd7969d5fe980c642941";
    public static final String CASHFREE_TEST_APP_ID = "59117949aef92b9232d968371195";
    public static final String CASHFREE_LIVE_APP_ID = "14924ffc7ebd7969d5fe980c642941";

    public static final String CASHFREE_TEST = "TEST";
    public static final String CASHFREE_PROD = "PROD";
    public static String CASHFREE_TYPE = "PROD";
    public static String TOURNAMENT = "2";
    public static String CONTEST = "1";

    public static String API_SUCCESS = "success";
    public static String API_ERROR = "error";

    public static String GAME_VALORANT_ID = "144231";
    public static String GAME_WCC_RIVALS_ID = "144232";
    public static String GAME_P_LITE_ID = "144233";
    public static String GAME_FREE_FIRE_ID = "144234";
    public static String GAME_CALL_OF_DUTY_ID = "144235";
    public static String GAME_CLASH_ROYALE_ID = "144236";
    public static String GAME_P_MOBILE_ID = "144237";

    public static String VIDEOS_POPULAR = "2";
    public static String VIDEOS_HOST_STREAMS = "3";

    public static String TAG = "AdGyde";

    public static boolean isFirsttime = true;

    public static final String NOTIFY_URL_LIVE = "https://www.gamerji.in/gamerji/WebServices/payment/verifyPaymentData/";
    public static final String NOTIFY_URL_TEST = "http://3.15.60.207/gamerji/WebServices/payment/verifyPaymentData/";
    public static String NOTIFY_URL = "https://www.gamerji.in/gamerji/WebServices/payment/verifyPaymentData/";

    public static final String TODAY = "1";
    public static final String WEEKLY = "2";
    public static final String MONTHLY = "3";
    public static final String ALLTIME = "4";

    public static final String AD_IMPRESSION = "1";
    public static final String AD_CLICK = "2";
    public static final String AD_PLAY = "3";

    public static final int HEADER = 1;
    public static final int ITEM = 2;
    public static int SNACK_BAR_RETRY = 101;

    public static final String PROFILE_BOTTOM_SHEET_P_MOBILE_INFO = "PMobileInfo";
    public static final String PROFILE_BOTTOM_SHEET_CLASH_INFO = "ClashInfo";
    public static final String PROFILE_BOTTOM_SHEET_POINTS_INFO = "PointsInfo";
    public static final String PROFILE_BOTTOM_SHEET_TEAM_INFO = "TeamInfo";

    public static ArrayList<DashboardModel.GamesData.Gdata> gdataArrayList = new ArrayList<>();
      public static ArrayList<DashboardGamesModel> mArrGames = new ArrayList<>();

    public static ArrayList<SignInModel.UserData.GamesData.GameData> getUserData(Context context) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = sharedPrefs.getString(USER_GAME_DATA, "");
        Type type = new TypeToken<List<SignInModel.UserData.GamesData.GameData>>() {
        }.getType();
        //ArrayList<SignInModel.UserData.GamesData.GameData> userGameDataarrayList = gson.fromJson(json, type);
        return gson.fromJson(json, type);
    }

    public static void setUserGameData(ArrayList<SignInModel.UserData.GamesData.GameData> gamesDataArrayList, Context context) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        Gson gson = new Gson();

        String json = gson.toJson(gamesDataArrayList);

        editor.putString(USER_GAME_DATA, json);
        editor.apply();
        editor.commit();
        // getUserData();
    }

    public static void setUserGameDataLoopj(ArrayList<GamesDataModel> gamesDataArrayList, Context context) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        Gson gson = new Gson();

        String json = gson.toJson(gamesDataArrayList);

        editor.putString(USER_GAME_DATA, json);
        editor.apply();
        editor.commit();
        // getUserData();
    }

    public static String getUserContestId(ArrayList<ContestDetailsModel.ContestsData.UsersData> usersDataArrayList, Context context) {

        String contestUserId = "";
        for (int i = 0; i < usersDataArrayList.size(); i++) {
            if (Pref.getValue(context, Constants.UserID, "", Constants.FILENAME).equalsIgnoreCase(usersDataArrayList.get(i).UserID)) {
                contestUserId = usersDataArrayList.get(i).ContestUserID;
                break;
            }
        }
        return contestUserId;

    }


    public static void SnakeMessageYellow(View view, String mMessage) {
        try {
            if (view == null || mMessage == null || mMessage.isEmpty())
                return;
            TSnackbar snackbar = TSnackbar.make(view, mMessage, TSnackbar.LENGTH_SHORT);

            snackbar.getView().setBackgroundColor(Color.parseColor("#E29618"));
            TextView textView = snackbar.getView().findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
            Typeface typeface = ResourcesCompat.getFont(view.getContext(), R.font.poppins);
            textView.setMaxLines(4);
            textView.setTypeface(typeface);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }


    }

    public static boolean CheckRestrictedStates(Context context) {
        List<String> restricted_states = Arrays.asList(context.getResources().getStringArray(R.array.restriced_states));
        boolean isRestricted = false;
        if (!restricted_states.isEmpty()) {
            for (int i = 0; i < restricted_states.size(); i++) {
                if (Pref.getValue(context, Constants.State, "", Constants.FILENAME).equalsIgnoreCase(restricted_states.get(i))) {
                    isRestricted = true;
                    break;
                }
            }
        }

        return isRestricted;
    }

    public static boolean isCitizenOfIndia(Context context) {
        boolean isCitizen = true;
        if (!Pref.getValue(context, Constants.CountryCode, "", Constants.FILENAME).equalsIgnoreCase("+91")) {
            isCitizen = false;
        }
        return isCitizen;
    }

    public static void DownloadReceipt(Context context, Uri uri) {
        DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI |
                DownloadManager.Request.NETWORK_MOBILE);

// set title and description
        request.setTitle("File Downloading");
        request.setDescription("Please wait while the receipt is being downloaded.");

        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

//set the local destination for download file to a path within the application's external files directory
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "Receipt.pdf");
        request.setMimeType("*/*");
        downloadManager.enqueue(request);
    }

    public static List<List<String>> addInvertedCommas(List<List<String>> strings) {
        for (int i = 0; i < strings.size(); i++) {
            for (int j = 0; j < strings.get(i).size(); j++) {
                String value = "\"" + strings.get(i).get(j) + "\"";
                strings.get(i).set(j, value);
            }
        }
        return strings;
    }

    public static void ChangeEnvironment(boolean ENVIRONMENT) {
        if (ENVIRONMENT) {
            BASE_API = LIVE_APi;
            CASHFREE_TYPE = CASHFREE_PROD;
            NOTIFY_URL = NOTIFY_URL_LIVE;
            CASHFREE_APP_ID = CASHFREE_LIVE_APP_ID;
            SOCKET_API = SOCKET_BASE_API;
        } else {
            BASE_API = TEST_BASE_API;
            CASHFREE_TYPE = CASHFREE_TEST;
            NOTIFY_URL = NOTIFY_URL_TEST;
            CASHFREE_APP_ID = CASHFREE_TEST_APP_ID;
            SOCKET_API = SOCKET_TEST_API;
        }
    }
}
