package com.esports.gamerjipro.Utils

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import com.androidadvance.topsnackbar.BuildConfig
import com.androidadvance.topsnackbar.TSnackbar
import com.esports.gamerjipro.R
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonNull
import com.google.gson.JsonObject
import java.text.DecimalFormat

val VERBOSE = Log.VERBOSE
val DEBUG = Log.DEBUG
val INFO = Log.INFO
val WARN = Log.WARN
val ERROR = Log.ERROR
val WTF = Log.ASSERT

private val TAG = "LogHelper"

fun log(level: Int, msg: String?, throwable: Throwable?) {
    if (BuildConfig.DEBUG) {
        val elements = Throwable().stackTrace
        var callerClassName = "?"
        var callerMethodName = "?"
        var callerLineNumber = "?"
        if (elements.size >= 4) {
            callerClassName = elements[3].className
            callerClassName = callerClassName.substring(callerClassName.lastIndexOf('.') + 1)
            if (callerClassName.indexOf("$") > 0) {
                callerClassName = callerClassName.substring(0, callerClassName.indexOf("$"))
            }
            callerMethodName = elements[3].methodName
            callerMethodName = callerMethodName.substring(callerMethodName.lastIndexOf('_') + 1)
            if (callerMethodName == "<init>") {
                callerMethodName = callerClassName
            }
            callerLineNumber = elements[3].lineNumber.toString()
        }

        val stack = "[" + callerClassName + "." + callerMethodName + "():" + callerLineNumber + "]" + if (TextUtils.isEmpty(msg)) "" else " "

        when (level) {
            VERBOSE -> Log.v(TAG, stack + msg!!, throwable)
            DEBUG -> Log.d(TAG, stack + msg!!, throwable)
            INFO -> Log.i(TAG, stack + msg!!, throwable)
            WARN -> Log.w(TAG, stack + msg!!, throwable)
            ERROR -> Log.e(TAG, stack + msg!!, throwable)
            WTF -> Log.wtf(TAG, stack + msg!!, throwable)
            else -> {
            }
        }
    }
}

fun Any.logd(tag: String = "TAG", throwable: Throwable? = null) {
    log(DEBUG, this.toString(), throwable)
}

fun Any.loge(tag: String = "", throwable: Throwable? = null) {
    log(ERROR, this.toString(), throwable)
}

fun Any.logv(tag: String = "", throwable: Throwable? = null) {
    log(VERBOSE, this.toString(), throwable)
}

fun Any.logw(tag: String = "", throwable: Throwable? = null) {
    log(WARN, this.toString(), throwable)
}

fun Any.logwtf(tag: String = "", throwable: Throwable? = null) {
    log(WTF, this.toString(), throwable)
}

fun Any.logi(tag: String = "", throwable: Throwable? = null) {
    log(INFO, this.toString(), throwable)
}

fun Float.toFormat(): String = DecimalFormat("###.00").format(this)

fun JsonObject.jsonString(key: String): String = if (this.get(key) != null && JsonNull.INSTANCE != this.get(key)) this.get(key).asString else ""

fun JsonObject.jsonInt(key: String): Int = if (this.get(key) != null && JsonNull.INSTANCE != this.get(key)) this.get(key).asInt else 0

fun JsonObject.jsonFloat(key: String): Float = if (this.get(key) != null && JsonNull.INSTANCE != this.get(key)) this.get(key).asFloat else 0.0f

fun JsonElement.jsonArray(key: String): JsonArray = if (this is JsonObject && this.asJsonObject.get(key) != null && this.asJsonObject.get(key) != JsonNull.INSTANCE) this.asJsonObject.get(key).asJsonArray else JsonArray()

fun JsonElement.jsonString(key: String): String = if (this is JsonObject && this.asJsonObject.get(key) != null && this.asJsonObject.get(key) != JsonNull.INSTANCE) this.asJsonObject.get(key).asString else ""

fun JsonElement.jsonString(obj: String, key: String): String = if (this is JsonObject && this.asJsonObject.get(obj) != null && this.asJsonObject.get(obj) != JsonNull.INSTANCE) this.asJsonObject.get(obj).jsonString(key) else ""

fun JsonElement.jsonInt(obj: String, key: String): Int = if (this is JsonObject && this.asJsonObject.get(obj) != null && this.asJsonObject.get(obj) != JsonNull.INSTANCE) this.asJsonObject.get(obj).jsonInt(key) else 0

fun JsonElement.jsonFloat(obj: String, key: String): Float = if (this is JsonObject && this.asJsonObject.get(obj) != null && this.asJsonObject.get(obj) != JsonNull.INSTANCE) this.asJsonObject.get(obj).jsonFloat(key) else 0F

fun JsonElement.jsonInt(key: String): Int = if (this is JsonObject && this.asJsonObject.get(key) != null && this.asJsonObject.get(key) != JsonNull.INSTANCE && this.asJsonObject.get(key).jsonString().isNotEmpty()) this.asJsonObject.get(key).asInt else 0

fun JsonElement.jsonFloat(key: String): Float = if (this is JsonObject && this.asJsonObject.get(key) != null && this.asJsonObject.get(key) != JsonNull.INSTANCE && this.asJsonObject.get(key).jsonString().isNotEmpty()) this.asJsonObject.get(key).asFloat else 0f

fun String.toast(context: Context) {
    Toast.makeText(context, this, Toast.LENGTH_SHORT).show()
}

fun Fragment.putInt(key: String, value: Int): Fragment {
    val bundle = if (this.arguments != null) this.arguments else Bundle()
    bundle!!.putInt(key, value)
    this.arguments = bundle
    return this
}

fun Fragment.putString(key: String, value: String): Fragment {
    val bundle = if (this.arguments != null) this.arguments else Bundle()
    bundle!!.putString(key, value)
    this.arguments = bundle
    return this
}

fun View.visible(visible: Boolean) {
    this.visibility = if (visible) VISIBLE else GONE
}

fun String.warn(view: View) {
    val snackbar = TSnackbar.make(view, this, TSnackbar.LENGTH_SHORT)
    val textView = snackbar.view.findViewById<View>(com.androidadvance.topsnackbar.R.id.snackbar_text) as TextView
    textView.setTextColor(Color.WHITE)
    textView.typeface = ResourcesCompat.getFont(view.context, R.font.poppins)
    textView.textSize = 18.5f
    snackbar.setActionTextColor(Color.WHITE)
    snackbar.view.setBackgroundColor(Color.parseColor("#E29618"))
    snackbar.show()
}

fun Any.jsonString(): String = if (this == JsonNull.INSTANCE) "" else (this as JsonElement).asString