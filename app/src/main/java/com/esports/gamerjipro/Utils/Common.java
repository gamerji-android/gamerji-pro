package com.esports.gamerjipro.Utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

public class Common {

    /**
     * Insert Log
     *
     * @param mMessage - Message of the defined log
     */
    public static void insertLog(String mMessage) {
        Log.e("Tag", mMessage);
    }

    /**
     * Sets the toast message
     *
     * @param context - Context
     * @param message - Message for showing the toast
     */
    public static void setCustomToast(Context context, String message) {
        final Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        toast.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        }, 5000);
    }

    /**
     *
     * @param packageName - Package Name
     * @return - Return boolean value
     */
    public static boolean isAppInstalled(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        boolean installed = false;
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            installed = false;
        }
        return installed;
    }
}
