package com.esports.gamerjipro.Utils;

public class Proguard
{
   /* -keepclassmembers class * implements java.io.Serializable {
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

-keepnames class com.facebook.FacebookActivity
-keepnames class com.facebook.CustomTabActivity

-keep class com.facebook.all.All

-keep public class com.android.vending.billing.IInAppBillingService {
    public static com.android.vending.billing.IInAppBillingService asInterface(android.os.IBinder);
    public android.os.Bundle getSkuDetails(int, java.lang.String, java.lang.String, android.os.Bundle);
}

-keep class com.esports.gamerjipro.** { *; }
-keepattributes Signature
-keepattributes *Annotation*
        -keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }

-dontwarn okhttp3.**

        -verbose
# Use ProGuard only to get rid of unused classes
-dontobfuscate
-dontoptimize
-keepattributes *
        -keep class !com.bumptech.glide.repackaged.**,com.bumptech.glide.**

        # Keep the entry point to this library, see META-INF\services\javax.annotation.processing.Processor
-keep class com.bumptech.glide.annotation.compiler.GlideAnnotationProcessor


# "duplicate definition of library class"
        -dontnote sun.applet.**
        # "duplicate definition of library class"
        -dontnote sun.tools.jar.**
        # Reflective accesses in com.google.common.util.concurrent.* and some others
-dontnote com.bumptech.glide.repackaged.com.google.common.**
        # com.google.common.collect.* and some others (….common.*.*)
-dontwarn com.google.j2objc.annotations.Weak
# com.google.common.util.concurrent.FuturesGetChecked$GetCheckedTypeValidatorHolder$ClassValueValidator
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
#-dontwarn **
        -keepclassmembers class fqcn.of.javascript.interface.for.webview {
    public *;}
-keep class cz.msebera.android.httpclient.** { *; }
-keep class com.loopj.android.http.** { *; }


-keepclassmembers,allowshrinking,allowobfuscation class com.android.volley.NetworkDispatcher {
    void processRequest();
}
-keepclassmembers,allowshrinking,allowobfuscation class com.android.volley.CacheDispatcher {
    void processRequest();
}
-dontwarn android.support.v7.**
        -keep class android.support.v7.** { *; }
-keep interface android.support.v7.** { *; }
-dontwarn android.support.v4.app.**
        -keep class android.support.v4.app.** { *; }
-keep interface android.support.v4.app.** { *; }
-dontwarn com.google.android.material.**
        -keep class com.google.android.material.** { *; }

-dontwarn androidx.**
        -keep class androidx.** { *; }
-keep interface androidx.** { *; }*/
}
