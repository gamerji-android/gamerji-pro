package com.esports.gamerjipro.Utils

import com.esports.gamerjipro.Utils.DateTimeUtil.difference
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateTimeUtil {
    var difference = 0L
    val standardTimeZone = "UTC"
}

fun currentTimeMillis(): Long {
    return System.currentTimeMillis() + difference
}

fun currentTimeMillis(millis: Long) {
    difference = System.currentTimeMillis() - millis
}

fun String.getDate(datePattern: String): Date? {
    val inputFormat = SimpleDateFormat(datePattern, Locale.getDefault())
    try {
        return inputFormat.parse(this)
    } catch (e: ParseException) {
        e.printStackTrace()
    }

    return null
}

fun Long.getDateString(datePattern: String): String {
    val formatter = SimpleDateFormat(datePattern, Locale.getDefault())

    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this
    return formatter.format(calendar.time)
}

fun Long.getDateStringWithDaySuffix(datePattern: String): String {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this
    val suffix = getDaySuffix(calendar.get(Calendar.DATE))
    val formatter = SimpleDateFormat(datePattern.replace("X", suffix), Locale.getDefault())
    return formatter.format(calendar.time)
}

fun getDaySuffix(n: Int): String {
    if (n in 11..13) {
        return "th"
    }
    return when (n % 10) {
        1 -> "st"
        2 -> "nd"
        3 -> "rd"
        else -> "th"
    }
}

fun String.convertDateFromStandardTimeZone(datePattern: String): String {
    var outputDateStr = ""
    try {

        val inputFormat = SimpleDateFormat(datePattern, Locale.getDefault())
        inputFormat.timeZone = TimeZone.getTimeZone(DateTimeUtil.standardTimeZone)

        val outputFormat = SimpleDateFormat(datePattern, Locale.getDefault())
        outputFormat.timeZone = TimeZone.getDefault()

        val date = inputFormat.parse(this)
        if (date != null) {
            outputDateStr = outputFormat.format(date)
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }

    return outputDateStr
}

fun String.convertDateToStandardTimeZone(datePattern: String): String {
    var outputDateStr = ""
    try {

        val inputFormat = SimpleDateFormat(datePattern, Locale.getDefault())
        inputFormat.timeZone = TimeZone.getDefault()

        val outputFormat = SimpleDateFormat(datePattern, Locale.getDefault())
        outputFormat.timeZone = TimeZone.getTimeZone(DateTimeUtil.standardTimeZone)

        val date = inputFormat.parse(this)
        if (date != null) {
            outputDateStr = outputFormat.format(date)
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }

    return outputDateStr
}

fun Int.to2Digit(): String {
    return (if (this < 10) "0$this" else this.toString())
}

fun String.changeDatePattern(inputPattern: String, outputPattern: String): String {
    var outputDateStr = ""
    try {

        val inputFormat = SimpleDateFormat(inputPattern, Locale.getDefault())
        inputFormat.timeZone = TimeZone.getTimeZone("GMT+5:30")

        val outputFormat = SimpleDateFormat(outputPattern, Locale.getDefault())
        val date = inputFormat.parse(this)
        if (date != null) {
            outputDateStr = outputFormat.format(date)
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }

    return outputDateStr
}
