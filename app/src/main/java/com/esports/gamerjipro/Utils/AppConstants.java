package com.esports.gamerjipro.Utils;

public class AppConstants {

    // Games
    public final static String GAME_P_MOBILE = "P MOBILE";
    public final static String GAME_CLASH_ROYALE = "Clash Royale";

    // Games Status Id
    public final static String GAME_STATUS_ID_WAITING = "1";
    public final static String GAME_STATUS_ID_STARTED = "2";
    public final static String GAME_STATUS_ID_IN_PROGRESS = "3";
    public final static String GAME_STATUS_ID_REVIEWING = "4";
    public final static String GAME_STATUS_ID_COMPLETED = "5";
    public final static String GAME_STATUS_ID_CANCELLED = "6";

    // Games Status
    public final static String GAME_STATUS_WAITING = "Waiting";
    public final static String GAME_STATUS_STARTED = "Started";
    public final static String GAME_STATUS_LIVE = "Live";
    public final static String GAME_STATUS_REVIEWING = "Reviewing";
    public final static String GAME_STATUS_COMPLETED = "Completed";
    public final static String GAME_STATUS_CANCELLED = "Cancelled";

    // Bundle Keys
    public final static String BUNDLE_STREAM_NAME = "StreamName";
    public final static String BUNDLE_STATUS = "Status";
    public final static String BUNDLE_STREAMER_STATUS = "StreamerStatus";
    public final static String BUNDLE_STREAMER_REASON = "StreamerReason";
    public final static String BUNDLE_STREAMER_DISPLAY_STATUS = "StreamerDisplayStatus";
    public final static String BUNDLE_PLAYER_NAME = "PlayerName";
    public final static String BUNDLE_QUICK_GAME_TYPE = "QuickGameType";
    public final static String BUNDLE_QUICK_GAME_CATEGORY_ID = "QuickGameCategoryId";
    public final static String BUNDLE_QUICK_GAME_ID = "QuickGameId";
    public final static String BUNDLE_QUICK_GAME_URL = "QuickGameURL";
    public final static String BUNDLE_QUICK_GAME_DISPLAY_MODE = "QuickGameDisplayMode";
    public final static String BUNDLE_QUICK_GAME_JOINED_ID = "QuickGamejoinedId";
    public final static String BUNDLE_VIEW_ALL_VIDEOS_CHANNEL_ID = "ViewAllVideosChannelId";
}
